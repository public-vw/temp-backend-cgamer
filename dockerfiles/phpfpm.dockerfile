FROM bitnami/php-fpm:7.4

RUN apt update
RUN apt install -y build-essential
RUN apt install -y autoconf gcc make curl php-curl
RUN apt install -y php-http php-pecl-http net-tools

RUN pecl install -o -f redis

#RUN pecl install xdebug

RUN  rm -rf /tmp/pear

USER root
