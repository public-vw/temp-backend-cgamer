#!/bin/bash

OS='UNKNOWN'

if [[ $(uname) == "Darwin" ]]; then
    OS='macOS'
elif [[ $(uname) == "Linux" ]]; then
    OS='linux'
else
    echo "Unsupported OS"
    exit 1
fi

echo "Running in $OS"

#--------------------------------------------------------------------------------

if [[ $OS == "Linux" ]]; then
    HOST_IP=$(ip -4 addr show docker0 | grep -e 'inet \K[\d.]+')
    export HOST_IP
fi

#if [[ "$OSTYPE" == "darwin"* ]]; then
#    HOST_IP=$(ifconfig en0 | grep inet | awk '$1=="inet" {print $2}')
#    export HOST_IP
#fi


docker compose up -d $@

