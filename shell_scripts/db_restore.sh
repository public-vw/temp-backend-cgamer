#!/bin/bash

source ../configs/db.env
backup_file=$1".sql"
command="mysql -u$MARIADB_USER -p\"$MARIADB_PASSWORD\" $MARIADB_DATABASE < $backup_file"

# shellcheck disable=SC2164
cd ../DB_Backups
tar xzf $1.tar.gz $1.sql

docker compose exec --workdir=/backup mariadb /bin/bash -c "$command"

rm -f $1.sql
