#!/bin/bash

if [[ $(pwd) == *"shell_scripts"* ]]; then
  echo "Please run this script from root folder, using bash ./shell_scripts/starter.sh"
  exit 1
fi

echo "Makes global .env file"
cp .env.example .env

vim .env

mkdir data
chmod 777 data

(
  source .env

  if [ "$MODE" == "prod" ]; then
    echo "Running in production mode"
    cp lara_root/.env.local lara_root/.env.prod
    vim lara_root/.env.prod
  fi

  ./shell_scripts/run_docker.sh

  ./composer.sh install

  if [ "$MODE" == "prod" ]; then
    echo "Regenerates key in production mode"
    ./artisan.sh key:generate
  fi

  ./artisan.sh migrate --seed

)