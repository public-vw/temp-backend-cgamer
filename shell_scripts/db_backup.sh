#!/bin/bash

# shellcheck disable=SC2027
current_date=$(date +"%Y%m%d")
current_time=$(date +"%H%M%S")
# shellcheck disable=SC2034
random_code=$(cat /dev/urandom | tr -dc 'A-Za-z' | fold -w 3 | head -n 1)
filename="db_"$current_date"_"$current_time"_"$random_code

source ../configs/db.env
backup_file=$filename".sql"
command1="mysqldump -u$MARIADB_USER -p\"$MARIADB_PASSWORD\" $MARIADB_DATABASE > $backup_file"
command2="tar czf $filename.tar.gz $filename.sql;"

docker compose exec -u $(id -u):$(id -g) --workdir=/backup mariadb /bin/bash -c "$command1"
docker compose exec -u $(id -u):$(id -g) --workdir=/backup mariadb /bin/bash -c "$command2"

cd ../DB_Backups
rm -f $backup_file
