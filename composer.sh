#!/bin/bash

show_help() {
    echo "Usage: $0 [OPTIONS] COMMAND"
    echo "Run docker compose with specific user settings and platform requirements."
    echo ""
    echo "Options:"
    echo "  -n, --no-ignore      Do not include --ignore-platform-req=ext-gd"
    echo "  --admin              Run without the -u <uid>:<gid> option"
    echo "  -h, --help           Display this help and exit"
}

parse_arguments() {
    for arg in "$@"; do
        case $arg in
            -n|--no-ignore)
                IGNORE_OPTION=""
                ;;
            --admin)
                USER_OPTION=""
                ;;
            -h|--help)
                show_help
                exit 0
                ;;
            *)
                # Add the argument to filtered_args unless it's a known option
                filtered_args+=("$arg")
                ;;
        esac
    done
}

USER_OPTION="-u $(id -u):$(id -g)"
IGNORE_OPTION="--ignore-platform-req=ext-zip --ignore-platform-req=ext-gd"
filtered_args=()

parse_arguments "$@"

docker compose run $USER_OPTION composer "${filtered_args[@]}" $IGNORE_OPTION
