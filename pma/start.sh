#!/bin/bash

source .env
docker run --rm --name ${DOCKER_NAME}-pmaserv -d --network ${DOCKER_NAME}_db_tier --link ${DOCKER_NAME}-mariadb:db -p ${PMA_PORT:-8080}:80 phpmyadmin
