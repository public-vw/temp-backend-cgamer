#!/bin/bash

# panel name must be in UpperCamelCase
##example: ./makeNewPanel.sh NewPanel

if [ "$#" -lt 1 ]; then
    echo "USAGE: ./makeNewPanel.sh NewPanel"
    echo "EXAMPLE: ./makeNewPanel.sh Agency"
    echo
    echo "Panel -> Admin, Sysadmin, ... (role name, capital case)"
    exit
fi

#change current directory
cd ..

lower=$(echo "$1" | sed -r 's/([A-Z])/_\L\1/g' | sed 's/^_//')

# copy routes
cp -a routes/{admin,"$lower"}

# copy resources/lang/en/
cp -a resources/lang/en/{admin,"$lower"}

# copy resources/assets/
cp -a resources/assets/{admin,"$lower"}

# copy resources/views/panel
cp -a resources/views/{admin,"$lower"}
## change titles
find resources/views/"$lower" -type f -exec sed -i -e 's/Admin/'"$1"'/g' {} \;
## change urls(assets paths)
find resources/views/"$lower" -type f -exec sed -i -e 's/admin/'"$lower"'/g' {} \;


# copy Controllers
cp -a app/Http/Controllers/{Admin,"$1"}
## change namespaces
find app/Http/Controllers/"$1" -type f -exec sed -i -e 's/Admin/'"$1"'/g' {} \;
## change views root folder name
find app/Http/Controllers/"$1" -type f -exec sed -i -e 's/admin/'"$lower"'/g' {} \;



# copy DataTables/Panel
cp -a app/DataTables/{Admin,"$1"}
## change namespaces
find app/DataTables/"$1" -type f -exec sed -i -e 's/Admin/'"$1"'/g' {} \;
## change views root folder name
find app/DataTables/"$1" -type f -exec sed -i -e 's/admin/'"$lower"'/g' {} \;



## Some manually performs: ##

echo "in RouteServiceProvider:"
echo "... copy function adminRoutes() -> newPanelRoutes()"
echo "... ... change admin to <new_panel>"
echo "... add the newPanelRoutes() inside boot()"
echo
echo "add <NewPanel>'s routes to config\routes.php"
echo
echo "add <NewPanel>'s assets to webpack.mix.js"
echo
echo "define new role <NewPanel>"
echo
echo "define route prefixes in config/routes.php"
echo
echo "adds sidebar seeder and add <newPanel>Composer in ViewComposers"
echo
echo "define <newPanel>Composer in ComposerServiceProvider"
echo
echo "Note: this bash file only makes EN lang folder for <NewPanel>"
