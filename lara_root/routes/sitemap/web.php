<?php

use Illuminate\Support\Facades\Route;

Route::get('main/{passcode?}', 'SitemapController@index');

Route::get('sitemap_links', 'SitemapController@links');
Route::get('sitemap_images', 'SitemapController@images');
