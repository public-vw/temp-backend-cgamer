<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'WelcomeController@index')->name('homepage');
Route::get('/token/{key?}', 'WelcomeController@index')->name('welcome.token');
Route::get('/upgrade/{key?}', 'WelcomeController@upgrade')->name('welcome.upgrade');
