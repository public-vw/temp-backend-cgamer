<?php

Route::get('role/permissions/{role}', 'RoleController@permissions')->name('roles.permissions.index');
Route::post('role/permissions/{role}/update', 'RoleController@updatePermissions')->name('roles.permissions.update');

Route::resource('roles', 'RoleController');
