<?php

Route::post('domains/all-list', 'DomainController@dbList')
    ->middleware('ajax')
    ->name('domains.list.all');

Route::resource('domains', 'DomainController');
