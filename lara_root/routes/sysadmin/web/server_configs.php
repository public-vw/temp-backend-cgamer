<?php

Route::get('server-configs/{server_config}/check', 'ServerConfigController@check')->name('server_configs.check');

Route::resource('server-configs', 'ServerConfigController')
    ->except(['edit','update'])
    ->names('server_configs');
