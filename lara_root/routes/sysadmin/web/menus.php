<?php

Route::post('menus/all-list', 'MenuController@dbList')
    ->middleware('ajax')
    ->name('menus.list.all');

Route::post('menus/config-list', 'MenuController@configList')
    ->middleware('ajax')
    ->name('menus.list.config');

Route::resource('menus', 'MenuController');
