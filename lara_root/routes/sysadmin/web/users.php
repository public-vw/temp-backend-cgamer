<?php

Route::post('users/all-list', 'UserController@dbList')
    ->middleware('ajax')
    ->name('users.list.all');

Route::get('users/roles/{user}', 'UserController@roles')->name('users.roles.index');
Route::put('users/roles/{user}/update', 'UserController@updateRoles')->name('users.roles.update');

Route::get('users/impersonate/{user}', 'UserController@setImpersonate')->name('users.impersonate.set');

Route::get('users/permissions/{user}', 'UserController@permissions')->name('users.permissions.index');
Route::post('users/permissions/{user}/update', 'UserController@updatePermissions')->name('users.permissions.update');

Route::resource('users', 'UserController');
