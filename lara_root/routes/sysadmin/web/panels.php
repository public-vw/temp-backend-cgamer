<?php

Route::post('panels/all-list', 'PanelController@dbList')
    ->middleware('ajax')
    ->name('panels.list.all');

Route::get('panels/exit', 'PanelController@exit')->name('panels.exit');

Route::resource('panels', 'PanelController');
