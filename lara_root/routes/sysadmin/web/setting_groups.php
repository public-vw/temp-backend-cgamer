<?php

Route::post('setting-groups/all-list', 'SettingGroupController@dbList')
    ->middleware('ajax')
    ->name('setting_groups.list.all');

Route::resource('setting-groups', 'SettingGroupController')->names('setting_groups');
