<?php
Route::get('backups', 'BackupController@index')
    ->name('backups.index');

Route::post('backups/backup', 'BackupController@backup')
    ->name('backups.backup');

Route::post('backups/restore', 'BackupController@restore')
    ->name('backups.restore');


