<?php

Route::get('component-generators', 'CompoGenController@index')->name('compo_generators.index');
Route::get('component-generators/{component}/form', 'CompoGenController@form')->name('compo_generators.form');

