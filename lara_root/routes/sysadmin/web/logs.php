<?php

Route::get('logs/cleanup','LogsController@freeup')->name('logs.cleanup');
Route::get('errors/cleanup','ErrorsController@cleanup')->name('errors.cleanup');

Route::resource('logs', 'LogController')->only('index', 'show', 'destroy');
Route::resource('errors','ErrorsController')->except('index', 'show', 'destroy');
