<?php
Route::get('structures', 'StructureController@index')
    ->name('structures.index');

Route::get('structures/field/create', 'StructureController@createField')
    ->name('structures.create.field');
Route::post('structures/field', 'StructureController@storeField')
    ->name('structures.store.field');

Route::get('structures/model/create', 'StructureController@createModel')
    ->name('structures.create.model');
Route::post('structures/model', 'StructureController@storeModel')
    ->name('structures.store.model');

Route::get('structures/model/{structure}', 'StructureController@show')
    ->name('structures.show.model');

Route::post('structures/all-list', 'StructureController@dbList')
    ->middleware('ajax')
    ->name('structures.list.all');

