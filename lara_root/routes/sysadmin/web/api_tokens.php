<?php

Route::delete('force-logout/{token}','ApiTokenController@forceLogout')->name('api_tokens.force_logout');
Route::resource('api_tokens', 'ApiTokenController')->only(['index', 'show']);
