<?php

use App\Models\BotGasfeeAlarm;
use App\Support\GetRates\GasFee;
use App\Support\Telegram\TelegramBotOperator;
use Carbon\Carbon;

Route::prefix('tests')->name('tests.')->group(function () {

    Route::get('google_ping', function () {

    });

    Route::get('global_server', function () {
        dd($_SERVER);
    });

    Route::get('getip', function () {
        return get_ip();
    });

    Route::get('trans', function () {
        $country = \App\Models\Country::find(1);

        $data = [
            'p'   => $country['title'],
            'q'   => $country->title,
            'fa'  => $country['title.fa'],
            'en'  => $country['title.en'],
            'fa2' => $country['title[fa]'],
            'en2' => $country['title[en]'],
            'fa3' => $country['title["fa"]'],
            'en3' => $country['title["en"]'],
        ];
        dd($data, $country->flag_iso);
        return view('sysadmin.items.tests.time');
    });

    Route::get('error/500', function () {
//        header($_SERVER['SERVER_PROTOCOL'] .' '. (int)$code ." Simulated Server Error", true, (int)$code);
//        header($_SERVER['SERVER_PROTOCOL'] .' 500 Internal Server Error', true, 500);
//        http_response_code(500);
//        status_header(500);
        require_once(app_path('this-should-cause-e500.esx'));
    });

    Route::get('error/{code?}', function ($code = 404) {
        throw new \Symfony\Component\HttpKernel\Exception\HttpException((int)$code);
//        throw new Exception('Sample Exception Message');
    });

    Route::get('tele', function () {
        alertAdminTelegram('Hello Master!');
        return 'done';
    });

    Route::get('time', function () {
        return view('sysadmin.items.tests.time');
    });

    Route::get('gasfee', function () {
        $chat_op = new TelegramBotOperator(2);
        $message = fn($gasfee) => '🎉 Cangragulations! 🎉'
            . "\n" . '<b>Gas Fee</b> now is $' . $gasfee
            . "\n===================="
            . "\n" . "<b>Date :</b> " . getFormedDate(Carbon::now())
            . "\n" . "<b>Time :</b> " . Carbon::now()->format('h:i:s A')
            . "\n\n\n" . "<b>Note :</b> This is a one-time alarm."
            . "\n" . "You can always set another <b>Alarm</b>"
            . "\n" . "Just touch >>> <b>/start</b>";

        $commands = [];

        $gasfee = null;
        $alarms = BotGasfeeAlarm::where('fired', false);
        if ($alarms->count() <= 0) return;

        $gasfee = (new GasFee())->get();
        $alarms = $alarms->where('threshold', '>', $gasfee)->get();
        foreach ($alarms as $alarm) {
            $alarm->update(['fired' => true]);
            $chat_op->sendMessageInlineMarkup($alarm->botUser->user_telegram_uid, trim($message($gasfee)), $commands, 'HTML', 1);
        }
    });
});

