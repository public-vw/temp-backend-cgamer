<?php

Route::get('backup/db','BackupController@dbBackup')->name('backups.db');
Route::get('backup/uploads','BackupController@uploadsBackup')->name('backups.uploads');
Route::post('backup/delete-locals','BackupController@deleteLocalBackup')->name('backups.local.delete');
Route::post('backup/list-locals','BackupController@listLocalBackup')->name('backups.local.list');
Route::post('backup/remote/upload','BackupController@uploadToRemoteBackup')->name('backups.remote.upload');
