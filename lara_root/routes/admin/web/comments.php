<?php

Route::post('comments/all-list', 'CommentController@dbList')
    ->middleware('ajax')
    ->name('comments.list.all');

Route::resource('comments', 'CommentController');
