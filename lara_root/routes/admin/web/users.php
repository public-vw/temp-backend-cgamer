<?php

Route::post('users/all-list', 'UserController@dbList')
    ->middleware('ajax')
    ->name('users.list.all');

Route::post('users/{user}/disable-two-step-auth', 'UserController@disableTwoStepAuth')
    ->name('users.disable_two_step_auth');

Route::post('users/{user}/reset-password', 'UserController@resetPassword')
    ->name('users.reset-password');

Route::resource('users', 'UserController');
