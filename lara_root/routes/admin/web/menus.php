<?php

Route::post('menus/all-list', 'MenuController@dbList')
    ->middleware('ajax')
    ->name('menus.list.all');

Route::post('menus/json-all', 'MenuController@json')
    ->middleware('ajax')
    ->name('menus.list.json');

Route::post('menus/json-all/save', 'MenuController@jsonSave')
    ->middleware('ajax')
    ->name('menus.save.json');

Route::get('menus/table-view', 'MenuController@tableIndex')->name('menus.table_view');

Route::resource('menus', 'MenuController');
