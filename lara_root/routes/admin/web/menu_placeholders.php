<?php

Route::post('menu-placeholders/all-list', 'MenuPlaceholderController@dbList')
    ->middleware('ajax')
    ->name('menu_placeholders.list.all');

Route::resource('menu-placeholders', 'MenuPlaceholderController')->names('menu_placeholders');
