<?php

Route::post('reviews/all-list', 'ReviewController@dbList')
    ->middleware('ajax')
    ->name('reviews.list.all');

Route::resource('reviews', 'ReviewController');
Route::post('reviews/bulk', 'ReviewController@bulk')->name('reviews.bulk');
Route::post('reviews/search', 'ReviewController@search')->name('reviews.search');
