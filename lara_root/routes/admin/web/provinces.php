<?php

Route::post('provinces/all-list', 'ProvinceController@dbList')
    ->middleware('ajax')
    ->name('provinces.list.all');

Route::resource('provinces', 'ProvinceController')->except('show');
