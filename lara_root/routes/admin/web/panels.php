<?php

Route::post('panels/all-list', 'PanelController@dbList')
    ->middleware('ajax')
    ->name('panels.list.all');
