<?php

Route::post('countries/all-list', 'CountryController@dbList')
    ->middleware('ajax')
    ->name('countries.list.all');

Route::resource('countries', 'CountryController')->except('show');
