<?php

Route::post('authors/all-list', 'AuthorController@dbList')
    ->middleware('ajax')
    ->name('authors.list.all');

Route::resource('authors', 'AuthorController');
