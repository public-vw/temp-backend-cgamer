<?php

//# new article category
//Route::get('article-categories/new', 'ArticleCategoryInFaceController@articleCategoryNew')->name('article_categories.inface.new');
# preview article category
Route::get('article-categories/{article_category}/preview', 'ArticleCategoryInFaceController@articleCategoryPreview')->name('article_categories.preview');
//# edit article category
Route::get('article-categories/inface/{article_category}/edit', 'ArticleCategoryInFaceController@articleCategoryEdit')->name('article_categories.inface.edit');
//# save article category, ajax
Route::post('article-categories/inface/new/save-draft', 'ArticleCategoryInFaceController@articleCategorySaveDraft')->name('article_categories.inface.save_draft');

//# save article category inner items, ajax
Route::post('article-categories/inface/save-item/{item_type}', 'ArticleCategoryInFaceController@articleCategorySaveItem')->name('article_categories.inface.save_items');

# save article category meta, ajax
Route::post('article-categories/inface/{article_category}/save-meta', 'ArticleCategoryInFaceController@articleCategorySaveMeta')->name('article_categories.save_meta');




Route::post('article-categories/json-all', 'ArticleCategoryController@json')
    ->middleware('ajax')
    ->name('article_categories.list.json');

Route::post('article-categories/json-all/save', 'ArticleCategoryController@jsonSave')
    ->middleware('ajax')
    ->name('article_categories.save.json');

Route::get('article-categories/table-view', 'ArticleCategoryController@tableIndex')->name('article_categories.table_view');

Route::resource('article-categories', 'ArticleCategoryController')->names('article_categories');
