<?php

Route::post('articles/all-list', 'ArticleController@dbList')
    ->middleware('ajax')
    ->name('articles.list.all');

Route::get('articles/accept-revision/{article}','ArticleController@acceptRevision')->name('articles.accept_revision');
Route::get('articles/ping-google/{article}','ArticleController@sendGooglePing')->name('articles.ping_google');

Route::resource('articles', 'ArticleController');
