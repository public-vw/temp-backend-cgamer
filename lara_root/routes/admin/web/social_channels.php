<?php

Route::post('social-channels/all-list', 'SocialChannelController@dbList')
    ->middleware('ajax')
    ->name('social_channels.list.all');

Route::resource('social-channels', 'SocialChannelController')->names('social_channels');
