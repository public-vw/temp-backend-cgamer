<?php

Route::post('cities/all-list', 'CityController@dbList')
    ->middleware('ajax')
    ->name('cities.list.all');

Route::resource('cities', 'CityController')->except('show');
