<?php

Route::post('requests/all-list', 'RequestController@dbList')
    ->middleware('ajax')
    ->name('requests.list.all');

Route::resource('requests', 'RequestController')->names('requests');
