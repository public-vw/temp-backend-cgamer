<?php

Route::post('attachment-types/all-list', 'AttachmentTypeController@dbList')
    ->middleware('ajax')
    ->name('attachment_types.list.all');

Route::resource('attachment-types', 'AttachmentTypeController')->names('attachment_types');
