<?php

Route::post('payments/all-list', 'PaymentController@dbList')
    ->middleware('ajax')
    ->name('payments.list.all');

Route::resource('payments', 'PaymentController');
