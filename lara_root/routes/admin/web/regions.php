<?php

Route::post('regions/all-list', 'RegionController@dbList')
    ->middleware('ajax')
    ->name('regions.list.all');

Route::resource('regions', 'RegionController')->except('show');
