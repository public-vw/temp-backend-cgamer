<?php

# upload image ajax
Route::post('attachments/images/upload-by-data/{type}', 'AttachmentInFaceController@uploadImageData')->name('attachments.upload.by_data');

Route::resource('attachments', 'AttachmentController');
