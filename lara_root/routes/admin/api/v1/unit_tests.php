<?php

Route::post('unit-tests/attached-files/check', 'UnitTestController@checkAttachedFiles')->name('unit_tests.attached_files.check');
Route::post('unit-tests/articles/attachments/check', 'UnitTestController@checkAttachmentsOnArticles')->name('unit_tests.articles.attachments.check');
