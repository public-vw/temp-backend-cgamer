<?php

Route::get('time',function (){
    return response()->json(\Carbon\Carbon::now());
})
    ->name('time')
    ->withoutMiddleware('auth:sanctum');


Route::get('auth-time',function (){
    $response = 'Auth @ '.\Carbon\Carbon::now();
    return response()->json($response);
})
    ->name('auth_time');

