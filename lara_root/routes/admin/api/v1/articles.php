<?php

Route::post('articles/{article}/change-author','ArticleController@changeAuthor')->name('articles.change_author');
Route::post('articles/{article}/change-category','ArticleController@changeCategory')->name('articles.change_category');
Route::post('articles/{article}/change-labels','ArticleController@changeLabels')->name('articles.change_labels');
Route::post('articles/list','ArticleController@list')->name('articles.list');

