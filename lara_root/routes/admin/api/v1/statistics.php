<?php

Route::post('statistics/articles/status', 'StatisticController@articlesStatus')->name('statistics.articles.status');
Route::post('statistics/article_categories/status', 'StatisticController@articleCategoriesStatus')->name('statistics.article_categories.status');
