<?php

Route::get('youtube/redirect', 'Auth\Oauth\YoutubeController@redirect')->name('youtube.redirect');
Route::any('youtube/callback', 'Auth\Oauth\YoutubeController@callback')->name('youtube.callback');
