<?php

Route::get('steam/redirect', 'Auth\Oauth\SteamController@redirect')->name('steam.redirect');
Route::any('steam/callback', 'Auth\Oauth\SteamController@callback')->name('steam.callback');
