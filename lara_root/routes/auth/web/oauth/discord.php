<?php

Route::get('discord/redirect', 'Auth\Oauth\DiscordController@redirect')->name('discord.redirect');
Route::any('discord/callback', 'Auth\Oauth\DiscordController@callback')->name('discord.callback');
