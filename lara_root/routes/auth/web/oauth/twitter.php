<?php

Route::get('twitter/redirect', 'Auth\Oauth\TwitterController@redirect')->name('twitter.redirect');
Route::any('twitter/callback', 'Auth\Oauth\TwitterController@callback')->name('twitter.callback');
