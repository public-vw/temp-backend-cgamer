<?php

Route::get('instagram/redirect', 'Auth\Oauth\InstagramController@redirect')->name('instagram.redirect');
Route::any('instagram/callback', 'Auth\Oauth\InstagramController@callback')->name('instagram.callback');
Route::get('instagram/de-auth', 'Auth\Oauth\InstagramController@deauthorize')->name('instagram.deauthorize');
Route::get('instagram/del-my-data', 'Auth\Oauth\InstagramController@deletemydata')->name('instagram.deletemydata');
