<?php

Route::get('telegram/redirect', 'Auth\Oauth\TelegramController@redirect')->name('telegram.redirect');
Route::any('telegram/callback', 'Auth\Oauth\TelegramController@callback')->name('telegram.callback');
