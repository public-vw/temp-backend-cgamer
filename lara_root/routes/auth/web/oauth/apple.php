<?php

Route::get('apple/redirect', 'Auth\Oauth\AppleController@redirect')->name('apple.redirect');
Route::any('apple/callback', 'Auth\Oauth\AppleController@callback')->name('apple.callback');
