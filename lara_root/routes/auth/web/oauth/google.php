<?php

Route::get('google/redirect', 'Auth\Oauth\GoogleController@redirect')->name('google.redirect');
Route::any('google/callback', 'Auth\Oauth\GoogleController@callback')->name('google.callback');
