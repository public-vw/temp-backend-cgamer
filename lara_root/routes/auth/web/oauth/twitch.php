<?php

Route::get('twitch/redirect', 'Auth\Oauth\TwitchController@redirect')->name('twitch.redirect');
Route::any('twitch/callback', 'Auth\Oauth\TwitchController@callback')->name('twitch.callback');
