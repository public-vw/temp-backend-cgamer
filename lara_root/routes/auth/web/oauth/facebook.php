<?php

Route::get('facebook/redirect', 'Auth\Oauth\FacebookController@redirect')->name('facebook.redirect');
Route::any('facebook/callback', 'Auth\Oauth\FacebookController@callback')->name('facebook.callback');
Route::get('facebook/de-auth', 'Auth\Oauth\FacebookController@deauthorize')->name('facebook.deauthorize');
Route::get('facebook/del-my-data', 'Auth\Oauth\FacebookController@deletemydata')->name('facebook.deletemydata');
