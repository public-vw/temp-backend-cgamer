<?php

Auth::routes([
    'register' => true,
    'login' => true,
]);
#TODO: activate below after adding settings table
//Auth::routes([
//    'register' => setting('auth.allow_register',false),
//    'login' => setting('auth.allow_login',true)
//]);

Route::get('logout', 'Auth\LoginController@logout')->name('logout.get');
Route::get('wait', 'Auth\LoginController@wait_to_activate')->name('wait_to_activate');
Route::get('switch-panel', 'GeneralController@switchToPanel')->name('switch_to_panel');

Route::get('two-step', 'GeneralController@showAuthTwo')->name('auth2');
Route::post('two-step', 'GeneralController@verifyAuthTwo')->name('auth2.check');
