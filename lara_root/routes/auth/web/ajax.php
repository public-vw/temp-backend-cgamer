<?php

Route::post('ajax/check', 'Auth\Ajax\LoginController@check')->name('ajax_auth.check');
Route::get('ajax/reset-password', 'GeneralController@resetPassword')->name('ajax_auth.reset_password');
Route::get('ajax/two-step', 'GeneralController@resendAjaxAuthTwo')->name('ajax_auth.resend_auth2');
Route::post('ajax/two-step', 'GeneralController@verifyAjaxAuthTwo')->name('ajax_auth.check_auth2');

