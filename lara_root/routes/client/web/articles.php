<?php

Route::post('articles/json-search', 'ArticleController@jsonSearch')
    ->middleware('ajax')
    ->name('articles.search.json');

# new article | operational :)
Route::get('articles/new', 'ArticleController@articleSingleNew')->name('articles.single.new');
# new article | operational :)
Route::get('articles/reached-limit', 'ArticleController@reachedLimit')->name('articles.limit');
# preview article | operational :)
Route::get('articles/{article}/preview', 'ArticleController@articleSinglePreview')->name('articles.single.preview');
# edit article | operational :)
Route::get('articles/{article}/edit', 'ArticleController@articleSingleEdit')->name('articles.single.edit');
# single article, ajax | semi operational :)
Route::post('articles/new/save-draft', 'ArticleController@articleSingleSave')->name('articles.single.save_draft');
Route::post('articles/set-state/{status}', 'ArticleController@articleChangeStatus')->name('articles.single.change_status');

# save article meta, ajax
Route::post('articles/{article}/save-meta', 'ArticleController@articleSaveMeta')->name('articles.save_meta');
