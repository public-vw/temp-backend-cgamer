<?php

Route::get('follow/{user}', 'FollowController@follow')->name('following.follow');
Route::get('unfollow/{user}', 'FollowController@unfollow')->name('following.unfollow');

Route::get('followers/kick-out/{user}', 'FollowController@kickOut')->name('followers.kick_out');
