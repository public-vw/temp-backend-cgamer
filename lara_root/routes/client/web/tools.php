<?php

Route::get('tools/exchange', 'ExchangeController@exchange')
    ->withoutMiddleware('auth')
    ->name('tools.exchange.view');

Route::get('tools/charge-wallet', 'ExchangeController@chargeWallet')
    ->name('tools.charge_wallet.view');

Route::post('tools/exchange/update-parameters', 'ExchangeController@updateParameters')
    ->withoutMiddleware('auth')
    ->name('tools.exchange.update_parameters');
