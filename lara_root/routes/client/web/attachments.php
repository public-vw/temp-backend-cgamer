<?php

# upload image ajax
Route::post('attachments/images/upload-by-data/{type}', 'AttachmentController@uploadImageData')->name('attachments.upload.by_data');

# upload image ajax by chunk
//Route::post('attachments/images/upload-by-data-map/chunk/{key}', 'AttachmentChunkController@uploadImageDataChunk')->name('attachments.upload.by_data.chunk');
//Route::post('attachments/images/upload-by-data-map/{type}', 'AttachmentChunkController@uploadImageDataChunkMap')->name('attachments.upload.by_data.by_chunk.map');
