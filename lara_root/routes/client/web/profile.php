<?php

Route::get('profile', 'ProfileController@index')->name('profile.index');
Route::post('profile/update/ajax', 'ProfileController@ajaxUpdate')->name('profile.ajax.update');
Route::put('profile/update', 'ProfileController@update')->name('profile.update');

Route::get('profile/change-password', 'ProfileController@showPasswordForm')->name('profile.show_password');
Route::post('profile/change-password', 'ProfileController@changePassword')->name('profile.change_password');
Route::post('profile/set-auth', 'ProfileController@setTwoStepAuth')->name('profile.set_two_step_auth');
Route::post('profile/stop-auth', 'ProfileController@removeTwoStepAuth')->name('profile.stop_two_step_auth');
