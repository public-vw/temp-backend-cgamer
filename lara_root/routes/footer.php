<?php

use Illuminate\Support\Facades\Route;

Route::get('.well-known/{folder}/{code}', function($folder,$code){
        return response('',200);
    })->name('well-known');

Route::get('{categories}/{article}.html', 'InterfaceZone\ArticleController@articleSingleView')
    ->where('categories','([\w\-\/]*)')
    ->name('articles.single.view');

Route::get('{categories}/', 'InterfaceZone\ArticleCategoryController@articleCategorySingleView')
    ->where('categories','([\w\-\/]*)')
    ->name('article_categories.single.view');
