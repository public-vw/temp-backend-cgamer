<?php

Route::get('auth-time', function () {
    $response = 'Auth @ ' . \Carbon\Carbon::now();
    return response()->json($response);
})->name('auth_time');

