<?php
//# all articles, view frame
//Route::get('articles', 'ArticleController@articleListView')->name('articles.list.view');
//# all articles, ajax
//Route::post('articles', 'ArticleController@articleList')->name('articles.list');

# single article, ajax
Route::post('articles/{article}', 'ArticleController@articleSingle')->name('articles.single');


