<?php

Route::get('terms', 'RouteController@terms')->name('terms');
Route::get('terms/european', 'RouteController@termsEuropean')->name('terms.european');
Route::get('terms/non-european', 'RouteController@termsNonEuropean')->name('terms.non_european');
