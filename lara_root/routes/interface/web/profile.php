<?php

Route::get('profile/{user}-', 'ProfileController@index');
Route::get('profile/{user}-{nickname?}', 'ProfileController@index')->name('profile.index');
