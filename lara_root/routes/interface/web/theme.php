<?php

Route::get('theme/{page}',function($page){
    if(!View::exists(config('routes.interface.view').'.pages.'.$page))
        return "Page <b>{$page}</b> not exists!";
    return view(config('routes.interface.view').'.pages.'.$page);
});
Route::get('theme/archive/{page}',function($page){
    if(!View::exists(config('routes.interface.view').'.pages._archive.'.$page))
        return "Page <b>{$page}</b> not exists!";
    return view(config('routes.interface.view').'.pages._archive.'.$page);
});
Route::get('theme/original/blade/{page}',function($page){
    if(!View::exists(config('routes.interface.view').'._theme._bladed.'.$page))
        return "Page <b>{$page}</b> not exists!";
    return view(config('routes.interface.view').'._theme._bladed.'.$page);
});
Route::get('theme/original/{page}',function($page){
    if(!View::exists(config('routes.interface.view').'._theme.'.$page))
        return "Page <b>{$page}</b> not exists!";
    return view(config('routes.interface.view').'._theme.'.$page);
});
