<?php

$re = '/\s+(\/\*(?:[^*]|\n|(?:\*(?:[^\/]|\n)))*\*\/)\s/m';
$subst = "\n\n";

$filepath = $argv[1];
$str = file_get_contents($filepath);

$result = preg_replace($re, $subst, $str);

echo $result;

file_put_contents($filepath, $result);
