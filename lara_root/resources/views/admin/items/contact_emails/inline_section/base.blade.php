@php
    $table = 'contact_emails';
    $title = __('Contact Emails');
    $headers = [
        'title'  => __('Title'),
        'email'  => __('Email'),
        'active' => __('Active'),
    ];
    $items = $VARIABLE->contactEmails ?? [];
    $morph = [
        'type'  => $VARIABLE->getTable(),
        'id'    => $VARIABLE->id,
    ];
@endphp

@compo("$VIEW_ROOT.components.inline_section")
