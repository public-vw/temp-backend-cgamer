@extends("$VIEW_ROOT.layouts.create_wide",[
    'MODEL' => 'ContactEmail',
    'TABLE' => $VIEW_FOLDERS[1],
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Title"),
                    "NAME"          => 'title',
                    "PLACEHOLDER"   => 'e.g. Agency branch 1 email',
                    "REQUIRED"      => true,
                ])

                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Email"),
                    "NAME"          => 'email',
                    "PLACEHOLDER"   => '',
                ])
            </div>
                @isset($morph)
                    @compo("$VIEW_ROOT.components.textfields.hidden",[
                        "NAME"  => 'emailable_type',
                        "VALUE" => $morph['type'],
                    ])

                    @compo("$VIEW_ROOT.components.textfields.hidden",[
                        "NAME"  => 'emailable_id',
                        "VALUE" => $morph['id'],
                    ])
                @else
                    <div class="row form-group">
                        @compo("$VIEW_ROOT.components.dropdowns.select2.single_array", [
                            "NAME"          => 'emailable_type',
                            "LABEL"         => __("Emailable Type"),
                            "PLACEHOLDER"   => __("Select Emailable Type..."),
                            "OPTIONS"       => config('morphs.emailable'),
                        ])

                        @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                            "NAME"                  => 'emailable_id',
                            "LABEL"                 => __("Emailable"),
                            "PLACEHOLDER"           => __("Select Emailable..."),
                            "MORPH_LIST"            => config('morphs.emailable'),
                            "AJAX_URL_DEPENDS_TO"   => '#emailable_type',
                        ])
                    </div>
                @endisset
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Order"),
                    "NAME"          => 'order',
                    "PLACEHOLDER"   => '',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.boxes.checkbox", [
                    "TITLE"    => __("Active"),
                    "NAME"     => 'active',
                    "CLASS"    => 'custom-control-input',
                    "SWITCH"   => true,
                    "CHECKED"  => true,
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.boxes.checkbox", [
                    "TITLE"   => __("Default"),
                    "NAME"    => 'default',
                    "CLASS"   => 'custom-control-input',
                    "SWITCH"  => true,
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.rich_textarea", [
                    "NAME"              => 'description',
                    "LABEL"             => __('Description'),
                    "CONTAINER_CLASS"   => 'col-12',
                    "ROWS"              => '10',
                    "MORE"              => 'cols="30"',
                ])
            </div>
        {{-- TODO create email verification --}}
        @endslot
    @endcomponent
@endsection
