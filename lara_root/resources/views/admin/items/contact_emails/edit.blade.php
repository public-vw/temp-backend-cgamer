@extends("$VIEW_ROOT.layouts.edit_wide",[
    'MODEL'     => 'ContactEmail',
    'VARIABLE'  => $contactEmail,
    'TITLE'     => $contactEmail->title,
    'TABLE'     => $VIEW_FOLDERS[1],
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Title"),
                    "NAME"          => 'title',
                    "VALUE"         => $contactEmail->title,
                    "PLACEHOLDER"   => 'e.g. Agency branch 1 email',
                    "REQUIRED"      => true,
                ])

                @compo("$VIEW_ROOT.components.dropdowns.select2.single_array", [
                    "NAME"          => 'emailable_type',
                    "LABEL"         => __("Emailable Type"),
                    "PLACEHOLDER"   => __("Select Emailable Type..."),
                    "OPTIONS"       => config('morphs.emailable'),
                    "VALUE"         => [
                        'id'    => $contactEmail->emailable_type ?? '',
                        'text'  => Str::ucfirst($contactEmail->emailable_type) ?? '',
                    ],
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Email"),
                    "NAME"          => 'email',
                    "VALUE"         => $contactEmail->email,
                    "PLACEHOLDER"   => '',
                ])

                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"                  => 'emailable_id',
                    "LABEL"                 => __("Emailable"),
                    "PLACEHOLDER"           => __("Select Emailable..."),
                    "MORPH_LIST"            => config('morphs.emailable'),
                    "AJAX_URL_DEPENDS_TO"   => '#emailable_type',
                    "VALUE"                 => [
                        'id'    => $contactEmail->emailable_id ?? '',
                        'text'  => $contactEmail->emailable->title ?? $contactEmail->emailable->username ?? '',
                    ],
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Order"),
                    "NAME"          => 'order',
                    "VALUE"         => $contactEmail->order,
                    "PLACEHOLDER"   => '',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.boxes.checkbox", [
                    "TITLE"    => __("Active"),
                    "NAME"     => 'active',
                    "CLASS"    => 'custom-control-input',
                    "SWITCH"   => true,
                    "CHECKED"  => $contactEmail->active,
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.boxes.checkbox", [
                    "TITLE"    => __("Default"),
                    "NAME"     => 'default',
                    "CLASS"    => 'custom-control-input',
                    "SWITCH"   => true,
                    "CHECKED"  => $contactEmail->default,
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.rich_textarea", [
                    "NAME"              => 'description',
                    "VALUE"             => $contactEmail->description,
                    "LABEL"             => __('Description'),
                    "CONTAINER_CLASS"   => 'col-12',
                    "ROWS"              => '10',
                    "MORE"              => 'cols="30"',
                ])
            </div>
        {{-- TODO create email verification --}}
        @endslot
    @endcomponent
@endsection
