@php
    $TABLE = model2table($variable->emailable_type);
    $emailable = $variable->emailable;
    if($TABLE){
        $route = Route::has("{$VIEW_ROOT}.{$TABLE}.show") ? route("{$VIEW_ROOT}.{$TABLE}.show", $emailable) : route("{$VIEW_ROOT}.{$TABLE}.edit", $emailable);
    }
@endphp

@compo("$VIEW_ROOT.components.clickables.link", [
    "URL"   => $route ?? '#',
    "TITLE" => $emailable->title ?? '',
])
