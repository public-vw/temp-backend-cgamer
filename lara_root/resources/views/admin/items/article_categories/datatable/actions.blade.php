@if($variable->status == config_key('enums.article_categories_status','active'))
    @compo("$VIEW_ROOT.components.clickables.link", [
        "URL"   => $variable->seoUrl(),
        "TITLE"    => "<i class='fa fa-eye'></i>",
        "CLASS"    => 'btn btn-sm btn-success',
        "RAW_HTML" => true,
        "TARGET" => '_blank',
    ])
@else
    @compo("$VIEW_ROOT.components.clickables.link", [
        "URL"   => route("admin.article_categories.preview", $variable->id),
        "TITLE"    => "<i class='fa fa-eye'></i>",
        "CLASS"    => 'btn btn-sm btn-info text-white',
        "RAW_HTML" => true,
        "TARGET" => '_blank',
    ])
@endif

@compo("$VIEW_ROOT.components.clickables.link", [
    "URL"   => route("admin.article_categories.inface.edit", $variable->id),
    "TITLE"    => "<i class='fa fa-pencil'></i>",
    "CLASS"    => 'btn btn-sm btn-primary',
    "RAW_HTML" => true,
    "TARGET" => '_blank',
])

@compo("$VIEW_ROOT.components.actions.delete", [
    "VARIABLE"  => $variable,
    "TABLE"     => $VIEW_FOLDERS[1],
])
