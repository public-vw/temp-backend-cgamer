@if($variable->parent_id)
    @compo("$VIEW_ROOT.components.clickables.link", [
    "URL"   => route("{$VIEW_ROOT}.article_categories.edit", $variable->parent->id),
    "TITLE" => Str::limit($variable->parent->title,30),
    ])
@else
    -- Root --
@endif
