@extends("$VIEW_ROOT.layouts.create_wide",[
    'MODEL' => 'Article Category',
    'TABLE' => $VIEW_FOLDERS[1],
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "NAME"       => 'title',
                    "LABEL"      => 'Title',
                    'REQUIRED'   => true,
                ])
                @compo("$VIEW_ROOT.components.dropdowns.mdb_nested_dropdown_ajax", [
                    "NAME"          => 'parent_id',
                    "LABEL"         => __("Parent"),
                    "AJAX_URL"      => route("{$VIEW_ROOT}.article_categories.list.json"),
                    "CONFIG"    => [
                        "allow_null"    => true,
                    ],
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "NAME"       => 'slug',
                    "LABEL"      => 'Slug',
                ])

                @compo("$VIEW_ROOT.components.textfields.text", [
                    "NAME"       => 'heading',
                    "LABEL"      => 'Heading',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.rich_textarea", [
                    "LABEL"      => 'Summary',
                    "NAME"       => 'summary',
                    "CONTAINER_CLASS"   => 'col-12',
                    "ROWS"              => '7',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.textarea", [
                    "LABEL"      => 'Description',
                    "NAME"       => 'description',
                    "CONTAINER_CLASS"   => 'col-12',
                    "ROWS"              => '10',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.dropdowns.select2.config_key", [
                    "NAME"          => 'status',
                    "LABEL"         => __("Status"),
                    "PLACEHOLDER"   => __("Select Status ..."),
                    "OPTIONS"       => config('enums.article_categories_status'),
                    "REQUIRED"      => true,
                ])
            </div>
        @endslot
    @endcomponent
@endsection
