@extends("$VIEW_ROOT.layouts.edit_wide",[
    'MODEL'     => 'Article Category',
    'VARIABLE'  => $articleCategory,
    'TITLE'     => $articleCategory->title,
    'TABLE'     => $VIEW_FOLDERS[1],
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "NAME"       => 'title',
                    "LABEL"      => 'Title',
                    "VALUE"      => $articleCategory->title,
                ])

                @compo("$VIEW_ROOT.components.dropdowns.mdb_nested_dropdown_ajax", [
                    "NAME"          => 'parent_id',
                    "LABEL"         => __("Parent"),
                    "AJAX_URL"      => route("{$VIEW_ROOT}.article_categories.list.json"),
                    "VALUE"       => [
                        'id'    => $articleCategory->parent_id ?? '',
                        'title' => $articleCategory->parent ? $articleCategory->parent->categoryChain('title') : [],
                    ],
                    "CONFIG"    => [
                        "allow_null"    => true,
                    ],
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "NAME"       => 'slug',
                    "LABEL"      => 'Slug',
                    "VALUE"      => $articleCategory->slug,
                ])

                @compo("$VIEW_ROOT.components.textfields.text", [
                    "NAME"       => 'heading',
                    "LABEL"      => 'Heading',
                    "VALUE"      => $articleCategory->heading,
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.rich_textarea", [
                    "LABEL"      => 'Summary',
                    "NAME"       => 'summary',
                    "VALUE"      => $articleCategory->summary,
                    "CONTAINER_CLASS"   => 'col-12',
                    "ROWS"              => '7',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.textarea", [
                    "LABEL"      => 'Description',
                    "NAME"       => 'description',
                    "VALUE"      => $articleCategory->description,
                    "CONTAINER_CLASS"   => 'col-12',
                    "ROWS"              => '10',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.dropdowns.select2.config_key", [
                    "NAME"          => 'status',
                    "LABEL"         => __("Status"),
                    "PLACEHOLDER"   => __("Select Status ..."),
                    "OPTIONS"       => config('enums.article_categories_status'),
                    "REQUIRED"      => true,
                    "VALUE"         => $articleCategory->status,
                ])
            </div>
        @endslot
    @endcomponent
@endsection

@section('outside')
    @includeRelativeSection("$VIEW_ROOT.attachments",[
        "VARIABLE"  => $articleCategory,
    ])

    @includeRelativeSection("$VIEW_ROOT.seo_details",[
        "VARIABLE"  => $articleCategory,
    ])

    @includeRelativeSection("$VIEW_ROOT.articles",[
        "VARIABLE"  => $articleCategory,
    ])
@endsection
