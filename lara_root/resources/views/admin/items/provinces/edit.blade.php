@extends("$VIEW_ROOT.layouts.edit_wide",[
    'MODEL'     => 'Province',
    'VARIABLE'  => $province,
    'TITLE'     => $province->title,
    'TABLE'     => $VIEW_FOLDERS[1],
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Title"),
                    "NAME"          => 'title',
                    "PLACEHOLDER"   => __("Province Name"),
                    "VALUE"         => "$province->title",
                    "REQUIRED"      => true,
                ])
                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'country_id',
                    "LABEL"         => __("Country"),
                    "PLACEHOLDER"   => __("Select Country ..."),
                    "AJAX_URL"      => route("$VIEW_ROOT.countries.list.all"),
                    "VALUE"         => [
                        'id'    => $province->country_id ?? '',
                        'text'  => $province->country->title ?? '',
                    ],
                    'CONFIG'        => [
                        'allow_null' => true,
                    ],
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Phone PreCode"),
                    "NAME"          => 'phone_precode',
                    "PLACEHOLDER"   => '44',
                    "VALUE"         => "$province->phone_precode",
                ])
            </div>
        @endslot
    @endcomponent
@endsection
