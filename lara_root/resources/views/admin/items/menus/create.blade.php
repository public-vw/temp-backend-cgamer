@extends("$VIEW_ROOT.layouts.create_wide",[
    'MODEL' => 'Menu',
    'TABLE' => $VIEW_FOLDERS[1],
])

@push('js')
    <script>
        function doBreakline(obj){
            if($(obj).is(':checked')){// is breakline
                $('.no-breakline').each(function () {
                    $(this).removeAttr('required').closest('div').slideUp(150);
                })
                return;
            }
            // is NOT breakline
            $('.no-breakline').each(function () {
                var requires = ['title', 'url_type'];
                $(this).closest('div').slideDown(100);

                requires.forEach(function (require) {
                    $('#' + require).attr('required', true);
                })
            })
        }
    </script>
@endpush

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.boxes.checkbox",[
                    "NAME"      => 'breakline',
                    "TITLE"     => __('Break Line'),
                    "CLASS"     => 'custom-control-input',
                    "ONCLICK"   => 'doBreakline(this)',
                    "SWITCH"    => true,
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Title"),
                    "NAME"          => 'title',
                    "MORE_CLASS"    => 'no-breakline',
                    "PLACEHOLDER"   => 'e.g. Settings',
                    "REQUIRED"      => true,
                ])

                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Slug"),
                    "NAME"          => 'slug',
                    "MORE_CLASS"    => 'no-breakline',
                    "PLACEHOLDER"   => 'e.g. setting',
                    "HELPER"        => 'slug should be unique, if you don\'t set slug it will generate automatically.',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'panel_id',
                    "LABEL"         => __("panel"),
                    "PLACEHOLDER"   => __("Select panel ..."),
                    "REQUIRED"      => true,
                    'CONFIG'        => [
                        'allow_null' => true,
                    ],
                    "AJAX_URL"      => route("$VIEW_ROOT.panels.list.all"),
                ])

                {{-- TODO don't show breaklines --}}
                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'parent_id',
                    "LABEL"         => __("Parent"),
                    "PLACEHOLDER"   => __("Select Parent ..."),
                    'CONFIG'        => [
                        'allow_null' => true,
                        'null_option_title' => '- ROOT -',
                    ],
                    "AJAX_URL"      => route("{$VIEW_ROOT}.{$VIEW_FOLDERS[1]}.list.all"),
                    "HELPER"        => 'if you don\'t set parent it well be independent.',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'placeholder_id',
                    "LABEL"         => __("Placeholder"),
                    "PLACEHOLDER"   => __("Placeholder"),
                    'CONFIG'        => [
                        'allow_null' => true,
                    ],
                    "AJAX_URL"      => route("$VIEW_ROOT.menu_placeholders.list.all"),
                    'DEPENDS_TO'    => '#panel_id',
                ])

                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Order"),
                    "NAME"          => 'order',
                    "TYPE"          => 'number',
                    "PLACEHOLDER"   => '',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Url"),
                    "NAME"          => 'url',
                    "MORE_CLASS"    => 'no-breakline',
                    "PLACEHOLDER"   => '',
                ])

                @compo("$VIEW_ROOT.components.dropdowns.select2.config_key", [
                    "NAME"          => 'url_type',
                    "LABEL"         => __("Url Type"),
                    "PLACEHOLDER"   => __("Select Url Type ..."),
                    "OPTIONS"       => config('enums.menus_url_type'),
                    "CLASS"         => 'no-breakline',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Class"),
                    "NAME"          => 'class',
                    "PLACEHOLDER"   => 'e.g. ',
                ])

                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Style"),
                    "NAME"          => 'style',
                    "PLACEHOLDER"   => 'e.g. margin: 2px;',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Icon"),
                    "NAME"          => 'icon',
                    "PLACEHOLDER"   => 'e.g. fa fa-arrow-left ',
                    "MORE_CLASS"    => 'no-breakline',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.boxes.checkbox", [
                    "TITLE"    => __("Active"),
                    "NAME"     => 'active',
                    "CLASS"    => 'custom-control-input',
                    "SWITCH"   => true,
                    "DEFAULT"  => 'active',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.boxes.checkbox", [
                    "TITLE"   => __("NoFollow"),
                    "NAME"    => 'nofollow',
                    "CLASS"   => 'custom-control-input no-breakline',
                    "SWITCH"  => true,
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.boxes.checkbox", [
                    "TITLE"         => __("Go New Tab"),
                    "NAME"          => 'go_new_tab',
                    "CLASS"         => 'custom-control-input no-breakline',
                    "SWITCH"        => true,
                ])
            </div>
        @endslot
    @endcomponent
@endsection
