@extends("$VIEW_ROOT.layouts.edit_wide",[
    'MODEL'     => 'Menu',
    'VARIABLE'  => $menu,
    'TITLE'     => $menu->title,
    'TABLE'     => $VIEW_FOLDERS[1],
])

@php($is_breakline = is_null($menu->title))

@push('js')
    <script>
        function doBreakline(obj){
            if($(obj).is(':checked')){// is breakline
                $('.no-breakline').each(function () {
                    $(this).removeAttr('required').closest('div').slideUp(150);
                })
                return;
            }
            // is NOT breakline
            $('.no-breakline').each(function () {
                var requires = ['title', 'url_type'];
                $(this).closest('div').slideDown(100);

                requires.forEach(function (require) {
                    $('#' + require).attr('required', true);
                })
            })
        }

        $(function (){
            doBreakline($('#breakline'));
        })
    </script>
@endpush

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.boxes.checkbox",[
                    "NAME"      => 'breakline',
                    "TITLE"     => __('Break Line'),
                    "CHECKED"   => $is_breakline,
                    "ONCLICK"   => 'doBreakline(this)',
                    "CLASS"     => 'custom-control-input',
                    "SWITCH"    => true,
                ])
            </div>

            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Title"),
                    "NAME"          => 'title',
                    "VALUE"         => $menu->title,
                    "PLACEHOLDER"   => 'e.g. Settings',
                    "REQUIRED"      => true,
                    "MORE_CLASS"    => 'no-breakline',
                ])

                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Slug"),
                    "NAME"          => 'slug',
                    "VALUE"         => $menu->slug,
                    "PLACEHOLDER"   => 'e.g. setting',
                    "HELPER"        => 'slug should be unique, if you don\'t set slug it will generate automatically.',
                    "MORE_CLASS"    => 'no-breakline',
                ])
            </div>
            @php($placeholder = $menu->placeholder)
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'panel_id',
                    "LABEL"         => __("Panel"),
                    "PLACEHOLDER"   => __("Select Panel ..."),
                    "AJAX_URL"      => route("$VIEW_ROOT.panels.list.all"),
                    "REQUIRED"      => true,
                    "VALUE"         => [
                        'id'    => $placeholder->panel_id ?? '',
                        'text'  => $placeholder->panel->title ?? '',
                    ],
                    'CONFIG'        => [
                        'allow_null'        => true,
                    ],
                ])

                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'parent_id',
                    "LABEL"         => __("Parent"),
                    "PLACEHOLDER"   => __("Select Parent ..."),
                    "AJAX_URL"      => route("{$VIEW_ROOT}.{$VIEW_FOLDERS[1]}.list.all"),
                    "HELPER"        => 'if you don\'t set parent it well be independent.',
                    "VALUE"         => [
                        'id'    => $menu->parent_id ?? '',
                        'text'  => $menu->parent->title ?? '',
                    ],
                    'CONFIG'        => [
                        'allow_null'        => true,
                        'null_option_title' => '- ROOT -',
                    ],
                ])
            </div>
            <div class="row form-group">
                {{-- TODO FIX DEPENDS_TO FOR PLACEHOLDER_ID --}}
                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'placeholder_id',
                    "LABEL"         => __("Placeholder"),
                    "PLACEHOLDER"   => __(""),
                    "AJAX_URL"      => route("$VIEW_ROOT.menu_placeholders.list.all"),
                    "VALUE"         => [
                        'id'    => $placeholder->id ?? '',
                        'text'  => $placeholder->title ?? '',
                    ],
                    'CONFIG'        => [
                        'allow_null' => true,
                    ],
                    'DEPENDS_TO'    => '#panel_id',
                ])

                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Order"),
                    "NAME"          => 'order',
                    "VALUE"         => $menu->order,
                    "TYPE"          => 'number',
                    "PLACEHOLDER"   => '',
                ])
            </div>

            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Url"),
                    "NAME"          => 'url',
                    "VALUE"         => $menu->url,
                    "PLACEHOLDER"   => '',
                    "MORE_CLASS"    => 'no-breakline',
                ])

                @compo("$VIEW_ROOT.components.dropdowns.select2.config_key", [
                    "NAME"          => 'url_type',
                    "LABEL"         => __("Url Type"),
                    "PLACEHOLDER"   => __("Select Url Type ..."),
                    "OPTIONS"       => config('enums.menus_url_type'),
                    "VALUE"         => $is_breakline ? '' : $menu->url_type,
                    "CLASS"         => 'no-breakline',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Class"),
                    "NAME"          => 'class',
                    "VALUE"         => $menu->class,
                    "PLACEHOLDER"   => 'e.g. ',
                ])

                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Style"),
                    "NAME"          => 'style',
                    "VALUE"         => $menu->style,
                    "PLACEHOLDER"   => 'e.g. margin: 2px;',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Icon"),
                    "NAME"          => 'icon',
                    "VALUE"         => $menu->icon,
                    "PLACEHOLDER"   => 'e.g. fa fa-arrow-left ',
                    "MORE_CLASS"    => 'no-breakline',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.boxes.checkbox", [
                    "TITLE"    => __("Active"),
                    "NAME"     => 'active',
                    "CLASS"    => 'custom-control-input',
                    "SWITCH"   => true,
                    "CHECKED"  => $menu->active,
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.boxes.checkbox", [
                    "TITLE"   => __("NoFollow"),
                    "NAME"    => 'nofollow',
                    "CLASS"   => 'custom-control-input no-breakline',
                    "SWITCH"  => true,
                    "CHECKED" => $menu->nofollow,
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.boxes.checkbox", [
                    "TITLE"   => __("Go New Tab"),
                    "NAME"    => 'go_new_tab',
                    "CLASS"   => 'custom-control-input no-breakline',
                    "SWITCH"  => true,
                    "CHECKED" => $menu->go_new_tab,
                ])
            </div>
        @endslot
    @endcomponent
@endsection

@section('outside')
    {{-- TODO: create attachments inline_section --}}
    {{--
        @includeRelativeSection("$VIEW_ROOT.attachments",[
            "VARIABLE"  => $article,
        ])
    --}}

    @includeRelativeSection("$VIEW_ROOT.seo_details",[
    "VARIABLE"  => $menu,
    ])
@endsection
