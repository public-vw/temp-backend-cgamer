@extends("$VIEW_ROOT.layouts.base")

@push('js')
    <script>
        let getURL = '{{ route("{$VIEW_ROOT}.{$VIEW_FOLDERS[1]}.list.json") }}';
        let setURL = '{{ route("{$VIEW_ROOT}.{$VIEW_FOLDERS[1]}.save.json") }}';
        let editItem = '{{ route("{$VIEW_ROOT}.{$VIEW_FOLDERS[1]}.edit",['#####']) }}';

        let $domenu = null;
        let $output = [];

        $(function () {
            $('#domenu').on('click', '.item-edit', function () {
                let id = $(this).closest('.dd3-content').find('input[name="id"]').val()
                let url = editItem.replace('#####', id)
                window.open(url, '_blank');
            })

            $('#domenu').on('click', '.item-details', function () {
                $(this).closest('.dd3-content').find('.dd-edit-box').toggle()
            })

            $('#save-items').click(function () {
                $.ajax({
                    type: 'POST',
                    url: setURL,
                    data: {
                        'list': $.parseJSON($output)
                    },
                    dataType: 'json',
                    success: function (response) {
                        console.log(response)
                    },
                    error: function (err) {
                        console.log(err)
                    }
                })

            })

            $.post(getURL, function (response) {
                $domenu = $('#domenu').domenu({
                    data: response,
                }).parseJson()
                    .on([
                        'onItemAdded',
                        'onSaveEditBoxInput',
                        'onItemDrop',
                        'onItemDrag',
                        'onItemRemoved',
                        'onItemEndEdit'
                    ], function (a, b, c) {
                        $output = $domenu.toJson()
                    })
            })
        })
    </script>
@endpush

@section('content')
    <div class="row">
        <div class="col">
            <h3 class="title-bar">
                <a href="javascript:;" id="save-items" class="btn btn-link">
                    <i class="fa fa-floppy-o"></i>&nbsp;Save Changes</a>
                <a href="{{route("{$VIEW_ROOT}.{$VIEW_FOLDERS[1]}.table_view")}}" class="btn btn-link">
                    <i class="fa fa-table"></i>&nbsp;Table View</a>
            </h3>
        </div>
    </div>

    <div class="menu-items-zone dd" id="domenu">
        <button class="dd-new-item">+</button>
        <li class="dd-item-blueprint">
            <button class="collapse" data-action="collapse" type="button" style="display: none;">–</button>
            <button class="expand" data-action="expand" type="button" style="display: none;">+</button>
            <div class="dd-handle dd3-handle">&nbsp;</div>
            <div class="dd3-content">
                <span class="item-name">[item_name]</span>
                <div class="dd-button-container">
                    <button class="item-edit bg-info text-white" data-id="[?id]"><i class="fa fa-pencil"></i></button>
                    <button class="item-add bg-success">+</button>
                    <button class="item-details">details</button>
                </div>
                <div class="dd-edit-box" style="display: none;">
                    <input type="text" name="title" autocomplete="off" placeholder="Title">
                    <input type="hidden" name="id"/>
                    {{--                           data-placeholder="Any nice idea for the title?"--}}
                    {{--                           data-default-value="doMenu List Item. 11 {?numeric.increment}">--}}
                    {{--   Old data has some issues in saving, that should I found before activating save button
                            But everything else is ready (Backend, ...) --}}
                    {{--                    <i class="end-edit">save</i>--}}
                </div>
            </div>
        </li>
        <ol class="dd-list"></ol>
    </div>
@endsection
@section('content0')
    <div class="he-tree">
        <div class="tree-children tree-root">
            <div class="tree-branch">
                <div class="tree-node-back">
                    <div class="tree-node">
                        <div class="nestable-item-content">
                            <div class="menu-title drag-trigger">
                                Home
                                <span class="menu-right"><span class="model-name">Custom</span>
                                        <i class="icon ion-md-arrow-dropdown"></i>
                                    </span>
                            </div>
                            <div class="menu-info" style="display: none;">
                                <div class="form-group">
                                    <label>Label</label> <input type="text" class="form-control input-sm">
                                </div>
                                <div class="form-group">
                                    <label>URL</label>
                                    <input type="text" class="form-control input-sm">
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Class</label>
                                            <input type="text" class="form-control input-sm">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group"><label>Target</label> <select
                                                class="input-sm form-control">
                                                <option value="">Normal</option>
                                                <option value="_blank">Open new tab</option>
                                            </select></div>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between"><a href="#"
                                                                               class="alert-text danger delete-menu-item">Delete</a>
                                    <span style="display: none;">Origin:  <a target="_blank"></a></span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
