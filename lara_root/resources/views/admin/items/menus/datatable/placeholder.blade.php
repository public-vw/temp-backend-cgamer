@if($variable->hasParent())
    @php($placeholder = $variable->placeholder)

@compo("$VIEW_ROOT.components.clickables.link", [
        "URL"   => route("$VIEW_ROOT.menu_placeholders.edit", $placeholder),
        "TITLE" => "$placeholder->title",
    ])
@else
    -
@endif
