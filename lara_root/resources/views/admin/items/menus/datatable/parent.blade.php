@if($variable->hasParent())
    @php($parent = $variable->parent)

    @compo("$VIEW_ROOT.components.clickables.link", [
        "URL"   => route("{$VIEW_ROOT}.{$VIEW_FOLDERS[1]}.show", $parent),
        "TITLE" => "$parent->title",
    ])
@else
    -
@endif
