@extends("$VIEW_ROOT.layouts.create_wide",[
    'MODEL' => 'Fact',
    'TABLE' => $VIEW_FOLDERS[1],
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Sentence"),
                    "NAME"          => 'sentence',
                    "CONTAINER_CLASS" => 'col-sm-9',
                    "MORE"  => 'style="direction:rtl"',
                ])
                @compo("$VIEW_ROOT.components.textfields.number", [
                    "LABEL"     => __("Probability"),
                    "NAME"      => 'probability',
                    "VALUE"     => 5,
                    "HELPER"    => '1->lowest , 10->highest',
                    "CONTAINER_CLASS" => 'col-sm-3',
                    "REQUIRED"  => true,
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.boxes.checkbox", [
                    "TITLE"   => __("Active"),
                    "NAME"    => 'active',
                    "CLASS"   => 'custom-control-input',
                    "SWITCH"  => true,
                ])
            </div>
        @endslot
    @endcomponent
@endsection
