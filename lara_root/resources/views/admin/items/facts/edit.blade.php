@extends("$VIEW_ROOT.layouts.edit_wide",[
    'MODEL'     => 'Fact',
    'VARIABLE'  => $fact,
    'TITLE'     => $fact->title,
    'TABLE'     => $VIEW_FOLDERS[1],
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Sentence"),
                    "NAME"          => 'sentence',
                    "VALUE"         => $fact->sentence,
                    "CONTAINER_CLASS" => 'col-sm-9',
                    "AUTOCOMPLETE"  =>  'false',
                    "MORE"  => 'style="direction:rtl"',
                ])
                @compo("$VIEW_ROOT.components.textfields.number", [
                    "LABEL"         => __("Probability"),
                    "NAME"          => 'probability',
                    "VALUE"         => $fact->probability,
                    "CONTAINER_CLASS" => 'col-sm-3',
                    "HELPER"   => '1->lowest , 10->highest',
                    "REQUIRED"      => true,
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.boxes.checkbox", [
                    "TITLE"   => __("Active"),
                    "NAME"    => 'active',
                    "CLASS"   => 'custom-control-input',
                    "SWITCH"  => true,
                    "CHECKED" => $fact->active,
                ])
            </div>
        @endslot
    @endcomponent
@endsection
