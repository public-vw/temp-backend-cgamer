@if(!$variable->hasRole('sysadmin@web'))
    @compo("$VIEW_ROOT.components.actions.delete", [
        "VARIABLE"  => $variable,
        "TABLE"     => $VIEW_FOLDERS[1],
    ])

    @compo("$VIEW_ROOT.components.actions.edit", [
        "VARIABLE"  => $variable,
        "TABLE"     => $VIEW_FOLDERS[1],
    ])
@endif
