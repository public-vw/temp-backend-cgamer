@extends("$VIEW_ROOT.layouts.base")

@section('title')
    {{ $user->name }} Profile | Change Password
@endsection

@section('content')
    @include("$VIEW_ROOT.includes.alerts")

    <form action="{{ route("$VIEW_ROOT.profile.change_password") }}" method="post">
        @csrf
        {{--            USE This if errors not showing on the forms--}}
        @include("$VIEW_ROOT.components.combinations.errors")

        <div class="lang-content-box">
            <div class="row">
                <div class="col-12">

                    <div class="row">
                        @compo("$VIEW_ROOT.components.textfields.password",[
                        "NAME"      => 'curr_password',
                        "LABEL"     => __('Current Password'),
                        "REQUIRED"  => true,
                        ])
                    </div>
                    <div class="row">
                        @compo("$VIEW_ROOT.components.textfields.password",[
                        "NAME"      => 'new_password',
                        "LABEL"     => __('New Password'),
                        "REQUIRED"  => true,
                        ])

                        @compo("$VIEW_ROOT.components.textfields.password",[
                        "NAME"      => 'new_password_confirmation',
                        "LABEL"     => __('New Password Confirmation'),
                        "REQUIRED"  => true,
                        ])
                    </div>

                    <div class="text-right div-submit">
                        @compo("$VIEW_ROOT.components.textfields.hidden",[
                            "NAME"  => 'after_action',
                            "VALUE" => 'stay',
                        ])
                        @compo("$VIEW_ROOT.components.clickables.submit",[
                            "TITLE" => '<i class="fa fa-check-circle-o"></i>&nbsp;'. __('Change Password'),
                            'CLASS' => 'btn-primary',
                        ])
                    </div>
                </div>
            </div>
        </div>

    </form>

    <hr/>

    @if($user->use_two_factor_auth)
        <form action="{{ route("$VIEW_ROOT.profile.stop_two_step_auth") }}"  method="post">
            @csrf()
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-md-5">
                            <b>Steps to <span class="text-danger">DISABLE</span> two step verification:</b>
                            <ul>
                                <li>Generate a token and write it in the box</li>
                                <li>Submit the token</li>
                            </ul>
                        </div>
                        <div class="col-md-7">
                            <div class="row">
                                @compo("$VIEW_ROOT.components.textfields.number",[
                                    "NAME"      => 'gtoken',
                                    "PLACEHOLDER"     => __('Google Auth Token'),
                                    "REQUIRED"  => true,
                                    "AUTOCOMPLETE" => false,
                                ])
                                <div class="col-md-6">
                                    @compo("$VIEW_ROOT.components.clickables.submit",[
                                        "TITLE" => '<i class="fa fa-check-circle-o"></i>&nbsp;'. __('Stop Two Step Auth'),
                                        'CLASS' => 'btn-danger btn-block',
                                    ])
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    @else
        <form action="{{ route("$VIEW_ROOT.profile.set_two_step_auth") }}" method="post">
            @csrf()
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-md-4 text-center">
                            {!! $googleToken !!}
                            <div>{{ $user->google_secret_key }}</div>
                        </div>
                        <div class="col-md-8">
                            <div class="row">
                                <b>Steps to <span class="text-success">ENABLE</span> two step verification:</b>
                                <ol>
                                    <li>Add secret code to your <b>Auth App</b>
                                        <ul>
                                            <li>Scan the QR-code with your <b>Google Authenticator App</b></li>
                                            <li>or, Copy Secret code and paste to your <b>Google Authenticator App</b></li>
                                        </ul>
                                    </li>
                                    <li>Generate a token and write it in the box</li>
                                    <li>Submit the token</li>
                                </ol>
                            </div>
                            <div class="row">
                                @compo("$VIEW_ROOT.components.textfields.number",[
                                    "NAME"      => 'gtoken',
                                    "PLACEHOLDER"     => __('Google Auth Token'),
                                    "REQUIRED"  => true,
                                    "CLASS" => 'full-width',
                                    "AUTOCOMPLETE" => false,
                                ])

                                <div class="col-md-6 float-right">
                                    @compo("$VIEW_ROOT.components.clickables.submit",[
                                        "TITLE" => '<i class="fa fa-check-circle-o"></i>&nbsp;'. __('Enable Two Step Auth'),
                                        'CLASS' => 'btn-success btn-block',
                                    ])
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    @endif
@endsection
