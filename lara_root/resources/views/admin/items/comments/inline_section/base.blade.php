@php
    $table = 'comments';
    $title = __('Comments');
    $headers = [
        'content' => __('Content'),
        'status'  => __('Status'),
    ];
    $items = $VARIABLE->comments ?? [];
    $morph = [
        'type'  => $VARIABLE->getTable(),
        'id'    => $VARIABLE->id,
    ];
@endphp

@compo("$VIEW_ROOT.components.inline_section")
