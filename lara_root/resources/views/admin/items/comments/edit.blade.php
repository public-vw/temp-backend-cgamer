@extends("$VIEW_ROOT.layouts.edit_wide",[
    'MODEL'     => 'Comment',
    'VARIABLE'  => $comment,
    'TITLE'     => $comment->title,
    'TABLE'     => $VIEW_FOLDERS[1],
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.dropdowns.select2.single_array", [
                    "NAME"          => 'commentable_type',
                    "LABEL"         => __("Commentable Type"),
                    "PLACEHOLDER"   => __("Select Commentable Type..."),
                    "OPTIONS"       => config('morphs.commentable'),
                    "VALUE"         => [
                        'id'    => $comment->commentable_type ?? '',
                        'text'  => Str::ucfirst($comment->commentable_type) ?? '',
                    ],
                ])

                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"                  => 'commentable_id',
                    "LABEL"                 => __("Commentable"),
                    "PLACEHOLDER"           => __("Select Commentable..."),
                    "MORPH_LIST"            => config('morphs.commentable'),
                    "AJAX_URL_DEPENDS_TO"   => '#commentable_type',
                    "VALUE"                 => [
                        'id'    => $comment->commentable_id ?? '',
                        'text'  => $comment->commentable->title ?? '',
                    ],
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'parent_id',
                    "LABEL"         => __("Parent"),
                    "PLACEHOLDER"   => __("Select Parent ..."),
                    "AJAX_URL"      => route("$VIEW_ROOT.comments.list.all"),
                    "VALUE"       => [
                        'id'    => $agency->parent_id ?? '',
                        'text'  => $agency->parent->title ?? '',
                    ],
                    "CONFIG"    => [
                        "allow_null"    => true,
                    ],
                    "FORMAT"        => "item.text.substr(0, 50) + '...'"
                ])

                @compo("$VIEW_ROOT.components.dropdowns.select2.config_key", [
                    "NAME"          => 'status',
                    "LABEL"         => __("Status"),
                    "PLACEHOLDER"   => __("Select Status ..."),
                    "OPTIONS"       => config('enums.comments_status'),
                    "VALUE"         => $comment->status,
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.rich_textarea", [
                    "NAME"              => 'content',
                    "VALUE"             => $comment->content,
                    "LABEL"             => __('Content'),
                    "CONTAINER_CLASS"   => 'col-12',
                    "ROWS"              => '10',
                    "MORE"              => 'cols="30"',
                ])
            </div>
        @endslot
    @endcomponent
@endsection
