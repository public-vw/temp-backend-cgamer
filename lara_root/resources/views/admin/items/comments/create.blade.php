@extends("$VIEW_ROOT.layouts.create_wide",[
    'MODEL' => 'Comment',
    'TABLE' => $VIEW_FOLDERS[1],
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
                @isset($morph)
                    @compo("$VIEW_ROOT.components.textfields.hidden",[
                        "NAME"  => 'commentable_type',
                        "VALUE" => $morph['type'],
                    ])

                    @compo("$VIEW_ROOT.components.textfields.hidden",[
                        "NAME"  => 'commentable_id',
                        "VALUE" => $morph['id'],
                    ])
                @else
                    <div class="row form-group">
                        @compo("$VIEW_ROOT.components.dropdowns.select2.single_array", [
                            "NAME"          => 'commentable_type',
                            "LABEL"         => __("Commentable Type"),
                            "PLACEHOLDER"   => __("Select Commentable Type..."),
                            "OPTIONS"       => config('morphs.commentable'),
                        ])

                        @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                            "NAME"                  => 'commentable_id',
                            "LABEL"                 => __("Commentable"),
                            "PLACEHOLDER"           => __("Select Commentable..."),
                            "MORPH_LIST"            => config('morphs.commentable'),
                            "AJAX_URL_DEPENDS_TO"   => '#commentable_type',
                        ])
                    </div>
                @endisset
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'parent_id',
                    "LABEL"         => __("Parent"),
                    'PLACEHOLDER'   => __("Select Parent"),
                    "AJAX_URL"      => route("{$VIEW_ROOT}.{$VIEW_FOLDERS[1]}.list.all"),
                    "CONFIG"    => [
                        "allow_null"    => true,
                    ],
                    "FORMAT"        => "item.text.substr(0, 50) + '...'"
                ])

                @compo("$VIEW_ROOT.components.dropdowns.select2.config_key", [
                    "NAME"          => 'status',
                    "LABEL"         => __("Status"),
                    "PLACEHOLDER"   => __("Select Status ..."),
                    "OPTIONS"       => config('enums.comments_status'),
                    "REQUIRED"      => true,
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.rich_textarea", [
                    "NAME"              => 'content',
                    "LABEL"             => __('Content'),
                    "CONTAINER_CLASS"   => 'col-12',
                    "ROWS"              => '10',
                    "MORE"              => 'cols="30"',
                ])
            </div>
        @endslot
    @endcomponent
@endsection
