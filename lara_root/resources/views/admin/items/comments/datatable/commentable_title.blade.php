@php
    $TABLE = model2table($variable->commentable_type);
    $commentable = $variable->commentable;
    if($TABLE){
        $route = Route::has("{$VIEW_ROOT}.{$TABLE}.show") ? route("{$VIEW_ROOT}.{$TABLE}.show", $commentable) : route("{$VIEW_ROOT}.{$TABLE}.edit", $commentable);
    }
@endphp

@compo("$VIEW_ROOT.components.clickables.link", [
    "URL"   => $route ?? '#',
    "TITLE" => $commentable->title ?? '',
])
