@compo("$VIEW_ROOT.components.clickables.link", [
    "URL"       => route("$VIEW_ROOT.{$VIEW_FOLDERS[1]}.edit", $variable),
    "TITLE"     => "<p>" . Str::substr($variable->content, 0, 20) . " ...</p>",
    "RAW_HTML"  => true,
])
