@php($user = $variable->user)

@if($user)
    @php($route = Route::has("{$VIEW_ROOT}.{$VIEW_FOLDERS[1]}.show") ? route("{$VIEW_ROOT}.{$VIEW_FOLDERS[1]}.show", $user) : route("{$VIEW_ROOT}.{$VIEW_FOLDERS[1]}.edit", $user))
@endif


@compo("$VIEW_ROOT.components.clickables.link", [
    "URL"   => $route ?? '#',
    "TITLE" => $user->username ?? '',
])
