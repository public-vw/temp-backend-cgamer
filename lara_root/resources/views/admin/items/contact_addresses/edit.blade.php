@extends("$VIEW_ROOT.layouts.edit_wide",[
    'MODEL'     => 'ContactAddress',
    'VARIABLE'  => $contactAddress,
    'TITLE'     => $contactAddress->title,
    'TABLE'     => $VIEW_FOLDERS[1],
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @php
            $city = $contactAddress->city;
            $province = $city->province;
            $country = $province->country;
        @endphp

        @slot('body')
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Title"),
                    "NAME"          => 'title',
                    "VALUE"         => $contactAddress->title,
                    "PLACEHOLDER"   => 'e.g. Agency branch 1',
                    "REQUIRED"      => true,
                ])

                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'country_id',
                    "LABEL"         => __("Country"),
                    "PLACEHOLDER"   => __("Select Country ..."),
                    "AJAX_URL"      => route("$VIEW_ROOT.countries.list.all"),
                    "VALUE"         => [
                        'id'    => $country->id ?? '',
                        'text'  => $country->title ?? '',
                    ],
                    'CONFIG'        => [
                        'allow_null' => true,
                    ],
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.dropdowns.select2.single_array", [
                    "NAME"          => 'addressable_type',
                    "LABEL"         => __("Addressable Type"),
                    "PLACEHOLDER"   => __("Select Addressable Type..."),
                    "OPTIONS"       => config('morphs.addressable'),
                    "VALUE"         => [
                        'id'    => $contactAddress->addressable_type ?? '',
                        'text'  => Str::ucfirst($contactAddress->addressable_type) ?? '',
                    ],
                ])

                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'province_id',
                    "LABEL"         => __("Province"),
                    "PLACEHOLDER"   => __("Select Province ..."),
                    "AJAX_URL"      => route("$VIEW_ROOT.provinces.list.all"),
                    "DEPENDS_TO"    => '#country_id',
                    "VALUE"         => [
                        'id'    => $province->id ?? '',
                        'text'  => $province->title ?? '',
                    ],
                    'CONFIG'        => [
                        'allow_null' => true,
                    ],
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"                  => 'addressable_id',
                    "LABEL"                 => __("Addressable"),
                    "PLACEHOLDER"           => __("Select Addressable..."),
                    "MORPH_LIST"            => config('morphs.addressable'),
                    "AJAX_URL_DEPENDS_TO"   => '#addressable_type',
                    "VALUE"                 => [
                        'id'    => $contactAddress->addressable_id ?? '',
                        'text'  => $contactAddress->addressable->title ?? '',
                    ],
                ])

                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'city_id',
                    "LABEL"         => __("City"),
                    "PLACEHOLDER"   => __("Select City ..."),
                    "AJAX_URL"      => route("$VIEW_ROOT.cities.list.all"),
                    "DEPENDS_TO"    => '#province_id',
                    "VALUE"         => [
                        'id'    => $city->id ?? '',
                        'text'  => $city->title ?? '',
                    ],
                    'CONFIG'        => [
                        'allow_null' => true,
                    ],
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Order"),
                    "NAME"          => 'order',
                    "TYPE"          => 'number',
                    "VALUE"         => $contactAddress->order,
                    "PLACEHOLDER"   => '',
                ])

                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'region_id',
                    "LABEL"         => __("Region"),
                    "PLACEHOLDER"   => __("Select Region ..."),
                    "AJAX_URL"      => route("$VIEW_ROOT.regions.list.all"),
                    "DEPENDS_TO"    => '#city_id',
                    "VALUE"         => [
                        'id'    => $contactAddress->region_id ?? '',
                        'text'  => $contactAddress->region->title ?? '',
                    ],
                    'CONFIG'        => [
                        'allow_null' => true,
                    ],
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.boxes.checkbox", [
                    "TITLE"    => __("Active"),
                    "NAME"     => 'active',
                    "CLASS"    => 'custom-control-input',
                    "SWITCH"   => true,
                    "CHECKED"  => $contactAddress->active,
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.boxes.checkbox", [
                    "TITLE"   => __("Default"),
                    "NAME"    => 'default',
                    "CLASS"   => 'custom-control-input',
                    "SWITCH"  => true,
                    "CHECKED" => $contactAddress->default,
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.textarea", [
                    "NAME"              => 'geocode',
                    "VALUE"             => $contactAddress->geocode,
                    "LABEL"             => __('Geocode'),
                    "CONTAINER_CLASS"   => 'col-12',
                    "ROWS"              => '5',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.rich_textarea", [
                    "NAME"              => 'description',
                    "VALUE"             => $contactAddress->description,
                    "LABEL"             => __('Description'),
                    "CONTAINER_CLASS"   => 'col-12',
                    "ROWS"              => '5',
                ])
            </div>
            @compo("$VIEW_ROOT.components.gmap", [
                "MAP_ID"     => 'contactAddress',
                "LABEL"     => 'contactAddress',
                "LATITUDE"  => 'latitude',
                "LONGITUDE" => 'longitude',
                "MAP_ZOOM"   => 'map_zoom',
                "VARIABLE"  => $contactAddress,
            ])
        @endslot
    @endcomponent
@endsection
