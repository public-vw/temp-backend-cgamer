@php
    $table = 'contact_addresses';
    $title = __('Contact Addresses');
    $headers = [
        'title'  => __('Title'),
        'region' => __('Region'),
        'active' => __('Active'),
    ];
    $items = $VARIABLE->contactAddresses ?? [];
    $morph = [
        'type'  => $VARIABLE->getTable(),
        'id'    => $VARIABLE->id,
    ];
@endphp

@compo("$VIEW_ROOT.components.inline_section")
