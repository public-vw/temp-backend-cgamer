@php
    $TABLE = model2table($variable->addressable_type);
    $addressable = $variable->addressable;
    if($TABLE){
        $route = Route::has("{$VIEW_ROOT}.{$TABLE}.show") ? route("{$VIEW_ROOT}.{$TABLE}.show", $addressable) : route("{$VIEW_ROOT}.{$TABLE}.edit", $addressable);
    }
@endphp

@compo("$VIEW_ROOT.components.clickables.link", [
    "URL"   => $route ?? '#',
    "TITLE" => $addressable->title ?? '',
])
