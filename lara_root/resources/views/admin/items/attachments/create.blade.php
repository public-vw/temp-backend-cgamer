@extends("$VIEW_ROOT.layouts.create_wide",[
    'MODEL'         => 'Attachment',
    'TABLE'         => $VIEW_FOLDERS[1],
    'HAS_UPLOAD'    => true,
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            @isset($morph)
                @compo("$VIEW_ROOT.components.textfields.hidden",[
                    "NAME"  => 'attachmentable_type',
                    "VALUE" => $morph['type'],
                ])

                @compo("$VIEW_ROOT.components.textfields.hidden",[
                    "NAME"  => 'attachmentable_id',
                    "VALUE" => $morph['id'],
                ])
            @else
                <div class="row form-group">
                    @compo("$VIEW_ROOT.components.dropdowns.select2.single_array", [
                        "NAME"          => 'attachmentable_type',
                        "LABEL"         => __("Attachmentable Type"),
                        "PLACEHOLDER"   => __("Select Attachmentable Type..."),
                        "OPTIONS"       => config('morphs.attachmentable'),
                    ])

                    @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                        "NAME"                  => 'attachmentable_id',
                        "LABEL"                 => __("Attachmentable"),
                        "PLACEHOLDER"           => __("Select Attachmentable..."),
                        "MORPH_LIST"            => config('morphs.attachmentable'),
                        "AJAX_URL_DEPENDS_TO"   => '#attachmentable_type',
                    ])
                </div>
            @endisset
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.files.attachment", [
                    "IMAGE"      => true,
                    "LABEL"      => 'File',
                    "NAME"       => 'file',
                    "NO_REMOVE"  => true,
                    "MORE_CLASS" => 'form-control-file',
                ])

                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'attachment_type_id',
                    "LABEL"         => __("Attachment Type"),
                    "PLACEHOLDER"   => __("Select Attachment Type ..."),
                    "AJAX_URL"      => route("$VIEW_ROOT.attachment_types.list.all"),
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.number", [
                    "LABEL"      => 'Quality Rank',
                    "NAME"       => 'quality_rank',
                ])

                @compo("$VIEW_ROOT.components.textfields.textarea", [
                    "NAME"              => 'properties',
                    "LABEL"             => __('Properties'),
                    "CONTAINER_CLASS"   => 'col-6',
                    "ROWS"              => '10',
                    "MORE"              => 'cols="30"',
                ])
            </div>
        {{-- TODO create quality rank process --}}
        {{-- TODO create self relation (thumbnail) --}}
        @endslot
    @endcomponent
@endsection
