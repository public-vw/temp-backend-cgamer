@php
    $table = 'attachments';
    $title = __('Attachments');
    $headers = [
        'image'  => __('Image'),
        'type' => __('Type'),
        'user' => __('Setter'),
        'path'  => __('File Path'),
    ];
    $items = $VARIABLE->attachments ?? [];
    $morph = [
        'id'    => $VARIABLE->id,
        'type'  => $VARIABLE->getTable(),
    ];
@endphp

@compo("$VIEW_ROOT.components.inline_section")
