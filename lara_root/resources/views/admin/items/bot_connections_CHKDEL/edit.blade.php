@extends("$VIEW_ROOT.layouts.edit_wide",[
    'MODEL'     => 'Bot Connection',
    'VARIABLE'  => $bot_connection,
    'TITLE'     => $bot_connection->title,
    'TABLE'     => $VIEW_FOLDERS[1],
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            <div class="row form-group">
                ## Form Goes Here ##
            </div>
        @endslot
    @endcomponent
@endsection
