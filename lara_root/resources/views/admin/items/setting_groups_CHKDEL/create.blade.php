@extends("$VIEW_ROOT.layouts.create_wide",[
    'MODEL' => 'SettingGroup',
    'TABLE' => $VIEW_FOLDERS[1],
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Title"),
                    "NAME"          => 'title',
                    "PLACEHOLDER"   => 'e.g. Users Settings',
                    "REQUIRED"      => true,
                ])

                {{-- TODO create sulg mutators--}}
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Slug"),
                    "NAME"          => 'slug',
                    "PLACEHOLDER"   => 'e.g. users_setting',
                    "HELPER"        => 'slug should be unique, if you don\'t set slug it will generate automatically.',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Order"),
                    "NAME"          => 'order',
                    "TYPE"          => 'number',
                    "PLACEHOLDER"   => '',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.boxes.checkbox", [
                    "TITLE"   => __("Hidden"),
                    "NAME"    => 'hidden',
                    "CLASS"   => 'custom-control-input',
                    "SWITCH"  => true,
                ])
            </div>
        @endslot
    @endcomponent
@endsection
