@extends("$VIEW_ROOT.layouts.edit_wide",[
    'MODEL'     => 'SettingGroup',
    'VARIABLE'  => $settingGroup,
    'TITLE'     => $settingGroup->title,
    'TABLE'     => $VIEW_FOLDERS[1],
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Title"),
                    "NAME"          => 'title',
                    "VALUE"         => $settingGroup->title,
                    "PLACEHOLDER"   => 'e.g. Users Settings',
                    "REQUIRED"      => true,
                ])

                {{-- TODO create sulg mutators--}}
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Slug"),
                    "NAME"          => 'slug',
                    "VALUE"         => $settingGroup->slug,
                    "PLACEHOLDER"   => 'e.g. users_setting',
                    "HELPER"        => 'slug should be unique, if you don\'t set slug it will generate automatically.',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Order"),
                    "NAME"          => 'order',
                    "VALUE"         => $settingGroup->order,
                    "TYPE"          => 'number',
                    "PLACEHOLDER"   => '',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.boxes.checkbox", [
                    "TITLE"   => __("Hidden"),
                    "NAME"    => 'hidden',
                    "CLASS"   => 'custom-control-input',
                    "SWITCH"  => true,
                    "CHECKED" => $settingGroup->hidden,
                ])
            </div>
        @endslot
    @endcomponent
@endsection
