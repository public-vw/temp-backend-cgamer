@extends("$VIEW_ROOT.layouts.edit_wide",[
    'MODEL'      => 'SeoDetail',
    'VARIABLE'   => $seoDetail,
    'TITLE'      => $seoDetail->title,
    'TABLE'      => $VIEW_FOLDERS[1],
    'HAS_UPLOAD' => true,
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Title"),
                    "NAME"          => 'title',
                    "VALUE"         => $seoDetail->title,
                    "PLACEHOLDER"   => 'e.g. Agency branch 1 instagram',
                    "REQUIRED"      => true,
                ])

                @compo("$VIEW_ROOT.components.dropdowns.select2.config_key", [
                    "NAME"          => 'type',
                    "LABEL"         => __("Type"),
                    "PLACEHOLDER"   => __("Select Type ..."),
                    "OPTIONS"       => config('enums.seo_details_type'),
                    "VALUE"         => $seoDetail->type,
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.dropdowns.select2.single_array", [
                    "NAME"          => 'seoable_type',
                    "LABEL"         => __("Seoable Type"),
                    "PLACEHOLDER"   => __("Select Seoable Type..."),
                    "OPTIONS"       => config('morphs.seoable'),
                    "VALUE"         => [
                        'id'    => $seoDetail->seoable_type ?? '',
                        'text'  => Str::ucfirst($seoDetail->seoable_type) ?? '',
                    ],
                ])

                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'seoable_id',
                    "LABEL"         => __("Seoable"),
                    "PLACEHOLDER"   => __("Select Seoable..."),
                    "MORPH_LIST"    => config('morphs.seoable'),
                    "VALUE"         => [
                        'id'    => $seoDetail->seoable_id ?? '',
                        'text'  => $seoDetail->seoable->title ?? $seoDetail->seoable->slug ?? $seoDetail->seoable->uri ?? '',
                    ],
                    "AJAX_URL_DEPENDS_TO"   => '#seoable_type',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Keywords"),
                    "NAME"          => 'keywords',
                    "VALUE"         => $seoDetail->keywords,
                    "PLACEHOLDER"   => '',
                ])

                {{-- TODO: check if needed to disable attachment_type field --}}
                {{-- TODO: show selected files and their types --}}
{{--                @php($thumbnail = $seoDetail->thumbnail)--}}
{{--                @php($attachmentType = $thumbnail->type)--}}
                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'attachment_type',
                    "LABEL"         => __("Attachment Type"),
                    "PLACEHOLDER"   => __("Select Attachment Type ..."),
                    "AJAX_URL"      => route("$VIEW_ROOT.attachment_types.list.all"),
                    "VALUE"         => [
{{--                        'id'    => $attachmentType->id ?? '',--}}
{{--                        'text'  => $attachmentType->title ?? '',--}}
                    ],
                    'CONFIG'        => [
                        'allow_null' => true,
                    ],
                ])
            </div>
            <div class="row form-group">
                <div class="col-md-6 col-sm-12"></div>

                {{-- TODO check and show file --}}
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"      => 'File',
                    "NAME"       => 'file',
                    "TYPE"       => 'file',
{{--                    "VALUE"      => asset('storage/'. $thumbnail->url),--}}
                    "MORE_CLASS" => 'form-control-file',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.textarea", [
                    "NAME"              => 'description',
                    "VALUE"             => $seoDetail->description,
                    "LABEL"             => __('Description'),
                    "CONTAINER_CLASS"   => 'col-12',
                    "ROWS"              => '10',
                ])
            </div>
        {{-- TODO improve upload (thumbnail_id) --}}
        @endslot
    @endcomponent
@endsection
