@extends("$VIEW_ROOT.layouts.create_wide", [
    'MODEL'      => 'SeoDetail',
    'TABLE'      => $VIEW_FOLDERS[1],
    'HAS_UPLOAD' => true,
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Title"),
                    "NAME"          => 'title',
                    "PLACEHOLDER"   => '',
                    "REQUIRED"      => true,
                ])

                @compo("$VIEW_ROOT.components.dropdowns.select2.config_key", [
                    "NAME"          => 'type',
                    "LABEL"         => __("Type"),
                    "PLACEHOLDER"   => __("Select Type ..."),
                    "OPTIONS"       => config('enums.seo_details_type'),
                ])
            </div>
            @isset($morph)
                @compo("$VIEW_ROOT.components.textfields.hidden",[
                    "NAME"  => 'seoable_type',
                    "VALUE" => $morph['type'],
                ])

                @compo("$VIEW_ROOT.components.textfields.hidden",[
                    "NAME"  => 'seoable_id',
                    "VALUE" => $morph['id'],
                ])
            @else
                <div class="row form-group">
                    @compo("$VIEW_ROOT.components.dropdowns.select2.single_array", [
                        "NAME"          => 'seoable_type',
                        "LABEL"         => __("Seoable Type"),
                        "PLACEHOLDER"   => __("Select Seoable Type..."),
                        "OPTIONS"       => config('morphs.seoable'),
                    ])

                    @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                        "NAME"                  => 'seoable_id',
                        "LABEL"                 => __("Seoable"),
                        "PLACEHOLDER"           => __("Select Seoable..."),
                        "MORPH_LIST"            => config('morphs.seoable'),
                        "AJAX_URL_DEPENDS_TO"   => '#seoable_type',
                    ])
                </div>
            @endif
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Keywords"),
                    "NAME"          => 'keywords',
                    "PLACEHOLDER"   => '',
                ])

                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'attachment_type',
                    "LABEL"         => __("Attachment Type"),
                    "PLACEHOLDER"   => __("Select Attachment Type ..."),
                    "AJAX_URL"      => route("$VIEW_ROOT.attachment_types.list.all"),
                    'CONFIG'        => [
                        'allow_null' => true,
                    ],
                ])
            </div>
            <div class="row form-group">
                <div class="col-md-6 col-sm-12"></div>

                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"      => 'File',
                    "NAME"       => 'file',
                    "TYPE"       => 'file',
                    "MORE_CLASS" => 'form-control-file',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.textarea", [
                    "NAME"              => 'description',
                    "LABEL"             => __('Description'),
                    "CONTAINER_CLASS"   => 'col-12',
                    "ROWS"              => '10',
                ])
            </div>
        {{-- TODO improve upload (thumbnail_id) --}}
        @endslot
    @endcomponent
@endsection
