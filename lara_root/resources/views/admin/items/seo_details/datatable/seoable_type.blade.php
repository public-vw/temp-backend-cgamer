@php($TABLE = model2table($variable->seoable_type))

@if($TABLE)
    @php($route = Route::has("{$VIEW_ROOT}.{$TABLE}.index") ? route("{$VIEW_ROOT}.{$TABLE}.index") : '#')
@endif

@compo("$VIEW_ROOT.components.clickables.link", [
    "URL"   => $route ?? '',
    "TITLE" => Str::ucfirst(Str::camel($variable->seoable_type)),
])
