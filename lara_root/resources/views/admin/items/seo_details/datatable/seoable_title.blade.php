@php
    $TABLE = model2table($variable->seoable_type);
    $seoable = $variable->seoable;
    if($TABLE){
        $route = Route::has("{$VIEW_ROOT}.{$TABLE}.show") ? route("{$VIEW_ROOT}.{$TABLE}.show", $seoable) : route("{$VIEW_ROOT}.{$TABLE}.edit", $seoable);
    }
@endphp


@compo("$VIEW_ROOT.components.clickables.link", [
    "URL"   => $route ?? '#',
    "TITLE" => $seoable->title ?? $seoable->slug ?? $seoable->uri ?? $seoable->id ?? '',
])
