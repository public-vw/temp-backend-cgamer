@php
    $table = 'seo_details';
    $title = __('Seo Details');
    $headers = [
        'type'  => __('Type'),
        'title' => __('Title'),
        'keywords' => __('Keywords'),
        'description' => __('Description'),
    ];
    $items = $VARIABLE->seoDetails ?? [];
    $morph = [
        'type'  => $VARIABLE->getTable(),
        'id'    => $VARIABLE->id,
    ];
@endphp

@compo("$VIEW_ROOT.components.inline_section")
