@extends("$VIEW_ROOT.layouts.edit_wide",[
    'MODEL'     => 'Model Attachment Type',
    'VARIABLE'  => $modelAttachmentType,
    'TITLE'     => $modelAttachmentType->title,
    'TABLE'     => $VIEW_FOLDERS[1],
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Model Name"),
                    "NAME"          => 'model_name',
                    "VALUE"         => $modelAttachmentType->model_name,
                    "PLACEHOLDER"   => 'e.g. Agency',
                    "REQUIRED"      => true,
                ])

                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'attachment_type_id',
                    "LABEL"         => __("Attachment Type"),
                    "PLACEHOLDER"   => __("Select Attachment Type ..."),
                    "AJAX_URL"      => route("$VIEW_ROOT.attachment_types.list.all"),
                    "VALUE"         => [
                        'id'    => $modelAttachmentType->attachment_type_id ?? '',
                        'text'  => $modelAttachmentType->attachmentType->title ?? '',
                    ],
                    "REQUIRED"      => true,
                ])
            </div>
        @endslot
    @endcomponent
@endsection
