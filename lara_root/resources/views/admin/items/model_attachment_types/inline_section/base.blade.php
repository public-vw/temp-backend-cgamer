@php
    $table = 'model_attachment_types';
    $title = __('Model Attachment Type');
    $headers = [
        'model_name'  => __('Model'),
    ];
    $items = $VARIABLE->modelAttachmentTypes ?? [];
    $morph = [
        'type'  => $VARIABLE->getTable(),
        'id'    => $VARIABLE->id,
    ];
@endphp

@compo("$VIEW_ROOT.components.inline_section")
