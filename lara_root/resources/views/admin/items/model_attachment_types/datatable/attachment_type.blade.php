@php($attachmentType = $variable->attachmentType)

@if($attachmentType)
    {{-- TODO: fix route --}}
    @php($route = Route::has("$VIEW_ROOT.attachment_types.show1") ? route("$VIEW_ROOT.attachment_types.show", $attachmentType) : route("$VIEW_ROOT.attachment_types.edit", $attachmentType))
@endif


@compo("$VIEW_ROOT.components.clickables.link", [
    "URL"   => $route ?? '#',
    "TITLE" => $attachmentType->title ?? '',
])
