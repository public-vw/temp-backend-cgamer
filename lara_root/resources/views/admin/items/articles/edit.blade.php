@extends("$VIEW_ROOT.layouts.edit_wide",[
    'MODEL'     => 'Article',
    'VARIABLE'  => $article,
    'TITLE'     => $article->heading,
    'TABLE'     => $VIEW_FOLDERS[1],
])

@section('information')
    Word count: {{ count_words($article->content) }}
@endsection

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            <div class="row form-group">

                @compo("$VIEW_ROOT.components.dropdowns.mdb_nested_dropdown_ajax", [
                    "NAME"          => 'category_id',
                    "LABEL"         => __("Category"),
                    "AJAX_URL"      => route("{$VIEW_ROOT}.article_categories.list.json"),
                    "VALUE"       => [
                        'id'    => $article->category_id ?? '',
                        'title' => $article->category ? $article->category->categoryChain('title') : [],
                    ],
                    "CONFIG"    => [
                        "allow_null"    => true,
                    ],
                ])

                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'author_id',
                    "LABEL"         => __("Author"),
                    'PLACEHOLDER'   => __("Select Author"),
                    "AJAX_URL"      => route("{$VIEW_ROOT}.authors.list.all"),
                    "VALUE"       => [
                        'id'    => $article->author_id ?? '',
                        'text'  => $article->author->user->nickname ?? '',
                    ],
                    "CONFIG"    => [
                        "allow_null"    => true,
                    ],
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "NAME"       => 'heading',
                    "LABEL"      => 'Heading (H1)',
                    "VALUE"      => $article->heading,
                    'REQUIRED'      => true,
                ])

                @compo("$VIEW_ROOT.components.textfields.text", [
                    "NAME"       => 'slug',
                    "LABEL"      => 'Slug',
                    "VALUE"      => $article->slug,
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.number", [
                    "NAME"       => 'reading_time',
                    "LABEL"      => 'Reading Time',
                    "VALUE"      => $article->reading_time,
                ])

                @cannot('articles.change_status')
                    <div class="col-md-6 col-sm-12">
                        <b>Status :</b> {{ config("enums.articles_status.".$article->status) }}
                    </div>
                @endcannot
            </div>
            <div class="row form-group">
                @can('articles.change_status')
                @compo("$VIEW_ROOT.components.dropdowns.select2.config_key", [
                    "NAME"          => 'status',
                    "LABEL"         => __("Status"),
                    "PLACEHOLDER"   => __("Select Status ..."),
                    "OPTIONS"       => config('enums.articles_status'),
                    'REQUIRED'      => true,
                    "VALUE"         => $article->status,
                    "HELPER"        => '<b>Note that:</b> by setting <u>active</u> here, you should send <b>googleping</b> <u>manually</u>',
                ])
                @endcan
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.rich_textarea", [
                    "LABEL"      => 'Summary',
                    "NAME"       => 'summary',
                    "VALUE"      => $article->summary,
                    "CONTAINER_CLASS"   => 'col-12',
                    "ROWS"              => '7',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.textarea", [
                    "LABEL"      => 'Content',
                    "NAME"       => 'content',
                    "VALUE"      => $article->content,
                    "CONTAINER_CLASS"   => 'col-12',
                    "ROWS"              => '10',
                ])
            </div>
        @endslot
    @endcomponent
@endsection

@section('outside')
        @includeRelativeSection("$VIEW_ROOT.attachments",[
            "VARIABLE"  => $article,
        ])

    @includeRelativeSection("$VIEW_ROOT.seo_details",[
        "VARIABLE"  => $article,
    ])
@endsection
