@extends("$VIEW_ROOT.layouts.create_wide",[
    'MODEL' => 'Article',
    'TABLE' => $VIEW_FOLDERS[1],
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.dropdowns.mdb_nested_dropdown_ajax", [
                    "NAME"          => 'category_id',
                    "LABEL"         => __("Category"),
                    "AJAX_URL"      => route("{$VIEW_ROOT}.article_categories.list.json"),
                    "CONFIG"    => [
                        "allow_null"    => true,
                    ],
                ])

                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'author_id',
                    "LABEL"         => __("Author"),
                    'PLACEHOLDER'   => __("Select Author"),
                    "AJAX_URL"      => route("{$VIEW_ROOT}.authors.list.all"),
                    "CONFIG"    => [
                        "allow_null"    => true,
                    ],
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "NAME"       => 'heading',
                    "LABEL"      => 'Heading (H1)',
                    'REQUIRED'      => true,
                ])
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "NAME"       => 'slug',
                    "LABEL"      => 'Slug',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.number", [
                    "NAME"       => 'reading_time',
                    "LABEL"      => 'Reading Time',
                ])

            </div>
            @can('articles.change_status')
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.dropdowns.select2.config_key", [
                    "NAME"          => 'status',
                    "LABEL"         => __("Status"),
                    "PLACEHOLDER"   => __("Select Status ..."),
                    "OPTIONS"       => config('enums.articles_status'),
                    'REQUIRED'      => true,
                ])
            </div>
            @endcan
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.rich_textarea", [
                    "LABEL"      => 'Summary',
                    "NAME"       => 'summary',
                    "CONTAINER_CLASS"   => 'col-12',
                    "ROWS"              => '7',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.textarea", [
                    "LABEL"      => 'Content',
                    "NAME"       => 'content',
                    "CONTAINER_CLASS"   => 'col-12',
                    "ROWS"              => '10',
                ])
            </div>
        @endslot
    @endcomponent
@endsection
