@php
    $table = 'articles';
    $title = __('Articles');
    $headers = [
        'heading'  => __('Heading'),
        'slug'  => __('Slug'),
    ];
    $items = $VARIABLE->articles ?? [];
    $morph = [
        'id'    => $VARIABLE->id,
        'type'  => $VARIABLE->getTable(),
    ];
@endphp

@compo("$VIEW_ROOT.components.inline_section")
