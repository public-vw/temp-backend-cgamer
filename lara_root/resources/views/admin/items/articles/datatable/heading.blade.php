@compo("$VIEW_ROOT.components.clickables.link", [
    "URL"   => route("{$VIEW_ROOT}.{$VIEW_FOLDERS[1]}.edit", $variable),
    "TITLE" => Str::limit($variable->heading, 20)."({$variable->id})",
])
