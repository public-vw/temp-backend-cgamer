@if($variable->status == config_key('enums.articles_status','active'))
{{--    View Article Page | After Activation --}}
    @compo("$VIEW_ROOT.components.clickables.link", [
        "URL"   => $variable->seoUrl(),
        "TITLE"    => "<i class='fa fa-eye'></i>",
        "CLASS"    => 'btn btn-sm btn-success',
        "RAW_HTML" => true,
        "TARGET" => '_blank',
    ])
@else
{{--    Preview Article Page | Before Activation --}}
    @compo("$VIEW_ROOT.components.clickables.link", [
        "URL"   => route("client.articles.single.preview", $variable->id),
        "TITLE"    => "<i class='fa fa-eye'></i>",
        "CLASS"    => 'btn btn-sm btn-info text-white',
        "RAW_HTML" => true,
        "TARGET" => '_blank',
    ])
@endif

@can('articles.change_status')
    @if(
        $variable->status == config_key('enums.articles_status','active')
        && $variable->hasRevision()
        && $variable->revision->status != config_key('enums.articles_status','draft')
        )
{{--    Activate Revision | Master Artice, with Requested review --}}
        @compo("$VIEW_ROOT.components.clickables.link", [
            "URL"   => route("admin.articles.accept_revision", $variable->revision),
            "TITLE"    => "<i class='fa fa-check'></i> {$variable->revision->id}",
            "CLASS"    => 'btn btn-sm btn-danger',
            "RAW_HTML" => true,
        ])
    @endif

    @if(
        $variable->isRevision()
        && $variable->status != config_key('enums.articles_status','draft')
        && $variable->master->status == config_key('enums.articles_status','active')
        )
{{--    Activate Revision | Revision Artice, with Requested Status, with Active Master --}}
        @compo("$VIEW_ROOT.components.clickables.link", [
            "URL"   => route("admin.articles.accept_revision", $variable),
            "TITLE"    => "{$variable->master->id} <i class='fa fa-check'></i>",
            "CLASS"    => 'btn btn-sm btn-danger',
            "RAW_HTML" => true,
        ])
    @endif
@endcan

@canany(makeAnyPermissionString('articles.edit'))
{{--    Edit in Interface --}}
@compo("$VIEW_ROOT.components.clickables.link", [
    "URL"   => route("client.articles.single.edit", $variable->id),
    "TITLE"    => "<i class='fa fa-pencil'></i>",
    "CLASS"    => 'btn btn-sm btn-primary',
    "RAW_HTML" => true,
    "TARGET" => '_blank',
])
@endcanany

@can('articles.ping_google')
{{--    Send Google Ping --}}
    @if($variable->status == config_key('enums.articles_status','active'))
        @compo("$VIEW_ROOT.components.clickables.link", [
            "URL"   => route("admin.articles.ping_google", $variable),
            "TITLE"    => "<i class='fa fa-google'></i>",
            "CLASS"    => 'btn btn-sm btn-warning',
            "RAW_HTML" => true,
        ])
    @endif
@endcan

{{--    Delete --}}
@canany(makeAnyPermissionArray('articles.delete'))
    @if(
        (
            $variable->author->user->isChildOfCurrent() &&
            $user->hasPermissionTo('articles.delete.branch') &&
            in_array($variable->status, config_keys('enums.articles_status',['draft','rejected']))
        ) || (
            $variable->author->user->isSameAsCurrent() &&
            $user->hasPermissionTo('articles.delete.own') &&
            in_array($variable->status, config_keys('enums.articles_status',['draft','requested']))
        ) || (
            $user->hasRole('sysadmin@web')
        )
    )
        @compo("$VIEW_ROOT.components.actions.delete", [
            "VARIABLE"  => $variable,
            "TABLE"     => $VIEW_FOLDERS[1],
        ])
    @endif
@endcanany
