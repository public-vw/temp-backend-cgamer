@compo("$VIEW_ROOT.components.clickables.link", [
    "URL"   => route("{$VIEW_ROOT}.authors.edit", $variable->author->id),
    "TITLE" => $variable->author->user->nickname,
])
