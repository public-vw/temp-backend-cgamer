{{ config("enums.articles_status.{$variable->status}") }}
@if($variable->hasRevision())
 | reved@ {{ $variable->revision->id }}
@endif
@if($variable->isRevision())
 | revof {{ $variable->master->id }}
@endif
