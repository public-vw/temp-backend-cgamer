@if($variable->category_id)
    @foreach($variable->category->categoryChain(['id','title']) as $category)
        @compo("$VIEW_ROOT.components.clickables.link", [
            "URL"   => route("{$VIEW_ROOT}.article_categories.edit", $category['id']),
            "TITLE" => Str::limit($category['title'],30),
            "TARGET" => 'style="display: inline-block;"',
        ])
    @endforeach
@else
    ----
@endif
