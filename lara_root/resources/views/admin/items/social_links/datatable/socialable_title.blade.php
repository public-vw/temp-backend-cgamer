@php
    $TABLE = model2table($variable->socialable_type);
    $socialable = $variable->socialable;
    if($TABLE){
        $route = Route::has("{$VIEW_ROOT}.{$TABLE}.show") ? route("{$VIEW_ROOT}.{$TABLE}.show", $socialable) : route("{$VIEW_ROOT}.{$TABLE}.edit", $socialable);
    }
@endphp

@compo("$VIEW_ROOT.components.clickables.link", [
    "URL"   => $route ?? '#',
    "TITLE" => $socialable->title ?? '',
])
