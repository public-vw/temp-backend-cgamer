@extends("$VIEW_ROOT.layouts.create_wide",[
    'MODEL' => 'SocialLink',
    'TABLE' => $VIEW_FOLDERS[1],
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Title"),
                    "NAME"          => 'title',
                    "PLACEHOLDER"   => 'e.g. Agency branch 1 instagram',
                    "REQUIRED"      => true,
                ])

                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'channel_id',
                    "LABEL"         => __("Social Channel"),
                    "PLACEHOLDER"   => __("Select Social Channel..."),
                    "AJAX_URL"      => route("$VIEW_ROOT.social_channels.list.all"),
                ])
            </div>
            @isset($morph)
                @compo("$VIEW_ROOT.components.textfields.hidden",[
                    "NAME"  => 'socialable_type',
                    "VALUE" => $morph['type'],
                ])

                @compo("$VIEW_ROOT.components.textfields.hidden",[
                    "NAME"  => 'socialable_id',
                    "VALUE" => $morph['id'],
                ])

            @else
                <div class="row form-group">
                    @compo("$VIEW_ROOT.components.dropdowns.select2.single_array", [
                        "NAME"          => 'socialable_type',
                        "LABEL"         => __("Socialable Type"),
                        "PLACEHOLDER"   => __("Select Socialable Type..."),
                        "OPTIONS"       => config('morphs.socialable'),
                    ])

                    @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                        "NAME"                  => 'socialable_id',
                        "LABEL"                 => __("Socialable"),
                        "PLACEHOLDER"           => __("Select Socialable..."),
                        "MORPH_LIST"            => config('morphs.socialable'),
                        "AJAX_URL_DEPENDS_TO"   => '#socialable_type',
                    ])
                </div>
            @endisset
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("UserName"),
                    "NAME"          => 'connection_string',
                    "PLACEHOLDER"   => '',
                ])

                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Order"),
                    "NAME"          => 'order',
                    "TYPE"          => 'number',
                    "PLACEHOLDER"   => '',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.boxes.checkbox", [
                    "TITLE"    => __("Active"),
                    "NAME"     => 'active',
                    "CLASS"    => 'custom-control-input',
                    "SWITCH"   => true,
                    "CHECKED"  => true,
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.boxes.checkbox", [
                    "TITLE"   => __("Default"),
                    "NAME"    => 'default',
                    "CLASS"   => 'custom-control-input',
                    "SWITCH"  => true,
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.rich_textarea", [
                    "NAME"              => 'description',
                    "LABEL"             => __('Description'),
                    "CONTAINER_CLASS"   => 'col-12',
                    "ROWS"              => '10',
                    "MORE"              => 'cols="30"',
                ])
            </div>
        {{-- TODO create Social Link verification --}}
        @endslot
    @endcomponent
@endsection
