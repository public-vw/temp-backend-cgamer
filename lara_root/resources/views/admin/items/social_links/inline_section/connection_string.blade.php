{{-- TODO add icon or attachment of social channel --}}
{{-- TODO fix http --}}
@php($socialChannel = $variable->socialChannel)
@compo("$VIEW_ROOT.components.clickables.link", [
    "URL"   => rtrim($socialChannel->url, '/') . '/' . ltrim($variable->connection_string, '/'),
    "TITLE" => "$socialChannel->title",
])
