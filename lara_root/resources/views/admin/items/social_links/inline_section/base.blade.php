@php
    $table = 'social_links';
    $title = __('Social Links');
    $headers = [
        'title'             => __('Title'),
        'socialChannel'     => __('Channel'),
        'connection_string' => __('Link'),
        'active'            => __('Active'),
    ];
    $items = $VARIABLE->socialLinks ?? [];
    $morph = [
        'type'  => $VARIABLE->getTable(),
        'id'    => $VARIABLE->id,
    ];
@endphp

@compo("$VIEW_ROOT.components.inline_section")
