@extends("$VIEW_ROOT.layouts.edit_wide",[
    'MODEL'     => 'Country',
    'VARIABLE'  => $country,
    'TITLE'     => $country->title,
    'TABLE'     => $VIEW_FOLDERS[1],
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text_lang", [
                    "LABEL"         => __("Title"),
                    "NAME"          => 'title',
                    "PLACEHOLDER"   => __("Country Name"),
                    "VALUE"         => $country,
                    "REQUIRED"      => true,
                ])

                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Phone PreCode"),
                    "NAME"          => 'phone_precode',
                    "PLACEHOLDER"   => __("Phone PreCode"),
                    "VALUE"         => "$country->phone_precode",
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text_flag", [
                    "LABEL"         => __("Flag Iso"),
                    "NAME"          => 'flag_iso',
                    "placeholder"   => __("Flag Iso"),
                    "value"         => "$country->flag_iso",
                ])

                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("TimeZone"),
                    "NAME"          => 'timezone',
                    "PLACEHOLDER"   => __("TimeZone"),
                    "VALUE"         => "$country->timezone",
                    "HELPER"        => 'TimeZone must be in numeric form starts with (+/-)<br/>e.g. +00:00',
                ])
            </div>
        @endslot
    @endcomponent
@endsection
