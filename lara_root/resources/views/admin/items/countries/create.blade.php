@extends("$VIEW_ROOT.layouts.create_wide",[
    'MODEL' => 'Country',
    'TABLE' => $VIEW_FOLDERS[1],
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text_lang", [
                    "LABEL"         => __("Title"),
                    "NAME"          => 'title',
                    "PLACEHOLDER"   => 'e.g. Turkey',
                    "required"      => true,
                ])

                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Phone PreCode"),
                    "NAME"          => 'phone_precode',
                    "PLACEHOLDER"   => 'e.g. 44',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text_flag", [
                    "LABEL"         => __("Flag Iso"),
                    "NAME"          => 'flag_iso',
                    "placeholder"   => 'e.g. GB',
                ])

                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("TimeZone"),
                    "NAME"          => 'timezone',
                    "PLACEHOLDER"   => 'e.g. +01:00',
                    "HELPER"        => 'TimeZone must be in numeric form starts with (+/-)<br/>e.g. +00:00',
                ])
            </div>
        @endslot
    @endcomponent
@endsection
