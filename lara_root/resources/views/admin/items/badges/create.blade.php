@extends("$VIEW_ROOT.layouts.create_wide",[
    'MODEL' => 'Badge',
    'TABLE' => $VIEW_FOLDERS[1],
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            <div class="row form-group">
                ## Form Goes Here ##
            </div>
        @endslot
    @endcomponent
@endsection
