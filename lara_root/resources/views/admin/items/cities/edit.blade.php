@extends("$VIEW_ROOT.layouts.edit_wide",[
    'MODEL'     => 'City',
    'VARIABLE'  => $city,
    'TITLE'     => $city->title,
    'TABLE'     => $VIEW_FOLDERS[1],
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            @php($province = $city->province)
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Title"),
                    "NAME"          => 'title',
                    "PLACEHOLDER"   => __("City Name"),
                    "VALUE"         => "$city->title",
                    "REQUIRED"      => true,
                ])

                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'country_id',
                    "LABEL"         => __("Country"),
                    "PLACEHOLDER"   => __("Select Country ..."),
                    "AJAX_URL"      => route("$VIEW_ROOT.countries.list.all"),
                    "VALUE"         => [
                        'id'    => $province->country_id ?? '',
                        'text'  => $province->country->title ?? '',
                    ],
                    'CONFIG'        => [
                        'allow_null' => true,
                    ],
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Phone PreCode"),
                    "NAME"          => 'phone_precode',
                    "PLACEHOLDER"   => '44',
                    "VALUE"         => "$city->phone_precode",
                ])

                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'province_id',
                    "LABEL"         => __("Province"),
                    "PLACEHOLDER"   => __("Select Province ..."),
                    "AJAX_URL"      => route("$VIEW_ROOT.provinces.list.all"),
                    "DEPENDS_TO"    => '#country_id',
                    "VALUE"         => [
                        'id'    => $city->province_id ?? '',
                        'text'  => $province->title ?? '',
                    ],
                    'CONFIG'        => [
                        'allow_null' => true,
                    ],
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("TimeZone"),
                    "NAME"          => 'timezone',
                    "PLACEHOLDER"   => 'e.g. +01:00',
                    "VALUE"         => "$city->timezone",
                    "HELPER"        => 'TimeZone must be in numeric form starts with (+/-)<br/>e.g. +00:00',
                ])
            </div>
        @endslot
    @endcomponent
@endsection
