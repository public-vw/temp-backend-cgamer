@extends("$VIEW_ROOT.layouts.create_wide",[
    'MODEL' => 'City',
    'TABLE' => $VIEW_FOLDERS[1],
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Title"),
                    "NAME"          => 'title',
                    "PLACEHOLDER"   => 'e.g. Istanbul',
                    "REQUIRED"      => true,
                ])

                {{-- TODO ADD FLAGS --}}
                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'country_id',
                    "LABEL"         => __("Country"),
                    "PLACEHOLDER"   => __("Select Country ..."),
                    "AJAX_URL"      => route("$VIEW_ROOT.countries.list.all"),
                    'CONFIG'        => [
                        'allow_null' => true,
                    ],
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Phone PreCode"),
                    "NAME"          => 'phone_precode',
                    "PLACEHOLDER"   => 'e.g. 44',
                ])

                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'province_id',
                    "LABEL"         => __("Province"),
                    "PLACEHOLDER"   => __("Select Province ..."),
                    "AJAX_URL"      => route("$VIEW_ROOT.provinces.list.all"),
                    "DEPENDS_TO"    => '#country_id',
                    'CONFIG'        => [
                        'allow_null' => true,
                    ],
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("TimeZone"),
                    "NAME"          => 'timezone',
                    "PLACEHOLDER"   => 'e.g. +01:00',
                    "HELPER"        => 'TimeZone must be in numeric form starts with (+/-)<br/>e.g. +00:00',
                ])
            </div>
        @endslot
    @endcomponent
@endsection
