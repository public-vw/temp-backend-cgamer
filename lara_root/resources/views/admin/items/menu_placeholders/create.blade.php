@extends("$VIEW_ROOT.layouts.create_wide",[
    'MODEL' => 'Menu Placeholder',
    'TABLE' => $VIEW_FOLDERS[1],
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'panel',
                    "LABEL"         => __("panel"),
                    "PLACEHOLDER"   => __("Select panel ..."),
                    "REQUIRED"      => true,
                    'CONFIG'        => [
                        'allow_null' => true,
                    ],
                    "AJAX_URL"      => route("$VIEW_ROOT.panels.list.all"),
                ])

                @compo("$VIEW_ROOT.components.textfields.text",[
                    "NAME"      => 'title',
                    "LABEL"     => __('Title'),
                    "REQUIRED"  => true,
                ])
            </div>
        @endslot
    @endcomponent
@endsection
