@extends("$VIEW_ROOT.layouts.edit_wide",[
    'MODEL'     => 'Menu Placeholder',
    'VARIABLE'  => $menuPlaceholder,
    'TITLE'     => $menuPlaceholder->title,
    'TABLE'     => $VIEW_FOLDERS[1],
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'panel_id',
                    "LABEL"         => __("Panel"),
                    "PLACEHOLDER"   => __("Select Panel ..."),
                    "AJAX_URL"      => route("$VIEW_ROOT.panels.list.all"),
                    "REQUIRED"      => true,
                    "VALUE"         => [
                        'id'    => $menuPlaceholder->panel_id ?? '',
                        'text'  => $menuPlaceholder->panel->title ?? '',
                    ],
                    'CONFIG'        => [
                        'allow_null'        => true,
                    ],
                ])

                @compo("$VIEW_ROOT.components.textfields.text",[
                    "NAME"      => 'title',
                    "LABEL"     => __('Title'),
                    "REQUIRED"  => true,
                    "VALUE"     => $menuPlaceholder->title,
                ])
            </div>
        @endslot
    @endcomponent
@endsection
