@php
    $TABLE = model2table($variable->rankable_type);
    $rankable = $variable->rankable;
@endphp

@if($TABLE)
    @php($route = Route::has("{$VIEW_ROOT}.{$TABLE}.show") ? route("{$VIEW_ROOT}.{$TABLE}.show", $rankable) : route("{$VIEW_ROOT}.{$TABLE}.edit", $rankable))
@endif


@compo("$VIEW_ROOT.components.clickables.link", [
    "URL"   => $route ?? '#',
    "TITLE" => $rankable->title ?? '',
])
