@php($user = $variable->user)
@compo("$VIEW_ROOT.components.clickables.link", [
    "URL"   => route("$VIEW_ROOT.users.show", $user),
    "TITLE" => "$user->username",
])
