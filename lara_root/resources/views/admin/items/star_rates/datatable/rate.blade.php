@if($variable->rate)
    <ul class="review-star left">
        @for($i = 0; $i < 5; $i++)
            @if($i < $variable->rate)
                <li><i class="fa fa-star"></i></li>
            @else
                <li><i class="fa fa-star-o"></i></li>
            @endif
        @endfor
    </ul>
@endif
