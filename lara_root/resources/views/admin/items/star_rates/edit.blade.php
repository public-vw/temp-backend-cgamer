@extends("$VIEW_ROOT.layouts.edit_wide",[
    'MODEL'     => 'StarRate',
    'VARIABLE'  => $starRate,
    'TITLE'     => 'StarRate',
    'TABLE'     => $VIEW_FOLDERS[1],
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.dropdowns.select2.single_array", [
                    "NAME"          => 'rankable_type',
                    "LABEL"         => __("Rankable Type"),
                    "PLACEHOLDER"   => __("Select Rankable Type..."),
                    "OPTIONS"       => config('morphs.rankable'),
                    "VALUE"         => [
                        'id'    => $starRate->rankable_type ?? '',
                        'text'  => Str::ucfirst($starRate->rankable_type) ?? '',
                    ],
                ])

                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'user_id',
                    "LABEL"         => __("User"),
                    "PLACEHOLDER"   => __("Select User ..."),
                    "AJAX_URL"      => route("$VIEW_ROOT.users.list.all"),
                    "VALUE"         => [
                        'id'    => $starRate->user_id ?? '',
                        'text'  => $starRate->user->username ?? '',
                    ],
                    'CONFIG'        => [
                        'allow_null' => true,
                    ],
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"                  => 'rankable_id',
                    "LABEL"                 => __("Rankable"),
                    "PLACEHOLDER"           => __("Select Rankable..."),
                    "VALUE"                 => [
                        'id'    => $starRate->rankable_id ?? '',
                        'text'  => $starRate->rankable->title ?? '',
                    ],
                    "MORPH_LIST"            => config('morphs.rankable'),
                    "AJAX_URL_DEPENDS_TO"   => '#rankable_type',
                ])

                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("IP"),
                    "NAME"          => 'ip',
                    "VALUE"         => $starRate->ip,
                    "PLACEHOLDER"   => '',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Rate"),
                    "NAME"          => 'rate',
                    "VALUE"         => $starRate->rate,
                    "TYPE"          => 'number',
                    "MORE"          => 'min="0" max="5"',
                ])

                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Weight"),
                    "NAME"          => 'weight',
                    "VALUE"         => $starRate->weight,
                    "TYPE"          => 'number',
                    "PLACEHOLDER"   => '',
                ])
            </div>
        @endslot
    @endcomponent
@endsection
