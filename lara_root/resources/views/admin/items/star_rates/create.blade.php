@extends("$VIEW_ROOT.layouts.create_wide",[
    'MODEL' => 'StarRate',
    'TABLE' => $VIEW_FOLDERS[1],
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.dropdowns.select2.single_array", [
                    "NAME"          => 'rankable_type',
                    "LABEL"         => __("Rankable Type"),
                    "PLACEHOLDER"   => __("Select Rankable Type..."),
                    "OPTIONS"       => config('morphs.rankable'),
                ])

                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'user_id',
                    "LABEL"         => __("User"),
                    "PLACEHOLDER"   => __("Select User ..."),
                    "AJAX_URL"      => route("$VIEW_ROOT.users.list.all"),
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"                  => 'rankable_id',
                    "LABEL"                 => __("Rankable"),
                    "PLACEHOLDER"           => __("Select Rankable..."),
                    "MORPH_LIST"            => config('morphs.rankable'),
                    "AJAX_URL_DEPENDS_TO"   => '#rankable_type',
                ])

                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("IP"),
                    "NAME"          => 'ip',
                    "PLACEHOLDER"   => '',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Rate"),
                    "NAME"          => 'rate',
                    "TYPE"          => 'number',
                    "MORE"          => 'min="0" max="5"',
                ])

                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Weight"),
                    "NAME"          => 'weight',
                    "TYPE"          => 'number',
                    "PLACEHOLDER"   => '',
                ])
            </div>
        @endslot
    @endcomponent
@endsection
