@php($country = $variable->city->province->country)
<span class="display-5 flag-icon flag-icon-{{ $country->flag_iso ?? '' }}"></span>
<span class="align-items-center ml-1 font-weight-bolder">{{ $country->title ?? 'no country set'}}</span>
