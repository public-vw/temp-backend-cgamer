@extends("$VIEW_ROOT.layouts.create_wide",[
    'MODEL' => 'Region',
    'TABLE' => $VIEW_FOLDERS[1],
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Title"),
                    "NAME"          => 'title',
                    "PLACEHOLDER"   => 'Region Name ...',
                    "REQUIRED"      => true,
                ])
                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'country_id',
                    "LABEL"         => __("Country"),
                    "PLACEHOLDER"   => __("Select Country ..."),
                    "AJAX_URL"      => route("$VIEW_ROOT.countries.list.all"),
                    'CONFIG'        => [
                        'allow_null' => true,
                    ],
                ])
            </div>
            <div class="row form-group">
                <div class="col-md-6 col-sm-12">

                </div>
                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'province_id',
                    "LABEL"         => __("Province"),
                    "PLACEHOLDER"   => __("Select Province ..."),
                    "AJAX_URL"      => route("$VIEW_ROOT.provinces.list.all"),
                    'CONFIG'        => [
                        'allow_null' => true,
                    ],
                    "DEPENDS_TO"    => '#country_id',
                ])
            </div>
            <div class="row form-group">
                <div class="col-md-6 col-sm-12">

                </div>
                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'city_id',
                    "LABEL"         => __("City"),
                    "PLACEHOLDER"   => __("Select City ..."),
                    "AJAX_URL"      => route("$VIEW_ROOT.cities.list.all"),
                    'CONFIG'        => [
                        'allow_null' => true,
                    ],
                    "DEPENDS_TO"    => '#province_id',
                ])
            </div>
        @endslot
    @endcomponent
@endsection
