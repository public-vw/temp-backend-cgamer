@extends("$VIEW_ROOT.layouts.edit_wide",[
    'MODEL'     => 'Region',
    'VARIABLE'  => $region,
    'TITLE'     => $region->title,
    'TABLE'     => $VIEW_FOLDERS[1],
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            @php($city = $region->city)
            @php($province = $city->province)
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Title"),
                    "NAME"          => 'title',
                    "PLACEHOLDER"   => __("Region Name"),
                    "VALUE"         => "$region->title",
                    "REQUIRED"      => true,
                ])

                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'country_id',
                    "LABEL"         => __("Country"),
                    "PLACEHOLDER"   => __("Select Country ..."),
                    "AJAX_URL"      => route("$VIEW_ROOT.countries.list.all"),
                    "VALUE"         => [
                        'id'    => $province->country_id ?? '',
                        'text'  => $province->country->title ?? '',
                    ],
                    'CONFIG'        => [
                        'allow_null' => true,
                    ],
                ])
            </div>
            <div class="row form-group">
                <div class="col-md-6 col-sm-12">

                </div>

                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'province_id',
                    "LABEL"         => __("Province"),
                    "PLACEHOLDER"   => __("Select Province ..."),
                    "AJAX_URL"      => route("$VIEW_ROOT.provinces.list.all"),
                    "DEPENDS_TO"    => '#country_id',
                    "VALUE"         => [
                        'id'    => $province->id ?? '',
                        'text'  => $province->title ?? '',
                    ],
                    'CONFIG'        => [
                        'allow_null' => true,
                    ],
                ])
            </div>
            <div class="row form-group">
                <div class="col-md-6 col-sm-12">

                </div>

                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'city_id',
                    "LABEL"         => __("City"),
                    "PLACEHOLDER"   => __("Select City ..."),
                    "AJAX_URL"      => route("$VIEW_ROOT.cities.list.all"),
                    "DEPENDS_TO"    => '#province_id',
                    "VALUE"         => [
                        'id'    => $city->id ?? '',
                        'text'  => $city->title ?? '',
                    ],
                    'CONFIG'        => [
                        'allow_null' => true,
                    ],
                ])
            </div>
        @endslot
    @endcomponent
@endsection
