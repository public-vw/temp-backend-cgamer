@extends("$VIEW_ROOT.layouts.create_wide",[
    'MODEL' => 'AttachmentType',
    'TABLE' => $VIEW_FOLDERS[1],
])

@push('js')
    <script>
        const snakeCase = string => {
            return string.replace(/\W+/g, " ")
                .split(/ |\B(?=[A-Z])/)
                .map(word => word.toLowerCase())
                .join('_');
        };

        $(function (){
            $('#title').on('keyup', function(){
                if ($('#slug').attr('manual') == 'yes') return;
                $('#slug').val(snakeCase($(this).val()));
            });
            $('#slug').on('keypress', function(){
                $('#slug').attr('manual','yes');
            });
        });
    </script>
@endpush

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Title"),
                    "NAME"          => 'title',
                    "PLACEHOLDER"   => '',
                    "REQUIRED"      => true,
                ])

                @compo("$VIEW_ROOT.components.dropdowns.select2.config_key", [
                    "NAME"          => 'type',
                    "LABEL"         => __("Type"),
                    "PLACEHOLDER"   => __("Select Type ..."),
                    "OPTIONS"       => config('enums.attachment_types'),
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Slug"),
                    "NAME"          => 'slug',
                    "PLACEHOLDER"   => '',
                    "REQUIRED"      => true,
                ])

                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Folder"),
                    "NAME"          => 'folder',
                    "PLACEHOLDER"   => 'images, videos, ...',
                    "REQUIRED"      => true,
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.textarea", [
                    "LABEL"         => __("Properties Limit"),
                    "NAME"          => 'properties_limit',
                    "TOOLTIP"       =>"minimum & maximum sizes, durations\n"
                                    ."[dur|10,100]\n"
                                    ."[width|10] <-- fix width\n"
                                    ."[height|10] <-- fix height\n"
                                    ."[width|10,100] <-- width between 10px and 100px\n"
                                    ."[width|10,~] <-- no upper bound\n"
                                    ."[width|~,10] <-- no lower bound\n"
                                    ."[box|10,100|20,-|-,300] <-- smart boxing\n"
                                    ."[size|100] <-- max filesize in KB" ,
                    "HELPER"        => '[ENTER] or [COMMA,] seperator accepted',
                ])
            </div>
        @endslot
    @endcomponent
@endsection
