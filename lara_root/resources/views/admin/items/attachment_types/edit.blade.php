@extends("$VIEW_ROOT.layouts.edit_wide",[
    'MODEL'     => 'AttachmentType',
    'VARIABLE'  => $attachmentType,
    'TITLE'     => $attachmentType->title,
    'TABLE'     => $VIEW_FOLDERS[1],
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Title"),
                    "NAME"          => 'title',
                    "PLACEHOLDER"   => '',
                    "VALUE"         => $attachmentType->title,
                    "REQUIRED"      => true,
                ])

                @compo("$VIEW_ROOT.components.dropdowns.select2.config_key", [
                    "NAME"          => 'type',
                    "LABEL"         => __("Type"),
                    "PLACEHOLDER"   => __("Select Type ..."),
                    "OPTIONS"       => config('enums.attachment_types'),
                    "VALUE"         => $attachmentType->type,
                ])
            </div>
            <div class="row form-group">
                {{-- TODO create mutator for slug --}}
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Slug"),
                    "NAME"          => 'slug',
                    "PLACEHOLDER"   => '',
                    "VALUE"         => $attachmentType->slug,
                    "REQUIRED"      => true,
                ])

                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Folder"),
                    "NAME"          => 'folder',
                    "PLACEHOLDER"   => 'images, videos, ...',
                    "REQUIRED"      => true,
                    "VALUE"         => $attachmentType->folder,
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.textarea", [
                    "LABEL"         => __("Properties Limit"),
                    "NAME"          => 'properties_limit',
                    "VALUE"         => $attachmentType->properties_limit,
                    "TOOLTIP"       =>"minimum & maximum sizes, durations\n"
                                    ."[dur|10,100]\n"
                                    ."[width|10] <-- fix width\n"
                                    ."[height|10] <-- fix height\n"
                                    ."[width|10,100] <-- width between 10px and 100px\n"
                                    ."[width|10,~] <-- no upper bound\n"
                                    ."[width|~,10] <-- no lower bound\n"
                                    ."[box|10,100|20,-|-,300] <-- smart boxing\n"
                                    ."[size|100] <-- max filesize in KB" ,
                    "HELPER"        => '[ENTER] or [COMMA,] seperator accepted',
                ])
            </div>
        @endslot
    @endcomponent
@endsection

@section('outside')
    @includeRelativeSection("$VIEW_ROOT.model_attachment_types",[
        "VARIABLE"  => $attachmentType,
    ])
@endsection
