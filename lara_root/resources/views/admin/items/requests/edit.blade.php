@extends("$VIEW_ROOT.layouts.edit_wide",[
    'MODEL'     => 'Request',
    'VARIABLE'  => $requesti,
    'TITLE'     => $requesti->title,
    'TABLE'     => $VIEW_FOLDERS[1],
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.dropdowns.select2.single_array", [
                    "NAME"          => 'requestable_type',
                    "LABEL"         => __("Requestable Type"),
                    "PLACEHOLDER"   => __("Select Requestable Type..."),
                    "OPTIONS"       => config('morphs.requestable'),
                    "VALUE"         => [
                        'id'    => $requesti->requestable_type ?? '',
                        'text'  => Str::ucfirst($requesti->requestable_type) ?? '',
                    ],
                ])

                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"                  => 'requestable_id',
                    "LABEL"                 => __("Requestable"),
                    "PLACEHOLDER"           => __("Select Requestable..."),
                    "MORPH_LIST"            => config('morphs.requestable'),
                    "AJAX_URL_DEPENDS_TO"   => '#requestable_type',
                    "VALUE"                 => [
                        'id'    => $requesti->requestable_id ?? '',
                        'text'  => $requesti->requestable->title ?? '',
                    ],
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Subject"),
                    "NAME"          => 'subject',
                    "VALUE"         => $requesti->subject,
                    "PLACEHOLDER"   => 'e.g. Agency branch 1 change title',
                ])

                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Field"),
                    "NAME"          => 'field',
                    "VALUE"         => $requesti->field,
                    "PLACEHOLDER"   => '',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.dropdowns.select2.config_key", [
                    "NAME"          => 'priority',
                    "LABEL"         => __("Priority"),
                    "PLACEHOLDER"   => __("Select Priority ..."),
                    "OPTIONS"       => config('enums.requests_priorities'),
                    "VALUE"         => $requesti->priority,
                ])

                @compo("$VIEW_ROOT.components.dropdowns.select2.config_key", [
                    "NAME"          => 'status',
                    "LABEL"         => __("Status"),
                    "PLACEHOLDER"   => __("Select Status ..."),
                    "OPTIONS"       => config('enums.requests_status'),
                    "VALUE"         => $requesti->status,
                ])
            </div>
            <div class="row form-group">
                <div class="col-md-6 col-sm-12"></div>

                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'user_id',
                    "LABEL"         => __("User"),
                    "PLACEHOLDER"   => __("Select User ..."),
                    "AJAX_URL"      => route("$VIEW_ROOT.users.list.all"),
                    "VALUE"         => [
                        'id'    => $requesti->user_id ?? '',
                        'text'  => $requesti->user->username ?? '',
                    ],
                    "REQUIRED"      => true,
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.rich_textarea", [
                    "NAME"              => 'description',
                    "VALUE"             => $requesti->description,
                    "LABEL"             => __('Description'),
                    "CONTAINER_CLASS"   => 'col-12',
                    "ROWS"              => '10',
                    "MORE"              => 'cols="30"',
                ])
            </div>
        @endslot
    @endcomponent
@endsection
