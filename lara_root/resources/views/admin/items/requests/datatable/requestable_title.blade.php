@php
    $TABLE = model2table($variable->requestable_type);
    $requestable = $variable->requestable;
    if($TABLE){
        $route = Route::has("{$VIEW_ROOT}.{$TABLE}.show") ? route("{$VIEW_ROOT}.{$TABLE}.show", $requestable) : route("{$VIEW_ROOT}.{$TABLE}.edit", $requestable);
    }
@endphp

@compo("$VIEW_ROOT.components.clickables.link", [
    "URL"   => $route ?? '#',
    "TITLE" => $requestable->title ?? '',
])
