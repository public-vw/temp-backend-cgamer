@extends("$VIEW_ROOT.layouts.create_wide",[
    'MODEL'      => 'SocialChannel',
    'TABLE'      => $VIEW_FOLDERS[1],
    'HAS_UPLOAD' => true,
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Title"),
                    "NAME"          => 'title',
                    "PLACEHOLDER"   => 'e.g. gmail',
                    "REQUIRED"      => true,
                ])

                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'attachment_type',
                    "LABEL"         => __("Attachment Type"),
                    "PLACEHOLDER"   => __("Select Attachment Type ..."),
                    "AJAX_URL"      => route("$VIEW_ROOT.attachment_types.list.all"),
                    'CONFIG'        => [
                        'allow_null' => true,
                    ],
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Url"),
                    "NAME"          => 'url',
                    "PLACEHOLDER"   => '',
                ])

                {{-- TODO improve upload (thumbnail_id) --}}
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL" => 'Attachment',
                    "NAME" => 'file',
                    "TYPE" => 'file',
                    "MORE_CLASS" => 'form-control-file',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.boxes.checkbox", [
                    "TITLE"    => __("Active"),
                    "NAME"     => 'active',
                    "CLASS"    => 'custom-control-input',
                    "SWITCH"   => true,
                    "CHECKED"  => true,
                ])
            </div>
        @endslot
    @endcomponent
@endsection
