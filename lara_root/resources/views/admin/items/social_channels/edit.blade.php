@extends("$VIEW_ROOT.layouts.edit_wide",[
    'MODEL'      => 'SocialChannel',
    'VARIABLE'   => $socialChannel,
    'TITLE'      => $socialChannel->title,
    'TABLE'      => $VIEW_FOLDERS[1],
    'HAS_UPLOAD' => true,
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Title"),
                    "NAME"          => 'title',
                    "VALUE"         => $socialChannel->title,
                    "PLACEHOLDER"   => 'e.g. Agency branch 1 email',
                    "REQUIRED"      => true,
                ])

            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Url"),
                    "NAME"          => 'url',
                    "VALUE"         => $socialChannel->url,
                    "PLACEHOLDER"   => '',
                ])

                @compo("$VIEW_ROOT.components.files.attachment", [
                    "LABEL"      => 'Thumbnail',
                    "NAME"       => 'thumbnail',
                    "TYPE_SLUG"  => 'social_channel_thumbnail',
                    "VALUE"      => $socialChannel->thumbnail->url,
                    "MORE_CLASS" => 'form-control-file',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.boxes.checkbox", [
                    "TITLE"    => __("Active"),
                    "NAME"     => 'active',
                    "CLASS"    => 'custom-control-input',
                    "SWITCH"   => true,
                    "CHECKED"  => $socialChannel->active,
                ])
            </div>
        @endslot
    @endcomponent
@endsection
