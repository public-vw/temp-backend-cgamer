@php
    $TABLE = model2table($variable->phonable_type);
    $phonable = $variable->phonable;
    if($TABLE){
        $route = Route::has("{$VIEW_ROOT}.{$TABLE}.show") ? route("{$VIEW_ROOT}.{$TABLE}.show", $phonable) : route("{$VIEW_ROOT}.{$TABLE}.edit", $phonable);
    }
@endphp

@compo("$VIEW_ROOT.components.clickables.link", [
    "URL"   => $route ?? '#',
    "TITLE" => $phonable->title ?? '',
])
