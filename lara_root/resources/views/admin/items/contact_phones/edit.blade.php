@extends("$VIEW_ROOT.layouts.edit_wide",[
    'MODEL'     => 'ContactPhone',
    'VARIABLE'  => $contactPhone,
    'TITLE'     => $contactPhone->title,
    'TABLE'     => $VIEW_FOLDERS[1],
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @php
            $city = $contactPhone->city;
            $province = $city->province;
            $country = $province->country;
        @endphp

        @slot('body')
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Title"),
                    "NAME"          => 'title',
                    "VALUE"         => $contactPhone->title,
                    "PLACEHOLDER"   => 'e.g. Agency branch 1 phone',
                    "REQUIRED"      => true,
                ])

                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'country_id',
                    "LABEL"         => __("Country"),
                    "PLACEHOLDER"   => __("Select Country ..."),
                    "AJAX_URL"      => route("$VIEW_ROOT.countries.list.all"),
                    "VALUE"         => [
                        'id'    => $country->id ?? '',
                        'text'  => $country->title ?? '',
                    ],
                    'CONFIG'        => [
                        'allow_null' => true,
                    ],
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.dropdowns.select2.single_array", [
                    "NAME"          => 'phonable_type',
                    "LABEL"         => __("Phonable Type"),
                    "PLACEHOLDER"   => __("Select Phonable Type..."),
                    "OPTIONS"       => config('morphs.phonable'),
                    "VALUE"         => [
                        'id'    => $contactPhone->phonable_type ?? '',
                        'text'  => Str::ucfirst($contactPhone->phonable_type) ?? '',
                    ],
                ])

                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'province_id',
                    "LABEL"         => __("Province"),
                    "PLACEHOLDER"   => __("Select Province ..."),
                    "AJAX_URL"      => route("$VIEW_ROOT.provinces.list.all"),
                    "DEPENDS_TO"       => '#country_id',
                    "VALUE"         => [
                        'id'    => $province->id ?? '',
                        'text'  => $province->title ?? '',
                    ],
                    'CONFIG'        => [
                        'allow_null' => true,
                    ],
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"                  => 'phonable_id',
                    "LABEL"                 => __("Phonable"),
                    "PLACEHOLDER"           => __("Select Phonable..."),
                    "MORPH_LIST"            => config('morphs.phonable'),
                    "AJAX_URL_DEPENDS_TO"   => '#phonable_type',
                    "VALUE"                 => [
                        'id'    => $contactPhone->phonable_id ?? '',
                        'text'  => $contactPhone->phonable->title ?? '',
                    ],
                ])

                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'city_id',
                    "LABEL"         => __("City"),
                    "PLACEHOLDER"   => __("Select City ..."),
                    "AJAX_URL"      => route("$VIEW_ROOT.cities.list.all"),
                    "DEPENDS_TO"       => '#province_id',
                    "VALUE"         => [
                        'id'    => $city->id ?? '',
                        'text'  => $city->title ?? '',
                    ],
                    'CONFIG'        => [
                        'allow_null' => true,
                    ],
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Number"),
                    "NAME"          => 'number',
                    "VALUE"         => $contactPhone->number,
                    "PLACEHOLDER"   => '',
                ])

                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'region_id',
                    "LABEL"         => __("Region"),
                    "PLACEHOLDER"   => __("Select Region ..."),
                    "AJAX_URL"      => route("$VIEW_ROOT.regions.list.all"),
                    "DEPENDS_TO"       => '#city_id',
                    "VALUE"         => [
                        'id'    => $contactPhone->region_id ?? '',
                        'text'  => $contactPhone->region->title ?? '',
                    ],
                    'CONFIG'        => [
                        'allow_null' => true,
                    ],
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Order"),
                    "NAME"          => 'order',
                    "type"          => 'number',
                    "VALUE"         => $contactPhone->order,
                    "PLACEHOLDER"   => '',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.boxes.checkbox", [
                    "TITLE"    => __("Active"),
                    "NAME"     => 'active',
                    "CLASS"    => 'custom-control-input',
                    "SWITCH"   => true,
                    "CHECKED"  => $contactPhone->active,
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.boxes.checkbox", [
                    "TITLE"    => __("Default"),
                    "NAME"     => 'default',
                    "CLASS"    => 'custom-control-input',
                    "SWITCH"   => true,
                    "CHECKED"  => $contactPhone->default,
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.rich_textarea", [
                    "NAME"              => 'description',
                    "VALUE"             => $contactPhone->description,
                    "LABEL"             => __('Description'),
                    "CONTAINER_CLASS"   => 'col-12',
                    "ROWS"              => '10',
                    "MORE"              => 'cols="30"',
                ])
            </div>
        {{-- TODO create phone verification --}}
        @endslot
    @endcomponent
@endsection
