@php
    $table = 'contact_phones';
    $title = __('Contact Phones');
    $headers = [
        'title'  => __('Title'),
        'number' => __('Number'),
        'region' => __('Region'),
        'active' => __('Active'),
    ];
    $items = $VARIABLE->contactPhones ?? [];
    $morph = [
        'type'  => $VARIABLE->getTable(),
        'id'    => $VARIABLE->id,
    ];
@endphp

@compo("$VIEW_ROOT.components.inline_section")
