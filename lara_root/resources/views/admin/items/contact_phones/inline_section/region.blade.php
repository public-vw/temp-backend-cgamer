@php($city = $variable->city)

@php($province = $city->province)

@php($country = $province->country)

@isset($country->flag_iso)
    <span class="display-5 flag-icon flag-icon-{{ $country->flag_iso }}"></span>
@endisset
<span class="align-items-center ml-1 font-weight-bolder">{{ $province->title ?? ''}}</span>
<span class="align-items-center ml-1 font-weight-bolder">{{ $city->title ?? 'no city set'}}</span>
<span class="align-items-center ml-1 font-weight-bolder">{{ $variable->region->title ?? 'no region set'}}</span>
