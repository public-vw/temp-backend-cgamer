@extends("$VIEW_ROOT.layouts.create_wide",[
    'MODEL' => 'ContactPhone',
    'TABLE' => $VIEW_FOLDERS[1],
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
                @isset($morph)
                    @compo("$VIEW_ROOT.components.textfields.hidden",[
                        "NAME"  => 'phonable_type',
                        "VALUE" => $morph['type'],
                    ])

                    @compo("$VIEW_ROOT.components.textfields.hidden",[
                        "NAME"  => 'phonable_id',
                        "VALUE" => $morph['id'],
                    ])
                @else
                    <div class="row form-group">
                        @compo("$VIEW_ROOT.components.dropdowns.select2.single_array", [
                            "NAME"          => 'phonable_type',
                            "LABEL"         => __("Phonable Type"),
                            "PLACEHOLDER"   => __("Select Phonable Type..."),
                            "OPTIONS"       => config('morphs.phonable'),
                        ])

                        @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                            "NAME"                  => 'phonable_id',
                            "LABEL"                 => __("Phonable"),
                            "PLACEHOLDER"           => __("Select Phonable..."),
                            "MORPH_LIST"            => config('morphs.phonable'),
                            "AJAX_URL_DEPENDS_TO"   => '#phonable_type',
                        ])
                    </div>
                @endisset
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Title"),
                    "NAME"          => 'title',
                    "PLACEHOLDER"   => 'e.g. Agency branch 1 phone',
                    "REQUIRED"      => true,
                ])

                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'country_id',
                    "LABEL"         => __("Country"),
                    "PLACEHOLDER"   => __("Select Country ..."),
                    "AJAX_URL"      => route("$VIEW_ROOT.countries.list.all"),
                    'CONFIG'        => [
                        'allow_null' => true,
                    ],
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Number"),
                    "NAME"          => 'number',
                    "PLACEHOLDER"   => '',
                ])

                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'province_id',
                    "LABEL"         => __("Province"),
                    "PLACEHOLDER"   => __("Select Province ..."),
                    "AJAX_URL"      => route("$VIEW_ROOT.provinces.list.all"),
                    'CONFIG'        => [
                        'allow_null' => true,
                    ],
                    "DEPENDS_TO"       => '#country_id',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Order"),
                    "NAME"          => 'order',
                    "TYPE"          => 'number',
                    "PLACEHOLDER"   => '',
                ])

                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'city_id',
                    "LABEL"         => __("City"),
                    "PLACEHOLDER"   => __("Select City ..."),
                    "AJAX_URL"      => route("$VIEW_ROOT.cities.list.all"),
                    'CONFIG'        => [
                        'allow_null' => true,
                    ],
                    "DEPENDS_TO"    => '#province_id',
                ])
            </div>
            <div class="row form-group">
                <div class="col-md-6 col-sm-12"></div>

                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'region_id',
                    "LABEL"         => __("Region"),
                    "PLACEHOLDER"   => __("Select Region ..."),
                    "AJAX_URL"      => route("$VIEW_ROOT.regions.list.all"),
                    'CONFIG'        => [
                        'allow_null' => true,
                    ],
                    "DEPENDS_TO"    => '#city_id',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.boxes.checkbox", [
                    "TITLE"    => __("Active"),
                    "NAME"     => 'active',
                    "CLASS"    => 'custom-control-input',
                    "SWITCH"   => true,
                    "CHECKED"  => true,
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.boxes.checkbox", [
                    "TITLE"   => __("Default"),
                    "NAME"    => 'default',
                    "CLASS"   => 'custom-control-input',
                    "SWITCH"  => true,
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.rich_textarea", [
                    "NAME"              => 'description',
                    "LABEL"             => __('Description'),
                    "CONTAINER_CLASS"   => 'col-12',
                    "ROWS"              => '10',
                    "MORE"              => 'cols="30"',
                ])
            </div>
        {{-- TODO create phone verification --}}
        @endslot
    @endcomponent
@endsection
