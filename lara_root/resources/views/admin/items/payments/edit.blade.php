@extends("$VIEW_ROOT.layouts.edit_wide",[
    'MODEL'     => 'Payment',
    'VARIABLE'  => $payment,
    'TITLE'     => $payment->title,
    'TABLE'     => $VIEW_FOLDERS[1],
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            <div class="row form-group">
                ## Form Goes Here ##
            </div>
        @endslot
    @endcomponent
@endsection
