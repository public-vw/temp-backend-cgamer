{{--@if(auth()->user()->hasRole('admin@web') || $variable->user->isChildOf(auth()->user()))--}}
@role('admin@web')
@compo("$VIEW_ROOT.components.actions.delete", [
    "VARIABLE"  => $variable,
    "TABLE"     => $VIEW_FOLDERS[1],
])
@endrole
{{--@endif--}}
