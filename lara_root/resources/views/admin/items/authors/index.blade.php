@extends("$VIEW_ROOT.layouts.list_wide",[
    'MODEL' => 'Author',
    'TABLE' => $VIEW_FOLDERS[1],
    'NO_CREATE' => $user->hasPermissionTo('authors.create.all')?'':'yes'
])
