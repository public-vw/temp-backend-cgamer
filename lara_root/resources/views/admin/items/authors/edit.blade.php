@extends("$VIEW_ROOT.layouts.edit_wide",[
    'MODEL'     => 'Author',
    'VARIABLE'  => $author,
    'TITLE'     => $author->user->nickname,
    'TABLE'     => $VIEW_FOLDERS[1],
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            @role('admin@web')
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'user_id',
                    "LABEL"         => __("User"),
                    "PLACEHOLDER"   => __("Select User ..."),
                    "AJAX_URL"      => route("$VIEW_ROOT.users.list.all"),
                    "REQUIRED"      => true,
                    "VALUE"       => [
                        'id'    => $author->user_id ?? '',
                        'text'  => $author->user->nickname ?? '',
                    ],
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                "LABEL"         => __("Article Limit"),
                "NAME"          => 'article_limit',
                "TYPE"          => 'number',
                "MORE"          => 'min="0" max="30"',
                "VALUE"         => $author->article_limit,
                ])
            </div>
            @else
                <div class="row form-group">
                    <div class="col-md-6 col-sm-12">
                        <b>Nickname :</b> {{ $author->user->nickname }}
                    </div>
                </div>
            @endrole
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.dropdowns.select2.config_key", [
                    "NAME"          => 'status',
                    "LABEL"         => __("Status"),
                    "PLACEHOLDER"   => __("Select Status ..."),
                    "OPTIONS"       => config('enums.authors_status'),
                    'REQUIRED'      => true,
                    "VALUE"         => $author->status,
                ])
            </div>
        @endslot
    @endcomponent
@endsection
