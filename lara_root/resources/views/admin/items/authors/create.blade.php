@extends("$VIEW_ROOT.layouts.create_wide",[
    'MODEL' => 'Author',
    'TABLE' => $VIEW_FOLDERS[1],
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'user_id',
                    "LABEL"         => __("User"),
                    "PLACEHOLDER"   => __("Select User ..."),
                    "AJAX_URL"      => route("$VIEW_ROOT.users.list.all"),
                    "REQUIRED"      => true,
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Article Limit"),
                    "NAME"          => 'article_limit',
                    "TYPE"          => 'number',
                    "MORE"          => 'min="0" max="30"',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.dropdowns.select2.config_key", [
                    "NAME"          => 'status',
                    "LABEL"         => __("Status"),
                    "PLACEHOLDER"   => __("Select Status ..."),
                    "OPTIONS"       => config('enums.authors_status'),
                    'REQUIRED'      => true,
                ])
            </div>
        @endslot
    @endcomponent
@endsection
