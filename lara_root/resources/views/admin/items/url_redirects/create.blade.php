@extends("$VIEW_ROOT.layouts.create_wide",[
    'MODEL' => 'Url Redirect',
    'TABLE' => $VIEW_FOLDERS[1],
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Title"),
                    "NAME"          => 'title',
                    "PLACEHOLDER"   => 'bugfix google index',
                    "REQUIRED"      => true,
                ])
                @compo("$VIEW_ROOT.components.dropdowns.select2.single_array", [
                    "NAME"          => 'state_code',
                    "LABEL"         => __("Redirection State"),
                    "OPTIONS"       => ['301'=>'Status 301', '302'=>'Status 302'],
                    "CONFIG"        => ['hide_searchbox' => true],
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("From"),
                    "NAME"          => 'from',
                    "PLACEHOLDER"   => '',
                    "REQUIRED"      => true,
                ])
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("To"),
                    "NAME"          => 'to',
                    "PLACEHOLDER"   => '',
                    "REQUIRED"      => true,
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.boxes.checkbox", [
                    "TITLE"    => __("Start With %From%"),
                    "NAME"     => 'start_with',
                    "CLASS"    => 'custom-control-input',
                    "SWITCH"   => true,
                    "CHECKED"  => false,
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.boxes.checkbox", [
                    "TITLE"    => __("External Link"),
                    "NAME"     => 'external',
                    "CLASS"    => 'custom-control-input',
                    "SWITCH"   => true,
                    "CHECKED"  => false,
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.boxes.checkbox", [
                    "TITLE"    => __("Active"),
                    "NAME"     => 'active',
                    "CLASS"    => 'custom-control-input',
                    "SWITCH"   => true,
                    "CHECKED"  => true,
                ])
            </div>
        @endslot
    @endcomponent
@endsection
