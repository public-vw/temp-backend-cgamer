@extends("$VIEW_ROOT.layouts.edit_wide",[
    'MODEL'     => 'Url Redirect',
    'VARIABLE'  => $urlRedirect,
    'TITLE'     => $urlRedirect->title,
    'TABLE'     => $VIEW_FOLDERS[1],
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Title"),
                    "NAME"          => 'title',
                    "PLACEHOLDER"   => 'bugfix google index',
                    "VALUE"         => $urlRedirect->title,
                    "REQUIRED"      => true,
                ])
                @compo("$VIEW_ROOT.components.dropdowns.select2.single_array", [
                    "NAME"          => 'state_code',
                    "LABEL"         => __("Redirection State"),
                    "VALUE"         => $urlRedirect->state_code,
                    "OPTIONS"       => ['301'=>'Status 301', '302'=>'Status 302'],
                    "CONFIG"        => ['hide_searchbox' => true],
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("From"),
                    "NAME"          => 'from',
                    "VALUE"         => $urlRedirect->from,
                    "PLACEHOLDER"   => '',
                    "REQUIRED"      => true,
                ])
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("To"),
                    "NAME"          => 'to',
                    "VALUE"         => $urlRedirect->to,
                    "PLACEHOLDER"   => '',
                    "REQUIRED"      => true,
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.boxes.checkbox", [
                    "TITLE"    => __("Start With %From%"),
                    "NAME"     => 'start_with',
                    "CLASS"    => 'custom-control-input',
                    "SWITCH"   => true,
                    "CHECKED"  => $urlRedirect->start_with,
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.boxes.checkbox", [
                    "TITLE"    => __("External Link"),
                    "NAME"     => 'external',
                    "CLASS"    => 'custom-control-input',
                    "SWITCH"   => true,
                    "CHECKED"  => $urlRedirect->external,
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.boxes.checkbox", [
                    "TITLE"    => __("Active"),
                    "NAME"     => 'active',
                    "CLASS"    => 'custom-control-input',
                    "SWITCH"   => true,
                    "CHECKED"  => $urlRedirect->active,
                ])
            </div>
        @endslot
    @endcomponent
@endsection
