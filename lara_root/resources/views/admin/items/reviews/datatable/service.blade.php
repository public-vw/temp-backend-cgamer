{{--#TODO create relation between review and service--}}

@php( $service = $variable->service )

@if(empty($service))
{{ __("[Deleted]") }}
@else
{{--#TODO fix href--}}
{{--#TODO check getModelName()--}}
{{--#TODO check get_class()--}}
@compo("$VIEW_ROOT.components.clickables.link", [
    "URL"   => "#",
    "TITLE" => "{{ get_class($service) }}",
    "CLASS" => "badge badge-dark",
])
@endif
