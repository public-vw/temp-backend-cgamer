@php
$title = "<strong>$variable->title</strong>
<p>" . Str::substr($variable->content,0,10) . " ...</p>";

if($variable->rate_number){
    $title.= '<ul class="review-star left">';
    for($i = 0; $i < 5; $i++){
        if($i < $variable->rate_number) $title .= '<li><i class="fa fa-star"></i></li>';
        else $title .= '<li><i class="fa fa-star-o"></i></li>';
    }
    $title .= '</ul>';
}
@endphp

@compo("$VIEW_ROOT.components.clickables.link", [
    "URL"       => route("$VIEW_ROOT.{$VIEW_FOLDERS[1]}.edit", $variable),
    "TITLE"     => $title,
    "RAW_HTML"  => true,
])
