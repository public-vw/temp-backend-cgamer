{{--#TODO create relation between review and service--}}

@php($service = $variable->service)

@if(empty($service))
{{ __("[Deleted]") }}
@else
{{--#TODO fix href--}}
@compo("$VIEW_ROOT.components.clickables.link", [
    "URL"   => "#",
    "TITLE" => "$service->title",
])

<p>
    @compo("$VIEW_ROOT.components.clickables.link", [
        "URL"       => "#",
        "TITLE"     => "<i class='fa fa-long-arrow-right' aria-hidden='true'></i> __('View :name', ['name' => get_class($service) ])",
        "TARGET"    => "_blank",
    ])
</p>
@endif
