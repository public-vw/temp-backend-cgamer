{{--#TODO fix href--}}

@php($author = $variable->author)

@if( empty($author))
{{ __("[Author Deleted]") }}
@else
    @compo("$VIEW_ROOT.components.clickables.link", [
        "URL"   => "#",
        "TITLE" => $author->email ?? 'Email',
    ])

    <p>
        @compo("$VIEW_ROOT.components.clickables.link", [
            "URL"   => "#",
            "TITLE" => "$variable->author_ip",
        ])
    </p>
@endif
