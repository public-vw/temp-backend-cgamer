@extends("$VIEW_ROOT.layouts.edit_wide",[
    'MODEL'     => 'Review',
    'VARIABLE'  => $review,
    'TITLE'     => '',
    'TABLE'     => $VIEW_FOLDERS[1],
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.dropdowns.select2.single_array", [
                    "NAME"          => 'reviewable_type',
                    "LABEL"         => __("Reviewable Type"),
                    "PLACEHOLDER"   => __("Select Reviewable Type..."),
                    "OPTIONS"       => config('morphs.reviewable'),
                    "VALUE"         => [
                        'id'    => $review->reviewable_type ?? '',
                        'text'  => Str::ucfirst($review->reviewable_type) ?? '',
                    ],
                ])

                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'author_id',
                    "LABEL"         => __("Author"),
                    "PLACEHOLDER"   => __("Select Author ..."),
                    "AJAX_URL"      => route("$VIEW_ROOT.users.list.all"),
                    "VALUE"         => [
                        'id'    => $review->author_id ?? '',
                        'text'  => $review->author->username ?? '',
                    ],
                    'CONFIG'        => [
                        'allow_null' => true,
                    ],
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"                  => 'reviewable_id',
                    "LABEL"                 => __("Reviewable"),
                    "PLACEHOLDER"           => __("Select Reviewable..."),
                    "MORPH_LIST"            => config('morphs.reviewable'),
                    "AJAX_URL_DEPENDS_TO"   => '#reviewable_type',
                    "VALUE"                 => [
                        'id'    => $review->reviewable_id ?? '',
                        'text'  => $review->reviewable->title ?? '',
                    ],
                ])

                @compo("$VIEW_ROOT.components.dropdowns.select2.config_key", [
                    "NAME"          => 'status',
                    "LABEL"         => __("Status"),
                    "PLACEHOLDER"   => __("Select Status ..."),
                    "OPTIONS"       => config('enums.reviews_status'),
                    "VALUE"         => $review->status,
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.rich_textarea", [
                    "NAME"              => 'content',
                    "VALUE"             => $review->content,
                    "LABEL"             => __('Content'),
                    "CONTAINER_CLASS"   => 'col-12',
                    "ROWS"              => '10',
                    "MORE"              => 'cols="30"',
                ])
            </div>
        @endslot
    @endcomponent
@endsection

@section('outside')
    @includeRelativeSection("$VIEW_ROOT.comments",[
        "VARIABLE"  => $review,
    ])
@endsection
