@extends("$VIEW_ROOT.layouts.base",['page_title' => '_Templates'])

@section('title',"$VIEW_ROOT | _Templates | Show One")

@section('content')

##CONTENTS##

    <br>

    <form action="{{route("$VIEW_ROOT._templates.edit",[$item])}}" method="get" style="display:inline-block">@csrf<input type="submit" value="Edit"></form>
    <form action="{{route("$VIEW_ROOT._templates.destroy",[$item])}}" method="post" style="display:inline-block">@csrf @method('DELETE')<input type="submit" value="Delete"></form>

    <br>

    @include("$VIEW_ROOT.components.combinations.errors")

@endsection

