@compo("$VIEW_ROOT.components.actions.edit", [
    "VARIABLE"  => $country,
    "TABLE"     => 'countries',
])
|
@compo("$VIEW_ROOT.components.actions.show", [
    "VARIABLE"  => $country,
    "TABLE"     => 'countries',
])
|
@compo("$VIEW_ROOT.components.actions.delete", [
    "VARIABLE"  => $country,
    "TABLE"     => 'countries',
])
