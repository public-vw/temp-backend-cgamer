@extends("$VIEW_ROOT.layouts.base",['page_title' => '_Templates'])

@section('title', ucfirst($VIEW_ROOT) . " | _Template | Create New")

@section('content')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">admin

            @include("$VIEW_ROOT.components.combinations.errors")

            <form action="{{route("$VIEW_ROOT._templates.store")}}" method="post">
                @csrf

##FORM ELEMENTS##

                @compo("$VIEW_ROOT.components.clickables.button",[
                "TITLE" => __('forms.globals.create'),
                "CLASS" => 'btn-primitive btn-brand btn-elevate btn-icon-sm',
                'TYPE'  => 'submit',
                ])

            </form>

        </div>
    </div>

@endsection


