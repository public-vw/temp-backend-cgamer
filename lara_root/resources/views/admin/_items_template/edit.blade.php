@extends("$VIEW_ROOT.layouts.base",['page_title' => '_Templates'])

@section('title',"$VIEW_ROOT | _Template | Edit One")

@push('css')
    <style>
        .warning{
            border:1px solid red;
        }
    </style>
@endpush

@section('content')

    @if($errors->any())
        @foreach($errors->all() as $error)
            <div>{{$error}}</div>
        @endforeach
        <br>
    @endif

    <form action="{{route("$VIEW_ROOT._templates.update",[$item])}}" method="post">
        @csrf

        @method('PATCH')

##FORM ELEMENTS##

        @compo("$VIEW_ROOT.components.clickables.button",[
            "TITLE" => __('forms.globals.edit'),
            "CLASS" => 'btn-primitive btn-brand btn-elevate btn-icon-sm',
            'TYPE'  => 'submit',
        ])

    </form>

@endsection
