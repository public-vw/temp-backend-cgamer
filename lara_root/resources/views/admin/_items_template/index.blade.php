@extends("$VIEW_ROOT.layouts.base",['page_title' => '_Templates'])

@section('title',"$VIEW_ROOT | _Templates | View")

@section('content')
    <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
        {{--
                <div class="alert alert-light alert-elevate" role="alert">
                    <div class="alert-icon"><i class="flaticon-warning kt-font-brand"></i></div>
                    <div class="alert-text">
                        DataTables is a plug-in for the jQuery Javascript library. It is a highly flexible tool, based upon the foundations of progressive enhancement, and will add advanced interaction controls to any HTML table.
                        <br>For more info see <a class="kt-link kt-font-bold" href="https://datatables.net/" target="_blank">the official home</a> of the plugin.
                    </div>
                </div>
        --}}
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="kt-font-brand flaticon2-line-chart"></i>
                    </span>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <div class="kt-portlet__head-actions">
                            {{--
                                                        <div class="dropdown dropdown-inline">
                                                            <button type="button" class="btn btn-default btn-icon-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                <i class="la la-download"></i> Export
                                                            </button>
                                                            <div class="dropdown-menu dropdown-menu-right">
                                                                <ul class="kt-nav">
                                                                    <li class="kt-nav__section kt-nav__section--first">
                                                                        <span class="kt-nav__section-text">Choose an option</span>
                                                                    </li>
                                                                    <li class="kt-nav__item">
                                                                        <a href="#" class="kt-nav__link">
                                                                            <i class="kt-nav__link-icon la la-print"></i>
                                                                            <span class="kt-nav__link-text">Print</span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="kt-nav__item">
                                                                        <a href="#" class="kt-nav__link">
                                                                            <i class="kt-nav__link-icon la la-copy"></i>
                                                                            <span class="kt-nav__link-text">Copy</span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="kt-nav__item">
                                                                        <a href="#" class="kt-nav__link">
                                                                            <i class="kt-nav__link-icon la la-file-excel-o"></i>
                                                                            <span class="kt-nav__link-text">Excel</span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="kt-nav__item">
                                                                        <a href="#" class="kt-nav__link">
                                                                            <i class="kt-nav__link-icon la la-file-text-o"></i>
                                                                            <span class="kt-nav__link-text">CSV</span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="kt-nav__item">
                                                                        <a href="#" class="kt-nav__link">
                                                                            <i class="kt-nav__link-icon la la-file-pdf-o"></i>
                                                                            <span class="kt-nav__link-text">PDF</span>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                            --}}
                            &nbsp;
                            <a href="{{route("$VIEW_ROOT._templates.create")}}" class="btn btn-brand btn-elevate btn-icon-sm">{!! __('lists.globals.new_record') !!}</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">

                @include("$VIEW_ROOT.components.tables.datatable",[
                    'view' => "$VIEW_ROOT._templates",
                    'COLUMNS' => [
##TABLE COLS##
                    ],
                    'ITEMS' => $items,
                ])

            </div>
        </div>
    </div>

@endsection

