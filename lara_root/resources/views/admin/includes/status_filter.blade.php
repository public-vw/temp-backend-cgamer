<div class="col text-right py-2 pr-4">
    @foreach($statuses as $status => $count)
        @continue($status == 'all')
        <a href="{{ route("{$VIEW_ROOT}.{$TABLE}.index", ['status' => $status]) }}" class="text-decoration-none">
            {{ __(config(implode('.',['enums',Str::lower($TABLE).'_status',$status]))) }}
            <span class="text-dark">({{ $count }})</span>
        </a> ▪
    @endforeach

        <a href="{{ route("{$VIEW_ROOT}.{$TABLE}.index") }}" class="text-decoration-none">
            {{ __("all") }}
            <span class="text-dark">({{ $statuses['all'] }})</span>
        </a>
</div>
