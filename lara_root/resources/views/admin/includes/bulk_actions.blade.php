@pushonce('js:bulk_action')
    <script>
        $('form.bulk-action-form').submit(function () {
            var action = $(this).find('[name=action]').val();
            var actionTitle = $(this).find('[name=action] option:selected').text();
            var idsField = $(this).find('[name=ids]');

            let ids = [];
            $("input.check-item:checked").each(function () {
                ids.push($(this).val());
            });
            if(ids.length <= 0){
                alert("First select some rows");
                return false;
            }

            $(idsField).val(JSON.stringify(ids));

            if(!confirm(actionTitle + " all selected items?")){
                return false;
            }

            return true;
        });

        $('.check-all,.rev-all').click(function (){
            $('.check-item').each(function(){
                $(this).trigger('click');
            });
        });
    </script>
@endpushonce

<div class="col">
<form method="post" action="{{ route("{$VIEW_ROOT}.{$TABLE}.bulk_actions") }}" class="bulk-action-form filter-form filter-form-left d-flex justify-content-start">
    @csrf
    <input type="hidden" name="ids" value="" />
    <div class="input-group">
        <select name="action" class="custom-select mr-0" required>
            <option value="">{{ __('lists.bulk_actions.title') }}</option>
            @foreach($statuses as $status => $count)
                @continue($status == 'all')
                @php($status_title = config(implode('.',['enums',Str::lower($TABLE).'_status',$status])))
                <option value="{{ $status }}">{{ __('lists.bulk_actions.moveto',['Status' => Str::ucfirst(__($status_title))]) }}</option>
            @endforeach
            <option value="delete">{{ __('lists.actions.delete') }}</option>
        </select>
        <div class="input-group-append">
            <button class="btn btn-info btn-icon px-4" type="submit">{{ __('Apply') }}</button>
        </div>
    </div>
</form>
</div>
