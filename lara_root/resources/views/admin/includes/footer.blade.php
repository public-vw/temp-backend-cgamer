<footer class="main-footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 copy-right" >
                {{ date('Y') }} &copy; {{ __('FastHomeFinder by') }} <a href="https://datarivers.org" target="_blank">{{ __('Datarivers Team') }}</a>
            </div>
            <div class="col-md-6">
                <div class="text-md-right footer-links d-none d-sm-block">
                    <a href="{{ 'javascript:;' }}" target="_blank"><i class="fa fa-question-circle-o"></i>&nbsp;{{ __('Support Center') }}</a>
                </div>
            </div>
        </div>
    </div>
</footer>
