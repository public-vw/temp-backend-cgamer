{{-- TODO change it to use for update pages --}}
<div class="panel">
    <div class="panel-title">
        <strong>
            {{ __("Seo Manager") }}
        </strong>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">
                        {{ __("Allow search engines to show this service in search results?") }}
                    </label>

                    @compo("$VIEW_ROOT.components.dropdowns.select",[
                        "name" => 'seo_index',
                        'key_field' => 'id',
                        'value_field' => 'id',
                        'title_fields' => 'title',
                        'options' => [
                        [
                            "id" => '1',
                            "title" => __('Yes'),
                        ],
                        [
                        "id" => '0',
                        "title" => __('No'),
                        ]
                        ],
                    ])

                </div>
            </div>
        </div>
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#seo_1">{{ __("General Options") }}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#seo_2">{{ __("Share Facebook") }}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#seo_3">{{ __("Share Twitter") }}</a>
            </li>
        </ul>
        <div class="tab-content">

            @compo("$VIEW_ROOT.components.combinations.seo_tab", [
                "id" => 'seo_1',
                "active" => true,
                "textLabel" => __("Seo Title"),
                "textName" => 'seo_title',
                "textPlaceholder" => __("Leave blank to use service title"),
                "textareaLabel" => __("Seo Description"),
                "textareaName" => 'seo_desc',
                "textareaPlaceholder" => __("Enter description..."),
                "imageLabel" => __("Featured Image"),
                "imageName" => 'seo_image',
            ])

            @compo("$VIEW_ROOT.components.combinations.seo_tab", [
                "id" => 'seo_2',
                "textLabel" => __("Facebook Title"),
                "textName" => 'seo_share[facebook][title]',
                "textPlaceholder" => __("Enter title..."),
                "textareaLabel" => __("Facebook Description"),
                "textareaName" => 'seo_share[facebook][desc]',
                "textareaPlaceholder" => __("Enter description..."),
                "imageLabel" => __("Facebook Image"),
                "imageName" => 'seo_share[facebook][image]',
            ])

            @compo("$VIEW_ROOT.components.combinations.seo_tab", [
                "id" => 'seo_3',
                "textLabel" => __("Twitter Title"),
                "textName" => 'seo_share[twitter][title]',
                "textPlaceholder" => __("Enter title..."),
                "textareaLabel" => __("Twitter Description"),
                "textareaName" => 'seo_share[twitter][desc]',
                "textareaPlaceholder" => __("Enter description..."),
                "imageLabel" => __("Twitter Image"),
                "imageName" => 'seo_share[twitter][image]',
            ])

        </div>
    </div>
</div>
