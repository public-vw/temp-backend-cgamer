@php($user = Auth::user())

<div class="header-logo flex-shrink-0">
    <h3 class="logo-text"><a href="{{ route("$VIEW_ROOT.home") }}">{{ config('app.name') }} <span class="app-version">{{ config('app.version') }}</span></a></h3>
</div>
<div class="header-widgets d-flex flex-grow-1">
    <div class="widgets-left d-flex flex-grow-1 align-items-center">
        <div class="header-widget">
            <span class="btn-toggle-admin-menu btn btn-sm btn-link"><i class="icon ion-ios-menu"></i></span>
        </div>
    </div>
    <div class="widgets-right flex-shrink-0 d-flex">
        <div class="dropdown header-widget widget-user">
            <div data-toggle="dropdown" class="user-dropdown d-flex align-items-center" aria-haspopup="true" aria-expanded="false">
                <span class="user-avatar flex-shrink-0">
                    @if($user->avatar_url):
                    <img class="image-responsive" src="{{ $user->avatar_url }}" alt="{{ $user->name }}">
                    @else
                        <span class="avatar-text" style="
                            background-color: {{ config("enums.random_colors.{$user->random_color}")[0] }};
                            color: {{ config("enums.random_colors.{$user->random_color}")[1] }}"
                        >{{ ucwords(substr($user->name, 0, 1)) }}</span>
                    @endif
                </span>
                <div class="user-info flex-grow-1">
                    <div class="user-name">{{ $user->name }}</div>
                    <div class="user-role">{{ ucfirst($VIEW_ROOT ?? '') }} Panel</div>
                </div>
                <i class="fa fa-angle-down" style="padding-top: 18px;"></i>
            </div>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="{{ route('public.homepage') }}" target="_blank"><i class="fa fa-home"></i>&nbsp;{{ __('Home') }}&nbsp;<div style="display:inline-block; font-size: 0.5rem; color:green;"><i class="fa fa-external-link"></i></div></a>
                <hr/>

                {{--TODO create admin profile edit route--}}
                <a class="dropdown-item" href="{{ route('admin.profile.edit') }}"><i class="fa fa-user"></i>&nbsp;{{ __('Edit Profile') }}</a>

                {{--TODO create admin change password route--}}
                <a class="dropdown-item" href="{{ route('admin.profile.show_password') }}"><i class="fa fa-key"></i>&nbsp;{{ __('Change Password') }}</a>
                <hr/>
                @multirole
                @foreach(auth()->user()->allowedPanels() as $panel)
                    @if(Route::has("$panel.home"))
                        <a class="dropdown-item" target="_blank" href="{{ route("$panel.home") }}">
                            <i class="fa fa-location-arrow"></i>&nbsp;{{ucwords($panel)}} Panel <i class="fa fa-external-link"></i>
                        </a>
                    @else
                        <a class="dropdown-item disabled" disabled="disabled" href="javascript:;">
                            <i class="fa fa-location-arrow"></i>&nbsp;{{ucwords($panel)}} Panel
                        </a>
                    @endif
                @endforeach

                <hr/>
                @endmultirole

                <a class="dropdown-item" href="{{ route('auth.logout.get') }}"><i class="fa fa-sign-out"></i>&nbsp;{{ __('Logout') }}
                </a>
            </div>
        </div>
    </div>
</div>

