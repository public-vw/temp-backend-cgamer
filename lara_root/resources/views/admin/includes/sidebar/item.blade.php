<li @if(!empty($menuItem->class))class="{{ $menuItem->class }}"@endif>
    <a href="{{ $menuItem->hasChild()?'javascript:;':$menuItem->href }}">
        @if(!empty($menuItem->icon))
            <span class="icon text-center">
                <i class="{{ $menuItem->icon }}"></i>
            </span>
        @endif
        {{ $menuItem->title }}
    </a>
    @if($menuItem->hasChild())
        <span class="btn-toggle">
            <i class="fa fa-angle-right pull-right"></i>
        </span>
        <ul class="children">
            @foreach($menuItem->children as $menuSubItem)
                @include("$VIEW_ROOT.includes.sidebar.subitem",['menuItem' => $menuSubItem])
            @endforeach
        </ul>
    @endif
</li>
