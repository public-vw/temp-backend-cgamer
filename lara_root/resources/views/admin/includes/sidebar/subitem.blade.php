<li @if(!empty($menuItem->class))class="{{ $menuItem->class }}"@endif>
    @if($menuItem->hasChild())
        <div class="text-center" style="padding-left:10px 15px 10px; background-color: #0b3cb8">
            @if(!empty($menuItem->icon))
                <span class="icon text-center">
                <i class="{{ $menuItem->icon }}"></i>
            </span>
            @endif
            {{ $menuItem->title }}
        </div>
        <ul class="children">
            @foreach($menuItem->children as $menuSubItem)
                @if(is_null($menuSubItem->title))
                    <hr>
                    @continue
                @endif

                @include("$VIEW_ROOT.includes.sidebar.subitem",['menuItem' => $menuSubItem])
            @endforeach
        </ul>
    @else
        <a href="{{ $menuItem->href }}">
            @if(!empty($menuItem->icon))
                <span class="icon text-center">
                <i class="{{ $menuItem->icon }}"></i>
            </span>
            @endif
            {{ $menuItem->title }}
        </a>
    @endif
</li>
