<?php

/*#TODO create permissions
$menus = [
    "$VIEW_ROOT"=>[
        'url'      => route("$VIEW_ROOT.home"),
        'title'    => __("Dashboard"),
        'icon'     => 'icon ion-ios-arrow-forward',
        'position' => 0
    ],
    'agency'=>[
        'url'      => route("$VIEW_ROOT.agencies.index"),
        'title'    => __("Agencies"),
        'icon'     => 'icon ion-ios-arrow-forward',
        'position' => 45,
    ],
    'country'=>[
        'url'      => route("$VIEW_ROOT.countries.index"),
        'title'    => __("Country"),
        'icon'     => 'icon ion-ios-arrow-forward',
        'position' => 50,
    ],
    'province'=>[
        'url'      => route("$VIEW_ROOT.provinces.index"),
        'title'    => __("Province"),
        'icon'     => 'icon ion-ios-arrow-forward',
        'position' => 55,
    ],
    'city'=>[
        'url'      => route("$VIEW_ROOT.cities.index"),
        'title'    => __("City"),
        'icon'     => 'icon ion-ios-arrow-forward',
        'position' => 60,
    ],
    'region'=>[
        'url'      => route("$VIEW_ROOT.regions.index"),
        'title'    => __("Region"),
        'icon'     => 'icon ion-ios-arrow-forward',
        'position' => 65,
    ],
    'contactAddress'=>[
        'url'      => route("$VIEW_ROOT.contact_addresses.index"),
        'title'    => __("Contact Address"),
        'icon'     => 'icon ion-ios-arrow-forward',
        'position' => 70,
    ],
    'contactPhone'=>[
        'url'      => route("$VIEW_ROOT.contact_phones.index"),
        'title'    => __("Contact Phone"),
        'icon'     => 'icon ion-ios-arrow-forward',
        'position' => 75,
    ],
    'contactEmail'=>[
        'url'      => route("$VIEW_ROOT.contact_emails.index"),
        'title'    => __("Contact Email"),
        'icon'     => 'icon ion-ios-arrow-forward',
        'position' => 80,
    ],
    'socialChannel'=>[
        'url'      => route("$VIEW_ROOT.social_channels.index"),
        'title'    => __("Social Channel"),
        'icon'     => 'icon ion-ios-arrow-forward',
        'position' => 85,
    ],
    'socialLink'=>[
        'url'      => route("$VIEW_ROOT.social_links.index"),
        'title'    => __("Social Link"),
        'icon'     => 'icon ion-ios-arrow-forward',
        'position' => 90,
    ],
    'attachmentType'=>[
        'url'      => route("$VIEW_ROOT.attachment-types.index"),
        'title'    => __("Attachment Type"),
        'icon'     => 'icon ion-ios-arrow-forward',
        'position' => 95,
    ],
    'attachment'=>[
        'url'      => route("$VIEW_ROOT.attachments.index"),
        'title'    => __("Attachment"),
        'icon'     => 'icon ion-ios-arrow-forward',
        'position' => 100,
    ],
    'log'=>[
        'url'      => route("$VIEW_ROOT.logs.index"),
        'title'    => __("Log"),
        'icon'     => 'icon ion-ios-arrow-forward',
        'position' => 105,
    ],
    'menu'=>[
        'url'      => route("$VIEW_ROOT.menus.index"),
        'title'    => __("Menu"),
        'icon'     => 'icon ion-ios-arrow-forward',
        'position' => 110,
    ],
    'seoPosition'=>[
        'url'      => route("$VIEW_ROOT.seo_positions.index"),
        'title'    => __("Seo Position"),
        'icon'     => 'icon ion-ios-arrow-forward',
        'position' => 115,
    ],
    'seoDetail'=>[
        'url'      => route("$VIEW_ROOT.seo_details.index"),
        'title'    => __("Seo Detail"),
        'icon'     => 'icon ion-ios-arrow-forward',
        'position' => 120,
    ],
    'settingGroup'=>[
        'url'      => route("$VIEW_ROOT.setting_groups.index"),
        'title'    => __("Setting Group"),
        'icon'     => 'icon ion-ios-arrow-forward',
        'position' => 125,
    ],
    'setting'=>[
        'url'      => route("$VIEW_ROOT.settings.index"),
        'title'    => __("Setting"),
        'icon'     => 'icon ion-ios-arrow-forward',
        'position' => 130,
    ],
    'user'=>[
        'url'      => route("$VIEW_ROOT.users.index"),
        'title'    => __("User"),
        'icon'     => 'icon ion-ios-arrow-forward',
        'position' => 135,
    ],
    'starRate'=>[
        'url'      => route("$VIEW_ROOT.star_rates.index"),
        'title'    => __("Star Rate"),
        'icon'     => 'icon ion-ios-arrow-forward',
        'position' => 140,
    ],
    'review'=>[
        'url'      => route("$VIEW_ROOT.reviews"),
        'title'    => __("Reviews"),
        'icon'     => 'icon ion-ios-arrow-forward',
        'position' => 200,
    ],
];*/

$menus = $menus['sidebar']->menus;

$user = auth()->user();
if (!empty($menus)){
    foreach ($menus as $k => $menuItem) {

# TODO: manage permissions
//        if (!empty($menuItem['permission']) and !$user->hasPermissionTo($menuItem['permission'])) {
//            unset($menus[$k]);
//            continue;
//        }

        $activeChild = false;
        $hasChildren = false;

        if (!empty($menuItem['children'])) {
            $hasChildren = true;

            foreach ($menuItem['children'] as $k2 => $menuItem2) {
# TODO: manage permissions
//                if (!empty($menuItem2['permission']) and !$user->hasPermissionTo($menuItem2['permission'])) {
//                    unset($menus[$k]['children'][$k2]);
//                    continue;
//                }

                $menus[$k]['children'][$k2]['class'] = $menuItem2->isCurrent() ? 'active' : '';
            }
        }

        $menus[$k]['class'] = $menuItem->isCurrent() ? 'active' : '';
//        $menus[$k]['class'] = ($currentUrl == $menuItem['href'] || $activeChild || strpos($currentUrl, $menuItem['href']) !== false) ? 'active' : '';
        $menus[$k]['class'] .= $hasChildren ? ' has-children' : '';
    }

//    //@todo Sort Menu by Position
//    $menus = array_values(Arr::sort($menus, function ($value) {
//        return $value['position'] ?? 100;
//    }));
}

?>

{{--@dd($menus)--}}
<ul class="main-menu">
    @foreach($menus as $menuItem)
        @if(is_null($menuItem->title))
            <hr>
            @continue
        @endif

        @include("$VIEW_ROOT.includes.sidebar.item",['menuItem' => $menuItem])
    @endforeach
</ul>

@push('css')
    <style>
        @media all and (min-width: 992px) {

            .main-menu li{ position: relative; }
            .main-menu li .children {
                position: absolute;
                left:100%;
                top:-7px;
            }
            .main-menu li:hover > .children{
                display: block;
                border: 1px solid rgba(0, 0, 0, 0.15);
            }
        }
    </style>
@endpush
