@pushonce('styles:select_box')
    <style>
        .notActive{
            color:silver;
        }
        .selitem img{
            max-width: 100%;
        }
        .selitem{
            cursor: pointer;
            border:2px solid transparent;
        }
        .selitem:hover{
            border:2px solid #f1f1f1;
        }
        .selected{
            border:2px solid #f92!important;
        }
    </style>
@endpushonce
@pushonce('scripts:select_box')
    <script>
        $(function(){
            $('.selitem').click(function(){
                $('#{{$NAME}}').val($(this).attr('data-val'));
                $('.selitem').removeClass('selected');
                $(this).addClass('selected');
            });
        });
    </script>    
@endpushonce

<div class="form-group{{ $errors->has($NAME) ? ' has-error' : '' }} @if(isset($HIDDEN) && $HIDDEN) hidden @endif @if(isset($CLASS) && $CLASS) {{$CLASS}} @endif">
    <label class="col-md-4 control-label">{{$TITLE}}</label>
    <div class="col-md-6" style=" padding: 0 30px 0;">
        <input type="hidden" id="{{$NAME}}" name="{{$NAME}}" value="{{ $act_res[$NAME] or old($NAME) }}"/>
        <div class="row" style="height:200px; overflow-y: auto; border: 1px solid silver;padding: 10px">
            @if(isset($IMAGES))
                @foreach($IMAGES as $opt)
                <div data-val="{{$opt->id}}" class="col-md-2 selitems selitem text-center @if(isset($act_res) && $act_res[$NAME] == $opt->id) selected @endif">
                    <img src="{{$opt->url}}" height="50"/>
                </div>
                @endforeach
            @endif
        </div>
        <div class="description">{{$DESCRIBE}}</div>
        @if ($errors->has($NAME))
        <span class="help-block">
            <strong>{{ $errors->first($NAME) }}</strong>
        </span>
        @endif
    </div>
</div>
