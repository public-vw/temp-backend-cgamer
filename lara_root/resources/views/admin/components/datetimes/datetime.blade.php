@extends("$VIEW_ROOT.layouts.component")

@pushonce('css:persian-datepicker')
<link href="{{ asset('vendor/persian-datepicker/persian-datepicker.min.css') }}" rel="stylesheet" type="text/css"/>
@endpushonce

@pushonce('js:persian-datepicker')
<script src="{{ asset('vendor/persian-datepicker/persian-date.min.js') }}"></script>
<script src="{{ asset('vendor/persian-datepicker/persian-datepicker.min.js') }}"></script>

<script>
  $('.persian-datepicker').persianDatepicker({
    initialValueType: 'persian',
    format: 'YYYY-MM-DD HH:mm:ss',
    initialValue: false,
    timePicker: {
      enabled: true,
    }
  })
</script>
@endpushonce

@section('input-'.$rnd)
    <input
           readonly="readonly"
           class="form-control persian-datepicker m-input @error($NAME) is-invalid @enderror {{ $CLASS ?? '' }}"
           style="direction: {{ $dir ?? 'ltr' }}"
           id="{{ $NAME }}" name="{{ $NAME }}"
           value="{{ $VALUE ?? old($NAME, $DEFAULT ?? '') }}"
        placeholder="{{ $placeholder ?? '' }}"
           {{ $REQUIRED ? 'required' : '' }}
            {!! $MORE ?? '' !!}
    />
@endsection
