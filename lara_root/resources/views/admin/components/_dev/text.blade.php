@extends("$VIEW_ROOT.layouts.component")

@if(isset($alias) && preg_match('/mynumeric|.*/',$alias) && 0)
    @php
        $data = explode('|',$alias)
    @endphp

    @pushonce("stack_js:{$alias}")
    <script>
      Inputmask.extendAliases({
        "{{$alias}}": {
          mask: 'n{' + '{{$data[1]}}' + '}',
        }
      })
    </script>
    @endpushonce
@endif

@section('input-'.$rnd)
    <input style="{{ !empty($direction) ? "direction: {$direction}" : '' }}"
           type="text" class="form-control @if(isset($disable) && $disable) disabled @endif"
           id="{{ $NAME }}" @if(!isset($disable) || !$disable) name="{{ $NAME }}" @endif
           value="{{ $value ?? old($NAME, $default ?? '') }}"
           @if(!isset($disable) || !$disable)
               @if(isset($alias))data-inputmask-alias="{{$alias}}" @endif
               @if(isset($required) && $required) required @endif
               placeholder="{{$placeholder ?? ''}}"
           @else
               readonly='readonly'
               disabled='disabled'
           @endif
           {!! $more ?? '' !!}
    />
@endsection
