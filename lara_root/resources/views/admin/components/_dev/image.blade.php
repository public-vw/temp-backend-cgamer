@extends("$VIEW_ROOT.layouts.component")

@section('input-'.$rnd)
    <input type="file" class="form-control"
           id="{{ $NAME }}" name="{{ $NAME }}"
           @if(isset($required) && $required) required @endif
            {!! $more ?? '' !!}
    />
@endsection
