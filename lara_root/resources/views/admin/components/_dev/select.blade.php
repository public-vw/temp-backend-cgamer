@extends("$VIEW_ROOT.layouts.component")

@section('input-'.$rnd)
    <select class="form-control" id="{{ $NAME }}" name="{{ $NAME }}" {!! $more ?? '' !!}>
        @if(!empty($options))
            @if(isset($null_value))
                <option value="">
                    {{$null_value ?? ''}}
                </option>
            @endif

            @foreach($options as $item)
                @php($titles = implode(' ', array_intersect_key(is_array($item) ? $item : $item->toArray(), array_fill_keys(explode('|', $title_fields), ''))))

                @if (!empty(old($NAME, $value ?? '')))
                    <option {{ old($NAME, $value ?? '') == $item[$value_field] ? 'selected="selected"' : '' }}
                            value="{{ $item[$value_field] }}">
                        {{ $titles }}
                    </option>
                @elseif (!empty($current_value))
                    <option {{ $current_value == $item[$value_field] ? 'selected="selected"' : '' }}
                            value="{{ $item[$value_field] }}">
                        {{ $titles }}
                    </option>
                @else
                    <option value="{{ $item[$value_field] }}">
                        {{ $titles }}
                    </option>
                @endif
            @endforeach
        @endif
    </select>
@endsection
