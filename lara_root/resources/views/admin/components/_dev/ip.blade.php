@extends("$VIEW_ROOT.layouts.component")

@pushonce('stack_js:jquery-mask')
<script src="{{ asset('vendor/jquery-mask/jquery.mask.min.js') }}"></script>

<script>
  $('.ip_address').mask('099.099.099.099')
</script>
@endpushonce

@section('input-'.$rnd)
    <input type="text" class="form-control ip_address"
           id="{{ $NAME }}" name="{{ $NAME }}"
           value="{{ $value ?? old($NAME, $default ?? '') }}"
           @if(isset($required) && $required) required @endif
           placeholder="{{ $placeholder ?? '' }}"
            {!! $more ?? '' !!}
    />
@endsection
