@extends("$VIEW_ROOT.layouts.component")

@section('input-'.$rnd)
    <span id="{{ $NAME }}"
           {!! $more ?? '' !!}
    >{{ $value ?? '' }}</span>
@endsection
