<div class="col" @isset($HREF) onclick="window.location='{{ $HREF }}';" @endisset>
    <div class="dashboard-report-card card {{ $BGCOLOR_CLASS }}">
        <div class="card-content">
            <span class="card-title">{{ $TITLE }}</span>
            @foreach($CONTENTS as $content)
                <div><span class="card-amount">{{ $content['value'] }}</span>&nbsp;{{ $content['title'] }}</div>
            @endforeach
{{--            <span class="card-desc">{{ $DESC }}</span>--}}
        </div>
        <div class="card-media">
            <i class="{{ $ICON }}"></i>
        </div>
    </div>
</div>
