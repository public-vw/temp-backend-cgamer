@extends("$VIEW_ROOT.layouts.component")

@section('input-'.$rnd)
    <div @isset($SWITCH) class="custom-control custom-switch" @endisset>
        <input
            name="{{ $NAME }}"
            id="{{ $ID ?? $NAME }}"
            type="checkbox"

            value="{{ $VALUE ?? '1' }}"
            {{ ((isset($CHECKED) && $CHECKED) || old($NAME, $DEFAULT ?? '') == $NAME)  ? 'checked' : '' }}

            @if(isset($CLASS)) class="{{ $CLASS }}" @endif

            @if(!isset($DISABLE) || !$DISABLE)
                @if(isset($REQUIRED) && $REQUIRED) required @endif
                placeholder="{{$PLACEHOLDER ?? ''}}"
            @else
                readonly='readonly'
                disabled='disabled'
            @endif
            @isset($ONCLICK) onclick="{!! $ONCLICK !!}" @endisset

            {!! $MORE ?? '' !!}
        />
        <label @isset($SWITCH) class="custom-control-label" for="{{ $ID ?? $NAME }}" @endisset>{!! $TITLE !!}</label>
    </div>
@endsection
