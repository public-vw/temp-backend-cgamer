<a
    href="{{ $HREF ?? 'javascript:;' }}"
    type="{{ $TYPE ?? 'button' }}"
    class="btn {{ $CLASS ?? 'btn-outline-info' }}{{ $ICON ?? '' }} btn-block"
    style="font-size:16px; line-height: 40px;"
    @if(isset($ONCLICK)) onclick="{!! $ONCLICK !!}" @endif
>{!! $TITLE ?? 'Untitled' !!}</a>
