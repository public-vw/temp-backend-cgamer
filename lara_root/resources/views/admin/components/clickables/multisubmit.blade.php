@pushonce('js:multi_submit')
<script>
    $(function(){
        $('.act-btn-{{$rnd}}').on('click',function(){
            $('#after_action').val($(this).attr('data-act'));
            @isset($TARGET_FORM)
                $('#{{$TARGET_FORM}}').submit();
            @else
                $(this).closest('form').submit();
            @endisset
        })
    })
</script>
@endpushonce

@compo("$VIEW_ROOT.components.textfields.hidden",[
    "NAME"  => 'after_action',
    "VALUE" => array_keys($DEFAULT)[0],
])
<div class="btn-group">
    @foreach($DEFAULT as $action => $title)
        <button type="button" class="act-btn-{{$rnd}} btn {{ $BTNCLASS ?? 'btn-success' }}" data-act="{{ $action }}">{!!  $title !!}</button>
    @endforeach
    <button type="button" class="btn {{ $BTNCLASS ?? 'btn-success' }} dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <span class="sr-only">Toggle Dropdown</span>
    </button>
    <div class="dropdown-menu dropdown-menu-right">
        @foreach($ITEMS as $action => $title)
            @if($title)
                <a class="act-btn-{{$rnd}} dropdown-item" href="javascript:;" data-act="{{ $action }}">{!! $title !!}</a>
            @else
                <div class="dropdown-divider"></div>
            @endif
        @endforeach
    </div>
</div>
