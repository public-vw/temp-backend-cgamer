<form action="{{ route("$VIEW_ROOT.$TABLE.destroy", $VARIABLE) }}" method="post" class="d-inline">
    @csrf
    @method('DELETE')
    {{-- TODO FIX CLASS --}}
    @compo("$VIEW_ROOT.components.clickables.link", [
        "ONCLICK"  => "if(confirm('".__('Are you sure?')."'))$(this).closest('form').submit();",
        "TITLE"    => $TITLE ?? "<i class='fa fa-trash-o'></i>",
        "CLASS"    => $CLASS ?? 'btn btn-sm pull-right btn-danger',
        "RAW_HTML" => true,
    ])
</form>
