@compo("$VIEW_ROOT.components.clickables.link", [
    "URL"       => route("$VIEW_ROOT.$TABLE.edit", $VARIABLE),
    "TITLE"     => $TITLE ?? "<i class='fa fa-edit'></i>",
    "CLASS"     => $CLASS ?? 'btn btn-sm btn-info mr-1',
    "RAW_HTML"  => true,
])
