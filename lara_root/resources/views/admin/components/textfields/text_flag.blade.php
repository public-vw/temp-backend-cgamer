@extends("$VIEW_ROOT.layouts.component")

@pushonce('js:flag')
    <script>
        $('#{{ $id ?? $NAME }}').on('input', function () {
            var flag = $(this).val();
            $('.flag-icon').attr('class', 'display-4 flag-icon flag-icon-' + flag);
        })
    </script>
@endpushonce

@section('input-'.$rnd)
    <input
        id="{{ $id ?? $NAME }}"
        name="{{ $NAME }}"
        type="{{ $type ?? 'text' }}"
        class="form-control m-input @error($NAME) is-invalid @enderror {{ $more_class ?? '' }}"
        value="{{ $value ?? old($NAME, $default ?? '') }}"
        autocomplete=@if(!isset($autocomplete) || $autocomplete) "{{ config('app.name', 'Laravel') }}_{{ $NAME }}" @else "off" @endif

        @if(!isset($disable) || !$disable)
            @if(isset($required) && $required) required @endif
            placeholder="{{$placeholder ?? ''}}"
        @else
            readonly='readonly'
            disabled='disabled'
        @endif
        {!! $more ?? '' !!}
    />

    <br/>
    <span class="display-4 flag-icon flag-icon-{{ $value ?? old($NAME, $default ?? '') }}"></span>
@endsection
