@extends("$VIEW_ROOT.layouts.component")

@push('js')
    <script>
        $(function(){
            $('[data-tag="dropbtn-{{$rnd}}"]').on('click',function(){
                let mainbtn = $('#dropmainbtn-{{$rnd}}');
                let curlangid = $(mainbtn).attr('data-show');
                let newlangid = $(this).attr('data-lang');

                $(mainbtn).html($(this).html()).attr('data-show',newlangid);
                $('[data-tag="dropinp-{{$rnd}}"][data-lang="'+curlangid+'"]').hide();
                $('[data-tag="dropinp-{{$rnd}}"][data-lang="'+newlangid+'"]').show();

                $('[data-tag="dropbtn-{{$rnd}}"][data-lang="'+newlangid+'"]').hide();
                $('[data-tag="dropbtn-{{$rnd}}"][data-lang="'+curlangid+'"]').show();
            })
        })
    </script>
@endpush

@php($currentLang = \App\Models\Language::getCurrent())
@section('input-'.$rnd)
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <button id="dropmainbtn-{{$rnd}}"
                    data-show="{{$currentLang->id}}"
                    class="btn btn-outline-secondary dropdown-toggle"
                    type="button"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false">
                <span class="display-5 flag-icon flag-icon-{{ $currentLang->flag_iso }}"></span>
                {{ $currentLang->title }}</button>
            <div class="dropdown-menu">
                @foreach(\App\Models\Language::getList() as $lang)
                    <a
                        class="dropdown-item"
                        data-tag="dropbtn-{{$rnd}}"
                        data-lang="{{$lang->id}}"
                        @if($lang->id == $currentLang->id)
                        style=" display:none; "
                        @endif
                        href="javascript:;">
                        <span class="display-5 flag-icon flag-icon-{{ $lang->flag_iso }}"></span>
                        {{ $lang->title }}</a>
                @endforeach
            </div>
        </div>
        @foreach(\App\Models\Language::getList() as $lang)
            <input
                id="{{ $ID ?? $NAME }}-{{ $lang->code }}"
                data-tag="dropinp-{{$rnd}}"
                data-lang="{{$lang->id}}"
                name="{{ $NAME }}[{{ $lang->code }}]"
                type="text"
                style="
                @if($lang->id != $currentLang->id)
                 display:none;
                @endif
                 direction:{{$lang->direction}}"
                class="form-control @error($NAME) is-invalid @enderror"
                value="{{ $VALUE["{$NAME}.{$lang->code}"] ?? old( $DEFAULT ?? '') }}"
                autocomplete=@if(!isset($AUTOCOMPLETE) || $AUTOCOMPLETE) "{{ config('app.name', 'Laravel') }}_{{ $NAME }}_{{ $lang->code }}" @else "off" @endif

            @if(!isset($DASABLE) || !$DASABLE)
                @if(isset($REQUIRED) && $REQUIRED) required @endif
                placeholder="{{$PLACEHOLDER ?? ''}}"
            @else
                readonly='readonly'
                disabled='disabled'
            @endif
            {!! $MORE ?? '' !!}
            />
        @endforeach
    </div>


@endsection
