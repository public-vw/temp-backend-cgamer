@extends("$VIEW_ROOT.layouts.component")

@section('input-'.$rnd)
        @foreach(\App\Models\Language::getList() as $lang)
            <div>
                <b>{{ $lang->title }} : </b>
                <input
                        id="{{ $ID ?? $NAME }}-{{ $lang->code }}"
                        name="{{ $NAME }}[{{ $lang->code }}]"
                        type="text"
                        class="form-control @error($NAME) is-invalid @enderror"
                        value="{{ $VALUE["{$NAME}.{$lang->code}"] ?? old( $DEFAULT ?? '') }}"
                        autocomplete=@if(!isset($AUTOCOMPLETE) || $AUTOCOMPLETE) "{{ config('app.name', 'Laravel') }}_{{ $NAME }}_{{ $lang->code }}" @else "off" @endif

                @if(!isset($DASABLE) || !$DASABLE)
                    @if(isset($REQUIRED) && $REQUIRED) required @endif
                    placeholder="{{$PLACEHOLDER ?? ''}}"
                @else
                    readonly='readonly'
                    disabled='disabled'
                @endif
                {!! $MORE ?? '' !!}
                />
            </div>
        @endforeach


@endsection
