@component("$VIEW_ROOT.components.panel")
    @slot('title',$title)
    @slot('body')
        <div class="form-group">
            <div class="form-controls">
                <div class="form-group-item">
                    <div class="form-group-item">
                        <div class="g-items-header">
                            <div class="row">
                                @foreach($headers as $header)
                                    <div class="col">{{ $header }}</div>
                                @endforeach
                                <div class="col"></div>
                            </div>
                        </div>
                        <div class="g-items">
                            @foreach($items as $item)
                                <div class="item">
                                    <div class="row">
                                        @foreach($headers as $key => $header)
                                            <div class="col">
                                                {{ view("$VIEW_ROOT.items.$table.inline_section.$key")->withVariable($item) }}
                                            </div>
                                        @endforeach
                                        <div class="col-auto">
                                            {{-- TODO fix delete button --}}
                                            @compo("$VIEW_ROOT.components.actions.delete", [
                                                "VARIABLE" => $item,
                                                "TABLE"    => $table,
                                            ])

                                            @compo("$VIEW_ROOT.components.actions.edit", [
                                                "VARIABLE" => $item,
                                                "TABLE"    => $table,
                                            ])
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="text-right">
                            {{-- TODO: check if better to use session --}}
                            @compo("$VIEW_ROOT.components.clickables.link", [
                                "URL"       => route("$VIEW_ROOT.$table.create", ['morph-type' => $morph['type'], 'morph-id' => $morph['id']]),
                                "TITLE"     => '<i class="icon ion-ios-add-circle-outline"></i> ' . __("Add item"),
                                "CLASS"     => 'btn btn-info btn-sm btn-add-item',
                                "RAW_HTML"  => true,
                                "TARGET"    => '_blank',
                            ])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endslot
@endcomponent
