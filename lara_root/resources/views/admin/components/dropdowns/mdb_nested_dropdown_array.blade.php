<input type="hidden" name="{{ $NAME }}" value="{{ $VALUE ?? '' }}">
<div class="dropdown">
    <button
        class="btn btn-primary dropdown-toggle"
        type="button"
        id="dropdownMenuButton"
        data-mdb-toggle="dropdown"
        aria-expanded="false"
    >
        {{ $LABEL }}
    </button>
    @php
        mdb_nested_dropdown_fn($ITEMS ?? []);
        function mdb_nested_dropdown_fn($items, $isChild = false){
            echo '<ul class="dropdown-menu '.($isChild ? 'dropdown-submenu' : '').'"';
            echo ($isChild ? 'aria-labelledby="dropdownMenuButton"' : '');
            echo '>';

            foreach($items as $item){
                echo "<li>";
                echo "<a class='dropdown-item' href='#' data-id='{$item[0]}'>{$item[1]}</a>";
                if(isset($item[2])){echo mdb_nested_dropdown_fn($item[2], true);}
                echo "</li>";
            }

            echo '</ul>';
        }
    @endphp
</div>
