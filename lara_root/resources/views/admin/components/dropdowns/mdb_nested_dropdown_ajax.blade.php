@extends("$VIEW_ROOT.layouts.component")

@php( $separator = ' <i class="fa fa-arrow-right"></i> ' )

@pushonce('css:nested-select')
<style>
    .nested-select-wrapper {
        position: relative;
        background: #FFF;
        color: #2e2e2e;
        outline: none;
        cursor: pointer;
    }

    .nested-select-wrapper ul{
        padding-left: 10px;
    }

    .nested-select-wrapper ul.nested-select-submenu{
        padding-left: 0;
    }

    .nested-select-wrapper li span {
        width: 100%;
        display: block;
        padding: 5px;
        padding-top: 0;
    }

    .nested-select-wrapper li span.selected{
        background-color: #cca799;
    }

    .nested-select-wrapper li span:hover{
        background-color: #ede;
    }

    .nested-select-wrapper > span:after {
        content: "";
        width: 0;
        height: 0;
        position: absolute;
        right: 16px;
        top: calc(50% + 4px);
        margin-top: -6px;
        border-width: 6px 6px 0 6px;
        border-style: solid;
        border-color: #2e2e2e transparent;
    }

    .nested-select-wrapper .dropdown {
        position: absolute;
        display: none;
        z-index: 10;
        top: 100%;
        background-color: #f9f9f9;
        border: 1px solid #c9c9c9;
        left: 0;
        right: 0;
        height: 200px;
        margin: -1px;
        overflow-y: auto;
    }

    .nested-select-wrapper .dropdown li {
        display: block;
        text-decoration: none;
        color: #2e2e2e;
        padding: 5px;
        cursor: pointer;
    }

    .nested-select-wrapper .dropdown li > span.color-box {
        padding: 0 12px;
        margin-right: 5px;
    }

    .nested-select-wrapper .dropdown li:hover {
        background: #f9f9f9;
        cursor: pointer;
    }
</style>
@endpushonce

@pushonce('js:nested-select')
<script>
    let separator = '{!! $separator !!}'
    $(function (){
        $('.nested-select-wrapper, .nested-select-wrapper > span.toggle-selected').click(function(e){
            e.stopPropagation()
            $(this).closest('.nested-select-wrapper').find('.dropdown').slideToggle(100);
        });

        $('.nested-select-wrapper').on('click','.nested-select-item',function(e) {
            e.stopPropagation()
            $(this).closest('.dropdown').slideUp(100);
            var box = $(this).closest('.nested-select-wrapper').find('.toggle-selected:first');
            var input = $(this).closest('.nested-select-wrapper').find('input:first');

            let data = $(this).text().replace(/[\.\s]+$/, "").replace(/^[\.\s]+/, "");
            let parent = $(this).closest('ul').closest('li')
            while(parent.length > 0){
                title = $(parent).find('span:first').text().replace(/[\.\s]+$/, "").replace(/^[\.\s]+/, "")
                data = title + separator + data;
                parent = $(parent).closest('ul').closest('li')
            }

            $(box).html(data);
            $(input).val($(this).attr('data-id'));
        });

        $.ajax({
            type: 'POST',
            url: '{{ $AJAX_URL }}',
            dataType: 'json',
            success: function (response) {
                response = JSON.parse(response)

                html = mdb_nested_dropdown_fn(response)
                $('div.nested-select-wrapper').append(html)
            },
            error: function (err) {
                console.log(err)
            }
        })

        function mdb_nested_dropdown_fn(items, isChild = false, addBefore = '') {
            let menuItemsHTML = '';

            menuItemsHTML += '<ul class="nested-select' + (isChild ? ' nested-select-submenu' : ' dropdown') + '"';
            menuItemsHTML += '>';

            @if($CONFIG['allow_null'] ?? false)
                if(!isChild){
                    menuItemsHTML += "<li>";
                    menuItemsHTML += "<span class='nested-select-item' data-id=''>[ NULL ]</span>";
                    menuItemsHTML += "</li>";
                }
            @endif
            $.each(items, function (i,item) {
                menuItemsHTML += "<li>";
                menuItemsHTML += "<span class='nested-select-item' data-id='" + item['id'] + "'>" + addBefore + item['title'] + "</span>";
                if (typeof item['children'] != 'undefined') {
                    menuItemsHTML += mdb_nested_dropdown_fn(item['children'], true, addBefore + '... ');
                }
                menuItemsHTML += "</li>";
            });
            menuItemsHTML += '</ul>';
            return menuItemsHTML;
        }
    })
</script>
@endpushonce

@section('input-'.$rnd)
<div class="nested-select-wrapper form-control">
    <input type="hidden" name="{{ $NAME }}" value="{{ $VALUE['id'] ?? '' }}">
    <span class="nested-select-box toggle-selected">
        @if($VALUE['title'] ?? false)
                <span class="color-box">{!! implode($separator, $VALUE['title']) !!}&nbsp;</span>
            @else
                <span>Choose a {{ $LABEL }} ...</span>
            @endif
    </span>
</div>
@endsection
