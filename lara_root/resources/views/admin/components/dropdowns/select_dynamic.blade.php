@extends("$VIEW_ROOT.layouts.component")

@section('input-'.$rnd)
    <select class="form-control" id="{{ $ID ?? $NAME }}" name="{{ $NAME }}" {!! $MORE ?? '' !!}>
        @if(!empty($OPTIONS))
            @if(isset($NULL_VALUE))
                <option value="">
                    {{$NULL_VALUE ?? ''}}
                </option>
            @endif

            @foreach($OPTIONS as $item)
                @php($titles = implode(' ', array_intersect_key(is_array($item) ? $item : $item->toArray(), array_fill_keys(explode('|', $TITLE_FIELDS), ''))))

                @if (!empty(old($NAME, $VALUE ?? '')))
                    <option {{ old($NAME, $VALUE ?? '') == $item[$VALUE_FIELD] ? 'selected="selected"' : '' }}
                            value="{{ $item[$VALUE_FIELD] }}">
                        {{ $titles }}
                    </option>
                @elseif (!empty($CURRENT_VALUE))
                    <option {{ $CURRENT_VALUE == $item[$VALUE_FIELD] ? 'selected="selected"' : '' }}
                            value="{{ $item[$VALUE_FIELD] }}">
                        {{ $titles }}
                    </option>
                @else
                    <option value="{{ $item[$VALUE_FIELD] }}">
                        {{ $titles }}
                    </option>
                @endif
            @endforeach
        @endif
    </select>
@endsection
