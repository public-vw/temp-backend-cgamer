@extends("$VIEW_ROOT.layouts.component")
{{-- TODO fix responsive select --}}
@push("js")
    <script>
        $('#{{ $NAME }}').select2({
            placeholder: "{{ $placeholder }}",
            ajax: {
                url: "{{ $ajaxUrl }}",
                dataType: 'json',
                data: function (params) {
                    return {
                        @if(isset($depends))
                            depends_val: $('#{{ $depends }}').val(),
                        @endif
                        search: params.term
                    };
                },
                processResults: function (data) {
                    return {
                        results:  $.map(data, function (item) {
                            return {
                                text: item.title,
                                @if(isset($hasFlag) && $hasFlag)
                                    flag: item.flag_iso,
                                @endif
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            },
            templateResult: formatState,
            allowClear: true,
            theme: 'classic'
        });

        function formatState (item) {
            var format ='<h5>' +
                @if(isset($hasFlag) && $hasFlag)
                    '<span class="flag-icon flag-icon-' + item.flag + '"></span>' +
                @endif
                '&nbsp;' + item.text +
                '</h5>';
            return $(format);
        }
    </script>
@endpush

@section('input-'.$rnd)
    <select class="form-control" id="{{ $id ?? $NAME }}" name="{{ $NAME }}" {!! $more ?? '' !!}>
        @if(!empty($options))
            @if(isset($null_value))
                <option value="">
                    {{$null_value ?? ''}}
                </option>
            @endif

            @foreach($options as $item)
                @php($titles = implode(' ', array_intersect_key(is_array($item) ? $item : $item->toArray(), array_fill_keys(explode('|', $title_fields), ''))))

                @if (!empty(old($NAME, $value ?? '')))
                    <option {{ old($NAME, $value ?? '') == $item[$value_field] ? 'selected="selected"' : '' }}
                            value="{{ $item[$value_field] }}">
                        {{ $titles }}
                    </option>
                @elseif (!empty($current_value))
                    <option {{ $current_value == $item[$value_field] ? 'selected="selected"' : '' }}
                            value="{{ $item[$value_field] }}">
                        {{ $titles }}
                    </option>
                @else
                    <option value="{{ $item[$value_field] }}">
                        {{ $titles }}
                    </option>
                @endif
            @endforeach
        @endif
    </select>
@endsection
