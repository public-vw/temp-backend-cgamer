@extends("$VIEW_ROOT.layouts.component")

@push('js')
    <script>
        $(document).ready(function () {
            $('select[name="{{ $NAME }}"]').select2({
                @if($CONFIG['hide_searchbox'] ?? false)
                    minimumResultsForSearch: Infinity,
                @endif
                @if($CONFIG['allow_null'] ?? false)
                    placeholder: {
                        id       : '',
                        text     : '{{ $CONFIG['null_option_title'] ?? '---' }}',
                        selected : true,
                        search   : ''
                    },
                @endif
                data: $.map({!! json_encode($OPTIONS) !!}, function (val, key) {
                    return {
                        id : key,
                        text : val,
                        selected : key=='{{ $VALUE ?? '' }}',
                    }
                })
            });
        });
    </script>
@endpush

@section('input-'.$rnd)
    <select id="{{ $ID ?? $NAME }}" name="{{ $NAME }}" class="form-control {{ $CLASS ?? '' }}"
        @if($VALUE ?? false)
            value="{{ $VALUE ?? null}}"
        @endif
        {{ $REQUIRED ?? false ? 'required':'' }}
        {!! $MORE ?? '' !!}
        @if($DISABLE ?? false) disabled @endif
        @if($MULTIPLE ?? false) multiple="multiple" @endif
    >
        @if($CONFIG['allow_null'] ?? false)
            <option></option>
        @endif
    </select>
    @isset($HELPER)<small>{!! $HELPER !!}</small>@endisset
@endsection
