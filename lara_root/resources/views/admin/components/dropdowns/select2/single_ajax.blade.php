@extends("$VIEW_ROOT.layouts.component")

@php
    $AJAX_URLS = [];
    if(isset($MORPH_LIST)){
        foreach($MORPH_LIST as $morph){
            $morph_route = model2route($morph);
            $AJAX_URLS[$morph] = Route::has("$VIEW_ROOT.$morph_route.list.all") ?
                route("$VIEW_ROOT.$morph_route.list.all") : route("$VIEW_ROOT.$morph_route.list.json");
        }
    }
@endphp

@push('js')
    <script>
        $(document).ready(function () {
                @if(isset($MORPH_LIST))
                    ajax_urls = JSON.parse('{!! json_encode($AJAX_URLS) !!}');
                    console.log(ajax_urls);
                @endif

                var single_ajax_{{$NAME}} = $('select[name="{{ $NAME }}"]').select2({
                @if($CONFIG['hide_searchbox'] ?? false)
                    minimumResultsForSearch: Infinity,
                @endif
                @if($FORMAT ?? false)
                    templateResult: function(item) {
                        return {!! $FORMAT !!};
                    },
                @endif
                ajax: {
                    @if(isset($AJAX_URL_DEPENDS_TO))
                        url: function(){
                            console.log('index : '+$('{{ $AJAX_URL_DEPENDS_TO }}').val());
                            console.log('ajax url : '+ajax_urls[$('{{ $AJAX_URL_DEPENDS_TO }}').val()]);
                            return ajax_urls[$('{{ $AJAX_URL_DEPENDS_TO }}').val()];
                        },
                    @else
                        url: "{!! $AJAX_URL !!}",
                    @endif
                    dataType: "{{ $CONFIG['ajax_datatype'] ?? 'json' }}",
                    method: "{{ $CONFIG['ajax_method'] ?? 'post' }}",

                    data: function (params) {
                        return {
                            @if(isset($DEPENDS_TO))
                                depends_val: $('{{ $DEPENDS_TO }}').val(),
                            @endif
                            term: params.term,
                            q: params.q,
                            _type: params._type, //query | query_append
                            page: params.page
                        };
                    },

                    processResults: function (data) {
                        @if($CONFIG['allow_null'] ?? false)
                            data.unshift({
                                id       : '',
                                text     : '{{ $CONFIG['null_option_title'] ?? '---' }}',
                                selected : true,
                                search   : ''
                            });
                        @endif

                        return {
                            @if($CONFIG['allow_null'] ?? false)
                                placeholder: {
                                    id : '',
                                    text: '{{ $PLACEHOLDER ?? '---' }}',
                                    selected: 'selected',
                                    search: ''
                                },
                            @endif
                            results: data
                        };
                    },
                    cache: true
                }
            });
        });
    </script>
@endpush

@section('input-'.$rnd)
    <select id="{{ $ID ?? $NAME }}" name="{{ $NAME }}" class="form-control {{ $CLASS ?? '' }}" {{ $REQUIRED ?? false ? 'required':'' }}
        @if($VALUE ?? false)
            value="{{ $VALUE['id'] ?? null}}"
        @endif
        {!! $MORE ?? '' !!}
        @if($DISABLE ?? false) disabled @endif
        @if($MULTIPLE ?? false) multiple="multiple" @endif
    >
        @if($CONFIG['allow_null'] ?? false)
            <option></option>
        @endif
        @if($VALUE ?? false)
            <option value="{{ $VALUE['id'] ?? null}}" selected="selected">{{ $VALUE['text'] ?? $CONFIG['null_option_title'] ?? '---' }}</option>
        @endif
    </select>
    @isset($HELPER)<small>{!! $HELPER !!}</small>@endisset
@endsection
