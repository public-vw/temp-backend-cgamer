@pushonce('js:map')
    <script>
        var marker;
        var map;

        function placeMarker(location) {
            marker.setMap(null);
            marker = new google.maps.Marker({
                position: location,
                map: map,
                {{--icon:'{{ assetlink("defaults/images/marker.png", true) }}',--}}
                animation:google.maps.Animation.DROP
            });
            marker.setMap(map);
            $('input[name="{{ $LATITUDE }}"]').val(location.lat());
            $('input[name="{{ $LONGITUDE }}"]').val(location.lng());
            $('input[name="{{ $MAP_ZOOM }}"]').val(Math.floor(map.getZoom()));
        }

        function initMap() {
            address = new google.maps.LatLng( {{ $VARIABLE->latitude ?? config('maps.latitude') }}, {{ $VARIABLE->longitude ?? config('maps.longitude') }} );
            addressZoom = {{ $VARIABLE->map_zoom ?? config('maps.map_zoom') }};

            map=new google.maps.Map(document.getElementById("{{ $MAP_ID }}"),{
                center:address,
                zoom:addressZoom,
                disableDefaultUI:true,
                mapTypeControl:true,
                mapTypeId:google.maps.MapTypeId.ROADMAP,
                styles: [{
                    featureType: "poi",
                    elementType: "labels",
                    stylers: [{
                        visibility: "off"
                    }]
                }]
            });

            marker=new google.maps.Marker({
                position:address,
{{--                    icon:'{{ assetlink("defaults/images/marker.png",true) }}',--}}
                animation:google.maps.Animation.BOUNCE
            });

            marker.setMap(map);

            google.maps.event.trigger(map, "resize");

            google.maps.event.addListener(map, 'click', function(event) {
                placeMarker(event.latLng);
                map.panTo(event.latLng);
            });

            var infowindow = new google.maps.InfoWindow({
                content:"<span style='display:block; float:left;'><b>{{ env('APP_TITLE') }} </b></span>"
            });
            google.maps.event.addListener(marker, 'click', function() {
                infowindow.open(map,marker);
            });
        }

        var script = document.createElement('script');
        script.src = 'https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAP') }}&callback=initMap';
        script.async = true;

        document.head.appendChild(script);

    </script>
@endpushonce

<div class="row form-group">
    @compo("$VIEW_ROOT.components.textfields.text", [
        "LABEL"         => __("Latitude"),
        "NAME"          => $LATITUDE,
        "DEFAULT"       => config('maps.latitude'),
        "VALUE"         => $VARIABLE->latitude ?? '',
        "PLACEHOLDER"   => __("Latitude"),
    ])

    @compo("$VIEW_ROOT.components.textfields.text", [
        "LABEL"         => __("Longitude"),
        "NAME"          => $LONGITUDE,
        "DEFAULT"       => config('maps.longitude'),
        "VALUE"         => $VARIABLE->longitude ?? '',
        "PLACEHOLDER"   => __("Longitude"),
    ])
</div>
<div class="row form-group">
    @compo("$VIEW_ROOT.components.textfields.text", [
        "LABEL"         => __("Map Zoom"),
        "NAME"          => $MAP_ZOOM,
        "DEFAULT"       => config('maps.map_zoom'),
        "VALUE"         => $VARIABLE->map_zoom ?? '',
        "PLACEHOLDER"   => __("Map Zoom"),
    ])
</div>
<div class="row form-group">
    <div class="form-group col-12 {{ $CLASS ?? '' }}">
        <label class="control-label">{{ $LABEL }}
            @if($REQUIRED ?? false) <span style="color: red; vertical-align:super">&nbsp;*</span>@endif
        </label>

        <div class="col-12">
            <div id='{{ $MAP_ID }}' style="width: 100%; height:300px; z-index: 10;"></div>
        </div>
    </div>
</div>
