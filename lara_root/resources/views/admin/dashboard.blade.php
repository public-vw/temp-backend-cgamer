@extends("$VIEW_ROOT.layouts.base")

@section('content')
    <div class="container-fluid">
        <div class="row row-cols-md-4 row-cols-sm-1">
            @compo("$VIEW_ROOT.components.boxes.card",[
                "BGCOLOR_CLASS" => 'info',
                "ICON"   => 'icon ion-md-folder-open',
                "TITLE"  => 'Categories',
                "CONTENTS" => [
                    [
                        'title' => 'requested',
                        'value' => $data['categories']['requested'],
                    ],
                    [
                        'title' => 'all',
                        'value' => $data['categories']['all'],
                    ],
                ],
                'DESC'   => 'All Categories',
                'HREF'   => route('admin.article_categories.index'),
            ])

            @compo("$VIEW_ROOT.components.boxes.card",[
                "BGCOLOR_CLASS" => 'success',
                "ICON"   => 'icon ion-md-document',
                "TITLE"  => 'Articles',
                "CONTENTS" => [
                    [
                        'title' => 'requested',
                        'value' => $data['articles']['requested'],
                    ],
                    [
                        'title' => 'all',
                        'value' => $data['articles']['all'],
                    ],
                ],
                "DESC"   => 'All Articles',
                'HREF'   => route('admin.articles.index'),
            ])

            @compo("$VIEW_ROOT.components.boxes.card",[
                "BGCOLOR_CLASS" => 'purple',
                "ICON"   => 'icon ion-md-person',
                "TITLE"  => 'Authors',
                "CONTENTS" => [
                    [
                        'title' => 'requested',
                        'value' => $data['authors']['requested'],
                    ],
                    [
                        'title' => 'all',
                        'value' => $data['authors']['all'],
                    ],
                ],
                "DESC"   => 'All Authors',
                'HREF'   => route('admin.authors.index'),
            ])

        </div>
    </div>
@endsection
