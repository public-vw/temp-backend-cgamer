@extends('mainframe')

@section('head')
    <!-- Styles -->
    <link href="{{ asset("assets/$VIEW_ROOT/css/app.css") }}" rel="stylesheet">
@endsection

@push('mainjs')
    <script src="{{ asset("assets/$VIEW_ROOT/js/app.js") }}" ></script>
@endpush

@section('body')
    <div id="app">
        <div class="main-header d-flex">
            @include("$VIEW_ROOT.includes.header")
        </div>
        <div class="main-sidebar">
            @include("$VIEW_ROOT.includes.sidebar.body")
        </div>
        <div class="main-content">

            {{--TODO create breacrumbs--}}

            @yield('content')
            @include("$VIEW_ROOT.includes.footer")
        </div>

        <div class="backdrop-sidebar-mobile"></div>
    </div>

{{--TODO fix vue for menu--}}


@endsection
