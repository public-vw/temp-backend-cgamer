@extends("$VIEW_ROOT.layouts.base")
{{-- ---------------------------------------------------------------------- --}}
@section('content')
    <h1>{{$http_error}}</h1>
    <p class="m-error_desc">
        {{__('errorPages.'.$http_error.'.header')}}
        <br>
        <a href="{{url('/')}}" class="btn btn-default btn-lg">{{__('errorPages.button')}}</a>
    </p>
@endsection
