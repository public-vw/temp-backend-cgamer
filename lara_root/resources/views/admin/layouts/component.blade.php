<div class="form-group {{ $CONTAINER_CLASS ?? 'col-md-6 col-sm-12' }}">
    @isset($LABEL)
        <label class="control-label">{{ $LABEL }}
            @if($REQUIRED ?? false) <span style="color: red">&nbsp;*</span>@endif
            @if($TOOLTIP ?? false) <span data-toggle="tooltip" data-placement="bottom" title="{!! $TOOLTIP !!}"><i class="fa fa-question-circle text-info"></i></span>@endif
        </label>
    @endisset
    @yield('input-'.$rnd)
    @error($NAME)
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
    @enderror
</div>
