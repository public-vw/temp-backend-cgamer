@extends("$VIEW_ROOT.layouts.base")

@section('content')
    <div class="container-fluid">
        <div class="d-flex justify-content-between mb20">
            <div>
                <h3 class="title-bar">{{ __('Edit :title', ['title' => $TITLE]) }}</h3>
            </div>
            <div>

                @include("{$VIEW_ROOT}.layouts.information")

                @include("{$VIEW_ROOT}.layouts.preview_button")

                @include("{$VIEW_ROOT}.layouts.sibling_buttons")

                @compo("$VIEW_ROOT.components.actions.delete", [
                    "TITLE" => "delete",
                    "TABLE" => $TABLE,
                    "CLASS" => 'btn btn-danger',
                ])
            </div>
        </div>
        <form id="{{$TABLE}}-form" action="{{ route("{$VIEW_ROOT}.{$TABLE}.update", $VARIABLE) }}" method="post" @isset($HAS_UPLOAD) enctype="multipart/form-data" @endisset>
            @csrf
            @method('PUT')

            {{--            USE This if errors not showing on the forms--}}
            @include("$VIEW_ROOT.components.combinations.errors")

            @include("$VIEW_ROOT.includes.alerts")

            <div class="lang-content-box">
                <div class="row">
                    <div class="col-12">
                        @yield('inside')
                        <div class="text-right div-submit mt-1 mb-3">
                            @compo("$VIEW_ROOT.components.clickables.link",[
                                "TITLE"     => '<i class="fa fa-arrow-circle-o-left"></i>&nbsp;'. __('Back'),
                                "URL"       => route("{$VIEW_ROOT}.{$TABLE}.index"),
                                'RAW_HTML'  => true,
                            ])
                            @compo("$VIEW_ROOT.components.clickables.multisubmit",[
                                "TARGET_FORM" => "$TABLE-form",
                                "DEFAULT" => [
                                    "stay" => '<i class="fa fa-check-circle-o"></i>&nbsp;'. __('Save'),
                                ],
                                'BTNCLASS' => 'btn-primary',
                                "ITEMS" =>  [
                                    "back" => '<i class="fa fa-check-circle"></i>&nbsp;'. __('Save & Done'),
                                ],
                            ])
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div class="row">
            <div class="col-md-12">
                @yield('outside')
            </div>
        </div>
    </div>
@endsection
