@extends("$VIEW_ROOT.layouts.base")

@section('content')
    <form action="{{ route("{$VIEW_ROOT}.{$TABLE}.store") }}" method="post" @isset($HAS_UPLOAD) enctype="multipart/form-data" @endisset>
        @csrf
        <div class="container-fluid">
            <div class="d-flex justify-content-between mb20">
                <div>
                    <h3 class="title-bar">{{ __("Add New $MODEL") }}</h3>
                </div>
            </div>

{{--            USE This if errors not showing on the forms--}}
            @include("$VIEW_ROOT.components.combinations.errors")

            @include("$VIEW_ROOT.includes.alerts")

            <div class="lang-content-box">
                <div class="row">
                    <div class="col-12">
                        @yield('inside')
                        <div class="text-right div-submit">
                            @compo("$VIEW_ROOT.components.clickables.link",[
                                "TITLE"     => '<i class="fa fa-arrow-circle-o-left"></i>&nbsp;'. __('Back'),
                                "URL"       => route("{$VIEW_ROOT}.{$TABLE}.index"),
                                'RAW_HTML'  => true,
                            ])
                            @compo("$VIEW_ROOT.components.clickables.multisubmit",[
                                "DEFAULT" => [
                                    "edit" => '<i class="fa fa-pencil-square-o"></i> '. __('Create & Edit'),
                                ],
                                'BTNCLASS' => 'btn-success',
                                "ITEMS" =>  [
                                    "back" => '<i class="fa fa-check-circle"></i>&nbsp;'. __('Create & Done'),
                                    "break1"  => null,
                                    "stay" => '<i class="fa fa-check-circle-o"></i>&nbsp;'. __('Create & New'),
                                ],
                            ])
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </form>
@endsection
