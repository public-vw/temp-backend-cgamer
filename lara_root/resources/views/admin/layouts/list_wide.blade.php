@extends("$VIEW_ROOT.layouts.base")

@push('js')
    {{ $dataTable->scripts() }}
@endpush

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <h3 class="title-bar">
                    {{ __("All " . Str::plural($MODEL)) }}
                    @empty($NO_CREATE)
                        <a href="{{route("{$VIEW_ROOT}.{$TABLE}.create")}}" class="btn btn-link"><i class="fa fa-plus-circle"></i>&nbsp;Create</a>
                    @endempty
                    @isset($CASCADE_VIEW)
                        <a href="{{route("{$VIEW_ROOT}.{$TABLE}.index")}}" class="btn btn-link">
                            <i class="fa fa-list"></i>&nbsp;Cascade View</a>
                    @endisset
                </h3>
            </div>
        </div>
        <div class="row">
            @if($statuses ?? false)
                @includeWhen($BULK_ACTION ?? false, "{$VIEW_ROOT}.includes.bulk_actions")
                @include("{$VIEW_ROOT}.includes.status_filter")
            @endif
        </div>

        @include("$VIEW_ROOT.includes.alerts")

        {{--TODO create header status--}}

        <div class="panel">
            <div class="panel-body">
                <form class="bravo-form-item">
                    <div class="table-responsive">
                        {{ $dataTable->table(['class' => 'table table-hover']) }}
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
