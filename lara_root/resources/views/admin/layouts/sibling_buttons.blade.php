@if($VARIABLE->previousId ?? false)
    @compo("$VIEW_ROOT.components.clickables.link", [
    "URL"     => route("{$VIEW_ROOT}.{$TABLE}.edit", [$VARIABLE->previousId]),
    "TITLE"    => "<i class='fa fa-chevron-circle-left'></i>",
    "CLASS"    => 'btn btn-info text-white',
    "RAW_HTML" => true,
    ])
@endif

@if($VARIABLE->nextId ?? false)
    @compo("$VIEW_ROOT.components.clickables.link", [
    "URL"     => route("{$VIEW_ROOT}.{$TABLE}.edit", [$VARIABLE->nextId]),
    "TITLE"    => "<i class='fa fa-chevron-circle-right'></i>",
    "CLASS"    => 'btn btn-info text-white',
    "RAW_HTML" => true,
    ])
@endif
