@if(\Illuminate\Support\Facades\Route::has("{$VIEW_ROOT}.{$TABLE}.preview") ?? false)
    @compo("$VIEW_ROOT.components.clickables.link", [
    "URL"     => route("{$VIEW_ROOT}.{$TABLE}.preview", [$VARIABLE->id]),
    "TITLE"    => "<i class='fa fa-eye'></i>",
    "CLASS"    => 'btn btn-success text-white',
    "TARGET"    => '_blank',
    "RAW_HTML" => true,
    ])
@elseif(\Illuminate\Support\Facades\Route::has("{$VIEW_ROOT}.{$TABLE}.single.preview") ?? false)
    @compo("$VIEW_ROOT.components.clickables.link", [
    "URL"     => route("{$VIEW_ROOT}.{$TABLE}.single.preview", [$VARIABLE->id]),
    "TITLE"    => "<i class='fa fa-eye'></i>",
    "CLASS"    => 'btn btn-success text-white',
    "TARGET"    => '_blank',
    "RAW_HTML" => true,
    ])
@elseif(\Illuminate\Support\Facades\Route::has("client.{$TABLE}.single.preview") ?? false)
    @compo("$VIEW_ROOT.components.clickables.link", [
    "URL"     => route("client.{$TABLE}.single.preview", [$VARIABLE->id]),
    "TITLE"    => "<i class='fa fa-eye'></i>",
    "CLASS"    => 'btn btn-success text-white',
    "TARGET"    => '_blank',
    "RAW_HTML" => true,
    ])
@endif
