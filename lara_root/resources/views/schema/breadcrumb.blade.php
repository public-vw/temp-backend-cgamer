<script type="application/ld+json">
    {
        "@context": "https://schema.org",
        "@type": "BreadcrumbList",
        "itemListElement": [
            @php($position = 1)
            @foreach($items as $name => $url)
                {
                    "@type": "ListItem",
                    "position": {{ $position++ }},
                    "name": "{{ $name }}",
                    "item": "{!! $url !!}@if(!$loop->last)/@endif"
                }
                @if(!$loop->last),@endif
            @endforeach
        ]
    }
</script>
