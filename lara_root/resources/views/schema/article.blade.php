<script type="application/ld+json">
    {
      "@context": "https://schema.org"
      ,"@type": "Article"
      ,"headline": "{{ $heading }}"
        @if($images)
          ,"image": ["{!! $images !!}"]
        @endif

      ,"datePublished": "{{ $dates['created']->format('c') }}"
      ,"dateModified": "{{ $dates['modified']->format('c') }}"
      ,"author": [{
          "@type": "Person"
          ,"name": "{{ $author['nickname'] }}"
          ,"url": "{{ route('public.profile.index',['user'=>$author['user'], 'nickname' => $author['nickname']]) }}"
      }]
    }
</script>
