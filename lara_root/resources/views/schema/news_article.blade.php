<script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@type": "NewsArticle",
      "headline": "{{ $heading }}",
      "image": [{{ $images }}],
      "datePublished": "{{ $dates['created']->format('c') }}",
      "dateModified": "{{ $dates['modified']->format('c') }}",
      "author": [{
          "@type": "Person",
          "name": "{{ $author['title'] }}",
          "url": "{{ route('public.profile.index',['user'=>$author['user'], 'nickname' => $author['nickname']]) }}"
      }]
    }
</script>
