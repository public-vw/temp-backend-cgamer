<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="{{ str_replace('_', '-', app()->getLocale()) }}"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="{{ str_replace('_', '-', app()->getLocale()) }}"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="{{ str_replace('_', '-', app()->getLocale()) }}"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<!--<![endif]-->
    <head>
        <meta charset="utf-8"/>
        <meta content="text/html; charset=utf-8" http-equiv="content-type"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        @yield('meta')

        @if(!empty(config('tokens.google.webmaster')))
            <meta name="google-site-verification" content="{{config('tokens.google.webmaster')}}" />
        @endif
        @if(!empty(config('tokens.bing.webmaster')))
            <meta name="msvalidate.01" content="{{config('tokens.bing.webmaster')}}"/>
        @endif
        @if(app()->getLocale()=='fa' && !empty(config('tokens.ir.enamad')))
            <meta name="enamad" content="{{config('tokens.ir.enamad')}}"/>
        @endif
        @if(app()->getLocale()=='fa' && !empty(config('tokens.ir.samandehi')))
            <meta name="samandehi" content="{{config('tokens.ir.samandehi')}}"/>
        @endif

        <title>@yield('page_title') | {{$brandings['site_title'] ?? config('app.title', 'Laravel') }}</title>

{{--        #TODO: add canonical page meta --}}
{{--        #TODO: add hreflang meta --}}

        {{-- CSRF Token --}}
        <meta name="csrf-token" content="{{ csrf_token() }}">

        @yield('amp_url')

        {{--<!-- twitter card starts from here, if you don't need remove this section -->--}}
        {{--<meta name="twitter:card" content="summary"/>--}}
        {{--<meta name="twitter:creator" content="@yourtwitterusername"/>--}}
        <meta name="twitter:url" content="{{url()->current()}}"/>
        <meta name="twitter:title" content="@yield('page_title') | {{$brandings['site_title'] ?? config('app.title', 'Laravel')}}">
        {{--<!-- maximum 140 char -->--}}
        <meta name="twitter:description" content="@yield('page_description')">
        <meta name="twitter:site" content="{{$brandings['site_title'] ?? config('app.title', 'Laravel')}}">
        <meta name="twitter:image" content="@yield('page_image',asset("assets/interface/images/logo/logo-white.svg"))">
        {{--<!-- when you post this page url in twitter , this image will be shown -->--}}
        {{--<!-- twitter card ends from here -->--}}

        {{--<!-- facebook open graph starts from here, if you don't need then delete open graph related  -->--}}
        <meta property="og:title" content="@yield('page_title') | {{$brandings['site_title'] ?? config('app.title', 'Laravel')}}"/>
        <meta property="og:site_name" content="{{$brandings['site_title'] ?? config('app.title', 'Laravel')}}"/>
        <meta property="og:description" content="@yield('page_description')"/>
        <meta property="og:url" content="{{url()->current()}}"/>
        <meta property="og:locale" content="{{ app()->getLocale() }}"/>
        {{--<!--meta property="fb:admins" content="" /-->  <!-- use this if you have  -->--}}
        <meta property="og:type" content="website"/>
        <meta property="og:image" content="@yield('page_image',asset("assets/interface/images/logo/logo-white.svg"))"/>
        {{--<!-- when you post this page url in facebook , this image will be shown -->--}}
        {{--<!-- facebook open graph ends from here -->--}}

        <meta name="generator" content="{{trans('brand.site.generator')}}" />
        <meta name="author" content="{{trans('brand.site.author')}}" />
        <meta name="copyright" content="{{trans('brand.site.copyright')}}" />
        <meta name="email" content="{{trans('brand.site.author.email')}}" />
        <meta name="document-type" content="Public" />
        <meta name="document-rating" content="General" />
        <meta name="classification" content="Consumer" />
        <meta name="rating" content="general" />
        <meta name="resource-type" content="document" />
        <meta name="robots" content="INDEX,FOLLOW" />
        <meta name="googlebot" content="index" />
        <meta name="revisit-after" content="1 Day" />
        <meta name="doc-class" content="Living Document" />
        <meta name="keywords" content="@yield('page_keywords')" />
        <meta name="description" content="@yield('page_description')" />


        @stack('css')

        <!-- Favicons -->
            <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/interface/images/logo/favicon.ico') }}"/>
            <!-- this icon shows in browser toolbar -->
            <link rel="icon" type="image/x-icon" href="{{ asset('assets/interface/images/logo/favicon.ico') }}"/>

            <link href="{{ asset('assets/interface/images/logo/favicon.ico') }}" rel="apple-touch-icon"/>

            {{--<link rel="apple-touch-icon" sizes="57x57" href="assets/img/favicon/apple-icon-57x57.png">--}}
            {{--<link rel="apple-touch-icon" sizes="60x60" href="assets/img/favicon/apple-icon-60x60.png">--}}
            <link href="{{ asset('assets/interface/images/logo/favicon.ico') }}" rel="apple-touch-icon" sizes="72x72"/>
            {{--<link rel="apple-touch-icon" sizes="76x76" href="assets/img/favicon/apple-icon-76x76.png">--}}
            <link href="{{ asset('assets/interface/images/logo/favicon.ico') }}" rel="apple-touch-icon" sizes="114x114"/>
            {{--<link rel="apple-touch-icon" sizes="120x120" href="assets/img/favicon/apple-icon-120x120.png">--}}
            {{--<link rel="apple-touch-icon" sizes="144x144" href="assets/img/favicon/apple-icon-144x144.png">--}}
            {{--<link rel="apple-touch-icon" sizes="152x152" href="assets/img/favicon/apple-icon-152x152.png">--}}
            {{--<link rel="apple-touch-icon" sizes="180x180" href="assets/img/favicon/apple-icon-180x180.png">--}}

            {{--<link rel="icon" type="image/png" sizes="192x192" href="assets/img/favicon/android-icon-192x192.png">--}}
            {{--<link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicon/favicon-32x32.png">--}}
            {{--<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon/favicon-96x96.png">--}}
            {{--<link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicon/favicon-16x16.png">--}}

            {{--<link rel="manifest" href="assets/img/favicon/manifest.json">--}}

        @unlessrole('admin@web')
            @if(env('APP_ENV','local')!=='local' && !empty(config('tokens.google.tagmanager')))
                <!-- Google Tag Manager -->
                <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                })(window,document,'script','dataLayer','{{config('tokens.google.tagmanager')}}');</script>
                <!-- End Google Tag Manager -->
            @endif
        @endunlessrole

        @yield('head')

{{-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries --}}
{{--       WARNING: Respond.js doesn't work if you view the page via file:// --}}
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="{{$BODY_CLASS ?? ''}}"
        {!! $NO_COPY ?? false ? 'oncopy="return false"' : ''  !!}
        {!! $NO_CUT ?? false ? 'oncut="return false"' : ''  !!}
        {!! $NO_PASTE ?? false ? 'onpaste="return false"' : ''  !!}
    >
        @unlessrole('admin@web')
            @if(env('APP_ENV','local')!=='local' && !empty(config('tokens.google.tagmanager')))
                <!-- Google Tag Manager (noscript) -->
                <noscript><iframe src="https://www.googletagmanager.com/ns.html?id={{config('tokens.google.tagmanager')}}"
                height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
                <!-- End Google Tag Manager (noscript) -->
            @endif
        @endunlessrole

        <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
            your browser</a> to improve your experience.</p>
        <![endif]-->

        @includeWhen(Session::has('impersonate'),'includes.impersonate_logback')

        @yield('body')

        @stack('mainjs')
        <script>
            ajaxSet : {
                if (typeof jQuery !== "function") {
                    console.log('jQuery not implemented yet!')
                }

                if (typeof $ !== "function") {
                    console.log('$ not implemented yet!')
                    break ajaxSet
                }

                if (typeof $.ajaxSetup !== "function") {
                    console.log('ajaxSetup not implemented yet!')
                    break ajaxSet
                }

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
            }
        </script>
        @stack('js')

        @section('noty')
            <script type="text/javascript">
                $(function(){
                    @if(\Session::has('alerts'))
                        <?php $alerts = session('alerts'); ?>
                        @foreach($alerts as $alert)
                            var n = noty({
                                type: '{{$alert[0]}}', // success, error, warning, information, notification
                                text: '{{$alert[1]}}'
                            });
                            n.setTimeout({{$alert[0]=='warning'?'10000':'8000'}});
                        @endforeach
                    @endif
                    @if(isset($errors) && count($errors)>0)
                        @foreach($errors as $i=>$err)
                            @break($i >= 3)
                            var e{{$i}} = noty({
                                type: 'error', // success, error, warning, information, notification
                                text: '{{$err}}'
                            });
                            e{{$i}}.setTimeout(8000);
                        @endforeach
                    @endif
                });
            </script>
        @show

        @stack('ld-json')

        @section('crisp')
            @if(config('app.crisp_chat_enable',false))
                <script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="{{ config('app.crisp_website_id') }}";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>
            @endif
        @show
    </body>
</html>
