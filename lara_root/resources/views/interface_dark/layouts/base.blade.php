@extends('mainframe',['BODY_CLASS' => ($NO_HEADER ?? false) ? 'no-header':''])

@section('bootstrap-css','')
@section('bootstrap-js','')
@section('jquery','')
@section('fontawesome','')
@section('flags','')
@section('noty','')
@section('meta')
    <meta name="color-scheme" content="dark">
    @section('canonical')
        <link rel="canonical" href="{{ url()->current() }}">
    @show
@endsection

@section('head')
{{--    Fonts --}}
{{--    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">--}}

{{--    Styles --}}
    <link href="{{ asset("assets/{$VIEW_ROOT}/css/main.css") }}" rel="stylesheet">
@endsection

@prepend('mainjs')
{{--    TODO: these should add as npm module --}}
    <script type="text/javascript" src="{{ asset("assets/{$VIEW_ROOT}/js/vendor/xm_accordion.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/{$VIEW_ROOT}/js/vendor/xm_dropdown.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/{$VIEW_ROOT}/js/vendor/xm_hexagon.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/{$VIEW_ROOT}/js/vendor/xm_popup.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/{$VIEW_ROOT}/js/vendor/xm_progressBar.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/{$VIEW_ROOT}/js/vendor/xm_tab.min.js") }}"></script>
    <script type="text/javascript" src="{{ asset("assets/{$VIEW_ROOT}/js/vendor/xm_tooltip.min.js") }}"></script>

    <script type="text/javascript" src="{{ asset("assets/{$VIEW_ROOT}/js/app.js") }}"></script>

{{--    <script src="https://cdn.jsdelivr.net/npm/vue@2"></script>--}}
{{--    <script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>--}}
{{--    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>--}}
{{--    <script src="https://code.jquery.com/jquery-3.6.0.slim.min.js"></script>--}}

@endprepend

@section('body')
    @includeUnless(($NO_LOADER ?? false), "$VIEW_ROOT.includes.loader")

    {{--    @include("$VIEW_ROOT.includes.toolbar")--}}

    {{--    TODO: sass._grid:408 enable padding in full mode, if you want these --}}
    {{--    @include("$VIEW_ROOT.includes.chat_widget_list")--}}
    {{--    @include("$VIEW_ROOT.includes.chat_widget_single")--}}

    @includeUnless(($NO_HEADER ?? false), "$VIEW_ROOT.includes.header.base")
    @isset($VUE_OBJ)
        <div id="{{ $VUE_OBJ }}">
    @endisset
            @yield('content')
    @isset($VUE_OBJ)
        </div>
    @endisset

    @guest
        @if (Route::has('auth.register'))

        <a href="javascript:;" id="trigReglogModal" class="popup-reglog-trigger not-visible"></a>
        <div class="popup-reglog">
            @include("$VIEW_ROOT.includes.reglog.base")
        </div>

        @endif
    @endguest

@endsection
