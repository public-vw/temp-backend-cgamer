@extends("$VIEW_ROOT.layouts.base",[
        'FILE_NAME' => 'marketplace-product',
        'PAGE_TITLE' => 'Homepage',
    ])

@section('content')

    @include("$VIEW_ROOT.includes.nav_widget_small")

    @include("$VIEW_ROOT.includes.chat_widget_list")

<!-- HEADER -->
<header class="header">
    <!-- HEADER ACTIONS -->
    <div class="header-actions">
        <!-- HEADER BRAND -->
        <div class="header-brand">
            <!-- LOGO -->
            <div class="logo">
                <!-- ICON LOGO VIKINGER -->
                <svg class="icon-logo-vikinger small">
                    <use xlink:href="#svg-logo-vikinger"></use>
                </svg>
                <!-- /ICON LOGO VIKINGER -->
            </div>
            <!-- /LOGO -->

            <!-- HEADER BRAND TEXT -->
            <h1 class="header-brand-text">Vikinger</h1>
            <!-- /HEADER BRAND TEXT -->
        </div>
        <!-- /HEADER BRAND -->
    </div>
    <!-- /HEADER ACTIONS -->

    <!-- HEADER ACTIONS -->
    <div class="header-actions">
        <!-- SIDEMENU TRIGGER -->
        <div class="sidemenu-trigger navigation-widget-trigger">
            <!-- ICON GRID -->
            <svg class="icon-grid">
                <use xlink:href="#svg-grid"></use>
            </svg>
            <!-- /ICON GRID -->
        </div>
        <!-- /SIDEMENU TRIGGER -->

        <!-- MOBILEMENU TRIGGER -->
        <div class="mobilemenu-trigger navigation-widget-mobile-trigger">
            <!-- BURGER ICON -->
            <div class="burger-icon inverted">
                <!-- BURGER ICON BAR -->
                <div class="burger-icon-bar"></div>
                <!-- /BURGER ICON BAR -->

                <!-- BURGER ICON BAR -->
                <div class="burger-icon-bar"></div>
                <!-- /BURGER ICON BAR -->

                <!-- BURGER ICON BAR -->
                <div class="burger-icon-bar"></div>
                <!-- /BURGER ICON BAR -->
            </div>
            <!-- /BURGER ICON -->
        </div>
        <!-- /MOBILEMENU TRIGGER -->

        <!-- NAVIGATION -->
        <nav class="navigation">
            <!-- MENU MAIN -->
            <ul class="menu-main">
                <!-- MENU MAIN ITEM -->
                <li class="menu-main-item">
                    <!-- MENU MAIN ITEM LINK -->
                    <a class="menu-main-item-link" href="#">Home</a>
                    <!-- /MENU MAIN ITEM LINK -->
                </li>
                <!-- /MENU MAIN ITEM -->

                <!-- MENU MAIN ITEM -->
                <li class="menu-main-item">
                    <!-- MENU MAIN ITEM LINK -->
                    <a class="menu-main-item-link" href="#">Careers</a>
                    <!-- /MENU MAIN ITEM LINK -->
                </li>
                <!-- /MENU MAIN ITEM -->

                <!-- MENU MAIN ITEM -->
                <li class="menu-main-item">
                    <!-- MENU MAIN ITEM LINK -->
                    <a class="menu-main-item-link" href="#">Faqs</a>
                    <!-- /MENU MAIN ITEM LINK -->
                </li>
                <!-- /MENU MAIN ITEM -->

                <!-- MENU MAIN ITEM -->
                <li class="menu-main-item">
                    <!-- MENU MAIN ITEM LINK -->
                    <p class="menu-main-item-link">
                        <!-- ICON DOTS -->
                        <svg class="icon-dots">
                            <use xlink:href="#svg-dots"></use>
                        </svg>
                        <!-- /ICON DOTS -->
                    </p>
                    <!-- /MENU MAIN ITEM LINK -->

                    <!-- MENU MAIN -->
                    <ul class="menu-main">
                        <!-- MENU MAIN ITEM -->
                        <li class="menu-main-item">
                            <!-- MENU MAIN ITEM LINK -->
                            <a class="menu-main-item-link" href="#">About Us</a>
                            <!-- /MENU MAIN ITEM LINK -->
                        </li>
                        <!-- /MENU MAIN ITEM -->

                        <!-- MENU MAIN ITEM -->
                        <li class="menu-main-item">
                            <!-- MENU MAIN ITEM LINK -->
                            <a class="menu-main-item-link" href="#">Our Blog</a>
                            <!-- /MENU MAIN ITEM LINK -->
                        </li>
                        <!-- /MENU MAIN ITEM -->

                        <!-- MENU MAIN ITEM -->
                        <li class="menu-main-item">
                            <!-- MENU MAIN ITEM LINK -->
                            <a class="menu-main-item-link" href="#">Contact Us</a>
                            <!-- /MENU MAIN ITEM LINK -->
                        </li>
                        <!-- /MENU MAIN ITEM -->

                        <!-- MENU MAIN ITEM -->
                        <li class="menu-main-item">
                            <!-- MENU MAIN ITEM LINK -->
                            <a class="menu-main-item-link" href="#">Privacy Policy</a>
                            <!-- /MENU MAIN ITEM LINK -->
                        </li>
                        <!-- /MENU MAIN ITEM -->
                    </ul>
                    <!-- /MENU MAIN -->
                </li>
                <!-- /MENU MAIN ITEM -->
            </ul>
            <!-- /MENU MAIN -->
        </nav>
        <!-- /NAVIGATION -->
    </div>
    <!-- /HEADER ACTIONS -->

    <!-- HEADER ACTIONS -->
    <div class="header-actions search-bar">
        <!-- INTERACTIVE INPUT -->
        <div class="interactive-input dark">
            <input type="text" id="search-main" name="search_main" placeholder="Search here for people or groups">
            <!-- INTERACTIVE INPUT ICON WRAP -->
            <div class="interactive-input-icon-wrap">
                <!-- INTERACTIVE INPUT ICON -->
                <svg class="interactive-input-icon icon-magnifying-glass">
                    <use xlink:href="#svg-magnifying-glass"></use>
                </svg>
                <!-- /INTERACTIVE INPUT ICON -->
            </div>
            <!-- /INTERACTIVE INPUT ICON WRAP -->

            <!-- INTERACTIVE INPUT ACTION -->
            <div class="interactive-input-action">
                <!-- INTERACTIVE INPUT ACTION ICON -->
                <svg class="interactive-input-action-icon icon-cross-thin">
                    <use xlink:href="#svg-cross-thin"></use>
                </svg>
                <!-- /INTERACTIVE INPUT ACTION ICON -->
            </div>
            <!-- /INTERACTIVE INPUT ACTION -->
        </div>
        <!-- /INTERACTIVE INPUT -->

        <!-- DROPDOWN BOX -->
        <div class="dropdown-box padding-bottom-small header-search-dropdown">
            <!-- DROPDOWN BOX CATEGORY -->
            <div class="dropdown-box-category">
                <!-- DROPDOWN BOX CATEGORY TITLE -->
                <p class="dropdown-box-category-title">Members</p>
                <!-- /DROPDOWN BOX CATEGORY TITLE -->
            </div>
            <!-- /DROPDOWN BOX CATEGORY -->

            <!-- DROPDOWN BOX LIST -->
            <div class="dropdown-box-list small no-scroll">
                <!-- DROPDOWN BOX LIST ITEM -->
                <a class="dropdown-box-list-item" href="profile-timeline.html">
                    <!-- USER STATUS -->
                    <div class="user-status notification">
                        <!-- USER STATUS AVATAR -->
                        <div class="user-status-avatar">
                            <!-- USER AVATAR -->
                            <div class="user-avatar small no-outline">
                                <!-- USER AVATAR CONTENT -->
                                <div class="user-avatar-content">
                                    <!-- HEXAGON -->
                                    <div class="hexagon-image-30-32" data-src="img/avatar/05.jpg"></div>
                                    <!-- /HEXAGON -->
                                </div>
                                <!-- /USER AVATAR CONTENT -->

                                <!-- USER AVATAR PROGRESS -->
                                <div class="user-avatar-progress">
                                    <!-- HEXAGON -->
                                    <div class="hexagon-progress-40-44"></div>
                                    <!-- /HEXAGON -->
                                </div>
                                <!-- /USER AVATAR PROGRESS -->

                                <!-- USER AVATAR PROGRESS BORDER -->
                                <div class="user-avatar-progress-border">
                                    <!-- HEXAGON -->
                                    <div class="hexagon-border-40-44"></div>
                                    <!-- /HEXAGON -->
                                </div>
                                <!-- /USER AVATAR PROGRESS BORDER -->

                                <!-- USER AVATAR BADGE -->
                                <div class="user-avatar-badge">
                                    <!-- USER AVATAR BADGE BORDER -->
                                    <div class="user-avatar-badge-border">
                                        <!-- HEXAGON -->
                                        <div class="hexagon-22-24"></div>
                                        <!-- /HEXAGON -->
                                    </div>
                                    <!-- /USER AVATAR BADGE BORDER -->

                                    <!-- USER AVATAR BADGE CONTENT -->
                                    <div class="user-avatar-badge-content">
                                        <!-- HEXAGON -->
                                        <div class="hexagon-dark-16-18"></div>
                                        <!-- /HEXAGON -->
                                    </div>
                                    <!-- /USER AVATAR BADGE CONTENT -->

                                    <!-- USER AVATAR BADGE TEXT -->
                                    <p class="user-avatar-badge-text">12</p>
                                    <!-- /USER AVATAR BADGE TEXT -->
                                </div>
                                <!-- /USER AVATAR BADGE -->
                            </div>
                            <!-- /USER AVATAR -->
                        </div>
                        <!-- /USER STATUS AVATAR -->

                        <!-- USER STATUS TITLE -->
                        <p class="user-status-title"><span class="bold">Neko Bebop</span></p>
                        <!-- /USER STATUS TITLE -->

                        <!-- USER STATUS TEXT -->
                        <p class="user-status-text">1 friends in common</p>
                        <!-- /USER STATUS TEXT -->

                        <!-- USER STATUS ICON -->
                        <div class="user-status-icon">
                            <!-- ICON FRIEND -->
                            <svg class="icon-friend">
                                <use xlink:href="#svg-friend"></use>
                            </svg>
                            <!-- /ICON FRIEND -->
                        </div>
                        <!-- /USER STATUS ICON -->
                    </div>
                    <!-- /USER STATUS -->
                </a>
                <!-- /DROPDOWN BOX LIST ITEM -->

                <!-- DROPDOWN BOX LIST ITEM -->
                <a class="dropdown-box-list-item" href="profile-timeline.html">
                    <!-- USER STATUS -->
                    <div class="user-status notification">
                        <!-- USER STATUS AVATAR -->
                        <div class="user-status-avatar">
                            <!-- USER AVATAR -->
                            <div class="user-avatar small no-outline">
                                <!-- USER AVATAR CONTENT -->
                                <div class="user-avatar-content">
                                    <!-- HEXAGON -->
                                    <div class="hexagon-image-30-32" data-src="img/avatar/15.jpg"></div>
                                    <!-- /HEXAGON -->
                                </div>
                                <!-- /USER AVATAR CONTENT -->

                                <!-- USER AVATAR PROGRESS -->
                                <div class="user-avatar-progress">
                                    <!-- HEXAGON -->
                                    <div class="hexagon-progress-40-44"></div>
                                    <!-- /HEXAGON -->
                                </div>
                                <!-- /USER AVATAR PROGRESS -->

                                <!-- USER AVATAR PROGRESS BORDER -->
                                <div class="user-avatar-progress-border">
                                    <!-- HEXAGON -->
                                    <div class="hexagon-border-40-44"></div>
                                    <!-- /HEXAGON -->
                                </div>
                                <!-- /USER AVATAR PROGRESS BORDER -->

                                <!-- USER AVATAR BADGE -->
                                <div class="user-avatar-badge">
                                    <!-- USER AVATAR BADGE BORDER -->
                                    <div class="user-avatar-badge-border">
                                        <!-- HEXAGON -->
                                        <div class="hexagon-22-24"></div>
                                        <!-- /HEXAGON -->
                                    </div>
                                    <!-- /USER AVATAR BADGE BORDER -->

                                    <!-- USER AVATAR BADGE CONTENT -->
                                    <div class="user-avatar-badge-content">
                                        <!-- HEXAGON -->
                                        <div class="hexagon-dark-16-18"></div>
                                        <!-- /HEXAGON -->
                                    </div>
                                    <!-- /USER AVATAR BADGE CONTENT -->

                                    <!-- USER AVATAR BADGE TEXT -->
                                    <p class="user-avatar-badge-text">7</p>
                                    <!-- /USER AVATAR BADGE TEXT -->
                                </div>
                                <!-- /USER AVATAR BADGE -->
                            </div>
                            <!-- /USER AVATAR -->
                        </div>
                        <!-- /USER STATUS AVATAR -->

                        <!-- USER STATUS TITLE -->
                        <p class="user-status-title"><span class="bold">Tim Rogers</span></p>
                        <!-- /USER STATUS TITLE -->

                        <!-- USER STATUS TEXT -->
                        <p class="user-status-text">4 friends in common</p>
                        <!-- /USER STATUS TEXT -->

                        <!-- USER STATUS ICON -->
                        <div class="user-status-icon">
                            <!-- ICON FRIEND -->
                            <svg class="icon-friend">
                                <use xlink:href="#svg-friend"></use>
                            </svg>
                            <!-- /ICON FRIEND -->
                        </div>
                        <!-- /USER STATUS ICON -->
                    </div>
                    <!-- /USER STATUS -->
                </a>
                <!-- /DROPDOWN BOX LIST ITEM -->
            </div>
            <!-- /DROPDOWN BOX LIST -->

            <!-- DROPDOWN BOX CATEGORY -->
            <div class="dropdown-box-category">
                <!-- DROPDOWN BOX CATEGORY TITLE -->
                <p class="dropdown-box-category-title">Groups</p>
                <!-- /DROPDOWN BOX CATEGORY TITLE -->
            </div>
            <!-- /DROPDOWN BOX CATEGORY -->

            <!-- DROPDOWN BOX LIST -->
            <div class="dropdown-box-list small no-scroll">
                <!-- DROPDOWN BOX LIST ITEM -->
                <a class="dropdown-box-list-item" href="group-timeline.html">
                    <!-- USER STATUS -->
                    <div class="user-status notification">
                        <!-- USER STATUS AVATAR -->
                        <div class="user-status-avatar">
                            <!-- USER AVATAR -->
                            <div class="user-avatar small no-border">
                                <!-- USER AVATAR CONTENT -->
                                <div class="user-avatar-content">
                                    <!-- HEXAGON -->
                                    <div class="hexagon-image-40-44" data-src="img/avatar/24.jpg"></div>
                                    <!-- /HEXAGON -->
                                </div>
                                <!-- /USER AVATAR CONTENT -->
                            </div>
                            <!-- /USER AVATAR -->
                        </div>
                        <!-- /USER STATUS AVATAR -->

                        <!-- USER STATUS TITLE -->
                        <p class="user-status-title"><span class="bold">Cosplayers of the World</span></p>
                        <!-- /USER STATUS TITLE -->

                        <!-- USER STATUS TEXT -->
                        <p class="user-status-text">139 members</p>
                        <!-- /USER STATUS TEXT -->

                        <!-- USER STATUS ICON -->
                        <div class="user-status-icon">
                            <!-- ICON GROUP -->
                            <svg class="icon-group">
                                <use xlink:href="#svg-group"></use>
                            </svg>
                            <!-- /ICON GROUP -->
                        </div>
                        <!-- /USER STATUS ICON -->
                    </div>
                    <!-- /USER STATUS -->
                </a>
                <!-- /DROPDOWN BOX LIST ITEM -->
            </div>
            <!-- /DROPDOWN BOX LIST -->

            <!-- DROPDOWN BOX CATEGORY -->
            <div class="dropdown-box-category">
                <!-- DROPDOWN BOX CATEGORY TITLE -->
                <p class="dropdown-box-category-title">Marketplace</p>
                <!-- /DROPDOWN BOX CATEGORY TITLE -->
            </div>
            <!-- /DROPDOWN BOX CATEGORY -->

            <!-- DROPDOWN BOX LIST -->
            <div class="dropdown-box-list small no-scroll">
                <!-- DROPDOWN BOX LIST ITEM -->
                <a class="dropdown-box-list-item" href="marketplace-product.html">
                    <!-- USER STATUS -->
                    <div class="user-status no-padding-top">
                        <!-- USER STATUS AVATAR -->
                        <div class="user-status-avatar">
                            <!-- PICTURE -->
                            <figure class="picture small round liquid">
                                <img src="img/marketplace/items/07.jpg" alt="item-07">
                            </figure>
                            <!-- /PICTURE -->
                        </div>
                        <!-- /USER STATUS AVATAR -->

                        <!-- USER STATUS TITLE -->
                        <p class="user-status-title"><span class="bold">Mercenaries White Frame</span></p>
                        <!-- /USER STATUS TITLE -->

                        <!-- USER STATUS TEXT -->
                        <p class="user-status-text">By Neko Bebop</p>
                        <!-- /USER STATUS TEXT -->

                        <!-- USER STATUS ICON -->
                        <div class="user-status-icon">
                            <!-- ICON MARKETPLACE -->
                            <svg class="icon-marketplace">
                                <use xlink:href="#svg-marketplace"></use>
                            </svg>
                            <!-- /ICON MARKETPLACE -->
                        </div>
                        <!-- /USER STATUS ICON -->
                    </div>
                    <!-- /USER STATUS -->
                </a>
                <!-- /DROPDOWN BOX LIST ITEM -->
            </div>
            <!-- /DROPDOWN BOX LIST -->
        </div>
        <!-- /DROPDOWN BOX -->
    </div>
    <!-- /HEADER ACTIONS -->

    <!-- HEADER ACTIONS -->
    <div class="header-actions">
        <!-- PROGRESS STAT -->
        <div class="progress-stat">
            <!-- BAR PROGRESS WRAP -->
            <div class="bar-progress-wrap">
                <!-- BAR PROGRESS INFO -->
                <p class="bar-progress-info">Next: <span class="bar-progress-text"></span></p>
                <!-- /BAR PROGRESS INFO -->
            </div>
            <!-- /BAR PROGRESS WRAP -->

            <!-- PROGRESS STAT BAR -->
            <div id="logged-user-level" class="progress-stat-bar"></div>
            <!-- /PROGRESS STAT BAR -->
        </div>
        <!-- /PROGRESS STAT -->
    </div>
    <!-- /HEADER ACTIONS -->

    <!-- HEADER ACTIONS -->
    <div class="header-actions">
        <!-- ACTION LIST -->
        <div class="action-list dark">
            <!-- ACTION LIST ITEM WRAP -->
            <div class="action-list-item-wrap">
                <!-- ACTION LIST ITEM -->
                <div class="action-list-item header-dropdown-trigger">
                    <!-- ACTION LIST ITEM ICON -->
                    <svg class="action-list-item-icon icon-shopping-bag">
                        <use xlink:href="#svg-shopping-bag"></use>
                    </svg>
                    <!-- /ACTION LIST ITEM ICON -->
                </div>
                <!-- /ACTION LIST ITEM -->

                <!-- DROPDOWN BOX -->
                <div class="dropdown-box no-padding-bottom header-dropdown">
                    <!-- DROPDOWN BOX HEADER -->
                    <div class="dropdown-box-header">
                        <!-- DROPDOWN BOX HEADER TITLE -->
                        <p class="dropdown-box-header-title">Shopping Cart <span class="highlighted">3</span></p>
                        <!-- /DROPDOWN BOX HEADER TITLE -->
                    </div>
                    <!-- /DROPDOWN BOX HEADER -->

                    <!-- DROPDOWN BOX LIST -->
                    <div class="dropdown-box-list scroll-small no-hover" data-simplebar>
                        <!-- DROPDOWN BOX LIST ITEM -->
                        <div class="dropdown-box-list-item">
                            <!-- CART ITEM PREVIEW -->
                            <div class="cart-item-preview">
                                <!-- CART ITEM PREVIEW IMAGE -->
                                <a class="cart-item-preview-image" href="marketplace-product.html">
                                    <!-- PICTURE -->
                                    <figure class="picture medium round liquid">
                                        <img src="img/marketplace/items/01.jpg" alt="item-01">
                                    </figure>
                                    <!-- /PICTURE -->
                                </a>
                                <!-- /CART ITEM PREVIEW IMAGE -->

                                <!-- CART ITEM PREVIEW TITLE -->
                                <p class="cart-item-preview-title"><a href="marketplace-product.html">Twitch Stream UI
                                        Pack</a></p>
                                <!-- /CART ITEM PREVIEW TITLE -->

                                <!-- CART ITEM PREVIEW TEXT -->
                                <p class="cart-item-preview-text">Regular License</p>
                                <!-- /CART ITEM PREVIEW TEXT -->

                                <!-- CART ITEM PREVIEW PRICE -->
                                <p class="cart-item-preview-price"><span class="highlighted">$</span> 12.00 x 1</p>
                                <!-- /CART ITEM PREVIEW PRICE -->

                                <!-- CART ITEM PREVIEW ACTION -->
                                <div class="cart-item-preview-action">
                                    <!-- ICON DELETE -->
                                    <svg class="icon-delete">
                                        <use xlink:href="#svg-delete"></use>
                                    </svg>
                                    <!-- /ICON DELETE -->
                                </div>
                                <!-- /CART ITEM PREVIEW ACTION -->
                            </div>
                            <!-- /CART ITEM PREVIEW -->
                        </div>
                        <!-- /DROPDOWN BOX LIST ITEM -->

                        <!-- DROPDOWN BOX LIST ITEM -->
                        <div class="dropdown-box-list-item">
                            <!-- CART ITEM PREVIEW -->
                            <div class="cart-item-preview">
                                <!-- CART ITEM PREVIEW IMAGE -->
                                <a class="cart-item-preview-image" href="marketplace-product.html">
                                    <!-- PICTURE -->
                                    <figure class="picture medium round liquid">
                                        <img src="img/marketplace/items/11.jpg" alt="item-11">
                                    </figure>
                                    <!-- /PICTURE -->
                                </a>
                                <!-- /CART ITEM PREVIEW IMAGE -->

                                <!-- CART ITEM PREVIEW TITLE -->
                                <p class="cart-item-preview-title"><a href="marketplace-product.html">Gaming Coin Badges
                                        Pack</a></p>
                                <!-- /CART ITEM PREVIEW TITLE -->

                                <!-- CART ITEM PREVIEW TEXT -->
                                <p class="cart-item-preview-text">Regular License</p>
                                <!-- /CART ITEM PREVIEW TEXT -->

                                <!-- CART ITEM PREVIEW PRICE -->
                                <p class="cart-item-preview-price"><span class="highlighted">$</span> 6.00 x 1</p>
                                <!-- /CART ITEM PREVIEW PRICE -->

                                <!-- CART ITEM PREVIEW ACTION -->
                                <div class="cart-item-preview-action">
                                    <!-- ICON DELETE -->
                                    <svg class="icon-delete">
                                        <use xlink:href="#svg-delete"></use>
                                    </svg>
                                    <!-- /ICON DELETE -->
                                </div>
                                <!-- /CART ITEM PREVIEW ACTION -->
                            </div>
                            <!-- /CART ITEM PREVIEW -->
                        </div>
                        <!-- /DROPDOWN BOX LIST ITEM -->

                        <!-- DROPDOWN BOX LIST ITEM -->
                        <div class="dropdown-box-list-item">
                            <!-- CART ITEM PREVIEW -->
                            <div class="cart-item-preview">
                                <!-- CART ITEM PREVIEW IMAGE -->
                                <a class="cart-item-preview-image" href="marketplace-product.html">
                                    <!-- PICTURE -->
                                    <figure class="picture medium round liquid">
                                        <img src="img/marketplace/items/10.jpg" alt="item-10">
                                    </figure>
                                    <!-- /PICTURE -->
                                </a>
                                <!-- /CART ITEM PREVIEW IMAGE -->

                                <!-- CART ITEM PREVIEW TITLE -->
                                <p class="cart-item-preview-title"><a href="marketplace-product.html">Twitch Stream UI
                                        Pack</a></p>
                                <!-- /CART ITEM PREVIEW TITLE -->

                                <!-- CART ITEM PREVIEW TEXT -->
                                <p class="cart-item-preview-text">Regular License</p>
                                <!-- /CART ITEM PREVIEW TEXT -->

                                <!-- CART ITEM PREVIEW PRICE -->
                                <p class="cart-item-preview-price"><span class="highlighted">$</span> 26.00 x 1</p>
                                <!-- /CART ITEM PREVIEW PRICE -->

                                <!-- CART ITEM PREVIEW ACTION -->
                                <div class="cart-item-preview-action">
                                    <!-- ICON DELETE -->
                                    <svg class="icon-delete">
                                        <use xlink:href="#svg-delete"></use>
                                    </svg>
                                    <!-- /ICON DELETE -->
                                </div>
                                <!-- /CART ITEM PREVIEW ACTION -->
                            </div>
                            <!-- /CART ITEM PREVIEW -->
                        </div>
                        <!-- /DROPDOWN BOX LIST ITEM -->

                        <!-- DROPDOWN BOX LIST ITEM -->
                        <div class="dropdown-box-list-item">
                            <!-- CART ITEM PREVIEW -->
                            <div class="cart-item-preview">
                                <!-- CART ITEM PREVIEW IMAGE -->
                                <a class="cart-item-preview-image" href="marketplace-product.html">
                                    <!-- PICTURE -->
                                    <figure class="picture medium round liquid">
                                        <img src="img/marketplace/items/04.jpg" alt="item-04">
                                    </figure>
                                    <!-- /PICTURE -->
                                </a>
                                <!-- /CART ITEM PREVIEW IMAGE -->

                                <!-- CART ITEM PREVIEW TITLE -->
                                <p class="cart-item-preview-title"><a href="marketplace-product.html">Generic Joystick
                                        Pack</a></p>
                                <!-- /CART ITEM PREVIEW TITLE -->

                                <!-- CART ITEM PREVIEW TEXT -->
                                <p class="cart-item-preview-text">Regular License</p>
                                <!-- /CART ITEM PREVIEW TEXT -->

                                <!-- CART ITEM PREVIEW PRICE -->
                                <p class="cart-item-preview-price"><span class="highlighted">$</span> 16.00 x 1</p>
                                <!-- /CART ITEM PREVIEW PRICE -->

                                <!-- CART ITEM PREVIEW ACTION -->
                                <div class="cart-item-preview-action">
                                    <!-- ICON DELETE -->
                                    <svg class="icon-delete">
                                        <use xlink:href="#svg-delete"></use>
                                    </svg>
                                    <!-- /ICON DELETE -->
                                </div>
                                <!-- /CART ITEM PREVIEW ACTION -->
                            </div>
                            <!-- /CART ITEM PREVIEW -->
                        </div>
                        <!-- /DROPDOWN BOX LIST ITEM -->
                    </div>
                    <!-- /DROPDOWN BOX LIST -->

                    <!-- CART PREVIEW TOTAL -->
                    <div class="cart-preview-total">
                        <!-- CART PREVIEW TOTAL TITLE -->
                        <p class="cart-preview-total-title">Total:</p>
                        <!-- /CART PREVIEW TOTAL TITLE -->

                        <!-- CART PREVIEW TOTAL TEXT -->
                        <p class="cart-preview-total-text"><span class="highlighted">$</span> 60.00</p>
                        <!-- /CART PREVIEW TOTAL TEXT -->
                    </div>
                    <!-- /CART PREVIEW TOTAL -->

                    <!-- DROPDOWN BOX ACTIONS -->
                    <div class="dropdown-box-actions">
                        <!-- DROPDOWN BOX ACTION -->
                        <div class="dropdown-box-action">
                            <!-- BUTTON -->
                            <a class="button secondary" href="marketplace-cart.html">Shopping Cart</a>
                            <!-- /BUTTON -->
                        </div>
                        <!-- /DROPDOWN BOX ACTION -->

                        <!-- DROPDOWN BOX ACTION -->
                        <div class="dropdown-box-action">
                            <!-- BUTTON -->
                            <a class="button primary" href="marketplace-checkout.html">Go to Checkout</a>
                            <!-- /BUTTON -->
                        </div>
                        <!-- /DROPDOWN BOX ACTION -->
                    </div>
                    <!-- /DROPDOWN BOX ACTIONS -->
                </div>
                <!-- /DROPDOWN BOX -->
            </div>
            <!-- /ACTION LIST ITEM WRAP -->

            <!-- ACTION LIST ITEM WRAP -->
            <div class="action-list-item-wrap">
                <!-- ACTION LIST ITEM -->
                <div class="action-list-item header-dropdown-trigger">
                    <!-- ACTION LIST ITEM ICON -->
                    <svg class="action-list-item-icon icon-friend">
                        <use xlink:href="#svg-friend"></use>
                    </svg>
                    <!-- /ACTION LIST ITEM ICON -->
                </div>
                <!-- /ACTION LIST ITEM -->

                <!-- DROPDOWN BOX -->
                <div class="dropdown-box header-dropdown">
                    <!-- DROPDOWN BOX HEADER -->
                    <div class="dropdown-box-header">
                        <!-- DROPDOWN BOX HEADER TITLE -->
                        <p class="dropdown-box-header-title">Friend Requests</p>
                        <!-- /DROPDOWN BOX HEADER TITLE -->

                        <!-- DROPDOWN BOX HEADER ACTIONS -->
                        <div class="dropdown-box-header-actions">
                            <!-- DROPDOWN BOX HEADER ACTION -->
                            <p class="dropdown-box-header-action">Find Friends</p>
                            <!-- /DROPDOWN BOX HEADER ACTION -->

                            <!-- DROPDOWN BOX HEADER ACTION -->
                            <p class="dropdown-box-header-action">Settings</p>
                            <!-- /DROPDOWN BOX HEADER ACTION -->
                        </div>
                        <!-- /DROPDOWN BOX HEADER ACTIONS -->
                    </div>
                    <!-- /DROPDOWN BOX HEADER -->

                    <!-- DROPDOWN BOX LIST -->
                    <div class="dropdown-box-list no-hover" data-simplebar>
                        <!-- DROPDOWN BOX LIST ITEM -->
                        <div class="dropdown-box-list-item">
                            <!-- USER STATUS -->
                            <div class="user-status request">
                                <!-- USER STATUS AVATAR -->
                                <a class="user-status-avatar" href="profile-timeline.html">
                                    <!-- USER AVATAR -->
                                    <div class="user-avatar small no-outline">
                                        <!-- USER AVATAR CONTENT -->
                                        <div class="user-avatar-content">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-image-30-32" data-src="img/avatar/16.jpg"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR CONTENT -->

                                        <!-- USER AVATAR PROGRESS -->
                                        <div class="user-avatar-progress">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-progress-40-44"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR PROGRESS -->

                                        <!-- USER AVATAR PROGRESS BORDER -->
                                        <div class="user-avatar-progress-border">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-border-40-44"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR PROGRESS BORDER -->

                                        <!-- USER AVATAR BADGE -->
                                        <div class="user-avatar-badge">
                                            <!-- USER AVATAR BADGE BORDER -->
                                            <div class="user-avatar-badge-border">
                                                <!-- HEXAGON -->
                                                <div class="hexagon-22-24"></div>
                                                <!-- /HEXAGON -->
                                            </div>
                                            <!-- /USER AVATAR BADGE BORDER -->

                                            <!-- USER AVATAR BADGE CONTENT -->
                                            <div class="user-avatar-badge-content">
                                                <!-- HEXAGON -->
                                                <div class="hexagon-dark-16-18"></div>
                                                <!-- /HEXAGON -->
                                            </div>
                                            <!-- /USER AVATAR BADGE CONTENT -->

                                            <!-- USER AVATAR BADGE TEXT -->
                                            <p class="user-avatar-badge-text">14</p>
                                            <!-- /USER AVATAR BADGE TEXT -->
                                        </div>
                                        <!-- /USER AVATAR BADGE -->
                                    </div>
                                    <!-- /USER AVATAR -->
                                </a>
                                <!-- /USER STATUS AVATAR -->

                                <!-- USER STATUS TITLE -->
                                <p class="user-status-title"><a class="bold" href="profile-timeline.html">Ginny
                                        Danvers</a></p>
                                <!-- /USER STATUS TITLE -->

                                <!-- USER STATUS TEXT -->
                                <p class="user-status-text">6 friends in common</p>
                                <!-- /USER STATUS TEXT -->

                                <!-- ACTION REQUEST LIST -->
                                <div class="action-request-list">
                                    <!-- ACTION REQUEST -->
                                    <div class="action-request accept">
                                        <!-- ACTION REQUEST ICON -->
                                        <svg class="action-request-icon icon-add-friend">
                                            <use xlink:href="#svg-add-friend"></use>
                                        </svg>
                                        <!-- /ACTION REQUEST ICON -->
                                    </div>
                                    <!-- /ACTION REQUEST -->

                                    <!-- ACTION REQUEST -->
                                    <div class="action-request decline">
                                        <!-- ACTION REQUEST ICON -->
                                        <svg class="action-request-icon icon-remove-friend">
                                            <use xlink:href="#svg-remove-friend"></use>
                                        </svg>
                                        <!-- /ACTION REQUEST ICON -->
                                    </div>
                                    <!-- /ACTION REQUEST -->
                                </div>
                                <!-- ACTION REQUEST LIST -->
                            </div>
                            <!-- /USER STATUS -->
                        </div>
                        <!-- /DROPDOWN BOX LIST ITEM -->

                        <!-- DROPDOWN BOX LIST ITEM -->
                        <div class="dropdown-box-list-item">
                            <!-- USER STATUS -->
                            <div class="user-status request">
                                <!-- USER STATUS AVATAR -->
                                <a class="user-status-avatar" href="profile-timeline.html">
                                    <!-- USER AVATAR -->
                                    <div class="user-avatar small no-outline">
                                        <!-- USER AVATAR CONTENT -->
                                        <div class="user-avatar-content">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-image-30-32" data-src="img/avatar/14.jpg"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR CONTENT -->

                                        <!-- USER AVATAR PROGRESS -->
                                        <div class="user-avatar-progress">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-progress-40-44"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR PROGRESS -->

                                        <!-- USER AVATAR PROGRESS BORDER -->
                                        <div class="user-avatar-progress-border">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-border-40-44"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR PROGRESS BORDER -->

                                        <!-- USER AVATAR BADGE -->
                                        <div class="user-avatar-badge">
                                            <!-- USER AVATAR BADGE BORDER -->
                                            <div class="user-avatar-badge-border">
                                                <!-- HEXAGON -->
                                                <div class="hexagon-22-24"></div>
                                                <!-- /HEXAGON -->
                                            </div>
                                            <!-- /USER AVATAR BADGE BORDER -->

                                            <!-- USER AVATAR BADGE CONTENT -->
                                            <div class="user-avatar-badge-content">
                                                <!-- HEXAGON -->
                                                <div class="hexagon-dark-16-18"></div>
                                                <!-- /HEXAGON -->
                                            </div>
                                            <!-- /USER AVATAR BADGE CONTENT -->

                                            <!-- USER AVATAR BADGE TEXT -->
                                            <p class="user-avatar-badge-text">3</p>
                                            <!-- /USER AVATAR BADGE TEXT -->
                                        </div>
                                        <!-- /USER AVATAR BADGE -->
                                    </div>
                                    <!-- /USER AVATAR -->
                                </a>
                                <!-- /USER STATUS AVATAR -->

                                <!-- USER STATUS TITLE -->
                                <p class="user-status-title"><a class="bold" href="profile-timeline.html">Paul Lang</a>
                                </p>
                                <!-- /USER STATUS TITLE -->

                                <!-- USER STATUS TEXT -->
                                <p class="user-status-text">2 friends in common</p>
                                <!-- /USER STATUS TEXT -->

                                <!-- ACTION REQUEST LIST -->
                                <div class="action-request-list">
                                    <!-- ACTION REQUEST -->
                                    <div class="action-request accept">
                                        <!-- ACTION REQUEST ICON -->
                                        <svg class="action-request-icon icon-add-friend">
                                            <use xlink:href="#svg-add-friend"></use>
                                        </svg>
                                        <!-- /ACTION REQUEST ICON -->
                                    </div>
                                    <!-- /ACTION REQUEST -->

                                    <!-- ACTION REQUEST -->
                                    <div class="action-request decline">
                                        <!-- ACTION REQUEST ICON -->
                                        <svg class="action-request-icon icon-remove-friend">
                                            <use xlink:href="#svg-remove-friend"></use>
                                        </svg>
                                        <!-- /ACTION REQUEST ICON -->
                                    </div>
                                    <!-- /ACTION REQUEST -->
                                </div>
                                <!-- ACTION REQUEST LIST -->
                            </div>
                            <!-- /USER STATUS -->
                        </div>
                        <!-- /DROPDOWN BOX LIST ITEM -->

                        <!-- DROPDOWN BOX LIST ITEM -->
                        <div class="dropdown-box-list-item">
                            <!-- USER STATUS -->
                            <div class="user-status request">
                                <!-- USER STATUS AVATAR -->
                                <a class="user-status-avatar" href="profile-timeline.html">
                                    <!-- USER AVATAR -->
                                    <div class="user-avatar small no-outline">
                                        <!-- USER AVATAR CONTENT -->
                                        <div class="user-avatar-content">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-image-30-32" data-src="img/avatar/11.jpg"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR CONTENT -->

                                        <!-- USER AVATAR PROGRESS -->
                                        <div class="user-avatar-progress">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-progress-40-44"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR PROGRESS -->

                                        <!-- USER AVATAR PROGRESS BORDER -->
                                        <div class="user-avatar-progress-border">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-border-40-44"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR PROGRESS BORDER -->

                                        <!-- USER AVATAR BADGE -->
                                        <div class="user-avatar-badge">
                                            <!-- USER AVATAR BADGE BORDER -->
                                            <div class="user-avatar-badge-border">
                                                <!-- HEXAGON -->
                                                <div class="hexagon-22-24"></div>
                                                <!-- /HEXAGON -->
                                            </div>
                                            <!-- /USER AVATAR BADGE BORDER -->

                                            <!-- USER AVATAR BADGE CONTENT -->
                                            <div class="user-avatar-badge-content">
                                                <!-- HEXAGON -->
                                                <div class="hexagon-dark-16-18"></div>
                                                <!-- /HEXAGON -->
                                            </div>
                                            <!-- /USER AVATAR BADGE CONTENT -->

                                            <!-- USER AVATAR BADGE TEXT -->
                                            <p class="user-avatar-badge-text">9</p>
                                            <!-- /USER AVATAR BADGE TEXT -->
                                        </div>
                                        <!-- /USER AVATAR BADGE -->
                                    </div>
                                    <!-- /USER AVATAR -->
                                </a>
                                <!-- /USER STATUS AVATAR -->

                                <!-- USER STATUS TITLE -->
                                <p class="user-status-title"><a class="bold" href="profile-timeline.html">Cassie May</a>
                                </p>
                                <!-- /USER STATUS TITLE -->

                                <!-- USER STATUS TEXT -->
                                <p class="user-status-text">4 friends in common</p>
                                <!-- /USER STATUS TEXT -->

                                <!-- ACTION REQUEST LIST -->
                                <div class="action-request-list">
                                    <!-- ACTION REQUEST -->
                                    <div class="action-request accept">
                                        <!-- ACTION REQUEST ICON -->
                                        <svg class="action-request-icon icon-add-friend">
                                            <use xlink:href="#svg-add-friend"></use>
                                        </svg>
                                        <!-- /ACTION REQUEST ICON -->
                                    </div>
                                    <!-- /ACTION REQUEST -->

                                    <!-- ACTION REQUEST -->
                                    <div class="action-request decline">
                                        <!-- ACTION REQUEST ICON -->
                                        <svg class="action-request-icon icon-remove-friend">
                                            <use xlink:href="#svg-remove-friend"></use>
                                        </svg>
                                        <!-- /ACTION REQUEST ICON -->
                                    </div>
                                    <!-- /ACTION REQUEST -->
                                </div>
                                <!-- ACTION REQUEST LIST -->
                            </div>
                            <!-- /USER STATUS -->
                        </div>
                        <!-- /DROPDOWN BOX LIST ITEM -->
                    </div>
                    <!-- /DROPDOWN BOX LIST -->

                    <!-- DROPDOWN BOX BUTTON -->
                    <a class="dropdown-box-button secondary" href="hub-profile-requests.html">View all Requests</a>
                    <!-- /DROPDOWN BOX BUTTON -->
                </div>
                <!-- /DROPDOWN BOX -->
            </div>
            <!-- /ACTION LIST ITEM WRAP -->

            <!-- ACTION LIST ITEM WRAP -->
            <div class="action-list-item-wrap">
                <!-- ACTION LIST ITEM -->
                <div class="action-list-item header-dropdown-trigger">
                    <!-- ACTION LIST ITEM ICON -->
                    <svg class="action-list-item-icon icon-messages">
                        <use xlink:href="#svg-messages"></use>
                    </svg>
                    <!-- /ACTION LIST ITEM ICON -->
                </div>
                <!-- /ACTION LIST ITEM -->

                <!-- DROPDOWN BOX -->
                <div class="dropdown-box header-dropdown">
                    <!-- DROPDOWN BOX HEADER -->
                    <div class="dropdown-box-header">
                        <!-- DROPDOWN BOX HEADER TITLE -->
                        <p class="dropdown-box-header-title">Messages</p>
                        <!-- /DROPDOWN BOX HEADER TITLE -->

                        <!-- DROPDOWN BOX HEADER ACTIONS -->
                        <div class="dropdown-box-header-actions">
                            <!-- DROPDOWN BOX HEADER ACTION -->
                            <p class="dropdown-box-header-action">Mark all as Read</p>
                            <!-- /DROPDOWN BOX HEADER ACTION -->

                            <!-- DROPDOWN BOX HEADER ACTION -->
                            <p class="dropdown-box-header-action">Settings</p>
                            <!-- /DROPDOWN BOX HEADER ACTION -->
                        </div>
                        <!-- /DROPDOWN BOX HEADER ACTIONS -->
                    </div>
                    <!-- /DROPDOWN BOX HEADER -->

                    <!-- DROPDOWN BOX LIST -->
                    <div class="dropdown-box-list medium" data-simplebar>
                        <!-- DROPDOWN BOX LIST ITEM -->
                        <a class="dropdown-box-list-item" href="hub-profile-messages.html">
                            <!-- USER STATUS -->
                            <div class="user-status">
                                <!-- USER STATUS AVATAR -->
                                <div class="user-status-avatar">
                                    <!-- USER AVATAR -->
                                    <div class="user-avatar small no-outline">
                                        <!-- USER AVATAR CONTENT -->
                                        <div class="user-avatar-content">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-image-30-32" data-src="img/avatar/04.jpg"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR CONTENT -->

                                        <!-- USER AVATAR PROGRESS -->
                                        <div class="user-avatar-progress">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-progress-40-44"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR PROGRESS -->

                                        <!-- USER AVATAR PROGRESS BORDER -->
                                        <div class="user-avatar-progress-border">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-border-40-44"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR PROGRESS BORDER -->

                                        <!-- USER AVATAR BADGE -->
                                        <div class="user-avatar-badge">
                                            <!-- USER AVATAR BADGE BORDER -->
                                            <div class="user-avatar-badge-border">
                                                <!-- HEXAGON -->
                                                <div class="hexagon-22-24"></div>
                                                <!-- /HEXAGON -->
                                            </div>
                                            <!-- /USER AVATAR BADGE BORDER -->

                                            <!-- USER AVATAR BADGE CONTENT -->
                                            <div class="user-avatar-badge-content">
                                                <!-- HEXAGON -->
                                                <div class="hexagon-dark-16-18"></div>
                                                <!-- /HEXAGON -->
                                            </div>
                                            <!-- /USER AVATAR BADGE CONTENT -->

                                            <!-- USER AVATAR BADGE TEXT -->
                                            <p class="user-avatar-badge-text">6</p>
                                            <!-- /USER AVATAR BADGE TEXT -->
                                        </div>
                                        <!-- /USER AVATAR BADGE -->
                                    </div>
                                    <!-- /USER AVATAR -->
                                </div>
                                <!-- /USER STATUS AVATAR -->

                                <!-- USER STATUS TITLE -->
                                <p class="user-status-title"><span class="bold">Bearded Wonder</span></p>
                                <!-- /USER STATUS TITLE -->

                                <!-- USER STATUS TEXT -->
                                <p class="user-status-text">Great! Then will meet with them at the party...</p>
                                <!-- /USER STATUS TEXT -->

                                <!-- USER STATUS TIMESTAMP -->
                                <p class="user-status-timestamp floaty">29 mins ago</p>
                                <!-- /USER STATUS TIMESTAMP -->
                            </div>
                            <!-- /USER STATUS -->
                        </a>
                        <!-- /DROPDOWN BOX LIST ITEM -->

                        <!-- DROPDOWN BOX LIST ITEM -->
                        <a class="dropdown-box-list-item" href="hub-profile-messages.html">
                            <!-- USER STATUS -->
                            <div class="user-status">
                                <!-- USER STATUS AVATAR -->
                                <div class="user-status-avatar">
                                    <!-- USER AVATAR -->
                                    <div class="user-avatar small no-outline">
                                        <!-- USER AVATAR CONTENT -->
                                        <div class="user-avatar-content">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-image-30-32" data-src="img/avatar/05.jpg"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR CONTENT -->

                                        <!-- USER AVATAR PROGRESS -->
                                        <div class="user-avatar-progress">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-progress-40-44"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR PROGRESS -->

                                        <!-- USER AVATAR PROGRESS BORDER -->
                                        <div class="user-avatar-progress-border">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-border-40-44"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR PROGRESS BORDER -->

                                        <!-- USER AVATAR BADGE -->
                                        <div class="user-avatar-badge">
                                            <!-- USER AVATAR BADGE BORDER -->
                                            <div class="user-avatar-badge-border">
                                                <!-- HEXAGON -->
                                                <div class="hexagon-22-24"></div>
                                                <!-- /HEXAGON -->
                                            </div>
                                            <!-- /USER AVATAR BADGE BORDER -->

                                            <!-- USER AVATAR BADGE CONTENT -->
                                            <div class="user-avatar-badge-content">
                                                <!-- HEXAGON -->
                                                <div class="hexagon-dark-16-18"></div>
                                                <!-- /HEXAGON -->
                                            </div>
                                            <!-- /USER AVATAR BADGE CONTENT -->

                                            <!-- USER AVATAR BADGE TEXT -->
                                            <p class="user-avatar-badge-text">12</p>
                                            <!-- /USER AVATAR BADGE TEXT -->
                                        </div>
                                        <!-- /USER AVATAR BADGE -->
                                    </div>
                                    <!-- /USER AVATAR -->
                                </div>
                                <!-- /USER STATUS AVATAR -->

                                <!-- USER STATUS TITLE -->
                                <p class="user-status-title"><span class="bold">Neko Bebop</span></p>
                                <!-- /USER STATUS TITLE -->

                                <!-- USER STATUS TEXT -->
                                <p class="user-status-text">Awesome! I'll see you there!</p>
                                <!-- /USER STATUS TEXT -->

                                <!-- USER STATUS TIMESTAMP -->
                                <p class="user-status-timestamp floaty">54 mins ago</p>
                                <!-- /USER STATUS TIMESTAMP -->
                            </div>
                            <!-- /USER STATUS -->
                        </a>
                        <!-- /DROPDOWN BOX LIST ITEM -->

                        <!-- DROPDOWN BOX LIST ITEM -->
                        <a class="dropdown-box-list-item" href="hub-profile-messages.html">
                            <!-- USER STATUS -->
                            <div class="user-status">
                                <!-- USER STATUS AVATAR -->
                                <div class="user-status-avatar">
                                    <!-- USER AVATAR -->
                                    <div class="user-avatar small no-outline">
                                        <!-- USER AVATAR CONTENT -->
                                        <div class="user-avatar-content">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-image-30-32" data-src="img/avatar/03.jpg"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR CONTENT -->

                                        <!-- USER AVATAR PROGRESS -->
                                        <div class="user-avatar-progress">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-progress-40-44"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR PROGRESS -->

                                        <!-- USER AVATAR PROGRESS BORDER -->
                                        <div class="user-avatar-progress-border">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-border-40-44"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR PROGRESS BORDER -->

                                        <!-- USER AVATAR BADGE -->
                                        <div class="user-avatar-badge">
                                            <!-- USER AVATAR BADGE BORDER -->
                                            <div class="user-avatar-badge-border">
                                                <!-- HEXAGON -->
                                                <div class="hexagon-22-24"></div>
                                                <!-- /HEXAGON -->
                                            </div>
                                            <!-- /USER AVATAR BADGE BORDER -->

                                            <!-- USER AVATAR BADGE CONTENT -->
                                            <div class="user-avatar-badge-content">
                                                <!-- HEXAGON -->
                                                <div class="hexagon-dark-16-18"></div>
                                                <!-- /HEXAGON -->
                                            </div>
                                            <!-- /USER AVATAR BADGE CONTENT -->

                                            <!-- USER AVATAR BADGE TEXT -->
                                            <p class="user-avatar-badge-text">16</p>
                                            <!-- /USER AVATAR BADGE TEXT -->
                                        </div>
                                        <!-- /USER AVATAR BADGE -->
                                    </div>
                                    <!-- /USER AVATAR -->
                                </div>
                                <!-- /USER STATUS AVATAR -->

                                <!-- USER STATUS TITLE -->
                                <p class="user-status-title"><span class="bold">Nick Grissom</span></p>
                                <!-- /USER STATUS TITLE -->

                                <!-- USER STATUS TEXT -->
                                <p class="user-status-text">Can you stream that new game?</p>
                                <!-- /USER STATUS TEXT -->

                                <!-- USER STATUS TIMESTAMP -->
                                <p class="user-status-timestamp floaty">2 hours ago</p>
                                <!-- /USER STATUS TIMESTAMP -->
                            </div>
                            <!-- /USER STATUS -->
                        </a>
                        <!-- /DROPDOWN BOX LIST ITEM -->

                        <!-- DROPDOWN BOX LIST ITEM -->
                        <a class="dropdown-box-list-item" href="hub-profile-messages.html">
                            <!-- USER STATUS -->
                            <div class="user-status">
                                <!-- USER STATUS AVATAR -->
                                <div class="user-status-avatar">
                                    <!-- USER AVATAR -->
                                    <div class="user-avatar small no-outline">
                                        <!-- USER AVATAR CONTENT -->
                                        <div class="user-avatar-content">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-image-30-32" data-src="img/avatar/07.jpg"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR CONTENT -->

                                        <!-- USER AVATAR PROGRESS -->
                                        <div class="user-avatar-progress">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-progress-40-44"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR PROGRESS -->

                                        <!-- USER AVATAR PROGRESS BORDER -->
                                        <div class="user-avatar-progress-border">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-border-40-44"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR PROGRESS BORDER -->

                                        <!-- USER AVATAR BADGE -->
                                        <div class="user-avatar-badge">
                                            <!-- USER AVATAR BADGE BORDER -->
                                            <div class="user-avatar-badge-border">
                                                <!-- HEXAGON -->
                                                <div class="hexagon-22-24"></div>
                                                <!-- /HEXAGON -->
                                            </div>
                                            <!-- /USER AVATAR BADGE BORDER -->

                                            <!-- USER AVATAR BADGE CONTENT -->
                                            <div class="user-avatar-badge-content">
                                                <!-- HEXAGON -->
                                                <div class="hexagon-dark-16-18"></div>
                                                <!-- /HEXAGON -->
                                            </div>
                                            <!-- /USER AVATAR BADGE CONTENT -->

                                            <!-- USER AVATAR BADGE TEXT -->
                                            <p class="user-avatar-badge-text">26</p>
                                            <!-- /USER AVATAR BADGE TEXT -->
                                        </div>
                                        <!-- /USER AVATAR BADGE -->
                                    </div>
                                    <!-- /USER AVATAR -->
                                </div>
                                <!-- /USER STATUS AVATAR -->

                                <!-- USER STATUS TITLE -->
                                <p class="user-status-title"><span class="bold">Sarah Diamond</span></p>
                                <!-- /USER STATUS TITLE -->

                                <!-- USER STATUS TEXT -->
                                <p class="user-status-text">I'm sending you the latest news of the release...</p>
                                <!-- /USER STATUS TEXT -->

                                <!-- USER STATUS TIMESTAMP -->
                                <p class="user-status-timestamp floaty">16 hours ago</p>
                                <!-- /USER STATUS TIMESTAMP -->
                            </div>
                            <!-- /USER STATUS -->
                        </a>
                        <!-- /DROPDOWN BOX LIST ITEM -->

                        <!-- DROPDOWN BOX LIST ITEM -->
                        <a class="dropdown-box-list-item" href="hub-profile-messages.html">
                            <!-- USER STATUS -->
                            <div class="user-status">
                                <!-- USER STATUS AVATAR -->
                                <div class="user-status-avatar">
                                    <!-- USER AVATAR -->
                                    <div class="user-avatar small no-outline">
                                        <!-- USER AVATAR CONTENT -->
                                        <div class="user-avatar-content">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-image-30-32" data-src="img/avatar/12.jpg"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR CONTENT -->

                                        <!-- USER AVATAR PROGRESS -->
                                        <div class="user-avatar-progress">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-progress-40-44"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR PROGRESS -->

                                        <!-- USER AVATAR PROGRESS BORDER -->
                                        <div class="user-avatar-progress-border">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-border-40-44"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR PROGRESS BORDER -->

                                        <!-- USER AVATAR BADGE -->
                                        <div class="user-avatar-badge">
                                            <!-- USER AVATAR BADGE BORDER -->
                                            <div class="user-avatar-badge-border">
                                                <!-- HEXAGON -->
                                                <div class="hexagon-22-24"></div>
                                                <!-- /HEXAGON -->
                                            </div>
                                            <!-- /USER AVATAR BADGE BORDER -->

                                            <!-- USER AVATAR BADGE CONTENT -->
                                            <div class="user-avatar-badge-content">
                                                <!-- HEXAGON -->
                                                <div class="hexagon-dark-16-18"></div>
                                                <!-- /HEXAGON -->
                                            </div>
                                            <!-- /USER AVATAR BADGE CONTENT -->

                                            <!-- USER AVATAR BADGE TEXT -->
                                            <p class="user-avatar-badge-text">10</p>
                                            <!-- /USER AVATAR BADGE TEXT -->
                                        </div>
                                        <!-- /USER AVATAR BADGE -->
                                    </div>
                                    <!-- /USER AVATAR -->
                                </div>
                                <!-- /USER STATUS AVATAR -->

                                <!-- USER STATUS TITLE -->
                                <p class="user-status-title"><span class="bold">James Murdock</span></p>
                                <!-- /USER STATUS TITLE -->

                                <!-- USER STATUS TEXT -->
                                <p class="user-status-text">Great! Then will meet with them at the party...</p>
                                <!-- /USER STATUS TEXT -->

                                <!-- USER STATUS TIMESTAMP -->
                                <p class="user-status-timestamp floaty">7 days ago</p>
                                <!-- /USER STATUS TIMESTAMP -->
                            </div>
                            <!-- /USER STATUS -->
                        </a>
                        <!-- /DROPDOWN BOX LIST ITEM -->

                        <!-- DROPDOWN BOX LIST ITEM -->
                        <a class="dropdown-box-list-item" href="hub-profile-messages.html">
                            <!-- USER STATUS -->
                            <div class="user-status">
                                <!-- USER STATUS AVATAR -->
                                <div class="user-status-avatar">
                                    <!-- USER AVATAR -->
                                    <div class="user-avatar small no-outline">
                                        <!-- USER AVATAR CONTENT -->
                                        <div class="user-avatar-content">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-image-30-32" data-src="img/avatar/10.jpg"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR CONTENT -->

                                        <!-- USER AVATAR PROGRESS -->
                                        <div class="user-avatar-progress">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-progress-40-44"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR PROGRESS -->

                                        <!-- USER AVATAR PROGRESS BORDER -->
                                        <div class="user-avatar-progress-border">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-border-40-44"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR PROGRESS BORDER -->

                                        <!-- USER AVATAR BADGE -->
                                        <div class="user-avatar-badge">
                                            <!-- USER AVATAR BADGE BORDER -->
                                            <div class="user-avatar-badge-border">
                                                <!-- HEXAGON -->
                                                <div class="hexagon-22-24"></div>
                                                <!-- /HEXAGON -->
                                            </div>
                                            <!-- /USER AVATAR BADGE BORDER -->

                                            <!-- USER AVATAR BADGE CONTENT -->
                                            <div class="user-avatar-badge-content">
                                                <!-- HEXAGON -->
                                                <div class="hexagon-dark-16-18"></div>
                                                <!-- /HEXAGON -->
                                            </div>
                                            <!-- /USER AVATAR BADGE CONTENT -->

                                            <!-- USER AVATAR BADGE TEXT -->
                                            <p class="user-avatar-badge-text">5</p>
                                            <!-- /USER AVATAR BADGE TEXT -->
                                        </div>
                                        <!-- /USER AVATAR BADGE -->
                                    </div>
                                    <!-- /USER AVATAR -->
                                </div>
                                <!-- /USER STATUS AVATAR -->

                                <!-- USER STATUS TITLE -->
                                <p class="user-status-title"><span class="bold">The Green Goo</span></p>
                                <!-- /USER STATUS TITLE -->

                                <!-- USER STATUS TEXT -->
                                <p class="user-status-text">Can you stream that new game?</p>
                                <!-- /USER STATUS TEXT -->

                                <!-- USER STATUS TIMESTAMP -->
                                <p class="user-status-timestamp floaty">10 days ago</p>
                                <!-- /USER STATUS TIMESTAMP -->
                            </div>
                            <!-- /USER STATUS -->
                        </a>
                        <!-- /DROPDOWN BOX LIST ITEM -->
                    </div>
                    <!-- /DROPDOWN BOX LIST -->

                    <!-- DROPDOWN BOX BUTTON -->
                    <a class="dropdown-box-button primary" href="hub-profile-messages.html">View all Messages</a>
                    <!-- /DROPDOWN BOX BUTTON -->
                </div>
                <!-- /DROPDOWN BOX -->
            </div>
            <!-- /ACTION LIST ITEM WRAP -->

            <!-- ACTION LIST ITEM WRAP -->
            <div class="action-list-item-wrap">
                <!-- ACTION LIST ITEM -->
                <div class="action-list-item unread header-dropdown-trigger">
                    <!-- ACTION LIST ITEM ICON -->
                    <svg class="action-list-item-icon icon-notification">
                        <use xlink:href="#svg-notification"></use>
                    </svg>
                    <!-- /ACTION LIST ITEM ICON -->
                </div>
                <!-- /ACTION LIST ITEM -->

                <!-- DROPDOWN BOX -->
                <div class="dropdown-box header-dropdown">
                    <!-- DROPDOWN BOX HEADER -->
                    <div class="dropdown-box-header">
                        <!-- DROPDOWN BOX HEADER TITLE -->
                        <p class="dropdown-box-header-title">Notifications</p>
                        <!-- /DROPDOWN BOX HEADER TITLE -->

                        <!-- DROPDOWN BOX HEADER ACTIONS -->
                        <div class="dropdown-box-header-actions">
                            <!-- DROPDOWN BOX HEADER ACTION -->
                            <p class="dropdown-box-header-action">Mark all as Read</p>
                            <!-- /DROPDOWN BOX HEADER ACTION -->

                            <!-- DROPDOWN BOX HEADER ACTION -->
                            <p class="dropdown-box-header-action">Settings</p>
                            <!-- /DROPDOWN BOX HEADER ACTION -->
                        </div>
                        <!-- /DROPDOWN BOX HEADER ACTIONS -->
                    </div>
                    <!-- /DROPDOWN BOX HEADER -->

                    <!-- DROPDOWN BOX LIST -->
                    <div class="dropdown-box-list" data-simplebar>
                        <!-- DROPDOWN BOX LIST ITEM -->
                        <div class="dropdown-box-list-item unread">
                            <!-- USER STATUS -->
                            <div class="user-status notification">
                                <!-- USER STATUS AVATAR -->
                                <a class="user-status-avatar" href="profile-timeline.html">
                                    <!-- USER AVATAR -->
                                    <div class="user-avatar small no-outline">
                                        <!-- USER AVATAR CONTENT -->
                                        <div class="user-avatar-content">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-image-30-32" data-src="img/avatar/03.jpg"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR CONTENT -->

                                        <!-- USER AVATAR PROGRESS -->
                                        <div class="user-avatar-progress">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-progress-40-44"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR PROGRESS -->

                                        <!-- USER AVATAR PROGRESS BORDER -->
                                        <div class="user-avatar-progress-border">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-border-40-44"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR PROGRESS BORDER -->

                                        <!-- USER AVATAR BADGE -->
                                        <div class="user-avatar-badge">
                                            <!-- USER AVATAR BADGE BORDER -->
                                            <div class="user-avatar-badge-border">
                                                <!-- HEXAGON -->
                                                <div class="hexagon-22-24"></div>
                                                <!-- /HEXAGON -->
                                            </div>
                                            <!-- /USER AVATAR BADGE BORDER -->

                                            <!-- USER AVATAR BADGE CONTENT -->
                                            <div class="user-avatar-badge-content">
                                                <!-- HEXAGON -->
                                                <div class="hexagon-dark-16-18"></div>
                                                <!-- /HEXAGON -->
                                            </div>
                                            <!-- /USER AVATAR BADGE CONTENT -->

                                            <!-- USER AVATAR BADGE TEXT -->
                                            <p class="user-avatar-badge-text">16</p>
                                            <!-- /USER AVATAR BADGE TEXT -->
                                        </div>
                                        <!-- /USER AVATAR BADGE -->
                                    </div>
                                    <!-- /USER AVATAR -->
                                </a>
                                <!-- /USER STATUS AVATAR -->

                                <!-- USER STATUS TITLE -->
                                <p class="user-status-title"><a class="bold" href="profile-timeline.html">Nick
                                        Grissom</a> posted a comment on your <a class="highlighted"
                                                                                href="profile-timeline.html">status
                                        update</a></p>
                                <!-- /USER STATUS TITLE -->

                                <!-- USER STATUS TIMESTAMP -->
                                <p class="user-status-timestamp">2 minutes ago</p>
                                <!-- /USER STATUS TIMESTAMP -->

                                <!-- USER STATUS ICON -->
                                <div class="user-status-icon">
                                    <!-- ICON COMMENT -->
                                    <svg class="icon-comment">
                                        <use xlink:href="#svg-comment"></use>
                                    </svg>
                                    <!-- /ICON COMMENT -->
                                </div>
                                <!-- /USER STATUS ICON -->
                            </div>
                            <!-- /USER STATUS -->
                        </div>
                        <!-- /DROPDOWN BOX LIST ITEM -->

                        <!-- DROPDOWN BOX LIST ITEM -->
                        <div class="dropdown-box-list-item">
                            <!-- USER STATUS -->
                            <div class="user-status notification">
                                <!-- USER STATUS AVATAR -->
                                <a class="user-status-avatar" href="profile-timeline.html">
                                    <!-- USER AVATAR -->
                                    <div class="user-avatar small no-outline">
                                        <!-- USER AVATAR CONTENT -->
                                        <div class="user-avatar-content">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-image-30-32" data-src="img/avatar/07.jpg"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR CONTENT -->

                                        <!-- USER AVATAR PROGRESS -->
                                        <div class="user-avatar-progress">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-progress-40-44"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR PROGRESS -->

                                        <!-- USER AVATAR PROGRESS BORDER -->
                                        <div class="user-avatar-progress-border">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-border-40-44"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR PROGRESS BORDER -->

                                        <!-- USER AVATAR BADGE -->
                                        <div class="user-avatar-badge">
                                            <!-- USER AVATAR BADGE BORDER -->
                                            <div class="user-avatar-badge-border">
                                                <!-- HEXAGON -->
                                                <div class="hexagon-22-24"></div>
                                                <!-- /HEXAGON -->
                                            </div>
                                            <!-- /USER AVATAR BADGE BORDER -->

                                            <!-- USER AVATAR BADGE CONTENT -->
                                            <div class="user-avatar-badge-content">
                                                <!-- HEXAGON -->
                                                <div class="hexagon-dark-16-18"></div>
                                                <!-- /HEXAGON -->
                                            </div>
                                            <!-- /USER AVATAR BADGE CONTENT -->

                                            <!-- USER AVATAR BADGE TEXT -->
                                            <p class="user-avatar-badge-text">26</p>
                                            <!-- /USER AVATAR BADGE TEXT -->
                                        </div>
                                        <!-- /USER AVATAR BADGE -->
                                    </div>
                                    <!-- /USER AVATAR -->
                                </a>
                                <!-- /USER STATUS AVATAR -->

                                <!-- USER STATUS TITLE -->
                                <p class="user-status-title"><a class="bold" href="profile-timeline.html">Sarah
                                        Diamond</a> left a like <img class="reaction" src="img/reaction/like.png"
                                                                     alt="reaction-like"> reaction on your <a
                                        class="highlighted" href="profile-timeline.html">status update</a></p>
                                <!-- /USER STATUS TITLE -->

                                <!-- USER STATUS TIMESTAMP -->
                                <p class="user-status-timestamp">17 minutes ago</p>
                                <!-- /USER STATUS TIMESTAMP -->

                                <!-- USER STATUS ICON -->
                                <div class="user-status-icon">
                                    <!-- ICON THUMBS UP -->
                                    <svg class="icon-thumbs-up">
                                        <use xlink:href="#svg-thumbs-up"></use>
                                    </svg>
                                    <!-- /ICON THUMBS UP -->
                                </div>
                                <!-- /USER STATUS ICON -->
                            </div>
                            <!-- /USER STATUS -->
                        </div>
                        <!-- /DROPDOWN BOX LIST ITEM -->

                        <!-- DROPDOWN BOX LIST ITEM -->
                        <div class="dropdown-box-list-item">
                            <!-- USER STATUS -->
                            <div class="user-status notification">
                                <!-- USER STATUS AVATAR -->
                                <a class="user-status-avatar" href="profile-timeline.html">
                                    <!-- USER AVATAR -->
                                    <div class="user-avatar small no-outline">
                                        <!-- USER AVATAR CONTENT -->
                                        <div class="user-avatar-content">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-image-30-32" data-src="img/avatar/02.jpg"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR CONTENT -->

                                        <!-- USER AVATAR PROGRESS -->
                                        <div class="user-avatar-progress">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-progress-40-44"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR PROGRESS -->

                                        <!-- USER AVATAR PROGRESS BORDER -->
                                        <div class="user-avatar-progress-border">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-border-40-44"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR PROGRESS BORDER -->

                                        <!-- USER AVATAR BADGE -->
                                        <div class="user-avatar-badge">
                                            <!-- USER AVATAR BADGE BORDER -->
                                            <div class="user-avatar-badge-border">
                                                <!-- HEXAGON -->
                                                <div class="hexagon-22-24"></div>
                                                <!-- /HEXAGON -->
                                            </div>
                                            <!-- /USER AVATAR BADGE BORDER -->

                                            <!-- USER AVATAR BADGE CONTENT -->
                                            <div class="user-avatar-badge-content">
                                                <!-- HEXAGON -->
                                                <div class="hexagon-dark-16-18"></div>
                                                <!-- /HEXAGON -->
                                            </div>
                                            <!-- /USER AVATAR BADGE CONTENT -->

                                            <!-- USER AVATAR BADGE TEXT -->
                                            <p class="user-avatar-badge-text">13</p>
                                            <!-- /USER AVATAR BADGE TEXT -->
                                        </div>
                                        <!-- /USER AVATAR BADGE -->
                                    </div>
                                    <!-- /USER AVATAR -->
                                </a>
                                <!-- /USER STATUS AVATAR -->

                                <!-- USER STATUS TITLE -->
                                <p class="user-status-title"><a class="bold" href="profile-timeline.html">Destroy
                                        Dex</a> posted a comment on your <a class="highlighted"
                                                                            href="profile-photos.html">photo</a></p>
                                <!-- /USER STATUS TITLE -->

                                <!-- USER STATUS TIMESTAMP -->
                                <p class="user-status-timestamp">31 minutes ago</p>
                                <!-- /USER STATUS TIMESTAMP -->

                                <!-- USER STATUS ICON -->
                                <div class="user-status-icon">
                                    <!-- ICON COMMENT -->
                                    <svg class="icon-comment">
                                        <use xlink:href="#svg-comment"></use>
                                    </svg>
                                    <!-- /ICON COMMENT -->
                                </div>
                                <!-- /USER STATUS ICON -->
                            </div>
                            <!-- /USER STATUS -->
                        </div>
                        <!-- /DROPDOWN BOX LIST ITEM -->

                        <!-- DROPDOWN BOX LIST ITEM -->
                        <div class="dropdown-box-list-item">
                            <!-- USER STATUS -->
                            <div class="user-status notification">
                                <!-- USER STATUS AVATAR -->
                                <a class="user-status-avatar" href="profile-timeline.html">
                                    <!-- USER AVATAR -->
                                    <div class="user-avatar small no-outline">
                                        <!-- USER AVATAR CONTENT -->
                                        <div class="user-avatar-content">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-image-30-32" data-src="img/avatar/10.jpg"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR CONTENT -->

                                        <!-- USER AVATAR PROGRESS -->
                                        <div class="user-avatar-progress">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-progress-40-44"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR PROGRESS -->

                                        <!-- USER AVATAR PROGRESS BORDER -->
                                        <div class="user-avatar-progress-border">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-border-40-44"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR PROGRESS BORDER -->

                                        <!-- USER AVATAR BADGE -->
                                        <div class="user-avatar-badge">
                                            <!-- USER AVATAR BADGE BORDER -->
                                            <div class="user-avatar-badge-border">
                                                <!-- HEXAGON -->
                                                <div class="hexagon-22-24"></div>
                                                <!-- /HEXAGON -->
                                            </div>
                                            <!-- /USER AVATAR BADGE BORDER -->

                                            <!-- USER AVATAR BADGE CONTENT -->
                                            <div class="user-avatar-badge-content">
                                                <!-- HEXAGON -->
                                                <div class="hexagon-dark-16-18"></div>
                                                <!-- /HEXAGON -->
                                            </div>
                                            <!-- /USER AVATAR BADGE CONTENT -->

                                            <!-- USER AVATAR BADGE TEXT -->
                                            <p class="user-avatar-badge-text">5</p>
                                            <!-- /USER AVATAR BADGE TEXT -->
                                        </div>
                                        <!-- /USER AVATAR BADGE -->
                                    </div>
                                    <!-- /USER AVATAR -->
                                </a>
                                <!-- /USER STATUS AVATAR -->

                                <!-- USER STATUS TITLE -->
                                <p class="user-status-title"><a class="bold" href="profile-timeline.html">The Green
                                        Goo</a> left a love <img class="reaction" src="img/reaction/love.png"
                                                                 alt="reaction-love"> reaction on your <a
                                        class="highlighted" href="profile-timeline.html">status update</a></p>
                                <!-- /USER STATUS TITLE -->

                                <!-- USER STATUS TIMESTAMP -->
                                <p class="user-status-timestamp">2 hours ago</p>
                                <!-- /USER STATUS TIMESTAMP -->

                                <!-- USER STATUS ICON -->
                                <div class="user-status-icon">
                                    <!-- ICON THUMBS UP -->
                                    <svg class="icon-thumbs-up">
                                        <use xlink:href="#svg-thumbs-up"></use>
                                    </svg>
                                    <!-- /ICON THUMBS UP -->
                                </div>
                                <!-- /USER STATUS ICON -->
                            </div>
                            <!-- /USER STATUS -->
                        </div>
                        <!-- /DROPDOWN BOX LIST ITEM -->

                        <!-- DROPDOWN BOX LIST ITEM -->
                        <div class="dropdown-box-list-item">
                            <!-- USER STATUS -->
                            <div class="user-status notification">
                                <!-- USER STATUS AVATAR -->
                                <a class="user-status-avatar" href="profile-timeline.html">
                                    <!-- USER AVATAR -->
                                    <div class="user-avatar small no-outline">
                                        <!-- USER AVATAR CONTENT -->
                                        <div class="user-avatar-content">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-image-30-32" data-src="img/avatar/05.jpg"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR CONTENT -->

                                        <!-- USER AVATAR PROGRESS -->
                                        <div class="user-avatar-progress">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-progress-40-44"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR PROGRESS -->

                                        <!-- USER AVATAR PROGRESS BORDER -->
                                        <div class="user-avatar-progress-border">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-border-40-44"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR PROGRESS BORDER -->

                                        <!-- USER AVATAR BADGE -->
                                        <div class="user-avatar-badge">
                                            <!-- USER AVATAR BADGE BORDER -->
                                            <div class="user-avatar-badge-border">
                                                <!-- HEXAGON -->
                                                <div class="hexagon-22-24"></div>
                                                <!-- /HEXAGON -->
                                            </div>
                                            <!-- /USER AVATAR BADGE BORDER -->

                                            <!-- USER AVATAR BADGE CONTENT -->
                                            <div class="user-avatar-badge-content">
                                                <!-- HEXAGON -->
                                                <div class="hexagon-dark-16-18"></div>
                                                <!-- /HEXAGON -->
                                            </div>
                                            <!-- /USER AVATAR BADGE CONTENT -->

                                            <!-- USER AVATAR BADGE TEXT -->
                                            <p class="user-avatar-badge-text">12</p>
                                            <!-- /USER AVATAR BADGE TEXT -->
                                        </div>
                                        <!-- /USER AVATAR BADGE -->
                                    </div>
                                    <!-- /USER AVATAR -->
                                </a>
                                <!-- /USER STATUS AVATAR -->

                                <!-- USER STATUS TITLE -->
                                <p class="user-status-title"><a class="bold" href="profile-timeline.html">Neko Bebop</a>
                                    posted a comment on your <a class="highlighted" href="profile-timeline.html">status
                                        update</a></p>
                                <!-- /USER STATUS TITLE -->

                                <!-- USER STATUS TIMESTAMP -->
                                <p class="user-status-timestamp">3 hours ago</p>
                                <!-- /USER STATUS TIMESTAMP -->

                                <!-- USER STATUS ICON -->
                                <div class="user-status-icon">
                                    <!-- ICON COMMENT -->
                                    <svg class="icon-comment">
                                        <use xlink:href="#svg-comment"></use>
                                    </svg>
                                    <!-- /ICON COMMENT -->
                                </div>
                                <!-- /USER STATUS ICON -->
                            </div>
                            <!-- /USER STATUS -->
                        </div>
                        <!-- /DROPDOWN BOX LIST ITEM -->
                    </div>
                    <!-- /DROPDOWN BOX LIST -->

                    <!-- DROPDOWN BOX BUTTON -->
                    <a class="dropdown-box-button secondary" href="hub-profile-notifications.html">View all
                        Notifications</a>
                    <!-- /DROPDOWN BOX BUTTON -->
                </div>
                <!-- /DROPDOWN BOX -->
            </div>
            <!-- /ACTION LIST ITEM WRAP -->
        </div>
        <!-- /ACTION LIST -->

        <!-- ACTION ITEM WRAP -->
        <div class="action-item-wrap">
            <!-- ACTION ITEM -->
            <div class="action-item dark header-settings-dropdown-trigger">
                <!-- ACTION ITEM ICON -->
                <svg class="action-item-icon icon-settings">
                    <use xlink:href="#svg-settings"></use>
                </svg>
                <!-- /ACTION ITEM ICON -->
            </div>
            <!-- /ACTION ITEM -->

            <!-- DROPDOWN NAVIGATION -->
            <div class="dropdown-navigation header-settings-dropdown">
                <!-- DROPDOWN NAVIGATION HEADER -->
                <div class="dropdown-navigation-header">
                    <!-- USER STATUS -->
                    <div class="user-status">
                        <!-- USER STATUS AVATAR -->
                        <a class="user-status-avatar" href="profile-timeline.html">
                            <!-- USER AVATAR -->
                            <div class="user-avatar small no-outline">
                                <!-- USER AVATAR CONTENT -->
                                <div class="user-avatar-content">
                                    <!-- HEXAGON -->
                                    <div class="hexagon-image-30-32" data-src="img/avatar/01.jpg"></div>
                                    <!-- /HEXAGON -->
                                </div>
                                <!-- /USER AVATAR CONTENT -->

                                <!-- USER AVATAR PROGRESS -->
                                <div class="user-avatar-progress">
                                    <!-- HEXAGON -->
                                    <div class="hexagon-progress-40-44"></div>
                                    <!-- /HEXAGON -->
                                </div>
                                <!-- /USER AVATAR PROGRESS -->

                                <!-- USER AVATAR PROGRESS BORDER -->
                                <div class="user-avatar-progress-border">
                                    <!-- HEXAGON -->
                                    <div class="hexagon-border-40-44"></div>
                                    <!-- /HEXAGON -->
                                </div>
                                <!-- /USER AVATAR PROGRESS BORDER -->

                                <!-- USER AVATAR BADGE -->
                                <div class="user-avatar-badge">
                                    <!-- USER AVATAR BADGE BORDER -->
                                    <div class="user-avatar-badge-border">
                                        <!-- HEXAGON -->
                                        <div class="hexagon-22-24"></div>
                                        <!-- /HEXAGON -->
                                    </div>
                                    <!-- /USER AVATAR BADGE BORDER -->

                                    <!-- USER AVATAR BADGE CONTENT -->
                                    <div class="user-avatar-badge-content">
                                        <!-- HEXAGON -->
                                        <div class="hexagon-dark-16-18"></div>
                                        <!-- /HEXAGON -->
                                    </div>
                                    <!-- /USER AVATAR BADGE CONTENT -->

                                    <!-- USER AVATAR BADGE TEXT -->
                                    <p class="user-avatar-badge-text">24</p>
                                    <!-- /USER AVATAR BADGE TEXT -->
                                </div>
                                <!-- /USER AVATAR BADGE -->
                            </div>
                            <!-- /USER AVATAR -->
                        </a>
                        <!-- /USER STATUS AVATAR -->

                        <!-- USER STATUS TITLE -->
                        <p class="user-status-title"><span class="bold">Hi Marina!</span></p>
                        <!-- /USER STATUS TITLE -->

                        <!-- USER STATUS TEXT -->
                        <p class="user-status-text small"><a href="profile-timeline.html">@marinavalentine</a></p>
                        <!-- /USER STATUS TEXT -->
                    </div>
                    <!-- /USER STATUS -->
                </div>
                <!-- /DROPDOWN NAVIGATION HEADER -->

                <!-- DROPDOWN NAVIGATION CATEGORY -->
                <p class="dropdown-navigation-category">My Profile</p>
                <!-- /DROPDOWN NAVIGATION CATEGORY -->

                <!-- DROPDOWN NAVIGATION LINK -->
                <a class="dropdown-navigation-link" href="hub-profile-info.html">Profile Info</a>
                <!-- /DROPDOWN NAVIGATION LINK -->

                <!-- DROPDOWN NAVIGATION LINK -->
                <a class="dropdown-navigation-link" href="hub-profile-social.html">Social &amp; Stream</a>
                <!-- /DROPDOWN NAVIGATION LINK -->

                <!-- DROPDOWN NAVIGATION LINK -->
                <a class="dropdown-navigation-link" href="hub-profile-notifications.html">Notifications</a>
                <!-- /DROPDOWN NAVIGATION LINK -->

                <!-- DROPDOWN NAVIGATION LINK -->
                <a class="dropdown-navigation-link" href="hub-profile-messages.html">Messages</a>
                <!-- /DROPDOWN NAVIGATION LINK -->

                <!-- DROPDOWN NAVIGATION LINK -->
                <a class="dropdown-navigation-link" href="hub-profile-requests.html">Friend Requests</a>
                <!-- /DROPDOWN NAVIGATION LINK -->

                <!-- DROPDOWN NAVIGATION CATEGORY -->
                <p class="dropdown-navigation-category">Account</p>
                <!-- /DROPDOWN NAVIGATION CATEGORY -->

                <!-- DROPDOWN NAVIGATION LINK -->
                <a class="dropdown-navigation-link" href="hub-account-info.html">Account Info</a>
                <!-- /DROPDOWN NAVIGATION LINK -->

                <!-- DROPDOWN NAVIGATION LINK -->
                <a class="dropdown-navigation-link" href="hub-account-password.html">Change Password</a>
                <!-- /DROPDOWN NAVIGATION LINK -->

                <!-- DROPDOWN NAVIGATION LINK -->
                <a class="dropdown-navigation-link" href="hub-account-settings.html">General Settings</a>
                <!-- /DROPDOWN NAVIGATION LINK -->

                <!-- DROPDOWN NAVIGATION CATEGORY -->
                <p class="dropdown-navigation-category">Groups</p>
                <!-- /DROPDOWN NAVIGATION CATEGORY -->

                <!-- DROPDOWN NAVIGATION LINK -->
                <a class="dropdown-navigation-link" href="hub-group-management.html">Manage Groups</a>
                <!-- /DROPDOWN NAVIGATION LINK -->

                <!-- DROPDOWN NAVIGATION LINK -->
                <a class="dropdown-navigation-link" href="hub-group-invitations.html">Invitations</a>
                <!-- /DROPDOWN NAVIGATION LINK -->

                <!-- DROPDOWN NAVIGATION CATEGORY -->
                <p class="dropdown-navigation-category">My Store</p>
                <!-- /DROPDOWN NAVIGATION CATEGORY -->

                <!-- DROPDOWN NAVIGATION LINK -->
                <a class="dropdown-navigation-link" href="hub-store-account.html">My Account <span class="highlighted">$250,32</span></a>
                <!-- /DROPDOWN NAVIGATION LINK -->

                <!-- DROPDOWN NAVIGATION LINK -->
                <a class="dropdown-navigation-link" href="hub-store-statement.html">Sales Statement</a>
                <!-- /DROPDOWN NAVIGATION LINK -->

                <!-- DROPDOWN NAVIGATION LINK -->
                <a class="dropdown-navigation-link" href="hub-store-items.html">Manage Items</a>
                <!-- /DROPDOWN NAVIGATION LINK -->

                <!-- DROPDOWN NAVIGATION LINK -->
                <a class="dropdown-navigation-link" href="hub-store-downloads.html">Downloads</a>
                <!-- /DROPDOWN NAVIGATION LINK -->

                <!-- DROPDOWN NAVIGATION BUTTON -->
                <p class="dropdown-navigation-button button small secondary">Logout</p>
                <!-- /DROPDOWN NAVIGATION BUTTON -->
            </div>
            <!-- /DROPDOWN NAVIGATION -->
        </div>
        <!-- /ACTION ITEM WRAP -->
    </div>
    <!-- /HEADER ACTIONS -->
</header>
<!-- /HEADER -->

<!-- FLOATY BAR -->
<aside class="floaty-bar">
    <!-- BAR ACTIONS -->
    <div class="bar-actions">
        <!-- PROGRESS STAT -->
        <div class="progress-stat">
            <!-- BAR PROGRESS WRAP -->
            <div class="bar-progress-wrap">
                <!-- BAR PROGRESS INFO -->
                <p class="bar-progress-info">Next: <span class="bar-progress-text"></span></p>
                <!-- /BAR PROGRESS INFO -->
            </div>
            <!-- /BAR PROGRESS WRAP -->

            <!-- PROGRESS STAT BAR -->
            <div id="logged-user-level-cp" class="progress-stat-bar"></div>
            <!-- /PROGRESS STAT BAR -->
        </div>
        <!-- /PROGRESS STAT -->
    </div>
    <!-- /BAR ACTIONS -->

    <!-- BAR ACTIONS -->
    <div class="bar-actions">
        <!-- ACTION LIST -->
        <div class="action-list dark">
            <!-- ACTION LIST ITEM -->
            <a class="action-list-item" href="marketplace-cart.html">
                <!-- ACTION LIST ITEM ICON -->
                <svg class="action-list-item-icon icon-shopping-bag">
                    <use xlink:href="#svg-shopping-bag"></use>
                </svg>
                <!-- /ACTION LIST ITEM ICON -->
            </a>
            <!-- /ACTION LIST ITEM -->

            <!-- ACTION LIST ITEM -->
            <a class="action-list-item" href="hub-profile-requests.html">
                <!-- ACTION LIST ITEM ICON -->
                <svg class="action-list-item-icon icon-friend">
                    <use xlink:href="#svg-friend"></use>
                </svg>
                <!-- /ACTION LIST ITEM ICON -->
            </a>
            <!-- /ACTION LIST ITEM -->

            <!-- ACTION LIST ITEM -->
            <a class="action-list-item" href="hub-profile-messages.html">
                <!-- ACTION LIST ITEM ICON -->
                <svg class="action-list-item-icon icon-messages">
                    <use xlink:href="#svg-messages"></use>
                </svg>
                <!-- /ACTION LIST ITEM ICON -->
            </a>
            <!-- /ACTION LIST ITEM -->

            <!-- ACTION LIST ITEM -->
            <a class="action-list-item unread" href="hub-profile-notifications.html">
                <!-- ACTION LIST ITEM ICON -->
                <svg class="action-list-item-icon icon-notification">
                    <use xlink:href="#svg-notification"></use>
                </svg>
                <!-- /ACTION LIST ITEM ICON -->
            </a>
            <!-- /ACTION LIST ITEM -->
        </div>
        <!-- /ACTION LIST -->

        <!-- ACTION ITEM WRAP -->
        <a class="action-item-wrap" href="hub-profile-info.html">
            <!-- ACTION ITEM -->
            <div class="action-item dark">
                <!-- ACTION ITEM ICON -->
                <svg class="action-item-icon icon-settings">
                    <use xlink:href="#svg-settings"></use>
                </svg>
                <!-- /ACTION ITEM ICON -->
            </div>
            <!-- /ACTION ITEM -->
        </a>
        <!-- /ACTION ITEM WRAP -->
    </div>
    <!-- /BAR ACTIONS -->
</aside>
<!-- /FLOATY BAR -->

<!-- CONTENT GRID -->
<div class="content-grid">
    <!-- SECTION BANNER -->
    <div class="section-banner">
        <!-- SECTION BANNER ICON -->
        <img class="section-banner-icon" src="img/banner/marketplace-icon.png" alt="marketplace-icon">
        <!-- /SECTION BANNER ICON -->

        <!-- SECTION BANNER TITLE -->
        <p class="section-banner-title">Marketplace</p>
        <!-- /SECTION BANNER TITLE -->

        <!-- SECTION BANNER TEXT -->
        <p class="section-banner-text">The best place for the community to buy and sell!</p>
        <!-- /SECTION BANNER TEXT -->
    </div>
    <!-- /SECTION BANNER -->

    <!-- SECTION HEADER -->
    <div class="section-header">
        <!-- SECTION HEADER INFO -->
        <div class="section-header-info">
            <!-- SECTION PRETITLE -->
            <p class="section-pretitle">HTML Templates</p>
            <!-- /SECTION PRETITLE -->

            <!-- SECTION TITLE -->
            <h2 class="section-title">Pixel Diamond Gaming Magazine</h2>
            <!-- /SECTION TITLE -->
        </div>
        <!-- /SECTION HEADER INFO -->

        <!-- SECTION HEADER ACTIONS -->
        <div class="section-header-actions">
            <!-- SECTION HEADER SUBSECTION -->
            <a class="section-header-subsection" href="marketplace.html">Marketplace</a>
            <!-- /SECTION HEADER SUBSECTION -->

            <!-- SECTION HEADER SUBSECTION -->
            <a class="section-header-subsection" href="marketplace-category.html">Digital Items</a>
            <!-- /SECTION HEADER SUBSECTION -->

            <!-- SECTION HEADER SUBSECTION -->
            <p class="section-header-subsection">Pixel Diamond Gaming Magazine</p>
            <!-- /SECTION HEADER SUBSECTION -->
        </div>
        <!-- /SECTION HEADER ACTIONS -->
    </div>
    <!-- /SECTION HEADER -->

    <!-- GRID -->
    <div class="grid grid-9-3">
        <!-- MARKETPLACE CONTENT -->
        <div class="marketplace-content grid-column">
            <!-- SLIDER PANEL -->
            <div class="slider-panel">
                <!-- SLIDER PANEL SLIDES -->
                <div id="product-box-slider-items" class="slider-panel-slides">
                    <!-- SLIDER PANEL SLIDE -->
                    <div class="slider-panel-slide">
                        <!-- SLIDER PANEL SLIDE IMAGE -->
                        <figure class="slider-panel-slide-image liquid">
                            <img src="img/marketplace/items/10.jpg" alt="item-10">
                        </figure>
                        <!-- /SLIDER PANEL SLIDE IMAGE -->
                    </div>
                    <!-- /SLIDER PANEL SLIDE -->

                    <!-- SLIDER PANEL SLIDE -->
                    <div class="slider-panel-slide">
                        <!-- SLIDER PANEL SLIDE IMAGE -->
                        <figure class="slider-panel-slide-image liquid">
                            <img src="img/marketplace/items/15.jpg" alt="item-15">
                        </figure>
                        <!-- /SLIDER PANEL SLIDE IMAGE -->
                    </div>
                    <!-- /SLIDER PANEL SLIDE -->

                    <!-- SLIDER PANEL SLIDE -->
                    <div class="slider-panel-slide">
                        <!-- SLIDER PANEL SLIDE IMAGE -->
                        <figure class="slider-panel-slide-image liquid">
                            <img src="img/marketplace/items/16.jpg" alt="item-16">
                        </figure>
                        <!-- /SLIDER PANEL SLIDE IMAGE -->
                    </div>
                    <!-- /SLIDER PANEL SLIDE -->

                    <!-- SLIDER PANEL SLIDE -->
                    <div class="slider-panel-slide">
                        <!-- SLIDER PANEL SLIDE IMAGE -->
                        <figure class="slider-panel-slide-image liquid">
                            <img src="img/marketplace/items/17.jpg" alt="item-17">
                        </figure>
                        <!-- /SLIDER PANEL SLIDE IMAGE -->
                    </div>
                    <!-- /SLIDER PANEL SLIDE -->

                    <!-- SLIDER PANEL SLIDE -->
                    <div class="slider-panel-slide">
                        <!-- SLIDER PANEL SLIDE IMAGE -->
                        <figure class="slider-panel-slide-image liquid">
                            <img src="img/marketplace/items/18.jpg" alt="item-18">
                        </figure>
                        <!-- /SLIDER PANEL SLIDE IMAGE -->
                    </div>
                    <!-- /SLIDER PANEL SLIDE -->

                    <!-- SLIDER PANEL SLIDE -->
                    <div class="slider-panel-slide">
                        <!-- SLIDER PANEL SLIDE IMAGE -->
                        <figure class="slider-panel-slide-image liquid">
                            <img src="img/marketplace/items/19.jpg" alt="item-19">
                        </figure>
                        <!-- /SLIDER PANEL SLIDE IMAGE -->
                    </div>
                    <!-- /SLIDER PANEL SLIDE -->

                    <!-- SLIDER PANEL SLIDE -->
                    <div class="slider-panel-slide">
                        <!-- SLIDER PANEL SLIDE IMAGE -->
                        <figure class="slider-panel-slide-image liquid">
                            <img src="img/marketplace/items/20.jpg" alt="item-20">
                        </figure>
                        <!-- /SLIDER PANEL SLIDE IMAGE -->
                    </div>
                    <!-- /SLIDER PANEL SLIDE -->
                </div>
                <!-- /SLIDER PANEL SLIDES -->

                <!-- SLIDER PANEL ROSTER -->
                <div class="slider-panel-roster">
                    <!-- SLIDER CONTROLS -->
                    <div id="product-box-slider-controls" class="slider-controls">
                        <!-- SLIDER CONTROL -->
                        <div class="slider-control left">
                            <!-- SLIDER CONTROL ICON -->
                            <svg class="slider-control-icon icon-small-arrow">
                                <use xlink:href="#svg-small-arrow"></use>
                            </svg>
                            <!-- /SLIDER CONTROL ICON -->
                        </div>
                        <!-- /SLIDER CONTROL -->

                        <!-- SLIDER CONTROL -->
                        <div class="slider-control right">
                            <!-- SLIDER CONTROL ICON -->
                            <svg class="slider-control-icon icon-small-arrow">
                                <use xlink:href="#svg-small-arrow"></use>
                            </svg>
                            <!-- /SLIDER CONTROL ICON -->
                        </div>
                        <!-- /SLIDER CONTROL -->
                    </div>
                    <!-- /SLIDER CONTROLS -->

                    <!-- BUTTON -->
                    <a class="button secondary"
                       href="https://themeforest.net/item/pixel-diamond-html-esports-gaming-magazine-community/23798711"
                       target="_blank">Live Preview</a>
                    <!-- /BUTTON -->

                    <!-- ROSTER PICTURES -->
                    <div id="product-box-slider-roster" class="roster-pictures">
                        <!-- ROSTER PICTURE -->
                        <div class="roster-picture">
                            <!-- ROSTER PICTURE IMAGE -->
                            <figure class="roster-picture-image liquid">
                                <img src="img/marketplace/items/10.jpg" alt="item-10">
                            </figure>
                            <!-- /ROSTER PICTURE IMAGE -->
                        </div>
                        <!-- /ROSTER PICTURE -->

                        <!-- ROSTER PICTURE -->
                        <div class="roster-picture">
                            <!-- ROSTER PICTURE IMAGE -->
                            <figure class="roster-picture-image liquid">
                                <img src="img/marketplace/items/15.jpg" alt="item-15">
                            </figure>
                            <!-- /ROSTER PICTURE IMAGE -->
                        </div>
                        <!-- /ROSTER PICTURE -->

                        <!-- ROSTER PICTURE -->
                        <div class="roster-picture">
                            <!-- ROSTER PICTURE IMAGE -->
                            <figure class="roster-picture-image liquid">
                                <img src="img/marketplace/items/16.jpg" alt="item-16">
                            </figure>
                            <!-- /ROSTER PICTURE IMAGE -->
                        </div>
                        <!-- /ROSTER PICTURE -->

                        <!-- ROSTER PICTURE -->
                        <div class="roster-picture">
                            <!-- ROSTER PICTURE IMAGE -->
                            <figure class="roster-picture-image liquid">
                                <img src="img/marketplace/items/17.jpg" alt="item-17">
                            </figure>
                            <!-- /ROSTER PICTURE IMAGE -->
                        </div>
                        <!-- /ROSTER PICTURE -->

                        <!-- ROSTER PICTURE -->
                        <div class="roster-picture">
                            <!-- ROSTER PICTURE IMAGE -->
                            <figure class="roster-picture-image liquid">
                                <img src="img/marketplace/items/18.jpg" alt="item-18">
                            </figure>
                            <!-- /ROSTER PICTURE IMAGE -->
                        </div>
                        <!-- /ROSTER PICTURE -->

                        <!-- ROSTER PICTURE -->
                        <div class="roster-picture">
                            <!-- ROSTER PICTURE IMAGE -->
                            <figure class="roster-picture-image liquid">
                                <img src="img/marketplace/items/19.jpg" alt="item-19">
                            </figure>
                            <!-- /ROSTER PICTURE IMAGE -->
                        </div>
                        <!-- /ROSTER PICTURE -->

                        <!-- ROSTER PICTURE -->
                        <div class="roster-picture">
                            <!-- ROSTER PICTURE IMAGE -->
                            <figure class="roster-picture-image liquid">
                                <img src="img/marketplace/items/20.jpg" alt="item-20">
                            </figure>
                            <!-- /ROSTER PICTURE IMAGE -->
                        </div>
                        <!-- /ROSTER PICTURE -->
                    </div>
                    <!-- /ROSTER PICTURES -->
                </div>
                <!-- /SLIDER PANEL ROSTER -->
            </div>
            <!-- /SLIDER PANEL -->

            <!-- TAB BOX -->
            <div class="tab-box">
                <!-- TAB BOX OPTIONS -->
                <div class="tab-box-options">
                    <!-- TAB BOX OPTION -->
                    <div class="tab-box-option">
                        <!-- TAB BOX OPTION TITLE -->
                        <p class="tab-box-option-title">Description</p>
                        <!-- /TAB BOX OPTION TITLE -->
                    </div>
                    <!-- /TAB BOX OPTION -->

                    <!-- TAB BOX OPTION -->
                    <div class="tab-box-option">
                        <!-- TAB BOX OPTION TITLE -->
                        <p class="tab-box-option-title">Comments <span class="highlighted">4</span></p>
                        <!-- /TAB BOX OPTION TITLE -->
                    </div>
                    <!-- /TAB BOX OPTION -->

                    <!-- TAB BOX OPTION -->
                    <div class="tab-box-option">
                        <!-- TAB BOX OPTION TITLE -->
                        <p class="tab-box-option-title">Reviews <span class="highlighted">3</span></p>
                        <!-- /TAB BOX OPTION TITLE -->
                    </div>
                    <!-- /TAB BOX OPTION -->
                </div>
                <!-- /TAB BOX OPTIONS -->

                <!-- TAB BOX ITEMS -->
                <div class="tab-box-items">
                    <!-- TAB BOX ITEM -->
                    <div class="tab-box-item">
                        <!-- TAB BOX ITEM CONTENT -->
                        <div class="tab-box-item-content">
                            <!-- TAB BOX ITEM TITLE -->
                            <p class="tab-box-item-title">The Best eSports and Gaming Magazine Template!</p>
                            <!-- /TAB BOX ITEM TITLE -->

                            <!-- TAB BOX ITEM PARAGRAPH -->
                            <p class="tab-box-item-paragraph">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
                                veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                consequat. Duis aute irure dolor in henderit in voluptate velit esse cillum dolore eu
                                fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                                culpa.</p>
                            <!-- /TAB BOX ITEM PARAGRAPH -->

                            <!-- TAB BOX ITEM PARAGRAPH -->
                            <p class="tab-box-item-paragraph">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
                                veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore
                                eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa
                                qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur
                                adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                                enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                                commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                                dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                                culpa qui officia deserunt mollit anim id est laborum.</p>
                            <!-- /TAB BOX ITEM PARAGRAPH -->

                            <!-- TAB BOX ITEM TITLE -->
                            <p class="tab-box-item-title">356+ HTML Elements Library included</p>
                            <!-- /TAB BOX ITEM TITLE -->

                            <!-- TAB BOX ITEM PARAGRAPH -->
                            <p class="tab-box-item-paragraph">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
                                veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                consequat. Duis aute irure dolor in henderit in voluptate velit esse cillum dolore eu
                                fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
                                officia deserunt mollit anim id est laborum.</p>
                            <!-- /TAB BOX ITEM PARAGRAPH -->

                            <!-- TAB BOX ITEM TITLE -->
                            <p class="tab-box-item-title">Included in the Pack:</p>
                            <!-- /TAB BOX ITEM TITLE -->

                            <!-- BULLET ITEM LIST -->
                            <ul class="bullet-item-list">
                                <!-- BULLET ITEM -->
                                <li class="bullet-item">
                                    <!-- BULLET ITEM ICON -->
                                    <svg class="bullet-item-icon icon-check">
                                        <use xlink:href="#svg-check"></use>
                                    </svg>
                                    <!-- /BULLET ITEM ICON -->

                                    <!-- BULLET ITEM TEXT -->
                                    <p class="bullet-item-text">64 HTML Files</p>
                                    <!-- /BULLET ITEM TEXT -->
                                </li>
                                <!-- /BULLET ITEM -->

                                <!-- BULLET ITEM -->
                                <li class="bullet-item">
                                    <!-- BULLET ITEM ICON -->
                                    <svg class="bullet-item-icon icon-check">
                                        <use xlink:href="#svg-check"></use>
                                    </svg>
                                    <!-- /BULLET ITEM ICON -->

                                    <!-- BULLET ITEM TEXT -->
                                    <p class="bullet-item-text">CSS and JS Elements Library with 365+ Items!</p>
                                    <!-- /BULLET ITEM TEXT -->
                                </li>
                                <!-- /BULLET ITEM -->

                                <!-- BULLET ITEM -->
                                <li class="bullet-item">
                                    <!-- BULLET ITEM ICON -->
                                    <svg class="bullet-item-icon icon-check">
                                        <use xlink:href="#svg-check"></use>
                                    </svg>
                                    <!-- /BULLET ITEM ICON -->

                                    <!-- BULLET ITEM TEXT -->
                                    <p class="bullet-item-text">63 PSD Files Included SAVE $12</p>
                                    <!-- /BULLET ITEM TEXT -->
                                </li>
                                <!-- /BULLET ITEM -->

                                <!-- BULLET ITEM -->
                                <li class="bullet-item">
                                    <!-- BULLET ITEM ICON -->
                                    <svg class="bullet-item-icon icon-check">
                                        <use xlink:href="#svg-check"></use>
                                    </svg>
                                    <!-- /BULLET ITEM ICON -->

                                    <!-- BULLET ITEM TEXT -->
                                    <p class="bullet-item-text">Easy template customization using npm & grunt
                                        (optional)</p>
                                    <!-- /BULLET ITEM TEXT -->
                                </li>
                                <!-- /BULLET ITEM -->

                                <!-- BULLET ITEM -->
                                <li class="bullet-item">
                                    <!-- BULLET ITEM ICON -->
                                    <svg class="bullet-item-icon icon-check">
                                        <use xlink:href="#svg-check"></use>
                                    </svg>
                                    <!-- /BULLET ITEM ICON -->

                                    <!-- BULLET ITEM TEXT -->
                                    <p class="bullet-item-text">Vector Illustrations Included</p>
                                    <!-- /BULLET ITEM TEXT -->
                                </li>
                                <!-- /BULLET ITEM -->

                                <!-- BULLET ITEM -->
                                <li class="bullet-item">
                                    <!-- BULLET ITEM ICON -->
                                    <svg class="bullet-item-icon icon-check">
                                        <use xlink:href="#svg-check"></use>
                                    </svg>
                                    <!-- /BULLET ITEM ICON -->

                                    <!-- BULLET ITEM TEXT -->
                                    <p class="bullet-item-text">84+ eSports Widgets</p>
                                    <!-- /BULLET ITEM TEXT -->
                                </li>
                                <!-- /BULLET ITEM -->

                                <!-- BULLET ITEM -->
                                <li class="bullet-item">
                                    <!-- BULLET ITEM ICON -->
                                    <svg class="bullet-item-icon icon-check">
                                        <use xlink:href="#svg-check"></use>
                                    </svg>
                                    <!-- /BULLET ITEM ICON -->

                                    <!-- BULLET ITEM TEXT -->
                                    <p class="bullet-item-text">Custom Plugins</p>
                                    <!-- /BULLET ITEM TEXT -->
                                </li>
                                <!-- /BULLET ITEM -->

                                <!-- BULLET ITEM -->
                                <li class="bullet-item">
                                    <!-- BULLET ITEM ICON -->
                                    <svg class="bullet-item-icon icon-check">
                                        <use xlink:href="#svg-check"></use>
                                    </svg>
                                    <!-- /BULLET ITEM ICON -->

                                    <!-- BULLET ITEM TEXT -->
                                    <p class="bullet-item-text">Fully Responsive</p>
                                    <!-- /BULLET ITEM TEXT -->
                                </li>
                                <!-- /BULLET ITEM -->

                                <!-- BULLET ITEM -->
                                <li class="bullet-item">
                                    <!-- BULLET ITEM ICON -->
                                    <svg class="bullet-item-icon icon-check">
                                        <use xlink:href="#svg-check"></use>
                                    </svg>
                                    <!-- /BULLET ITEM ICON -->

                                    <!-- BULLET ITEM TEXT -->
                                    <p class="bullet-item-text">Google Fonts</p>
                                    <!-- /BULLET ITEM TEXT -->
                                </li>
                                <!-- /BULLET ITEM -->
                            </ul>
                            <!-- /BULLET ITEM LIST -->
                        </div>
                        <!-- /TAB BOX ITEM CONTENT -->
                    </div>
                    <!-- /TAB BOX ITEM -->

                    <!-- TAB BOX ITEM -->
                    <div class="tab-box-item">
                        <!-- POST COMMENT LIST -->
                        <div class="post-comment-list no-border-top">
                            <!-- POST COMMENT -->
                            <div class="post-comment">
                                <!-- USER AVATAR -->
                                <a class="user-avatar small no-outline" href="profile-timeline.html">
                                    <!-- USER AVATAR CONTENT -->
                                    <div class="user-avatar-content">
                                        <!-- HEXAGON -->
                                        <div class="hexagon-image-30-32" data-src="img/avatar/04.jpg"></div>
                                        <!-- /HEXAGON -->
                                    </div>
                                    <!-- /USER AVATAR CONTENT -->

                                    <!-- USER AVATAR PROGRESS -->
                                    <div class="user-avatar-progress">
                                        <!-- HEXAGON -->
                                        <div class="hexagon-progress-40-44"></div>
                                        <!-- /HEXAGON -->
                                    </div>
                                    <!-- /USER AVATAR PROGRESS -->

                                    <!-- USER AVATAR PROGRESS BORDER -->
                                    <div class="user-avatar-progress-border">
                                        <!-- HEXAGON -->
                                        <div class="hexagon-border-40-44"></div>
                                        <!-- /HEXAGON -->
                                    </div>
                                    <!-- /USER AVATAR PROGRESS BORDER -->

                                    <!-- USER AVATAR BADGE -->
                                    <div class="user-avatar-badge">
                                        <!-- USER AVATAR BADGE BORDER -->
                                        <div class="user-avatar-badge-border">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-22-24"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR BADGE BORDER -->

                                        <!-- USER AVATAR BADGE CONTENT -->
                                        <div class="user-avatar-badge-content">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-dark-16-18"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR BADGE CONTENT -->

                                        <!-- USER AVATAR BADGE TEXT -->
                                        <p class="user-avatar-badge-text">6</p>
                                        <!-- /USER AVATAR BADGE TEXT -->
                                    </div>
                                    <!-- /USER AVATAR BADGE -->
                                </a>
                                <!-- /USER AVATAR -->

                                <!-- POST COMMENT TEXT -->
                                <p class="post-comment-text user-tag purchased"><a class="post-comment-text-author"
                                                                                   href="profile-timeline.html">Bearded
                                        Wonder</a></p>
                                <!-- /POST COMMENT TEXT -->

                                <!-- POST COMMENT TEXT -->
                                <p class="post-comment-text">Hi! I just purchased this item and I have a question, does
                                    it have the PSD files included? Thanks!</p>
                                <!-- /POST COMMENT TEXT -->

                                <!-- CONTENT ACTIONS -->
                                <div class="content-actions">
                                    <!-- CONTENT ACTION -->
                                    <div class="content-action">
                                        <!-- META LINE -->
                                        <div class="meta-line">
                                            <!-- META LINE LIST -->
                                            <div class="meta-line-list reaction-item-list small">
                                                <!-- REACTION ITEM -->
                                                <div class="reaction-item">
                                                    <!-- REACTION IMAGE -->
                                                    <img class="reaction-image reaction-item-dropdown-trigger"
                                                         src="img/reaction/happy.png" alt="reaction-happy">
                                                    <!-- /REACTION IMAGE -->

                                                    <!-- SIMPLE DROPDOWN -->
                                                    <div class="simple-dropdown padded reaction-item-dropdown">
                                                        <!-- SIMPLE DROPDOWN TEXT -->
                                                        <p class="simple-dropdown-text"><img class="reaction"
                                                                                             src="img/reaction/happy.png"
                                                                                             alt="reaction-happy"> <span
                                                                class="bold">Happy</span></p>
                                                        <!-- /SIMPLE DROPDOWN TEXT -->

                                                        <!-- SIMPLE DROPDOWN TEXT -->
                                                        <p class="simple-dropdown-text">Marcus Jhonson</p>
                                                        <!-- /SIMPLE DROPDOWN TEXT -->
                                                    </div>
                                                    <!-- /SIMPLE DROPDOWN -->
                                                </div>
                                                <!-- /REACTION ITEM -->

                                                <!-- REACTION ITEM -->
                                                <div class="reaction-item">
                                                    <!-- REACTION IMAGE -->
                                                    <img class="reaction-image reaction-item-dropdown-trigger"
                                                         src="img/reaction/like.png" alt="reaction-like">
                                                    <!-- /REACTION IMAGE -->

                                                    <!-- SIMPLE DROPDOWN -->
                                                    <div class="simple-dropdown padded reaction-item-dropdown">
                                                        <!-- SIMPLE DROPDOWN TEXT -->
                                                        <p class="simple-dropdown-text"><img class="reaction"
                                                                                             src="img/reaction/like.png"
                                                                                             alt="reaction-like"> <span
                                                                class="bold">Like</span></p>
                                                        <!-- /SIMPLE DROPDOWN TEXT -->

                                                        <!-- SIMPLE DROPDOWN TEXT -->
                                                        <p class="simple-dropdown-text">Neko Bebop</p>
                                                        <!-- /SIMPLE DROPDOWN TEXT -->

                                                        <!-- SIMPLE DROPDOWN TEXT -->
                                                        <p class="simple-dropdown-text">Nick Grissom</p>
                                                        <!-- /SIMPLE DROPDOWN TEXT -->

                                                        <!-- SIMPLE DROPDOWN TEXT -->
                                                        <p class="simple-dropdown-text">Sarah Diamond</p>
                                                        <!-- /SIMPLE DROPDOWN TEXT -->
                                                    </div>
                                                    <!-- /SIMPLE DROPDOWN -->
                                                </div>
                                                <!-- /REACTION ITEM -->
                                            </div>
                                            <!-- /META LINE LIST -->

                                            <!-- META LINE TEXT -->
                                            <p class="meta-line-text">4</p>
                                            <!-- /META LINE TEXT -->
                                        </div>
                                        <!-- /META LINE -->

                                        <!-- META LINE -->
                                        <div class="meta-line">
                                            <!-- META LINE LINK -->
                                            <p class="meta-line-link light reaction-options-small-dropdown-trigger">
                                                React!</p>
                                            <!-- /META LINE LINK -->

                                            <!-- REACTION OPTIONS -->
                                            <div class="reaction-options small reaction-options-small-dropdown">
                                                <!-- REACTION OPTION -->
                                                <div class="reaction-option text-tooltip-tft" data-title="Like">
                                                    <!-- REACTION OPTION IMAGE -->
                                                    <img class="reaction-option-image" src="img/reaction/like.png"
                                                         alt="reaction-like">
                                                    <!-- /REACTION OPTION IMAGE -->
                                                </div>
                                                <!-- /REACTION OPTION -->

                                                <!-- REACTION OPTION -->
                                                <div class="reaction-option text-tooltip-tft" data-title="Love">
                                                    <!-- REACTION OPTION IMAGE -->
                                                    <img class="reaction-option-image" src="img/reaction/love.png"
                                                         alt="reaction-love">
                                                    <!-- /REACTION OPTION IMAGE -->
                                                </div>
                                                <!-- /REACTION OPTION -->

                                                <!-- REACTION OPTION -->
                                                <div class="reaction-option text-tooltip-tft" data-title="Dislike">
                                                    <!-- REACTION OPTION IMAGE -->
                                                    <img class="reaction-option-image" src="img/reaction/dislike.png"
                                                         alt="reaction-dislike">
                                                    <!-- /REACTION OPTION IMAGE -->
                                                </div>
                                                <!-- /REACTION OPTION -->

                                                <!-- REACTION OPTION -->
                                                <div class="reaction-option text-tooltip-tft" data-title="Happy">
                                                    <!-- REACTION OPTION IMAGE -->
                                                    <img class="reaction-option-image" src="img/reaction/happy.png"
                                                         alt="reaction-happy">
                                                    <!-- /REACTION OPTION IMAGE -->
                                                </div>
                                                <!-- /REACTION OPTION -->

                                                <!-- REACTION OPTION -->
                                                <div class="reaction-option text-tooltip-tft" data-title="Funny">
                                                    <!-- REACTION OPTION IMAGE -->
                                                    <img class="reaction-option-image" src="img/reaction/funny.png"
                                                         alt="reaction-funny">
                                                    <!-- /REACTION OPTION IMAGE -->
                                                </div>
                                                <!-- /REACTION OPTION -->

                                                <!-- REACTION OPTION -->
                                                <div class="reaction-option text-tooltip-tft" data-title="Wow">
                                                    <!-- REACTION OPTION IMAGE -->
                                                    <img class="reaction-option-image" src="img/reaction/wow.png"
                                                         alt="reaction-wow">
                                                    <!-- /REACTION OPTION IMAGE -->
                                                </div>
                                                <!-- /REACTION OPTION -->

                                                <!-- REACTION OPTION -->
                                                <div class="reaction-option text-tooltip-tft" data-title="Angry">
                                                    <!-- REACTION OPTION IMAGE -->
                                                    <img class="reaction-option-image" src="img/reaction/angry.png"
                                                         alt="reaction-angry">
                                                    <!-- /REACTION OPTION IMAGE -->
                                                </div>
                                                <!-- /REACTION OPTION -->

                                                <!-- REACTION OPTION -->
                                                <div class="reaction-option text-tooltip-tft" data-title="Sad">
                                                    <!-- REACTION OPTION IMAGE -->
                                                    <img class="reaction-option-image" src="img/reaction/sad.png"
                                                         alt="reaction-sad">
                                                    <!-- /REACTION OPTION IMAGE -->
                                                </div>
                                                <!-- /REACTION OPTION -->
                                            </div>
                                            <!-- /REACTION OPTIONS -->
                                        </div>
                                        <!-- /META LINE -->

                                        <!-- META LINE -->
                                        <div class="meta-line">
                                            <!-- META LINE LINK -->
                                            <p class="meta-line-link light">Reply</p>
                                            <!-- /META LINE LINK -->
                                        </div>
                                        <!-- /META LINE -->

                                        <!-- META LINE -->
                                        <div class="meta-line">
                                            <!-- META LINE TIMESTAMP -->
                                            <p class="meta-line-timestamp">15 minutes ago</p>
                                            <!-- /META LINE TIMESTAMP -->
                                        </div>
                                        <!-- /META LINE -->

                                        <!-- META LINE -->
                                        <div class="meta-line settings">
                                            <!-- POST SETTINGS WRAP -->
                                            <div class="post-settings-wrap">
                                                <!-- POST SETTINGS -->
                                                <div class="post-settings post-settings-dropdown-trigger">
                                                    <!-- POST SETTINGS ICON -->
                                                    <svg class="post-settings-icon icon-more-dots">
                                                        <use xlink:href="#svg-more-dots"></use>
                                                    </svg>
                                                    <!-- /POST SETTINGS ICON -->
                                                </div>
                                                <!-- /POST SETTINGS -->

                                                <!-- SIMPLE DROPDOWN -->
                                                <div class="simple-dropdown post-settings-dropdown">
                                                    <!-- SIMPLE DROPDOWN LINK -->
                                                    <p class="simple-dropdown-link">Report Post</p>
                                                    <!-- /SIMPLE DROPDOWN LINK -->
                                                </div>
                                                <!-- /SIMPLE DROPDOWN -->
                                            </div>
                                            <!-- /POST SETTINGS WRAP -->
                                        </div>
                                        <!-- /META LINE -->
                                    </div>
                                    <!-- /CONTENT ACTION -->
                                </div>
                                <!-- /CONTENT ACTIONS -->
                            </div>
                            <!-- /POST COMMENT -->

                            <!-- POST COMMENT -->
                            <div class="post-comment unread reply-2">
                                <!-- USER AVATAR -->
                                <a class="user-avatar small no-outline" href="profile-timeline.html">
                                    <!-- USER AVATAR CONTENT -->
                                    <div class="user-avatar-content">
                                        <!-- HEXAGON -->
                                        <div class="hexagon-image-30-32" data-src="img/avatar/01.jpg"></div>
                                        <!-- /HEXAGON -->
                                    </div>
                                    <!-- /USER AVATAR CONTENT -->

                                    <!-- USER AVATAR PROGRESS -->
                                    <div class="user-avatar-progress">
                                        <!-- HEXAGON -->
                                        <div class="hexagon-progress-40-44"></div>
                                        <!-- /HEXAGON -->
                                    </div>
                                    <!-- /USER AVATAR PROGRESS -->

                                    <!-- USER AVATAR PROGRESS BORDER -->
                                    <div class="user-avatar-progress-border">
                                        <!-- HEXAGON -->
                                        <div class="hexagon-border-40-44"></div>
                                        <!-- /HEXAGON -->
                                    </div>
                                    <!-- /USER AVATAR PROGRESS BORDER -->

                                    <!-- USER AVATAR BADGE -->
                                    <div class="user-avatar-badge">
                                        <!-- USER AVATAR BADGE BORDER -->
                                        <div class="user-avatar-badge-border">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-22-24"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR BADGE BORDER -->

                                        <!-- USER AVATAR BADGE CONTENT -->
                                        <div class="user-avatar-badge-content">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-dark-16-18"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR BADGE CONTENT -->

                                        <!-- USER AVATAR BADGE TEXT -->
                                        <p class="user-avatar-badge-text">24</p>
                                        <!-- /USER AVATAR BADGE TEXT -->
                                    </div>
                                    <!-- /USER AVATAR BADGE -->
                                </a>
                                <!-- /USER AVATAR -->

                                <!-- POST COMMENT TEXT -->
                                <p class="post-comment-text user-tag author"><a class="post-comment-text-author"
                                                                                href="profile-timeline.html">Marina
                                        Valentine</a></p>
                                <!-- /POST COMMENT TEXT -->

                                <!-- POST COMMENT TEXT -->
                                <p class="post-comment-text">Yes! They are all included in the pack!</p>
                                <!-- /POST COMMENT TEXT -->

                                <!-- CONTENT ACTIONS -->
                                <div class="content-actions">
                                    <!-- CONTENT ACTION -->
                                    <div class="content-action">
                                        <!-- META LINE -->
                                        <div class="meta-line">
                                            <!-- META LINE LIST -->
                                            <div class="meta-line-list reaction-item-list small">
                                                <!-- REACTION ITEM -->
                                                <div class="reaction-item">
                                                    <!-- REACTION IMAGE -->
                                                    <img class="reaction-image reaction-item-dropdown-trigger"
                                                         src="img/reaction/like.png" alt="reaction-like">
                                                    <!-- /REACTION IMAGE -->

                                                    <!-- SIMPLE DROPDOWN -->
                                                    <div class="simple-dropdown padded reaction-item-dropdown">
                                                        <!-- SIMPLE DROPDOWN TEXT -->
                                                        <p class="simple-dropdown-text"><img class="reaction"
                                                                                             src="img/reaction/like.png"
                                                                                             alt="reaction-like"> <span
                                                                class="bold">Like</span></p>
                                                        <!-- /SIMPLE DROPDOWN TEXT -->

                                                        <!-- SIMPLE DROPDOWN TEXT -->
                                                        <p class="simple-dropdown-text">Bearded Wonder</p>
                                                        <!-- /SIMPLE DROPDOWN TEXT -->
                                                    </div>
                                                    <!-- /SIMPLE DROPDOWN -->
                                                </div>
                                                <!-- /REACTION ITEM -->
                                            </div>
                                            <!-- /META LINE LIST -->

                                            <!-- META LINE TEXT -->
                                            <p class="meta-line-text">1</p>
                                            <!-- /META LINE TEXT -->
                                        </div>
                                        <!-- /META LINE -->

                                        <!-- META LINE -->
                                        <div class="meta-line">
                                            <!-- META LINE LINK -->
                                            <p class="meta-line-link light reaction-options-small-dropdown-trigger">
                                                React!</p>
                                            <!-- /META LINE LINK -->

                                            <!-- REACTION OPTIONS -->
                                            <div class="reaction-options small reaction-options-small-dropdown">
                                                <!-- REACTION OPTION -->
                                                <div class="reaction-option text-tooltip-tft" data-title="Like">
                                                    <!-- REACTION OPTION IMAGE -->
                                                    <img class="reaction-option-image" src="img/reaction/like.png"
                                                         alt="reaction-like">
                                                    <!-- /REACTION OPTION IMAGE -->
                                                </div>
                                                <!-- /REACTION OPTION -->

                                                <!-- REACTION OPTION -->
                                                <div class="reaction-option text-tooltip-tft" data-title="Love">
                                                    <!-- REACTION OPTION IMAGE -->
                                                    <img class="reaction-option-image" src="img/reaction/love.png"
                                                         alt="reaction-love">
                                                    <!-- /REACTION OPTION IMAGE -->
                                                </div>
                                                <!-- /REACTION OPTION -->

                                                <!-- REACTION OPTION -->
                                                <div class="reaction-option text-tooltip-tft" data-title="Dislike">
                                                    <!-- REACTION OPTION IMAGE -->
                                                    <img class="reaction-option-image" src="img/reaction/dislike.png"
                                                         alt="reaction-dislike">
                                                    <!-- /REACTION OPTION IMAGE -->
                                                </div>
                                                <!-- /REACTION OPTION -->

                                                <!-- REACTION OPTION -->
                                                <div class="reaction-option text-tooltip-tft" data-title="Happy">
                                                    <!-- REACTION OPTION IMAGE -->
                                                    <img class="reaction-option-image" src="img/reaction/happy.png"
                                                         alt="reaction-happy">
                                                    <!-- /REACTION OPTION IMAGE -->
                                                </div>
                                                <!-- /REACTION OPTION -->

                                                <!-- REACTION OPTION -->
                                                <div class="reaction-option text-tooltip-tft" data-title="Funny">
                                                    <!-- REACTION OPTION IMAGE -->
                                                    <img class="reaction-option-image" src="img/reaction/funny.png"
                                                         alt="reaction-funny">
                                                    <!-- /REACTION OPTION IMAGE -->
                                                </div>
                                                <!-- /REACTION OPTION -->

                                                <!-- REACTION OPTION -->
                                                <div class="reaction-option text-tooltip-tft" data-title="Wow">
                                                    <!-- REACTION OPTION IMAGE -->
                                                    <img class="reaction-option-image" src="img/reaction/wow.png"
                                                         alt="reaction-wow">
                                                    <!-- /REACTION OPTION IMAGE -->
                                                </div>
                                                <!-- /REACTION OPTION -->

                                                <!-- REACTION OPTION -->
                                                <div class="reaction-option text-tooltip-tft" data-title="Angry">
                                                    <!-- REACTION OPTION IMAGE -->
                                                    <img class="reaction-option-image" src="img/reaction/angry.png"
                                                         alt="reaction-angry">
                                                    <!-- /REACTION OPTION IMAGE -->
                                                </div>
                                                <!-- /REACTION OPTION -->

                                                <!-- REACTION OPTION -->
                                                <div class="reaction-option text-tooltip-tft" data-title="Sad">
                                                    <!-- REACTION OPTION IMAGE -->
                                                    <img class="reaction-option-image" src="img/reaction/sad.png"
                                                         alt="reaction-sad">
                                                    <!-- /REACTION OPTION IMAGE -->
                                                </div>
                                                <!-- /REACTION OPTION -->
                                            </div>
                                            <!-- /REACTION OPTIONS -->
                                        </div>
                                        <!-- /META LINE -->

                                        <!-- META LINE -->
                                        <div class="meta-line">
                                            <!-- META LINE LINK -->
                                            <p class="meta-line-link light">Reply</p>
                                            <!-- /META LINE LINK -->
                                        </div>
                                        <!-- /META LINE -->

                                        <!-- META LINE -->
                                        <div class="meta-line">
                                            <!-- META LINE TIMESTAMP -->
                                            <p class="meta-line-timestamp">15 minutes ago</p>
                                            <!-- /META LINE TIMESTAMP -->
                                        </div>
                                        <!-- /META LINE -->

                                        <!-- META LINE -->
                                        <div class="meta-line settings">
                                            <!-- POST SETTINGS WRAP -->
                                            <div class="post-settings-wrap">
                                                <!-- POST SETTINGS -->
                                                <div class="post-settings post-settings-dropdown-trigger">
                                                    <!-- POST SETTINGS ICON -->
                                                    <svg class="post-settings-icon icon-more-dots">
                                                        <use xlink:href="#svg-more-dots"></use>
                                                    </svg>
                                                    <!-- /POST SETTINGS ICON -->
                                                </div>
                                                <!-- /POST SETTINGS -->

                                                <!-- SIMPLE DROPDOWN -->
                                                <div class="simple-dropdown post-settings-dropdown">
                                                    <!-- SIMPLE DROPDOWN LINK -->
                                                    <p class="simple-dropdown-link">Report Post</p>
                                                    <!-- /SIMPLE DROPDOWN LINK -->
                                                </div>
                                                <!-- /SIMPLE DROPDOWN -->
                                            </div>
                                            <!-- /POST SETTINGS WRAP -->
                                        </div>
                                        <!-- /META LINE -->
                                    </div>
                                    <!-- /CONTENT ACTION -->
                                </div>
                                <!-- /CONTENT ACTIONS -->
                            </div>
                            <!-- /POST COMMENT -->

                            <!-- POST COMMENT -->
                            <div class="post-comment">
                                <!-- USER AVATAR -->
                                <a class="user-avatar small no-outline" href="profile-timeline.html">
                                    <!-- USER AVATAR CONTENT -->
                                    <div class="user-avatar-content">
                                        <!-- HEXAGON -->
                                        <div class="hexagon-image-30-32" data-src="img/avatar/03.jpg"></div>
                                        <!-- /HEXAGON -->
                                    </div>
                                    <!-- /USER AVATAR CONTENT -->

                                    <!-- USER AVATAR PROGRESS -->
                                    <div class="user-avatar-progress">
                                        <!-- HEXAGON -->
                                        <div class="hexagon-progress-40-44"></div>
                                        <!-- /HEXAGON -->
                                    </div>
                                    <!-- /USER AVATAR PROGRESS -->

                                    <!-- USER AVATAR PROGRESS BORDER -->
                                    <div class="user-avatar-progress-border">
                                        <!-- HEXAGON -->
                                        <div class="hexagon-border-40-44"></div>
                                        <!-- /HEXAGON -->
                                    </div>
                                    <!-- /USER AVATAR PROGRESS BORDER -->

                                    <!-- USER AVATAR BADGE -->
                                    <div class="user-avatar-badge">
                                        <!-- USER AVATAR BADGE BORDER -->
                                        <div class="user-avatar-badge-border">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-22-24"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR BADGE BORDER -->

                                        <!-- USER AVATAR BADGE CONTENT -->
                                        <div class="user-avatar-badge-content">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-dark-16-18"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR BADGE CONTENT -->

                                        <!-- USER AVATAR BADGE TEXT -->
                                        <p class="user-avatar-badge-text">16</p>
                                        <!-- /USER AVATAR BADGE TEXT -->
                                    </div>
                                    <!-- /USER AVATAR BADGE -->
                                </a>
                                <!-- /USER AVATAR -->

                                <!-- POST COMMENT TEXT -->
                                <p class="post-comment-text"><a class="post-comment-text-author"
                                                                href="profile-timeline.html">Nick Grissom</a></p>
                                <!-- /POST COMMENT TEXT -->

                                <!-- POST COMMENT TEXT -->
                                <p class="post-comment-text">Hi Marina! I'm really liking what you did here! I've
                                    checked all the pages and the design is really great, plus it has lots of statistics
                                    and match overview options! Amazing job, and good luck!</p>
                                <!-- /POST COMMENT TEXT -->

                                <!-- CONTENT ACTIONS -->
                                <div class="content-actions">
                                    <!-- CONTENT ACTION -->
                                    <div class="content-action">
                                        <!-- META LINE -->
                                        <div class="meta-line">
                                            <!-- META LINE LINK -->
                                            <p class="meta-line-link light reaction-options-small-dropdown-trigger">
                                                React!</p>
                                            <!-- /META LINE LINK -->

                                            <!-- REACTION OPTIONS -->
                                            <div class="reaction-options small reaction-options-small-dropdown">
                                                <!-- REACTION OPTION -->
                                                <div class="reaction-option text-tooltip-tft" data-title="Like">
                                                    <!-- REACTION OPTION IMAGE -->
                                                    <img class="reaction-option-image" src="img/reaction/like.png"
                                                         alt="reaction-like">
                                                    <!-- /REACTION OPTION IMAGE -->
                                                </div>
                                                <!-- /REACTION OPTION -->

                                                <!-- REACTION OPTION -->
                                                <div class="reaction-option text-tooltip-tft" data-title="Love">
                                                    <!-- REACTION OPTION IMAGE -->
                                                    <img class="reaction-option-image" src="img/reaction/love.png"
                                                         alt="reaction-love">
                                                    <!-- /REACTION OPTION IMAGE -->
                                                </div>
                                                <!-- /REACTION OPTION -->

                                                <!-- REACTION OPTION -->
                                                <div class="reaction-option text-tooltip-tft" data-title="Dislike">
                                                    <!-- REACTION OPTION IMAGE -->
                                                    <img class="reaction-option-image" src="img/reaction/dislike.png"
                                                         alt="reaction-dislike">
                                                    <!-- /REACTION OPTION IMAGE -->
                                                </div>
                                                <!-- /REACTION OPTION -->

                                                <!-- REACTION OPTION -->
                                                <div class="reaction-option text-tooltip-tft" data-title="Happy">
                                                    <!-- REACTION OPTION IMAGE -->
                                                    <img class="reaction-option-image" src="img/reaction/happy.png"
                                                         alt="reaction-happy">
                                                    <!-- /REACTION OPTION IMAGE -->
                                                </div>
                                                <!-- /REACTION OPTION -->

                                                <!-- REACTION OPTION -->
                                                <div class="reaction-option text-tooltip-tft" data-title="Funny">
                                                    <!-- REACTION OPTION IMAGE -->
                                                    <img class="reaction-option-image" src="img/reaction/funny.png"
                                                         alt="reaction-funny">
                                                    <!-- /REACTION OPTION IMAGE -->
                                                </div>
                                                <!-- /REACTION OPTION -->

                                                <!-- REACTION OPTION -->
                                                <div class="reaction-option text-tooltip-tft" data-title="Wow">
                                                    <!-- REACTION OPTION IMAGE -->
                                                    <img class="reaction-option-image" src="img/reaction/wow.png"
                                                         alt="reaction-wow">
                                                    <!-- /REACTION OPTION IMAGE -->
                                                </div>
                                                <!-- /REACTION OPTION -->

                                                <!-- REACTION OPTION -->
                                                <div class="reaction-option text-tooltip-tft" data-title="Angry">
                                                    <!-- REACTION OPTION IMAGE -->
                                                    <img class="reaction-option-image" src="img/reaction/angry.png"
                                                         alt="reaction-angry">
                                                    <!-- /REACTION OPTION IMAGE -->
                                                </div>
                                                <!-- /REACTION OPTION -->

                                                <!-- REACTION OPTION -->
                                                <div class="reaction-option text-tooltip-tft" data-title="Sad">
                                                    <!-- REACTION OPTION IMAGE -->
                                                    <img class="reaction-option-image" src="img/reaction/sad.png"
                                                         alt="reaction-sad">
                                                    <!-- /REACTION OPTION IMAGE -->
                                                </div>
                                                <!-- /REACTION OPTION -->
                                            </div>
                                            <!-- /REACTION OPTIONS -->
                                        </div>
                                        <!-- /META LINE -->

                                        <!-- META LINE -->
                                        <div class="meta-line">
                                            <!-- META LINE LINK -->
                                            <p class="meta-line-link light">Reply</p>
                                            <!-- /META LINE LINK -->
                                        </div>
                                        <!-- /META LINE -->

                                        <!-- META LINE -->
                                        <div class="meta-line">
                                            <!-- META LINE TIMESTAMP -->
                                            <p class="meta-line-timestamp">15 minutes ago</p>
                                            <!-- /META LINE TIMESTAMP -->
                                        </div>
                                        <!-- /META LINE -->

                                        <!-- META LINE -->
                                        <div class="meta-line settings">
                                            <!-- POST SETTINGS WRAP -->
                                            <div class="post-settings-wrap">
                                                <!-- POST SETTINGS -->
                                                <div class="post-settings post-settings-dropdown-trigger">
                                                    <!-- POST SETTINGS ICON -->
                                                    <svg class="post-settings-icon icon-more-dots">
                                                        <use xlink:href="#svg-more-dots"></use>
                                                    </svg>
                                                    <!-- /POST SETTINGS ICON -->
                                                </div>
                                                <!-- /POST SETTINGS -->

                                                <!-- SIMPLE DROPDOWN -->
                                                <div class="simple-dropdown post-settings-dropdown">
                                                    <!-- SIMPLE DROPDOWN LINK -->
                                                    <p class="simple-dropdown-link">Report Post</p>
                                                    <!-- /SIMPLE DROPDOWN LINK -->
                                                </div>
                                                <!-- /SIMPLE DROPDOWN -->
                                            </div>
                                            <!-- /POST SETTINGS WRAP -->
                                        </div>
                                        <!-- /META LINE -->
                                    </div>
                                    <!-- /CONTENT ACTION -->
                                </div>
                                <!-- /CONTENT ACTIONS -->

                                <!-- POST COMMENT FORM -->
                                <div class="post-comment-form">
                                    <!-- USER AVATAR -->
                                    <div class="user-avatar small no-outline">
                                        <!-- USER AVATAR CONTENT -->
                                        <div class="user-avatar-content">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-image-30-32" data-src="img/avatar/01.jpg"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR CONTENT -->

                                        <!-- USER AVATAR PROGRESS -->
                                        <div class="user-avatar-progress">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-progress-40-44"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR PROGRESS -->

                                        <!-- USER AVATAR PROGRESS BORDER -->
                                        <div class="user-avatar-progress-border">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-border-40-44"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR PROGRESS BORDER -->

                                        <!-- USER AVATAR BADGE -->
                                        <div class="user-avatar-badge">
                                            <!-- USER AVATAR BADGE BORDER -->
                                            <div class="user-avatar-badge-border">
                                                <!-- HEXAGON -->
                                                <div class="hexagon-22-24"></div>
                                                <!-- /HEXAGON -->
                                            </div>
                                            <!-- /USER AVATAR BADGE BORDER -->

                                            <!-- USER AVATAR BADGE CONTENT -->
                                            <div class="user-avatar-badge-content">
                                                <!-- HEXAGON -->
                                                <div class="hexagon-dark-16-18"></div>
                                                <!-- /HEXAGON -->
                                            </div>
                                            <!-- /USER AVATAR BADGE CONTENT -->

                                            <!-- USER AVATAR BADGE TEXT -->
                                            <p class="user-avatar-badge-text">24</p>
                                            <!-- /USER AVATAR BADGE TEXT -->
                                        </div>
                                        <!-- /USER AVATAR BADGE -->
                                    </div>
                                    <!-- /USER AVATAR -->

                                    <!-- FORM -->
                                    <form class="form">
                                        <!-- FORM ROW -->
                                        <div class="form-row">
                                            <!-- FORM ITEM -->
                                            <div class="form-item">
                                                <!-- FORM INPUT -->
                                                <div class="form-input small">
                                                    <label for="post-reply-1">Your Reply</label>
                                                    <input type="text" id="post-reply-1" name="post_reply_1">
                                                </div>
                                                <!-- /FORM INPUT -->
                                            </div>
                                            <!-- /FORM ITEM -->
                                        </div>
                                        <!-- /FORM ROW -->
                                    </form>
                                    <!-- /FORM -->
                                </div>
                                <!-- /POST COMMENT FORM -->
                            </div>
                            <!-- /POST COMMENT -->

                            <!-- POST COMMENT -->
                            <div class="post-comment">
                                <!-- USER AVATAR -->
                                <a class="user-avatar small no-outline" href="profile-timeline.html">
                                    <!-- USER AVATAR CONTENT -->
                                    <div class="user-avatar-content">
                                        <!-- HEXAGON -->
                                        <div class="hexagon-image-30-32" data-src="img/avatar/05.jpg"></div>
                                        <!-- /HEXAGON -->
                                    </div>
                                    <!-- /USER AVATAR CONTENT -->

                                    <!-- USER AVATAR PROGRESS -->
                                    <div class="user-avatar-progress">
                                        <!-- HEXAGON -->
                                        <div class="hexagon-progress-40-44"></div>
                                        <!-- /HEXAGON -->
                                    </div>
                                    <!-- /USER AVATAR PROGRESS -->

                                    <!-- USER AVATAR PROGRESS BORDER -->
                                    <div class="user-avatar-progress-border">
                                        <!-- HEXAGON -->
                                        <div class="hexagon-border-40-44"></div>
                                        <!-- /HEXAGON -->
                                    </div>
                                    <!-- /USER AVATAR PROGRESS BORDER -->

                                    <!-- USER AVATAR BADGE -->
                                    <div class="user-avatar-badge">
                                        <!-- USER AVATAR BADGE BORDER -->
                                        <div class="user-avatar-badge-border">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-22-24"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR BADGE BORDER -->

                                        <!-- USER AVATAR BADGE CONTENT -->
                                        <div class="user-avatar-badge-content">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-dark-16-18"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR BADGE CONTENT -->

                                        <!-- USER AVATAR BADGE TEXT -->
                                        <p class="user-avatar-badge-text">12</p>
                                        <!-- /USER AVATAR BADGE TEXT -->
                                    </div>
                                    <!-- /USER AVATAR BADGE -->
                                </a>
                                <!-- /USER AVATAR -->

                                <!-- POST COMMENT TEXT -->
                                <p class="post-comment-text user-tag purchased"><a class="post-comment-text-author"
                                                                                   href="profile-timeline.html">Neko
                                        Bebop</a></p>
                                <!-- /POST COMMENT TEXT -->

                                <!-- POST COMMENT TEXT -->
                                <p class="post-comment-text">Thanks for this great template! I’m already using it and
                                    it’s really awesome ;)</p>
                                <!-- /POST COMMENT TEXT -->

                                <!-- CONTENT ACTIONS -->
                                <div class="content-actions">
                                    <!-- CONTENT ACTION -->
                                    <div class="content-action">
                                        <!-- META LINE -->
                                        <div class="meta-line">
                                            <!-- META LINE LINK -->
                                            <p class="meta-line-link light reaction-options-small-dropdown-trigger">
                                                React!</p>
                                            <!-- /META LINE LINK -->

                                            <!-- REACTION OPTIONS -->
                                            <div class="reaction-options small reaction-options-small-dropdown">
                                                <!-- REACTION OPTION -->
                                                <div class="reaction-option text-tooltip-tft" data-title="Like">
                                                    <!-- REACTION OPTION IMAGE -->
                                                    <img class="reaction-option-image" src="img/reaction/like.png"
                                                         alt="reaction-like">
                                                    <!-- /REACTION OPTION IMAGE -->
                                                </div>
                                                <!-- /REACTION OPTION -->

                                                <!-- REACTION OPTION -->
                                                <div class="reaction-option text-tooltip-tft" data-title="Love">
                                                    <!-- REACTION OPTION IMAGE -->
                                                    <img class="reaction-option-image" src="img/reaction/love.png"
                                                         alt="reaction-love">
                                                    <!-- /REACTION OPTION IMAGE -->
                                                </div>
                                                <!-- /REACTION OPTION -->

                                                <!-- REACTION OPTION -->
                                                <div class="reaction-option text-tooltip-tft" data-title="Dislike">
                                                    <!-- REACTION OPTION IMAGE -->
                                                    <img class="reaction-option-image" src="img/reaction/dislike.png"
                                                         alt="reaction-dislike">
                                                    <!-- /REACTION OPTION IMAGE -->
                                                </div>
                                                <!-- /REACTION OPTION -->

                                                <!-- REACTION OPTION -->
                                                <div class="reaction-option text-tooltip-tft" data-title="Happy">
                                                    <!-- REACTION OPTION IMAGE -->
                                                    <img class="reaction-option-image" src="img/reaction/happy.png"
                                                         alt="reaction-happy">
                                                    <!-- /REACTION OPTION IMAGE -->
                                                </div>
                                                <!-- /REACTION OPTION -->

                                                <!-- REACTION OPTION -->
                                                <div class="reaction-option text-tooltip-tft" data-title="Funny">
                                                    <!-- REACTION OPTION IMAGE -->
                                                    <img class="reaction-option-image" src="img/reaction/funny.png"
                                                         alt="reaction-funny">
                                                    <!-- /REACTION OPTION IMAGE -->
                                                </div>
                                                <!-- /REACTION OPTION -->

                                                <!-- REACTION OPTION -->
                                                <div class="reaction-option text-tooltip-tft" data-title="Wow">
                                                    <!-- REACTION OPTION IMAGE -->
                                                    <img class="reaction-option-image" src="img/reaction/wow.png"
                                                         alt="reaction-wow">
                                                    <!-- /REACTION OPTION IMAGE -->
                                                </div>
                                                <!-- /REACTION OPTION -->

                                                <!-- REACTION OPTION -->
                                                <div class="reaction-option text-tooltip-tft" data-title="Angry">
                                                    <!-- REACTION OPTION IMAGE -->
                                                    <img class="reaction-option-image" src="img/reaction/angry.png"
                                                         alt="reaction-angry">
                                                    <!-- /REACTION OPTION IMAGE -->
                                                </div>
                                                <!-- /REACTION OPTION -->

                                                <!-- REACTION OPTION -->
                                                <div class="reaction-option text-tooltip-tft" data-title="Sad">
                                                    <!-- REACTION OPTION IMAGE -->
                                                    <img class="reaction-option-image" src="img/reaction/sad.png"
                                                         alt="reaction-sad">
                                                    <!-- /REACTION OPTION IMAGE -->
                                                </div>
                                                <!-- /REACTION OPTION -->
                                            </div>
                                            <!-- /REACTION OPTIONS -->
                                        </div>
                                        <!-- /META LINE -->

                                        <!-- META LINE -->
                                        <div class="meta-line">
                                            <!-- META LINE LINK -->
                                            <p class="meta-line-link light">Reply</p>
                                            <!-- /META LINE LINK -->
                                        </div>
                                        <!-- /META LINE -->

                                        <!-- META LINE -->
                                        <div class="meta-line">
                                            <!-- META LINE TIMESTAMP -->
                                            <p class="meta-line-timestamp">15 minutes ago</p>
                                            <!-- /META LINE TIMESTAMP -->
                                        </div>
                                        <!-- /META LINE -->

                                        <!-- META LINE -->
                                        <div class="meta-line settings">
                                            <!-- POST SETTINGS WRAP -->
                                            <div class="post-settings-wrap">
                                                <!-- POST SETTINGS -->
                                                <div class="post-settings post-settings-dropdown-trigger">
                                                    <!-- POST SETTINGS ICON -->
                                                    <svg class="post-settings-icon icon-more-dots">
                                                        <use xlink:href="#svg-more-dots"></use>
                                                    </svg>
                                                    <!-- /POST SETTINGS ICON -->
                                                </div>
                                                <!-- /POST SETTINGS -->

                                                <!-- SIMPLE DROPDOWN -->
                                                <div class="simple-dropdown post-settings-dropdown">
                                                    <!-- SIMPLE DROPDOWN LINK -->
                                                    <p class="simple-dropdown-link">Report Post</p>
                                                    <!-- /SIMPLE DROPDOWN LINK -->
                                                </div>
                                                <!-- /SIMPLE DROPDOWN -->
                                            </div>
                                            <!-- /POST SETTINGS WRAP -->
                                        </div>
                                        <!-- /META LINE -->
                                    </div>
                                    <!-- /CONTENT ACTION -->
                                </div>
                                <!-- /CONTENT ACTIONS -->
                            </div>
                            <!-- /POST COMMENT -->

                            <!-- POST COMMENT FORM -->
                            <div class="post-comment-form with-title">
                                <!-- POST COMMENT FORM TITLE -->
                                <p class="post-comment-form-title">Leave a Comment</p>
                                <!-- /POST COMMENT FORM TITLE -->

                                <!-- USER AVATAR -->
                                <div class="user-avatar small no-outline">
                                    <!-- USER AVATAR CONTENT -->
                                    <div class="user-avatar-content">
                                        <!-- HEXAGON -->
                                        <div class="hexagon-image-30-32" data-src="img/avatar/01.jpg"></div>
                                        <!-- /HEXAGON -->
                                    </div>
                                    <!-- /USER AVATAR CONTENT -->

                                    <!-- USER AVATAR PROGRESS -->
                                    <div class="user-avatar-progress">
                                        <!-- HEXAGON -->
                                        <div class="hexagon-progress-40-44"></div>
                                        <!-- /HEXAGON -->
                                    </div>
                                    <!-- /USER AVATAR PROGRESS -->

                                    <!-- USER AVATAR PROGRESS BORDER -->
                                    <div class="user-avatar-progress-border">
                                        <!-- HEXAGON -->
                                        <div class="hexagon-border-40-44"></div>
                                        <!-- /HEXAGON -->
                                    </div>
                                    <!-- /USER AVATAR PROGRESS BORDER -->

                                    <!-- USER AVATAR BADGE -->
                                    <div class="user-avatar-badge">
                                        <!-- USER AVATAR BADGE BORDER -->
                                        <div class="user-avatar-badge-border">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-22-24"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR BADGE BORDER -->

                                        <!-- USER AVATAR BADGE CONTENT -->
                                        <div class="user-avatar-badge-content">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-dark-16-18"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR BADGE CONTENT -->

                                        <!-- USER AVATAR BADGE TEXT -->
                                        <p class="user-avatar-badge-text">24</p>
                                        <!-- /USER AVATAR BADGE TEXT -->
                                    </div>
                                    <!-- /USER AVATAR BADGE -->
                                </div>
                                <!-- /USER AVATAR -->

                                <!-- FORM -->
                                <form class="form">
                                    <!-- FORM ROW -->
                                    <div class="form-row">
                                        <!-- FORM ITEM -->
                                        <div class="form-item">
                                            <!-- FORM INPUT -->
                                            <div class="form-input small">
                                                <label for="post-reply-2">Your Reply</label>
                                                <textarea id="post-reply-2" name="post_reply_2"></textarea>
                                            </div>
                                            <!-- /FORM INPUT -->
                                        </div>
                                        <!-- /FORM ITEM -->
                                    </div>
                                    <!-- /FORM ROW -->
                                </form>
                                <!-- /FORM -->
                            </div>
                            <!-- /POST COMMENT FORM -->
                        </div>
                        <!-- /POST COMMENT LIST -->
                    </div>
                    <!-- /TAB BOX ITEM -->

                    <!-- TAB BOX ITEM -->
                    <div class="tab-box-item">
                        <!-- POST COMMENT LIST -->
                        <div class="post-comment-list no-border-top">
                            <!-- POST COMMENT -->
                            <div class="post-comment">
                                <!-- USER AVATAR -->
                                <a class="user-avatar small no-outline" href="profile-timeline.html">
                                    <!-- USER AVATAR CONTENT -->
                                    <div class="user-avatar-content">
                                        <!-- HEXAGON -->
                                        <div class="hexagon-image-30-32" data-src="img/avatar/03.jpg"></div>
                                        <!-- /HEXAGON -->
                                    </div>
                                    <!-- /USER AVATAR CONTENT -->

                                    <!-- USER AVATAR PROGRESS -->
                                    <div class="user-avatar-progress">
                                        <!-- HEXAGON -->
                                        <div class="hexagon-progress-40-44"></div>
                                        <!-- /HEXAGON -->
                                    </div>
                                    <!-- /USER AVATAR PROGRESS -->

                                    <!-- USER AVATAR PROGRESS BORDER -->
                                    <div class="user-avatar-progress-border">
                                        <!-- HEXAGON -->
                                        <div class="hexagon-border-40-44"></div>
                                        <!-- /HEXAGON -->
                                    </div>
                                    <!-- /USER AVATAR PROGRESS BORDER -->

                                    <!-- USER AVATAR BADGE -->
                                    <div class="user-avatar-badge">
                                        <!-- USER AVATAR BADGE BORDER -->
                                        <div class="user-avatar-badge-border">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-22-24"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR BADGE BORDER -->

                                        <!-- USER AVATAR BADGE CONTENT -->
                                        <div class="user-avatar-badge-content">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-dark-16-18"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR BADGE CONTENT -->

                                        <!-- USER AVATAR BADGE TEXT -->
                                        <p class="user-avatar-badge-text">16</p>
                                        <!-- /USER AVATAR BADGE TEXT -->
                                    </div>
                                    <!-- /USER AVATAR BADGE -->
                                </a>
                                <!-- /USER AVATAR -->

                                <!-- POST COMMENT TEXT WRAP -->
                                <div class="post-comment-text-wrap">
                                    <!-- RATING LIST -->
                                    <div class="rating-list medium">
                                        <!-- RATING -->
                                        <div class="rating medium filled">
                                            <!-- RATING ICON -->
                                            <svg class="rating-icon medium icon-star">
                                                <use xlink:href="#svg-star"></use>
                                            </svg>
                                            <!-- /RATING ICON -->
                                        </div>
                                        <!-- /RATING -->

                                        <!-- RATING -->
                                        <div class="rating medium filled">
                                            <!-- RATING ICON -->
                                            <svg class="rating-icon medium icon-star">
                                                <use xlink:href="#svg-star"></use>
                                            </svg>
                                            <!-- /RATING ICON -->
                                        </div>
                                        <!-- /RATING -->

                                        <!-- RATING -->
                                        <div class="rating medium filled">
                                            <!-- RATING ICON -->
                                            <svg class="rating-icon medium icon-star">
                                                <use xlink:href="#svg-star"></use>
                                            </svg>
                                            <!-- /RATING ICON -->
                                        </div>
                                        <!-- /RATING -->

                                        <!-- RATING -->
                                        <div class="rating medium filled">
                                            <!-- RATING ICON -->
                                            <svg class="rating-icon medium icon-star">
                                                <use xlink:href="#svg-star"></use>
                                            </svg>
                                            <!-- /RATING ICON -->
                                        </div>
                                        <!-- /RATING -->

                                        <!-- RATING -->
                                        <div class="rating medium">
                                            <!-- RATING ICON -->
                                            <svg class="rating-icon medium icon-star">
                                                <use xlink:href="#svg-star"></use>
                                            </svg>
                                            <!-- /RATING ICON -->
                                        </div>
                                        <!-- /RATING -->
                                    </div>
                                    <!-- /RATING LIST -->

                                    <!-- POST COMMENT TEXT -->
                                    <p class="post-comment-text"><span class="bold">Reason:</span> <span
                                            class="highlighted">Item Quality</span></p>
                                    <!-- /POST COMMENT TEXT -->
                                </div>
                                <!-- /POST COMMENT TEXT WRAP -->

                                <!-- POST COMMENT TEXT -->
                                <p class="post-comment-text">Main reason would pretty much be all, nice clean code, easy
                                    to customise and work with. Commenting on each section is great, could not really
                                    ask for anything better. Top work!</p>
                                <!-- /POST COMMENT TEXT -->

                                <!-- CONTENT ACTIONS -->
                                <div class="content-actions">
                                    <!-- CONTENT ACTION -->
                                    <div class="content-action">
                                        <!-- META LINE -->
                                        <div class="meta-line">
                                            <!-- META LINE TEXT -->
                                            <p class="meta-line-text"><a href="profile-timeline.html">Nick Grissom</a>
                                            </p>
                                            <!-- /META LINE TEXT -->
                                        </div>
                                        <!-- /META LINE -->

                                        <!-- META LINE -->
                                        <div class="meta-line">
                                            <!-- META LINE TIMESTAMP -->
                                            <p class="meta-line-timestamp">15 minutes ago</p>
                                            <!-- /META LINE TIMESTAMP -->
                                        </div>
                                        <!-- /META LINE -->

                                        <!-- META LINE -->
                                        <div class="meta-line settings">
                                            <!-- POST SETTINGS WRAP -->
                                            <div class="post-settings-wrap">
                                                <!-- POST SETTINGS -->
                                                <div class="post-settings post-settings-dropdown-trigger">
                                                    <!-- POST SETTINGS ICON -->
                                                    <svg class="post-settings-icon icon-more-dots">
                                                        <use xlink:href="#svg-more-dots"></use>
                                                    </svg>
                                                    <!-- /POST SETTINGS ICON -->
                                                </div>
                                                <!-- /POST SETTINGS -->

                                                <!-- SIMPLE DROPDOWN -->
                                                <div class="simple-dropdown post-settings-dropdown">
                                                    <!-- SIMPLE DROPDOWN LINK -->
                                                    <p class="simple-dropdown-link">Report Review</p>
                                                    <!-- /SIMPLE DROPDOWN LINK -->
                                                </div>
                                                <!-- /SIMPLE DROPDOWN -->
                                            </div>
                                            <!-- /POST SETTINGS WRAP -->
                                        </div>
                                        <!-- /META LINE -->
                                    </div>
                                    <!-- /CONTENT ACTION -->
                                </div>
                                <!-- /CONTENT ACTIONS -->
                            </div>
                            <!-- /POST COMMENT -->

                            <!-- POST COMMENT -->
                            <div class="post-comment">
                                <!-- USER AVATAR -->
                                <a class="user-avatar small no-outline" href="profile-timeline.html">
                                    <!-- USER AVATAR CONTENT -->
                                    <div class="user-avatar-content">
                                        <!-- HEXAGON -->
                                        <div class="hexagon-image-30-32" data-src="img/avatar/04.jpg"></div>
                                        <!-- /HEXAGON -->
                                    </div>
                                    <!-- /USER AVATAR CONTENT -->

                                    <!-- USER AVATAR PROGRESS -->
                                    <div class="user-avatar-progress">
                                        <!-- HEXAGON -->
                                        <div class="hexagon-progress-40-44"></div>
                                        <!-- /HEXAGON -->
                                    </div>
                                    <!-- /USER AVATAR PROGRESS -->

                                    <!-- USER AVATAR PROGRESS BORDER -->
                                    <div class="user-avatar-progress-border">
                                        <!-- HEXAGON -->
                                        <div class="hexagon-border-40-44"></div>
                                        <!-- /HEXAGON -->
                                    </div>
                                    <!-- /USER AVATAR PROGRESS BORDER -->

                                    <!-- USER AVATAR BADGE -->
                                    <div class="user-avatar-badge">
                                        <!-- USER AVATAR BADGE BORDER -->
                                        <div class="user-avatar-badge-border">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-22-24"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR BADGE BORDER -->

                                        <!-- USER AVATAR BADGE CONTENT -->
                                        <div class="user-avatar-badge-content">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-dark-16-18"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR BADGE CONTENT -->

                                        <!-- USER AVATAR BADGE TEXT -->
                                        <p class="user-avatar-badge-text">6</p>
                                        <!-- /USER AVATAR BADGE TEXT -->
                                    </div>
                                    <!-- /USER AVATAR BADGE -->
                                </a>
                                <!-- /USER AVATAR -->

                                <!-- POST COMMENT TEXT WRAP -->
                                <div class="post-comment-text-wrap">
                                    <!-- RATING LIST -->
                                    <div class="rating-list medium">
                                        <!-- RATING -->
                                        <div class="rating medium filled">
                                            <!-- RATING ICON -->
                                            <svg class="rating-icon medium icon-star">
                                                <use xlink:href="#svg-star"></use>
                                            </svg>
                                            <!-- /RATING ICON -->
                                        </div>
                                        <!-- /RATING -->

                                        <!-- RATING -->
                                        <div class="rating medium filled">
                                            <!-- RATING ICON -->
                                            <svg class="rating-icon medium icon-star">
                                                <use xlink:href="#svg-star"></use>
                                            </svg>
                                            <!-- /RATING ICON -->
                                        </div>
                                        <!-- /RATING -->

                                        <!-- RATING -->
                                        <div class="rating medium filled">
                                            <!-- RATING ICON -->
                                            <svg class="rating-icon medium icon-star">
                                                <use xlink:href="#svg-star"></use>
                                            </svg>
                                            <!-- /RATING ICON -->
                                        </div>
                                        <!-- /RATING -->

                                        <!-- RATING -->
                                        <div class="rating medium filled">
                                            <!-- RATING ICON -->
                                            <svg class="rating-icon medium icon-star">
                                                <use xlink:href="#svg-star"></use>
                                            </svg>
                                            <!-- /RATING ICON -->
                                        </div>
                                        <!-- /RATING -->

                                        <!-- RATING -->
                                        <div class="rating medium">
                                            <!-- RATING ICON -->
                                            <svg class="rating-icon medium icon-star">
                                                <use xlink:href="#svg-star"></use>
                                            </svg>
                                            <!-- /RATING ICON -->
                                        </div>
                                        <!-- /RATING -->
                                    </div>
                                    <!-- /RATING LIST -->

                                    <!-- POST COMMENT TEXT -->
                                    <p class="post-comment-text"><span class="bold">Reason:</span> <span
                                            class="highlighted">Documentation</span></p>
                                    <!-- /POST COMMENT TEXT -->
                                </div>
                                <!-- /POST COMMENT TEXT WRAP -->

                                <!-- POST COMMENT TEXT -->
                                <p class="post-comment-text">Best template I have ever had. Good documentation and code
                                    practices.</p>
                                <!-- /POST COMMENT TEXT -->

                                <!-- CONTENT ACTIONS -->
                                <div class="content-actions">
                                    <!-- CONTENT ACTION -->
                                    <div class="content-action">
                                        <!-- META LINE -->
                                        <div class="meta-line">
                                            <!-- META LINE TEXT -->
                                            <p class="meta-line-text"><a href="profile-timeline.html">Bearded Wonder</a>
                                            </p>
                                            <!-- /META LINE TEXT -->
                                        </div>
                                        <!-- /META LINE -->

                                        <!-- META LINE -->
                                        <div class="meta-line">
                                            <!-- META LINE TIMESTAMP -->
                                            <p class="meta-line-timestamp">2 days ago</p>
                                            <!-- /META LINE TIMESTAMP -->
                                        </div>
                                        <!-- /META LINE -->

                                        <!-- META LINE -->
                                        <div class="meta-line settings">
                                            <!-- POST SETTINGS WRAP -->
                                            <div class="post-settings-wrap">
                                                <!-- POST SETTINGS -->
                                                <div class="post-settings post-settings-dropdown-trigger">
                                                    <!-- POST SETTINGS ICON -->
                                                    <svg class="post-settings-icon icon-more-dots">
                                                        <use xlink:href="#svg-more-dots"></use>
                                                    </svg>
                                                    <!-- /POST SETTINGS ICON -->
                                                </div>
                                                <!-- /POST SETTINGS -->

                                                <!-- SIMPLE DROPDOWN -->
                                                <div class="simple-dropdown post-settings-dropdown">
                                                    <!-- SIMPLE DROPDOWN LINK -->
                                                    <p class="simple-dropdown-link">Report Review</p>
                                                    <!-- /SIMPLE DROPDOWN LINK -->
                                                </div>
                                                <!-- /SIMPLE DROPDOWN -->
                                            </div>
                                            <!-- /POST SETTINGS WRAP -->
                                        </div>
                                        <!-- /META LINE -->
                                    </div>
                                    <!-- /CONTENT ACTION -->
                                </div>
                                <!-- /CONTENT ACTIONS -->
                            </div>
                            <!-- /POST COMMENT -->

                            <!-- POST COMMENT -->
                            <div class="post-comment">
                                <!-- USER AVATAR -->
                                <a class="user-avatar small no-outline" href="profile-timeline.html">
                                    <!-- USER AVATAR CONTENT -->
                                    <div class="user-avatar-content">
                                        <!-- HEXAGON -->
                                        <div class="hexagon-image-30-32" data-src="img/avatar/07.jpg"></div>
                                        <!-- /HEXAGON -->
                                    </div>
                                    <!-- /USER AVATAR CONTENT -->

                                    <!-- USER AVATAR PROGRESS -->
                                    <div class="user-avatar-progress">
                                        <!-- HEXAGON -->
                                        <div class="hexagon-progress-40-44"></div>
                                        <!-- /HEXAGON -->
                                    </div>
                                    <!-- /USER AVATAR PROGRESS -->

                                    <!-- USER AVATAR PROGRESS BORDER -->
                                    <div class="user-avatar-progress-border">
                                        <!-- HEXAGON -->
                                        <div class="hexagon-border-40-44"></div>
                                        <!-- /HEXAGON -->
                                    </div>
                                    <!-- /USER AVATAR PROGRESS BORDER -->

                                    <!-- USER AVATAR BADGE -->
                                    <div class="user-avatar-badge">
                                        <!-- USER AVATAR BADGE BORDER -->
                                        <div class="user-avatar-badge-border">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-22-24"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR BADGE BORDER -->

                                        <!-- USER AVATAR BADGE CONTENT -->
                                        <div class="user-avatar-badge-content">
                                            <!-- HEXAGON -->
                                            <div class="hexagon-dark-16-18"></div>
                                            <!-- /HEXAGON -->
                                        </div>
                                        <!-- /USER AVATAR BADGE CONTENT -->

                                        <!-- USER AVATAR BADGE TEXT -->
                                        <p class="user-avatar-badge-text">26</p>
                                        <!-- /USER AVATAR BADGE TEXT -->
                                    </div>
                                    <!-- /USER AVATAR BADGE -->
                                </a>
                                <!-- /USER AVATAR -->

                                <!-- POST COMMENT TEXT WRAP -->
                                <div class="post-comment-text-wrap">
                                    <!-- RATING LIST -->
                                    <div class="rating-list medium">
                                        <!-- RATING -->
                                        <div class="rating medium filled">
                                            <!-- RATING ICON -->
                                            <svg class="rating-icon medium icon-star">
                                                <use xlink:href="#svg-star"></use>
                                            </svg>
                                            <!-- /RATING ICON -->
                                        </div>
                                        <!-- /RATING -->

                                        <!-- RATING -->
                                        <div class="rating medium filled">
                                            <!-- RATING ICON -->
                                            <svg class="rating-icon medium icon-star">
                                                <use xlink:href="#svg-star"></use>
                                            </svg>
                                            <!-- /RATING ICON -->
                                        </div>
                                        <!-- /RATING -->

                                        <!-- RATING -->
                                        <div class="rating medium filled">
                                            <!-- RATING ICON -->
                                            <svg class="rating-icon medium icon-star">
                                                <use xlink:href="#svg-star"></use>
                                            </svg>
                                            <!-- /RATING ICON -->
                                        </div>
                                        <!-- /RATING -->

                                        <!-- RATING -->
                                        <div class="rating medium filled">
                                            <!-- RATING ICON -->
                                            <svg class="rating-icon medium icon-star">
                                                <use xlink:href="#svg-star"></use>
                                            </svg>
                                            <!-- /RATING ICON -->
                                        </div>
                                        <!-- /RATING -->

                                        <!-- RATING -->
                                        <div class="rating medium filled">
                                            <!-- RATING ICON -->
                                            <svg class="rating-icon medium icon-star">
                                                <use xlink:href="#svg-star"></use>
                                            </svg>
                                            <!-- /RATING ICON -->
                                        </div>
                                        <!-- /RATING -->
                                    </div>
                                    <!-- /RATING LIST -->

                                    <!-- POST COMMENT TEXT -->
                                    <p class="post-comment-text"><span class="bold">Reason:</span> <span
                                            class="highlighted">Item Quality</span></p>
                                    <!-- /POST COMMENT TEXT -->
                                </div>
                                <!-- /POST COMMENT TEXT WRAP -->

                                <!-- POST COMMENT TEXT -->
                                <p class="post-comment-text">5 stars for exceptional Customer Support (quick, precise,
                                    detailed responses to questions), but 5 stars also for Design Quality and
                                    Customization. It is a beautiful clean design that can easily be customized to your
                                    needs. I had really specific questions and I received detailed solutions very
                                    promptly. Thank you!</p>
                                <!-- /POST COMMENT TEXT -->

                                <!-- CONTENT ACTIONS -->
                                <div class="content-actions">
                                    <!-- CONTENT ACTION -->
                                    <div class="content-action">
                                        <!-- META LINE -->
                                        <div class="meta-line">
                                            <!-- META LINE TEXT -->
                                            <p class="meta-line-text"><a href="profile-timeline.html">Sarah Diamond</a>
                                            </p>
                                            <!-- /META LINE TEXT -->
                                        </div>
                                        <!-- /META LINE -->

                                        <!-- META LINE -->
                                        <div class="meta-line">
                                            <!-- META LINE TIMESTAMP -->
                                            <p class="meta-line-timestamp">4 weeks ago</p>
                                            <!-- /META LINE TIMESTAMP -->
                                        </div>
                                        <!-- /META LINE -->

                                        <!-- META LINE -->
                                        <div class="meta-line settings">
                                            <!-- POST SETTINGS WRAP -->
                                            <div class="post-settings-wrap">
                                                <!-- POST SETTINGS -->
                                                <div class="post-settings post-settings-dropdown-trigger">
                                                    <!-- POST SETTINGS ICON -->
                                                    <svg class="post-settings-icon icon-more-dots">
                                                        <use xlink:href="#svg-more-dots"></use>
                                                    </svg>
                                                    <!-- /POST SETTINGS ICON -->
                                                </div>
                                                <!-- /POST SETTINGS -->

                                                <!-- SIMPLE DROPDOWN -->
                                                <div class="simple-dropdown post-settings-dropdown">
                                                    <!-- SIMPLE DROPDOWN LINK -->
                                                    <p class="simple-dropdown-link">Report Review</p>
                                                    <!-- /SIMPLE DROPDOWN LINK -->
                                                </div>
                                                <!-- /SIMPLE DROPDOWN -->
                                            </div>
                                            <!-- /POST SETTINGS WRAP -->
                                        </div>
                                        <!-- /META LINE -->
                                    </div>
                                    <!-- /CONTENT ACTION -->
                                </div>
                                <!-- /CONTENT ACTIONS -->
                            </div>
                            <!-- /POST COMMENT -->
                        </div>
                        <!-- /POST COMMENT LIST -->
                    </div>
                    <!-- /TAB BOX ITEM -->
                </div>
                <!-- /TAB BOX ITEMS -->
            </div>
            <!-- /TAB BOX -->
        </div>
        <!-- /MARKETPLACE CONTENT -->

        <!-- MARKETPLACE SIDEBAR -->
        <div class="marketplace-sidebar">
            <!-- SIDEBAR BOX -->
            <div class="sidebar-box">
                <!-- SIDEBAR BOX ITEMS -->
                <div class="sidebar-box-items">
                    <!-- PRICE TITLE -->
                    <p class="price-title big"><span class="currency">$</span> 26.00</p>
                    <!-- /PRICE TITLE -->

                    <!-- FORM -->
                    <form class="form">
                        <!-- CHECKBOX WRAP -->
                        <div class="checkbox-wrap">
                            <input type="radio" id="license-regular" name="license_type" value="license-regular"
                                   checked>
                            <!-- CHECKBOX BOX -->
                            <div class="checkbox-box">
                                <!-- ICON CROSS -->
                                <svg class="icon-cross">
                                    <use xlink:href="#svg-cross"></use>
                                </svg>
                                <!-- /ICON CROSS -->
                            </div>
                            <!-- /CHECKBOX BOX -->
                            <label class="accordion-trigger-linked" for="license-regular">Regular License</label>

                            <!-- CHECKBOX INFO -->
                            <div class="checkbox-info accordion-content-linked accordion-open">
                                <!-- CHECKBOX INFO TEXT -->
                                <p class="checkbox-info-text">For use, by you or one client, in a single end product
                                    which end users are not charged for. <a href="#">View Details</a></p>
                                <!-- /CHECKBOX INFO TEXT -->
                            </div>
                            <!-- /CHECKBOX INFO -->
                        </div>
                        <!-- /CHECKBOX WRAP -->

                        <!-- CHECKBOX WRAP -->
                        <div class="checkbox-wrap">
                            <input type="radio" id="license-extended" name="license_type" value="license-extended">
                            <!-- CHECKBOX BOX -->
                            <div class="checkbox-box">
                                <!-- ICON CROSS -->
                                <svg class="icon-cross">
                                    <use xlink:href="#svg-cross"></use>
                                </svg>
                                <!-- /ICON CROSS -->
                            </div>
                            <!-- /CHECKBOX BOX -->
                            <label class="accordion-trigger-linked" for="license-extended">Extended License</label>

                            <!-- CHECKBOX INFO -->
                            <div class="checkbox-info accordion-content-linked">
                                <!-- CHECKBOX INFO TEXT -->
                                <p class="checkbox-info-text">For use, by you or one client, in a single end product
                                    which end users can be charged for. <a href="#">View Details</a></p>
                                <!-- /CHECKBOX INFO TEXT -->
                            </div>
                            <!-- /CHECKBOX INFO -->
                        </div>
                        <!-- /CHECKBOX WRAP -->
                    </form>
                    <!-- /FORM -->

                    <!-- BUTTON -->
                    <p class="button primary">Add to Your Cart!</p>
                    <!-- /BUTTON -->

                    <!-- USER STATS -->
                    <div class="user-stats">
                        <!-- USER STAT -->
                        <div class="user-stat big">
                            <!-- USER STAT TITLE -->
                            <p class="user-stat-title">1.360</p>
                            <!-- /USER STAT TITLE -->

                            <!-- USER STAT TEXT -->
                            <p class="user-stat-text">sales</p>
                            <!-- /USER STAT TEXT -->
                        </div>
                        <!-- /USER STAT -->

                        <!-- USER STAT -->
                        <div class="user-stat big">
                            <!-- USER STAT TITLE -->
                            <p class="user-stat-title">4.2/5</p>
                            <!-- /USER STAT TITLE -->

                            <!-- RATING LIST -->
                            <div class="rating-list">
                                <!-- RATING -->
                                <div class="rating filled">
                                    <!-- RATING ICON -->
                                    <svg class="rating-icon icon-star">
                                        <use xlink:href="#svg-star"></use>
                                    </svg>
                                    <!-- /RATING ICON -->
                                </div>
                                <!-- /RATING -->

                                <!-- RATING -->
                                <div class="rating filled">
                                    <!-- RATING ICON -->
                                    <svg class="rating-icon icon-star">
                                        <use xlink:href="#svg-star"></use>
                                    </svg>
                                    <!-- /RATING ICON -->
                                </div>
                                <!-- /RATING -->

                                <!-- RATING -->
                                <div class="rating filled">
                                    <!-- RATING ICON -->
                                    <svg class="rating-icon icon-star">
                                        <use xlink:href="#svg-star"></use>
                                    </svg>
                                    <!-- /RATING ICON -->
                                </div>
                                <!-- /RATING -->

                                <!-- RATING -->
                                <div class="rating filled">
                                    <!-- RATING ICON -->
                                    <svg class="rating-icon icon-star">
                                        <use xlink:href="#svg-star"></use>
                                    </svg>
                                    <!-- /RATING ICON -->
                                </div>
                                <!-- /RATING -->

                                <!-- RATING -->
                                <div class="rating">
                                    <!-- RATING ICON -->
                                    <svg class="rating-icon icon-star">
                                        <use xlink:href="#svg-star"></use>
                                    </svg>
                                    <!-- /RATING ICON -->
                                </div>
                                <!-- /RATING -->
                            </div>
                            <!-- /RATING LIST -->
                        </div>
                        <!-- /USER STAT -->
                    </div>
                    <!-- /USER STATS -->
                </div>
                <!-- /SIDEBAR BOX ITEMS -->

                <!-- SIDEBAR BOX TITLE -->
                <p class="sidebar-box-title medium-space">Item Author</p>
                <!-- /SIDEBAR BOX TITLE -->

                <!-- SIDEBAR BOX ITEMS -->
                <div class="sidebar-box-items">
                    <!-- USER STATUS -->
                    <div class="user-status">
                        <!-- USER STATUS AVATAR -->
                        <a class="user-status-avatar" href="profile-timeline.html">
                            <!-- USER AVATAR -->
                            <div class="user-avatar small no-outline">
                                <!-- USER AVATAR CONTENT -->
                                <div class="user-avatar-content">
                                    <!-- HEXAGON -->
                                    <div class="hexagon-image-30-32" data-src="img/avatar/01.jpg"></div>
                                    <!-- /HEXAGON -->
                                </div>
                                <!-- /USER AVATAR CONTENT -->

                                <!-- USER AVATAR PROGRESS -->
                                <div class="user-avatar-progress">
                                    <!-- HEXAGON -->
                                    <div class="hexagon-progress-40-44"></div>
                                    <!-- /HEXAGON -->
                                </div>
                                <!-- /USER AVATAR PROGRESS -->

                                <!-- USER AVATAR PROGRESS BORDER -->
                                <div class="user-avatar-progress-border">
                                    <!-- HEXAGON -->
                                    <div class="hexagon-border-40-44"></div>
                                    <!-- /HEXAGON -->
                                </div>
                                <!-- /USER AVATAR PROGRESS BORDER -->

                                <!-- USER AVATAR BADGE -->
                                <div class="user-avatar-badge">
                                    <!-- USER AVATAR BADGE BORDER -->
                                    <div class="user-avatar-badge-border">
                                        <!-- HEXAGON -->
                                        <div class="hexagon-22-24"></div>
                                        <!-- /HEXAGON -->
                                    </div>
                                    <!-- /USER AVATAR BADGE BORDER -->

                                    <!-- USER AVATAR BADGE CONTENT -->
                                    <div class="user-avatar-badge-content">
                                        <!-- HEXAGON -->
                                        <div class="hexagon-dark-16-18"></div>
                                        <!-- /HEXAGON -->
                                    </div>
                                    <!-- /USER AVATAR BADGE CONTENT -->

                                    <!-- USER AVATAR BADGE TEXT -->
                                    <p class="user-avatar-badge-text">24</p>
                                    <!-- /USER AVATAR BADGE TEXT -->
                                </div>
                                <!-- /USER AVATAR BADGE -->
                            </div>
                            <!-- /USER AVATAR -->
                        </a>
                        <!-- /USER STATUS AVATAR -->

                        <!-- USER STATUS TITLE -->
                        <p class="user-status-title"><a class="bold" href="profile-timeline.html">Marina Valentine</a>
                        </p>
                        <!-- /USER STATUS TITLE -->

                        <!-- USER STATUS TEXT -->
                        <p class="user-status-text small">5 items published</p>
                        <!-- /USER STATUS TEXT -->
                    </div>
                    <!-- /USER STATUS -->

                    <!-- BADGE LIST -->
                    <div class="badge-list small align-left">
                        <!-- BADGE ITEM -->
                        <div class="badge-item text-tooltip-tft" data-title="Gold User">
                            <img src="img/badge/gold-s.png" alt="badge-gold-s">
                        </div>
                        <!-- /BADGE ITEM -->

                        <!-- BADGE ITEM -->
                        <div class="badge-item text-tooltip-tft" data-title="Profile Age">
                            <img src="img/badge/age-s.png" alt="badge-age-s">
                        </div>
                        <!-- /BADGE ITEM -->

                        <!-- BADGE ITEM -->
                        <div class="badge-item text-tooltip-tft" data-title="Caffeinated">
                            <img src="img/badge/caffeinated-s.png" alt="badge-caffeinated-s">
                        </div>
                        <!-- /BADGE ITEM -->

                        <!-- BADGE ITEM -->
                        <div class="badge-item text-tooltip-tft" data-title="The Warrior">
                            <img src="img/badge/warrior-s.png" alt="badge-warrior-s">
                        </div>
                        <!-- /BADGE ITEM -->

                        <!-- BADGE ITEM -->
                        <a class="badge-item" href="profile-badges.html">
                            <img src="img/badge/blank-s.png" alt="badge-blank-s">
                            <!-- BADGE ITEM TEXT -->
                            <p class="badge-item-text">+9</p>
                            <!-- /BADGE ITEM TEXT -->
                        </a>
                        <!-- /BADGE ITEM -->
                    </div>
                    <!-- /BADGE LIST -->

                    <!-- BUTTON -->
                    <a class="button small white" href="profile-store.html">View Author's Store</a>
                    <!-- /BUTTON -->
                </div>
                <!-- /SIDEBAR BOX ITEMS -->

                <!-- SIDEBAR BOX TITLE -->
                <p class="sidebar-box-title medium-space">Item Details</p>
                <!-- /SIDEBAR BOX TITLE -->

                <!-- SIDEBAR BOX ITEMS -->
                <div class="sidebar-box-items">
                    <!-- INFORMATION LINE LIST -->
                    <div class="information-line-list">
                        <!-- INFORMATION LINE -->
                        <div class="information-line">
                            <!-- INFORMATION LINE TITLE -->
                            <p class="information-line-title">Updated</p>
                            <!-- /INFORMATION LINE TITLE -->

                            <!-- INFORMATION LINE TEXT -->
                            <p class="information-line-text"><span class="bold">October 13rd, 2019</span></p>
                            <!-- /INFORMATION LINE TEXT -->
                        </div>
                        <!-- /INFORMATION LINE -->

                        <!-- INFORMATION LINE -->
                        <div class="information-line">
                            <!-- INFORMATION LINE TITLE -->
                            <p class="information-line-title">Created</p>
                            <!-- /INFORMATION LINE TITLE -->

                            <!-- INFORMATION LINE TEXT -->
                            <p class="information-line-text"><span class="bold">August 17th, 2019</span></p>
                            <!-- /INFORMATION LINE TEXT -->
                        </div>
                        <!-- /INFORMATION LINE -->

                        <!-- INFORMATION LINE -->
                        <div class="information-line">
                            <!-- INFORMATION LINE TITLE -->
                            <p class="information-line-title">Category</p>
                            <!-- /INFORMATION LINE TITLE -->

                            <!-- INFORMATION LINE TEXT -->
                            <p class="information-line-text"><a href="marketplace-category.html">HTML Templates</a></p>
                            <!-- /INFORMATION LINE TEXT -->
                        </div>
                        <!-- /INFORMATION LINE -->

                        <!-- INFORMATION LINE -->
                        <div class="information-line">
                            <!-- INFORMATION LINE TITLE -->
                            <p class="information-line-title">Files<br>Included</p>
                            <!-- /INFORMATION LINE TITLE -->

                            <!-- INFORMATION LINE TEXT -->
                            <p class="information-line-text"><span class="bold">HTML Files, CSS Files, JS Files, Layered PSD</span>
                            </p>
                            <!-- /INFORMATION LINE TEXT -->
                        </div>
                        <!-- /INFORMATION LINE -->

                        <!-- INFORMATION LINE -->
                        <div class="information-line">
                            <!-- INFORMATION LINE TITLE -->
                            <p class="information-line-title">Layout</p>
                            <!-- /INFORMATION LINE TITLE -->

                            <!-- INFORMATION LINE TEXT -->
                            <p class="information-line-text"><span class="bold">Responsive</span></p>
                            <!-- /INFORMATION LINE TEXT -->
                        </div>
                        <!-- /INFORMATION LINE -->

                        <!-- INFORMATION LINE -->
                        <div class="information-line">
                            <!-- INFORMATION LINE TITLE -->
                            <p class="information-line-title">Tags</p>
                            <!-- /INFORMATION LINE TITLE -->

                            <!-- INFORMATION LINE TEXT -->
                            <p class="information-line-text"><span class="bold"><a href="marketplace-category.html">Gaming</a>, <a
                                        href="marketplace-category.html">Magazine</a>, <a
                                        href="marketplace-category.html">Web</a>, <a href="marketplace-category.html">eSports</a>, <a
                                        href="marketplace-category.html">Template</a></span></p>
                            <!-- /INFORMATION LINE TEXT -->
                        </div>
                        <!-- /INFORMATION LINE -->
                    </div>
                    <!-- /INFORMATION LINE LIST -->
                </div>
                <!-- /SIDEBAR BOX ITEMS -->

                <!-- SIDEBAR BOX TITLE -->
                <p class="sidebar-box-title medium-space">Item Share</p>
                <!-- /SIDEBAR BOX TITLE -->

                <!-- SIDEBAR BOX ITEMS -->
                <div class="sidebar-box-items">
                    <!-- SOCIAL LINKS -->
                    <div class="social-links small align-left">
                        <!-- SOCIAL LINK -->
                        <a class="social-link small facebook" href="#">
                            <!-- SOCIAL LINK ICON -->
                            <svg class="social-link-icon icon-facebook">
                                <use xlink:href="#svg-facebook"></use>
                            </svg>
                            <!-- /SOCIAL LINK ICON -->
                        </a>
                        <!-- /SOCIAL LINK -->

                        <!-- SOCIAL LINK -->
                        <a class="social-link small twitter" href="#">
                            <!-- SOCIAL LINK ICON -->
                            <svg class="social-link-icon icon-twitter">
                                <use xlink:href="#svg-twitter"></use>
                            </svg>
                            <!-- /SOCIAL LINK ICON -->
                        </a>
                        <!-- /SOCIAL LINK -->
                    </div>
                    <!-- /SOCIAL LINKS -->
                </div>
                <!-- /SIDEBAR BOX ITEMS -->
            </div>
            <!-- /SIDEBAR BOX -->
        </div>
        <!-- /MARKETPLACE SIDEBAR -->
    </div>
    <!-- /GRID -->
</div>
<!-- /CONTENT GRID -->

@endsection
