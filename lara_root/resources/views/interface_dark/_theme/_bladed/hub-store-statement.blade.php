@extends("$VIEW_ROOT.layouts.base",[
        'PAGE_TITLE' => 'Exchange Tools',
        'NO_HEADER' => true,
    ])

@push('css')
    <link rel="stylesheet" href="{{ asset("assets/$VIEW_ROOT/css/posts-single.css") }}">
@endpush

@section('content')
<div class="content-grid">
    <div class="section-banner">
        <img class="section-banner-icon" src="{{ asset("assets/$VIEW_ROOT/img/banner/accounthub-icon.png") }}" alt="accounthub-icon">

        <p class="section-banner-title">Account Hub</p>

        <p class="section-banner-text">Profile info, messages, settings and much more!</p>
    </div>

    <div class="grid grid-3-9 medium-space">
        <div class="account-hub-sidebar">
            <div class="sidebar-box no-padding">
                <div class="sidebar-menu round-borders">
                    <div class="sidebar-menu-item">
                        <div class="sidebar-menu-header accordion-trigger-linked">
                            <svg class="sidebar-menu-header-icon icon-profile">
                                <use xlink:href="#svg-profile"></use>
                            </svg>

                            <div class="sidebar-menu-header-control-icon">
                                <svg class="sidebar-menu-header-control-icon-open icon-minus-small">
                                    <use xlink:href="#svg-minus-small"></use>
                                </svg>

                                <svg class="sidebar-menu-header-control-icon-closed icon-plus-small">
                                    <use xlink:href="#svg-plus-small"></use>
                                </svg>
                            </div>

                            <p class="sidebar-menu-header-title">My Profile</p>

                            <p class="sidebar-menu-header-text">Change your avatar &amp; cover, accept friends, read
                                messages and more!</p>
                        </div>

                        <div class="sidebar-menu-body accordion-content-linked">
                            <a class="sidebar-menu-link" href="hub-profile-info.html">Profile Info</a>

                            <a class="sidebar-menu-link" href="hub-profile-social.html">Social &amp; Stream</a>

                            <a class="sidebar-menu-link" href="hub-profile-notifications.html">Notifications</a>

                            <a class="sidebar-menu-link" href="hub-profile-messages.html">Messages</a>

                            <a class="sidebar-menu-link" href="hub-profile-requests.html">Friend Requests</a>
                        </div>
                    </div>

                    <div class="sidebar-menu-item">
                        <div class="sidebar-menu-header accordion-trigger-linked">
                            <svg class="sidebar-menu-header-icon icon-settings">
                                <use xlink:href="#svg-settings"></use>
                            </svg>

                            <div class="sidebar-menu-header-control-icon">
                                <svg class="sidebar-menu-header-control-icon-open icon-minus-small">
                                    <use xlink:href="#svg-minus-small"></use>
                                </svg>

                                <svg class="sidebar-menu-header-control-icon-closed icon-plus-small">
                                    <use xlink:href="#svg-plus-small"></use>
                                </svg>
                            </div>

                            <p class="sidebar-menu-header-title">Account</p>

                            <p class="sidebar-menu-header-text">Change settings, configure notifications, and review
                                your privacy</p>
                        </div>

                        <div class="sidebar-menu-body accordion-content-linked">
                            <a class="sidebar-menu-link" href="hub-account-info.html">Account Info</a>

                            <a class="sidebar-menu-link" href="hub-account-password.html">Change Password</a>

                            <a class="sidebar-menu-link" href="hub-account-settings.html">General Settings</a>
                        </div>
                    </div>

                    <div class="sidebar-menu-item">
                        <div class="sidebar-menu-header accordion-trigger-linked">
                            <svg class="sidebar-menu-header-icon icon-group">
                                <use xlink:href="#svg-group"></use>
                            </svg>

                            <div class="sidebar-menu-header-control-icon">
                                <svg class="sidebar-menu-header-control-icon-open icon-minus-small">
                                    <use xlink:href="#svg-minus-small"></use>
                                </svg>

                                <svg class="sidebar-menu-header-control-icon-closed icon-plus-small">
                                    <use xlink:href="#svg-plus-small"></use>
                                </svg>
                            </div>

                            <p class="sidebar-menu-header-title">Groups</p>

                            <p class="sidebar-menu-header-text">Create new groups, manage the ones you created or accept
                                invites!</p>
                        </div>

                        <div class="sidebar-menu-body accordion-content-linked">
                            <a class="sidebar-menu-link" href="hub-group-management.html">Manage Groups</a>

                            <a class="sidebar-menu-link" href="hub-group-invitations.html">Invitations</a>
                        </div>
                    </div>

                    <div class="sidebar-menu-item">
                        <div class="sidebar-menu-header accordion-trigger-linked">
                            <svg class="sidebar-menu-header-icon icon-store">
                                <use xlink:href="#svg-store"></use>
                            </svg>

                            <div class="sidebar-menu-header-control-icon">
                                <svg class="sidebar-menu-header-control-icon-open icon-minus-small">
                                    <use xlink:href="#svg-minus-small"></use>
                                </svg>

                                <svg class="sidebar-menu-header-control-icon-closed icon-plus-small">
                                    <use xlink:href="#svg-plus-small"></use>
                                </svg>
                            </div>

                            <p class="sidebar-menu-header-title">My Store</p>

                            <p class="sidebar-menu-header-text">Review your account, manage products check stats and
                                much more!</p>
                        </div>

                        <div class="sidebar-menu-body accordion-content-linked accordion-open">
                            <a class="sidebar-menu-link" href="hub-store-account.html">My Account</a>

                            <a class="sidebar-menu-link active" href="hub-store-statement.html">Sales Statement</a>

                            <a class="sidebar-menu-link" href="hub-store-items.html">Manage Items</a>

                            <a class="sidebar-menu-link" href="hub-store-downloads.html">Downloads</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="account-hub-content">
            <div class="section-header">
                <div class="section-header-info">
                    <p class="section-pretitle">My Store</p>

                    <h2 class="section-title">Sales Statement</h2>
                </div>
            </div>

            <div class="section-filters-bar v6 v6-2">
                <div class="section-filters-bar-actions">
                    <form class="form">
                        <div class="form-item split">
                            <div class="form-input-decorated">
                                <div class="form-input small active">
                                    <label for="statement-from-date">From Date</label>
                                    <input type="text" id="statement-from-date" name="statement_from_date"
                                           value="02/22/2019">
                                </div>

                                <svg class="form-input-icon icon-events">
                                    <use xlink:href="#svg-events"></use>
                                </svg>
                            </div>

                            <div class="form-input-decorated">
                                <div class="form-input small active">
                                    <label for="statement-to-date">To Date</label>
                                    <input type="text" id="statement-to-date" name="statement_to_date"
                                           value="11/14/2019">
                                </div>

                                <svg class="form-input-icon icon-events">
                                    <use xlink:href="#svg-events"></use>
                                </svg>
                            </div>

                            <button class="button primary">
                                <svg class="icon-magnifying-glass">
                                    <use xlink:href="#svg-magnifying-glass"></use>
                                </svg>
                            </button>
                        </div>
                    </form>
                </div>

                <div class="section-filters-bar-actions">
                    <p class="button secondary">Download Statement</p>
                </div>
            </div>

            <div class="table-wrap" data-simplebar>
                <div class="table table-sales">
                    <div class="table-header">
                        <div class="table-header-column">
                            <p class="table-header-title">Date</p>
                        </div>

                        <div class="table-header-column padded-left">
                            <p class="table-header-title">Item</p>
                        </div>

                        <div class="table-header-column centered padded">
                            <p class="table-header-title">Type</p>
                        </div>

                        <div class="table-header-column centered padded">
                            <p class="table-header-title">Code</p>
                        </div>

                        <div class="table-header-column centered padded">
                            <p class="table-header-title">Price</p>
                        </div>

                        <div class="table-header-column centered padded">
                            <p class="table-header-title">Cut</p>
                        </div>

                        <div class="table-header-column centered padded">
                            <p class="table-header-title">Earning</p>
                        </div>

                        <div class="table-header-column padded-left"></div>
                    </div>

                    <div class="table-body same-color-rows">
                        <div class="table-row micro">
                            <div class="table-column">
                                <p class="table-text"><span class="light">Nov 15th, 2019</span></p>
                            </div>

                            <div class="table-column padded-left">
                                <a class="table-link" href="marketplace-product.html"><span class="highlighted">Pixel Diamond Gaming Magazine</span></a>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-title">Sale</p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-text"><span class="light">VK1287</span></p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-title">$26</p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-text"><span class="light">50%</span></p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-title">$13</p>
                            </div>

                            <div class="table-column padded-left">
                                <div class="percentage-diff-icon-wrap positive">
                                    <svg class="percentage-diff-icon icon-plus-small">
                                        <use xlink:href="#svg-plus-small"></use>
                                    </svg>
                                </div>
                            </div>
                        </div>

                        <div class="table-row micro">
                            <div class="table-column">
                                <p class="table-text"><span class="light">Nov 15th, 2019</span></p>
                            </div>

                            <div class="table-column padded-left">
                                <a class="table-link" href="marketplace-product.html"><span class="highlighted">Twitch Stream UI Pack</span></a>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-title">Sale</p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-text"><span class="light">VK1364</span></p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-title">$12</p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-text"><span class="light">50%</span></p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-title">$6</p>
                            </div>

                            <div class="table-column padded-left">
                                <div class="percentage-diff-icon-wrap positive">
                                    <svg class="percentage-diff-icon icon-plus-small">
                                        <use xlink:href="#svg-plus-small"></use>
                                    </svg>
                                </div>
                            </div>
                        </div>

                        <div class="table-row micro">
                            <div class="table-column">
                                <p class="table-text"><span class="light">Nov 14th, 2019</span></p>
                            </div>

                            <div class="table-column padded-left">
                                <a class="table-link" href="marketplace-product.html"><span class="highlighted">Pixel Diamond Gaming Magazine</span></a>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-title">Sale</p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-text"><span class="light">VK7638</span></p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-title">$26</p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-text"><span class="light">50%</span></p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-title">$13</p>
                            </div>

                            <div class="table-column padded-left">
                                <div class="percentage-diff-icon-wrap positive">
                                    <svg class="percentage-diff-icon icon-plus-small">
                                        <use xlink:href="#svg-plus-small"></use>
                                    </svg>
                                </div>
                            </div>
                        </div>

                        <div class="table-row micro">
                            <div class="table-column">
                                <p class="table-text"><span class="light">Nov 14th, 2019</span></p>
                            </div>

                            <div class="table-column padded-left">
                                <a class="table-link" href="marketplace-product.html"><span class="highlighted">Twitch Stream UI Pack</span></a>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-title">Sale</p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-text"><span class="light">VK7285</span></p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-title">$12</p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-text"><span class="light">50%</span></p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-title">$6</p>
                            </div>

                            <div class="table-column padded-left">
                                <div class="percentage-diff-icon-wrap positive">
                                    <svg class="percentage-diff-icon icon-plus-small">
                                        <use xlink:href="#svg-plus-small"></use>
                                    </svg>
                                </div>
                            </div>
                        </div>

                        <div class="table-row micro">
                            <div class="table-column">
                                <p class="table-text"><span class="light">Nov 14th, 2019</span></p>
                            </div>

                            <div class="table-column padded-left">
                                <a class="table-link" href="marketplace-product.html"><span class="highlighted">Twitch Stream UI Pack</span></a>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-title">Sale</p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-text"><span class="light">VK9673</span></p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-title">$12</p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-text"><span class="light">50%</span></p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-title">$6</p>
                            </div>

                            <div class="table-column padded-left">
                                <div class="percentage-diff-icon-wrap positive">
                                    <svg class="percentage-diff-icon icon-plus-small">
                                        <use xlink:href="#svg-plus-small"></use>
                                    </svg>
                                </div>
                            </div>
                        </div>

                        <div class="table-row micro">
                            <div class="table-column">
                                <p class="table-text"><span class="light">Nov 12th, 2019</span></p>
                            </div>

                            <div class="table-column padded-left">
                                <a class="table-link" href="marketplace-product.html"><span class="highlighted">Gaming Coin Badges Pack</span></a>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-title">Purchase</p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-text"><span class="light">VK2589</span></p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-title">$6</p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-text"><span class="light">-</span></p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-title">-$6</p>
                            </div>

                            <div class="table-column padded-left">
                                <div class="percentage-diff-icon-wrap negative">
                                    <svg class="percentage-diff-icon icon-minus-small">
                                        <use xlink:href="#svg-minus-small"></use>
                                    </svg>
                                </div>
                            </div>
                        </div>

                        <div class="table-row micro">
                            <div class="table-column">
                                <p class="table-text"><span class="light">Nov 9th, 2019</span></p>
                            </div>

                            <div class="table-column padded-left">
                                <a class="table-link" href="marketplace-product.html"><span class="highlighted">People Illustrations Pack 01</span></a>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-title">Purchase</p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-text"><span class="light">VK3146</span></p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-title">$5</p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-text"><span class="light">-</span></p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-title">-$5</p>
                            </div>

                            <div class="table-column padded-left">
                                <div class="percentage-diff-icon-wrap negative">
                                    <svg class="percentage-diff-icon icon-minus-small">
                                        <use xlink:href="#svg-minus-small"></use>
                                    </svg>
                                </div>
                            </div>
                        </div>

                        <div class="table-row micro">
                            <div class="table-column">
                                <p class="table-text"><span class="light">Nov 9th, 2019</span></p>
                            </div>

                            <div class="table-column padded-left">
                                <a class="table-link" href="marketplace-product.html"><span class="highlighted">Pixel Diamond Gaming Magazine</span></a>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-title">Sale</p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-text"><span class="light">VK4577</span></p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-title">$26</p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-text"><span class="light">50%</span></p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-title">$13</p>
                            </div>

                            <div class="table-column padded-left">
                                <div class="percentage-diff-icon-wrap positive">
                                    <svg class="percentage-diff-icon icon-plus-small">
                                        <use xlink:href="#svg-plus-small"></use>
                                    </svg>
                                </div>
                            </div>
                        </div>

                        <div class="table-row micro">
                            <div class="table-column">
                                <p class="table-text"><span class="light">Nov 9th, 2019</span></p>
                            </div>

                            <div class="table-column padded-left">
                                <a class="table-link" href="marketplace-product.html"><span class="highlighted">Emerald Dragon Digital Marketplace</span></a>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-title">Sale</p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-text"><span class="light">VK6379</span></p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-title">$24</p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-text"><span class="light">50%</span></p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-title">$12</p>
                            </div>

                            <div class="table-column padded-left">
                                <div class="percentage-diff-icon-wrap positive">
                                    <svg class="percentage-diff-icon icon-plus-small">
                                        <use xlink:href="#svg-plus-small"></use>
                                    </svg>
                                </div>
                            </div>
                        </div>

                        <div class="table-row micro">
                            <div class="table-column">
                                <p class="table-text"><span class="light">Nov 8th, 2019</span></p>
                            </div>

                            <div class="table-column padded-left">
                                <a class="table-link" href="marketplace-product.html"><span class="highlighted">Pixel Diamond Gaming Magazine</span></a>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-title">Sale</p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-text"><span class="light">VK9932</span></p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-title">$26</p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-text"><span class="light">50%</span></p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-title">$13</p>
                            </div>

                            <div class="table-column padded-left">
                                <div class="percentage-diff-icon-wrap positive">
                                    <svg class="percentage-diff-icon icon-plus-small">
                                        <use xlink:href="#svg-plus-small"></use>
                                    </svg>
                                </div>
                            </div>
                        </div>

                        <div class="table-row micro">
                            <div class="table-column">
                                <p class="table-text"><span class="light">Nov 5th, 2019</span></p>
                            </div>

                            <div class="table-column padded-left">
                                <a class="table-link" href="marketplace-product.html"><span class="highlighted">Twitch Stream UI Pack</span></a>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-title">Sale</p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-text"><span class="light">VK1274</span></p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-title">$12</p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-text"><span class="light">50%</span></p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-title">$6</p>
                            </div>

                            <div class="table-column padded-left">
                                <div class="percentage-diff-icon-wrap positive">
                                    <svg class="percentage-diff-icon icon-plus-small">
                                        <use xlink:href="#svg-plus-small"></use>
                                    </svg>
                                </div>
                            </div>
                        </div>

                        <div class="table-row micro">
                            <div class="table-column">
                                <p class="table-text"><span class="light">Nov 4th, 2019</span></p>
                            </div>

                            <div class="table-column padded-left">
                                <a class="table-link" href="marketplace-product.html"><span class="highlighted">Emerald Dragon Digital Marketplace</span></a>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-title">Sale</p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-text"><span class="light">VK3345</span></p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-title">$24</p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-text"><span class="light">50%</span></p>
                            </div>

                            <div class="table-column centered padded">
                                <p class="table-title">$12</p>
                            </div>

                            <div class="table-column padded-left">
                                <div class="percentage-diff-icon-wrap positive">
                                    <svg class="percentage-diff-icon icon-plus-small">
                                        <use xlink:href="#svg-plus-small"></use>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="section-pager-bar-wrap align-right">
                <div class="section-pager-bar">
                    <div class="section-pager">
                        <div class="section-pager-item active">
                            <p class="section-pager-item-text">01</p>
                        </div>

                        <div class="section-pager-item">
                            <p class="section-pager-item-text">02</p>
                        </div>

                        <div class="section-pager-item">
                            <p class="section-pager-item-text">03</p>
                        </div>

                        <div class="section-pager-item">
                            <p class="section-pager-item-text">04</p>
                        </div>

                        <div class="section-pager-item">
                            <p class="section-pager-item-text">05</p>
                        </div>

                        <div class="section-pager-item">
                            <p class="section-pager-item-text">06</p>
                        </div>
                    </div>

                    <div class="section-pager-controls">
                        <div class="slider-control left disabled">
                            <svg class="slider-control-icon icon-small-arrow">
                                <use xlink:href="#svg-small-arrow"></use>
                            </svg>
                        </div>

                        <div class="slider-control right">
                            <svg class="slider-control-icon icon-small-arrow">
                                <use xlink:href="#svg-small-arrow"></use>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
