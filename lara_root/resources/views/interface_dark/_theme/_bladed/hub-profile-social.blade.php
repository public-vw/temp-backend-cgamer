@extends("$VIEW_ROOT.layouts.base",[
        'PAGE_TITLE' => 'Exchange Tools',
        'NO_HEADER' => true,
    ])

@push('css')
    <link rel="stylesheet" href="{{ asset("assets/$VIEW_ROOT/css/posts-single.css") }}">
@endpush

@section('content')
    <div class="content-grid">
    <div class="section-banner">
        <img class="section-banner-icon" src="img/banner/accounthub-icon.png" alt="accounthub-icon">

        <p class="section-banner-title">Account Hub</p>

        <p class="section-banner-text">Profile info, messages, settings and much more!</p>
    </div>

    <div class="grid grid-3-9 medium-space">
        <div class="account-hub-sidebar">
            <div class="sidebar-box no-padding">
                <div class="sidebar-menu">
                    <div class="sidebar-menu-item">
                        <div class="sidebar-menu-header accordion-trigger-linked">
                            <svg class="sidebar-menu-header-icon icon-profile">
                                <use xlink:href="#svg-profile"></use>
                            </svg>

                            <div class="sidebar-menu-header-control-icon">
                                <svg class="sidebar-menu-header-control-icon-open icon-minus-small">
                                    <use xlink:href="#svg-minus-small"></use>
                                </svg>

                                <svg class="sidebar-menu-header-control-icon-closed icon-plus-small">
                                    <use xlink:href="#svg-plus-small"></use>
                                </svg>
                            </div>

                            <p class="sidebar-menu-header-title">My Profile</p>

                            <p class="sidebar-menu-header-text">Change your avatar &amp; cover, accept friends, read
                                messages and more!</p>
                        </div>

                        <div class="sidebar-menu-body accordion-content-linked accordion-open">
                            <a class="sidebar-menu-link" href="hub-profile-info.html">Profile Info</a>

                            <a class="sidebar-menu-link active" href="hub-profile-social.html">Social &amp; Stream</a>

                            <a class="sidebar-menu-link" href="hub-profile-notifications.html">Notifications</a>

                            <a class="sidebar-menu-link" href="hub-profile-messages.html">Messages</a>

                            <a class="sidebar-menu-link" href="hub-profile-requests.html">Friend Requests</a>
                        </div>
                    </div>

                    <div class="sidebar-menu-item">
                        <div class="sidebar-menu-header accordion-trigger-linked">
                            <svg class="sidebar-menu-header-icon icon-settings">
                                <use xlink:href="#svg-settings"></use>
                            </svg>

                            <div class="sidebar-menu-header-control-icon">
                                <svg class="sidebar-menu-header-control-icon-open icon-minus-small">
                                    <use xlink:href="#svg-minus-small"></use>
                                </svg>

                                <svg class="sidebar-menu-header-control-icon-closed icon-plus-small">
                                    <use xlink:href="#svg-plus-small"></use>
                                </svg>
                            </div>

                            <p class="sidebar-menu-header-title">Account</p>

                            <p class="sidebar-menu-header-text">Change settings, configure notifications, and review
                                your privacy</p>
                        </div>

                        <div class="sidebar-menu-body accordion-content-linked">
                            <a class="sidebar-menu-link" href="hub-account-info.html">Account Info</a>

                            <a class="sidebar-menu-link" href="hub-account-password.html">Change Password</a>

                            <a class="sidebar-menu-link" href="hub-account-settings.html">General Settings</a>
                        </div>
                    </div>

                    <div class="sidebar-menu-item">
                        <div class="sidebar-menu-header accordion-trigger-linked">
                            <svg class="sidebar-menu-header-icon icon-group">
                                <use xlink:href="#svg-group"></use>
                            </svg>

                            <div class="sidebar-menu-header-control-icon">
                                <svg class="sidebar-menu-header-control-icon-open icon-minus-small">
                                    <use xlink:href="#svg-minus-small"></use>
                                </svg>

                                <svg class="sidebar-menu-header-control-icon-closed icon-plus-small">
                                    <use xlink:href="#svg-plus-small"></use>
                                </svg>
                            </div>

                            <p class="sidebar-menu-header-title">Groups</p>

                            <p class="sidebar-menu-header-text">Create new groups, manage the ones you created or accept
                                invites!</p>
                        </div>

                        <div class="sidebar-menu-body accordion-content-linked">
                            <a class="sidebar-menu-link" href="hub-group-management.html">Manage Groups</a>

                            <a class="sidebar-menu-link" href="hub-group-invitations.html">Invitations</a>
                        </div>
                    </div>

                    <div class="sidebar-menu-item">
                        <div class="sidebar-menu-header accordion-trigger-linked">
                            <svg class="sidebar-menu-header-icon icon-store">
                                <use xlink:href="#svg-store"></use>
                            </svg>

                            <div class="sidebar-menu-header-control-icon">
                                <svg class="sidebar-menu-header-control-icon-open icon-minus-small">
                                    <use xlink:href="#svg-minus-small"></use>
                                </svg>

                                <svg class="sidebar-menu-header-control-icon-closed icon-plus-small">
                                    <use xlink:href="#svg-plus-small"></use>
                                </svg>
                            </div>

                            <p class="sidebar-menu-header-title">My Store</p>

                            <p class="sidebar-menu-header-text">Review your account, manage products check stats and
                                much more!</p>
                        </div>

                        <div class="sidebar-menu-body accordion-content-linked">
                            <a class="sidebar-menu-link" href="hub-store-account.html">My Account</a>

                            <a class="sidebar-menu-link" href="hub-store-statement.html">Sales Statement</a>

                            <a class="sidebar-menu-link" href="hub-store-items.html">Manage Items</a>

                            <a class="sidebar-menu-link" href="hub-store-downloads.html">Downloads</a>
                        </div>
                    </div>
                </div>

                <div class="sidebar-box-footer">
                    <p class="button primary">Save Changes!</p>

                    <p class="button white small-space">Discard All</p>
                </div>
            </div>
        </div>

        <div class="account-hub-content">
            <div class="section-header">
                <div class="section-header-info">
                    <p class="section-pretitle">My Profile</p>

                    <h2 class="section-title">Social Networks</h2>
                </div>
            </div>

            <div class="grid-column">
                <div class="widget-box">
                    <p class="widget-box-title">Your Social Accounts</p>

                    <div class="widget-box-content">
                        <form class="form">
                            <div class="form-row split">
                                <div class="form-item">
                                    <div class="form-input social-input small active">
                                        <div class="social-link no-hover facebook">
                                            <svg class="icon-facebook">
                                                <use xlink:href="#svg-facebook"></use>
                                            </svg>
                                        </div>

                                        <label for="social-account-facebook">Facebook Username</label>
                                        <input type="text" id="social-account-facebook" name="social_account_facebook"
                                               value="marinavalentine182">
                                    </div>
                                </div>

                                <div class="form-item">
                                    <div class="form-input social-input small active">
                                        <div class="social-link no-hover twitter">
                                            <svg class="icon-twitter">
                                                <use xlink:href="#svg-twitter"></use>
                                            </svg>
                                        </div>

                                        <label for="social-account-twitter">Twitter Username</label>
                                        <input type="text" id="social-account-twitter" name="social_account_twitter"
                                               value="dghuntress">
                                    </div>
                                </div>
                            </div>

                            <div class="form-row split">
                                <div class="form-item">
                                    <div class="form-input social-input small active">
                                        <div class="social-link no-hover instagram">
                                            <svg class="icon-instagram">
                                                <use xlink:href="#svg-instagram"></use>
                                            </svg>
                                        </div>

                                        <label for="social-account-instagram">Instagram Username</label>
                                        <input type="text" id="social-account-instagram" name="social_account_instagram"
                                               value="mvalentine">
                                    </div>
                                </div>

                                <div class="form-item">
                                    <div class="form-input social-input small active">
                                        <div class="social-link no-hover twitch">
                                            <svg class="icon-twitch">
                                                <use xlink:href="#svg-twitch"></use>
                                            </svg>
                                        </div>

                                        <label for="social-account-twitch">Twitch Username</label>
                                        <input type="text" id="social-account-twitch" name="social_account_twitch"
                                               value="gamehuntress">
                                    </div>
                                </div>
                            </div>

                            <div class="form-row split">
                                <div class="form-item">
                                    <div class="form-input social-input small">
                                        <div class="social-link no-hover google">
                                            <svg class="icon-google">
                                                <use xlink:href="#svg-google"></use>
                                            </svg>
                                        </div>

                                        <label for="social-account-google">Google + Username</label>
                                        <input type="text" id="social-account-google" name="social_account_google">
                                    </div>
                                </div>

                                <div class="form-item">
                                    <div class="form-input social-input small active">
                                        <div class="social-link no-hover youtube">
                                            <svg class="icon-youtube">
                                                <use xlink:href="#svg-youtube"></use>
                                            </svg>
                                        </div>

                                        <label for="social-account-youtube">Youtube Username</label>
                                        <input type="text" id="social-account-youtube" name="social_account_youtube"
                                               value="dagamehuntress">
                                    </div>
                                </div>
                            </div>

                            <div class="form-row split">
                                <div class="form-item">
                                    <div class="form-input social-input small active">
                                        <div class="social-link no-hover patreon">
                                            <svg class="icon-patreon">
                                                <use xlink:href="#svg-patreon"></use>
                                            </svg>
                                        </div>

                                        <label for="social-account-patreon">Patreon Username</label>
                                        <input type="text" id="social-account-patreon" name="social_account_patreon"
                                               value="huntressplays">
                                    </div>
                                </div>

                                <div class="form-item">
                                    <div class="form-input social-input small active">
                                        <div class="social-link no-hover discord">
                                            <svg class="icon-discord">
                                                <use xlink:href="#svg-discord"></use>
                                            </svg>
                                        </div>

                                        <label for="social-account-discord">Discord Channel</label>
                                        <input type="text" id="social-account-discord" name="social_account_discord"
                                               value="huntressgrounds">
                                    </div>
                                </div>
                            </div>

                            <div class="form-row split">
                                <div class="form-item">
                                    <div class="form-input social-input small">
                                        <div class="social-link no-hover deviantart">
                                            <svg class="icon-deviantart">
                                                <use xlink:href="#svg-deviantart"></use>
                                            </svg>
                                        </div>

                                        <label for="social-account-deviantart">DeviantArt Username</label>
                                        <input type="text" id="social-account-deviantart"
                                               name="social_account_deviantart">
                                    </div>
                                </div>

                                <div class="form-item">
                                    <div class="form-input social-input small">
                                        <div class="social-link no-hover behance">
                                            <svg class="icon-behance">
                                                <use xlink:href="#svg-behance"></use>
                                            </svg>
                                        </div>

                                        <label for="social-account-behance">Behance Username</label>
                                        <input type="text" id="social-account-behance" name="social_account_behance">
                                    </div>
                                </div>
                            </div>

                            <div class="form-row split">
                                <div class="form-item">
                                    <div class="form-input social-input small">
                                        <div class="social-link no-hover dribbble">
                                            <svg class="icon-dribbble">
                                                <use xlink:href="#svg-dribbble"></use>
                                            </svg>
                                        </div>

                                        <label for="social-account-dribbble">Dribbble Username</label>
                                        <input type="text" id="social-account-dribbble" name="social_account_dribbble">
                                    </div>
                                </div>

                                <div class="form-item">
                                    <div class="form-input social-input small">
                                        <div class="social-link no-hover artstation">
                                            <svg class="icon-artstation">
                                                <use xlink:href="#svg-artstation"></use>
                                            </svg>
                                        </div>

                                        <label for="social-account-artstation">ArtStation Username</label>
                                        <input type="text" id="social-account-artstation"
                                               name="social_account_artstation">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="widget-box">
                    <p class="widget-box-title">Twitter Feed</p>

                    <div class="widget-box-content">
                        <div class="switch-option">
                            <p class="switch-option-title">Enable Twitter Feed</p>

                            <p class="switch-option-text">Turn on this switch to show your connected twitter account in
                                your profile page</p>

                            <div class="form-switch active">
                                <div class="form-switch-button"></div>
                            </div>

                            <a class="button twitter" href="#">
                                <svg class="button-icon spaced icon-twitter">
                                    <use xlink:href="#svg-twitter"></use>
                                </svg>
                                Link your Twitter Account
                            </a>

                            <p class="switch-option-meta">Linked Account: <span class="bold">@dghuntress</span></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="section-header">
                <div class="section-header-info">
                    <p class="section-pretitle">My Profile</p>

                    <h2 class="section-title">Twitch Stream</h2>
                </div>
            </div>

            <div class="grid-column">
                <div class="widget-box">
                    <p class="widget-box-title">Connect your Account</p>

                    <div class="widget-box-content">
                        <div class="switch-option">
                            <p class="switch-option-title">Enable Stream Profile Section</p>

                            <p class="switch-option-text">Turn on this switch to show your connected stream in your
                                profile for everyone to see!</p>

                            <div class="form-switch">
                                <div class="form-switch-button"></div>
                            </div>

                            <a class="button twitch" href="#">
                                <svg class="button-icon spaced icon-twitch">
                                    <use xlink:href="#svg-twitch"></use>
                                </svg>
                                Link your Twitch Account
                            </a>

                            <p class="switch-option-meta">Linked Account: <span class="bold">@gamehuntress</span></p>
                        </div>
                    </div>
                </div>

                <div class="widget-box">
                    <p class="widget-box-title">Your Channel FAQs</p>

                    <div class="widget-box-content">
                        <form class="form">
                            <div class="form-row split">
                                <div class="form-item">
                                    <div class="form-input small active">
                                        <label for="profile-social-question-1">Question</label>
                                        <input type="text" id="profile-social-question-1"
                                               name="profile_social_question_1" value="Do I only play new games?">
                                    </div>
                                </div>

                                <div class="form-item">
                                    <div class="form-input small mid-textarea active">
                                        <label for="profile-social-answer-1">Answer</label>
                                        <textarea id="profile-social-answer-1" name="profile_social_answer_1">Although I play a lot of newer games, I also have a small time on weekends that I use to play some cool retro games.</textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row split">
                                <div class="form-item">
                                    <div class="form-input small active">
                                        <label for="profile-social-question-2">Question</label>
                                        <input type="text" id="profile-social-question-2"
                                               name="profile_social_question_2" value="Do I take stream requests?">
                                    </div>
                                </div>

                                <div class="form-item">
                                    <div class="form-input small mid-textarea active">
                                        <label for="profile-social-answer-2">Answer</label>
                                        <textarea id="profile-social-answer-2" name="profile_social_answer_2">Yes! Join me on my channel's chatbox every saturday and I'll be taking game requests for upcoming streams.</textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row split">
                                <div class="form-item">
                                    <div class="form-input small">
                                        <label for="profile-social-question-3">Question</label>
                                        <input type="text" id="profile-social-question-3"
                                               name="profile_social_question_3">
                                    </div>
                                </div>

                                <div class="form-item">
                                    <div class="form-input small mid-textarea">
                                        <label for="profile-social-answer-3">Answer</label>
                                        <textarea id="profile-social-answer-3"
                                                  name="profile_social_answer_3"></textarea>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <p class="button small white add-field-button">+ Add New Field</p>
                    </div>
                </div>

                <div class="widget-box">
                    <p class="widget-box-title">Your Streaming Schedule</p>

                    <div class="widget-box-content">
                        <form class="form">
                            <div class="form-row">
                                <div class="form-item">
                                    <div class="form-input small mid-textarea active">
                                        <label for="profile-social-stream-schedule-description">Description</label>
                                        <textarea id="profile-social-stream-schedule-description"
                                                  name="profile_social_stream_schedule_description">My main stream is Saturday at 9PM but I also make "Let's Play" streams on weekdays. In addition I make special morning streams at 10AM all 1st‘s, 15th’s and 30th’s of every month with the lastest gaming news. All times are EDT Eastern Daylight Time</textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row split">
                                <div class="form-item">
                                    <div class="form-select">
                                        <label for="profile-social-stream-schedule-monday">Monday</label>
                                        <select id="profile-social-stream-schedule-monday"
                                                name="profile_social_stream_schedule_monday">
                                            <option value="0">-</option>
                                            <option value="1" selected>10PM</option>
                                        </select>
                                        <svg class="form-select-icon icon-small-arrow">
                                            <use xlink:href="#svg-small-arrow"></use>
                                        </svg>
                                    </div>
                                </div>

                                <div class="form-item">
                                    <div class="form-select">
                                        <label for="profile-social-stream-schedule-tuesday">Tuesday</label>
                                        <select id="profile-social-stream-schedule-tuesday"
                                                name="profile_social_stream_schedule_tuesday">
                                            <option value="0">-</option>
                                            <option value="1">10PM</option>
                                        </select>
                                        <svg class="form-select-icon icon-small-arrow">
                                            <use xlink:href="#svg-small-arrow"></use>
                                        </svg>
                                    </div>
                                </div>

                                <div class="form-item">
                                    <div class="form-select">
                                        <label for="profile-social-stream-schedule-wednesday">Wednesday</label>
                                        <select id="profile-social-stream-schedule-wednesday"
                                                name="profile_social_stream_schedule_wednesday">
                                            <option value="0">-</option>
                                            <option value="1" selected>9PM</option>
                                        </select>
                                        <svg class="form-select-icon icon-small-arrow">
                                            <use xlink:href="#svg-small-arrow"></use>
                                        </svg>
                                    </div>
                                </div>

                                <div class="form-item">
                                    <div class="form-select">
                                        <label for="profile-social-stream-schedule-thursday">Thursday</label>
                                        <select id="profile-social-stream-schedule-thursday"
                                                name="profile_social_stream_schedule_thursday">
                                            <option value="0">-</option>
                                            <option value="1" selected>10PM</option>
                                        </select>
                                        <svg class="form-select-icon icon-small-arrow">
                                            <use xlink:href="#svg-small-arrow"></use>
                                        </svg>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row split">
                                <div class="form-item">
                                    <div class="form-select">
                                        <label for="profile-social-stream-schedule-friday">Friday</label>
                                        <select id="profile-social-stream-schedule-friday"
                                                name="profile_social_stream_schedule_friday">
                                            <option value="0">-</option>
                                            <option value="1" selected>9PM</option>
                                        </select>
                                        <svg class="form-select-icon icon-small-arrow">
                                            <use xlink:href="#svg-small-arrow"></use>
                                        </svg>
                                    </div>
                                </div>

                                <div class="form-item">
                                    <div class="form-select">
                                        <label for="profile-social-stream-schedule-saturday">Saturday</label>
                                        <select id="profile-social-stream-schedule-saturday"
                                                name="profile_social_stream_schedule_saturday">
                                            <option value="0">-</option>
                                            <option value="1" selected>9PM</option>
                                        </select>
                                        <svg class="form-select-icon icon-small-arrow">
                                            <use xlink:href="#svg-small-arrow"></use>
                                        </svg>
                                    </div>
                                </div>

                                <div class="form-item">
                                    <div class="form-select">
                                        <label for="profile-social-stream-schedule-sunday">Sunday</label>
                                        <select id="profile-social-stream-schedule-sunday"
                                                name="profile_social_stream_schedule_sunday">
                                            <option value="0">-</option>
                                            <option value="1">9PM</option>
                                        </select>
                                        <svg class="form-select-icon icon-small-arrow">
                                            <use xlink:href="#svg-small-arrow"></use>
                                        </svg>
                                    </div>
                                </div>

                                <div class="form-item"></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
