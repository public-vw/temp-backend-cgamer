<h2 class="form-box-title">ثبت‌نام کاربر جدید</h2>

<p class="lined-text">کد تایید اطلاعات برای شما ارسال شد</p>

<div class="form">
    <div class="form-row">
        <div class="form-item">
            <div class="form-input" v-bind:class="{'active': auth.token != '' }">
                <label for="token">کد احراز هویت</label>
                <input type="text" id="token" v-model="auth.token" />
                <span v-if="minorState==2">کد وارد شده معتبر نمی‌باشد</span>
            </div>
        </div>
    </div>

    <div class="form-row space-between">
        <div class="form-item">
            <a class="form-link" href="javascript:;" v-on:click="resetState()">نیاز به اصلاح اطلاعات وارد شده دارید؟</a>
        </div>
        <div class="form-item" v-if="minorState==3">
            <a class="form-link" href="javascript:;" v-on:click="resendCode()">ارسال مجدد کد</a>
        </div>
    </div>

    <p class="form-text">
        <svg class="icon-trophy fill-primary-color">
            <use xlink:href="#svg-trophy"></use>
        </svg>
        در سایت کریپتوباز، با امنیت کامل درآمد کسب کنید، مقاله بخوانید و دانش خود را نشر دهید</p>
</div>
