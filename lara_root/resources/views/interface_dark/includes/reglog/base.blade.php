@push('js')
    <script>
        //removeAfterCreation
        window.routes = window.routes ? window.routes : {};
        window.routes['ajax_auth_check'] = '{{ route('auth.ajax_auth.check') }}';
        {{--window.routes['ajax_reset_password'] = '{{ route('auth.ajax_auth.reset_password') }}';--}}
        window.routes['ajax_auth2_resend'] = '{{ route('auth.ajax_auth.resend_auth2') }}';
    </script>
@endpush

<div class="form-box login-register-form-element" id="reglog-form">
    <img class="form-box-decoration medium" src="{{ asset("assets/{$VIEW_ROOT}/img/landing/rocket.png") }}" alt="register">

    <template v-if="state==0">
        @include("$VIEW_ROOT.includes.reglog._waiting_ajax")
    </template>
    <template v-else-if="state==10">
        @include("$VIEW_ROOT.includes.reglog.first_form.base")
    </template>
    <template v-else-if="state==20">
        @include("$VIEW_ROOT.includes.reglog._well_logging")
    </template>
    <template v-else-if="state==21">
        @include("$VIEW_ROOT.includes.reglog._well_logging_but")
    </template>
    <template v-else-if="state==50">
        @include("$VIEW_ROOT.includes.reglog._get_token")
    </template>
    <template v-else-if="state==40">
        @include("$VIEW_ROOT.includes.reglog._wrong_password")
    </template>

</div>
