{{--<h2 class="form-box-title">دسترسی به اکانت</h2>--}}

<div class="form">
    <p class="form-text" v-if="minorState == 1">
        <svg class="icon-check fill-primary-color">
            <use xlink:href="#svg-check"></use>
        </svg>
        اکانت شما در دست بررسی می باشد</p>
    <p class="form-text" v-else-if="minorState == 2">
        <svg class="icon-cross fill-primary-color">
            <use xlink:href="#svg-cross"></use>
        </svg>
        اکانت شما در توقیف شده است</p>
</div>
