<p class="lined-text">با اکانت شبکه‌های اجتماعی وارد شو</p>

<div class="social-links">
    <a class="social-link twitch text-tooltip-tft" data-title="Twitch" href="{{ route('auth.twitch.redirect') }}" rel="nofollow">
        <svg class="icon-twitch">
            <use xlink:href="#svg-twitch"></use>
        </svg>
    </a>

    <a class="social-link instagram text-tooltip-tft" data-title="Instagram" href="{{ route('auth.instagram.redirect') }}" rel="nofollow">
        <svg class="icon-instagram">
            <use xlink:href="#svg-instagram"></use>
        </svg>
    </a>

    <a class="social-link twitter text-tooltip-tft" data-title="Twitter" href="{{ route('auth.twitter.redirect') }}" rel="nofollow">
        <svg class="icon-twitter">
            <use xlink:href="#svg-twitter"></use>
        </svg>
    </a>

    <a class="social-link facebook text-tooltip-tft" data-title="Facebook" href="{{ route('auth.facebook.redirect') }}" rel="nofollow">
        <svg class="icon-facebook">
            <use xlink:href="#svg-facebook"></use>
        </svg>
    </a>

    <a class="social-link telegram text-tooltip-tft" data-title="Telegram" href="{{ route('auth.telegram.redirect') }}" rel="nofollow">
        <svg class="icon-telegram">
            <use xlink:href="#svg-send-message"></use>
        </svg>
    </a>

    <a class="social-link discord text-tooltip-tft" data-title="Discord" href="{{ route('auth.discord.redirect') }}" rel="nofollow">
        <svg class="icon-discord">
            <use xlink:href="#svg-discord"></use>
        </svg>
    </a>

    <a class="social-link googlew text-tooltip-tft" data-title="Google/Gmail" href="{{ route('auth.google.redirect') }}" rel="nofollow">
        <img src="{{ asset('assets/interface_dark/img/socials/googlew.png') }}" alt="google_oauth">
    </a>

    {{--    <a class="social-link steam text-tooltip-tft" data-title="Steam" href="{{ route('auth.steam.redirect') }}" rel="nofollow">--}}
    {{--        <svg class="icon-steam">--}}
    {{--            <use xlink:href="#svg-steam"></use>--}}
    {{--        </svg>--}}
    {{--    </a>--}}




    {{--    <a class="social-link youtube text-tooltip-tft" data-title="Youtube" href="{{ route('auth.youtube.redirect') }}" rel="nofollow">--}}
    {{--        <svg class="icon-youtube">--}}
    {{--            <use xlink:href="#svg-youtube"></use>--}}
    {{--        </svg>--}}
    {{--    </a>--}}
</div>

