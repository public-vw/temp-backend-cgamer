
<p class="lined-text">با ایمیل یا شماره موبایل وارد شو</p>

<form class="form">
    <template v-if="minorState == 0">
        <div class="form-row">
            <div class="form-item">
                <div class="form-input" v-bind:class="{'active': auth.username != '' }">
                    <label for="username">شماره موبایل یا ایمیل</label>
                    <input type="text" id="username" v-model="auth.username"  />
                </div>
            </div>
        </div>

        <div class="form-row">
            <div class="form-item">
                <div class="form-input" v-bind:class="{'active': auth.password != '' }">
                    <label for="password">رمز ورود</label>
                    <input type="password" id="password" v-model="auth.password" />
                </div>
            </div>
        </div>

        <div class="form-row space-between">
            <div class="form-item">
                <div class="checkbox-wrap">
                    <input type="checkbox" id="remember" v-model="auth.remember" value="1" checked />
                    <div class="checkbox-box">
                        <svg class="icon-check">
                            <use xlink:href="#svg-check"></use>
                        </svg>
                    </div>
                    <label for="remember">مرا به خاطر بسپار</label>
                </div>
            </div>

            <div class="form-item">
                <a class="form-link" href="javascript:;" v-on:click="forgetPassword()">رمز خود را فراموش کرده‌اید؟</a>
            </div>
        </div>

        <div class="form-row">
            <div class="form-item">
                <div class="checkbox-wrap">
                    <input type="checkbox" id="rules" v-model="rules" checked/>
                    <div class="checkbox-box">
                        <svg class="icon-check">
                            <use xlink:href="#svg-check"></use>
                        </svg>
                    </div>
                    <label for="rules">کلیه <a href="{{ route('public.terms') }}" target="_blank">قوانین سایت</a> را می‌پذیرم</label>
                </div>
            </div>
        </div>

        <div class="form-row">
            <div class="form-item">
                <button type="button" class="button medium secondary" v-on:click="sendAuthData()">به اکانت خود وارد شوید</button>
            </div>
        </div>
    </template>
    <template v-if="minorState == 1">
        <div class="form-row">
            <div class="form-item">
                <div class="form-input" v-bind:class="{'active': auth.username != '' }">
                    <label for="username">شماره موبایل یا ایمیل</label>
                    <input type="text" id="username" v-model="auth.username"  />
                </div>
            </div>
        </div>

        <div class="form-row">
            <div class="form-item">
                <button type="button" class="button medium secondary" v-on:click="sendForgetPasswordData()">درخواست تغییر کلمه عبور</button>
            </div>
        </div>
    </template>
</form>
