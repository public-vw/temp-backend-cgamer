<h2 class="form-box-title">دسترسی به اکانت</h2>

@include("$VIEW_ROOT.includes.reglog.first_form._with_socials")


{{--@include("$VIEW_ROOT.includes.reglog.first_form._with_userpass")--}}

<p class="form-text">
    <svg class="icon-trophy fill-primary-color">
        <use xlink:href="#svg-trophy"></use>
    </svg>
    در سایت کریپتوباز، با امنیت کامل درآمد کسب کنید، مقاله بخوانید و دانش خود را نشر دهید</p>

<p class="form-text">
    <svg class="icon-notification fill-primary-color">
        <use xlink:href="#svg-notification"></use>
    </svg>
    هر گونه استفاده از خدمات سایت کریپتوباز به معنی پذیرفتن تمام <a href="{{ route('public.terms') }}" target="_blank">قوانین سایت</a> از طرف کاربر است.</p>
