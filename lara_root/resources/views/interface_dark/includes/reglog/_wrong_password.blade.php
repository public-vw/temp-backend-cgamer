<h2 class="form-box-title">دسترسی به اکانت</h2>

<p class="lined-text">اطلاعات وارد شده صحیح نمی باشد</p>

<div class="form">
    <div class="form-row">
        <div class="form-item">
            <a class="form-link" href="javascript:;" v-on:click="resetState()">مجدد تلاش کنید</a>
        </div>
    </div>
</div>
