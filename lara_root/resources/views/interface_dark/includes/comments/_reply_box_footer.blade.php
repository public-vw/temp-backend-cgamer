
<div class="post-comment-form">
    <div class="user-avatar small no-outline">
        <div class="user-avatar-content">
            <div class="hexagon-image-30-32" data-src="{{ asset("assets/$VIEW_ROOT/img/avatar/01.jpg") }}"></div>
        </div>

        <div class="user-avatar-progress">
            <div class="hexagon-progress-40-44"></div>
        </div>

        <div class="user-avatar-progress-border">
            <div class="hexagon-border-40-44"></div>
        </div>

        <div class="user-avatar-badge">
            <div class="user-avatar-badge-border">
                <div class="hexagon-22-24"></div>
            </div>

            <div class="user-avatar-badge-content">
                <div class="hexagon-dark-16-18"></div>
            </div>

            <p class="user-avatar-badge-text">24</p>
        </div>
    </div>

    <form class="form">
        <div class="form-row">
            <div class="form-item">
                <div class="form-input small">
                    <label for="post-reply">Your Reply</label>
                    <input type="text" id="post-reply" name="post_reply">
                </div>
            </div>
        </div>
    </form>
</div>
