<div class="content-actions">
    <div class="content-action">
        <div class="meta-line">
            <div class="meta-line-list reaction-item-list small">
                <div class="reaction-item">
                    <img class="reaction-image reaction-item-dropdown-trigger"
                         src="{{ asset("assets/$VIEW_ROOT/img/reaction/happy.png") }}" alt="reaction-happy">

                    <div class="simple-dropdown padded reaction-item-dropdown">
                        <p class="simple-dropdown-text">
                            <img class="reaction" src="{{ asset("assets/$VIEW_ROOT/img/reaction/happy.png") }}" alt="reaction-happy"/>
                            <span class="bold">Happy</span>
                        </p>

                        <p class="simple-dropdown-text">Marcus Jhonson</p>
                    </div>
                </div>

                <div class="reaction-item">
                    <img class="reaction-image reaction-item-dropdown-trigger"
                         src="{{ asset("assets/$VIEW_ROOT/img/reaction/like.png") }}" alt="reaction-like">

                    <div class="simple-dropdown padded reaction-item-dropdown">
                        <p class="simple-dropdown-text"><img class="reaction"
                                                             src="{{ asset("assets/$VIEW_ROOT/img/reaction/like.png") }}"
                                                             alt="reaction-like"> <span
                                class="bold">Like</span></p>

                        <p class="simple-dropdown-text">Neko Bebop</p>

                        <p class="simple-dropdown-text">Nick Grissom</p>

                        <p class="simple-dropdown-text">Sarah Diamond</p>
                    </div>
                </div>
            </div>

            <p class="meta-line-text">4</p>
        </div>

        <div class="meta-line">
            <p class="meta-line-link light reaction-options-small-dropdown-trigger">React!</p>

            <div class="reaction-options small reaction-options-small-dropdown">
                <div class="reaction-option text-tooltip-tft" data-title="Like">
                    <img class="reaction-option-image"
                         src="{{ asset("assets/$VIEW_ROOT/img/reaction/like.png") }}"
                         alt="reaction-like">
                </div>

                <div class="reaction-option text-tooltip-tft" data-title="Love">
                    <img class="reaction-option-image"
                         src="{{ asset("assets/$VIEW_ROOT/img/reaction/love.png") }}"
                         alt="reaction-love">
                </div>

                <div class="reaction-option text-tooltip-tft" data-title="Dislike">
                    <img class="reaction-option-image"
                         src="{{ asset("assets/$VIEW_ROOT/img/reaction/dislike.png") }}"
                         alt="reaction-dislike">
                </div>

                <div class="reaction-option text-tooltip-tft" data-title="Happy">
                    <img class="reaction-option-image"
                         src="{{ asset("assets/$VIEW_ROOT/img/reaction/happy.png") }}"
                         alt="reaction-happy">
                </div>

                <div class="reaction-option text-tooltip-tft" data-title="Funny">
                    <img class="reaction-option-image"
                         src="{{ asset("assets/$VIEW_ROOT/img/reaction/funny.png") }}"
                         alt="reaction-funny">
                </div>

                <div class="reaction-option text-tooltip-tft" data-title="Wow">
                    <img class="reaction-option-image"
                         src="{{ asset("assets/$VIEW_ROOT/img/reaction/wow.png") }}"
                         alt="reaction-wow">
                </div>

                <div class="reaction-option text-tooltip-tft" data-title="Angry">
                    <img class="reaction-option-image"
                         src="{{ asset("assets/$VIEW_ROOT/img/reaction/angry.png") }}"
                         alt="reaction-angry">
                </div>

                <div class="reaction-option text-tooltip-tft" data-title="Sad">
                    <img class="reaction-option-image"
                         src="{{ asset("assets/$VIEW_ROOT/img/reaction/sad.png") }}"
                         alt="reaction-sad">
                </div>
            </div>
        </div>

        <div class="meta-line">
            <p class="meta-line-link light">Reply</p>
        </div>

        <div class="meta-line">
            <p class="meta-line-timestamp">15 minutes ago</p>
        </div>

        <div class="meta-line settings">
            <div class="post-settings-wrap">
                <div class="post-settings post-settings-dropdown-trigger">
                    <svg class="post-settings-icon icon-more-dots">
                        <use xlink:href="#svg-more-dots"></use>
                    </svg>
                </div>

                <div class="simple-dropdown post-settings-dropdown">
                    <p class="simple-dropdown-link">Report Post</p>
                </div>
            </div>
        </div>
    </div>
</div>
