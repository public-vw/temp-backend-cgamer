<div class="post-comment {{ $CLASS ?? '' }}">
    @include("$VIEW_ROOT.includes.comments._single_comment._profile")

    <p class="post-comment-text">
        <a class="post-comment-text-author" href="profile-timeline.html">Neko Bebop</a>
        {{ $CONTENT }}
    </p>

    @include("$VIEW_ROOT.includes.comments._single_comment._react")
</div>
