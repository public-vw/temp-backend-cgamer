<div id="comments" class="post-comment-list">

    @include("$VIEW_ROOT.includes.comments._reply_box")

    @include("$VIEW_ROOT.includes.comments._single_comment.base",[
        'CLASS' => 'unread',
        'CONTENT' => "I also started streaming with a simmilar game! I'm very excited to see what's
            next on your streams and for your next projects"
    ])

    @include("$VIEW_ROOT.includes.comments._single_comment.base",[
        'CLASS' => 'unread reply-2',
        'CONTENT' => "I also started streaming with a simmilar game! I'm very excited to see what's
            next on your streams and for your next projects"
    ])

    @include("$VIEW_ROOT.includes.comments._single_comment.base",[
        'CLASS' => 'reply-2',
        'CONTENT' => "I also started streaming with a simmilar game! I'm very excited to see what's
            next on your streams and for your next projects"
    ])

    @include("$VIEW_ROOT.includes.comments._single_comment.base",[
        'CONTENT' => "It was great to start this with you and keep streming together! I'm hoping that we
            can do this for many years to come...and for everyone else, keep posted because we have lots of
            surprises, including a sneak peek of upcoming games and new DLCs"
    ])

    @include("$VIEW_ROOT.includes.comments._more")

    @include("$VIEW_ROOT.includes.comments._reply_box_footer")

</div>
