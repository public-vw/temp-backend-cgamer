@push('css')
    <style>
        #bt-sp{height:187px;}
    </style>
@endpush

@push('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js">
    </script>
    <script>
        $(function(){
            $( window ).resize(function() {
                $('#bt-sp').height($(window).height() - $('#bt-sp').offset().top - 100)
            });
            $( window ).trigger('resize')
        })
    </script>
@endpush
<div id="bt-sp"> </div>
