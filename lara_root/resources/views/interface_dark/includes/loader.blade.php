<div class="page-loader">
    <div class="page-loader-decoration">
        <img src="{{ asset("assets/interface/images/logo/logo-blue.svg") }}" width="120px" height="120px" alt="{{ config('app.title', 'Laravel') }} logo">
    </div>

    <div class="page-loader-info">
        <p class="page-loader-info-title">{{ config('app.title') }}</p>

        <p class="page-loader-info-text">در حال بارگذاری ...</p>
    </div>

    <div class="page-loader-indicator loader-bars">
        <div class="loader-bar"></div>
        <div class="loader-bar"></div>
        <div class="loader-bar"></div>
        <div class="loader-bar"></div>
        <div class="loader-bar"></div>
        <div class="loader-bar"></div>
        <div class="loader-bar"></div>
        <div class="loader-bar"></div>
    </div>

    <div class="loader-fact-container">
        <span class="loader-fact-title">میدونستی ...</span>
        <span class="loader-fact-content">{{ $fact }}</span>
    </div>
</div>
