<div class="header-actions">
    <div class="sidemenu-trigger navigation-widget-trigger">
        <svg class="icon-grid">
            <use xlink:href="#svg-grid"></use>
        </svg>
    </div>

    <div class="mobilemenu-trigger navigation-widget-mobile-trigger">
        <div class="burger-icon inverted">
            <div class="burger-icon-bar"></div>

            <div class="burger-icon-bar"></div>

            <div class="burger-icon-bar"></div>
        </div>
    </div>

    <nav class="navigation">
        <ul class="menu-main">
            <li class="menu-main-item">
                <a class="menu-main-item-link" href="#">Home</a>
            </li>

            <li class="menu-main-item">
                <a class="menu-main-item-link" href="#">Careers</a>
            </li>

            <li class="menu-main-item">
                <a class="menu-main-item-link" href="#">Faqs</a>
            </li>

            <li class="menu-main-item">
                <p class="menu-main-item-link">
                    <svg class="icon-dots">
                        <use xlink:href="#svg-dots"></use>
                    </svg>
                </p>

                <ul class="menu-main">
                    <li class="menu-main-item">
                        <a class="menu-main-item-link" href="#">About Us</a>
                    </li>

                    <li class="menu-main-item">
                        <a class="menu-main-item-link" href="#">Our Blog</a>
                    </li>

                    <li class="menu-main-item">
                        <a class="menu-main-item-link" href="#">Contact Us</a>
                    </li>

                    <li class="menu-main-item">
                        <a class="menu-main-item-link" href="#">Privacy Policy</a>
                    </li>
                </ul>
            </li>
        </ul>
    </nav>
</div>
