<header class="header {{ $HEADER_CLASS ?? '' }}">

    @include("$VIEW_ROOT.includes.header._logo")
    <div></div>
    <div></div>
{{--    @include("$VIEW_ROOT.includes.header._sidebar_trigger")--}}

{{--    @include("$VIEW_ROOT.includes.header._searchbox")--}}

{{--    @include("$VIEW_ROOT.includes.header._progressbar")--}}

{{--    <div class="header-actions">--}}
{{--        @include("$VIEW_ROOT.includes.header._actions")--}}

{{--        @include("$VIEW_ROOT.includes.header._profile")--}}
{{--    </div>--}}
</header>



{{--
TODO:Seems has NO Functionality!
<aside class="floaty-bar">
    <div class="bar-actions">
        <div class="progress-stat">
            <div class="bar-progress-wrap">
                <p class="bar-progress-info">NextQQQQQ: <span class="bar-progress-text"></span></p>
            </div>

            <div id="logged-user-level-cp" class="progress-stat-bar"></div>
        </div>
    </div>

    <div class="bar-actions">
        <div class="action-list dark">
            <a class="action-list-item" href="marketplace-cart.html">
                <svg class="action-list-item-icon icon-shopping-bag">
                    <use xlink:href="#svg-shopping-bag"></use>
                </svg>
            </a>

            <a class="action-list-item" href="hub-profile-requests.html">
                <svg class="action-list-item-icon icon-friend">
                    <use xlink:href="#svg-friend"></use>
                </svg>
            </a>

            <a class="action-list-item" href="hub-profile-messages.html">
                <svg class="action-list-item-icon icon-messages">
                    <use xlink:href="#svg-messages"></use>
                </svg>
            </a>

            <a class="action-list-item unread" href="hub-profile-notifications.html">
                <svg class="action-list-item-icon icon-notification">
                    <use xlink:href="#svg-notification"></use>
                </svg>
            </a>
        </div>

        <a class="action-item-wrap" href="hub-profile-info.html">
            <div class="action-item dark">
                <svg class="action-item-icon icon-settings">
                    <use xlink:href="#svg-settings"></use>
                </svg>
            </div>
        </a>
    </div>
</aside>
--}}
