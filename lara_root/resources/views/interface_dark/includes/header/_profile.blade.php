<div class="action-item-wrap">
    <div class="action-item dark header-settings-dropdown-trigger">
        <svg class="action-item-icon icon-settings">
            <use xlink:href="#svg-settings"></use>
        </svg>
    </div>

    <div class="dropdown-navigation header-settings-dropdown">
        <div class="dropdown-navigation-header">
            <div class="user-status">
                <a class="user-status-avatar" href="profile-timeline.html">
                    <div class="user-avatar small no-outline">
                        <div class="user-avatar-content">
                            <div class="hexagon-image-30-32" data-src="{{ asset("assets/$VIEW_ROOT/img/avatar/01.jpg") }}"></div>
                        </div>

                        <div class="user-avatar-progress">
                            <div class="hexagon-progress-40-44"></div>
                        </div>

                        <div class="user-avatar-progress-border">
                            <div class="hexagon-border-40-44"></div>
                        </div>

                        <div class="user-avatar-badge">
                            <div class="user-avatar-badge-border">
                                <div class="hexagon-22-24"></div>
                            </div>

                            <div class="user-avatar-badge-content">
                                <div class="hexagon-dark-16-18"></div>
                            </div>

                            <p class="user-avatar-badge-text">24</p>
                        </div>
                    </div>
                </a>

                <p class="user-status-title"><span class="bold">Hi Marina!</span></p>

                <p class="user-status-text small"><a href="profile-timeline.html">@marinavalentine</a></p>
            </div>
        </div>

        <p class="dropdown-navigation-category">My Profile</p>

        <a class="dropdown-navigation-link" href="hub-profile-info.html">Profile Info</a>

        <a class="dropdown-navigation-link" href="hub-profile-social.html">Social &amp; Stream</a>

        <a class="dropdown-navigation-link" href="hub-profile-notifications.html">Notifications</a>

        <a class="dropdown-navigation-link" href="hub-profile-messages.html">Messages</a>

        <a class="dropdown-navigation-link" href="hub-profile-requests.html">Friend Requests</a>

        <p class="dropdown-navigation-category">Account</p>

        <a class="dropdown-navigation-link" href="hub-account-info.html">Account Info</a>

        <a class="dropdown-navigation-link" href="hub-account-password.html">Change Password</a>

        <a class="dropdown-navigation-link" href="hub-account-settings.html">General Settings</a>

        <p class="dropdown-navigation-category">Groups</p>

        <a class="dropdown-navigation-link" href="hub-group-management.html">Manage Groups</a>

        <a class="dropdown-navigation-link" href="hub-group-invitations.html">Invitations</a>

        <p class="dropdown-navigation-category">My Store</p>

        <a class="dropdown-navigation-link" href="hub-store-account.html">My Account <span class="highlighted">$250,32</span></a>

        <a class="dropdown-navigation-link" href="hub-store-statement.html">Sales Statement</a>

        <a class="dropdown-navigation-link" href="hub-store-items.html">Manage Items</a>

        <a class="dropdown-navigation-link" href="hub-store-downloads.html">Downloads</a>

        <p class="dropdown-navigation-button button small secondary">Logout</p>
    </div>
</div>
