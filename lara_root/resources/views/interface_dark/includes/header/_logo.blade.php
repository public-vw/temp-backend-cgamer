<div class="header-actions">
    <a href="{{ route('public.homepage') }}" class="header-brand">
        <div class="logo">
            <img src="{{ asset("assets/interface/images/logo/logo-white.svg") }}" width="70px" height="70px" alt="{{ config('app.title', 'Laravel') }} logo">
        </div>
        <div class="header-brand-title">{{ config('app.long_title') }}</div>
    </a>
</div>
