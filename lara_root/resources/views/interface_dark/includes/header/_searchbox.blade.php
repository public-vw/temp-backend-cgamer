<div class="header-actions search-bar">
    <div class="interactive-input dark">
        <input type="text" id="search-main" name="search_main" placeholder="Search here for people or groups">
        <div class="interactive-input-icon-wrap">
            <svg class="interactive-input-icon icon-magnifying-glass">
                <use xlink:href="#svg-magnifying-glass"></use>
            </svg>
        </div>

        <div class="interactive-input-action">
            <svg class="interactive-input-action-icon icon-cross-thin">
                <use xlink:href="#svg-cross-thin"></use>
            </svg>
        </div>
    </div>

    <div class="dropdown-box padding-bottom-small header-search-dropdown">
        <div class="dropdown-box-category">
            <p class="dropdown-box-category-title">Members</p>
        </div>

        <div class="dropdown-box-list small no-scroll">
            <a class="dropdown-box-list-item" href="profile-timeline.html">
                <div class="user-status notification">
                    <div class="user-status-avatar">
                        <div class="user-avatar small no-outline">
                            <div class="user-avatar-content">
                                <div class="hexagon-image-30-32" data-src="{{ asset("assets/$VIEW_ROOT/img/avatar/05.jpg") }}"></div>
                            </div>

                            <div class="user-avatar-progress">
                                <div class="hexagon-progress-40-44"></div>
                            </div>

                            <div class="user-avatar-progress-border">
                                <div class="hexagon-border-40-44"></div>
                            </div>

                            <div class="user-avatar-badge">
                                <div class="user-avatar-badge-border">
                                    <div class="hexagon-22-24"></div>
                                </div>

                                <div class="user-avatar-badge-content">
                                    <div class="hexagon-dark-16-18"></div>
                                </div>

                                <p class="user-avatar-badge-text">12</p>
                            </div>
                        </div>
                    </div>

                    <p class="user-status-title"><span class="bold">Neko Bebop</span></p>

                    <p class="user-status-text">1 friends in common</p>

                    <div class="user-status-icon">
                        <svg class="icon-friend">
                            <use xlink:href="#svg-friend"></use>
                        </svg>
                    </div>
                </div>
            </a>

            <a class="dropdown-box-list-item" href="profile-timeline.html">
                <div class="user-status notification">
                    <div class="user-status-avatar">
                        <div class="user-avatar small no-outline">
                            <div class="user-avatar-content">
                                <div class="hexagon-image-30-32" data-src="{{ asset("assets/$VIEW_ROOT/img/avatar/15.jpg") }}"></div>
                            </div>

                            <div class="user-avatar-progress">
                                <div class="hexagon-progress-40-44"></div>
                            </div>

                            <div class="user-avatar-progress-border">
                                <div class="hexagon-border-40-44"></div>
                            </div>

                            <div class="user-avatar-badge">
                                <div class="user-avatar-badge-border">
                                    <div class="hexagon-22-24"></div>
                                </div>

                                <div class="user-avatar-badge-content">
                                    <div class="hexagon-dark-16-18"></div>
                                </div>

                                <p class="user-avatar-badge-text">7</p>
                            </div>
                        </div>
                    </div>

                    <p class="user-status-title"><span class="bold">Tim Rogers</span></p>

                    <p class="user-status-text">4 friends in common</p>

                    <div class="user-status-icon">
                        <svg class="icon-friend">
                            <use xlink:href="#svg-friend"></use>
                        </svg>
                    </div>
                </div>
            </a>
        </div>

        <div class="dropdown-box-category">
            <p class="dropdown-box-category-title">Groups</p>
        </div>

        <div class="dropdown-box-list small no-scroll">
            <a class="dropdown-box-list-item" href="group-timeline.html">
                <div class="user-status notification">
                    <div class="user-status-avatar">
                        <div class="user-avatar small no-border">
                            <div class="user-avatar-content">
                                <div class="hexagon-image-40-44" data-src="{{ asset("assets/$VIEW_ROOT/img/avatar/24.jpg") }}"></div>
                            </div>
                        </div>
                    </div>

                    <p class="user-status-title"><span class="bold">Cosplayers of the World</span></p>

                    <p class="user-status-text">139 members</p>

                    <div class="user-status-icon">
                        <svg class="icon-group">
                            <use xlink:href="#svg-group"></use>
                        </svg>
                    </div>
                </div>
            </a>
        </div>

        <div class="dropdown-box-category">
            <p class="dropdown-box-category-title">Marketplace</p>
        </div>

        <div class="dropdown-box-list small no-scroll">
            <a class="dropdown-box-list-item" href="marketplace-product.html">
                <div class="user-status no-padding-top">
                    <div class="user-status-avatar">
                        <figure class="picture small round liquid">
                            <img src="{{ asset("assets/$VIEW_ROOT/img/marketplace/items/07.jpg") }}" alt="item-07">
                        </figure>
                    </div>

                    <p class="user-status-title"><span class="bold">Mercenaries White Frame</span></p>

                    <p class="user-status-text">By Neko Bebop</p>

                    <div class="user-status-icon">
                        <svg class="icon-marketplace">
                            <use xlink:href="#svg-marketplace"></use>
                        </svg>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>
