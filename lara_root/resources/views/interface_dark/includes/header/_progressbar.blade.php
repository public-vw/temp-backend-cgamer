<div class="header-actions">
    <div class="progress-stat">
        <div class="bar-progress-wrap">
            <p class="bar-progress-info">Next: <span class="bar-progress-text"></span></p>
        </div>

        <div id="logged-user-level" class="progress-stat-bar"></div>
    </div>
</div>
