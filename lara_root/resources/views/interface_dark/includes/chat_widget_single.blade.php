<aside id="chat-widget-message" class="chat-widget chat-widget-overlay hidden sidebar right">
    <div class="chat-widget-header">
        <div class="chat-widget-close-button">
            <svg class="chat-widget-close-button-icon icon-back-arrow">
                <use xlink:href="#svg-back-arrow"></use>
            </svg>
        </div>

        <div class="user-status">
            <div class="user-status-avatar">
                <div class="user-avatar small no-outline online">
                    <div class="user-avatar-content">
                        <div class="hexagon-image-30-32" data-src="{{ asset("assets/$VIEW_ROOT/img/avatar/03.jpg") }}"></div>
                    </div>

                    <div class="user-avatar-progress">
                        <div class="hexagon-progress-40-44"></div>
                    </div>

                    <div class="user-avatar-progress-border">
                        <div class="hexagon-border-40-44"></div>
                    </div>

                    <div class="user-avatar-badge">
                        <div class="user-avatar-badge-border">
                            <div class="hexagon-22-24"></div>
                        </div>

                        <div class="user-avatar-badge-content">
                            <div class="hexagon-dark-16-18"></div>
                        </div>

                        <p class="user-avatar-badge-text">16</p>
                    </div>
                </div>
            </div>

            <p class="user-status-title"><span class="bold">Nick Grissom</span></p>

            <p class="user-status-tag online">Online</p>
        </div>
    </div>

    <div class="chat-widget-conversation" data-simplebar>
        <div class="chat-widget-speaker left">
            <div class="chat-widget-speaker-avatar">
                <div class="user-avatar tiny no-border">
                    <div class="user-avatar-content">
                        <div class="hexagon-image-24-26" data-src="{{ asset("assets/$VIEW_ROOT/img/avatar/03.jpg") }}"></div>
                    </div>
                </div>
            </div>

            <p class="chat-widget-speaker-message">Hi Marina! It's been a long time!</p>

            <p class="chat-widget-speaker-timestamp">Yesterday at 8:36PM</p>
        </div>

        <div class="chat-widget-speaker right">
            <p class="chat-widget-speaker-message">Hey Nick!</p>

            <p class="chat-widget-speaker-message">You're right, it's been a really long time! I think the last time we
                saw was at Neko's party</p>

            <p class="chat-widget-speaker-timestamp">10:05AM</p>
        </div>

        <div class="chat-widget-speaker left">
            <div class="chat-widget-speaker-avatar">
                <div class="user-avatar tiny no-border">
                    <div class="user-avatar-content">
                        <div class="hexagon-image-24-26" data-src="{{ asset("assets/$VIEW_ROOT/img/avatar/03.jpg") }}"></div>
                    </div>
                </div>
            </div>

            <p class="chat-widget-speaker-message">Yeah! I remember now! The stream launch party</p>

            <p class="chat-widget-speaker-message">That reminds me that I wanted to ask you something</p>

            <p class="chat-widget-speaker-message">Can you stream the new game?</p>
        </div>
    </div>

    <form class="chat-widget-form">
        <div class="interactive-input small">
            <input type="text" id="chat-widget-message-text" name="chat_widget_message_text"
                   placeholder="Write a message...">
            <div class="interactive-input-icon-wrap">
                <svg class="interactive-input-icon icon-send-message">
                    <use xlink:href="#svg-send-message"></use>
                </svg>
            </div>

            <div class="interactive-input-action">
                <svg class="interactive-input-action-icon icon-cross-thin">
                    <use xlink:href="#svg-cross-thin"></use>
                </svg>
            </div>
        </div>
    </form>
</aside>
