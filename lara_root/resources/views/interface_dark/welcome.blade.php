@extends("$VIEW_ROOT.layouts.base",[
        'PAGE_TITLE' => 'Home',
        'NO_LOADER' => true,
        'NO_HEADER' => true,
    ])

@section('content')
    <div class="landing">
        <div class="landing-decoration"></div>

        <div class="landing-info">
            <div class="logo">
                <img src="{{ asset("assets/interface/images/logo/logo-blue.svg") }}" width="60px">
            </div>

            <h2 class="landing-info-pretitle">خوش اومدی به</h2>
            <h1 class="landing-info-title">کریپتوباز</h1>

            <p class="landing-info-text">نسل بعدی شبکه اجتماعی و انجمن! با دوستان خود ارتباط برقرار کنید</p>

        </div>
        @include("$VIEW_ROOT.includes.bottom_spacer")
    </div>
@endsection
