@include("$VIEW_ROOT.admins.article_category.edit_tools.items._heading_action_buttons",[
    'ITEM_TYPE' => 'ArticleCategory'
])
<figure class="post-preview-image liquid">
    <img src="{{ $IMAGE }}" alt="{{ $ALT }}">
</figure>
