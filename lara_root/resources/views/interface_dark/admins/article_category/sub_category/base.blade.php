<div class="post-preview item-container" data-cat-id="{{ $subcategory->id }}">

    @include("$VIEW_ROOT.admins.article_category.sub_category._image",[
        'IMAGE' => $subcategory->lastImage('article_category_header') ?? asset("assets/$VIEW_ROOT/img/cover/10.jpg"),
        'ALT' => $subcategory->seo()['google']->title ?? $subcategory->heading
    ])
    <div class="post-preview-info fixed-height">
        @include("$VIEW_ROOT.admins.article_category.sub_category._header",[
            'LAST_UPDATE' => $subcategory->updated_at->diffForHumans(),
            'TITLE' => $subcategory->heading,
        ])

        @include("$VIEW_ROOT.admins.article_category.sub_category._body",[
            'CONTENT' => $subcategory->summary,
            'LINK' => route('admin.article_categories.inface.edit',['article_category' => $subcategory]),
        ])
    </div>

    <div class="content-actions">
{{--        @include("$VIEW_ROOT.admins.article_category.sub_category._react")--}}
{{--        @include("$VIEW_ROOT.admins.article_category.sub_category._state")--}}
    </div>
</div>
