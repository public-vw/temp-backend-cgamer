<div class="post-preview-info-top">
    <p class="post-preview-timestamp">آخرین بروزرسانی {{ $LAST_UPDATE }}</p>

    <p class="post-preview-title" contenteditable="true" oninput="handleItemInput('header',this, 'text')">{{ $TITLE }}</p>
</div>
