<div class="profile-header category-header">
    @include("$VIEW_ROOT.admins.article_category.header._image",[
        'IMAGE' => $category->lastImage('article_category_header') ?? asset("assets/$VIEW_ROOT/img/cover/01.jpg"),
        'ALT' => $category->seo()['google']->title ?? $category->heading
    ])

    <div class="profile-header-info category-header-info">
{{--        @include("$VIEW_ROOT.admins.article_category.header._avatar")--}}

{{--        @include("$VIEW_ROOT.admins.article_category.header._socials")--}}

{{--        @include("$VIEW_ROOT.admins.article_category.header._states")--}}

{{--        @include("$VIEW_ROOT.admins.article_category.header._actions")--}}

        <h1 class="category-title" contenteditable="true" id="article-header"
            oninput="handleInput('header',this, 'text')">{{ $category->heading ?? 'عنوان دسته‌بندی'}}
            <a href="{{ $category->seoUrl() }}" contenteditable="false" class="button float-left px-3" target="_blank">مشاهده نمای کاربران</a>
            <a href="javascript:;" id='seo_meta_button' contenteditable="false" class="button float-left px-3">سئو متا</a>
        </h1>
        @include("$VIEW_ROOT.admins.article_category.header._seo_meta")

    </div>
</div>
