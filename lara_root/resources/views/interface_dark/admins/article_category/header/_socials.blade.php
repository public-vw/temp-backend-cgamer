<div class="profile-header-social-links-wrap">
    <div id="profile-header-social-links-slider" class="profile-header-social-links">
        <div class="profile-header-social-link">
            <a class="social-link facebook" href="#">
                <svg class="icon-facebook">
                    <use xlink:href="#svg-facebook"></use>
                </svg>
            </a>
        </div>

        <div class="profile-header-social-link">
            <a class="social-link twitter" href="#">
                <svg class="icon-twitter">
                    <use xlink:href="#svg-twitter"></use>
                </svg>
            </a>
        </div>

        <div class="profile-header-social-link">
            <a class="social-link instagram" href="#">
                <svg class="icon-instagram">
                    <use xlink:href="#svg-instagram"></use>
                </svg>
            </a>
        </div>

        <div class="profile-header-social-link">
            <a class="social-link twitch" href="#">
                <svg class="icon-twitch">
                    <use xlink:href="#svg-twitch"></use>
                </svg>
            </a>
        </div>

        <div class="profile-header-social-link">
            <a class="social-link youtube" href="#">
                <svg class="icon-youtube">
                    <use xlink:href="#svg-youtube"></use>
                </svg>
            </a>
        </div>

        <div class="profile-header-social-link">
            <a class="social-link patreon" href="#">
                <svg class="icon-patreon">
                    <use xlink:href="#svg-patreon"></use>
                </svg>
            </a>
        </div>

        <div class="profile-header-social-link">
            <a class="social-link discord" href="#">
                <svg class="icon-discord">
                    <use xlink:href="#svg-discord"></use>
                </svg>
            </a>
        </div>
    </div>
    <div id="profile-header-social-links-slider-controls" class="slider-controls">
        <div class="slider-control right">
            <svg class="slider-control-icon icon-small-arrow">
                <use xlink:href="#svg-small-arrow"></use>
            </svg>
        </div>

        <div class="slider-control right">
            <svg class="slider-control-icon icon-small-arrow">
                <use xlink:href="#svg-small-arrow"></use>
            </svg>
        </div>
    </div>
</div>
