@include("$VIEW_ROOT.admins.article_category.edit_tools.items._heading_action_buttons",[
    'ITEM_TYPE' => 'Article'
])
<figure class="event-preview-cover category-post liquid">
    <img src="{{ $IMAGE }}" alt="{{ $ALT }}">
</figure>
<div class="admin-buttons">
    <a class="button float-left px-3">{{ config_trans('enums.articles_status',$ARTICLE->status) }}</a>
</div>
