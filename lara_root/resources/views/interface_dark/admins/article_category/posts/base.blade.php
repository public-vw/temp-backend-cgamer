<div class="event-preview item-container" data-cat-id="{{ $ARTICLE->id }}">
    @include("$VIEW_ROOT.admins.article_category.posts._image",[
        'IMAGE' => $ARTICLE->lastImage('article_header') ?? $ARTICLE->category->lastImage('article_default_header') ?? asset("assets/$VIEW_ROOT/img/cover/19.jpg"),
        'ALT' => $ARTICLE->seo()['google']->title ?? $ARTICLE->heading
    ])

    <div class="event-preview-info">
        <div class="event-preview-info-top">
{{--            @include("$VIEW_ROOT.admins.article_category.posts._date")--}}

            @include("$VIEW_ROOT.admins.article_category.posts._heading",[
                        'HEADING' => $ARTICLE->heading
                        ])


            <div class="post-open-paragraph" contenteditable="true" oninput="handleItemInput('body',this, 'text')">{!! $ARTICLE->summary ?? '' !!}</div>
        </div>

        <div class="event-preview-info-bottom">

{{--            @include("$VIEW_ROOT.admins.article_category.posts._text_icon")--}}

{{--            @include("$VIEW_ROOT.admins.article_category.posts._react")--}}

            <a class="button white white-tertiary" href="{{ $ARTICLE->seoUrl() }}">بیشتر بدانید ...</a>
        </div>
    </div>
</div>
