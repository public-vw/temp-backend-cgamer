@php
    $categories = $article->category->categoryChain(['slug','heading']);

    $html[] = [
        'url' => route('public.homepage'),
        'title' => __('seo.homepage.title'),
    ];

    $slugs = [];
    foreach($categories as $category){
        $slugs[] = $category['slug'];
        $html[] = [
            'url' => route('footer.article_categories.single.view',[implode('/',$slugs)]),
            'title' => $category['heading'],
        ];
    }

    $html[] = [
        'url' => 'javascript:;',
        'title' => $article->heading,
    ];
@endphp

<ul class="post-open-breadcrumb breadcrumb">
@foreach($html as $li)
    <li>
        <a href="{{ $li['url'] }}" @if(!$loop->last) target="_blank" @endif>
            {{ $li['title'] }}
        </a>
    </li>
@endforeach
</ul>

