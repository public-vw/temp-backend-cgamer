@pushonce('_css:croppie')
<style type="text/css">
    #cropi {
        -ms-transform: scale(0.5, 0.5); /* IE 9 */
        -webkit-transform: scale(0.5, 0.5); /* Safari */
        transform: scale(0.5, 0.5);
        border: 1px dashed blue;
    }
</style>
@endpushonce
@pushonce('_js:croppie')
<script type="text/javascript">
    var parentW = $('#cropi').width();
{{--    var upload_url = "{{route('panel_upload',['page'=>$IMAGE_TYPE])}}";--}}
    var waiting_icon = "{{asset('defaults/images/ajax_waiting.gif')}}";
    var view_height = Math.min(500,{{$imageTypes[$IMAGE_TYPE]->max_width ?? 300}});
    var view_width = Math.min(500,{{$imageTypes[$IMAGE_TYPE]->max_height ?? 300}});
    // var bound_height = Math.max(Math.max(2*view_width,2*view_height),600);
    // var bound_height = Math.max(2*view_width,2*view_height);
    var bound_height = view_height+100;
    var bound_width = view_width+100;
    var nocopypaste = @if($NOCOPYPASTE ?? false) true @else false @endif ;
</script>
<script src="{{ asset('assets/interface/js/croppie_adder.js') }}" type="text/javascript"></script>
@endpushonce
@pushonce('js:croppie')
<script src="{{ asset('assets/interface/js/croppie_adder.js') }}" type="text/javascript"></script>
@endpushonce

<div id="picture_NAME" class="col-md-6 picture cropiTrigContainer">
    <input type="hidden" class='picture_id' name="NAME" value=""/>
    <div>
{{--        <img src="{{asset(isset($act_res[$NAME])&&isset($act_res[$ANCHOR]->url)?$act_res[$ANCHOR]->url : (isset($DEFAULT)?$DEFAULT:assetlink('defaults/images/no_image.jpg')) )}}" height="{{$BOX_HEIGHT or 50}}" class="cropiTrig"/>--}}
    </div>
</div>
