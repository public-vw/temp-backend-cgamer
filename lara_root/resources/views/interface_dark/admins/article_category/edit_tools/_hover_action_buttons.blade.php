@push('js')
    <script>
        //removeAfterCreation
        window.routes['article_search'] = '{{ route('client.articles.search.json') }}';
    </script>
@endpush


<div id="hover-action-buttons" contenteditable="false">
    <div class="btn set-link">
        <span class="btn-icon"><i class="fas fa-link"></i></span>
    </div>

</div>

<div class="hover-action-forms frm set-link" contenteditable="false">
    <div class="row nav-buttons">
        <button data-bind=".anchor"   class="selected" value="1">لینک به داخل متن</button>
        <button data-bind=".internal"   value="2">لینک به صفحه سایت</button>
        <button data-bind=".external" value="3">لینک به خارج از سایت</button>
    </div>
    <div class="row-box anchor">
        <select id="anchor-target">
            <option value="">یک تیتر رو انتخاب کن</option>
            <option>هدینگ اول</option>
            <option>هدینگ دوم</option>
        </select>
    </div>
    <div class="row-box internal">
        <select id="internal-target">
            <option value="">یک نوشته رو انتخاب کن</option>
            <option>مقاله شماره یک</option>
        </select>
        <div class="row">
            <span><input type="checkbox" data-config="new" value="1" checked>new page</span>
        </div>
    </div>
    <div class="row-box external">
        <input type="text" id="external-target" placeholder="آدرس URL صفحه مورد نظر"/>
        <div class="row">
            <span><input type="checkbox" data-config="new" value="1" checked>new page</span>
            <span><input type="checkbox" data-config="nof" value="2" checked>no follow</span>
        </div>
    </div>
    <div class="row submit">
        <button class='btn btn-submit' type="button">ثبت کن</button>
        <a href="javascript:;" class="btn-cancel">لغو</a>

    </div>
</div>
