<div class="item-heading-action-buttons" contenteditable="false">
    <div class="btn change-image">
        <span class="btn-icon"><i class="fas fa-image"></i>&nbsp;تغییر تصویر</span>
    </div>
    <div class="btn item-save-action" style="display: none" data-item-type="{{ $ITEM_TYPE }}"></div>
    <div class="btn item-cancel-action" style="display: none"></div>

</div>
