<p data-key='paragraph' class="template category-description" style="display:{{ $display }}" data-placeholder="برای تغییر این پاراگراف اینجا کلیک کن"></p>

<h2 data-key='heading2' class="template post-open-title" style="display:{{ $display }}" data-placeholder="تیتر H2"></h2>

<h3 data-key='heading3' class="template post-open-title" style="display:{{ $display }}" data-placeholder="تیتر H3"></h3>

<h4 data-key='heading4' class="template post-open-title" style="display:{{ $display }}" data-placeholder="تیتر H4"></h4>

<ul data-key='ul' class="template post-open-paragraph" style="display:{{ $display }}"><li class="editable" data-placeholder="آیتم ۱"></li><li class="editable" data-placeholder="آیتم ۲"></li></ul>

<ol data-key='ol' class="template post-open-paragraph" style="display:{{ $display }}"><li class="editable" data-placeholder="آیتم ۱"></li><li class="editable" data-placeholder="آیتم ۲"></li></ol>

<a data-key='link' class="template" style="display:{{ $display }}" href="##LINK##" data-config="">##TITLE##</a>

<div data-key='image' class="template" data-changed="1" style="display:{{ $display }}" id="">
    <figure class="post-open-image">
        <img src="{{ asset('assets/interface/images/690x320.png') }}" data-db-id="placeholder"/>
    </figure>
    <p class="post-open-image-caption editable" data-placeholder="عنوان توصیفی تصویر"></p>
</div>


