@include("$VIEW_ROOT.admins.edit_tools.articles_single._heading_action_buttons")
<figure class="post-open-cover liquid" id="heading_image">
    @isset($article)
    <img src="{{ $article->lastImage('article_header') ?? asset("assets/interface/images/article_heading/top-view-typing2.webp") }}" alt="">
    @else
        <img src="{{ asset("assets/interface/images/article_heading/top-view-typing2.webp") }}"/>
    @endisset
</figure>
