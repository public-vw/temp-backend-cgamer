<div class="section-filters-bar v2">
    <form class="form">
        <div class="form-item split medium">
            <div class="form-select">
                <label for="post-filter-category">Filter By</label>
                <select id="post-filter-category" name="post_filter_category">
                    <option value="0">Reactions Received</option>
                    <option value="1">Comment Count</option>
                    <option value="2">Share Count</option>
                </select>
                <svg class="form-select-icon icon-small-arrow">
                    <use xlink:href="#svg-small-arrow"></use>
                </svg>
            </div>

            <div class="form-select">
                <label for="post-filter-order">Order By</label>
                <select id="post-filter-order" name="post_filter_order">
                    <option value="0">Highest to Lowest</option>
                    <option value="1">Lowest to Highest</option>
                </select>
                <svg class="form-select-icon icon-small-arrow">
                    <use xlink:href="#svg-small-arrow"></use>
                </svg>
            </div>

            <div class="form-select">
                <label for="post-filter-show">Show</label>
                <select id="post-filter-show" name="post_filter_show">
                    <option value="0">12 Posts per Page</option>
                    <option value="1">24 Posts per Page</option>
                </select>
                <svg class="form-select-icon icon-small-arrow">
                    <use xlink:href="#svg-small-arrow"></use>
                </svg>
            </div>

            <button class="button primary">Filter Posts</button>
        </div>
    </form>
</div>

