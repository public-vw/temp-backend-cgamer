@extends("$VIEW_ROOT.layouts.base",[
        'FILE_NAME' => 'profile-blog',
        'PAGE_TITLE' => 'Article Category',
        'HEADER_CLASS' => 'category-header',
    ])

@section('page_title')
    {{ $category->seo()['google']->title ?? $category->heading }}
@endsection

@section('page_keywords')
    {{ $category->seo()['google']->keywords ?? '' }}
@endsection

@section('page_description')
    {{ $category->seo()['google']->description ?? $category->heading }}
@endsection

@push('css')
    <link rel="stylesheet" href="{{ asset("assets/$VIEW_ROOT/admins/css/article-category.css") }}">
@endpush

@push('js')
    <script src="{{ asset("assets/interface/admins/js/article-category.js") }}"></script>
    <script>
        //removeAfterCreation
        window.routes = {};
        window.routes['article_category_save_draft'] = '{{ route('admin.article_categories.inface.save_draft') }}';
        window.routes['item_save'] = '{{ route('admin.article_categories.inface.save_items',['item_type' => '#####']) }}';
        window.routes['upload_image_header'] = '{{ route('admin.attachments.upload.by_data',['type' => 'article_category_header']) }}';
        window.routes['preview'] = '{{ route('admin.article_categories.preview',['article_category' => '#####']) }}';
        window.routes['article_category_save_seo_meta'] = '{{ route('admin.article_categories.save_meta',['article_category' => $category]) }}';
        {{--window.routes['upload_image_inside_map'] = '{{ route('client.attachments.upload.by_data.by_chunk.map',['type' => 'article_inside']) }}';--}}
        {{--window.routes['upload_image_inside_chunks'] = '{{ route('client.attachments.upload.by_data.chunk',['key' => '#####']) }}';--}}
    </script>
@endpush

@section('content')

    {{--    TODO: sass._grid:408 enable padding in full mode, if you want these --}}
    {{--    TODO: sass._post-open:125 enable smaller width, if you want these --}}
    {{--    @include("$VIEW_ROOT.includes.nav_widget_small")--}}


    {{--    TODO: These are good to show author properties--}}
    {{--    TODO: sass._post-open:125 enable smaller width, if you want these --}}
    {{--    @include("$VIEW_ROOT.includes.nav_widget")--}}
    {{--    @include("$VIEW_ROOT.includes.nav_widget_mobile")--}}

    <div class="content-grid" id="article-body" data-id="{{isset($category)?$category->id:''}}" data-length="{{ strlen($category->description) }}">
        <form id="uploader-form" method="POST" action="{{ route('client.attachments.upload.by_data',['type' => 'article_inside']) }}" enctype="multipart/form-data">
            <input type="file" id="uploader" name="file" accept="image/*" class="not-visible"/>
        </form>

        @include("$VIEW_ROOT.admins.article_category.edit_tools._hover_action_buttons")
        @include("$VIEW_ROOT.admins.article_category.edit_tools._inline_action_buttons")
        @include("$VIEW_ROOT.admins.article_category.edit_tools._templates",['display' => 'none'])

        @include("$VIEW_ROOT.admins.article_category.header.base")

        <section class="section">
            <div contenteditable="true" id="article-content" class="post-open-content" oninput="handleInput('body',this)">
                @if(isset($category) && trim($category->description) )
                    {!! $category->description !!}
                @else
                    <div class="placeholder">Content Goes Here!</div>
                @endif
            </div>

            @include("$VIEW_ROOT.admins.article_category.edit_tools._action_buttons")
{{--            @include("$VIEW_ROOT.admins.article_category._filter")--}}

            @if($category->articles()->count())
                @include("$VIEW_ROOT.admins.article_category._section_heading",['TITLE'=>'آخرین نوشته‌ها'])
                <div class="grid grid-3-3-3-3 centered-on-mobile">
                    @foreach($category->activeArticles() as $article)
                        @include("$VIEW_ROOT.admins.article_category.posts.base",['ARTICLE' => $article])
                    @endforeach
                </div>
            @endif

        </section>

        <section class="section">
            @if($category->hasChild())
                @include("$VIEW_ROOT.admins.article_category._section_heading",['TITLE' => 'دسته‌بندی‌های زیرمجموعه این دسته‌بندی'])
                <div class="grid grid-4-4-4 centered-on-mobile">
                    @foreach($category->children as $subcategory)
                        @include("$VIEW_ROOT.admins.article_category.sub_category.base")
                    @endforeach
                </div>
            @endif
        </section>

    </div>

@endsection
