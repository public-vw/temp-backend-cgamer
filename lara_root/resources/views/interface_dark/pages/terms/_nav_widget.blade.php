<nav id="navigation-widget" class="navigation-widget navigation-widget-desktop sidebar left hidden" data-simplebar>
    <figure class="navigation-widget-cover liquid">
        <img src="{{ asset("assets/$VIEW_ROOT/img/cover/01.jpg") }}" alt="cover-01">
    </figure>

    <div class="user-short-description">
        <a class="user-short-description-avatar user-avatar medium" href="profile-timeline.html">
            <div class="user-avatar-border">
                <div class="hexagon-120-132"></div>
            </div>

            <div class="user-avatar-content">
                <div class="hexagon-image-82-90" data-src="{{ asset("assets/$VIEW_ROOT/img/avatar/01.jpg") }}"></div>
            </div>

            <div class="user-avatar-progress">
                <div class="hexagon-progress-100-110"></div>
            </div>

            <div class="user-avatar-progress-border">
                <div class="hexagon-border-100-110"></div>
            </div>

            <div class="user-avatar-badge">
                <div class="user-avatar-badge-border">
                    <div class="hexagon-32-36"></div>
                </div>

                <div class="user-avatar-badge-content">
                    <div class="hexagon-dark-26-28"></div>
                </div>

                <p class="user-avatar-badge-text">24</p>
            </div>
        </a>

        <p class="user-short-description-title"><a href="profile-timeline.html">Marina Valentine</a></p>

        <p class="user-short-description-text"><a href="#">www.gamehuntress.com</a></p>
    </div>

    <div class="badge-list small">
        <div class="badge-item">
            <img src="{{ asset("assets/$VIEW_ROOT/img/badge/gold-s.png") }}" alt="badge-gold-s">
        </div>

        <div class="badge-item">
            <img src="{{ asset("assets/$VIEW_ROOT/img/badge/age-s.png") }}" alt="badge-age-s">
        </div>

        <div class="badge-item">
            <img src="{{ asset("assets/$VIEW_ROOT/img/badge/caffeinated-s.png") }}" alt="badge-caffeinated-s">
        </div>

        <div class="badge-item">
            <img src="{{ asset("assets/$VIEW_ROOT/img/badge/warrior-s.png") }}" alt="badge-warrior-s">
        </div>

        <a class="badge-item" href="profile-badges.html">
            <img src="{{ asset("assets/$VIEW_ROOT/img/badge/blank-s.png") }}" alt="badge-blank-s">
            <p class="badge-item-text">+9</p>
        </a>
    </div>

    <div class="user-stats">
        <div class="user-stat">
            <p class="user-stat-title">930</p>

            <p class="user-stat-text">posts</p>
        </div>

        <div class="user-stat">
            <p class="user-stat-title">82</p>

            <p class="user-stat-text">friends</p>
        </div>

        <div class="user-stat">
            <p class="user-stat-title">5.7k</p>

            <p class="user-stat-text">visits</p>
        </div>
    </div>

    <ul class="menu">
        <li class="menu-item">
            <a class="menu-item-link" href="newsfeed.html">
                <svg class="menu-item-link-icon icon-newsfeed">
                    <use xlink:href="#svg-newsfeed"></use>
                </svg>
                Newsfeed
            </a>
        </li>

        <li class="menu-item">
            <a class="menu-item-link" href="overview.html">
                <svg class="menu-item-link-icon icon-overview">
                    <use xlink:href="#svg-overview"></use>
                </svg>
                Overview
            </a>
        </li>

        <li class="menu-item">
            <a class="menu-item-link" href="groups.html">
                <svg class="menu-item-link-icon icon-group">
                    <use xlink:href="#svg-group"></use>
                </svg>
                Groups
            </a>
        </li>

        <li class="menu-item">
            <a class="menu-item-link" href="members.html">
                <svg class="menu-item-link-icon icon-members">
                    <use xlink:href="#svg-members"></use>
                </svg>
                Members
            </a>
        </li>

        <li class="menu-item">
            <a class="menu-item-link" href="badges.html">
                <svg class="menu-item-link-icon icon-badges">
                    <use xlink:href="#svg-badges"></use>
                </svg>
                Badges
            </a>
        </li>

        <li class="menu-item">
            <a class="menu-item-link" href="quests.html">
                <svg class="menu-item-link-icon icon-quests">
                    <use xlink:href="#svg-quests"></use>
                </svg>
                Quests
            </a>
        </li>

        <li class="menu-item">
            <a class="menu-item-link" href="streams.html">
                <svg class="menu-item-link-icon icon-streams">
                    <use xlink:href="#svg-streams"></use>
                </svg>
                Streams
            </a>
        </li>

        <li class="menu-item">
            <a class="menu-item-link" href="events.html">
                <svg class="menu-item-link-icon icon-events">
                    <use xlink:href="#svg-events"></use>
                </svg>
                Events
            </a>
        </li>

        <li class="menu-item">
            <a class="menu-item-link" href="forums.html">
                <svg class="menu-item-link-icon icon-forums">
                    <use xlink:href="#svg-forums"></use>
                </svg>
                Forums
            </a>
        </li>

        <li class="menu-item">
            <a class="menu-item-link" href="marketplace.html">
                <svg class="menu-item-link-icon icon-marketplace">
                    <use xlink:href="#svg-marketplace"></use>
                </svg>
                Marketplace
            </a>
        </li>
    </ul>
</nav>
