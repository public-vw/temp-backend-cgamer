<h2 class="post-open-title" id="payment">پرداخت به کریپتوباز</h2>
<p class="post-open-paragraph">
    این سایت بستری برای تبادل دارایی‌های دیجیتال مانند بیت‌کوین، اس.ال.پی، و اتریوم با تومان می‌باشد.
    هیچ گونه تبادل ارزی اعم از خرید و فروش دلار یا سایر ارزهای کاغذی، در این بستر صورت نمی گیرد.
</p>

<p class="post-open-paragraph">
    توجه داشته باشید که شما فقط میتوانید از کارت هایی که در پنل کاربری خود ثبت کرده اید و تایید شده اند که به نام خودتان
    میباشد اقدام به افزایش اعتبار آنلاین کنید .
</p>
<p class="post-open-paragraph">
    کاربران کریپتوباز متعهد هستند که از خدمات سایت کریپتوباز تنها برای خود استفاده نموده و مسئولیت استفاده از خدمات کریپتوباز با اکانت کاربر برای فرد غیر که فرآیند احراز هویت را طی نکرده باشد، به عهده خود کاربر خواهد بود.
</p>
<p class="post-open-paragraph">
    شماره حساب اعلام شده به سایت و همچنین آدرس کیف پول ها جهت برداشت رمزارز نیز می بایست متعلق به کاربر بوده و کاربران مجاز به دادن آدرس کیف پول متعلق به اشخاص دیگر نیستند.
</p>
<p class="post-open-paragraph">
    کریپتوباز این اطمینان را می‌دهد که واریزی‌های کاربران را نزد خود به امانت و به بهترین شکل و با بالاترین استانداردهای امنیتی ممکن، حفظ نماید تا براساس خدماتی که کاربر درخواست کرده است تسویه شود. در صورت بروز هرگونه مشکل امنیتی، کریپتوباز متعهد به جبران خسارت خواهد بود.
</p>
<p class="post-open-paragraph">
    در صورت تمایل برای برداشت ارزهای دیجیتال، مسئولیت ارائه‌ی آدرس صحیح کیف پول به عهده‌ی کاربر خواهد بود. در صورت بروز هر گونه مشکل اعم از اشتباه در ورود آدرس صحیح، نقص آدرس، مشکلات کیف پول مقصد و بلوکه شدن دارایی‌های کاربران در کیف پول مقصد، هیچ گونه مسئولیتی به عهده‌ی کریپتوباز نخواهد بود.
</p>
<p class="post-open-paragraph">
    کریپتوباز در مقابل واریز توکن یا کوین بر بستر اشتباه یا کوین هایی که در کریپتوباز پشتیبانی نمی شود هیچ گونه مسئولیتی نداشته و با توجه به مکانیسم ذخیره سازی سرد امکان استخراج این توکن ها با استفاده از ولت ثالث وجود ندارد. لذا مسئولیت هرگونه واریز اشتباه با خود کاربر بوده و کاربر حق هیچ گونه شکایتی را نخواهد داشت.
</p>
<p class="post-open-paragraph">
    درخواست برداشت ریال از حساب کاربری در سریع‌ترین زمان ممکن پس از ثبت، بررسی خواهد شد. زمان واریز پول به حساب کاربران بر اساس محدودیت‌های انتقال وجه بین بانکی، متفاوت خواهد بود. برای اطلاعات بیش‌تر، به قوانین انتقال وجه بین بانکی ( پایا ، ساتنا) مراجعه فرمایید.
</p>
<p class="post-open-paragraph">
    هرچند تیم کریپتوباز تمام تلاش خود را می کند تا با بالاترین کیفیت و سریع‌ترین روش‌ها اقدام به تحویل سرویس درخواستی نماید، کاربر می‌پذیرد که با توجه به نوسانات قیمت ارز، مشکلات انتقال پول و تغییرات لحظه‌ای ارزهای دیجیتال، امکان تغییر در پرداختی لحظه آخر وجود دارد.
</p>
<p class="post-open-paragraph">
    اگر کریپتوباز تحت هر عنوان اشتباهاً یا من غیر حق، وجوه یا رمزارزی را به حساب کاربر منظور یا در محاسبات خود هر نوع اشتباهی نماید، هر زمان مجاز و مختار است راساً و مستقلاً و بدون انجام هیچ گونه تشریفات اداری و قضائی و دریافت اجازه کتبی از متعهد (صاحب حساب) در رفع اشتباه و برداشت از حساب‌های وی اقدام نماید و تشخیص کریپتوباز نسبت به وقوع اشتباه یا پرداخت بدون حق و لزوم برداشت از حساب معتبر خواهد بود و کاربر حق هرگونه اعتراض و ادعایی را در خصوص نحوه عملکرد کریپتوباز از هر جهت از خود ساقط می نماید.
</p>
<p class="post-open-paragraph">
    کاربران کریپتوباز می‌پذیرند در صورت تشخیص کارشناسان پشتیانی کریپتوباز، جهت حفظ امنیت دارایی حساب کاربریشان با اطلاع قبلی نسبت به برقراری تماس تصویری با تیم کریپتوباز همکاری نمایند.
    {{--        همچنین بنا به دستور نهادهای قضایی مانند پلیس فتا، ممکن است برداشت رمزارز به مدت 24 تا 72 ساعت کاری پس از زمان واریز از طریق درگاه شتابی، محدود گردد.--}}
</p>
<p class="post-open-paragraph">
    کریپتوباز به عنوان بازار آنلاین تبادل ارزهای دیجیتال، هیچ گونه مسئولیتی در قبال نحوه‌ی معاملات کاربران و سود و زیان حاصل از آن ندارد.
</p>
<p class="post-open-paragraph">
    در صورت بروز هرگونه مشکل یا ابهام در هر یک از معاملات، کریپتوباز حق دارد مستقلاً آن معامله را ابطال و دارایی‌های هر یک از طرفین را به حساب خودشان عودت دهد. بدیهی است که در صورتی که اشکال از سمت کریپتوباز باشد، موظف خواهد بود که جبران خسارت نماید و در غیراین صورت کاربر حق هرگونه اعتراض و ادعایی را در خصوص نحوه عملکرد کریپتوباز از هر جهت از خود ساقط می‌نماید.
</p>
<p class="post-open-paragraph">
    کریپتوباز هیچگونه مسئولیتی در قبال منبع پول های واریزی نداشته و تماما مسئولیت بر عهده شخص واریز کننده میباشد .
</p>
<p class="post-open-paragraph">
    افزایش اعتبار به صورت آنی بوده و روزانه از هر کارت بانکی امکان افزایش تا سقف ۵۰ میلیون تومان و دو کارت بانکی متفاوت
    نهایتا 100 میلیون تومان فراهم میباشد .
</p>
<p class="post-open-paragraph">
    برای افزایش بیشتر از 100 میلیون تومان در روز میتوانید از گزینه واریز بانکی و مراجعه به بانک و یا از طریق اینترنت
    بانک خود اقدام نمایید.
</p>
