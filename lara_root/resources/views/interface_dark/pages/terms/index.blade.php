@extends("$VIEW_ROOT.layouts.base",[
        'FILE_NAME' => 'profile-post',
        'PAGE_TITLE' => 'Terms & Conditions',
        'HEADER_CLASS' => 'article-header',
    ])

@php
    $files = scandir(base_path("resources/views/$VIEW_ROOT/pages/terms/content/"));
    foreach ($files as $i => &$file) {
        if(preg_match('/^\d{1,2}/', $file) !== 1)unset($files[$i]);
        $file = substr($file, 0, -1 * strlen('.blade.php'));
    }

    $update = \Carbon\Carbon::createFromFormat('d/m/Y',  '19/08/2021');
@endphp

@section('page_title','قوانین سایت')
@section('page_description','قوانین جامعه کریپتوباز را در این صفحه مطالعه کن')

@push('css')
    <link rel="stylesheet" href="{{ asset("assets/$VIEW_ROOT/pages/css/terms.css") }}">
@endpush

@section('content')
<div class="content-grid">
    @include("$VIEW_ROOT.pages.terms._banner")

    <div class="grid grid-3-9 medium-space">

        @include("$VIEW_ROOT.pages.terms._sidebar")

        <div class="account-hub-content">

            <div class="grid-column">
                <div class="widget-box">
                    @foreach($files as $file)
                        @include("{$VIEW_ROOT}.pages.terms.content.{$file}")
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

