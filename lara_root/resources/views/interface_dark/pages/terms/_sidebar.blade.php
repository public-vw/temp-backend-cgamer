<div class="account-hub-sidebar">
    <div class="sidebar-box no-padding">
        <div class="sidebar-menu">
            <div class="sidebar-menu-item">
                <div class="sidebar-menu-body accordion-content-linked accordion-open">
                    <a class="sidebar-menu-link active" href="#important-point">تذکر مهم</a>

                    <a class="sidebar-menu-link" href="#responsibilities">تعهدات فعالیت در کریپتوباز</a>

{{--                    <a class="sidebar-menu-link" href="#payment">نحوه پرداخت به کریپتوباز</a>--}}
                </div>
            </div>

        </div>

    </div>
</div>
