<nav id="navigation-widget-small" class="navigation-widget navigation-widget-desktop closed sidebar left delayed">
    <a class="user-avatar small no-outline online" href="profile-timeline.html">
        <div class="user-avatar-content">
            <div class="hexagon-image-30-32" data-src="{{ asset("assets/$VIEW_ROOT/img/avatar/01.jpg") }}"></div>
        </div>

        <div class="user-avatar-progress">
            <div class="hexagon-progress-40-44"></div>
        </div>

        <div class="user-avatar-progress-border">
            <div class="hexagon-border-40-44"></div>
        </div>

        <div class="user-avatar-badge">
            <div class="user-avatar-badge-border">
                <div class="hexagon-22-24"></div>
            </div>

            <div class="user-avatar-badge-content">
                <div class="hexagon-dark-16-18"></div>
            </div>

            <p class="user-avatar-badge-text">24</p>
        </div>
    </a>

    <ul class="menu small">
        <li class="menu-item">
            <a class="menu-item-link text-tooltip-tfr" href="newsfeed.html" data-title="Newsfeed">
                <svg class="menu-item-link-icon icon-newsfeed">
                    <use xlink:href="#svg-newsfeed"></use>
                </svg>
            </a>
        </li>

        <li class="menu-item">
            <a class="menu-item-link text-tooltip-tfr" href="overview.html" data-title="Overview">
                <svg class="menu-item-link-icon icon-overview">
                    <use xlink:href="#svg-overview"></use>
                </svg>
            </a>
        </li>

        <li class="menu-item">
            <a class="menu-item-link text-tooltip-tfr" href="groups.html" data-title="Groups">
                <svg class="menu-item-link-icon icon-group">
                    <use xlink:href="#svg-group"></use>
                </svg>
            </a>
        </li>

        <li class="menu-item">
            <a class="menu-item-link text-tooltip-tfr" href="members.html" data-title="Members">
                <svg class="menu-item-link-icon icon-members">
                    <use xlink:href="#svg-members"></use>
                </svg>
            </a>
        </li>

        <li class="menu-item">
            <a class="menu-item-link text-tooltip-tfr" href="badges.html" data-title="Badges">
                <svg class="menu-item-link-icon icon-badges">
                    <use xlink:href="#svg-badges"></use>
                </svg>
            </a>
        </li>

        <li class="menu-item">
            <a class="menu-item-link text-tooltip-tfr" href="quests.html" data-title="Quests">
                <svg class="menu-item-link-icon icon-quests">
                    <use xlink:href="#svg-quests"></use>
                </svg>
            </a>
        </li>

        <li class="menu-item">
            <a class="menu-item-link text-tooltip-tfr" href="streams.html" data-title="Streams">
                <svg class="menu-item-link-icon icon-streams">
                    <use xlink:href="#svg-streams"></use>
                </svg>
            </a>
        </li>

        <li class="menu-item">
            <a class="menu-item-link text-tooltip-tfr" href="events.html" data-title="Events">
                <svg class="menu-item-link-icon icon-events">
                    <use xlink:href="#svg-events"></use>
                </svg>
            </a>
        </li>

        <li class="menu-item">
            <a class="menu-item-link text-tooltip-tfr" href="forums.html" data-title="Forums">
                <svg class="menu-item-link-icon icon-forums">
                    <use xlink:href="#svg-forums"></use>
                </svg>
            </a>
        </li>

        <li class="menu-item">
            <a class="menu-item-link text-tooltip-tfr" href="marketplace.html" data-title="Marketplace">
                <svg class="menu-item-link-icon icon-marketplace">
                    <use xlink:href="#svg-marketplace"></use>
                </svg>
            </a>
        </li>
    </ul>
</nav>
