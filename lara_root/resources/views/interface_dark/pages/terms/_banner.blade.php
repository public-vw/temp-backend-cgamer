<div class="section-banner">
    <img class="section-banner-icon terms" src="{{ asset("assets/$VIEW_ROOT/img/banner/shield2.png") }}" alt="terms" width="149px" height="177px">

    <h1 class="section-banner-title">قوانین جامعه بلاک‌چین کریپتوباز</h1>

    <p class="section-banner-text"><strong>آخرین بروزرسانی: </strong>{{ $update->diffForHumans() }}</span> @ {{$update->format('M Y')}}</p>
</div>
