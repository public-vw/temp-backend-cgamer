<div class="widget-box">
    {{--    @include("$$VIEW_ROOT.pages.profile._widget_settings")--}}

    <p class="widget-box-title">Personal Info</p>

    <div class="widget-box-content">
        <div class="information-line-list">
            <div class="information-line">
                <p class="information-line-title">Email</p>
                <p class="information-line-text" contenteditable="true">ghuntress@yourmail.com</p>
            </div>

            <div class="information-line">
                <p class="information-line-title">Birthday</p>
                <p class="information-line-text" contenteditable="true">August 24th, 1987</p>
            </div>

            <div class="information-line">
                <p class="information-line-title">Xb ID</p>
                <p class="information-line-text" contenteditable="true">GameHuntress89</p>
            </div>
        </div>
    </div>
</div>
