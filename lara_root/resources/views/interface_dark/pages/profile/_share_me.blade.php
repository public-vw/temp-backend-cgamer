<div class="widget-box">
    {{--    @include("$$VIEW_ROOT.pages.profile._widget_settings")--}}

    <h2 class="widget-box-title">حمایت از نویسنده</h2>
    <p class="widget-box-description">برای حمایت از این نویسنده، پروفایل اون رو <b>بازنشر کن</b></p>

    <div class="widget-box-content">
        <div class="information-line-list">
            <div class="social-links">

                {!! $shareComponent !!}


            </div>
        </div>
    </div>
</div>
