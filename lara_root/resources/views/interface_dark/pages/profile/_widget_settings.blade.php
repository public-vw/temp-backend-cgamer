<div class="widget-box-settings">
    <div class="post-settings-wrap">
        <div class="post-settings widget-box-post-settings-dropdown-trigger">
            <svg class="post-settings-icon icon-more-dots">
                <use xlink:href="#svg-more-dots"></use>
            </svg>
        </div>

        <div class="simple-dropdown widget-box-post-settings-dropdown">
            <p class="simple-dropdown-link">Widget Settings</p>
        </div>
    </div>
</div>
