<div class="account-hub-sidebar">
    <div class="sidebar-box">
        <div class="progress-arc-summary">
            <div class="progress-arc-wrap">
                <div class="progress-arc">
                </div>

                <div class="progress-arc-info">
                    @include("$VIEW_ROOT.pages.profile._avatar")
                </div>
            </div>

            <div class="progress-arc-summary-info">
                <p class="progress-arc-summary-title">{{ $user->name }}</p>

                <h1 class="progress-arc-summary-subtitle ltr">{{ '@'.$user->nickname }}</h1>

            </div>


        </div>

        {{--    @include("$$VIEW_ROOT.pages.profile._sidebar_status")--}}

        @auth
            @if($user->id == auth()->user()->id)
            <div class="sidebar-box-footer">
                <a class="button primary" href="{{ route('client.profile.index') }}">ویرایش پروفایل</a>
            </div>
            @elseif(auth()->user()->isFollowing($user))
                <div class="sidebar-box-footer">
                    <a class="button" href="{{ route('client.following.unfollow',[$user]) }}">آنفالو</a>
                </div>
            @else
                <div class="sidebar-box-footer">
                    <a class="button primary" href="{{ route('client.following.follow',[$user]) }}">میخواهم فالو کنم</a>
                </div>
            @endif
        @else
            <div class="sidebar-box-footer">
                <a class="button primary popup-reglog-trigger" href="javascript:;">میخواهم فالو کنم</a>
            </div>
        @endauth

{{--
        <div class="sidebar-box-footer">
            <p class="button primary">Follow</p>
            <p class="button white small-space">Unfollow</p>
        </div>
--}}
    </div>
</div>



