<div class="widget-box">
    {{--                @include("$$VIEW_ROOT.pages.profile._widget_settings")--}}

    <p class="widget-box-title">More Stats</p>

    <div class="widget-box-content">
        <div class="stat-block-list">
            <div class="stat-block">
                <div class="stat-block-decoration">
                    <svg class="stat-block-decoration-icon icon-friend">
                        <use xlink:href="#svg-friend"></use>
                    </svg>
                </div>

                <div class="stat-block-info">
                    <p class="stat-block-title">Last friend added</p>

                    <p class="stat-block-text">5 Days Ago</p>
                </div>
            </div>

            <div class="stat-block">
                <div class="stat-block-decoration">
                    <svg class="stat-block-decoration-icon icon-status">
                        <use xlink:href="#svg-status"></use>
                    </svg>
                </div>

                <div class="stat-block-info">
                    <p class="stat-block-title">Last post update</p>

                    <p class="stat-block-text">1 Day Ago</p>
                </div>
            </div>

            <div class="stat-block">
                <div class="stat-block-decoration">
                    <svg class="stat-block-decoration-icon icon-comment">
                        <use xlink:href="#svg-comment"></use>
                    </svg>
                </div>

                <div class="stat-block-info">
                    <p class="stat-block-title">Most commented post</p>

                    <p class="stat-block-text">56 Comments</p>
                </div>
            </div>

            <div class="stat-block">
                <div class="stat-block-decoration">
                    <svg class="stat-block-decoration-icon icon-thumbs-up">
                        <use xlink:href="#svg-thumbs-up"></use>
                    </svg>
                </div>

                <div class="stat-block-info">
                    <p class="stat-block-title">Most liked post</p>

                    <p class="stat-block-text">904 Likes</p>
                </div>
            </div>
        </div>
    </div>
</div>
