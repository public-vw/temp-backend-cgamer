<div class="widget-box">

{{--    @include("$$VIEW_ROOT.pages.profile._widget_settings")--}}

    <h2 class="widget-box-title">درباره من</h2>

    <div class="widget-box-content">
        @isset($user->meta->about_me)
            <p class="paragraph">{{ $user->meta->about_me }}</p>
        @endisset

        <div class="information-line-list">
            <div class="information-line">
                <p class="information-line-title">شروع عضویت</p>
                <p class="information-line-text">{{ $user->created_at->diffForHumans() }}</p>
            </div>

        </div>
    </div>
</div>
