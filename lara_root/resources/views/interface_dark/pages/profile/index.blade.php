@extends("$VIEW_ROOT.layouts.base",[
        'FILE_NAME' => 'marketplace-product',
        'PAGE_TITLE' => 'Profile',
    ])

@section('page_title','پروفایل نویسنده'.' | '.strtoupper($user->nickname))

@push('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
@endpush

@push('js')
{{--    <script src="{{ asset('js/share.js') }}"></script>--}}
@endpush

@section('content')

<div class="content-grid full">
{{--        @include("$VIEW_ROOT.pages.profile._header")--}}
    {{--    @include("$VIEW_ROOT.pages.profile._navigation_bar") --}}

    <div class="grid grid-3-9">
        <div class="grid-column">
            @include("$VIEW_ROOT.pages.profile._sidebar")

            {{--            @include("$VIEW_ROOT.pages.profile._more_stats")--}}
        </div>
        <div class="grid-column">
            @include("$VIEW_ROOT.pages.profile._about_me")

            @includeWhen($user->author,"$VIEW_ROOT.pages.profile._share_me")
        </div>

{{--        <div class="grid-column">--}}
{{--            @include("$VIEW_ROOT.pages.profile._interests")--}}
{{--            @include("$VIEW_ROOT.pages.profile._jobs")--}}
{{--        </div>--}}

    </div>
    @role('author_supervisor@web|admin@web')
        @includeWhen($user->author->inctiveArticles()->count(),"$VIEW_ROOT.clients.profile._last_inactive_posts")
    @endrole
    @includeWhen($user->author->latestActiveArticles()->count(),"$VIEW_ROOT.pages.profile._last_posts")
</div>

@endsection
