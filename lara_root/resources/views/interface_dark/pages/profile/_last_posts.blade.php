<section class="section">
    <div class="section-header">
        <div class="section-header-info">
            <h2 class="section-title">چند مقاله آخر این نویسنده</h2>
        </div>
    </div>
    <div class="grid grid-3-3-3-3 centered-on-mobile">
        @foreach($user->author->latestActiveArticles() as $article)
            @include("$VIEW_ROOT.pages.profile.posts.base",['ARTICLE' => $article])
        @endforeach
    </div>
</section>
