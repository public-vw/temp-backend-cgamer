<div class="post-preview" onclick="window.location='{{ $subcategory->seoUrl() }}'">

    @include("$VIEW_ROOT.pages.article_category.sub_category._image",[
        'IMAGE' => $subcategory->lastImage('article_category_header') ?? asset("assets/$VIEW_ROOT/img/cover/10.jpg"),
        'ALT' => $subcategory->seo()['google']->title ?? $subcategory->heading
    ])
    @role('admin@web')
        <div class="admin-buttons">
            <a href="{{ route('admin.article_categories.edit',['article_category' => $subcategory]) }}" class="button float-left px-3" target="_blank">ویرایش در پنل</a>
        </div>
    @endrole

    <div class="post-preview-info fixed-height">
        @include("$VIEW_ROOT.pages.article_category.sub_category._header",[
            'LAST_UPDATE' => $subcategory->updated_at->diffForHumans(),
            'TITLE' => $subcategory->heading,
        ])

        @include("$VIEW_ROOT.pages.article_category.sub_category._body",[
            'CONTENT' => $subcategory->summary,
            'LINK' => $subcategory->seoUrl(),
        ])
    </div>

    <div class="content-actions">
{{--        @include("$VIEW_ROOT.pages.article_category.sub_category._react")--}}
{{--        @include("$VIEW_ROOT.pages.article_category.sub_category._state")--}}
    </div>
</div>
