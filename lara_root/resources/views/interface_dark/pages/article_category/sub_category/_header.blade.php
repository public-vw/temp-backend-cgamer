<div class="post-preview-info-top">
    <p class="post-preview-timestamp">آخرین بروزرسانی {{ $LAST_UPDATE }}</p>

    <h3 class="post-preview-title">{{ $TITLE }}</h3>
</div>
