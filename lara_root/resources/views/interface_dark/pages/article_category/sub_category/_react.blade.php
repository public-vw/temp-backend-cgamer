<div class="content-action">
    <div class="meta-line">
        <div class="meta-line-list reaction-item-list">
            <div class="reaction-item">
                <img class="reaction-image reaction-item-dropdown-trigger"
                     src="{{ asset("assets/$VIEW_ROOT/img/reaction/happy.png") }}" alt="reaction-happy">

                <div class="simple-dropdown padded reaction-item-dropdown">
                    <p class="simple-dropdown-text"><img class="reaction"
                                                         src="{{ asset("assets/$VIEW_ROOT/img/reaction/happy.png") }}"
                                                         alt="reaction-happy"> <span class="bold">Happy</span>
                    </p>

                    <p class="simple-dropdown-text">Neko Bebop</p>

                    <p class="simple-dropdown-text">Nick Grissom</p>

                    <p class="simple-dropdown-text">Sarah Diamond</p>

                    <p class="simple-dropdown-text">Marcus Jhonson</p>
                </div>
            </div>

            <div class="reaction-item">
                <img class="reaction-image reaction-item-dropdown-trigger"
                     src="{{ asset("assets/$VIEW_ROOT/img/reaction/love.png") }}" alt="reaction-love">

                <div class="simple-dropdown padded reaction-item-dropdown">
                    <p class="simple-dropdown-text"><img class="reaction"
                                                         src="{{ asset("assets/$VIEW_ROOT/img/reaction/love.png") }}"
                                                         alt="reaction-love"> <span class="bold">Love</span>
                    </p>

                    <p class="simple-dropdown-text">Jett Spiegel</p>

                    <p class="simple-dropdown-text">Jane Rodgers</p>
                </div>
            </div>

            <div class="reaction-item">
                <img class="reaction-image reaction-item-dropdown-trigger"
                     src="{{ asset("assets/$VIEW_ROOT/img/reaction/funny.png") }}" alt="reaction-funny">

                <div class="simple-dropdown padded reaction-item-dropdown">
                    <p class="simple-dropdown-text"><img class="reaction"
                                                         src="{{ asset("assets/$VIEW_ROOT/img/reaction/funny.png") }}"
                                                         alt="reaction-funny"> <span class="bold">Funny</span>
                    </p>

                    <p class="simple-dropdown-text">Neko Bebop</p>

                    <p class="simple-dropdown-text">Nick Grissom</p>

                    <p class="simple-dropdown-text">Sarah Diamond</p>

                    <p class="simple-dropdown-text">Jett Spiegel</p>

                    <p class="simple-dropdown-text">Marcus Jhonson</p>

                    <p class="simple-dropdown-text">Jane Rodgers</p>

                    <p class="simple-dropdown-text"><span class="bold">and 10 more...</span></p>
                </div>
            </div>
        </div>

        <p class="meta-line-text">21</p>
    </div>
</div>
