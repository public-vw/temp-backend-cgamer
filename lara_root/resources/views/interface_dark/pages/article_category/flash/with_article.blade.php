@auth
    <a class="product-category-box category-featured" href="{{ route('client.articles.single.new') }}">
@else
    <a class="product-category-box category-featured popup-reglog-trigger" href="javascript:;">
@endauth
    <p class="product-category-box-title">برای ثبت</p>

    <p class="product-category-box-text">نوشته جدید خودت</p>

    <p class="product-category-box-tag">کلیک کن</p>
</a>
