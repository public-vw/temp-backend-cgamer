@auth
    <a class="product-category-box " href="{{ route('client.articles.single.new') }}">
@else
    <a class="product-category-box  popup-reglog-trigger" href="javascript:;">
@endauth
        <div class="section-banner more-padding hoverable">
            <img class="section-banner-icon newarticle" src="{{ asset("assets/$VIEW_ROOT/img/article/Chubby_Bull.png") }}" alt="new_article" width="200px">

            <p class="section-banner-title text-black">اولین نوشته این دسته‌بندی رو خودت ثبت کن!</p>

            <p class="section-banner-text text-black">تمام نوشته‌های جامعه کریپتوباز توسط کاربرا و تجربیاتشون ایجاد می‌شن</p>
        </div>
    </a>
