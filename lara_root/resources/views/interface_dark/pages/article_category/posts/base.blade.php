<div class="event-preview">
    @include("$VIEW_ROOT.pages.article_category.posts._image",[
        'IMAGE' => $ARTICLE->lastImage('article_header') ?? $ARTICLE->category->lastImage('article_default_header') ?? asset("assets/$VIEW_ROOT/img/cover/19.jpg"),
        'ALT' => $ARTICLE->seo()['google']->title ?? $ARTICLE->heading
    ])
    @role('admin@web')
        <div class="admin-buttons">
            <a href="{{ route('admin.articles.edit',['article' => $ARTICLE]) }}" class="button float-left px-3" target="_blank">ویرایش در پنل</a>
        </div>
    @endrole

    <div class="event-preview-info">
        <div class="event-preview-info-top">
{{--            @include("$VIEW_ROOT.pages.article_category.posts._date")--}}

            @include("$VIEW_ROOT.pages.article_category.posts._heading",[
                        'HEADING' => $ARTICLE->heading,
                        'LINK' => $ARTICLE->seoUrl(),
                        ])

            <p class="post-open-paragraph">{!! $ARTICLE->summary ?? '' !!}</p>
        </div>

        <div class="event-preview-info-bottom">

{{--            @include("$VIEW_ROOT.pages.article_category.posts._text_icon")--}}

{{--            @include("$VIEW_ROOT.pages.article_category.posts._react")--}}

            <a class="button white white-tertiary" href="{{ $ARTICLE->seoUrl() }}">بیشتر بدانید ...</a>
        </div>
    </div>
</div>
