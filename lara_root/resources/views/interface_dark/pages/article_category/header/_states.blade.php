<div class="user-stats">
    <div class="user-stat big">
        <p class="user-stat-title">930</p>

        <p class="user-stat-text">posts</p>
    </div>

    <div class="user-stat big">
        <p class="user-stat-title">82</p>

        <p class="user-stat-text">friends</p>
    </div>

    <div class="user-stat big">
        <p class="user-stat-title">5.7k</p>

        <p class="user-stat-text">visits</p>
    </div>

    <div class="user-stat big">
        <img class="user-stat-image" src="{{ asset("assets/$VIEW_ROOT/img/flag/usa.png") }}" alt="flag-usa">

        <p class="user-stat-text">usa</p>
    </div>
</div>
