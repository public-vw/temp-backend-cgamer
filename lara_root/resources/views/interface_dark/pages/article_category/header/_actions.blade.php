<div class="profile-header-info-actions">
    <p class="profile-header-info-action button secondary"><span class="hide-text-mobile">Add</span> Friend
        +</p>

    <p class="profile-header-info-action button primary"><span class="hide-text-mobile">Send</span> Message
    </p>
</div>
