<div class="profile-header category-header">
    @include("$VIEW_ROOT.pages.article_category.header._image",[
        'IMAGE' => $category->lastImage('article_category_header') ?? asset("assets/$VIEW_ROOT/img/cover/01.jpg"),
        'ALT' => $category->seo()['google']->title ?? $category->heading
    ])

    <div class="profile-header-info category-header-info">
{{--        @include("$VIEW_ROOT.pages.article_category.header._avatar")--}}

{{--        @include("$VIEW_ROOT.pages.article_category.header._socials")--}}

{{--        @include("$VIEW_ROOT.pages.article_category.header._states")--}}

{{--        @include("$VIEW_ROOT.pages.article_category.header._actions")--}}

        <h1 class="category-title">{{ $category->heading }}
        @role('admin@web')
            <a href="{{ route('admin.article_categories.inface.edit',['article_category' => $category]) }}" class="button float-left px-3">ویرایش</a>
            <a href="{{ route('admin.article_categories.edit',['article_category' => $category]) }}" class="button float-left px-3" target="_blank">ویرایش در پنل</a>
            @if($category->parent)
                <a href="{{ $category->parent->seoUrl() }}" class="button float-left px-3">شاخه قبلی</a>
            @endif
        @endrole
        </h1>

    </div>
</div>
