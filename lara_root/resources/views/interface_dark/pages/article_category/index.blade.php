@extends("$VIEW_ROOT.layouts.base",[
        'FILE_NAME' => 'profile-blog',
        'PAGE_TITLE' => 'Article Category',
        'HEADER_CLASS' => 'category-header',
    ])

@section('canonical')
    <link rel="canonical" href="{{ url()->current() }}/">
@endsection

@section('page_title')
{{ $category->seo()['google']->title ?? $category->heading }}
@endsection

@section('page_keywords')
{{ $category->seo()['google']->keywords ?? '' }}
@endsection

@section('page_description')
{{ $category->seo()['google']->description ?? $category->heading }}
@endsection

@section('page_image'){{
    $category->lastImage('article_category_header')
    ?? asset("assets/$VIEW_ROOT/img/cover/01.jpg")
}}@endsection

@push('css')
    <link rel="stylesheet" href="{{ asset("assets/$VIEW_ROOT/pages/css/article-category.css") }}">
@endpush

@section('content')

    {{--    TODO: sass._grid:408 enable padding in full mode, if you want these --}}
    {{--    TODO: sass._post-open:125 enable smaller width, if you want these --}}
    {{--    @include("$VIEW_ROOT.includes.nav_widget_small")--}}


    {{--    TODO: These are good to show author properties--}}
    {{--    TODO: sass._post-open:125 enable smaller width, if you want these --}}
    {{--    @include("$VIEW_ROOT.includes.nav_widget")--}}
    {{--    @include("$VIEW_ROOT.includes.nav_widget_mobile")--}}

    <div class="content-grid">
        @include("$VIEW_ROOT.pages.article_category.header.base")

        <section class="section">
            <p class="category-description">{!! $category->description !!}</p>

{{--            @include("$VIEW_ROOT.pages.article_category._filter")--}}

            @if($category->activeArticles()->count())
                @include("$VIEW_ROOT.pages.article_category._section_heading",['TITLE'=>'آخرین نوشته‌ها'])
                <div class="grid grid-3-3-3-3 centered-on-mobile">
                    @foreach($category->activeArticles() as $article)
                        @include("$VIEW_ROOT.pages.article_category.posts.base",['ARTICLE' => $article])
                    @endforeach

                    @includeWhen(Route::has('auth.register'),"$VIEW_ROOT.pages.article_category.flash.with_article")
                </div>
            @else
                @includeWhen(Route::has('auth.register'),"$VIEW_ROOT.pages.article_category.flash.without_article")
            @endif

        </section>

        <section class="section">
        @if($category->hasChild())
            @include("$VIEW_ROOT.pages.article_category._section_heading",['TITLE' => 'دسته‌بندی‌های زیرمجموعه این دسته‌بندی'])
            <div class="grid grid-4-4-4 centered-on-mobile">
                @foreach($category->activeChildren as $subcategory)
                    @include("$VIEW_ROOT.pages.article_category.sub_category.base")
                @endforeach
            </div>
        @endif
        </section>

    </div>

@endsection
