<div class="content-grid medium">
    <div class="section-header medium">
        <div class="section-header-info">
            <p class="section-pretitle">Browse Other</p>

            <h2 class="section-title">Related Articles</h2>
        </div>
    </div>

    <div class="grid grid-4-4 centered">
        <div class="post-preview">
            <figure class="post-preview-image liquid">
                <img src="{{ asset("assets/$VIEW_ROOT/img/cover/10.jpg") }}" alt="cover-10">
            </figure>

            <div class="post-preview-info fixed-height">
                <div class="post-preview-info-top">
                    <p class="post-preview-timestamp">3 weeks ago</p>

                    <p class="post-preview-title">I spoke with the developers of RoBot SaMurai II at the 2019
                        GamingCon</p>
                </div>

                <div class="post-preview-info-bottom">
                    <p class="post-preview-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                        exercitation ullamco laboris nisi ut...</p>

                    <a class="post-preview-link" href="profile-post.html">Read more...</a>
                </div>
            </div>

            <div class="content-actions">
                <div class="content-action">
                    <div class="meta-line">
                        <div class="meta-line-list reaction-item-list">
                            <div class="reaction-item">
                                <img class="reaction-image reaction-item-dropdown-trigger" src="{{ asset("assets/$VIEW_ROOT/img/reaction/happy.png") }}"
                                     alt="reaction-happy">

                                <div class="simple-dropdown padded reaction-item-dropdown">
                                    <p class="simple-dropdown-text"><img class="reaction" src="{{ asset("assets/$VIEW_ROOT/img/reaction/happy.png") }}"
                                                                         alt="reaction-happy"> <span
                                            class="bold">Happy</span></p>

                                    <p class="simple-dropdown-text">Neko Bebop</p>

                                    <p class="simple-dropdown-text">Nick Grissom</p>

                                    <p class="simple-dropdown-text">Sarah Diamond</p>

                                    <p class="simple-dropdown-text">Marcus Jhonson</p>
                                </div>
                            </div>

                            <div class="reaction-item">
                                <img class="reaction-image reaction-item-dropdown-trigger" src="{{ asset("assets/$VIEW_ROOT/img/reaction/love.png") }}"
                                     alt="reaction-love">

                                <div class="simple-dropdown padded reaction-item-dropdown">
                                    <p class="simple-dropdown-text"><img class="reaction" src="{{ asset("assets/$VIEW_ROOT/img/reaction/love.png") }}"
                                                                         alt="reaction-love"> <span
                                            class="bold">Love</span></p>

                                    <p class="simple-dropdown-text">Jett Spiegel</p>

                                    <p class="simple-dropdown-text">Jane Rodgers</p>
                                </div>
                            </div>

                            <div class="reaction-item">
                                <img class="reaction-image reaction-item-dropdown-trigger" src="{{ asset("assets/$VIEW_ROOT/img/reaction/funny.png") }}"
                                     alt="reaction-funny">

                                <div class="simple-dropdown padded reaction-item-dropdown">
                                    <p class="simple-dropdown-text"><img class="reaction" src="{{ asset("assets/$VIEW_ROOT/img/reaction/funny.png") }}"
                                                                         alt="reaction-funny"> <span
                                            class="bold">Funny</span></p>

                                    <p class="simple-dropdown-text">Neko Bebop</p>

                                    <p class="simple-dropdown-text">Nick Grissom</p>

                                    <p class="simple-dropdown-text">Sarah Diamond</p>

                                    <p class="simple-dropdown-text">Jett Spiegel</p>

                                    <p class="simple-dropdown-text">Marcus Jhonson</p>

                                    <p class="simple-dropdown-text">Jane Rodgers</p>

                                    <p class="simple-dropdown-text"><span class="bold">and 10 more...</span></p>
                                </div>
                            </div>
                        </div>

                        <p class="meta-line-text">21</p>
                    </div>
                </div>

                <div class="content-action">
                    <div class="meta-line">
                        <a class="meta-line-link" href="profile-post.html#comments">8 Comments</a>
                    </div>

                    <div class="meta-line">
                        <p class="meta-line-text">0 Shares</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="post-preview">
            <figure class="post-preview-image liquid">
                <img src="{{ asset("assets/$VIEW_ROOT/img/cover/20.jpg") }}" alt="cover-20">
            </figure>

            <div class="post-preview-info fixed-height">
                <div class="post-preview-info-top">
                    <p class="post-preview-timestamp">4 weeks ago</p>

                    <p class="post-preview-title">I will be at this month's StreamCon with NekoBebop</p>
                </div>

                <div class="post-preview-info-bottom">
                    <p class="post-preview-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                        exercitation ullamco laboris nisi ut...</p>

                    <a class="post-preview-link" href="profile-post.html">Read more...</a>
                </div>
            </div>

            <div class="content-actions">
                <div class="content-action">
                    <div class="meta-line">
                        <div class="meta-line-list reaction-item-list">
                            <div class="reaction-item">
                                <img class="reaction-image reaction-item-dropdown-trigger" src="{{ asset("assets/$VIEW_ROOT/img/reaction/happy.png") }}"
                                     alt="reaction-happy">

                                <div class="simple-dropdown padded reaction-item-dropdown">
                                    <p class="simple-dropdown-text"><img class="reaction" src="{{ asset("assets/$VIEW_ROOT/img/reaction/happy.png") }}"
                                                                         alt="reaction-happy"> <span
                                            class="bold">Happy</span></p>

                                    <p class="simple-dropdown-text">Neko Bebop</p>

                                    <p class="simple-dropdown-text">Nick Grissom</p>

                                    <p class="simple-dropdown-text">Sarah Diamond</p>

                                    <p class="simple-dropdown-text">Marcus Jhonson</p>
                                </div>
                            </div>

                            <div class="reaction-item">
                                <img class="reaction-image reaction-item-dropdown-trigger" src="{{ asset("assets/$VIEW_ROOT/img/reaction/love.png") }}"
                                     alt="reaction-love">

                                <div class="simple-dropdown padded reaction-item-dropdown">
                                    <p class="simple-dropdown-text"><img class="reaction" src="{{ asset("assets/$VIEW_ROOT/img/reaction/love.png") }}"
                                                                         alt="reaction-love"> <span
                                            class="bold">Love</span></p>

                                    <p class="simple-dropdown-text">Jett Spiegel</p>

                                    <p class="simple-dropdown-text">Jane Rodgers</p>
                                </div>
                            </div>

                            <div class="reaction-item">
                                <img class="reaction-image reaction-item-dropdown-trigger" src="{{ asset("assets/$VIEW_ROOT/img/reaction/like.png") }}"
                                     alt="reaction-like">

                                <div class="simple-dropdown padded reaction-item-dropdown">
                                    <p class="simple-dropdown-text"><img class="reaction" src="{{ asset("assets/$VIEW_ROOT/img/reaction/like.png") }}"
                                                                         alt="reaction-like"> <span
                                            class="bold">Like</span></p>

                                    <p class="simple-dropdown-text">Neko Bebop</p>

                                    <p class="simple-dropdown-text">Nick Grissom</p>
                                </div>
                            </div>
                        </div>

                        <p class="meta-line-text">8</p>
                    </div>
                </div>

                <div class="content-action">
                    <div class="meta-line">
                        <a class="meta-line-link" href="profile-post.html#comments">2 Comments</a>
                    </div>

                    <div class="meta-line">
                        <p class="meta-line-text">0 Shares</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
