@extends("$VIEW_ROOT.layouts.base",[
        'FILE_NAME' => 'profile-post',
        'HEADER_CLASS' => 'article-header',

        'NO_COPY' => true,
        'NO_CUT' => true,
    ])

@section('page_title')
{{ $article->seo()['google']->title ?? $article->heading }}
@endsection

@section('page_keywords')
{{ $article->seo()['google']->keywords ?? '' }}
@endsection

@section('page_description')
{{ $article->seo()['google']->description ?? $article->heading }}
@endsection

@section('page_image'){{
    $article->lastImage('article_header')
    ?? ( $article->category? $article->category->lastImage('article_default_header'):null )
    ?? asset("assets/$VIEW_ROOT/img/cover/19.jpg")
}}@endsection

@push('css')
    <link rel="stylesheet" href="{{ asset("assets/$VIEW_ROOT/pages/css/articles-single.css") }}">
@endpush

@push('js')
    <script src="{{ asset('assets/interface/pages/js/breadcrumb.js') }}"></script>
    <script src="{{ asset('assets/interface/js/single-item.js') }}"></script>
    <script>
        //removeAfterCreation
        window.routes['articles_single_body'] = '{{ route('public.articles.single',[$article]) }}';
        window.routes['articles_single_part'] = '{{ route('public.articles.single',[$article]) }}';
        {{--window.routes['public.agency_groups.single'] = '{{ route('public.agency_groups.single',['#####']) }}';--}}
    </script>
@endpush

@section('content')

{{--    TODO: sass._grid:408 enable padding in full mode, if you want these --}}
{{--    TODO: sass._post-open:125 enable smaller width, if you want these --}}
{{--    @include("$VIEW_ROOT.includes.nav_widget_small")--}}


{{--    TODO: These are good to show author properties--}}
{{--    TODO: sass._post-open:125 enable smaller width, if you want these --}}
{{--    @include("$VIEW_ROOT.includes.nav_widget")--}}
{{--    @include("$VIEW_ROOT.includes.nav_widget_mobile")--}}


<div class="content-grid full">
    <article class="post-open">
        @include("$VIEW_ROOT.pages.articles_single._header",[
            'IMAGE' => $article->lastImage('article_header') ?? ($article->category? $article->category->lastImage('article_default_header'):null) ?? asset("assets/$VIEW_ROOT/img/cover/19.jpg"),
            'ALT'   => $article->seo()['google']->title ?? $article->heading,
        ])

        <div class="post-open-body">
            <div class="post-open-heading">
                <h1 class="post-open-title">{{ $article->heading }}</h1>
                @includeWhen($article->category,"$VIEW_ROOT.pages.articles_single._breadcrumb")
                @if(isset($preview) && $preview)
                    <div class="post-open-breadcrumb">
                        <ul>
                            @if(!$article->category)<li><b>فاقد دسته‌بندی</b> می‌باشد.</li>@endif
{{--                            TODO: Operable below line --}}
                            @if(false)<li>دارای تصویر <b>Placeholder</b> می‌باشد.</li>@endif
                        </ul>
                    </div>
                @endif

                    @author($article)
                        <p class="post-open-timestamp">
                            آخرین تغییر: <span class="highlighted">{{ $article->updated_at->diffForHumans() }}</span>
                        @if( $article->author->user->isSameAsCurrent()
                            && !in_array($article->status, config_keys('enums.articles_status', ['checking', 'paused']))
                        )
                            <a href="{{ route('client.articles.single.edit',[$article]) }}">مطلب خودت رو توسعه بده</a>
                        @endif
                        @role('author_supervisor@web|admin@web')
                            وضعیت: {{ config_trans('enums.articles_status',$article->status) }}
                            @if(in_array($article->status ,config_keys('enums.articles_status',['draft','requested'])))
                                <a href="{{ route('client.articles.single.edit',[$article]) }}">شروع بررسی این مطلب</a>
                            @elseif(!in_array($article->status ,config_keys('enums.articles_status',['seo-ready'])))
                                <a href="{{ route('client.articles.single.edit',[$article]) }}">ویرایش این مطلب</a>
                            @endif
                        @endrole
                        </p>
                    @else
                        <p class="post-open-timestamp"><span class="highlighted">{{ $article->created_at->diffForHumans() }}</span>{{ $article->reading_time }} دقیقه مطالعه</p>
                    @endauthor
            </div>

            <div class="post-open-content">
                @include("$VIEW_ROOT.pages.articles_single._sidebar")

                <div class="post-open-content-body vue-el" data-route="articles_single_body">
                {!! $article->publicContent() !!}
{{--                    @include("$VIEW_ROOT.pages.articles_single._tags_list")--}}
                </div>
            </div>

{{--            TODO: remove margin-bottom at sass:_post-open:50, when belows activated --}}
{{--            @includeWhen(!isset($preview),"$VIEW_ROOT.pages.articles_single._participation",['ROUTE' => 'articles_single_part'])--}}

{{--            @includeWhen(!isset($preview),"$VIEW_ROOT.pages.articles_single._reactions")--}}

{{--            @includeWhen(!isset($preview),"$VIEW_ROOT.includes.comments.base")--}}
        </div>
    </article>

</div>

{{--    @include("$VIEW_ROOT.pages.articles_single._related_articles")--}}
@endsection


@push('ld-json')
    @include('schema.article',[
        'heading' => $article->heading,
        'images' => $article->images(),
        'dates' => [
            'created' => $article->created_at,
            'modified' => $article->updated_at,
        ],
        'author' => [
            'user' => $article->author->user,
            'nickname' => $article->author->user->nickname ?? null ,
        ],
    ])
@endpush
