<div class="post-options">
    <div class="post-option-wrap">
        <div class="post-option reaction-options-dropdown-trigger">
            <svg class="post-option-icon icon-thumbs-up">
                <use xlink:href="#svg-thumbs-up"></use>
            </svg>

            <p class="post-option-text">React!</p>
        </div>

        <div class="reaction-options reaction-options-dropdown">
            <div class="reaction-option text-tooltip-tft" data-title="Like">
                <img class="reaction-option-image" src="{{ asset("assets/$VIEW_ROOT/img/reaction/like.png") }}" alt="reaction-like">
            </div>

            <div class="reaction-option text-tooltip-tft" data-title="Love">
                <img class="reaction-option-image" src="{{ asset("assets/$VIEW_ROOT/img/reaction/love.png") }}" alt="reaction-love">
            </div>

            <div class="reaction-option text-tooltip-tft" data-title="Dislike">
                <img class="reaction-option-image" src="{{ asset("assets/$VIEW_ROOT/img/reaction/dislike.png") }}" alt="reaction-dislike">
            </div>

            <div class="reaction-option text-tooltip-tft" data-title="Happy">
                <img class="reaction-option-image" src="{{ asset("assets/$VIEW_ROOT/img/reaction/happy.png") }}" alt="reaction-happy">
            </div>

            <div class="reaction-option text-tooltip-tft" data-title="Funny">
                <img class="reaction-option-image" src="{{ asset("assets/$VIEW_ROOT/img/reaction/funny.png") }}" alt="reaction-funny">
            </div>

            <div class="reaction-option text-tooltip-tft" data-title="Wow">
                <img class="reaction-option-image" src="{{ asset("assets/$VIEW_ROOT/img/reaction/wow.png") }}" alt="reaction-wow">
            </div>

            <div class="reaction-option text-tooltip-tft" data-title="Angry">
                <img class="reaction-option-image" src="{{ asset("assets/$VIEW_ROOT/img/reaction/angry.png") }}" alt="reaction-angry">
            </div>

            <div class="reaction-option text-tooltip-tft" data-title="Sad">
                <img class="reaction-option-image" src="{{ asset("assets/$VIEW_ROOT/img/reaction/sad.png") }}" alt="reaction-sad">
            </div>
        </div>
    </div>

    <div class="post-option active">
        <svg class="post-option-icon icon-comment">
            <use xlink:href="#svg-comment"></use>
        </svg>

        <p class="post-option-text">Comment</p>
    </div>

    <div class="post-option">
        <svg class="post-option-icon icon-share">
            <use xlink:href="#svg-share"></use>
        </svg>

        <p class="post-option-text">Share</p>
    </div>
</div>
