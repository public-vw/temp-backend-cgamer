<div class="content-actions vue-el" data-route="{{ $ROUTE }}">
    <div class="content-action">
        <div class="meta-line">
            <div class="meta-line-list reaction-item-list">
                <div class="reaction-item">
                    <img class="reaction-image reaction-item-dropdown-trigger" src="{{ asset("assets/$VIEW_ROOT/img/reaction/happy.png") }}"
                         alt="reaction-happy">

                    <div class="simple-dropdown padded reaction-item-dropdown">
                        <p class="simple-dropdown-text"><img class="reaction" src="{{ asset("assets/$VIEW_ROOT/img/reaction/happy.png") }}"
                                                             alt="reaction-happy"> <span
                                class="bold">Happy</span></p>

                        <p class="simple-dropdown-text">Matt Parker</p>

                        <p class="simple-dropdown-text">Destroy Dex</p>

                        <p class="simple-dropdown-text">The Green Goo</p>
                    </div>
                </div>

                <div class="reaction-item">
                    <img class="reaction-image reaction-item-dropdown-trigger" src="{{ asset("assets/$VIEW_ROOT/img/reaction/love.png") }}"
                         alt="reaction-love">

                    <div class="simple-dropdown padded reaction-item-dropdown">
                        <p class="simple-dropdown-text"><img class="reaction" src="{{ asset("assets/$VIEW_ROOT/img/reaction/love.png") }}"
                                                             alt="reaction-love"> <span
                                class="bold">Love</span></p>

                        <p class="simple-dropdown-text">Sandra Strange</p>
                    </div>
                </div>

                <div class="reaction-item">
                    <img class="reaction-image reaction-item-dropdown-trigger" src="{{ asset("assets/$VIEW_ROOT/img/reaction/like.png") }}"
                         alt="reaction-like">

                    <div class="simple-dropdown padded reaction-item-dropdown">
                        <p class="simple-dropdown-text"><img class="reaction" src="{{ asset("assets/$VIEW_ROOT/img/reaction/like.png") }}"
                                                             alt="reaction-like"> <span
                                class="bold">Like</span></p>

                        <p class="simple-dropdown-text">Neko Bebop</p>

                        <p class="simple-dropdown-text">Nick Grissom</p>

                        <p class="simple-dropdown-text">Sarah Diamond</p>

                        <p class="simple-dropdown-text">Jett Spiegel</p>

                        <p class="simple-dropdown-text">Marcus Jhonson</p>

                        <p class="simple-dropdown-text">Jane Rodgers</p>

                        <p class="simple-dropdown-text"><span class="bold">and 2 more...</span></p>
                    </div>
                </div>
            </div>

            <p class="meta-line-text">12</p>
        </div>

        <div class="meta-line">
            <div class="meta-line-list user-avatar-list">
                <div class="user-avatar micro no-stats">
                    <div class="user-avatar-border">
                        <div class="hexagon-22-24"></div>
                    </div>

                    <div class="user-avatar-content">
                        <div class="hexagon-image-18-20" data-src="{{ asset("assets/$VIEW_ROOT/img/avatar/09.jpg") }}"></div>
                    </div>
                </div>

                <div class="user-avatar micro no-stats">
                    <div class="user-avatar-border">
                        <div class="hexagon-22-24"></div>
                    </div>

                    <div class="user-avatar-content">
                        <div class="hexagon-image-18-20" data-src="{{ asset("assets/$VIEW_ROOT/img/avatar/08.jpg") }}"></div>
                    </div>
                </div>

                <div class="user-avatar micro no-stats">
                    <div class="user-avatar-border">
                        <div class="hexagon-22-24"></div>
                    </div>

                    <div class="user-avatar-content">
                        <div class="hexagon-image-18-20" data-src="{{ asset("assets/$VIEW_ROOT/img/avatar/12.jpg") }}"></div>
                    </div>
                </div>

                <div class="user-avatar micro no-stats">
                    <div class="user-avatar-border">
                        <div class="hexagon-22-24"></div>
                    </div>

                    <div class="user-avatar-content">
                        <div class="hexagon-image-18-20" data-src="{{ asset("assets/$VIEW_ROOT/img/avatar/16.jpg") }}"></div>
                    </div>
                </div>

                <div class="user-avatar micro no-stats">
                    <div class="user-avatar-border">
                        <div class="hexagon-22-24"></div>
                    </div>

                    <div class="user-avatar-content">
                        <div class="hexagon-image-18-20" data-src="{{ asset("assets/$VIEW_ROOT/img/avatar/06.jpg") }}"></div>
                    </div>
                </div>
            </div>

            <p class="meta-line-text">14 Participants</p>
        </div>
    </div>

    <div class="content-action">
        <div class="meta-line">
            <p class="meta-line-link">3 Comments</p>
        </div>

        <div class="meta-line">
            <p class="meta-line-text">0 Shares</p>
        </div>
    </div>
</div>
