<div class="navigation-wrap">
    <nav class="navigation grid-limit">
        {{--        <a class="button small primary"--}}
        {{--           href="https://themeforest.net/item/vikinger-social-community-and-marketplace-html-template/25715500"--}}
        {{--           target="_blank">Buy it Now!</a>--}}

        <ul class="menu">
            @foreach([
                    'کریپتوباز'=>['/'],
//                    'درباره ما'=>['/about-us'],
//                    'تالار فرصت'=>['/cooperating'],
                    'قوانین سایت'=>['/terms'],
                ] as $title => $details)
            <li class="menu-item">
                <a class="menu-item-link primary" href="{{ $details[0] }}">{{ $title }}</a>
            </li>
            @endforeach
            @auth
                <li class="menu-item">
                    <a class="menu-item-link primary" href="{{ route('client.profile.index') }}">
                        پروفایل من
                        @include("$VIEW_ROOT.pages.homepage._avatar_small")
                    </a>
                </li>
            @endauth
        </ul>

        <div class="logo void">
            <img src="{{ asset("assets/interface/images/logo/logo-white.svg") }}" width="90px" height="90px" alt="{{ config('app.title', 'Laravel') }} logo">
        </div>

    </nav>
</div>
