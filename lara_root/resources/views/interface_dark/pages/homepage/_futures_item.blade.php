<article class="feature-item">
    <img class="feature-item-image" src="{{ $ICON }}" alt="{{ $ALT }}">

    <h2 class="feature-item-title">{{ $TITLE }}</h2>

    <p class="feature-item-text">{!! $CONTENT !!}</p>
</article>
