@extends("$VIEW_ROOT.layouts.base",[
        'FILE_NAME' => 'not-in-package',
        'NO_HEADER' => true,
    ])

@section('page_title')
    {{ __('seo.homepage.title') }}
@endsection

@section('page_keywords')
    {{ __('seo.homepage.keywords') }}
@endsection

@section('page_description')
    {{ __('seo.homepage.description') }}
@endsection

@section('head')
    <link href="{{ asset("assets/{$VIEW_ROOT}/css/homepage.css") }}" rel="stylesheet">
    <link rel="canonical" href="{{ route('public.homepage') }}">
@endsection

@section('content')

    @include("{$VIEW_ROOT}.pages.homepage._navigation")

    @include("{$VIEW_ROOT}.pages.homepage._banner")

        {{--    @include("{$VIEW_ROOT}.pages.homepage._itemlist")--}}

        {{--    @include("{$VIEW_ROOT}.pages.homepage._section_wrap_boxes",[
                    'LABEL' => '',
                    'HEADING' => '',
                    'DESCRIPTION' => '',
                ])    --}}

    @if($futures ?? false)
    @include("{$VIEW_ROOT}.pages.homepage._futures",[
        'LABEL' => '',
        'HEADING' => '',
        'DESCRIPTION' => '',
        'ITEMS' => [
            [
                'icon'    => '',
                'alt'     => '',
                'title'   => '',
                'content' => '',
            ],
        ],
    ])
    @endif

{{--    @include("{$VIEW_ROOT}.pages.homepage._section_wrap_image")--}}

    <div class="vrbg-container">
        @for($i=1; $i<=6; $i++)
            <div class="vrbg vr{{ $i }}"><img src="{{ asset("assets/{$VIEW_ROOT}/img/vr_icons/{$i}.svg") }}" alt="vr_icon{{ $i }}"></div>
        @endfor

    @foreach($categories as $i => $category)
        @include("{$VIEW_ROOT}.pages.homepage._promo_section_single",[
            'LABEL' => $category->parent->heading,
            "TITLE" => $category->heading,
            "CONTENT" => $category->summary,
            "IMAGE" => [
                'src' => $category->lastImage('article_category_homepage_thumbnail') ?? asset("assets/$VIEW_ROOT/img/homepage/02.png") ,
                'alt' => $category->heading,
            ],
            'LINK' => $category->seoUrl(),
            'LINK_TITLE' => 'بیشتر بدونیم',
            'INVERTED' => $i % 2,
        ])
    @endforeach
    </div>

    {{--    @include("{$VIEW_ROOT}.pages.homepage._section_wrap_rows")--}}

@endsection

