<div id="features" class="section-wrap">
    <section class="section grid-limit">
        <div class="section-info">
            <p class="section-pretitle">{{ $LABEL }}</p>

            <h2 class="section-title">{{ $HEADING }}</h2>

            <p class="section-text">{{ $DESCRIPTION }}</p>
        </div>

        <div class="grid grid-4">
            @foreach($ITEMS as $ITEM)

                @include("{$VIEW_ROOT}.pages.homepage._futures_item",[
                    'ICON' => $ITEM['icon'] ?? asset("assets/$VIEW_ROOT/img/homepage/html5.png"),
                    'ALT' => $ITEM['title'] ?? '',
                    'TITLE' => $ITEM['title'] ?? '',
                    'CONTENT' => $ITEM['content'] ?? '',
                ])

            @endforeach
        </div>
    </section>
</div>
