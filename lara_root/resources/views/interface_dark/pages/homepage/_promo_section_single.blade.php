<div class="promo-section-wrap">
    <section class="promo-section grid-limit @if($INVERTED ?? false) inverted @endif">
        <div class="promo-section-info">
            <p class="promo-section-pretitle">{{ $LABEL }}</p>

            <h2 class="promo-section-title">{!! $TITLE !!}</h2>

            <p class="promo-section-text">{!! strip_tags($CONTENT) !!}</p>

            <a href="{{ $LINK }}" class="button full">{{ $LINK_TITLE }}</a>
        </div>

        <img class="promo-section-image" src="{{ $IMAGE['src'] }}" alt="{{ $IMAGE['alt'] }}">
    </section>
</div>
