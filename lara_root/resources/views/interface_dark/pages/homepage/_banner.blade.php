<div id="top" class="banner-wrap">
    <div class="banner grid-limit">
        <div class="banner-info">
            <h1>
            <p class="banner-pretitle">جامعه بلاک‌چین</p>

            <p class="banner-title">{{ config('app.title') }}</p>
            </h1>
            <p class="banner-text">{{ config('app.description') }}</p>

            @guest
                @if (Route::has('auth.register'))
                    <a class="button popup-reglog-trigger" href="javascript:;"><img src="{{ asset('assets/interface_dark/img/cartoon/very-surprised.png') }}" alt="register"/>همین الان همراه بشم!</a>
                @else
                    <a class="button btn-right" href="{{ $starterCategory->seoUrl() }}"><img src="{{ asset('assets/interface_dark/img/cartoon/junior-b.png') }}" alt="junior"/>تازه وارد هستم</a>
                @endif
            @else
                <a class="button btn-right" href="{{ route('client.articles.single.new') }}"><img src="{{ asset('assets/interface_dark/img/cartoon/senior-b.png') }}" alt="senior"/>می‌خوام پست بگذارم</a>
                <a class="button btn-right" href="{{ $starterCategory->seoUrl() }}"><img src="{{ asset('assets/interface_dark/img/cartoon/junior-b.png') }}" alt="junior"/>تازه وارد هستم</a>
            @endguest
        </div>
    </div>
</div>
