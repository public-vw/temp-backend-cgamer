<div class="section-wrap">
    <section class="section grid-limit">
        <div class="section-info">
            <p class="section-pretitle">Features</p>

            <h2 class="section-title">Discover Tons of Widgets!</h2>

            <p class="section-text">We carefully crafted lots of widgets for you to have an incredible amount of
                customization options! Everything you may need like different post updates, badge boxes, about boxes,
                calendars, product boxes, media, quests, infographics, streams, discussions, blog posts and much
                more!</p>
        </div>

        <img class="section-image" src="{{ asset("assets/$VIEW_ROOT/img/homepage/01.png") }}" alt="section-image-01">

        <a class="section-button button tertiary" href="https://odindesignthemes.com/vikinger/profile-timeline.html"
           target="_blank">Browse all in the Live Demo!</a>
    </section>
</div>
