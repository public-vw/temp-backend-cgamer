@php($user = auth()->user())
<div class="user-avatar small no-outline avatar-popup-trigger" style="float: right;margin-top: 7px;margin-left: 7px;">
    <div class="user-avatar-content">
        <img width="34px" height="34px" style="border-radius:50%; border: 2px solid #90ce46; padding: 2px;" src="{{ $user->lastImage('avatar') ?? asset("assets/$VIEW_ROOT/img/avatar/01.jpg") }}"/>
    </div>
</div>
