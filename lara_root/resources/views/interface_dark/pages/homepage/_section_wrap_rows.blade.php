<div class="section-wrap">
    <section class="section grid-limit">
        <div class="section-info">
            <p class="section-pretitle">Features</p>

            <h2 class="section-title">Illustrations and Icons Included!</h2>

            <p class="section-text">Yes! All illustrations like the one in the landing, the people illustrations, the
                badges, all the banners, forum icons and more were designed for this template by us and are all included
                in the PSD folder of the pack! We also included all icons in SVG and PS vectors!</p>
        </div>

        <img class="section-image" src="{{ asset("assets/$VIEW_ROOT/img/homepage/10.png") }}" alt="section-image-10">
    </section>
</div>
