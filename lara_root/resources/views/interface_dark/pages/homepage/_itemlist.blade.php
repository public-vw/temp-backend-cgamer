<div class="architecture-item-list">
    <div class="architecture-item">
        <img class="architecture-image" src="{{ asset("assets/$VIEW_ROOT/img/homepage/html.png") }}" alt="feature-html">

        <img class="architecture-image-hover" src="{{ asset("assets/$VIEW_ROOT/img/homepage/html-hover.png") }}" alt="feature-html-hover">
    </div>

    <div class="architecture-item">
        <img class="architecture-image" src="{{ asset("assets/$VIEW_ROOT/img/homepage/css.png") }}" alt="feature-css">

        <img class="architecture-image-hover" src="{{ asset("assets/$VIEW_ROOT/img/homepage/css-hover.png") }}" alt="feature-css-hover">
    </div>

    <div class="architecture-item">
        <img class="architecture-image" src="{{ asset("assets/$VIEW_ROOT/img/homepage/js.png") }}" alt="feature-js">

        <img class="architecture-image-hover" src="{{ asset("assets/$VIEW_ROOT/img/homepage/js-hover.png") }}" alt="feature-js-hover">
    </div>

    <div class="architecture-item">
        <img class="architecture-image" src="{{ asset("assets/$VIEW_ROOT/img/homepage/sass.png") }}" alt="feature-sass">

        <img class="architecture-image-hover" src="{{ asset("assets/$VIEW_ROOT/img/homepage/sass-hover.png") }}" alt="feature-sass-hover">
    </div>

    <div class="architecture-item">
        <img class="architecture-image" src="{{ asset("assets/$VIEW_ROOT/img/homepage/googlef.png") }}" alt="feature-googlef">

        <img class="architecture-image-hover" src="{{ asset("assets/$VIEW_ROOT/img/homepage/googlef-hover.png") }}"
             alt="feature-googlef-hover">
    </div>
</div>
