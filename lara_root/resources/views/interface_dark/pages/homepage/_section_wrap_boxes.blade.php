<div id="demos" class="section-wrap">
    <section class="section grid-limit">
        <div class="section-info">
            <p class="section-pretitle">{{ $LABEL }}</p>

            <h2 class="section-title">{{ $HEADING }}</h2>

            <p class="section-text">{{ $DESCRIPTION }}</p>
        </div>

        <div class="grid grid-3">
            <div class="demo-box">
                <a href="https://odindesignthemes.com/vikinger/" target="_blank">
                    <figure class="demo-box-cover liquid">
                        <img src="{{ asset("assets/$VIEW_ROOT/img/homepage/landing.jpg") }}" alt="page-landing">
                    </figure>
                </a>

                <p class="demo-box-title">Landing</p>

                <div class="demo-box-actions">
                    <a class="button tiny primary" href="https://odindesignthemes.com/vikinger/"
                       target="_blank">Light</a>

                    <a class="button tiny secondary" href="https://odindesignthemes.com/vikinger-dark/" target="_blank">Dark</a>
                </div>
            </div>

            <div class="demo-box">
                <a href="https://odindesignthemes.com/vikinger/profile-timeline.html" target="_blank">
                    <figure class="demo-box-cover liquid">
                        <img src="{{ asset("assets/$VIEW_ROOT/img/homepage/profile-timeline.jpg") }}" alt="page-profile-timeline">
                    </figure>
                </a>

                <p class="demo-box-title">Profile Timeline</p>

                <div class="demo-box-actions">
                    <a class="button tiny primary" href="https://odindesignthemes.com/vikinger/profile-timeline.html"
                       target="_blank">Light</a>

                    <a class="button tiny secondary"
                       href="https://odindesignthemes.com/vikinger-dark/profile-timeline.html" target="_blank">Dark</a>
                </div>
            </div>

            <div class="demo-box">
                <a href="https://odindesignthemes.com/vikinger/" target="_blank">
                    <figure class="demo-box-cover liquid">
                        <img src="{{ asset("assets/$VIEW_ROOT/img/homepage/landing.jpg") }}" alt="page-landing">
                    </figure>
                </a>

                <p class="demo-box-title">Landing</p>

                <div class="demo-box-actions">
                    <a class="button tiny primary" href="https://odindesignthemes.com/vikinger/"
                       target="_blank">Light</a>

                    <a class="button tiny secondary" href="https://odindesignthemes.com/vikinger-dark/" target="_blank">Dark</a>
                </div>
            </div>

            <div class="demo-box">
                <a href="https://odindesignthemes.com/vikinger/profile-timeline.html" target="_blank">
                    <figure class="demo-box-cover liquid">
                        <img src="{{ asset("assets/$VIEW_ROOT/img/homepage/profile-timeline.jpg") }}" alt="page-profile-timeline">
                    </figure>
                </a>

                <p class="demo-box-title">Profile Timeline</p>

                <div class="demo-box-actions">
                    <a class="button tiny primary" href="https://odindesignthemes.com/vikinger/profile-timeline.html"
                       target="_blank">Light</a>

                    <a class="button tiny secondary"
                       href="https://odindesignthemes.com/vikinger-dark/profile-timeline.html" target="_blank">Dark</a>
                </div>
            </div>

            <div class="demo-box">
                <a href="https://odindesignthemes.com/vikinger/" target="_blank">
                    <figure class="demo-box-cover liquid">
                        <img src="{{ asset("assets/$VIEW_ROOT/img/homepage/landing.jpg") }}" alt="page-landing">
                    </figure>
                </a>

                <p class="demo-box-title">Landing</p>

                <div class="demo-box-actions">
                    <a class="button tiny primary" href="https://odindesignthemes.com/vikinger/"
                       target="_blank">Light</a>

                    <a class="button tiny secondary" href="https://odindesignthemes.com/vikinger-dark/" target="_blank">Dark</a>
                </div>
            </div>

            <div class="demo-box">
                <a href="https://odindesignthemes.com/vikinger/profile-timeline.html" target="_blank">
                    <figure class="demo-box-cover liquid">
                        <img src="{{ asset("assets/$VIEW_ROOT/img/homepage/profile-timeline.jpg") }}" alt="page-profile-timeline">
                    </figure>
                </a>

                <p class="demo-box-title">Profile Timeline</p>

                <div class="demo-box-actions">
                    <a class="button tiny primary" href="https://odindesignthemes.com/vikinger/profile-timeline.html"
                       target="_blank">Light</a>

                    <a class="button tiny secondary"
                       href="https://odindesignthemes.com/vikinger-dark/profile-timeline.html" target="_blank">Dark</a>
                </div>
            </div>

            <div class="demo-box">
                <a href="https://odindesignthemes.com/vikinger/" target="_blank">
                    <figure class="demo-box-cover liquid">
                        <img src="{{ asset("assets/$VIEW_ROOT/img/homepage/landing.jpg") }}" alt="page-landing">
                    </figure>
                </a>

                <p class="demo-box-title">Landing</p>

                <div class="demo-box-actions">
                    <a class="button tiny primary" href="https://odindesignthemes.com/vikinger/"
                       target="_blank">Light</a>

                    <a class="button tiny secondary" href="https://odindesignthemes.com/vikinger-dark/" target="_blank">Dark</a>
                </div>
            </div>

            <div class="demo-box">
                <a href="https://odindesignthemes.com/vikinger/profile-timeline.html" target="_blank">
                    <figure class="demo-box-cover liquid">
                        <img src="{{ asset("assets/$VIEW_ROOT/img/homepage/profile-timeline.jpg") }}" alt="page-profile-timeline">
                    </figure>
                </a>

                <p class="demo-box-title">Profile Timeline</p>

                <div class="demo-box-actions">
                    <a class="button tiny primary" href="https://odindesignthemes.com/vikinger/profile-timeline.html"
                       target="_blank">Light</a>

                    <a class="button tiny secondary"
                       href="https://odindesignthemes.com/vikinger-dark/profile-timeline.html" target="_blank">Dark</a>
                </div>
            </div>

        </div>
    </section>
</div>

<div class="section-wrap">
    <section class="section grid-limit">
        <div class="section-info">
            <p class="section-pretitle">Features</p>

            <h2 class="section-title">Discover Tons of Widgets!</h2>

            <p class="section-text">We carefully crafted lots of widgets for you to have an incredible amount of
                customization options! Everything you may need like different post updates, badge boxes, about boxes,
                calendars, product boxes, media, quests, infographics, streams, discussions, blog posts and much
                more!</p>
        </div>

        <img class="section-image" src="{{ asset("assets/$VIEW_ROOT/img/homepage/01.png") }}" alt="section-image-01">

        <a class="section-button button tertiary" href="https://odindesignthemes.com/vikinger/profile-timeline.html"
           target="_blank">Browse all in the Live Demo!</a>
    </section>
</div>
