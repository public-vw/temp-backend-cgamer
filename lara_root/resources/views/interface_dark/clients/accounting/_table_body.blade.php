<div class="table-row micro">
    <div class="table-column">
        <p class="table-text"><span class="light">Nov 15th, 2019</span></p>
    </div>

    <div class="table-column padded-left">
        <a class="table-link" href="marketplace-product.html"><span class="highlighted">Pixel Diamond Gaming Magazine</span></a>
    </div>

    <div class="table-column centered padded">
        <p class="table-title">Sale</p>
    </div>

    <div class="table-column centered padded">
        <p class="table-text"><span class="light">VK1287</span></p>
    </div>

    <div class="table-column centered padded">
        <p class="table-title">$26</p>
    </div>


    <div class="table-column centered padded">
        <p class="table-title">$13</p>
    </div>

    <div class="table-column padded-left">
        <div class="percentage-diff-icon-wrap positive">
            <svg class="percentage-diff-icon icon-plus-small">
                <use xlink:href="#svg-plus-small"></use>
            </svg>
        </div>
    </div>
</div>

<div class="table-row micro">
    <div class="table-column">
        <p class="table-text"><span class="light">Nov 15th, 2019</span></p>
    </div>

    <div class="table-column padded-left">
        <a class="table-link" href="marketplace-product.html"><span class="highlighted">Twitch Stream UI Pack</span></a>
    </div>

    <div class="table-column centered padded">
        <p class="table-title">Sale</p>
    </div>

    <div class="table-column centered padded">
        <p class="table-text"><span class="light">VK1364</span></p>
    </div>

    <div class="table-column centered padded">
        <p class="table-title">$12</p>
    </div>


    <div class="table-column centered padded">
        <p class="table-title">$6</p>
    </div>

    <div class="table-column padded-left">
        <div class="percentage-diff-icon-wrap positive">
            <svg class="percentage-diff-icon icon-plus-small">
                <use xlink:href="#svg-plus-small"></use>
            </svg>
        </div>
    </div>
</div>

<div class="table-row micro">
    <div class="table-column">
        <p class="table-text"><span class="light">Nov 14th, 2019</span></p>
    </div>

    <div class="table-column padded-left">
        <a class="table-link" href="marketplace-product.html"><span class="highlighted">Pixel Diamond Gaming Magazine</span></a>
    </div>

    <div class="table-column centered padded">
        <p class="table-title">Sale</p>
    </div>

    <div class="table-column centered padded">
        <p class="table-text"><span class="light">VK7638</span></p>
    </div>

    <div class="table-column centered padded">
        <p class="table-title">$26</p>
    </div>


    <div class="table-column centered padded">
        <p class="table-title">$13</p>
    </div>

    <div class="table-column padded-left">
        <div class="percentage-diff-icon-wrap positive">
            <svg class="percentage-diff-icon icon-plus-small">
                <use xlink:href="#svg-plus-small"></use>
            </svg>
        </div>
    </div>
</div>

<div class="table-row micro">
    <div class="table-column">
        <p class="table-text"><span class="light">Nov 14th, 2019</span></p>
    </div>

    <div class="table-column padded-left">
        <a class="table-link" href="marketplace-product.html"><span class="highlighted">Twitch Stream UI Pack</span></a>
    </div>

    <div class="table-column centered padded">
        <p class="table-title">Sale</p>
    </div>

    <div class="table-column centered padded">
        <p class="table-text"><span class="light">VK7285</span></p>
    </div>

    <div class="table-column centered padded">
        <p class="table-title">$12</p>
    </div>


    <div class="table-column centered padded">
        <p class="table-title">$6</p>
    </div>

    <div class="table-column padded-left">
        <div class="percentage-diff-icon-wrap positive">
            <svg class="percentage-diff-icon icon-plus-small">
                <use xlink:href="#svg-plus-small"></use>
            </svg>
        </div>
    </div>
</div>

<div class="table-row micro">
    <div class="table-column">
        <p class="table-text"><span class="light">Nov 14th, 2019</span></p>
    </div>

    <div class="table-column padded-left">
        <a class="table-link" href="marketplace-product.html"><span class="highlighted">Twitch Stream UI Pack</span></a>
    </div>

    <div class="table-column centered padded">
        <p class="table-title">Sale</p>
    </div>

    <div class="table-column centered padded">
        <p class="table-text"><span class="light">VK9673</span></p>
    </div>

    <div class="table-column centered padded">
        <p class="table-title">$12</p>
    </div>


    <div class="table-column centered padded">
        <p class="table-title">$6</p>
    </div>

    <div class="table-column padded-left">
        <div class="percentage-diff-icon-wrap positive">
            <svg class="percentage-diff-icon icon-plus-small">
                <use xlink:href="#svg-plus-small"></use>
            </svg>
        </div>
    </div>
</div>

<div class="table-row micro">
    <div class="table-column">
        <p class="table-text"><span class="light">Nov 12th, 2019</span></p>
    </div>

    <div class="table-column padded-left">
        <a class="table-link" href="marketplace-product.html"><span class="highlighted">Gaming Coin Badges Pack</span></a>
    </div>

    <div class="table-column centered padded">
        <p class="table-title">Purchase</p>
    </div>

    <div class="table-column centered padded">
        <p class="table-text"><span class="light">VK2589</span></p>
    </div>

    <div class="table-column centered padded">
        <p class="table-title">$6</p>
    </div>

    <div class="table-column centered padded">
        <p class="table-text"><span class="light">-</span></p>
    </div>


    <div class="table-column padded-left">
        <div class="percentage-diff-icon-wrap negative">
            <svg class="percentage-diff-icon icon-minus-small">
                <use xlink:href="#svg-minus-small"></use>
            </svg>
        </div>
    </div>
</div>

<div class="table-row micro">
    <div class="table-column">
        <p class="table-text"><span class="light">Nov 9th, 2019</span></p>
    </div>

    <div class="table-column padded-left">
        <a class="table-link" href="marketplace-product.html"><span class="highlighted">People Illustrations Pack 01</span></a>
    </div>

    <div class="table-column centered padded">
        <p class="table-title">Purchase</p>
    </div>

    <div class="table-column centered padded">
        <p class="table-text"><span class="light">VK3146</span></p>
    </div>

    <div class="table-column centered padded">
        <p class="table-title">$5</p>
    </div>

    <div class="table-column centered padded">
        <p class="table-text"><span class="light">-</span></p>
    </div>

    <div class="table-column padded-left">
        <div class="percentage-diff-icon-wrap negative">
            <svg class="percentage-diff-icon icon-minus-small">
                <use xlink:href="#svg-minus-small"></use>
            </svg>
        </div>
    </div>
</div>

<div class="table-row micro">
    <div class="table-column">
        <p class="table-text"><span class="light">Nov 9th, 2019</span></p>
    </div>

    <div class="table-column padded-left">
        <a class="table-link" href="marketplace-product.html"><span class="highlighted">Pixel Diamond Gaming Magazine</span></a>
    </div>

    <div class="table-column centered padded">
        <p class="table-title">Sale</p>
    </div>

    <div class="table-column centered padded">
        <p class="table-text"><span class="light">VK4577</span></p>
    </div>

    <div class="table-column centered padded">
        <p class="table-title">$26</p>
    </div>


    <div class="table-column centered padded">
        <p class="table-title">$13</p>
    </div>

    <div class="table-column padded-left">
        <div class="percentage-diff-icon-wrap positive">
            <svg class="percentage-diff-icon icon-plus-small">
                <use xlink:href="#svg-plus-small"></use>
            </svg>
        </div>
    </div>
</div>

<div class="table-row micro">
    <div class="table-column">
        <p class="table-text"><span class="light">Nov 9th, 2019</span></p>
    </div>

    <div class="table-column padded-left">
        <a class="table-link" href="marketplace-product.html"><span class="highlighted">Emerald Dragon Digital Marketplace</span></a>
    </div>

    <div class="table-column centered padded">
        <p class="table-title">Sale</p>
    </div>

    <div class="table-column centered padded">
        <p class="table-text"><span class="light">VK6379</span></p>
    </div>

    <div class="table-column centered padded">
        <p class="table-title">$24</p>
    </div>


    <div class="table-column centered padded">
        <p class="table-title">$12</p>
    </div>

    <div class="table-column padded-left">
        <div class="percentage-diff-icon-wrap positive">
            <svg class="percentage-diff-icon icon-plus-small">
                <use xlink:href="#svg-plus-small"></use>
            </svg>
        </div>
    </div>
</div>

<div class="table-row micro">
    <div class="table-column">
        <p class="table-text"><span class="light">Nov 8th, 2019</span></p>
    </div>

    <div class="table-column padded-left">
        <a class="table-link" href="marketplace-product.html"><span class="highlighted">Pixel Diamond Gaming Magazine</span></a>
    </div>

    <div class="table-column centered padded">
        <p class="table-title">Sale</p>
    </div>

    <div class="table-column centered padded">
        <p class="table-text"><span class="light">VK9932</span></p>
    </div>

    <div class="table-column centered padded">
        <p class="table-title">$26</p>
    </div>


    <div class="table-column centered padded">
        <p class="table-title">$13</p>
    </div>

    <div class="table-column padded-left">
        <div class="percentage-diff-icon-wrap positive">
            <svg class="percentage-diff-icon icon-plus-small">
                <use xlink:href="#svg-plus-small"></use>
            </svg>
        </div>
    </div>
</div>

<div class="table-row micro">
    <div class="table-column">
        <p class="table-text"><span class="light">Nov 5th, 2019</span></p>
    </div>

    <div class="table-column padded-left">
        <a class="table-link" href="marketplace-product.html"><span class="highlighted">Twitch Stream UI Pack</span></a>
    </div>

    <div class="table-column centered padded">
        <p class="table-title">Sale</p>
    </div>

    <div class="table-column centered padded">
        <p class="table-text"><span class="light">VK1274</span></p>
    </div>

    <div class="table-column centered padded">
        <p class="table-title">$12</p>
    </div>


    <div class="table-column centered padded">
        <p class="table-title">$6</p>
    </div>

    <div class="table-column padded-left">
        <div class="percentage-diff-icon-wrap positive">
            <svg class="percentage-diff-icon icon-plus-small">
                <use xlink:href="#svg-plus-small"></use>
            </svg>
        </div>
    </div>
</div>

<div class="table-row micro">
    <div class="table-column">
        <p class="table-text"><span class="light">Nov 4th, 2019</span></p>
    </div>

    <div class="table-column padded-left">
        <a class="table-link" href="marketplace-product.html"><span class="highlighted">Emerald Dragon Digital Marketplace</span></a>
    </div>

    <div class="table-column centered padded">
        <p class="table-title">Sale</p>
    </div>

    <div class="table-column centered padded">
        <p class="table-text"><span class="light">VK3345</span></p>
    </div>

    <div class="table-column centered padded">
        <p class="table-title">$24</p>
    </div>


    <div class="table-column centered padded">
        <p class="table-title">$12</p>
    </div>

    <div class="table-column padded-left">
        <div class="percentage-diff-icon-wrap positive">
            <svg class="percentage-diff-icon icon-plus-small">
                <use xlink:href="#svg-plus-small"></use>
            </svg>
        </div>
    </div>
</div>
