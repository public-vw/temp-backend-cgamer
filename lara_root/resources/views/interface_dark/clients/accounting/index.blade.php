@extends("$VIEW_ROOT.layouts.base",[
        'PAGE_TITLE' => 'Exchange Tools',
        'NO_HEADER' => true,
    ])

@push('css')
    <link rel="stylesheet" href="{{ asset("assets/$VIEW_ROOT/css/posts-single.css") }}">
@endpush

@section('content')
<div class="content-grid">
    @include("$VIEW_ROOT.clients.accounting._banner")

    <div class="grid grid-full medium-space">
        <div class="account-hub-content">
            <div class="section-header">
                <div class="section-header-info">
                    <p class="section-pretitle">حسابداری من</p>

                    <h2 class="section-title">پرداخت‌ها و دریافت‌ها</h2>
                </div>
            </div>

{{--            @include("$VIEW_ROOT.clients.accounting._filterbar")--}}

            <div class="table-wrap" data-simplebar>
                <div class="table table-sales">
                    <div class="table-header">
                        @include("$VIEW_ROOT.clients.accounting._table_header")
                    </div>

                    <div class="table-body same-color-rows">
                        @include("$VIEW_ROOT.clients.accounting._table_body")
                    </div>
                </div>
            </div>

            <div class="section-pager-bar-wrap align-right">
                @include("$VIEW_ROOT.clients.accounting._table_pagination")
            </div>
        </div>
    </div>
</div>
@endsection
