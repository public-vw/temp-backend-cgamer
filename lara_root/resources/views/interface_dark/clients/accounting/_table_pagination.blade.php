<div class="section-pager-bar">
    <div class="section-pager">
        <div class="section-pager-item active">
            <p class="section-pager-item-text">01</p>
        </div>

        <div class="section-pager-item">
            <p class="section-pager-item-text">02</p>
        </div>

        <div class="section-pager-item">
            <p class="section-pager-item-text">03</p>
        </div>

        <div class="section-pager-item">
            <p class="section-pager-item-text">04</p>
        </div>

        <div class="section-pager-item">
            <p class="section-pager-item-text">05</p>
        </div>

        <div class="section-pager-item">
            <p class="section-pager-item-text">06</p>
        </div>
    </div>

    <div class="section-pager-controls">
        <div class="slider-control left disabled">
            <svg class="slider-control-icon icon-small-arrow">
                <use xlink:href="#svg-small-arrow"></use>
            </svg>
        </div>

        <div class="slider-control right">
            <svg class="slider-control-icon icon-small-arrow">
                <use xlink:href="#svg-small-arrow"></use>
            </svg>
        </div>
    </div>
</div>
