<div class="section-filters-bar v6 v6-2">
    <div class="section-filters-bar-actions">
        <form class="form">
            <div class="form-item split">
                <div class="form-input-decorated">
                    <div class="form-input small active">
                        <label for="statement-from-date">From Date</label>
                        <input type="text" id="statement-from-date" name="statement_from_date"
                               value="02/22/2019">
                    </div>

                    <svg class="form-input-icon icon-events">
                        <use xlink:href="#svg-events"></use>
                    </svg>
                </div>

                <div class="form-input-decorated">
                    <div class="form-input small active">
                        <label for="statement-to-date">To Date</label>
                        <input type="text" id="statement-to-date" name="statement_to_date"
                               value="11/14/2019">
                    </div>

                    <svg class="form-input-icon icon-events">
                        <use xlink:href="#svg-events"></use>
                    </svg>
                </div>

                <button class="button primary">
                    <svg class="icon-magnifying-glass">
                        <use xlink:href="#svg-magnifying-glass"></use>
                    </svg>
                </button>
            </div>
        </form>
    </div>

    <div class="section-filters-bar-actions">
        <p class="button secondary">Download Statement</p>
    </div>
</div>
