<div class="account-hub-sidebar">
    <div class="sidebar-box">
        <div class="progress-arc-summary">

            <div class="progress-arc separator-bottom">
                {{--    <div class="progress-arc">--}}
                {{--        <canvas id="profile-completion-chart"></canvas>--}}
                {{--    </div>--}}

                {{--    <div class="progress-arc-info">--}}
                {{--        <p class="progress-arc-title">{{$PERCENT}}%</p>--}}
                {{--    </div>--}}

                <div class="sidebar-box-items">
                    <div class="totals-line-list">
                        <p class="price-title">Wallet Amount</p>
                        <p class="price-title big">{{ $AMOUNT }} <span class="currency">IRT</span></p>
                    </div>
                </div>
            </div>
            <div class="progress-arc-info">
                @include("$VIEW_ROOT.clients.profile._avatar")
            </div>

            <div class="progress-arc-summary-info">
                <p class="progress-arc-summary-subtitle" contenteditable="true">{{ $user->name }}</p>
                <p class="progress-arc-summary-subtitle ltr">@<span contenteditable="true">{{ $user->nickname }}</span></p>

                {{--    <p class="progress-arc-summary-text">Complete your profile by filling profile info fields,--}}
                {{--        completing quests &amp; unlocking badges</p>--}}
            </div>
        </div>


        {{--    @include("$VIEW_ROOT.clients.profile._sidebar_status")--}}

        <div class="sidebar-box-footer">
            <p class="button white small-space">Secure Logout</p>
        </div>
    </div>
</div>
