<div class="widget-box">
    {{--    @include("$VIEW_ROOT.clients.profile._widget_settings")--}}

    <p class="widget-box-title">اطلاعات شخصی</p>
    <p class="widget-box-description">این اطلاعات محرمانه است و فقط برای ارتباط با شما استفاده می‌شود. اما در سایت نمایش داده <b>نمی‌شود</b></p>

    <div class="widget-box-content">
        <div class="information-line-list">
            <div class="information-line">
                <p class="information-line-title">شناسه کاربری</p>
                <p class="information-line-text ltr" data-col="username" data-cur="{{ $user->username }}" spellcheck="false" contenteditable="true">{{ $user->username }}</p>
                <span class="error-box" data-col="username"><i class="fas fa-times-circle"></i> <span></span></span>
            </div>
            <div class="information-line">
                <p class="information-line-title">شماره موبایل</p>
                <p class="information-line-text ltr" data-col="mobile" data-cur="{{ $user->mobile }}" contenteditable="true">{{ $user->mobile ?? 'ثبت کنید'}}</p>
                <span class="error-box" data-col="mobile"><i class="fas fa-times-circle"></i> <span></span></span>
                @if(!$user->mobile_verified_at)
                    <p class="information-line-alt">دریافت کد تایید</p>
                @endif
            </div>
            <div class="information-line">
                <p class="information-line-title">ایمیل</p>
                <p class="information-line-text ltr" data-col="email" data-cur="{{ $user->email }}" contenteditable="true">{{ $user->email ?? 'ثبت کنید'}}</p>
                <span class="error-box" data-col="email"><i class="fas fa-times-circle"></i> <span></span></span>
                @if(!$user->email_verified_at)
                    <p class="information-line-alt">دریافت کد تایید</p>
                @endif
            </div>
        </div>
    </div>
</div>
