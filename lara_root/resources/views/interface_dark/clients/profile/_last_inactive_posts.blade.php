<section class="section">
    <div class="section-header">
        <div class="section-header-info">
            <h2 class="section-title">مقالات غیرفعال</h2>
        </div>
    </div>
    <div class="grid grid-3-3-3-3 centered-on-mobile">
        @foreach($user->author->inctiveArticles() as $article)
            @include("$VIEW_ROOT.clients.profile.posts.base",['ARTICLE' => $article])
        @endforeach
    </div>
</section>
