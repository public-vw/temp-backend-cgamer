<div class="widget-box">
    {{--                @include("$VIEW_ROOT.clients.profile._widget_settings")--}}

    <p class="widget-box-title">Jobs &amp; Education</p>

    <div class="widget-box-content">
        <div class="timeline-information-list">
            <div class="timeline-information">
                <p class="timeline-information-title">Lead Costume Designer</p>

                <p class="timeline-information-date">2015 - NOW</p>

                <p class="timeline-information-text">Lead Costume Designer for the "Amazzo Costumes" agency.
                    I'm in charge of a ten person group, overseeing all the proyects and talking to
                    potential clients. I also handle some face to face interviews for new candidates.</p>
            </div>

            <div class="timeline-information">
                <p class="timeline-information-title">Costume Designer</p>

                <p class="timeline-information-date">2013 - 2015</p>

                <p class="timeline-information-text">Costume Designer for the "Jenny Taylors" agency. Was in
                    charge of working side by side with the best designers in order to complete and perfect
                    orders.</p>
            </div>

            <div class="timeline-information">
                <p class="timeline-information-title">Designer Intern</p>

                <p class="timeline-information-date">2012 - 2013</p>

                <p class="timeline-information-text">Intern for the "Jenny Taylors" agency. Was in charge of
                    the communication with the clients and day-to-day chores.</p>
            </div>

            <div class="timeline-information">
                <p class="timeline-information-title">The Antique College of Design</p>

                <p class="timeline-information-date">2007 - 2012</p>

                <p class="timeline-information-text">Bachelor of Costume Design in the Antique College. It
                    was a five years intensive career, plus a course about Cosplays. Average: A+</p>
            </div>
        </div>
    </div>
</div>
