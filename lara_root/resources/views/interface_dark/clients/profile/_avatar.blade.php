<div class="user-short-description-avatar user-avatar big avatar-popup-trigger">
    <div class="user-avatar-border">
        <div class="hexagon-148-164"></div>
    </div>

    <div class="user-avatar-content">
        <div class="hexagon-image-100-110"
             data-src="{{ $user->lastImage('avatar') ?? asset("assets/$VIEW_ROOT/img/avatar/01.jpg") }}"></div>
    </div>

    <div class="user-avatar-progress">
        <div class="hexagon-progress-124-136"></div>
    </div>

    <div class="user-avatar-progress-border">
        <div class="hexagon-border-124-136"></div>
    </div>

{{--    <div class="user-avatar-badge">--}}
{{--        <div class="user-avatar-badge-border">--}}
{{--            <div class="hexagon-40-44"></div>--}}
{{--        </div>--}}

{{--        <div class="user-avatar-badge-content">--}}
{{--            <div class="hexagon-dark-32-34"></div>--}}
{{--        </div>--}}

{{--        <p class="user-avatar-badge-text">24</p>--}}
{{--    </div>--}}
</div>
