<div class="widget-box">
    {{--    @include("$VIEW_ROOT.clients.profile._widget_settings")--}}

    <p class="widget-box-title">ارتباط با شبکه‌های اجتماعی</p>
    <p class="widget-box-description">با اتصال اکانت خود به شبکه‌های اجتماعی، می‌توتی بدون نیاز به <b>حفظ کردن و وارد کردن کلمه عبور</b> توی سایت لاگین کنی<br>و از ابزارهای تبادل اطلاعات با اکانت خودت بهره ببری.</p>

    <div class="widget-box-content">
        <div class="information-line-list">

            <div class="social-links">
                @if(!$user->oauth || !$user->oauth->twitch_id)
                <a class="social-link twitch text-tooltip-tft" data-title="Twitch" href="{{ route('auth.twitch.redirect') }}">
                    <svg class="icon-twitch">
                        <use xlink:href="#svg-twitch"></use>
                    </svg>
                </a>
                @endif

                @if(!$user->oauth || !$user->oauth->instagram_id)
                <a class="social-link instagram text-tooltip-tft" data-title="Instagram" href="{{ route('auth.instagram.redirect') }}">
                    <svg class="icon-instagram">
                        <use xlink:href="#svg-instagram"></use>
                    </svg>
                </a>
                @endif

                @if(!$user->oauth || !$user->oauth->twitter_id)
                <a class="social-link twitter text-tooltip-tft" data-title="Twitter" href="{{ route('auth.twitter.redirect') }}">
                    <svg class="icon-twitter">
                        <use xlink:href="#svg-twitter"></use>
                    </svg>
                </a>
                @endif

                @if(!$user->oauth || !$user->oauth->facebook_id)
                <a class="social-link facebook text-tooltip-tft" data-title="Facebook" href="{{ route('auth.facebook.redirect') }}">
                    <svg class="icon-facebook">
                        <use xlink:href="#svg-facebook"></use>
                    </svg>
                </a>
                @endif

                @if(!$user->oauth || !$user->oauth->telegram_id)
                <a class="social-link telegram text-tooltip-tft" data-title="Telegram" href="{{ route('auth.telegram.redirect') }}">
                    <svg class="icon-telegram">
                        <use xlink:href="#svg-send-message"></use>
                    </svg>
                </a>
                @endif

                @if(!$user->oauth || !$user->oauth->discord_id)
                <a class="social-link discord text-tooltip-tft" data-title="Discord" href="{{ route('auth.discord.redirect') }}">
                    <svg class="icon-discord">
                        <use xlink:href="#svg-discord"></use>
                    </svg>
                </a>
                @endif

                @if(!$user->oauth || !$user->oauth->google_id)
                <a class="social-link googlew text-tooltip-tft" data-title="Google/Gmail" href="{{ route('auth.google.redirect') }}">
                    <img src="{{ asset('assets/interface_dark/img/socials/googlew.png') }}" alt="google_oauth">
                </a>
                @endif

                {{--    <a class="social-link steam" href="{{ route('auth.steam.redirect') }}">--}}
                {{--        <svg class="icon-steam">--}}
                {{--            <use xlink:href="#svg-steam"></use>--}}
                {{--        </svg>--}}
                {{--    </a>--}}




                {{--    <a class="social-link youtube" href="#">--}}
                {{--        <svg class="icon-youtube">--}}
                {{--            <use xlink:href="#svg-youtube"></use>--}}
                {{--        </svg>--}}
                {{--    </a>--}}
            </div>


        </div>
    </div>

    @includeWhen($user->oauth && $user->oauth->hasAny(),"$VIEW_ROOT.clients.profile._social_accounts_connected")
</div>
