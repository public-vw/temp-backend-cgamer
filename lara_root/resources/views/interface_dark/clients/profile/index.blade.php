@extends("$VIEW_ROOT.layouts.base",[
        'FILE_NAME' => 'marketplace-product',
        'PAGE_TITLE' => 'Profile',
    ])

@section('page_title','پروفایل اعضا')

@push('css')
    <link rel="stylesheet" href="{{ asset('assets/interface_dark/clients/css/profile.css') }}">
@endpush

@push('js')
    <script src="{{ asset('assets/interface/clients/js/profile.js') }}"></script>
    <script>
        //removeAfterCreation
        window.routes = {};
        window.routes['clients_profile'] = '{{ route('client.profile.ajax.update') }}';
    </script>
@endpush

@php( $wallet = '100K' )

@section('content')

<div class="content-grid full">
{{--        @include("$VIEW_ROOT.clients.profile._header")--}}
    {{--    @include("$VIEW_ROOT.clients.profile._navigation_bar") --}}

{{--    @include("$VIEW_ROOT.clients.profile._avatar_selector")--}}

    <div class="grid grid-3-9">
        <div class="grid-column">
            @includeWhen($user->completion < 100,"$VIEW_ROOT.clients.profile._sidebar_progress",['PERCENT' => $user->completion])
            @includeWhen($user->completion >= 100,"$VIEW_ROOT.clients.profile._sidebar_wallet",['AMOUNT' => $wallet])

            {{--            @include("$VIEW_ROOT.clients.profile._more_stats")--}}
        </div>
        <div class="grid-column">
            @include("$VIEW_ROOT.clients.profile._about_me")

            @include("$VIEW_ROOT.clients.profile._personal_info")

            @include("$VIEW_ROOT.clients.profile._social_accounts")
        </div>
        <div class="grid-column save-action" data-url="clients_profile" style="position: sticky; bottom: 0; display: none">
            <div class="sidebar-box">
                <p class="button white white-solid">ذخیره تغییرات</p>
            </div>
        </div>

{{--        <div class="grid-column">--}}
{{--            @include("$VIEW_ROOT.clients.profile._interests")--}}
{{--            @include("$VIEW_ROOT.clients.profile._jobs")--}}
{{--        </div>--}}

    </div>
    @includeWhen($user->author->latestActiveArticles()->count(),"$VIEW_ROOT.clients.profile._last_posts")
    @includeWhen($user->author->inctiveArticles()->count(),"$VIEW_ROOT.clients.profile._last_inactive_posts")

</div>

@endsection
