<hr style="outline: 1px solid #3e475b; margin:20px 0"/>
<p class="widget-box-title" >شبکه‌های اجتماعی متصل به اکانت شما</p>
<p class="widget-box-description">در حال حاضر به این اکانت‌ها <b>متصل هستی</b> و می‌تونی با اونا لاگین کنی</p>

<div class="widget-box-content">
    <div class="information-line-list">

        <div class="social-links">
            @if($user->oauth->twitch_id)
            <p class="social-link twitch text-tooltip-tft" data-title="Twitch">
                <svg class="icon-twitch">
                    <use xlink:href="#svg-twitch"></use>
                </svg>
            </p>
            @endif

            @if($user->oauth->instagram_id)
            <p class="social-link instagram text-tooltip-tft" data-title="Instagram">
                <svg class="icon-instagram">
                    <use xlink:href="#svg-instagram"></use>
                </svg>
            </p>
            @endif

            @if($user->oauth->twitter_id)
            <p class="social-link twitter text-tooltip-tft" data-title="Twitter">
                <svg class="icon-twitter">
                    <use xlink:href="#svg-twitter"></use>
                </svg>
            </p>
            @endif

            @if($user->oauth->facebook_id)
            <p class="social-link facebook text-tooltip-tft" data-title="Facebook">
                <svg class="icon-facebook">
                    <use xlink:href="#svg-facebook"></use>
                </svg>
            </p>
            @endif

            @if($user->oauth->telegram_id)
            <p class="social-link telegram text-tooltip-tft" data-title="Telegram">
                <svg class="icon-telegram">
                    <use xlink:href="#svg-send-message"></use>
                </svg>
            </p>
            @endif

            @if($user->oauth->discord_id)
            <p class="social-link discord text-tooltip-tft" data-title="Discord">
                <svg class="icon-discord">
                    <use xlink:href="#svg-discord"></use>
                </svg>
            </p>
            @endif

            @if($user->oauth->google_id)
            <p class="social-link googlew text-tooltip-tft" data-title="Google/Gmail">
                <img src="{{ asset('assets/interface_dark/img/socials/googlew.png') }}" alt="google_oauth">
            </p>
            @endif

            {{--    <a class="social-link steam">--}}
            {{--        <svg class="icon-steam">--}}
            {{--            <use xlink:href="#svg-steam"></use>--}}
            {{--        </svg>--}}
            {{--    </a>--}}




            {{--    <a class="social-link youtube" href="#">--}}
            {{--        <svg class="icon-youtube">--}}
            {{--            <use xlink:href="#svg-youtube"></use>--}}
            {{--        </svg>--}}
            {{--    </a>--}}
        </div>


    </div>
</div>
