<div class="profile-header">
    <figure class="profile-header-cover liquid">
        <img src="{{ asset("assets/$VIEW_ROOT/img/cover/01.jpg") }}" alt="cover-01">
    </figure>

    <div class="profile-header-info">
        <div class="user-short-description big">
            <a class="user-short-description-avatar user-avatar big" href="profile-timeline.html">
                <div class="user-avatar-border">
                    <div class="hexagon-148-164"></div>
                </div>

                <div class="user-avatar-content">
                    <div class="hexagon-image-100-110"
                         data-src="{{ asset("assets/$VIEW_ROOT/img/avatar/01.jpg") }}"></div>
                </div>

                <div class="user-avatar-progress">
                    <div class="hexagon-progress-124-136"></div>
                </div>

                <div class="user-avatar-progress-border">
                    <div class="hexagon-border-124-136"></div>
                </div>

                <div class="user-avatar-badge">
                    <div class="user-avatar-badge-border">
                        <div class="hexagon-40-44"></div>
                    </div>

                    <div class="user-avatar-badge-content">
                        <div class="hexagon-dark-32-34"></div>
                    </div>

                    <p class="user-avatar-badge-text">24</p>
                </div>
            </a>

            <a class="user-short-description-avatar user-short-description-avatar-mobile user-avatar medium"
               href="profile-timeline.html">
                <div class="user-avatar-border">
                    <div class="hexagon-120-132"></div>
                </div>

                <div class="user-avatar-content">
                    <div class="hexagon-image-82-90"
                         data-src="{{ asset("assets/$VIEW_ROOT/img/avatar/01.jpg") }}"></div>
                </div>

                <div class="user-avatar-progress">
                    <div class="hexagon-progress-100-110"></div>
                </div>

                <div class="user-avatar-progress-border">
                    <div class="hexagon-border-100-110"></div>
                </div>

                <div class="user-avatar-badge">
                    <div class="user-avatar-badge-border">
                        <div class="hexagon-32-36"></div>
                    </div>

                    <div class="user-avatar-badge-content">
                        <div class="hexagon-dark-26-28"></div>
                    </div>

                    <p class="user-avatar-badge-text">24</p>
                </div>
            </a>

            <p class="user-short-description-title"><a href="profile-timeline.html">Marina Valentine</a></p>

            <p class="user-short-description-text"><a href="#">www.gamehuntress.com</a></p>
        </div>

        <div class="profile-header-social-links-wrap">
            <div id="profile-header-social-links-slider" class="profile-header-social-links">
                <div class="profile-header-social-link">
                    <a class="social-link facebook" href="#">
                        <svg class="icon-facebook">
                            <use xlink:href="#svg-facebook"></use>
                        </svg>
                    </a>
                </div>

                <div class="profile-header-social-link">
                    <a class="social-link twitter" href="#">
                        <svg class="icon-twitter">
                            <use xlink:href="#svg-twitter"></use>
                        </svg>
                    </a>
                </div>

                <div class="profile-header-social-link">
                    <a class="social-link instagram" href="#">
                        <svg class="icon-instagram">
                            <use xlink:href="#svg-instagram"></use>
                        </svg>
                    </a>
                </div>

                <div class="profile-header-social-link">
                    <a class="social-link twitch" href="#">
                        <svg class="icon-twitch">
                            <use xlink:href="#svg-twitch"></use>
                        </svg>
                    </a>
                </div>

                <div class="profile-header-social-link">
                    <a class="social-link youtube" href="#">
                        <svg class="icon-youtube">
                            <use xlink:href="#svg-youtube"></use>
                        </svg>
                    </a>
                </div>

                <div class="profile-header-social-link">
                    <a class="social-link patreon" href="#">
                        <svg class="icon-patreon">
                            <use xlink:href="#svg-patreon"></use>
                        </svg>
                    </a>
                </div>

                <div class="profile-header-social-link">
                    <a class="social-link discord" href="#">
                        <svg class="icon-discord">
                            <use xlink:href="#svg-discord"></use>
                        </svg>
                    </a>
                </div>
            </div>

            <div id="profile-header-social-links-slider-controls" class="slider-controls">
                <div class="slider-control left">
                    <svg class="slider-control-icon icon-small-arrow">
                        <use xlink:href="#svg-small-arrow"></use>
                    </svg>
                </div>

                <div class="slider-control right">
                    <svg class="slider-control-icon icon-small-arrow">
                        <use xlink:href="#svg-small-arrow"></use>
                    </svg>
                </div>
            </div>
        </div>

        <div class="user-stats">
            <div class="user-stat big">
                <p class="user-stat-title">930</p>

                <p class="user-stat-text">posts</p>
            </div>

            <div class="user-stat big">
                <p class="user-stat-title">82</p>

                <p class="user-stat-text">friends</p>
            </div>

            <div class="user-stat big">
                <p class="user-stat-title">5.7k</p>

                <p class="user-stat-text">visits</p>
            </div>

            <div class="user-stat big">
                <img class="user-stat-image" src="{{ asset("assets/$VIEW_ROOT/img/flag/usa.png") }}" alt="flag-usa">

                <p class="user-stat-text">usa</p>
            </div>
        </div>

        <div class="profile-header-info-actions">
            <p class="profile-header-info-action button secondary"><span class="hide-text-mobile">Add</span> Friend
                +</p>

            <p class="profile-header-info-action button primary"><span class="hide-text-mobile">Send</span> Message
            </p>
        </div>
    </div>
</div>
