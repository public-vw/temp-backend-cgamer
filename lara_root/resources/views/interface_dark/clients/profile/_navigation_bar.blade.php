<nav class="section-navigation">
    <div id="section-navigation-slider" class="section-menu">
        <a class="section-menu-item active" href="profile-about.html">
            <svg class="section-menu-item-icon icon-profile">
                <use xlink:href="#svg-profile"></use>
            </svg>

            <p class="section-menu-item-text">About</p>
        </a>

        <a class="section-menu-item" href="profile-timeline.html">
            <svg class="section-menu-item-icon icon-timeline">
                <use xlink:href="#svg-timeline"></use>
            </svg>

            <p class="section-menu-item-text">Timeline</p>
        </a>

        <a class="section-menu-item" href="profile-friends.html">
            <svg class="section-menu-item-icon icon-friend">
                <use xlink:href="#svg-friend"></use>
            </svg>

            <p class="section-menu-item-text">Friends</p>
        </a>

        <a class="section-menu-item" href="profile-groups.html">
            <svg class="section-menu-item-icon icon-group">
                <use xlink:href="#svg-group"></use>
            </svg>

            <p class="section-menu-item-text">Groups</p>
        </a>

        <a class="section-menu-item" href="profile-photos.html">
            <svg class="section-menu-item-icon icon-photos">
                <use xlink:href="#svg-photos"></use>
            </svg>

            <p class="section-menu-item-text">Photos</p>
        </a>

        <a class="section-menu-item" href="profile-videos.html">
            <svg class="section-menu-item-icon icon-videos">
                <use xlink:href="#svg-videos"></use>
            </svg>

            <p class="section-menu-item-text">Videos</p>
        </a>

        <a class="section-menu-item" href="profile-badges.html">
            <svg class="section-menu-item-icon icon-badges">
                <use xlink:href="#svg-badges"></use>
            </svg>

            <p class="section-menu-item-text">Badges</p>
        </a>

        <a class="section-menu-item" href="profile-stream.html">
            <svg class="section-menu-item-icon icon-streams">
                <use xlink:href="#svg-streams"></use>
            </svg>

            <p class="section-menu-item-text">Streams</p>
        </a>

        <a class="section-menu-item" href="profile-blog.html">
            <svg class="section-menu-item-icon icon-blog-posts">
                <use xlink:href="#svg-blog-posts"></use>
            </svg>

            <p class="section-menu-item-text">Blog</p>
        </a>

        <a class="section-menu-item" href="profile-forum.html">
            <svg class="section-menu-item-icon icon-forum">
                <use xlink:href="#svg-forum"></use>
            </svg>

            <p class="section-menu-item-text">Forum</p>
        </a>

        <a class="section-menu-item" href="profile-store.html">
            <svg class="section-menu-item-icon icon-store">
                <use xlink:href="#svg-store"></use>
            </svg>

            <p class="section-menu-item-text">Store</p>
        </a>
    </div>

    <div id="section-navigation-slider-controls" class="slider-controls">
        <div class="slider-control left">
            <svg class="slider-control-icon icon-small-arrow">
                <use xlink:href="#svg-small-arrow"></use>
            </svg>
        </div>

        <div class="slider-control right">
            <svg class="slider-control-icon icon-small-arrow">
                <use xlink:href="#svg-small-arrow"></use>
            </svg>
        </div>
    </div>
</nav>
