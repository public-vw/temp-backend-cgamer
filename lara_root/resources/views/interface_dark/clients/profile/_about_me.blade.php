<div class="widget-box">

{{--    @include("$VIEW_ROOT.clients.profile._widget_settings")--}}

    <p class="widget-box-title">درباره من</p>

    <div class="widget-box-content">
        <p class="paragraph editable" data-col="about_me" data-cur="{{ $user->meta->about_me ?? '' }}" contenteditable data-placeholder="برای تغییر این پاراگراف، اینجا کلیک کن">{{ $user->meta->about_me ?? '' }}</p>
        <span class="error-box" data-col="about_me"><i class="fas fa-times-circle"></i> <span></span></span>

        <div class="information-line-list">
            <div class="information-line">
                <p class="information-line-title">شروع عضویت</p>
                <p class="information-line-text">{{ $user->created_at->diffForHumans() }}</p>
            </div>

            <div class="information-line">
                <p class="information-line-title">لینک معرفی</p>
                <p class="information-line-text ltr"><span style="color:#999">{{config('app.url')}}/</span><span class="editable" data-col="ref_code" data_cur="{{ $user->ref_code ?? '' }}" contenteditable data-placeholder="کلیک کن">{{ $user->ref_code ?? '' }}</span>
                    <span class="error-box" data-col="ref_code"><i class="fas fa-times-circle"></i> <span></span></span>
                </p>
            </div>

{{--            <div class="information-line">--}}
{{--                <p class="information-line-title">تاریخ تولد <b>میلادی</b></p>--}}
{{--                <p class="information-line-text"><input type="date" value="{{ $user->meta->birthdate ?? '' }}"/></p>--}}
{{--            </div>--}}
{{--            <div class="information-line">--}}
{{--                <p><a href="https://www.bahesab.ir/time/conversion/" rel="nofollow" target="_blank">تبدیل آنلاین شمسی به میلادی</a></p>--}}
{{--            </div>--}}

{{--            <div class="information-line">--}}
{{--                <p class="information-line-title">وب‌سایت</p>--}}
{{--                <p class="information-line-text" contenteditable="true">برای ثبت وب‌سایتت کلیک کن</p>--}}
{{--            </div>--}}
        </div>
    </div>
</div>
