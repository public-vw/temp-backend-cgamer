<div class="achievement-status-list">
    <div class="achievement-status">
        <p class="achievement-status-progress">11/30</p>

        <div class="achievement-status-info">
            <p class="achievement-status-title">Quests</p>

            <p class="achievement-status-text">Completed</p>
        </div>

        <img class="achievement-status-image" src="{{ asset("assets/$VIEW_ROOT/img/badge/completedq-s.png") }}"
             alt="bdage-completedq-s">
    </div>

    <div class="achievement-status">
        <p class="achievement-status-progress">22/46</p>

        <div class="achievement-status-info">
            <p class="achievement-status-title">Badges</p>

            <p class="achievement-status-text">Unlocked</p>
        </div>

        <img class="achievement-status-image" src="{{ asset("assets/$VIEW_ROOT/img/badge/unlocked-badge.png") }}"
             alt="bdage-unlocked-badge">
    </div>

</div>
