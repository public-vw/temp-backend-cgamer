<div class="account-hub-sidebar">
    <div class="sidebar-box">
        <div class="progress-arc-summary">
            <div class="progress-arc-wrap">
                <div class="progress-arc">
                    <canvas id="profile-completion-chart" data-percent="{{$PERCENT}}"></canvas>
                </div>

                <div class="progress-arc-info">
                    <p class="progress-arc-title">{{$PERCENT}}%</p>
                </div>
            </div>

            <div class="progress-arc-summary-info">
                <p class="progress-arc-summary-title mb-3">درصد تکمیل پروفایل</p>

                @include("$VIEW_ROOT.clients.profile._avatar_small")
                <p class="progress-arc-summary-subtitle" data-col="nickname" data-cur="{{ $user->nickname }}" spellcheck="false" contenteditable="true">
                    {{ $user->nickname }}
                </p>
                <span class="error-box" data-col="nickname"><i class="fas fa-times-circle"></i> <span></span></span>

{{--                <p class="progress-arc-summary-text">برای استفاده از تمام امکانات سایت، مدارک خودت رو ثبت کن</p>--}}
            </div>


        </div>

        {{--    @include("$VIEW_ROOT.clients.profile._sidebar_status")--}}

        <div class="sidebar-box-footer">
{{--            <p class="button primary">تکمیل مدارک</p>--}}

            <a class="button white small-space" href="{{ route('auth.logout') }}" onclick="return confirm('آیا مطمئن هستید؟')">خروج از اکانت</a>
{{--            <p class="button white white-solid small-space save-action" data-url="clients_profile" style="display: none">ذخیره تغییرات</p>--}}
        </div>
    </div>
</div>



