<div class="widget-box">
    {{--    @include("$VIEW_ROOT.clients.profile._widget_settings")--}}

    <p class="widget-box-title">Interests</p>

    <div class="widget-box-content">
        <div class="information-block-list">
            <div class="information-block">
                <p class="information-block-title">Favourite TV Shows</p>

                <p class="information-block-text">Breaking Good, RedDevil, People of Interest, The Running
                    Dead, Found, American Guy, The Last Windbender, Game of Wars.</p>
            </div>

            <div class="information-block">
                <p class="information-block-title">Favourite Music Bands / Artists</p>

                <p class="information-block-text">Iron Maid, DC/AC, Megablow, Kung Fighters, System of a
                    Revenge, Rammstown.</p>
            </div>

            <div class="information-block">
                <p class="information-block-title">Favourite Movies</p>

                <p class="information-block-text">The Revengers Saga, The Scarred Wizard and the Fire Crown,
                    Crime Squad, Metal Man, The Dark Rider, Watchers, The Impossible Heist.</p>
            </div>

            <div class="information-block">
                <p class="information-block-title">Favourite Books</p>

                <p class="information-block-text">The Crime of the Century, Egiptian Mythology 101, The
                    Scarred Wizard, Lord of the Wings, Amongst Gods, The Oracle, A Tale of Air and
                    Water.</p>
            </div>

            <div class="information-block">
                <p class="information-block-title">Favourite Games</p>

                <p class="information-block-text">The First of Us, Assassin’s Squad, Dark Assylum, NMAK16,
                    Last Cause 4, Grand Snatch Auto.</p>
            </div>
        </div>
    </div>
</div>
