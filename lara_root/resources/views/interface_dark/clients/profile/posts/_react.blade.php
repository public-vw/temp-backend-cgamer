<div class="meta-line">
    <div class="meta-line-list user-avatar-list">
        <div class="user-avatar micro no-stats">
            <div class="user-avatar-border">
                <div class="hexagon-22-24"></div>
            </div>

            <div class="user-avatar-content">
                <div class="hexagon-image-18-20" data-src="{{ asset("assets/$VIEW_ROOT/img/avatar/06.jpg") }}"></div>
            </div>
        </div>

        <div class="user-avatar micro no-stats">
            <div class="user-avatar-border">
                <div class="hexagon-22-24"></div>
            </div>

            <div class="user-avatar-content">
                <div class="hexagon-image-18-20" data-src="{{ asset("assets/$VIEW_ROOT/img/avatar/07.jpg") }}"></div>
            </div>
        </div>

        <div class="user-avatar micro no-stats">
            <div class="user-avatar-border">
                <div class="hexagon-22-24"></div>
            </div>

            <div class="user-avatar-content">
                <div class="hexagon-image-18-20" data-src="{{ asset("assets/$VIEW_ROOT/img/avatar/02.jpg") }}"></div>
            </div>
        </div>

        <div class="user-avatar micro no-stats">
            <div class="user-avatar-border">
                <div class="hexagon-22-24"></div>
            </div>

            <div class="user-avatar-content">
                <div class="hexagon-image-18-20" data-src="{{ asset("assets/$VIEW_ROOT/img/avatar/10.jpg") }}"></div>
            </div>
        </div>

        <div class="user-avatar micro no-stats">
            <div class="user-avatar-border">
                <div class="hexagon-22-24"></div>
            </div>

            <div class="user-avatar-content">
                <div class="hexagon-image-18-20" data-src="{{ asset("assets/$VIEW_ROOT/img/avatar/05.jpg") }}"></div>
            </div>
        </div>
    </div>

    <p class="meta-line-text">+24 will assist</p>
</div>
