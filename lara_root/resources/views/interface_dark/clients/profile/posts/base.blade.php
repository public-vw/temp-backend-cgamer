<div class="event-preview">
    @include("$VIEW_ROOT.clients.profile.posts._image",[
        'IMAGE' => $ARTICLE->lastImage('article_header') ?? ($ARTICLE->category ? $ARTICLE->category->lastImage('article_default_header'):null) ?? asset("assets/$VIEW_ROOT/img/cover/19.jpg"),
        'ALT' => $ARTICLE->seo()['google']->title ?? $ARTICLE->heading
    ])
    @role('admin@web|author_supervisor@web')
        <div class="admin-buttons">
            <a href="{{ route('admin.articles.edit',['article' => $ARTICLE]) }}" class="button float-left px-3" target="_blank">ویرایش در پنل</a>
        </div>
    @endrole

    <div class="event-preview-info">
        <div class="event-preview-info-top">
{{--            @include("$VIEW_ROOT.clients.profile.posts._date")--}}

            @include("$VIEW_ROOT.clients.profile.posts._heading",[
                        'HEADING' => $ARTICLE->heading,
                        'LINK' => $ARTICLE->seoUrl(),
                        ])
            <p class="event-preview-text">{{ config_trans('enums.articles_status',$ARTICLE->status) }}</p>

            <p class="event-preview-text">{!! $ARTICLE->summary ?? '' !!}</p>
        </div>

        <div class="event-preview-info-bottom">

{{--            @include("$VIEW_ROOT.clients.profile.posts._text_icon")--}}

{{--            @include("$VIEW_ROOT.clients.profile.posts._react")--}}

            <a class="button white white-tertiary" target="_blank" href="{{ route("client.articles.single.preview", $ARTICLE) }}">پیش نمایش</a>
        </div>
    </div>
</div>
