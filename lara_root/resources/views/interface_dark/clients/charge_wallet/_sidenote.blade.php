<div class="grid-column">
    <div class="sidebar-box">
        <p class="sidebar-box-title">اطلاعات حساب جهت واریز وجه</p>

        <div class="sidebar-box-items">
            <div class="totals-line-list separator-bottom">
                <div class="totals-line">
                    <div class="totals-line-info">
                        <p class="totals-line-title"><span class="bold">شماره حساب</span></p>
                    </div>
                    <p class="price-title"><span class="currency">279-8000-14427936-2</span></p>
{{--                    <p class="price-title"><span class="currency">299-8000-587581-1</span></p>--}}
                </div>
                <div class="totals-line">
                    <div class="totals-line-info">
                        <p class="totals-line-title"><span class="bold">شماره شبا</span></p>
                    </div>
                    <p class="price-title"><span class="currency">IR330570027980014427936102</span></p>
{{--                    <p class="price-title"><span class="currency">IR830570029980000587581001</span></p>--}}
                </div>
            </div>

            <div class="totals-line-list">
                <div class="totals-line">
                    <div class="totals-line-info">
                        <p class="totals-line-title"><span class="bold">به نام</span></p>
                    </div>

                    <p class="price-title"><span class="currency">آقای یوسف یاس</span></p>
{{--                    <p class="price-title"><span class="currency">آقای آرمین قاسمی</span></p>--}}
                </div>
            </div>

        </div>

    </div>
</div>
