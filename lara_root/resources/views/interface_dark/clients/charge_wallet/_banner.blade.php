<div class="section-banner">
    <img class="section-banner-icon" src="{{ asset("assets/$VIEW_ROOT/img/banner/marketplace-icon.png") }}" alt="marketplace-icon">

    <p class="section-banner-title">کیف پول خودت رو شارژ کن</p>

    <p class="section-banner-text">پرداخت با کیف پول سریع و مطمئنه</p>
</div>
