@extends("$VIEW_ROOT.layouts.base",[
        'PAGE_TITLE' => 'Charge Wallet',
        'NO_HEADER' => true,
    ])

@push('js')
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
{{--    <script src="{{ asset("assets/$VIEW_ROOT/js/jalaly-date.js") }}"></script>--}}
{{--<script type="text/javascript">--}}
{{--    $(document).ready(function() {--}}
{{--        $("#charge-wallet-date").pDatepicker();--}}
{{--    });--}}
{{--</script>--}}

    <script src="{{ asset("assets/$VIEW_ROOT/js/tools-charge-wallet.js") }}"></script>
    <script>
        //removeAfterCreation
        window.routes = window.routes ? window.routes : {};
        window.routes['ajax_update_params'] = '{{ route('client.tools.exchange.update_parameters') }}';
        window.routes['upload_image_inside'] = '{{ route('client.attachments.upload.by_data',['type' => 'payment_paper']) }}';
    </script>
@endpush

@section('content')

    <div class="content-grid">

        @include("$VIEW_ROOT.clients.charge_wallet._banner")

        <div class="grid grid-6-6 small-space">
            @include("$VIEW_ROOT.clients.charge_wallet._sidenote")

            <div class="grid-column">
                <div class="widget-box" id="tools-charge-wallet">

                    <div class="widget-box-content">
                        <form class="form">

                            <div class="form-row split">
                                <div class="form-item">
                                    <div class="form-input" v-bind:class="{'active': form.date != null }">
                                        <label for="charge-wallet-date">تاریخ واریز</label>
                                        <input type="text" id="charge-wallet-date" v-model="form.date" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-item">
                                    <div class="form-input" v-bind:class="{'active': form.amount != null }">
                                        <label for="charge-wallet-amount">مبلغ (تومان)</label>
                                        <input type="text" id="charge-wallet-amount" v-model="form.amount" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-item">
                                    <div class="form-input">
                                        <input type="file" id="uploader" class="not-visible" @change="readImage" />
                                        <label for="charge-wallet-file">تصویر فیش واریزی</label>
                                        <input type="text" id="charge-wallet-file" onclick="javascript:document.querySelector('#uploader').click(); this.blur()"/>

                                        <div class="page-loader-indicator loader-bars" v-if="loading">
                                            <div class="loader-bar"></div>
                                            <div class="loader-bar"></div>
                                            <div class="loader-bar"></div>
                                            <div class="loader-bar"></div>
                                            <div class="loader-bar"></div>
                                            <div class="loader-bar"></div>
                                        </div>
                                        <img v-bind:src="form.file" v-if="form.file !== null" id="payment-image" v-show="!loading"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-item">

                                    <p class="widget-box-title">متد واریز</p>

                                    <div class="widget-box-content small-margin-top">

                                        <div class="form-row">
                                            <div class="checkbox-wrap">
                                                <input type="radio" id="payment-method-direct" name="payment-method" value="payment-method-direct" checked>
                                                <div class="checkbox-box round"></div>
                                                <label class="accordion-trigger-linked"
                                                       for="payment-method-direct">واریز مستقیم به حساب</label>

                                                <div class="checkbox-info accordion-content-linked accordion-open">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <div class="checkbox-wrap">
                                                <input type="radio" id="payment-method-paya" name="payment-method"
                                                       value="payment-method-paya">
                                                <div class="checkbox-box round"></div>
                                                <label class="accordion-trigger-linked" for="payment-method-paya">پایا / ساتنا</label>

                                                <div class="checkbox-info accordion-content-linked">
                                                    <p class="checkbox-info-text">با این متد، کیف پول پس از دریافت وجه شارژ خواهد شد</p>
                                                </div>

                                            </div>
                                        </div>

                                    </div>






                                </div>
                            </div>

                            <div class="switch-option">
                                <a class="button twitter" href="#">
                                    <svg class="button-icon spaced icon-check">
                                        <use xlink:href="#svg-check"></use>
                                    </svg>
                                    ثبت سند واریز
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>


@endsection
