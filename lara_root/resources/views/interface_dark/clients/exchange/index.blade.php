@extends("$VIEW_ROOT.layouts.base",[
        'PAGE_TITLE' => 'Exchange Tools',
        'NO_HEADER' => true,
    ])

@push('js')
    <script src="{{ asset("assets/$VIEW_ROOT/js/tools-exchange.js") }}"></script>

    <script>
        //removeAfterCreation
        window.routes = window.routes ? window.routes : {};
        window.routes['ajax_update_params'] = '{{ route('client.tools.exchange.update_parameters') }}';
    </script>
@endpush

@section('content')

    <div class="content-grid">

        @include("$VIEW_ROOT.clients.exchange._banner")

        <div class="section-header">
            <div class="section-header-info">
                <p class="section-pretitle">ثبت سفارش</p>

                <h2 class="section-title">خرید WETH و شارژ Ronin Wallet</h2>
            </div>
        </div>

        <div class="grid grid-8-4 small-space" id="tools-exchange">
            <div class="grid-column">
                <div class="widget-box">

                    <div class="widget-box-content">
                        <form class="form">

                            <div class="form-row">
                                <div class="form-item">
                                    <div class="form-input" v-bind:class="{'active': form.address != '' }">
                                        <label for="wallet-address">آدرس ronin wallet</label>
                                        <input type="text" id="wallet-address" v-model="form.address" class="ltr" autofocus>
                                        <p class="mt-2 mr-2">آدرس Ronin با "ronin:" شروع می‌شه که بهتره از کیف پول Ronin خودت کپی کنی</p>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-item">
                                    <div class="form-input" v-bind:class="{'active': form.amount != '' }">
                                        <label for="amounts">مبلغ (تومان)</label>
                                        <input type="text" id="amounts" autocomplete="" v-model="form.amount" class="ltr">
                                    </div>
                                </div>
                            </div>


                            <div class="switch-option float-left" v-if="stopUpdate">
                                <a class="button px-3" href="javascript:;">
                                    برای بروزرسانی صفحه رو refresh کن
                                </a>
                            </div>
                            <div class="switch-option float-left" v-if="!stopUpdate && counter == 0">
                                <a class="button px-3" href="javascript:;">
                                    پارامترها در حال بروزرسانی هستن
                                </a>
                            </div>
                            <div class="switch-option float-left" v-else-if="!stopUpdate">
                                <a class="button px-3 " href="javascript:;" @click="processForm()">
                                    <svg class="button-icon spaced icon-check">
                                        <use xlink:href="#svg-check"></use>
                                    </svg>
                                    سفارش خودت رو ثبت کن
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            @include("$VIEW_ROOT.clients.exchange._sidenote")
        </div>
    </div>


@endsection
