<div class="section-banner">
    <img class="section-banner-icon" src="{{ asset("assets/$VIEW_ROOT/img/banner/marketplace-icon.png") }}" alt="marketplace-icon">

    <p class="section-banner-title">حساب رونین خودت رو شارژ کن</p>

    <p class="section-banner-text">بهترین مکان برای اعضای جامعه کریپتوباز برای شارژ حساب رونین</p>
</div>
