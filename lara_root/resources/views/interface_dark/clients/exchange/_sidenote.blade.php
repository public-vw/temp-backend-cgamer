<div class="grid-column">
    <div class="sidebar-box">
        <p class="sidebar-box-title">شارژی که تحویل خواهد شد</p>

        <div class="sidebar-box-items">
            <div class="totals-line-list separator-bottom">
                <p class="price-title big"><span class="currency">WETH</span> <span class="px-1">@{{order.weth}}</span></p>

                <div class="totals-line mt-2 text-center" v-if="stopUpdate">
                    <div class="totals-line-info block">
                        <p class="totals-line-text text-warning">برای بروزرسانی صفحه رو refresh کن</p>
                    </div>
                </div>
                <div class="totals-line mt-2 text-center" v-if="!stopUpdate && counter <= settings.ttlShow || needDegree == true">
                    <transition name="fade" mode="out-in">
                        <div key='wait' class="totals-line-info block" v-if="counter == 0">
                            <p class="totals-line-text text-warning">پارامترها در حال بروزرسانی هستن</p>
                        </div>
                        <div key='count' class="totals-line-info block" v-else-if="counter <= settings.ttlShow">
                            <p class="totals-line-text text-warning"><span>@{{ counter }}</span> ثانیه دیگه پارامترها آپدیت می‌شن</p>
                        </div>
                    </transition>
                    <div class="totals-line-info block popup-reglog-trigger" v-if="needDegree == true">
                        <p class="totals-line-text text-danger">برای این میزان شارژ باید مدارک خودت رو تایید کنی</p>
                    </div>
                </div>
            </div>

            <div class="totals-line-list separator-bottom">
                <div class="totals-line">
                    <div class="totals-line-info">
                        <p class="totals-line-title"><span class="bold">مبلغی که save می‌کنی</span></p>
                    </div>

                    <p class="price-title"><span class="currency">$</span> @{{ calcs.saving }}</p>
                </div>
            </div>

            <div class="totals-line-list separator-bottom">
                <div class="totals-line">
                    <div class="totals-line-info">
                        <p class="totals-line-title"><span class="bold">Gas Fee</span></p>

                        <p class="totals-line-text">اعضای کریپتوباز این هزینه رو پرداخت نمی‌کنن</p>
                    </div>

                    <p class="price-title skew-strike"><span class="currency">$</span> @{{ calcs.gasfee }}</p>
                </div>
                <div class="totals-line">
                    <div class="totals-line-info">
                        <p class="totals-line-title"><span class="bold">کارمزد خدمات ما</span></p>
                        <p class="totals-line-text">صرف هزینه‌های طراحی و نگهداری وب‌سایت می‌شه</p>
                    </div>

                    <p class="price-title"><span class="currency">$</span> @{{ calcs.ourcost }}</p>
                </div>
            </div>

            <div class="totals-line-list">
                <div class="totals-line line-height-big">
                    <p>
                        <svg class="icon-trophy fill-primary-color valign-sub">
                            <use xlink:href="#svg-trophy"></use>
                        </svg>
                        همین حالا با کمترین هزینه wallet خودت رو شارژ کن</p>
                </div>
            </div>
        </div>

    </div>
</div>
