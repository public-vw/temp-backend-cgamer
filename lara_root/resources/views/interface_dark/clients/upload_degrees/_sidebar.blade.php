<div class="account-hub-sidebar">
    <div class="sidebar-box no-padding">
        <div class="sidebar-menu round-borders">
            <div class="sidebar-menu-item">
                <div class="sidebar-menu-header accordion-trigger-linked">
                    <svg class="sidebar-menu-header-icon icon-profile">
                        <use xlink:href="#svg-profile"></use>
                    </svg>

                    <div class="sidebar-menu-header-control-icon">
                        <svg class="sidebar-menu-header-control-icon-open icon-minus-small">
                            <use xlink:href="#svg-minus-small"></use>
                        </svg>

                        <svg class="sidebar-menu-header-control-icon-closed icon-plus-small">
                            <use xlink:href="#svg-plus-small"></use>
                        </svg>
                    </div>

                    <p class="sidebar-menu-header-title">My Profile</p>

                    <p class="sidebar-menu-header-text">Change your avatar &amp; cover, accept friends, read
                        messages and more!</p>
                </div>

                <div class="sidebar-menu-body accordion-content-linked accordion-open">
                    <a class="sidebar-menu-link" href="hub-profile-info.html">Profile Info</a>

                    <a class="sidebar-menu-link" href="hub-profile-social.html">Social &amp; Stream</a>

                    <a class="sidebar-menu-link" href="hub-profile-notifications.html">Notifications</a>

                    <a class="sidebar-menu-link" href="hub-profile-messages.html">Messages</a>

                    <a class="sidebar-menu-link active" href="hub-profile-requests.html">Friend Requests</a>
                </div>
            </div>

            <div class="sidebar-menu-item">
                <div class="sidebar-menu-header accordion-trigger-linked">
                    <svg class="sidebar-menu-header-icon icon-settings">
                        <use xlink:href="#svg-settings"></use>
                    </svg>

                    <div class="sidebar-menu-header-control-icon">
                        <svg class="sidebar-menu-header-control-icon-open icon-minus-small">
                            <use xlink:href="#svg-minus-small"></use>
                        </svg>

                        <svg class="sidebar-menu-header-control-icon-closed icon-plus-small">
                            <use xlink:href="#svg-plus-small"></use>
                        </svg>
                    </div>

                    <p class="sidebar-menu-header-title">Account</p>

                    <p class="sidebar-menu-header-text">Change settings, configure notifications, and review
                        your privacy</p>
                </div>

                <div class="sidebar-menu-body accordion-content-linked">
                    <a class="sidebar-menu-link" href="hub-account-info.html">Account Info</a>

                    <a class="sidebar-menu-link" href="hub-account-password.html">Change Password</a>

                    <a class="sidebar-menu-link" href="hub-account-settings.html">General Settings</a>
                </div>
            </div>

            <div class="sidebar-menu-item">
                <div class="sidebar-menu-header accordion-trigger-linked">
                    <svg class="sidebar-menu-header-icon icon-group">
                        <use xlink:href="#svg-group"></use>
                    </svg>

                    <div class="sidebar-menu-header-control-icon">
                        <svg class="sidebar-menu-header-control-icon-open icon-minus-small">
                            <use xlink:href="#svg-minus-small"></use>
                        </svg>

                        <svg class="sidebar-menu-header-control-icon-closed icon-plus-small">
                            <use xlink:href="#svg-plus-small"></use>
                        </svg>
                    </div>

                    <p class="sidebar-menu-header-title">Groups</p>

                    <p class="sidebar-menu-header-text">Create new groups, manage the ones you created or accept
                        invites!</p>
                </div>

                <div class="sidebar-menu-body accordion-content-linked">
                    <a class="sidebar-menu-link" href="hub-group-management.html">Manage Groups</a>

                    <a class="sidebar-menu-link" href="hub-group-invitations.html">Invitations</a>
                </div>
            </div>

            <div class="sidebar-menu-item">
                <div class="sidebar-menu-header accordion-trigger-linked">
                    <svg class="sidebar-menu-header-icon icon-store">
                        <use xlink:href="#svg-store"></use>
                    </svg>

                    <div class="sidebar-menu-header-control-icon">
                        <svg class="sidebar-menu-header-control-icon-open icon-minus-small">
                            <use xlink:href="#svg-minus-small"></use>
                        </svg>

                        <svg class="sidebar-menu-header-control-icon-closed icon-plus-small">
                            <use xlink:href="#svg-plus-small"></use>
                        </svg>
                    </div>

                    <p class="sidebar-menu-header-title">My Store</p>

                    <p class="sidebar-menu-header-text">Review your account, manage products check stats and
                        much more!</p>
                </div>

                <div class="sidebar-menu-body accordion-content-linked">
                    <a class="sidebar-menu-link" href="hub-store-account.html">My Account</a>

                    <a class="sidebar-menu-link" href="hub-store-statement.html">Sales Statement</a>

                    <a class="sidebar-menu-link" href="hub-store-items.html">Manage Items</a>

                    <a class="sidebar-menu-link" href="hub-store-downloads.html">Downloads</a>
                </div>
            </div>
        </div>
    </div>
</div>
