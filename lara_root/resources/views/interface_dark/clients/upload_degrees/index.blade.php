@extends("$VIEW_ROOT.layouts.base",[
        'PAGE_TITLE' => 'Exchange Tools',
        'NO_HEADER' => false,
    ])

@push('css')
    <link rel="stylesheet" href="{{ asset("assets/$VIEW_ROOT/css/posts-single.css") }}">
@endpush

@section('content')
<div class="content-grid">
{{--    @include("$VIEW_ROOT.clients.upload_degrees._banner")--}}

    <div class="grid grid-full medium-space">
{{--        @include("$VIEW_ROOT.clients.upload_degrees._sidebar")--}}

        <div class="account-hub-content">
            <div class="section-header">
                <div class="section-header-info">
                    <p class="section-pretitle">اطلاعات شخصی من</p>

                    <h2 class="section-title">احراز هویت </h2>
                </div>

                <div class="section-header-actions">
                    <p class="section-header-action">3 مورد نیاز به بررسی دارد</p>
                </div>
            </div>

            <div class="notification-box-list">
                @include("$VIEW_ROOT.clients.upload_degrees._rowbox",[
                    'TITLE' => 'کارت ملی عکس‌دار',
                    'DESC' => 'کارت ملی ‌های قدیمی و تاریخ گذشته تایید نمی شوند',
                    'STATUS' => 0, // not sent yet
                ])

                @include("$VIEW_ROOT.clients.upload_degrees._rowbox",[
                    'TITLE' => 'ایمیل',
                    'DESC' => 'این ایمیل در سایت منتشر نمیشود و فقط برای ارتباط با شما در موارد ضروری نظیر فراموشی کلمه عبور مورد استفاده قرار خواهد گرفت',
                    'STATUS' => 1, // pending
                ])

                @include("$VIEW_ROOT.clients.upload_degrees._rowbox",[
                    'TITLE' => 'شماره موبایل',
                    'DESC' => 'این شماره موبایل در سایت منتشر نمیشود و فقط برای ارتباط با شما در موارد ضروری نظیر فراموشی کلمه عبور مورد استفاده قرار خواهد گرفت',
                    'STATUS' => 3, // done
                ])

                @include("$VIEW_ROOT.clients.upload_degrees._rowbox",[
                    'TITLE' => 'تصویر احراز هویت',
                    'DESC' => 'برای حفظ دارایی‌های کاربران',
                    'STATUS' => 2, // declined
                ])
            </div>
        </div>
    </div>
</div>
@endsection
