<div class="section-banner">
    <img class="section-banner-icon" src="{{ asset("assets/$VIEW_ROOT/img/banner/accounthub-icon.png") }}" alt="accounthub-icon">

    <p class="section-banner-title">Account Hub</p>

    <p class="section-banner-text">Profile info, messages, settings and much more!</p>
</div>
