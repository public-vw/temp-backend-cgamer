<div class="notification-box">
    <div class="user-status request">
        <a class="user-status-avatar" href="#">
            <div class="user-avatar small no-outline">
                <div class="user-avatar-content">
                    <div class="hexagon-image-30-32" data-src="{{ asset("assets/$VIEW_ROOT/img/avatar/16.jpg") }}"></div>
                </div>

                <div class="user-avatar-progress">
                    <div class="hexagon-progress-40-44"></div>
                </div>

                <div class="user-avatar-progress-border">
                    <div class="hexagon-border-40-44"></div>
                </div>
            </div>
        </a>

        <p class="user-status-title"><a class="bold" href="#">{{ $TITLE }}</a></p>

        <p class="user-status-text small-space">{{ $DESC }}</p>

        <div class="action-request-list">
            @if($STATUS == 0)
            <p class="action-request accept with-text">
                <svg class="action-request-icon icon-camera">
                    <use xlink:href="#svg-camera"></use>
                </svg>

                <span class="action-request-text">بارگذاری</span>
            </p>
            @endif
            @if($STATUS == 1)
            <p class="action-request accept with-text">
                <svg class="action-request-icon icon-magnifying-glass">
                    <use xlink:href="#svg-magnifying-glass"></use>
                </svg>

                <span class="action-request-text">در حال بررسی ...</span>
            </p>
            @endif

            @if($STATUS == 2)
                <div class="action-request positive">
                    <svg class="action-request-icon icon-check">
                        <use xlink:href="#svg-check"></use>
                    </svg>
                </div>
            @endif

            @if($STATUS == 3)
                    <p class="action-request accept with-text">
                        <svg class="action-request-icon icon-camera">
                            <use xlink:href="#svg-camera"></use>
                        </svg>

                        <span class="action-request-text">بارگذاری مجدد</span>
                    </p>
                <div class="action-request negative">
                    <svg class="action-request-icon icon-cross">
                        <use xlink:href="#svg-cross"></use>
                    </svg>
                </div>
            @endif
        </div>
    </div>
</div>
