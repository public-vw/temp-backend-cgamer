@extends("$VIEW_ROOT.layouts.base",[
        'FILE_NAME' => 'profile-post',
        'PAGE_TITLE' => 'Limit Reached',
        'HEADER_CLASS' => 'article-header',
    ])

@section('page_title','محدودیت ثبت نوشته')
@section('page_description','تعداد نوشته‌های منتشر نشده شما بیش از حد مجاز است')

@push('css')
{{--    <link rel="stylesheet" href="{{ asset("assets/$VIEW_ROOT/pages/css/terms.css") }}">--}}
@endpush

@section('content')
    <div class="content-grid">
        @include("$VIEW_ROOT.clients.articles_limit._banner")

        <div class="grid medium-space">
            <h2>
                تعداد نوشته‌های باز شما بیش از حد مجاز است.
            </h2>
            <p>
                لازمه قبل از ثبت نوشته جدید، نوشته‌های قبلی خودت رو تکمیل کنی و به تایید برسونی.
            </p>
        </div>
    </div>

@endsection

