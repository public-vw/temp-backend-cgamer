<div id="cropi_container">
    <div id="cropi_image"></div>
    <div class="croppie-tools">
        <span type="button" class="btn btn-block btn-lg crop-btn-flip" data-deg="2"><i
                class="fas fa-sync-alt"></i></span>
        <span type="button" class="btn btn-block btn-lg crop-btn-flip" data-deg="4"><i
                class="fas fa-sync-alt fa-rotate-90"></i></span>
        <span type="button" class="btn btn-block btn-lg crop-btn-rotate" data-deg="90"><i
                class="fas fa-undo"></i></span>
        <span type="button" class="btn btn-block btn-lg crop-btn-rotate" data-deg="-90"><i
                class="fas fa-redo"></i></span>
    </div>
    <div class="cropi_actions">
        <span type="button" class="btn btn-link btn-block btn-lg" id="upload_cancel">Cancel</span>
        <span type="button" class="btn btn-success btn-block btn-lg" id="upload_result">Submit</span>
    </div>
</div>
<a href="javascript:;" id="popup-trigger"></a>

@include("$VIEW_ROOT.clients.articles_single.upload_box",[
    'IMAGE_TYPE' => '',
    'imageTypes' => [],
])
