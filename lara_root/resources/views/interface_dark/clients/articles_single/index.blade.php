@extends("$VIEW_ROOT.layouts.base",[
        'FILE_NAME' => 'profile-post',
        'PAGE_TITLE' => 'نوشته جدید',
        'HEADER_CLASS' => 'article-header',

        'NO_COPY' => false,
        'NO_CUT' => false,
    ])

@section('page_title','نوشته جدید')

@push('css')
    <link rel="stylesheet" href="{{ asset("assets/$VIEW_ROOT/clients/css/articles-single.css") }}"/>
@endpush

@push('js')
    <script src="{{ asset('assets/interface/pages/js/breadcrumb.js') }}"></script>
    <script src="{{ asset("assets/interface/clients/js/articles-single.js") }}"></script>
    <script>
        //removeAfterCreation
        window.routes = {};
        window.routes['articles_single_save_draft'] = '{{ route('client.articles.single.save_draft') }}';
        window.routes['articles_single_change_status'] = '{{ route('client.articles.single.change_status',['status' => '#####']) }}';
        window.routes['upload_image_header'] = '{{ route('client.attachments.upload.by_data',['type' => 'article_header']) }}';
        window.routes['upload_image_inside'] = '{{ route('client.attachments.upload.by_data',['type' => 'article_inside']) }}';
        window.routes['preview'] = '{{ route('client.articles.single.preview',['article' => '#####']) }}';
        window.routes['article_save_seo_meta'] = '{{ route('client.articles.save_meta',['article' => '#####']) }}';
        {{--window.routes['upload_image_inside_map'] = '{{ route('client.attachments.upload.by_data.by_chunk.map',['type' => 'article_inside']) }}';--}}
        {{--window.routes['upload_image_inside_chunks'] = '{{ route('client.attachments.upload.by_data.chunk',['key' => '#####']) }}';--}}
    </script>
@endpush

@section('content')
    {{--    @include("$VIEW_ROOT.clients.articles_single._croppie")--}}


    <div class="content-grid full">
        <form id="uploader-form" method="POST" action="{{ route('client.attachments.upload.by_data',['type' => 'article_inside']) }}" enctype="multipart/form-data">
            <input type="file" id="uploader" name="file" accept="image/*" class="not-visible"/>
        </form>

        <article class="post-open">
            @include("$VIEW_ROOT.clients.articles_single._header")

            <div class="post-open-body vue-el" id="article-body" data-id="{{isset($article)?$article->id:''}}" data-length="{{ isset($article)?strlen($article->content):0 }}">
                <div class="post-open-heading">
                    <h1 class="post-open-title pt-3" contenteditable="true" id="article-header"
                        oninput="handleInput('header',this, 'text')">{{ $article->heading ?? 'عنوان اصلی متن'}}</h1>

                    @isset($article)
                        @includeWhen($article->category,"$VIEW_ROOT.clients.articles_single._breadcrumb")
                        @include("$VIEW_ROOT.clients.articles_single._border_buttons")
                        @role('admin@web')
                            <p class="post-open-timestamp">
                                <span class="highlighted">
                                    <a href="javascript:;" id='seo_meta_button' >سئو متا</a>
                                </span>
                            </p>
                        @endrole
                    @else
                        <p id='preview_button' class="post-open-timestamp not-visible">
                            <span class="highlighted">
                                <a href="#" target="_blank">پیش‌نمایش</a>
                            </span>
                        </p>
                    @endisset

                    @includeWhen(isset($article),"$VIEW_ROOT.clients.articles_single._seo_meta")
                </div>

                <div class="post-open-content">
                    @include("$VIEW_ROOT.clients.articles_single._sidebar")

                    <div class="post-open-content-body" contenteditable="true" id="article-content"
                         oninput="handleInput('body',this)">

                        @include("$VIEW_ROOT.clients.articles_single._hover_action_buttons")
                        @include("$VIEW_ROOT.clients.articles_single._inline_action_buttons")
                        @include("$VIEW_ROOT.clients.articles_single._templates",['display' => 'none'])

                        @isset($article)
                            {!! $article->editableContent() !!}
                        @else
                            <div class="placeholder">Content Goes Here!</div>
                        @endif

                        @include("$VIEW_ROOT.clients.articles_single._action_buttons")

                        {{--                    @include("$VIEW_ROOT.clients.articles_single._tags_list")--}}
                    </div>
                </div>
            </div>
        </article>

    </div>

    {{--    @include("$VIEW_ROOT.clients.articles_single._related_articles")--}}
@endsection
