@php
    $categories = $article->category->categoryChain(['slug','heading']);

    $items['home'] = route('public.homepage');
    $html[] = [
        'url' => route('public.homepage'),
        'title' => __('seo.homepage.title'),
    ];

    $slugs = [];
    foreach($categories as $category){
        $slugs[] = $category['slug'];
        $items[$category['heading']] = route('footer.article_categories.single.view',[implode('/',$slugs)]);
        $html[] = [
            'url' => $items[$category['heading']],
            'title' => $category['heading'],
        ];
    }

    $last_cat = [
            'url' => $items[$category['heading']],
            'title' => $category['heading'],
        ];

    $items[$article->heading] = route('footer.articles.single.view',[implode('/',$slugs),$article->slug]);
    $html[] = [
        'url' => 'javascript:;',
        'title' => $article->heading,
    ];
@endphp

<div class="post-open-breadcrumb pt-5">
    <ul class="breadcrumb" :class="{'hide-bread':!visible}" ref="breadcrumb">
        @foreach($html as $li)
            <li>
                <a href="{{ $li['url'] }}" @if(!$loop->last) target="_blank" @endif>
                    {{ $li['title'] }}
                </a>
            </li>
        @endforeach
    </ul>
    <ul class="breadcrumb breadcrumb-little" :class="{'hide-bread':visible}">
        <li><a href="{{ $last_cat['url'] }}">دسته‌بندی</a></li>
        <li>
            <a href="{{ $last_cat['url'] }}" target="_blank">
                {{ $last_cat['title'] }}
            </a>
        </li>
    </ul>
</div>
