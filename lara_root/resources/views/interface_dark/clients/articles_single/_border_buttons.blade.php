@if(in_array($article->status, config_keys('enums.articles_status',['draft','rejected'])))
    <div class="post-open-breadcrumb">
        <button class="borderbtn set-requested">ارسال درخواست بررسی و انتشار این نوشته</button>
    </div>
@elseif($article->status == config_key('enums.articles_status','ready'))
    <div class="post-open-breadcrumb">
        این نوشته
        &nbsp;<span class="highlighted">تایید شده است</span>&nbsp;
        و در مرحله سئو شدن قرار دارد.
        {{--                                <div>--}}
        {{-- زمان تقریبی انتشار: <b>دو ساعت دیگر</b>--}}
        {{--                                </div>--}}
    </div>
    @role('admin@web')
    <div class="post-open-breadcrumb pt-4">
        <button class="borderbtn no-w100 set-seo-ready">ارسال برای منتشر شدن</button>
        <button class="borderbtn no-w100 warning set-rejected">نیاز به تغییر دارد</button>
    </div>
    @endrole
@elseif($article->status == config_key('enums.articles_status','seo-ready'))
    <div class="post-open-breadcrumb">
        این نوشته
        &nbsp;<span class="highlighted">تایید و سئو شده است</span>&nbsp;
        و در صف انتشار قرار دارد.
        {{--                                <div>--}}
        {{-- زمان تقریبی انتشار: <b>دو ساعت دیگر</b>--}}
        {{--                                </div>--}}
    </div>
    @role('admin@web')
    <div class="post-open-breadcrumb pt-4">
        <button class="borderbtn no-w100 set-active">منتشر شود</button>
    </div>
    @endrole
@elseif(in_array($article->status , config_keys('enums.articles_status',[
        'requested','checking','updated'
        ])))
    <div class="post-open-breadcrumb announcement" style="top: -105px">
        وضعیت این نوشته:
        &nbsp;<span class="highlighted">{{ config_trans('enums.articles_status',$article->status) }}</span>
    </div>
    @if(auth()->user()->hasRole('admin@web')
        || (
            auth()->user()->hasRole('author_supervisor@web')
            && $article->author->user->isChildOfCurrent()
        )
    )
        @if(in_array(
            $article->status,
            config_keys('enums.articles_status', ['draft', 'requested']),
        ))
            <div class="post-open-breadcrumb">
                <button class="borderbtn set-checking">شروع بررسی</button>
            </div>
        @endif
        @if($article->status == config_key('enums.articles_status','checking'))
            <div class="post-open-breadcrumb">
                <button class="borderbtn no-w100 set-ready">ارسال برای سئو شدن</button>
                <button class="borderbtn no-w100 warning set-rejected">نیاز به تغییر دارد</button>
            </div>
        @endif
    @endif
@elseif($article->status == config_key('enums.articles_status','paused'))
    <div class="post-open-breadcrumb">
        نمایش این نوشته با توضیح ذکر شده <b>متوقف</b> شده است
        <span class="highlighted">
            توضیحات توقف مقاله
        </span>
    </div>
@elseif($article->status == config_key('enums.articles_status','rejected'))
    <div class="post-open-breadcrumb">
        این نوشته با توضیح ذکر شده <b>غیرقابل نشر</b> اعلام شده است
        <span class="highlighted">
            توضیحات ریجکت شدن مقاله
        </span>
    </div>
@endif
<p class="post-open-timestamp">
    <span class="highlighted">
        <a href="{{ route('client.articles.single.preview',[$article]) }}" target="_blank">پیش‌نمایش</a>
    </span>
</p>

