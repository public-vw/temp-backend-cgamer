<div id="action-buttons" contenteditable="false">
    <div class="btn" id="add-paragraph">
        <span class="btn-icon"><i class="fas fa-paragraph"></i></span>
    </div>

    <div class="btn" id="add-image">
        <span class="btn-icon">
            <svg class="icon-photos demo-box-icon">
                <use xlink:href="#svg-photos"></use>
            </svg>
        </span>
    </div>

    <div class="btn" id="add-ul">
        <span class="btn-icon"><i class="fas fa-plus-circle"></i></span>
    </div>

    <div class="btn" id="add-ol">
        <span class="btn-icon"><i class="fas fa-plus-circle"></i></span>
    </div>

    <div class="btn" id="add-heading2">
        <span class="btn-icon">
            <svg class="icon-heading-h2 demo-box-icon">
                <use xlink:href="#svg-heading-h2"></use>
            </svg>
        </span>
    </div>

    <div class="btn" id="add-heading3">
        <span class="btn-icon">
            <svg class="icon-heading-h3 demo-box-icon">
                <use xlink:href="#svg-heading-h3"></use>
            </svg>
        </span>
    </div>

    <div class="btn" id="add-heading4">
        <span class="btn-icon">
            <svg class="icon-heading-h4 demo-box-icon">
                <use xlink:href="#svg-heading-h4"></use>
            </svg>
        </span>
    </div>

    <div class="btn wide save-action" style="display: none"></div>

</div>
