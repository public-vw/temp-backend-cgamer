<div id="inline-action-buttons" contenteditable="false">
    <div class="btn" id="go-up">
        <span class="btn-icon"><i class="fas fa-angle-up"></i></span>
    </div>

    <div class="btn" id="select-all-element">
{{--        <span class="btn-icon">--}}
{{--            <svg class="icon-select-all demo-box-icon">--}}
{{--                <use xlink:href="#svg-select-all"></use>--}}
{{--            </svg>--}}
{{--        </span>--}}
        <span class="btn-icon"><i class="fas fa-object-group"></i></span>
    </div>

    <div class="btn" id="change-image">
        <span class="btn-icon"><i class="fas fa-image"></i></span>
    </div>

    <div class="btn" id="delete-item">
        <span class="btn-icon"><i class="fas fa-trash"></i></span>
    </div>

    <div class="btn" id="go-down">
        <span class="btn-icon"><i class="fas fa-angle-down"></i></span>
    </div>
</div>
