<div class="seo-meta-forms frm set-meta" contenteditable="false">
    <div class="row nav-buttons">
        <button data-bind=".google" class="selected" value="1">گوگل</button>
        <button data-bind=".facebook" value="2">فیس‌بوک</button>
        <button data-bind=".twitter" value="3">توئیتر</button>
    </div>
    <div class="row-box google">
        <div class="row">
            <label for="google-title">Title</label>
            <input type="text" data-mode="google" data-part="title" value="{{ $article->seo()['google']->title ?? '' }}"/>
        </div>
        <div class="row">
            <label for="google-keywords">keywords</label>
            <textarea data-mode="google" data-part="keywords" rows="2">{{ $article->seo()['google']->keywords ?? '' }}</textarea>
        </div>
        <div class="row">
            <label for="google-description">description</label>
            <textarea data-mode="google" data-part="description" rows="4">{{ $article->seo()['google']->description ?? '' }}</textarea>
        </div>
    </div>
    <div class="row-box facebook">
        <div class="row">
            <label for="facebook-title">Title</label>
            <input type="text" data-mode="facebook" data-part="title" value="{{ $article->seo()['facebook']->title ?? '' }}"/>
        </div>
        <div class="row">
            <label for="facebook-keywords">keywords</label>
            <textarea data-mode="facebook" data-part="keywords" rows="2">{{ $article->seo()['facebook']->keywords ?? '' }}</textarea>
        </div>
        <div class="row">
            <label for="facebook-description">description</label>
            <textarea data-mode="facebook" data-part="description" rows="4">{{ $article->seo()['facebook']->description ?? '' }}</textarea>
        </div>
    </div>
    <div class="row-box twitter">
        <div class="row">
            <label for="twitter-title">Title</label>
            <input type="text" data-mode="twitter" data-part="title" value="{{ $article->seo()['twitter']->title ?? '' }}"/>
        </div>
        <div class="row">
            <label for="twitter-keywords">keywords</label>
            <textarea data-mode="twitter" data-part="keywords" rows="2">{{ $article->seo()['twitter']->keywords ?? '' }}</textarea>
        </div>
        <div class="row">
            <label for="twitter-description">description</label>
            <textarea data-mode="twitter" data-part="description" rows="4">{{ $article->seo()['twitter']->description ?? '' }}</textarea>
        </div>
    </div>
    <div class="row submit">
        <button class='btn btn-submit' type="button">ثبت کن</button>
        <a href="javascript:;" class="btn-cancel">لغو</a>
    </div>
</div>
