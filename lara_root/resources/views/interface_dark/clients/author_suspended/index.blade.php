@extends("$VIEW_ROOT.layouts.base",[
        'FILE_NAME' => 'profile-post',
        'PAGE_TITLE' => 'Limit Reached',
        'HEADER_CLASS' => 'article-header',
    ])

@section('page_title','تعلیق امکان نویسندگی')
@section('page_description','دسترسی شما برای نوشتن مقاله جدید توسط مدیر سایت معلق شده است')

@section('content')
    <div class="content-grid">
        @include("$VIEW_ROOT.clients.author_suspended._banner")

        <div class="grid medium-space">
            <h2>
                دسترسی شما برای نوشتن مقاله جدید توسط مدیر سایت معلق شده است.
            </h2>
            <p>
                با مدیر سایت تماس بگیرید و مورد را بررسی کنید.
            </p>
            <p>
                اگر علت تعلیق را میدانید در جهت رفع علت اقدام کنید و یا اگر تعلیق به صورت دوره‌ای صورت گرفته است تا زمان تعیین شده منتظر بمانید.
            </p>
        </div>
    </div>

@endsection

