<div class="feat_property list agency" id="agencies_single_item">
    <div class="thumb">
{{--        <img class="img-whp" src="{{ asset($agency->image) }}" alt="{{ $agency->title }}">--}}
        <img class="img-whp" src="{{ asset('assets/interface/images/agency/2.jpg') }}" alt="2.jpg">
        <div class="thmb_cntnt">
            <ul class="tag mb0">
                <li class="list-inline-item first-bg" v-if="item.rank && item.rank.total > 0">
                    <a href="#"><i class="fa fa-star"></i>&nbsp;@{{ item.rank.rank }}</a></li>
{{--                <li class="list-inline-item first-bg"><a href="#"><i class="fa fa-user"></i>&nbsp;{{ $agency->rank['total'] }}</a></li>--}}
                <li class="list-inline-item second-bg" v-if="item.user && item.user.mobile_verified_at">
                    <a href="#">Verified</a></li>
            </ul>
        </div>
    </div>
    <div class="details">
        <div class="tc_content">
            <h4>@{{ item.title }}</h4>
            <p class="text-thm" v-if="item.group" onclick="window.location=$(this).attr('data-path')" :data-path="js_route('public.agency_groups.single',[item.group.uri])">@{{ item.group.title }}&nbsp;<i class="fa fa-external-link"></i></p>
            <p class="text-thm" v-if="!item.group">&nbsp;</p>
            <ul class="prop_details mb0">
                <li><a href="#">Office1: 134 456 3210</a></li>
                <li><a href="#">Office2: 134 456 3210</a></li>
                <li><a href="#">Office3: 891 456 9874</a></li>
                <li><a href="#">Mobile: 891 456 9874</a></li>
                <li class="db-520">&nbsp;</li>
                <li class="db-520">&nbsp;</li>
                {{--                    <li v-if="typeof(item.user) != 'undefined'"><a href="#">Email: @{{ item.user.email }}</a></li>--}}
            </ul>
        </div>
        <div class="fp_footer stick_footer">
            <ul class="fp_meta float-left mb0">
                <li class="list-inline-item"><a href="#"><i class="fa fa-globe"></i></a></li>
                <li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li class="list-inline-item"><a href="#"><i class="fa fa-instagram"></i></a></li>
                <li class="list-inline-item"><a href="#"><i class="fa fa-pinterest"></i></a></li>
                <li class="list-inline-item"><a href="#"><i class="fa fa-map-marker"></i></a></li>
            </ul>
            <div class="fp_pdate float-right text-thm"><a :href="js_route('public.agencies.properties',[item.uri])">View My Listings <i class="fa fa-angle-right"></i></a></div>
        </div>
    </div>
</div>
