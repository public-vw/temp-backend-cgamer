<div class="col-lg-12">
    <div class="breadcrumb_content style2 mt30-767 mb30-767">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('public.homepage') }}">Home</a></li>
            <li class="breadcrumb-item active text-thm"><a href="{{ route('public.agencies.list') }}">Agencies</a></li>
        </ol>
        <h1 class="breadcrumb_title">{{ $agency->title }}</h1>
    </div>
</div>
