@extends("interface.layouts.base",[
        'PAGE_FILE' => 'page-listing-agencies-v3_single',
        'PAGE_TITLE' => $agency->title.' | Agency',
        'NO_BREAD'   => true,
    ])

@push('js')
    <script type="text/javascript" src="{{ asset('assets/interface/js/script.js') }}"></script>

    <script>
        //removeAfterCreation
        window.routes['agencies_single_item'] = '{{ route('public.agencies.single',[$agency->uri]) }}';
        window.routes['public.agency_groups.single'] = '{{ route('public.agency_groups.single',['#####']) }}';
        window.routes['public.agencies.properties'] = '{{ route('public.agencies.properties',['#####']) }}';
    </script>
    <script src="{{ asset('assets/interface/js/agencies_single-item.js') }}"></script>
@endpush

@section('content')
<section class="our-agent-single bgc-f7 pb30-991">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-8">
                <div class="row">
                    @include('interface.pages.agencies_single.header')
                    <div class="col-lg-12">
                        @include('interface.pages.agencies_single.item')
                        @include('interface.pages.agencies_single.content')
                    </div>
                </div>
            </div>
            @include('interface.pages.agencies_single.sidebar')
        </div>
    </div>
</section>
@endsection
