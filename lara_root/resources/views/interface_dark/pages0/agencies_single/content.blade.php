<div class="shop_single_tab_content style2 mt30">
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="description-tab" data-toggle="tab" href="#description" role="tab" aria-controls="description" aria-selected="true">Description</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="listing-tab" data-toggle="tab" href="#listing" role="tab" aria-controls="listing" aria-selected="false">Listing</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="agent-tab" data-toggle="tab" href="#agent" role="tab" aria-controls="agent" aria-selected="false">Agents</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="review-tab" data-toggle="tab" href="#review" role="tab" aria-controls="review" aria-selected="false">Reviews</a>
        </li>
    </ul>
    <div class="tab-content" id="myTabContent2">
        <div class="tab-pane fade show active" id="description" role="tabpanel" aria-labelledby="description-tab">
            <div class="product_single_content">
                <div class="mbp_pagination_comments">
                    <div class="mbp_first media">
                        <div class="media-body">
                            <p class="mb25">Evans Tower very high demand corner junior one bedroom plus a large balcony boasting full open NYC views. You need to see the views to believe them. Mint condition with new hardwood floors. Lots of closets plus washer and dryer.</p>
                            <p class="mt10 mb0">Fully furnished. Elegantly appointed condominium unit situated on premier location. PS6. The wide entry hall leads to a large living room with dining area. This expansive 2 bedroom and 2 renovated marble bathroom apartment has great windows. Despite the interior views, the apartments Southern and Eastern exposures allow for lovely natural light to fill every room. The master suite is surrounded by handcrafted milkwork and features incredible walk-in closet and storage space.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade row pl15 pl0-1199 pr15 pr0-1199" id="listing" role="tabpanel" aria-labelledby="listing-tab">
            <div class="col-lg-12">
                <div class="feat_property list style2 hvr-bxshd bdrrn mb10 mt20">
                    <div class="thumb">
                        <img class="img-whp" src="{{ asset('assets/interface/images/property/fp1.jpg') }}" alt="fp1.jpg">
                        <div class="thmb_cntnt">
                            <ul class="icon mb0">
                                <li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
                                <li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="details">
                        <div class="tc_content">
                            <div class="dtls_headr">
                                <ul class="tag">
                                    <li class="list-inline-item"><a href="#">For Rent</a></li>
                                    <li class="list-inline-item"><a href="#">Featured</a></li>
                                </ul>
                                <a class="fp_price" href="#">$13,000<small>/mo</small></a>
                            </div>
                            <p class="text-thm">Apartment</p>
                            <h4>Renovated Apartment</h4>
                            <p><span class="flaticon-placeholder"></span> 1421 San Pedro St, Los Angeles, CA 90015</p>
                            <ul class="prop_details mb0">
                                <li class="list-inline-item"><a href="#">Beds: 4</a></li>
                                <li class="list-inline-item"><a href="#">Baths: 2</a></li>
                                <li class="list-inline-item"><a href="#">Sq Ft: 5280</a></li>
                            </ul>
                        </div>
                        <div class="fp_footer">
                            <ul class="fp_meta float-left mb0">
                                <li class="list-inline-item"><a href="#"><img src="{{ asset('assets/interface/images/property/pposter1.png') }}" alt="pposter1.png"></a></li>
                                <li class="list-inline-item"><a href="#">Ali Tufan</a></li>
                            </ul>
                            <div class="fp_pdate float-right">4 years ago</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="feat_property list style2 hvr-bxshd bdrrn mb10">
                    <div class="thumb">
                        <img class="img-whp" src="{{ asset('assets/interface/images/property/fp3.jpg') }}" alt="fp3.jpg">
                        <div class="thmb_cntnt">
                            <ul class="icon mb0">
                                <li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
                                <li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="details">
                        <div class="tc_content">
                            <div class="dtls_headr">
                                <ul class="tag">
                                    <li class="list-inline-item"><a href="#">For Rent</a></li>
                                    <li class="list-inline-item"><a href="#">Featured</a></li>
                                </ul>
                                <a class="fp_price" href="#">$13,000<small>/mo</small></a>
                            </div>
                            <p class="text-thm">Apartment</p>
                            <h4>Luxury Family Home</h4>
                            <p><span class="flaticon-placeholder"></span> 1421 San Pedro St, Los Angeles, CA 90015</p>
                            <ul class="prop_details mb0">
                                <li class="list-inline-item"><a href="#">Beds: 4</a></li>
                                <li class="list-inline-item"><a href="#">Baths: 2</a></li>
                                <li class="list-inline-item"><a href="#">Sq Ft: 5280</a></li>
                            </ul>
                        </div>
                        <div class="fp_footer">
                            <ul class="fp_meta float-left mb0">
                                <li class="list-inline-item"><a href="#"><img src="{{ asset('assets/interface/images/property/pposter1.png') }}" alt="pposter1.png"></a></li>
                                <li class="list-inline-item"><a href="#">Ali Tufan</a></li>
                            </ul>
                            <div class="fp_pdate float-right">4 years ago</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="feat_property list style2 hvr-bxshd bdrrn">
                    <div class="thumb">
                        <img class="img-whp" src="{{ asset('assets/interface/images/property/fp2.jpg') }}" alt="fp2.jpg">
                        <div class="thmb_cntnt">
                            <ul class="icon mb0">
                                <li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
                                <li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="details">
                        <div class="tc_content">
                            <div class="dtls_headr">
                                <ul class="tag">
                                    <li class="list-inline-item"><a href="#">For Rent</a></li>
                                    <li class="list-inline-item"><a href="#">Featured</a></li>
                                </ul>
                                <a class="fp_price" href="#">$13,000<small>/mo</small></a>
                            </div>
                            <p class="text-thm">Apartment</p>
                            <h4>Gorgeous Villa Bay View</h4>
                            <p><span class="flaticon-placeholder"></span> 1421 San Pedro St, Los Angeles, CA 90015</p>
                            <ul class="prop_details mb0">
                                <li class="list-inline-item"><a href="#">Beds: 4</a></li>
                                <li class="list-inline-item"><a href="#">Baths: 2</a></li>
                                <li class="list-inline-item"><a href="#">Sq Ft: 5280</a></li>
                            </ul>
                        </div>
                        <div class="fp_footer">
                            <ul class="fp_meta float-left mb0">
                                <li class="list-inline-item"><a href="#"><img src="{{ asset('assets/interface/images/property/pposter1.png') }}" alt="pposter1.png"></a></li>
                                <li class="list-inline-item"><a href="#">Ali Tufan</a></li>
                            </ul>
                            <div class="fp_pdate float-right">4 years ago</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade row pl15 pl0-1199 pr15 pr0-1199" id="agent" role="tabpanel" aria-labelledby="agent-tab">
            <div class="col-lg-12">
                <div class="feat_property list style2 agent hvr-bxshd bdrrn mb10 mt20">
                    <div class="thumb">
                        <img class="img-whp" src="{{ asset('assets/interface/images/team/11.jpg') }}" alt="11.jpg">
                        <div class="thmb_cntnt">
                            <ul class="tag mb0">
                                <li class="list-inline-item dn"></li>
                                <li class="list-inline-item"><a href="#">2 Listings</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="details">
                        <div class="tc_content">
                            <h4>Christopher Pakulla</h4>
                            <p class="text-thm">Agent</p>
                            <ul class="prop_details mb0">
                                <li><a href="#">Office: 134 456 3210</a></li>
                                <li><a href="#">Mobile: 891 456 9874</a></li>
                                <li><a href="#">Fax: 342 654 1258</a></li>
                                <li><a href="#">Email: pakulla@findhouse.com</a></li>
                            </ul>
                        </div>
                        <div class="fp_footer">
                            <ul class="fp_meta float-left mb0">
                                <li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li class="list-inline-item"><a href="#"><i class="fa fa-instagram"></i></a></li>
                                <li class="list-inline-item"><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                <li class="list-inline-item"><a href="#"><i class="fa fa-google"></i></a></li>
                            </ul>
                            <div class="fp_pdate float-right text-thm">View My Listings <i class="fa fa-angle-right"></i></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="feat_property list style2 agent hvr-bxshd bdrrn mb10">
                    <div class="thumb">
                        <img class="img-whp" src="{{ asset('assets/interface/images/team/12.jpg') }}" alt="12.jpg">
                        <div class="thmb_cntnt">
                            <ul class="tag mb0">
                                <li class="list-inline-item dn"></li>
                                <li class="list-inline-item"><a href="#">2 Listings</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="details">
                        <div class="tc_content">
                            <h4>Anna Harrison</h4>
                            <p class="text-thm">Agent</p>
                            <ul class="prop_details mb0">
                                <li><a href="#">Office: 134 456 3210</a></li>
                                <li><a href="#">Mobile: 891 456 9874</a></li>
                                <li><a href="#">Fax: 342 654 1258</a></li>
                                <li><a href="#">Email: annaharris@findhouse.com</a></li>
                            </ul>
                        </div>
                        <div class="fp_footer">
                            <ul class="fp_meta float-left mb0">
                                <li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li class="list-inline-item"><a href="#"><i class="fa fa-instagram"></i></a></li>
                                <li class="list-inline-item"><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                <li class="list-inline-item"><a href="#"><i class="fa fa-google"></i></a></li>
                            </ul>
                            <div class="fp_pdate float-right text-thm">View My Listings <i class="fa fa-angle-right"></i></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="feat_property list style2 agent hvr-bxshd bdrrn">
                    <div class="thumb">
                        <img class="img-whp" src="{{ asset('assets/interface/images/team/13.jpg') }}" alt="13.jpg">
                        <div class="thmb_cntnt">
                            <ul class="tag mb0">
                                <li class="list-inline-item dn"></li>
                                <li class="list-inline-item"><a href="#">2 Listings</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="details">
                        <div class="tc_content">
                            <h4>Luxury Family Home</h4>
                            <p class="text-thm">Agent</p>
                            <ul class="prop_details mb0">
                                <li><a href="#">Office: 134 456 3210</a></li>
                                <li><a href="#">Mobile: 891 456 9874</a></li>
                                <li><a href="#">Fax: 342 654 1258</a></li>
                                <li><a href="#">Email: pakulla@findhouse.com</a></li>
                            </ul>
                        </div>
                        <div class="fp_footer">
                            <ul class="fp_meta float-left mb0">
                                <li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li class="list-inline-item"><a href="#"><i class="fa fa-instagram"></i></a></li>
                                <li class="list-inline-item"><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                <li class="list-inline-item"><a href="#"><i class="fa fa-google"></i></a></li>
                            </ul>
                            <div class="fp_pdate float-right text-thm">4 years ago</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="review" role="tabpanel" aria-labelledby="review-tab">
            <div class="product_single_content">
                <div class="mbp_pagination_comments">
                    <ul class="total_reivew_view">
                        <li class="list-inline-item sub_titles">896 Reviews</li>
                        <li class="list-inline-item">
                            <ul class="star_list">
                                <li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
                                <li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
                                <li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
                                <li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
                                <li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
                            </ul>
                        </li>
                        <li class="list-inline-item avrg_review">( 4.5 out of 5 )</li>
                        <li class="list-inline-item write_review">Write a Review</li>
                    </ul>
                    <div class="mbp_first media">
                        <img src="{{ asset('assets/interface/images/testimonial/1.png" class="mr-3') }}" alt="1.png">
                        <div class="media-body">
                            <h4 class="sub_title mt-0">Diana Cooper
                                <div class="sspd_review dif">
                                    <ul>
                                        <li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
                                        <li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
                                        <li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
                                        <li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
                                        <li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
                                        <li class="list-inline-item"></li>
                                    </ul>
                                </div>
                            </h4>
                            <a class="sspd_postdate fz14" href="#">December 28, 2020</a>
                            <p class="mt10">Beautiful home, very picturesque and close to everything in jtree! A little warm for a hot weekend, but would love to come back during the cooler seasons!</p>
                        </div>
                    </div>
                    <div class="custom_hr"></div>
                    <div class="mbp_first media">
                        <img src="{{ asset('assets/interface/images/testimonial/2.png" class="mr-3') }}" alt="2.png">
                        <div class="media-body">
                            <h4 class="sub_title mt-0">Ali Tufan
                                <div class="sspd_review dif">
                                    <ul>
                                        <li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
                                        <li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
                                        <li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
                                        <li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
                                        <li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
                                        <li class="list-inline-item"></li>
                                    </ul>
                                </div>
                            </h4>
                            <a class="sspd_postdate fz14" href="#">December 28, 2020</a>
                            <p class="mt10">Beautiful home, very picturesque and close to everything in jtree! A little warm for a hot weekend, but would love to come back during the cooler seasons!</p>
                        </div>
                    </div>
                    <div class="custom_hr"></div>
                    <div class="mbp_comment_form style2">
                        <h4>Write a Review</h4>
                        <ul class="sspd_review">
                            <li class="list-inline-item">
                                <ul class="mb0">
                                    <li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
                                </ul>
                            </li>
                            <li class="list-inline-item review_rating_para">Your Rating & Review</li>
                        </ul>
                        <form class="comments_form">
                            <div class="form-group">
                                <input type="text" class="form-control" id="exampleInputName1" aria-describedby="textHelp" placeholder="Review Title">
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="12" placeholder="Your Review"></textarea>
                            </div>
                            <button type="submit" class="btn btn-thm">Submit Review <span class="flaticon-right-arrow-1"></span></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
