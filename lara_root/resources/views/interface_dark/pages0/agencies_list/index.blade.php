@extends("interface.layouts.base",[
        'PAGE_FILE'  => 'page-listing-agencies-v2',
        'PAGE_TITLE' => 'Agencies List',
        'NO_BREAD'   => true,
    ])

@push('js')
    <script type="text/javascript" src="{{ asset('assets/interface/js/script.js') }}"></script>

    <script>
        //removeAfterCreation
        window.routes['agencies_list_items'] = '{{ route('public.agencies.list') }}';
        window.routes['public.agencies.single'] = '{{ route('public.agencies.single',['#####']) }}';
        window.routes['public.agency_groups.single'] = '{{ route('public.agency_groups.single',['#####']) }}';
        window.routes['public.agencies.properties'] = '{{ route('public.agencies.properties',['#####']) }}';
    </script>
    <script src="{{ asset('assets/interface/js/agencies_list-items.js') }}"></script>
@endpush

@section('content')
<section class="our-agent-listing bgc-f7 pb30-991">
    <div class="container">
        <div class="row">
            @include('interface.pages.agencies_list.header')
        </div>
        <div class="row">
            <div class="col-lg-4 col-xl-4 sticky_sidebar">
                @include('interface.pages.agencies_list.sidebar')
            </div>
            <div class="col-md-12 col-lg-8" id="agencies_list_items">
                <div class="row">
                    <div class="grid_list_search_result style2">
                        @include('interface.pages.agencies_list.items_header')
                    </div>
                </div>
                <div class="row">
                        @include('interface.pages.agencies_list.item')
                    <div class="col-lg-12 mt20">
                        <div class="mbp_pagination">
                            @include('interface.includes.cursor_pagination')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
