<div class="col-sm-12 col-md-4 col-lg-3 col-xl-3">
    <div class="left_area">
        <p>@{{page_info.total}} Search results</p>
    </div>
</div>
<div class="col-sm-12 col-md-8 col-lg-9 col-xl-9">
    <div class="right_area style2 text-right">
        <ul>
            <li class="list-inline-item"><span class="shrtby">Sort by:</span>
                <select class="selectpicker show-tick">
                    <option>Highest Rank</option>
                    <option>More Properties</option>
                    <option>Newest Ones</option>
                    <option>Oldest Ones</option>
                </select>
            </li>
        </ul>
    </div>
</div>
