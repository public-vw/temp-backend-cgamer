<div class="col-lg-6">
    <div class="breadcrumb_content style2 mb0-991">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('public.homepage') }}">Home</a></li>
            <li class="breadcrumb-item active text-thm" aria-current="page">All Agencies</li>
        </ol>
        <h1 class="breadcrumb_title">All Agencies</h1>
    </div>
</div>
{{--TODO: Add Grid View--}}
{{--<div class="col-lg-6">--}}
{{--    <div class="listing_list_style tal-991">--}}
{{--        <ul>--}}
{{--            <li class="list-inline-item"><a href="javascript:;"><span class="fa fa-th-large"></span></a></li>--}}
{{--            <li class="list-inline-item"><a href="javascript:;"><span class="fa fa-th-list"></span></a></li>--}}
{{--        </ul>--}}
{{--    </div>--}}
{{--</div>--}}
