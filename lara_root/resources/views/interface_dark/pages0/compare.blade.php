@extends("interface.layouts.base",[
        'PAGE_FILE' => 'page-compare',
        'PAGE_TITLE' => 'Compare',
        'NO_BREAD'   => true,
    ])

@push('js')
    <script type="text/javascript" src="{{ asset('assets/interface/js/script.js') }}"></script>
@endpush

@section('content')
{{-- Inner Page Breadcrumb --}}
    <section class="inner_page_breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-xl-6">
                    <div class="breadcrumb_content">
                        <h4 class="breadcrumb_title">Compare</h4>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Compare</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>

{{-- Our Pricing Table --}}
    <section class="our-pricing bgc-fa">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <div class="main-title text-center">
                        <h2>Compare Listings</h2>
                        <p>We provide full service at every step</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="membership_container">
                        <ul class="mc_parent_list">
                            <li class="list-inline-item">
                                <ul class="mc_child_list one">
                                    <li><div class="membership_header dn"></div></li>
                                    <li>City</li>
                                    <li>Beds</li>
                                    <li>Rooms</li>
                                    <li>Garage</li>
                                    <li>Year of build</li>
                                    <li>Laundry Room</li>
                                    <li>Status</li>
                                </ul>
                            </li>
                            <li class="list-inline-item">
                                <ul class="mc_child_list two text-center">
                                    <li>
                                        <div class="membership_header">
                                            <div class="thumb">
                                                <a href="#"><span class="flaticon-close"></span></a>
                                                <img class="img-fluid w100" src="{{ asset('assets/interface/images/pricing/1.jpg') }}" alt="1.jpg">
                                                <div class="price">$13,000<span class="mnth">/mo</span></div>
                                            </div>
                                            <div class="details">
                                                <h4>Renovated Apartment</h4>
                                                <p>Apartment</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li><a href="#">New York</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#">7</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">1991</a></li>
                                    <li><a href="#">Yes</a></li>
                                    <li><a class="btn pricing_btn" href="#">Active</a></li>
                                </ul>
                            </li>
                            <li class="list-inline-item">
                                <ul class="mc_child_list three text-center">
                                    <li>
                                        <div class="membership_header">
                                            <div class="thumb">
                                                <a href="#"><span class="flaticon-close"></span></a>
                                                <img class="img-fluid w100" src="{{ asset('assets/interface/images/pricing/2.jpg') }}" alt="2.jpg">
                                                <div class="price">$13,000<span class="mnth">/mo</span></div>
                                            </div>
                                            <div class="details">
                                                <h4>Luxury Family Home</h4>
                                                <p>Apartment</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li><a href="#">New York</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#">7</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">1991</a></li>
                                    <li><a href="#">Yes</a></li>
                                    <li><a class="btn pricing_btn" href="#">Active</a></li>
                                </ul>
                            </li>
                            <li class="list-inline-item">
                                <ul class="mc_child_list four text-center">
                                    <li>
                                        <div class="membership_header">
                                            <div class="thumb">
                                                <a href="#"><span class="flaticon-close"></span></a>
                                                <img class="img-fluid w100" src="{{ asset('assets/interface/images/pricing/3.jpg') }}" alt="3.jpg">
                                                <div class="price">$13,000<span class="mnth">/mo</span></div>
                                            </div>
                                            <div class="details">
                                                <h4>Ample Penthouse</h4>
                                                <p>Apartment</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li><a href="#">New York</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#">7</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">1991</a></li>
                                    <li><a href="#">Yes</a></li>
                                    <li><a class="btn pricing_btn" href="#">Active</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

{{-- Start Partners --}}
    <section class="start-partners bgc-thm pt50 pb50">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="start_partner tac-smd">
                        <h2>Become a Real Estate Agent</h2>
                        <p>We only work with the best companies around the globe</p>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="parner_reg_btn text-right tac-smd">
                        <a class="btn btn-thm2" href="#">Register Now</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
