<style>
    .admin-hover-panel{
        position: fixed;
        top: 0;
        left: 0;
        padding: 10px;
        border-radius: 10px;
        background-color: #cca799;
        color: #313131;
        font-size: 16px;
        cursor: pointer;
        z-index: 100000;
        margin:10px;
        box-shadow: 2px 2px 8px #333;
        transition: 0.2s all ease-out;
    }
    a{color: #313131;}
    a:hover,a:focus{color: #3a3a3a;}
    .admin-hover-panel:hover {
        opacity: 0.4;
    }
</style>

<div class="admin-hover-panel">
    <a href="{{ route('header.impersonate.stop') }}">Log Back</a>
</div>
