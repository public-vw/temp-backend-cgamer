@extends("$VIEW_ROOT.layouts.app")

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    @include("$VIEW_ROOT.includes.alerts")

                    <form method="POST" action="{{ route('auth.auth2.check') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="gtoken" class="col-md-4 col-form-label text-md-right">{{ __('Two Step Authentication Token') }}</label>

                            <div class="col-md-6">
                                <input id="gtoken" type="phone" class="form-control @error('gtoken') is-invalid @enderror" name="gtoken" value="{{ old('gtoken') }}" required autocomplete="off" autofocus
                                minlength="6" maxlength="6" >

                                @error('gtoken')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Submit') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
