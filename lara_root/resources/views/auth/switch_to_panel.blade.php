@extends('mainframe')

@section('bootstrap-css','')
@section('bootstrap-js','')
@section('jquery','')
@section('fontawesome','')
@section('flags','')
@section('noty','')

@section('head')
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset("assets/sysadmin/css/app.css") }}" rel="stylesheet">
@endsection

@push('css')
    <style>
        .panel-clicker{
            cursor:pointer;
            transition: all 150ms;
        }
        .panel-clicker:hover{
            transform: scale(1.05);
            box-shadow: 0 10px 20px rgba(0,0,0,.12), 0 4px 8px rgba(0,0,0,.06);
        }
    </style>
@push('mainjs')
    <script src="{{ asset("assets/sysadmin/js/app.js") }}"></script>
    <script>
        $(function(){
            $('.panel-clicker').click(function(){
                window.location = $(this).attr('data-route');
            });
        });
    </script>
@endpush

@section('body')
    <div id="app">
        <main class="py-4">
            <div class="container">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                <div class="row">
                    <div class="col-md-12">
                        <div class="jumbotron py-2">
                            <h2>
                                Hi, {{$user->name}}
                            </h2>
                            <p>Choose your panel:</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    @foreach($panels as $panel)
                        <div class="col-md-3">
                            <div class="card text-center {{ (Route::has("$panel.home"))? 'panel-clicker' : '' }}"
                                @if(Route::has("$panel.home"))
                                    data-route="{{route("$panel.home")}}"
                                @endif>
                                <img src="{{ asset("assets/auth/img/".$panel.(Route::has("$panel.home")?'':'-disable').".jpg") }}" class="card-img-top" width="300px" height="250px" />
                                <div class="card-body">
                                    <h5 class="card-title">{{ucwords($panel)}} Panel</h5>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="col-md-3">
                        <div class="card text-center panel-clicker" data-route="{{ route("public.homepage") }}">
                            <img src="{{ asset("assets/auth/img/interface.jpg") }}" class="card-img-top" width="300px" height="250px" />
                            <div class="card-body">
                                <h5 class="card-title">Interface</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
@endsection


