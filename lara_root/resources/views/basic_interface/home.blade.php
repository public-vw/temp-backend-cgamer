@extends("$VIEW_ROOT.layouts.base",['page_title' => 'Home','no_bread' => true])

@push('css')
    <style>
        .version {
            font-size: 18px;
        }

        .details {
            font-size: 16px;
        }
    </style>
@endpush

@section('content')
    <div class="title m-b-md">
        {{ config('app.title') }}
        <span class="headline m-b-md">{{ config('app.version') }}</span>
        <div class="desc">{{ config('app.description') }}</div>
        <div class="version">
            <hr>
            {{$branch}} | {{ Str::substr($version,0,6) }}
        </div>
        @if(!empty($details))
            <div class="details">
                <hr>
                {!! nl2br($details) !!}
            </div>
        @endif
    </div>

    <div class="links">
        Follow us at : <a href="https://github.com/a-ghasemi">Github <i class="fa fa-github"></i></a>
    </div>
@endsection
