@extends('mainframe')

@section('bootstrap-css','')
@section('bootstrap-js','')
@section('jquery','')
@section('fontawesome','')
@section('flags','')
@section('noty','')

@section('head')
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset("assets/$VIEW_ROOT/css/app.css") }}" rel="stylesheet">
@endsection

@section('body')
    <div class="flex-center position-ref full-height">
            <div class="top-right links">
                @auth
                    <a href="{{ route('auth.switch_to_panel') }}">Account Panel</a>
                @else
                    @if (Route::has('auth.login'))
                        <a href="{{ route('auth.login') }}">Login</a>
                    @endif
                    @if (Route::has('auth.register'))
                        <a href="{{ route('auth.register') }}">Register</a>
                    @endif
                @endauth
            </div>

        <div class="content">
            @yield('content')
        </div>
    </div>
@endsection
