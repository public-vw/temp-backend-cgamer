<!DOCTYPE html>
<html>
    <head>
        <title>Laravel 5 Instagram API tutorial with example</title>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    </head>
    <body>
        <div class="container">
            <h2>Sitemap Simulator</h2><br/>
            <h4>Count: {{count($sitemaps)}}</h4><br/>
            @if(isset($sitemaps) && !empty($sitemaps))
                @foreach($sitemaps as $item)
                <table class="table table-bordered">
                    <thead style="background-color:#ffe">
                        <th colspan="3">url</th>
                        <th>date</th>
                        <th>priority</th>
                        <th>freq</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="3">
                            @if(isset($item[6]) && !empty($item[6]))
                                {{$item[0]}}
                            @else
                                <a href="{{ $item[0] }}">{{ substr($item[0],strlen(url('/'))) }}</a>
                            @endif
                            </td>
                            <td>{{$item[1]}}</td>
                            <td>{{$item[2]}}</td>
                            <td>{{$item[3]}}</td>
                        </tr>
                        @if(isset($item[6]) && !empty($item[6]))
                            <tr style="background-color:#eef">
                                @foreach($item[6] as $trans)
                                <th>{{$trans['language']}}</th>
                                @endforeach
                            </tr>
                            <tr>
                                @foreach($item[6] as $trans)
                                    <td>
                                        <a href="{{ $trans['url'] }}">{{ substr($trans['url'],strlen(url('/'))) }}</a>
                                    </td>
                                @endforeach
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </body>
</html>
