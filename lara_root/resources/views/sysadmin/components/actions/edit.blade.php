@compo("$VIEW_ROOT.components.clickables.link", [
    "URL"   => route("$VIEW_ROOT.$TABLE.edit", $VARIABLE),
    "TITLE" => $TITLE ?? "edit",
    "CLASS" => $CLASS ?? null,
])
