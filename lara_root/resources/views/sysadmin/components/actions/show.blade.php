@compo("$VIEW_ROOT.components.clickables.link", [
    "URL"   => route("{$VIEW_ROOT}.{$TABLE}.show", $VARIABLE),
    "TITLE" => $TITLE ?? "show",
    "CLASS" => 'btn btn-sm btn-success',
])
