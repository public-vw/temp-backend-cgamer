@if($PREPENDS ?? false)
    @foreach($PREPENDS as $prepend)
        {!! $prepend !!}
        @if($prepend !== end($PREPENDS))&nbsp;|&nbsp;@endif
    @endforeach
@endif
@isset($ACTIONS['edit'])
    @compo("$VIEW_ROOT.components.actions.edit", [
        "TABLE"     => $TABLE,
        "VARIABLE"  => $VARIABLE,
        "TITLE"     => 'Edit',
    ])
@endisset

@if(count($ACTIONS) > 1)|@endif

@isset($ACTIONS['delete'])
    @compo("$VIEW_ROOT.components.actions.delete", [
        "TABLE"         => $TABLE,
        "VARIABLE"      => $VARIABLE,
    ])
@endisset
