<div class="form-group{{ $errors->has($NAME.'_id') ? ' has-error' : '' }} @if(isset($HIDDEN) && $HIDDEN) hidden @endif @if(isset($CLASS) && $CLASS) {{$CLASS}} @endif">
    <label class="col-md-4 control-label">{{$TITLE}}</label>

    <div class="col-md-3">
        <input type="file" id="{{$NAME.'_id'}}" name="{{$NAME.'_id'}}"/>
        <div class="description">{{$DESCRIBE}}</div>

        @if ($errors->has($NAME.'_id'))
        <span class="help-block">
            <strong>{{ $errors->first($NAME.'_id') }}</strong>
        </span>
        @endif
    </div>
    @if(isset($act_res) && $act_res[$NAME])
    <div class="col-md-3">
        <div class="checkbox">
            <label>
                <input type="checkbox" name="removeImage-{{$NAME}}_id" value="1"/> {{trans('general.actions.removeImage')}}
            </label>
        </div>
        <img src="{{ url($act_res[$NAME]->url) }}" style="position:relative; height:{{$BOX_HEIGHT or 200}}px; width:100%;"/>
    </div>
    @endif
</div>
