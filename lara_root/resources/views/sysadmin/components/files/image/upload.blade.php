@pushonce('_styles:croppie')
    <style type="text/css">
        #cropi {
            -ms-transform: scale(0.5, 0.5); /* IE 9 */
            -webkit-transform: scale(0.5, 0.5); /* Safari */
            transform: scale(0.5, 0.5);
            border: 1px dashed blue;
        }
    </style>
@endpushonce
@pushonce('_scripts:croppie')
    <script type="text/javascript">
        var parentW = $('#cropi').width();
        var upload_url = "{{route('panel_upload',['page'=>$IMAGE_TYPE])}}";
        var waiting_icon = "{{assetlink('defaults/images/ajax_waiting.gif')}}";
        var view_height = Math.min(500,{{$imageTypes[$IMAGE_TYPE]->max_width or 300}});
        var view_width = Math.min(500,{{$imageTypes[$IMAGE_TYPE]->max_height or 300}});
        var bound_height = Math.max(Math.max(2*view_width,2*view_height),600);
        var nocopypaste = @if(isset($NOCOPYPASTE) && $NOCOPYPASTE == true) true @else false @endif ;
    </script>
    <script src="{{assetlink('vendor/Croppie/croppie_adder.min.js',true)}}" type="text/javascript"></script>
@endpushonce
@pushonce('scripts:croppie')
    <script type="text/javascript">
        var upload_url = "{{route('panel_upload',['page'=>$IMAGE_TYPE])}}";
        var waiting_icon = "{{assetlink('defaults/images/ajax_waiting.gif')}}";
        var view_width = {{$imageTypes[$IMAGE_TYPE]->max_width or 300}};
        var view_height = {{$imageTypes[$IMAGE_TYPE]->max_height or 300}};
        var bound_height = view_height+100;
        var bound_width = view_width+100;
        // var bound_height = Math.max(2*view_width,2*view_height);
    </script>
    <script src="{{assetlink('vendor/Croppie/croppie_adder.min.js',true)}}" type="text/javascript"></script>
@endpushonce

<div class="form-group{{ $errors->has($NAME) ? ' has-error' : '' }} @if(isset($HIDDEN) && $HIDDEN) hidden @endif @if(isset($CLASS) && $CLASS) {{$CLASS}} @endif">
    <label class="col-md-4 control-label">{{$TITLE}}</label>

    <div id="picture_{{$NAME}}" class="col-md-6 picture cropiTrigContainer">
        <input type="hidden" class='picture_id' name="{{$NAME}}" value="{{ $act_res[$NAME] or (isset($DEFAULT_VAL)?$DEFAULT_VAL:'')}}"/>
        <div>
            <img src="{{assetlink(isset($act_res[$NAME])&&isset($act_res[$ANCHOR]->url)?$act_res[$ANCHOR]->url : (isset($DEFAULT)?$DEFAULT:assetlink('defaults/images/no_image.jpg')) )}}" height="{{$BOX_HEIGHT or 50}}" class="cropiTrig"/>
              <a href="javascript:;" class="cropiTrig">{{trans('general.actions.newImage')}}</a> 
            | <a href="javascript:;" class="cropiTrigEdit" @if( !isset($act_res[$NAME]) || !isset($act_res[$ANCHOR]->url) || !isset($DEFAULT) || empty($DEFAULT) ) style="display:none;" @endif >{{trans('general.actions.editImage')}}</a> 
            @if(!isset($NOCOPYPASTE) || $NOCOPYPASTE == false)
            | <a href="javascript:;" class="cropiCopy" @if( !isset($act_res[$NAME]) || !isset($act_res[$ANCHOR]->url) ) style="display:none;" @endif >{{trans('general.actions.copyImage')}}</a>
            | <a href="javascript:;" class="cropiPaste" style="display: none;">{{trans('general.actions.pasteImage')}}</a>
            @endif
            | <a href="javascript:;" data-src="{{(isset($DEFAULT)?$DEFAULT:assetlink('defaults/images/no_image.jpg'))}}" class="cropiRemove">{{trans('general.actions.removeImage')}}</a>
        </div>
        <div class="description">{{$DESCRIBE}}</div>
        @if ($errors->has($NAME))
        <span class="help-block">
            <strong>{{ $errors->first($NAME) }}</strong>
        </span>
        @endif
    </div>
</div>
