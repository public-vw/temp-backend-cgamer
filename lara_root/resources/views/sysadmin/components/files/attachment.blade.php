@extends("$VIEW_ROOT.layouts.component")

@push('css')
<style>
    #img-{{ $NAME }}{
        position: relative;
        max-width: {{$IMAGE_BOX['width']['max'] ?? 300}}px;
        min-width: {{$IMAGE_BOX['width']['min'] ?? 200}}px;
        border: 1px solid {{ $IMAGE_BOX['colors']['border']  ?? '#ccc' }};
        border-radius: 5px;
        background-color: {{ $IMAGE_BOX['colors']['bg']  ?? '#eee' }};
        @if(!isset($VALUE)) display:none; @endif
    }
</style>
@endpush

@push('js')
    <script>
        $('#{{ $NAME }}').on('change',function(){
            $('#attachmentable_changed').val(true)
            @if($IMAGE ?? false)
                let imageId = 'img-{{ $NAME }}'
                let file = this.files[0]

                let reader = new FileReader();
                reader.onload = function (e) {
                    var image = new Image();
                    image.src = reader.result;

                    $('#' + imageId).prop('src', image.src);
                    $('#' + imageId).show();

                    image.onerror = function (e) {
                        console.log('Local load error', e)
                    }
                }
                reader.readAsDataURL(file);
            @endif
        })
    </script>
@endpush

@section('input-'.$rnd)

@if($TYPE_SLUG ?? false)
    @compo("$VIEW_ROOT.components.textfields.hidden",[
        "NAME"  => 'attachment_type_slug',
        "VALUE" => $TYPE_SLUG,
    ])
@endif
    @if($IMAGE ?? false)
        <img id="img-{{ $NAME }}" src="{{ $VALUE ?? '#' }}"/>
    @endif

    @compo("$VIEW_ROOT.components.textfields.hidden",[
        "NAME"  => 'attachmentable_changed',
        "VALUE" => false,
    ])

    @compo("$VIEW_ROOT.components.textfields.text", [
        "LABEL"      => '',
        "NAME"       => $NAME,
        "TYPE"       => 'file',
{{--        "MORE_CLASS" => 'form-control-file',--}}
    ])

    @if(!isset($NO_REMOVE) ?? false)
        @compo("$VIEW_ROOT.components.boxes.checkbox", [
            "NAME"  => 'attachmentable_remove',
            "VALUE" => false,
            "TITLE" => "Remove Attachment",
            "LABEL" => "",
        ])
    @endif

@endsection
