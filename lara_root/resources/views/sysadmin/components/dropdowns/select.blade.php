@extends("$VIEW_ROOT.layouts.component")

@section('input-'.$rnd)
    <select class="form-control {{ $CLASS ?? '' }}" id="{{ $NAME }}" name="{{ $NAME }}" {!! $MORE ?? '' !!}>
        @if(!empty($options))
            @if(isset($NULL_VALUE))
                <option value="">
                    {{$NULL_VALUE ?? ''}}
                </option>
            @endif

            @foreach($OPTIONS as $item => $title)

                @if (!empty(old($NAME, $value ?? '')))
                    <option {{ old($NAME, $value ?? '') == $item ? 'selected="selected"' : '' }}
                            value="{{ $item }}">
                        {{ $title }}
                    </option>
                @elseif (!empty($VALUE))
                    <option {{ $VALUE == $item ? 'selected="selected"' : '' }}
                            value="{{ $item }}">
                        {{ $title }}
                    </option>
                @else
                    <option value="{{ $item }}">
                        {{ $title }}
                    </option>
                @endif
            @endforeach
        @endif
    </select>
@endsection
