@extends("$VIEW_ROOT.layouts.component")

@push('js')
    <script>
        $(document).ready(function () {
            $('select[name="{{ $NAME }}"]').select2({
                @if($CONFIG['hide_searchbox'] ?? false)
                    minimumResultsForSearch: Infinity,
                @endif
                @if($CONFIG['allow_null'] ?? false)
                    placeholder: {
                        id       : '',
                        text     : '{{ $CONFIG['null_option_title'] ?? '---' }}',
                        selected : true,
                        search   : ''
                    },
                @endif
                data: $.map({!! json_encode($OPTIONS,JSON_FORCE_OBJECT) !!}, function (obj, key) {
                    return {
                        id : key,
                        selected : key=='{{ $VALUE ?? '' }}',
                        text : obj.toLowerCase().replace(/(?<= )[^\s]|^./g, a => a.toUpperCase())
                    }
                })
            });
        });
    </script>
@endpush

@section('input-'.$rnd)
    <select name="{{ $NAME }}" id="{{ $ID ?? $NAME }}" class="form-control {{ $CLASS ?? '' }}" {{ $REQUIRED ?? false ? 'required':'' }} @if($DISABLE ?? false) disabled @endif>
        {!! $MORE ?? '' !!} @if($DISABLE ?? false) disabled @endif>
        @if($CONFIG['allow_null'] ?? false)
            <option></option>
        @endif
    </select>
    @isset($HELPER)<small>{!! $HELPER !!}</small>@endisset
@endsection
