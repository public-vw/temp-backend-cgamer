@extends("$VIEW_ROOT.layouts.component")

@php
    $data[] = [
        'id' => '',
        'text' => '',
        'selected' => ($selected_id ?? 0) == 0,
    ];
    foreach($items as $item){
        $data[] = [
            'id' => $item->id,
            'text' => $item->title,
            'flag' => $item->flag_iso,
            'selected' => $item->id == ($selected_id ?? 0),
        ];
    }

    $data = json_encode($data);
@endphp

@pushonce('js:select2')
<script>
    var data = [
        {!! $data !!}
    ];
    var data = JSON.parse('{!! $data !!}');
    console.log(data);

    function formatState (country) {

        var $country = $(
            '<h3><span class="flag-icon flag-icon-' + country.flag + '"></span> ' + country.text + '</h3>'
        );
        return $country;
        function formatState (item) {
            var format ='<h5>' +
                '<span class="flag-icon flag-icon-' + item.flag + '"></span>' +
                '&nbsp;' + item.text +
                '</h5>';
            return $(format);
        }

        $("#country_id").select2({
            $("#{{$NAME}}").select2({
            data: data,
            templateResult: formatState,
            placeholder: {
                id: "",
                text: "Select Country ..."
                text: "{{ $placeholder }}"
            },
            allowClear: true,
            theme: 'classic'
        })

</script>
@endpushonce

@section('input-'.$rnd)
    <select class="form-control" id="{{ $NAME }}" name="{{ $NAME }}" {!! $more ?? '' !!}>
        @if(!empty($options))
            @if(isset($null_value))
                <option value="">
                    {{$null_value ?? ''}}
                </option>
            @endif

            @foreach($options as $item)
                @php($titles = implode(' ', array_intersect_key(is_array($item) ? $item : $item->toArray(), array_fill_keys(explode('|', $title_fields), ''))))

                @if (!empty(old($NAME, $value ?? '')))
                    <option {{ old($NAME, $value ?? '') == $item[$value_field] ? 'selected="selected"' : '' }}
                            value="{{ $item[$value_field] }}">
                        {{ $titles }}
                    </option>
                @elseif (!empty($current_value))
                    <option {{ $current_value == $item[$value_field] ? 'selected="selected"' : '' }}
                            value="{{ $item[$value_field] }}">
                        {{ $titles }}
                    </option>
                @else
                    <option value="{{ $item[$value_field] }}">
                        {{ $titles }}
                    </option>
                @endif
            @endforeach
        @endif
    </select>
@endsection

