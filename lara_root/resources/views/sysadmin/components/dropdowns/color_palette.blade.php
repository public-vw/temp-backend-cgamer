@extends("$VIEW_ROOT.layouts.component")

@pushonce('js:color-palette')
<script>
    $(function (){
        $('.color-palette-wrapper > span.toggle-selected').click(function(){
            $(this).closest('.color-palette-wrapper').find('.dropdown').slideToggle(100);
        });

        $('.color-palette-wrapper li').click(function(){
            $(this).closest('.dropdown').slideUp(100);
            var box = $(this).closest('.color-palette-wrapper').find('.toggle-selected:first');
            var input = $(this).closest('.color-palette-wrapper').find('input:first');
            $(box).html($(this).html());
            $(input).val($(this).attr('data-indx'));
        });

    });
</script>
@endpushonce

@pushonce('css:color-palette')
<style>
    .color-palette-wrapper {
        position: relative;
    background: #FFF;
    color: #2e2e2e;
    outline: none;
        cursor: pointer;
    }
    .color-palette-wrapper > span {
        width: 100%;
        display: block;
        padding: 5px;
        padding-top: 0;
    }
    .color-palette-wrapper > span > span.color-box {
        padding: 0 12px;
        margin-right: 5px;
    }
    .color-palette-wrapper > span:after {
        content: "";
        width: 0;
        height: 0;
        position: absolute;
        right: 16px;
        transform: rotate(180deg);
        top: calc(50% + 4px);
        margin-top: -6px;
        border-width: 6px 6px 0 6px;
        border-style: solid;
        border-color: #2e2e2e transparent;
    }

    .color-palette-wrapper .dropdown {
        position: absolute;
        display: none;
        z-index: 10;
        bottom: 100%;
        left: 0;
        right: 0;
        height: 200px;
        margin: 0;
        overflow-y: auto;
    }

    .color-palette-wrapper .dropdown li {
        display: block;
        text-decoration: none;
        color: #2e2e2e;
        padding: 5px;
        cursor: pointer;
    }

    .color-palette-wrapper .dropdown li > span.color-box {
        padding: 0 12px;
        margin-right: 5px;
    }

    .color-palette-wrapper .dropdown li:hover {
        background: #f9f9f9;
        cursor: pointer;
    }
</style>
@endpushonce

@section('input-'.$rnd)
<div class="color-palette-wrapper form-control">
    <input type="hidden" name="{{ $NAME }}" value="{{ $VALUE ?? '' }}">
    <span class="toggle-selected">
        @isset($VALUE)
            <span class="color-box" style="font-weight:bolder; color: {{ $COLORS[$VALUE][0] }}; background-color: {{ $COLORS[$VALUE][1] }}">ABC</span>
            <span style="color: {{ $COLORS[$VALUE][0] }}">{{ $COLORS[$VALUE][2] }}</span>
        @else
            <span>Choose a Color ...</span>
        @endisset
    </span>
    <ul class="dropdown form-control">
        @foreach($COLORS as $i => $color)
            <li data-indx="{{ $i }}">
                <span class="color-box" style="font-weight:bolder; color: {{ $color[0] }}; background-color: {{ $color[1] }}">ABC</span>
                <span style="color: {{ $color[0] }}">{{ $color[2] }}</span>
            </li>
        @endforeach
    </ul>
</div>
@endsection
