<button
    type="{{ $TYPE ?? 'button' }}"
    class="btn {{ $CLASS ?? 'btn-default' }}{{ $ICON ?? '' }}"
    @if(isset($ONCLICK)) onclick="{!! $ONCLICK !!}" @endif
    {{ $more ?? '' }}
>{!! $TITLE ?? 'Untitled' !!}</button>
