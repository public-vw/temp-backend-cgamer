<a href="{{ $URL ?? 'javascript:;' }}"
   @isset($ID) id="{{ $ID }}" @endisset
   @isset($TARGET) target="{{ $TARGET }}" @endisset
   class="{{ $CLASS ?? 'btn btn-link' }}{{ $ICON ?? '' }}"
   @isset($ONCLICK) onclick="{!! $ONCLICK !!}" @endisset
>
@if(isset($RAW_HTML) && $RAW_HTML === TRUE)
    {!! $TITLE ?? $DEFAULT_TITLE !!}
@else
    {{ $TITLE ?? $DEFAULT_TITLE }}
@endif
</a>
