@push('css')
    <style>
        table td form{
            cursor: pointer;
            display:inline-block;
        }
    </style>
@endpush

<table class="table table-striped table-bordered table-hover table-checkable">
    <thead>
        <tr>
            @foreach($COLUMNS as $col_name=>$col_title)
                <th>{{ $col_title }}</th>
            @endforeach
            <th>{{__('lists.actions.header')}}</th>
        </tr>
    </thead>
    <tbody>
    @foreach($ITEMS as $item)
        <tr>
            @foreach($COLUMNS as $col_name=>$col_title)
                <td>{{$item[$col_name]}}</td>
            @endforeach
            <td nowrap>
                <form action="{{route($view.'.show',[$item])}}" method="get" class="inline_form"><a class="btn" onclick="$(this).closest('.inline_form').submit()">{{ __('lists.actions.show') }}</a></form>
                &nbsp;|&nbsp;<form action="{{route($view.'.edit',[$item])}}" method="get" class="inline_form"><a class="btn" onclick="$(this).closest('.inline_form').submit()">{{ __('lists.actions.edit') }}</a></form>
                &nbsp;|&nbsp;<form action="{{route($view.'.destroy',[$item])}}" method="post" class="inline_form" onsubmit="return confirm('{{ __('lists.actions.delete_confirm_message') }}');">@csrf @method('DELETE')<a class="btn" onclick="$(this).closest('.inline_form').submit()">{{ __('lists.actions.delete') }}</a></form>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
{{ $ITEMS->links() }}
