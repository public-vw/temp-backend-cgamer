@extends("$VIEW_ROOT.layouts.component")

@section('input-'.$rnd)
    <div>
        @foreach($items as $key => $data)
            <label for="{{$NAME}}-{{$key}}">
                <input type="radio"
                       @if(isset($disable) && $disable) class="disabled" @endif
                       id="{{$NAME}}-{{$key}}"
                       @if(!isset($disable) || !$disable) name="{{ $NAME }}" @endif
                       value="{{ $key }}"
                       {{ old($NAME, $default ?? '') == $key  ? 'checked' : '' }}
                       @if(!isset($disable) || !$disable)
                           @if(isset($required) && $required) required @endif
                       @else
                           disabled='disabled'
                       @endif
                       {!! $more ?? '' !!}
                />
                {{ $data['title'] }}
            </label>
            <br>
        @endforeach
    </div>
@endsection
