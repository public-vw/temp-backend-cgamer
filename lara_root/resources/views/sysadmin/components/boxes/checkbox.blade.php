@extends("$VIEW_ROOT.layouts.component")

@push('js')
    <script>
        $(function (){
            $('#{{ $ID ?? $NAME }}').click(function(){
                $('#'+$(this).attr('data-hv')).val($(this).prop('checked') ? $(this).attr('value'):'0');
            });
        });
    </script>
@endpush

@section('input-'.$rnd)
    <input type="hidden" id="hv-{{ $ID ?? $NAME }}" name="{{ $NAME }}" value="{{ ($CHECKED ?? false) ? ($VALUE ?? '1')  : '0' }}"/>
    <div @isset($SWITCH) class="custom-control custom-switch" @endisset>
        <input
            data-hv="hv-{{ $ID ?? $NAME }}"
            id="{{ $ID ?? $NAME }}"
            type="checkbox"

            value="{{ $VALUE ?? '1' }}"
            {{ ((isset($CHECKED) && $CHECKED) || old($NAME, $DEFAULT ?? '') == $NAME)  ? 'checked' : '' }}

            @if(isset($CLASS)) class="{{ $CLASS }}" @endif

            @if(!isset($DISABLE) || !$DISABLE)
                @if(isset($REQUIRED) && $REQUIRED) required @endif
                placeholder="{{$PLACEHOLDER ?? ''}}"
            @else
                readonly='readonly'
                disabled='disabled'
            @endif
            @isset($ONCLICK) onclick="{!! $ONCLICK !!}" @endisset

            {!! $MORE ?? '' !!}
        />
        <label @isset($SWITCH) class="custom-control-label" for="{{ $ID ?? $NAME }}" @endisset>{!! $TITLE !!}</label>
    </div>
    @isset($HELPER)<small>{!! $HELPER !!}</small>@endisset
@endsection
