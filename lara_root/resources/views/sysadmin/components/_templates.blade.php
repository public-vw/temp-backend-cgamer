## SELECT2 | SINGLE OPTION | ARRAY ##
@compo("$VIEW_ROOT.components.dropdowns.select2.single_array",[
    'ID' => 'element_id',
    'MORE' => 'data-bind="more"',
    "NAME" => "field_name",
    'CLASS' => 'classname',
    'REQUIRED' => true,
    "OPTIONS" => json_encode([
        [
            'id' => 1,
            'text' => 'text1',
            'selected' => true,
            'search' => '',
        ],
    ]),

    'CONFIG' => [
        'hide_searchbox' => true,
        'allow_null' => true,
        'null_option_title' => '',
    ],
])
##############


## SELECT2 | SINGLE OPTION | AJAX ##
@compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax",[
    'ID' => 'element_id',
    "NAME" => "field_name",
    "AJAX_URL" => route('.ajax'),
    'CLASS' => 'classname',
    'REQUIRED' => true,
    'FORMAT' => '<h2>item</h2>',   #item is keyword
    'MORE' => 'data-bind="more"',

    'VALUE' => [
        "id"   => $item->id,
        "text" => $item->title,
    ],

    'CONFIG' => [
        'ajax_datatype' => 'json', # json | jsonp
        'ajax_method' => 'post',   # get | post

        'hide_searchbox' => true,
        'allow_null' => true,
        'null_option_title' => '',
    ],
])
##############










## BUTTON ##
@compo("$VIEW_ROOT.components.clickables.button",[
"TITLE" => __('forms.TABLE.button.action'),
'CLASS' => 'btn-default',
'TYPE' => 'button',
'ONCLICK' => 'doSomething()',
'data_confirm' => 'alert message',
])
##############
## SUBMIT ##
@compo("$VIEW_ROOT.components.clickables.submit",[
"TITLE" => __('forms.TABLE.button.action'),
"CLASS" => 'btn-primitive',
'ONCLICK' => 'doSomething()',
])
##############

## DROPDOWN | CONFIGS ##
@compo("$VIEW_ROOT.components.dropdowns.select2.config_key", [
    "NAME"          => 'status',
    "LABEL"         => __("Status"),
    "PLACEHOLDER"   => __("Select Status ..."),
    "OPTIONS"       => config('enums.seo_positions_status'),
    "REQUIRED"      => true,
    "VALUE"   => config('enums.seo_positions_status_default'),
])
##############


## TABLE ##
##############
## DATATABLE ##
##############

## ADDRESS PICKER ##
##############
## GMAP PICKER ##
##############

## DATE BOTH ##
##############
## DATE GREG ##
##############
## DATE PERS ##
##############

## DATETIME BOTH ##
##############
## DATETIME GREG ##
##############
## DATETIME PERS ##
##############

## CHECKBOX SINGLE ##
@compo("$VIEW_ROOT.components.boxes.checkbox",[
'no_label' => true,
"name" => 'NAME',
"title" => __('forms.TABLE.NAME.checkbox'),
"default" => '1',
"value" => '1',
])
##############
## CHECKBOX GROUP ##
##############
## RADIO GROUP ##
@compo("$VIEW_ROOT.components.boxes.radio",[
"NAME" => 'NAME',
"required" => true,
"title" => __('forms.TABLE.NAME.radio'),
'items' => [
'iran'   => [ //KEY is VALUE of Radio
'title' => 'ایرانی',
],
'other' => [
'title' => 'غیرایرانی',
],
],
'default' => 'iran',
])
##############
## SELECTBOX STATIC ##
@compo("$VIEW_ROOT.components.dropdowns.select",[
"name" => 'NAME',
"title" => __('forms.TABLE.NAME.select'),
"required" => true,
'key_field' => 'id',
'value_field' => 'id',
'title_fields' => 'title',
'options' => [
[
"id" => 'number',
"title" => __('forms.TABLE.NAME.option.ID'),
],
[
"id" => 'number',
"title" => __('forms.TABLE.NAME.option.ID'),
]
],
])
##############
## SELECTBOX DYNAMIC ##
@compo("$VIEW_ROOT.components.dropdowns.select",[
"name" => 'NAME',
"title" => __('forms.TABLE.NAME.select'),
"required" => true,
"key_field" => 'id',
"value_field" => 'id',
"title_fields" => 'field1|field2',
'options' => $TABLE_NAMEs,
])
##############

## LISTBOX ##
##############

## TEXT ##
@compo("$VIEW_ROOT.components.textfields.text",[
"NAME" => 'NAME',
"TITLE" => __('forms.TABLE.NAME.text'),
"DEFAULT" => 'string',
"REQUIRED" => true,
])
##############

## TEXT ARRAY ##
##############
## TEXT INV ##
##############
## EMAIL ##
@compo("$VIEW_ROOT.components.textfields.text",[
"NAME" => 'NAME',
"TITLE" => __('forms.TABLE.NAME.email'),
"DEFAULT" => 'string',
"REQUIRED" => true,
'TYPE' => 'email',
])
##############

## NUMBER ##
@compo("$VIEW_ROOT.components.textfields.text",[
"NAME" => 'NAME',
"TITLE" => __('forms.TABLE.NAME.number'),
"DEFAULT" => 'string',
"REQUIRED" => true,
'TYPE' => 'number',
])
##############
## PASSWORD ##
@compo("$VIEW_ROOT.components.textfields.text",[
"NAME" => 'NAME',
"TITLE" => __('forms.TABLE.NAME.password'),
"DEFAULT" => 'string',
'TYPE' => 'password',
"REQUIRED" => true,
])
##############
## IP ##
##############

## CAPTION ##
##############
## LINK ##
@compo("$VIEW_ROOT.components.clickables.link", [
    "URL"   => route("{$VIEW_ROOT}.{$VIEW_FOLDERS[1]}.edit", $variable),
    "TITLE" => "$variable->uuid",
])
##############
## HIDDEN ##
##############

## TEXTBOX ##
##############
## RICHBOX ##
##############
## ADMIN COMMENT ##
##############

## FILE ##
##############
## IMAGE ONE ##
@compo("$VIEW_ROOT.components.files.image",[
"NAME" => 'NAME',
"required" => true,
"title" => __('forms.TABLE.NAME.image'),
])
##############
## IMAGE ONE BOXED ##
##############
## IMAGE BULK ##
##############
## IMAGE BULK BOXED ##
##############







## STATIC SELECT ##
##############

## DYNAMIC SELECT ##
##############
