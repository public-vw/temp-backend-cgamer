<div class="panel">
    @if(!empty($title))
    <div class="panel-title">
        <strong>
            {!! $title !!}
        </strong>
    </div>
    @endif
    <div class="panel-body">
        {!! $body !!}
    </div>
</div>
