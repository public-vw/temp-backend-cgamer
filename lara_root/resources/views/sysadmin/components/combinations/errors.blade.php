@pushonce('css:all_errors')
    <style>
        .warning{
            border:1px solid red;
        }
    </style>
@endpushonce

@if($errors->any())
    @foreach($errors->all() as $error)
        <div>{{$error}}</div>
    @endforeach
    <br>
@endif
