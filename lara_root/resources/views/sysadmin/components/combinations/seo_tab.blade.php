<div class="tab-pane {{ $active ? 'active' : '' }}" id="{{ $id }}">
    <div class="form-group">
        <label class="control-label">{{ $textLabel }}</label>
        @compo("$VIEW_ROOT.components.textfields.text", [
            "NAME" => '{{ $textName }}',
            "PLACEHOLDER" => $textPlaceholder
        ])
    </div>
    <div class="form-group">
        <label class="control-label">{{ $textareaLabel }}</label>
        @compo("$VIEW_ROOT.components.textfields.textarea", [
            "NAME" => '{{ $textareaName }}',
            "ROWS" => '3',
            "PLACEHOLDER" => $textareaPlaceholder
        ])
    </div>
    <div class="form-group form-group-image">
        <label class="control-label">{{ $imageLabel }}</label>
        @compo("$VIEW_ROOT.components.textfields.text", [
            "NAME" => '{{ $imageName }}',
            "TYPE" => 'file',
            "MORE_CLASS" => 'form-control-file',
        ])
        {{-- TODO improve upload image --}}
    </div>
</div>
