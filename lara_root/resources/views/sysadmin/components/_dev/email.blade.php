@extends("$VIEW_ROOT.layouts.component")

@section('input-'.$rnd)
    <input type="email" class="form-control"
           id="{{ $NAME }}" name="{{ $NAME }}"
           value="{{ $value ?? old($NAME, $default ?? '') }}"
           @if(isset($required) && $required) required @endif
           placeholder="{{ $placeholder ?? '' }}"
            {!! $more ?? '' !!}
    />
@endsection
