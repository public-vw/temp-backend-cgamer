@extends("$VIEW_ROOT.layouts.component")

@section('input-'.$rnd)
    <input type="password" class="form-control m-input"
           id="{{ $NAME }}" name="{{ $NAME }}"
           value="{{ $value ?? old($NAME, $default ?? '') }}"
           @if(isset($alias))data-inputmask-alias="{{$alias}}"@endif
           @if(isset($required) && $required) required @endif
           placeholder="{{$placeholder ?? ''}}"
            {!! $more ?? '' !!}
    />
@endsection
