@extends("$VIEW_ROOT.layouts.component")

@pushonce('stack_css:persian-datepicker')
<link href="{{ asset('vendor/persian-datepicker/persian-datepicker.min.css') }}" rel="stylesheet" type="text/css"/>
@endpushonce

@pushonce('stack_js:persian-datepicker')
<script src="{{ asset('vendor/persian-datepicker/persian-date.min.js') }}"></script>
<script src="{{ asset('vendor/persian-datepicker/persian-datepicker.min.js') }}"></script>

<script>
  $('.persian-datepicker').persianDatepicker({
    initialValueType: 'persian',
    format: 'YYYY-MM-DD HH:mm:ss',
    initialValue: false,
    timePicker: {
      enabled: true,
    }
  })
</script>
@endpushonce

@section('input-'.$rnd)
    <input type="text" class="form-control persian-datepicker" readonly="readonly"
           style="direction: {{ $dir ?? 'ltr' }}"
           id="{{ $NAME }}" name="{{ $NAME }}"
           value="{{ $value ?? old($NAME, $default ?? '') }}"
           @if(isset($required) && $required) required @endif
           placeholder="{{ $placeholder ?? '' }}"
            {!! $more ?? '' !!}
    />
@endsection
