@extends("$VIEW_ROOT.layouts.component")

@section('input-'.$rnd)
    <textarea rows="{{ $rows ?? 5 }}" class="form-control"
              id="{{ $NAME }}" name="{{ $NAME }}"
              placeholder="{{ $placeholder ?? '' }}"
              @if(isset($required) && $required) required @endif
              @if(isset($readonly) && $readonly) readonly="readonly" @endif
            {!! $more ?? '' !!}
        >{{$value ?? old($NAME, $default ?? '')}}</textarea>
@endsection
