@extends("$VIEW_ROOT.layouts.component")

@section('input-'.$rnd)
    <textarea rows="{{ $ROWS ?? 5 }}"
              class="form-control {{ $CLASS ?? ''}}"
              id="{{ $NAME }}" name="{{ $NAME }}"
              placeholder="{{ $PLACEHOLDER ?? '' }}"
              @if(isset($REQUIRED) && $REQUIRED) required @endif
              @if(isset($READONLY) && $READONLY) readonly="readonly" @endif
            {!! $MORE ?? '' !!}
        >{{$VALUE ?? old($NAME, $DEFAULT ?? '')}}</textarea>
    @isset($HELPER)<small>{!! $HELPER !!}</small>@endisset
@endsection
