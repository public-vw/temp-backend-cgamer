<input
    type="hidden"
    id="{{ $ID ?? $NAME }}"
    name="{{ $NAME }}"
    value="{{ $VALUE }}"
    {!! $MORE ?? '' !!}
/>
