@extends("$VIEW_ROOT.layouts.component")

@section('input-'.$rnd)
    <input
        id="{{ $id ?? $NAME }}"
        name="{{ $NAME }}"
        type="{{ $TYPE ?? 'text' }}"
        class="form-control @error($NAME) is-invalid @enderror {{ $CLASS ?? '' }} {{ $MORE_CLASS ?? '' }}"
        value="{{ $VALUE ?? old($NAME, $DEFAULT ?? '') }}"
        autocomplete=@if(!isset($AUTOCOMPLETE) || $AUTOCOMPLETE)"{{ Str::slug(config('app.name', 'Laravel')) }}_{{ $NAME }}"@else"off"@endif

        @if(!isset($DISABLE) || !$DISABLE)
            @if(isset($REQUIRED) && $REQUIRED) required @endif
            placeholder="{{$PLACEHOLDER ?? ''}}"
        @else
            readonly='readonly'
            disabled='disabled'
        @endif
        {!! $MORE ?? '' !!}
    />
    @isset($HELPER)<small>{!! $HELPER !!}</small>@endisset
@endsection
