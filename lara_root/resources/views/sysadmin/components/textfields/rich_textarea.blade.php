@extends("$VIEW_ROOT.layouts.component")
@push('js')
    <script src="{{ asset('vendor/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset("assets/$VIEW_ROOT/js/editor_config.js") }}"></script>
@endpush
@section('input-'.$rnd)
    <textarea rows="{{ $ROWS ?? 5 }}"
              class="form-control {{ $CLASS ?? '' }} d-none has-ckeditor"
              id="{{ $NAME }}" name="{{ $NAME }}"
              placeholder="{{ $PLACEHOLDER ?? '' }}"
              @if(isset($REQUIRED) && $REQUIRED) required @endif
              @if(isset($READONLY) && $READONLY) readonly="readonly" @endif
        {!! $MORE ?? '' !!}
        >{{$VALUE ?? old($NAME, $DEFAULT ?? '')}}</textarea>
    @isset($HELPER)<small>{!! $HELPER !!}</small>@endisset
@endsection
