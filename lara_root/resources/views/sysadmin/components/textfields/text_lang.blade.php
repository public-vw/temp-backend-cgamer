@extends("$VIEW_ROOT.layouts.component")

@section('input-'.$rnd)
    @if(isset($class) && in_array($NAME,('\\App\\Models\\'.$class)::$translatable ))
        @foreach(\App\Models\Language::getList() as $lang)
            <div>
                <b>{{ $lang->title }} : </b>
                <input
                        id="{{ $id ?? $NAME }}-{{ $lang['id'] }}"
                        name="{{ $NAME }}[{{ $lang['id'] }}]"
                        type="text"
                        class="form-control @error($NAME[$lang['id']]) is-invalid @enderror"
                        value="{{ $value ?? old($NAME[$lang['id']], $default ?? '') }}"
                        autocomplete=@if(!isset($autocomplete) || $autocomplete) "{{ config('app.name', 'Laravel') }}_{{ $NAME }}_{{ $lang['id'] }}" @else "off" @endif

                @if(!isset($disable) || !$disable)
                    @if(isset($required) && $required) required @endif
                    placeholder="{{$placeholder ?? ''}}"
                @else
                    readonly='readonly'
                    disabled='disabled'
                @endif
                {!! $more ?? '' !!}
                />
            </div>
        @endforeach

    @else
        <input
                id="{{ $id ?? $NAME }}"
                name="{{ $NAME }}"
                type="text"
                class="form-control @error($NAME) is-invalid @enderror"
                value="{{ $value ?? old($NAME, $default ?? '') }}"
                autocomplete=@if(!isset($autocomplete) || $autocomplete) "{{ config('app.name', 'Laravel') }}_{{ $NAME }}" @else "off" @endif

        @if(!isset($disable) || !$disable)
            @if(isset($required) && $required) required @endif
            placeholder="{{$placeholder ?? ''}}"
        @else
            readonly='readonly'
            disabled='disabled'
        @endif
        {!! $more ?? '' !!}
        />
    @endif

@endsection
