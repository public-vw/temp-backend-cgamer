@pushonce('css:calendar')
<link rel="stylesheet" type="text/css" href="{{asset('vendor/enBank.calendar/enBank.calendar.css')}}"/>
@endpushonce

@pushonce('js:calendar')
<script src="{{asset('vendor/enBank.calendar/calendar.js')}}"></script>
<script src="{{asset('vendor/enBank.calendar/calendar-fa.js')}}"></script>
@endpushonce

@push('scripts')
<script type="text/javascript">
    $('#{{$NAME}}').datetimepicker({
        weekStart: 1,
        todayBtn:  1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView:1,
            forceParse: 0,
        showMeridian: 1,
        format: '{{$FORMAT}}'
    });
</script>
@endpush

@section('input-'.$rnd)
    <input id="{{$id ?? $NAME}}"
           type='text'
           class="form-control form-control-inline input-medium date-picker enRight @error($NAME) is-invalid @enderror"
           name="{{$NAME}}"
           value="{{ $value ?? old($NAME, $default ?? '') }}"

           @if(!isset($disable) || !$disable)
               @if(isset($required) && $required) required @endif
               placeholder="{{$placeholder ?? ''}}"
           @else
               readonly='readonly'
               disabled='disabled'
           @endif
           {!! $more ?? '' !!}
    />
@endsection
