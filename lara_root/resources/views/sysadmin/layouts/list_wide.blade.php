@extends("$VIEW_ROOT.layouts.base")

@push('js')
    {{ $dataTable->scripts() }}
@endpush

@section('content')
    <div class="container-fluid">
        <div class="d-flex justify-content-between mb20">
            <h3 class="title-bar">{{ __("All " . Str::plural($MODEL)) }}
                @empty($NO_CREATE)
                    <a href="{{route("{$VIEW_ROOT}.{$TABLE}.create")}}" class="btn btn-primary"><i class="fa fa-plus-circle"></i>&nbsp;Create</a>
                @endempty
                @isset($MORE_BUTTONS)
                    @foreach($MORE_BUTTONS as $title => $route)
                        <a href="{{route($route)}}" class="btn btn-primary"><i class="fa fa-plus-circle"></i>&nbsp;{!! $title !!}</a>
                    @endforeach
                @endisset
            </h3>
        </div>

        @if($bulkActions ?? false)
{{-- TODO: create bulk actions --}}
{{--            @include("{$VIEW_ROOT}.includes.bulk_actions")--}}
        @endif

        @if($headers ?? false)
{{-- TODO: create status headers --}}
{{--            @include("{$VIEW_ROOT}.includes.table_status_header")--}}
        @endif

        @include("$VIEW_ROOT.includes.alerts")

        <div class="panel">
            <div class="panel-body">
                <form class="bravo-form-item">
                    <div class="table-responsive">
                        {{ $dataTable->table(['class' => 'table table-hover']) }}
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
