@extends("$VIEW_ROOT.layouts.base")

@section('title')
    @if($panel = session('panel',false))
        Panel: {{ $panel->title }} |
    @endif
    {{ Str::plural($MODEL) }} | Edit | id: {{ $VARIABLE->id }}
@endsection

@section('right-title')
    @compo("$VIEW_ROOT.components.actions.delete", [
        "TABLE"         => $TABLE,
        "VARIABLE"      => $VARIABLE,
    ])
@endsection

@section('content')
    <form action="{{ route("$VIEW_ROOT." . $TABLE . ".update", $VARIABLE) }}" method="post" @isset($HAS_UPLOAD) enctype="multipart/form-data" @endisset>
        @method('PUT')
        @csrf

        @include("$VIEW_ROOT.components.combinations.errors")

        @include("$VIEW_ROOT.includes.alerts")

        <div class="lang-content-box">
            <div class="row">
                <div class="col-12">
                    @yield('inside')
                    <div class="text-right div-submit">
                        @compo("$VIEW_ROOT.components.clickables.link",[
                            "TITLE" => '<i class="fa fa-arrow-circle-o-left"></i>&nbsp;'. __('Back'),
                            "URL" => route("{$VIEW_ROOT}.{$TABLE}.index"),
                            'RAW_HTML' => true,
                        ])
                        @compo("$VIEW_ROOT.components.clickables.multisubmit",[
                            "DEFAULT" => [
                                "stay" => '<i class="fa fa-check-circle-o"></i>&nbsp;'. __('Save'),
                            ],
                            'BTNCLASS' => 'btn-primary',
                            "ITEMS" =>  [
                                "back" => '<i class="fa fa-check-circle"></i>&nbsp;'. __('Save & Done'),
                            ],
                        ])
                    </div>
                </div>
            </div>
        </div>

    </form>
@endsection
