@extends('mainframe')

@section('bootstrap-css','')
@section('bootstrap-js','')
@section('jquery','')
@section('flags','')
@section('noty','')

@section('head')
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset("assets/$VIEW_ROOT/css/app.css") }}" rel="stylesheet">
@endsection

@push('mainjs')
    <script src="{{ asset("assets/$VIEW_ROOT/js/app.js") }}"></script>
@endpush

@push('css')
    <style>
        span.avatar{
            background-color: {{ config("enums.random_colors.".auth()->user()->random_color)[0] }};
            color: {{ config("enums.random_colors.".auth()->user()->random_color)[1] }};
        }
    </style>
@endpush

@section('body')
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <span class="navbar-role">
                    {{ucwords($VIEW_ROOT)}} Panel
                </span>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    <span class="avatar">{{ ucwords(substr(auth()->user()->name, 0, 1)) }}</span>
                                    {{ auth()->user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('sysadmin.profile.edit') }}">
                                        {{ __('User Profile') }}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('sysadmin.profile.show_password') }}">
                                        {{ __('Change Password') }}
                                    </a>

                                    <hr/>

                                    @multirole()
                                    @foreach(auth()->user()->allowedPanels() as $panel)
                                        @if(Route::has("$panel.home"))
                                            <a class="dropdown-item" target="_blank" href="{{ route("$panel.home") }}">
                                                {{ucwords($panel)}} Panel <i class="fa fa-external-link"></i>
                                            </a>
                                        @else
                                            <a class="dropdown-item disabled" disabled="disabled" href="javascript:;">
                                                {{ucwords($panel)}} Panel
                                            </a>
                                        @endif
                                    @endforeach

                                    <hr/>
                                    @endmultirole

                                    <a class="dropdown-item" href="{{ route('auth.logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('auth.logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>

                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            <div class="container">
                <div class="row justify-content-center">
                    @if(auth() && app()->getLocale() != 'fa')
                        @include("$VIEW_ROOT.sidebars.frame")
                    @endif
                    <div class="col-md-9">
                        <div class="card">
                            <div class="card-header">
                                @yield('title')
                                <div class="float-right">@yield('right-title')</div>
                            </div>

                            <div class="card-body">
                                @if (session('status'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session('status') }}
                                    </div>
                                @endif

                                @yield('content')
                            </div>
                        </div>
                    </div>
                    @if(app()->getLocale() == 'fa')
                        @include("$VIEW_ROOT.sidebars.frame")
                    @endif
                </div>
            </div>
        </main>
    </div>
@endsection
