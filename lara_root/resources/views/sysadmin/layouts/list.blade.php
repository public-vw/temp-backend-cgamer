@extends("$VIEW_ROOT.layouts.base")

@section('title')
    @if($panel = session('panel',false))
        Panel: {{ $panel->title }} |
    @endif
    {{ $MODEL }} | List
@endsection
@section('right-title')
    <b>Count: </b>{{ $items->total() }}
@endsection

@section('content')
    @include("$VIEW_ROOT.includes.alerts")

    <div class="row mb-3">
        @yield('action')
    </div>

    @if($items->total())
        <table class="table">
            <thead>
                @yield('thead')
            </thead>
            <tbody>
                @yield('tbody')
            </tbody>
        </table>
        {{ method_exists(get_class($items),'links')? $items->links() : '' }}
    @else
        <i>Nothing to show</i>
    @endif
@endsection

