@extends("$VIEW_ROOT.layouts.base")

@section('title')
    @if($panel = session('panel',false))
        Panel: {{ $panel->title }} |
    @endif
    {{ Str::plural($MODEL) }} | {{ $TITLE ?? 'Create' }}
@endsection

@section('content')
    <form action="{{ route($FORM_ROUTE ?? "$VIEW_ROOT.$TABLE.store") }}" method="post" @isset($HAS_UPLOAD) enctype="multipart/form-data" @endisset>
        @csrf

        @include("$VIEW_ROOT.includes.alerts")

        <div class="lang-content-box">
            <div class="row">
                <div class="col-12">
                    @yield('inside')
                    <div class="text-right div-submit">
                        @compo("$VIEW_ROOT.components.clickables.link",[
                            "TITLE" => '<i class="fa fa-arrow-circle-o-left"></i>&nbsp;'. __('Back'),
                            "URL" => route("{$VIEW_ROOT}.{$TABLE}.index"),
                            'RAW_HTML' => true,
                        ])
                        @compo("$VIEW_ROOT.components.clickables.multisubmit",[
                            "DEFAULT" => [
                                "stay" => '<i class="fa fa-check-circle-o"></i>&nbsp;'. __('Create & New'),
                            ],
                            'BTNCLASS' => 'btn-success',
                            "ITEMS" =>  [
                                "back" => '<i class="fa fa-check-circle"></i>&nbsp;'. __('Create & Done'),
                                "break1"  => null,
                                "edit" => '<i class="fa fa-pencil-square-o"></i> '. __('Create & Edit'),
                            ],
                        ])
                    </div>
                </div>
            </div>
        </div>

    </form>
@endsection
