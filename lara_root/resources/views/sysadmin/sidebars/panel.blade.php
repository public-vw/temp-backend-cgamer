@php
$sidebar = [
    'menu_placeholders' => 'Menu Placeholders',
    'menus' => 'Menus',
];
@endphp

    <div class="card border-info mb-3">
        <div class="card-header bg-info">
            Panel: {{session('panel')->title}}
            <a href="{{ route('sysadmin.panels.exit') }}" class="float-right btn btn-primary btn-sm" aria-label="Close">
                Close
            </a>
        </div>
        <div class="card-body">
            <ul class="sidebar">
                @foreach($sidebar as $route => $title)
                    @if(is_null($title))
                        <li><hr/></li>
                        @continue(1)
                    @endif
                    @if(\Request::route()->getName() == "sysadmin.$route.index" || \Request::route()->getName() == $route)
                        <li><b>{{ $title }}</b></li>
                    @else
                        @if(Route::has("sysadmin.$route.index"))
                            <li><a href="{{ route("sysadmin.$route.index") }}">{{ $title }}</a></li>
                        @elseif(Route::has("sysadmin.$route"))
                            <li><a href="{{ route("sysadmin.$route") }}">{{ $title }}</a></li>
                        @else
                            <li><a href="javascript:;">{{ $title }}</a></li>
                        @endif
                    @endif
                @endforeach
            </ul>
        </div>
    </div>
