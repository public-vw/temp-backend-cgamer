@php
    $sidebar = [
        'panels'            => 'Panels',
        'break1'            => null,
        'users'             => 'Users',
        'roles'             => 'Roles',
        'permissions'       => 'Permissions',
        'api_tokens'        => 'API Tokens',
        'break2'            => null,
        'logs'              => 'Logs',
        'failed_jobs'       => 'Faild Jobs',
        'break3'            => null,
        'server_configs'    => 'Server Configs',
        'alerts'            => 'Alert Settings',
        'break4'            => null,
        'domains'           => 'Domains',
        'site_brands'       => 'Branding',
        'setting_groups'    => 'Setting Groups',
        'settings'          => 'Settings',
        'break5'            => null,
        'bot_connections'   => 'Robot Connections',
        'telegram_robot_messages'   => 'Telegram Messages',
        'break6'            => null,
        'fake_records'      => 'Fake Records',
        'structures'        => 'Structure',
        'compo_generators'  => 'Component Generators',
        'break7'            => null,
        'backups'           => 'Backup/Restore',
    ];
@endphp

<div class="card">
    <div class="card-body">
        <ul class="sidebar">
            @foreach($sidebar as $route => $title)
                @if(is_null($title))
                    <li>
                        <hr/>
                    </li>
                    @continue(1)
                @endif
                @if(\Request::route()->getName() == "sysadmin.$route.index" || \Request::route()->getName() == $route)
                    <li><b>{{ $title }}</b></li>
                @else
                    @if(Route::has("sysadmin.$route.index"))
                        <li><a href="{{ route("sysadmin.$route.index") }}">{{ $title }}</a></li>
                    @elseif(Route::has("sysadmin.$route"))
                        <li><a href="{{ route("sysadmin.$route") }}">{{ $title }}</a></li>
                    @else
                        <li><a href="javascript:;">{{ $title }}</a></li>
                    @endif
                @endif
            @endforeach
            <li>
                <hr/>
            </li>
            <li><a href="{{ route('auth.logout') }}">{{ __('sysadmin/sidebar.logout') }}</a></li>
        </ul>
    </div>
</div>
