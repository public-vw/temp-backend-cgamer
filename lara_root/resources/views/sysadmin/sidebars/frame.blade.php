<div class="col-md-3">

@if(\Session::has('panel'))
    @include("$VIEW_ROOT.sidebars.panel")
@endisset

@include("$VIEW_ROOT.sidebars.main")

</div>
