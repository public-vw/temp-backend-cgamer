@extends("$VIEW_ROOT.layouts.base")

@section('title')
    Backup, Restore | Management
@endsection
{{--@section('right-title')--}}
{{--    <b>Count: </b>{{ $items->total() }}--}}
{{--@endsection--}}

@push('css')
    <style>
        .col-3 {
            display: flex;
        }

        .valign-bottom {
            align-self: flex-end;
        }
    </style>
@endpush

@section('content')
    @isset($result)
    <div class="row">
        <div class="col">
            <h3>Result</h3>
            <div><pre>{!! $result !!}</pre></div>
        </div>
    </div>
    <hr>
    @endisset
    <div class="row">
        <div class="col">
            <h3>Backup</h3>
        </div>
    </div>
    <form action="{{ route('sysadmin.backups.backup') }}" method="POST">
        @csrf
        <div class="row">
            <div class="col-9">
                @compo("$VIEW_ROOT.components.boxes.raw_checkbox",[
                    "NAME"      => 'upload',
                    "TITLE"     => __('Upload to Remote Folder'),
                    "CHECKED"   => true,
                ])
                @compo("$VIEW_ROOT.components.boxes.raw_checkbox",[
                    "NAME"      => 'nolocal',
                    "TITLE"     => __('Don\'t keep Local Backup'),
                    "CHECKED"   => true,
                ])
                @compo("$VIEW_ROOT.components.boxes.raw_checkbox",[
                    "NAME"      => 'silence',
                    "TITLE"     => __('Don\'t send Telegram Alert'),
                    "CHECKED"   => true,
                ])
            </div>
            <div class="col-3 valign-bottom">
                <button type="submit" class="btn btn-primary btn-block">Run Backup</button>
            </div>
        </div>
    </form>
    <hr>
    <div class="row">
        <div class="col">
            <h3>Restore</h3>
        </div>
    </div>
    <form action="{{ route('sysadmin.backups.restore') }}" method="POST">
        @csrf
        <div class="row">
            <div class="col-9">
                @compo("$VIEW_ROOT.components.textfields.text",[
                    "NAME"         => 'fileterm',
                    "LABEL"        => __('Backup File Term'),
                    "PLACEHOLDER"  => 'VxL (the code of the backup file)',
                    "REQUIRED"     => false,
                ])
                @compo("$VIEW_ROOT.components.boxes.raw_checkbox",[
                    "NAME"      => 'drop',
                    "TITLE"     => __('Drop previous records'),
                    "CHECKED"   => true,
                ])
                @compo("$VIEW_ROOT.components.boxes.raw_checkbox",[
                    "NAME"      => 'nobackup',
                    "TITLE"     => __('Don\'t take backup of current DB'),
                    "CHECKED"   => false,
                ])
            </div>
            <div class="col-3 valign-bottom">
                <button type="submit" class="btn btn-warning btn-block">Run Restore</button>
            </div>
        </div>
    </form>

@endsection

