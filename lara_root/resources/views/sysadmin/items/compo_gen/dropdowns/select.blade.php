@extends("$VIEW_ROOT.layouts.base")

@section('title',"Component Generators | Create $componentTitle")

@push('js')
    <script src="{{ asset('assets/sysadmin/js/component_generator.js') }}"></script>
@endpush

@section('content')
    <input type="hidden" id="compo_title" value="$VIEW_ROOT.components.dropdowns.select"/>

    <div class="row">
        @compo("$VIEW_ROOT.components.textfields.text",[
            "NAME"      => 'NAME',
            "CLASS"     => 'compo',
            "LABEL"     => 'Name',
            "REQUIRED"  => true,
        ])

        @compo("$VIEW_ROOT.components.textfields.text",[
            "NAME"      => 'CLASS',
            "CLASS"     => 'compo',
            "LABEL"     => 'Class',
            "REQUIRED"  => false,
        ])

        @compo("$VIEW_ROOT.components.textfields.textarea",[
            "NAME"      => 'OPTIONS',
            "CLASS"     => 'compo',
            "LABEL"     => 'Options',
            "REQUIRED"  => false,
        ])

        @compo("$VIEW_ROOT.components.textfields.text",[
            "NAME"      => 'NULL_VALUE',
            "CLASS"     => 'compo',
            "LABEL"     => 'Null Title',
            "REQUIRED"  => false,
        ])

        @compo("$VIEW_ROOT.components.textfields.text",[
            "NAME"      => 'VALUE',
            "CLASS"     => 'compo',
            "LABEL"     => 'Current Value',
            "REQUIRED"  => false,
        ])

        @compo("$VIEW_ROOT.components.textfields.textarea",[
            "NAME"      => 'MORE',
            "CLASS"     => 'compo',
            "LABEL"     => 'More Attributes',
            "REQUIRED"  => false,
        ])
    </div>
    <hr>
    <div class="row">
        @compo("$VIEW_ROOT.components.textfields.textarea",[
            "CONTAINER_CLASS" => 'col-md-12',
            "NAME"      => 'result',
            "READONLY"  => true,
            "LABEL"     => 'Result',
        ])
        <div class="col-md-4">
            @compo("$VIEW_ROOT.components.clickables.button",[
                "CONTAINER_CLASS" => 'col-md-4',
                "CLASS"     => 'btn-success',
                "TITLE"     => 'Copy to clipboard',
                "ONCLICK"   => 'copyToClipboard()',
            ])
        </div>
    </div>
@endsection
