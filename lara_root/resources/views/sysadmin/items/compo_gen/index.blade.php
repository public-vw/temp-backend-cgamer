@extends("$VIEW_ROOT.layouts.base")

@section('title','Component Generators | List')

@section('content')
    <div class="row">
        @foreach($componentGroups as $group_slug => $details)
            <div class="col-12">
                <h5>{{ $details[0] }}</h5>
                <div class="row row-cols-3">
                    @foreach($details['children'] as $slug => $component)
                        <div class="col">
                            @compo("$VIEW_ROOT.components.clickables.square_button",[
                                "TITLE"     => $component,
                                "HREF"      => route("$VIEW_ROOT.compo_generators.form",['component' => "$group_slug.$slug"]),
                            ])
                        </div>
                    @endforeach
                </div>
            </div>
        @endforeach
    </div>
@endsection
