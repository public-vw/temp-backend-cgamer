@extends("$VIEW_ROOT.layouts.create",[
    'MODEL' => 'Domain',
    'TABLE' => $VIEW_FOLDERS[1],
])

@section('inside')
    <div class="row">
        @compo("$VIEW_ROOT.components.textfields.text", [
            "LABEL"         => __("Title"),
            "NAME"          => 'title',
            "PLACEHOLDER"   => '',
            "REQUIRED"      => true,
        ])

        @compo("$VIEW_ROOT.components.textfields.text", [
            "LABEL"         => __("Domain Address"),
            "NAME"          => 'domain_address',
            "PLACEHOLDER"   => '',
            "REQUIRED"      => true,
        ])
    </div>
    <div class="row form-group">
        @compo("$VIEW_ROOT.components.boxes.checkbox", [
            "TITLE"   => __("Active"),
            "NAME"    => 'active',
            "CLASS"   => 'custom-control-input',
            "SWITCH"  => true,
        ])
    </div>
@endsection
