@extends("$VIEW_ROOT.layouts.edit",[
    'MODEL'     => 'Domain',
    'VARIABLE'  => $domain,
    'TABLE'     => $VIEW_FOLDERS[1],
])

@section('inside')
    <div class="row">
        @compo("$VIEW_ROOT.components.textfields.text", [
            "LABEL"         => __("Title"),
            "NAME"          => 'title',
            "VALUE"         => $domain->title,
            "PLACEHOLDER"   => '',
            "REQUIRED"      => true,
        ])

        @compo("$VIEW_ROOT.components.textfields.text", [
            "LABEL"         => __("Domain Address"),
            "NAME"          => 'domain_address',
            "VALUE"         => $domain->domain_address,
            "PLACEHOLDER"   => '',
            "REQUIRED"      => true,
        ])
    </div>
    <div class="row form-group">
        @compo("$VIEW_ROOT.components.boxes.checkbox", [
            "TITLE"    => __("Active"),
            "NAME"     => 'active',
            "CLASS"    => 'custom-control-input',
            "SWITCH"   => true,
            "CHECKED"  => $domain->active,
        ])
    </div>
@endsection
