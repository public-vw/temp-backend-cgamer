@if($domain = $variable->domain)
    @php($route = Route::has("$VIEW_ROOT.domains.show") ? route("$VIEW_ROOT.domains.show", $domain) : route("$VIEW_ROOT.domains.edit", $domain))
@endif

@compo("$VIEW_ROOT.components.clickables.link", [
    "URL"   => $route ?? '#',
    "TITLE" => $domain->title ?? '',
])
