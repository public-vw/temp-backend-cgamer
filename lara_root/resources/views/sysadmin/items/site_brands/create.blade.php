@extends("$VIEW_ROOT.layouts.create",[
    'MODEL' => 'Site Brand',
    'TABLE' => $VIEW_FOLDERS[1],
])

@section('inside')
    <div class="row">
        @compo("$VIEW_ROOT.components.textfields.text", [
            "LABEL"         => __("Title"),
            "NAME"          => 'title',
            "PLACEHOLDER"   => '',
            "REQUIRED"      => true,
        ])

        @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
            "NAME"          => 'domain_id',
            "LABEL"         => __("Domain"),
            "PLACEHOLDER"   => __("Select Domain ..."),
            "AJAX_URL"      => route("$VIEW_ROOT.domains.list.all"),
        ])
    </div>
    <div class="row">
        @compo("$VIEW_ROOT.components.textfields.text", [
            "LABEL"         => __("Slug"),
            "NAME"          => 'slug',
            "PLACEHOLDER"   => '',
            "REQUIRED"      => true,
        ])

        @compo("$VIEW_ROOT.components.textfields.text", [
            "LABEL"         => __("Order"),
            "NAME"          => 'order',
            "TYPE"          => 'number',
            "REQUIRED"      => true,
        ])
    </div>
    <div class="row">
        @compo("$VIEW_ROOT.components.textfields.text", [
            "LABEL"         => __("Language"),
            "NAME"          => 'lang',
            "PLACEHOLDER"   => 'e.g. en',
        ])

        @compo("$VIEW_ROOT.components.dropdowns.select2.config_key", [
            "NAME"          => 'direction',
            "LABEL"         => __("Direction"),
            "PLACEHOLDER"   => __("Select Direction ..."),
            "OPTIONS"       => config('enums.settings_direction'),
        ])
    </div>
    <div class="row">
        @compo("$VIEW_ROOT.components.dropdowns.select2.config_key", [
            "NAME"          => 'mode',
            "LABEL"         => __("Mode"),
            "PLACEHOLDER"   => __("Select Mode ..."),
            "OPTIONS"       => config('enums.settings_mode'),
        ])
    </div>
{{-- TODO: fix null value when have two rich_textarea --}}
    <div class="row">
        @compo("$VIEW_ROOT.components.textfields.rich_textarea", [
            "NAME"              => 'value',
            "LABEL"             => __('Value'),
            "CONTAINER_CLASS"   => 'col-12',
            "ROWS"              => '10',
            "MORE"              => 'cols="30"',
        ])
    </div>
    <div class="row">
        @compo("$VIEW_ROOT.components.textfields.rich_textarea", [
            "NAME"              => 'description',
            "LABEL"             => __('Description'),
            "CONTAINER_CLASS"   => 'col-12',
            "ROWS"              => '10',
            "MORE"              => 'cols="30"',
        ])
    </div>
@endsection
