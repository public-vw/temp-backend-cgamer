@extends("$VIEW_ROOT.layouts.edit",[
    'MODEL'     => 'Site Brand',
    'VARIABLE'  => $siteBrand,
    'TABLE'     => $VIEW_FOLDERS[1],
])

@section('inside')
    <div class="row">
        @compo("$VIEW_ROOT.components.textfields.text", [
            "LABEL"         => __("Title"),
            "NAME"          => 'title',
            "VALUE"         => $siteBrand->title,
            "PLACEHOLDER"   => '',
            "REQUIRED"      => true,
        ])

        @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
            "NAME"          => 'domain_id',
            "LABEL"         => __("Domain"),
            "PLACEHOLDER"   => __("Select Domain ..."),
            "AJAX_URL"      => route("$VIEW_ROOT.domains.list.all"),
            "VALUE"         => [
                'id'    => $siteBrand->domain_id ?? '',
                'text'  => $siteBrand->domain->title ?? '',
            ],
        ])
    </div>
    <div class="row">
        @compo("$VIEW_ROOT.components.textfields.text", [
            "LABEL"         => __("Slug"),
            "NAME"          => 'slug',
            "VALUE"         => $siteBrand->slug,
            "PLACEHOLDER"   => '',
        ])

        @compo("$VIEW_ROOT.components.textfields.text", [
            "LABEL"         => __("Order"),
            "NAME"          => 'order',
            "VALUE"         => $siteBrand->order,
            "TYPE"          => 'number',
        ])
    </div>
    <div class="row">
        @compo("$VIEW_ROOT.components.textfields.text", [
            "LABEL"         => __("Language"),
            "NAME"          => 'lang',
            "VALUE"         => $siteBrand->lang,
            "PLACEHOLDER"   => 'e.g. en',
        ])

        @compo("$VIEW_ROOT.components.dropdowns.select2.config_key", [
            "NAME"          => 'direction',
            "LABEL"         => __("Direction"),
            "PLACEHOLDER"   => __("Select Direction ..."),
            "OPTIONS"       => config('enums.settings_direction'),
            "VALUE"         => $siteBrand->direction,
        ])
    </div>
    <div class="row">
        @compo("$VIEW_ROOT.components.dropdowns.select2.config_key", [
            "NAME"          => 'mode',
            "LABEL"         => __("Mode"),
            "PLACEHOLDER"   => __("Select Mode ..."),
            "OPTIONS"       => config('enums.settings_mode'),
            "VALUE"         => $siteBrand->mode,
        ])
    </div>
    <div class="row">
        @compo("$VIEW_ROOT.components.textfields.rich_textarea", [
            "NAME"              => 'value',
            "VALUE"             => $siteBrand->value,
            "LABEL"             => __('Value'),
            "CONTAINER_CLASS"   => 'col-12',
            "ROWS"              => '10',
            "MORE"              => 'cols="30"',
        ])
    </div>
    <div class="row">
        @compo("$VIEW_ROOT.components.textfields.rich_textarea", [
            "NAME"              => 'description',
            "VALUE"             => $siteBrand->description,
            "LABEL"             => __('Description'),
            "CONTAINER_CLASS"   => 'col-12',
            "ROWS"              => '10',
            "MORE"              => 'cols="30"',
        ])
    </div>
@endsection
