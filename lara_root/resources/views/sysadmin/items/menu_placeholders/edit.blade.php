@extends("$VIEW_ROOT.layouts.edit", [
    'MODEL'     => 'Menu Placeholder',
    'TABLE'     => $VIEW_FOLDERS[1],
    'VARIABLE'  => $item,
])

@section('inside')
    <div class="row">
        @compo("$VIEW_ROOT.components.textfields.text",[
            "NAME"      => 'title',
            "LABEL"     => __('Title'),
            "REQUIRED"  => true,
            "VALUE"     => $item->title,
        ])
    </div>
@endsection
