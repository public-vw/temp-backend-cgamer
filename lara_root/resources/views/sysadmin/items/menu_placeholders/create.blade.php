@extends("$VIEW_ROOT.layouts.create", [
    'MODEL'     => 'Menu Placeholder',
    'TABLE'     => $VIEW_FOLDERS[1],
])

@section('inside')
    <div class="row">
        @compo("$VIEW_ROOT.components.textfields.text",[
            "NAME"      => 'title',
            "LABEL"     => __('Title'),
            "REQUIRED"  => true,
        ])
    </div>
@endsection
