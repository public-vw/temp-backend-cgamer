@extends("$VIEW_ROOT.layouts.create",[
    'MODEL' => 'Structure',
    'TITLE' => 'Create Field',
    'TABLE' => $VIEW_FOLDERS[1],
    'FORM_ROUTE' => "$VIEW_ROOT.structures.store.field",
])

@section('inside')
    <div class="row">
        @compo("$VIEW_ROOT.components.textfields.hidden",[
            "NAME"      => 'type',
            "VALUE"     => 'field',
        ])
        @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
            "NAME"          => 'parent_id',
            "LABEL"         => __("Parent Model"),
            "PLACEHOLDER"   => __("Select Parent Model ..."),
            "AJAX_URL"      => route("$VIEW_ROOT.structures.list.all"),
            "DEPENDS_VAL"   => 'model',
        ])
    </div>
    <div class="row">
        @compo("$VIEW_ROOT.components.textfields.text",[
            "NAME"      => 'title',
            "LABEL"     => __('Field Title'),
            "REQUIRED"  => true,
        ])
    </div>
@endsection
