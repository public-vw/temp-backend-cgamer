@extends("$VIEW_ROOT.layouts.list_wide",[
    'MODEL' => 'Structure',
    'TABLE' => $VIEW_FOLDERS[1],
    'NO_CREATE' => true,
    'MORE_BUTTONS' => [
        'Create Model' => "{$VIEW_ROOT}.structures.create.model",
        'Create Field' => "{$VIEW_ROOT}.structures.create.field",
    ],
])
