@extends("$VIEW_ROOT.layouts.base")

@section('title')
    Structure | Check Model | {{ $item->title }}
@endsection

@section('content')
    <div class="row row-cols-2">
        <div class="col">
            <h2 class="pb-2">Database</h2>
            <div class="row">
                <ul>
                    <li>migrations: <div class="text-info">{!! nl2br(implode("\n",$item->migrations())) !!}</div></li>
                    <li>models: <div class="text-info">{!! nl2br(implode("\n",$item->models())) !!}</div></li>
                    <li>factories: <div class="text-info">{!! nl2br(implode("\n",$item->factories())) !!}</div></li>
                </ul>
            </div>
        </div>

        <div class="col">
            <h2 class="pb-2">Routes</h2>
            <div class="row">
                <div class="col">
                    <div class="text-info">{!! nl2br(implode("\n",$item->routes())) !!}</div>
                </div>
            </div>
        </div>

        <div class="col">
            <h2 class="pb-2">Controllers</h2>
            <div class="row">
                <div class="col">
                    <li>controllers: <div class="text-info">{!! nl2br(implode("\n",$item->controllers())) !!}</div></li>
                    <li>custom requests: <div class="text-info">{!! nl2br(implode("\n",$item->requests())) !!}</div></li>
                    <li>datatables: <div class="text-info">{!! nl2br(implode("\n",$item->datatables())) !!}</div></li>
                    <li>transformers: <div class="text-info">{!! nl2br(implode("\n",$item->transformers())) !!}</div></li>
                </div>
            </div>
        </div>

        <div class="col">
            <h2 class="pb-2">Views</h2>
            <div class="row">
                <div class="col">
                    <div class="text-info">{!! nl2br(implode("\n",$item->views())) !!}</div>
                </div>
            </div>
        </div>
    </div>
@endsection
