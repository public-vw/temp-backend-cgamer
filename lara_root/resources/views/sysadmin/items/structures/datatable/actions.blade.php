@if($variable->type == 'model')
@compo("$VIEW_ROOT.components.clickables.link", [
"URL"   => route("{$VIEW_ROOT}.structures.show.model", $variable),
"TITLE" => "check",
"CLASS" => 'btn btn-sm btn-primary',
])
@endif
