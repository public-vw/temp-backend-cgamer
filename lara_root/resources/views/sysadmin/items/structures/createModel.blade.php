@extends("$VIEW_ROOT.layouts.create",[
    'MODEL' => 'Structure',
    'TITLE' => 'Create Model',
    'TABLE' => $VIEW_FOLDERS[1],
    'FORM_ROUTE' => "$VIEW_ROOT.structures.store.model",
])

@push('css')
    <style>
        h3 .description {
            font-size: 11px;
            color: #666;
        }
    </style>
@endpush

@section('inside')
    <div class="row">
        @compo("$VIEW_ROOT.components.textfields.hidden",[
            "NAME"      => 'type',
            "VALUE"     => 'model',
        ])

        @compo("$VIEW_ROOT.components.textfields.text",[
            "NAME"      => 'title',
            "LABEL"     => __('Model Title'),
            "REQUIRED"  => true,
        ])
    </div>
    <hr>
    <h3 class="pb-2">Panels
        @compo("$VIEW_ROOT.components.combinations.checkbox_inverter",['TARGET'=>'.panels-box'])
    </h3>
    <div class="row panels-box">
        @compo("$VIEW_ROOT.components.boxes.raw_checkbox",[
            "ID"        => "panels_sysadmin",
            "NAME"      => "panels[]",
            "TITLE"     => 'sysadmin',
            "VALUE"     => 'sysadmin',
            "CHECKED"   => false,
        ])
        @foreach($panels as $panel)
            @compo("$VIEW_ROOT.components.boxes.raw_checkbox",[
                "ID"        => "panels_{$panel->title}",
                "NAME"      => "panels[]",
                "TITLE"     => $panel->title,
                "VALUE"     => $panel->title,
                "CHECKED"   => false,
            ])
        @endforeach
    </div>
    <hr>
    <h3 class="pb-2">Data Model
        @compo("$VIEW_ROOT.components.combinations.checkbox_inverter",['TARGET'=>'.datamodel-box'])
    </h3>
    <div class="row datamodel-box">
        @compo("$VIEW_ROOT.components.boxes.raw_checkbox",[
            "ID"        => "options_model",
            "NAME"      => 'options[model]',
            "TITLE"     => 'Create Model',
            "CHECKED"   => false,
        ])

        @compo("$VIEW_ROOT.components.boxes.raw_checkbox",[
            "ID"        => "options_migration",
            "NAME"      => 'options[migration]',
            "TITLE"     => 'Create Migration',
            "CHECKED"   => false,
        ])

        @compo("$VIEW_ROOT.components.boxes.raw_checkbox",[
            "ID"        => "options_factory",
            "NAME"      => 'options[factory]',
            "TITLE"     => 'Create Factory',
            "CHECKED"   => false,
        ])

        @compo("$VIEW_ROOT.components.boxes.raw_checkbox",[
            "ID"        => "options_seeder",
            "NAME"      => 'options[seeder]',
            "TITLE"     => 'Create Seeder',
            "CHECKED"   => false,
        ])

        @compo("$VIEW_ROOT.components.boxes.raw_checkbox",[
            "ID"        => "options_permissions",
            "NAME"      => 'options[permissions]',
            "TITLE"     => 'Create Permissions',
            "CHECKED"   => false,
        ])
    </div>
    <hr>
    <h3 class="pb-2">Options
        @compo("$VIEW_ROOT.components.combinations.checkbox_inverter",['TARGET'=>'.options-box'])
        <div class="description">all ( except <b>transforms</b> ) need specifying <b>panel</b></div>
    </h3>
    <div class="row options-box">
        @compo("$VIEW_ROOT.components.boxes.raw_checkbox",[
            "ID"        => "options_route",
            "NAME"      => 'options[route]',
            "TITLE"     => 'Create Route',
            "CHECKED"   => true,
        ])

        @compo("$VIEW_ROOT.components.boxes.raw_checkbox",[
            "ID"        => "options_view",
            "NAME"      => 'options[view]',
            "TITLE"     => 'Create View',
            "CHECKED"   => true,
        ])

        @compo("$VIEW_ROOT.components.boxes.raw_checkbox",[
            "ID"        => "options_controller",
            "NAME"      => 'options[controller]',
            "TITLE"     => 'Create Controller',
            "CHECKED"   => true,
        ])
    </div>
    <div class="row options-box">
        @compo("$VIEW_ROOT.components.boxes.raw_checkbox",[
            "ID"        => "options_datatable",
            "NAME"      => 'options[datatable]',
            "TITLE"     => 'Create Data Table',
            "CHECKED"   => true,
        ])

        @compo("$VIEW_ROOT.components.boxes.raw_checkbox",[
            "ID"        => "options_request",
            "NAME"      => 'options[request]',
            "TITLE"     => 'Create Custom Request',
            "CHECKED"   => true,
        ])

        @compo("$VIEW_ROOT.components.boxes.raw_checkbox",[
            "ID"        => "options_transformer",
            "NAME"      => 'options[transformer]',
            "TITLE"     => 'Create Transformer',
            "CHECKED"   => true,
        ])
    </div>
@endsection
