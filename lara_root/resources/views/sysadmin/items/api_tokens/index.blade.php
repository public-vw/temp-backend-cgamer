@extends("$VIEW_ROOT.layouts.list",[
    'MODEL' => 'Personal Access Token',
    'TABLE' => $VIEW_FOLDERS[1],
])

@section('right-title','')
@section('action','')

@section('thead')
    <tr>
        <th>Device Name</th>
        <th>Mobile</th>
        <th>Token</th>
        <th>Abilities</th>
        <th>User Roles</th>
        <th>{{__('lists.actions.header')}}</th>
    </tr>
@endsection

@section('tbody')

    @foreach($items as $item)
        <tr>
            <td>{{ $item->name }} ({{ $item->id }})</td>
            <td>{{ $item->tokenable->mobile }}</td>
            <td>{{ strrev(Str::limit(strrev($item->token),10)) }}</td>
            <td>{{ $item->abilities ? implode('|',$item->abilities) : '---' }}</td>
            <td>{{ trim(str_replace('"', '', $item->tokenable->getRoleNames()), '[]') }}</td>
            <td>
                <form action="{{ route("$VIEW_ROOT.api_tokens.force_logout", $item) }}" method="post" class="d-inline">
                    @csrf
                    @method('DELETE')
                    @compo("$VIEW_ROOT.components.clickables.link", [
                        "ONCLICK"  => "if(confirm('".__('Are you sure?')."'))$(this).closest('form').submit();",
                        'TITLE'     => "Kill Token",
                        'CLASS' => 'btn btn-sm btn-warning',
                    ])
                </form>
            </td>
        </tr>
    @endforeach
@endsection

@section('action','')
