<div class="d-flex">
    @compo("$VIEW_ROOT.components.clickables.link", [
        "URL"   => route("{$VIEW_ROOT}.{$VIEW_FOLDERS[1]}.check", $variable),
        "TITLE" => "check",
        "CLASS" => 'btn btn-sm btn-info',
    ])
    <span class="px-1">|</span>
    @compo("$VIEW_ROOT.components.actions.delete", [
        "VARIABLE"  => $variable,
        "TABLE"     => $VIEW_FOLDERS[1],
    ])
</div>
