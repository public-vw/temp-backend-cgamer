@extends("$VIEW_ROOT.layouts.create", [
    'MODEL'     => 'Permission',
    'TABLE'     => $VIEW_FOLDERS[1],
])

@section('inside')
    <div class="row">
        @compo("$VIEW_ROOT.components.textfields.text",[
            "NAME"      => 'name',
            "LABEL"     => __('Permission Name'),
            "REQUIRED"  => true,
        ])
    </div>
@endsection
