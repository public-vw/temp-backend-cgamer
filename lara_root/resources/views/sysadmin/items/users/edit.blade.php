@extends("$VIEW_ROOT.layouts.edit",[
    'MODEL'       => 'User',
    'VARIABLE'    => $user,
    'DONT_DELETE' => $user->hasRole('sysadmin@web|sysadmin@api'),
    'TABLE'     => $VIEW_FOLDERS[1],
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'parent_id',
                    'LABEL'         => __("Parent"),
                    'PLACEHOLDER'   => __("No Parent"),
                    "AJAX_URL"      => route("$VIEW_ROOT.users.list.all"),
                    'VALUE'         => [
                        'id'   => $user->parent_id ?? null,
                        'text' => $user->parent->username ?? null,
                    ],
                    'CONFIG'        => [
                        'allow_null' => true,
                        'null_option_title' => '- No Parent -',
                    ],
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Completion"),
                    "NAME"          => 'completion',
                    "VALUE"          => $user->completion,
                    "PLACEHOLDER"   => '0~100',
                    "REQUIRED"      => true,
                ])
                @compo("$VIEW_ROOT.components.dropdowns.select2.config_key", [
                    "NAME"          => 'status',
                    "LABEL"         => __("Status"),
                    "PLACEHOLDER"   => __("Select Status ..."),
                    "OPTIONS"       => config('enums.users_status'),
                    "VALUE"         => $user->status,
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Name"),
                    "NAME"          => 'name',
                    "VALUE"         => $user->name,
                    "PLACEHOLDER"   => '',
                    "REQUIRED"      => true,
                ])

                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Username"),
                    "NAME"          => 'username',
                    "VALUE"         => $user->username,
                    "PLACEHOLDER"   => '',
                    "REQUIRED"      => true,
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Email"),
                    "NAME"          => 'email',
                    "VALUE"         => $user->email,
                    "PLACEHOLDER"   => '',
                ])

                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Ref Code"),
                    "NAME"          => 'ref_code',
                    "VALUE"         => $user->ref_code,
                    "PLACEHOLDER"   => '',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Mobile"),
                    "NAME"          => 'mobile',
                    "VALUE"         => $user->mobile,
                    "PLACEHOLDER"   => '',
                ])

                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Referer Id"),
                    "NAME"          => 'referer_id',
                    "VALUE"         => $user->referer_id,
                    "PLACEHOLDER"   => '',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Telegram UID"),
                    "NAME"          => 'telegram_uid',
                    "VALUE"         => $user->telegram_uid,
                    "PLACEHOLDER"   => '',
                ])

                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Password"),
                    "NAME"          => 'password',
                    "PLACEHOLDER"   => '',
                    "HELPER"        => 'don\'t fill to not change the password',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.boxes.checkbox", [
                    "TITLE"         => __("Has Two Step Auth"),
                    "NAME"          => 'use_two_factor_auth',
                    "CHECKED"       => $user->use_two_factor_auth,
                    "HELPER"        => "Google Secret Code: ".$user->google_secret_key,
                ])

                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Re-Enter Password"),
                    "NAME"          => 'password_confirmation',
                    "TYPE"          => 'password',
                    "PLACEHOLDER"   => '',
                ])
            </div>
            {{-- TODO create phone_verification --}}
            {{-- TODO create email_verification --}}
        @endslot
    @endcomponent
@endsection
