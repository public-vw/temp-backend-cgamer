@extends("$VIEW_ROOT.layouts.base")

@section('title')
    User Roles | Management
@endsection

@section('content')
    <form action="{{ route("$VIEW_ROOT.users.roles.update", $user) }}" method="post">
        @method('PUT')
        @csrf

        @include("$VIEW_ROOT.includes.alerts")

        <div class="lang-content-box">
            <div class="row">
                <div class="col-12">
                    @component("$VIEW_ROOT.components.panel")
                        @slot('title', '')

                        @slot('body')
                            <div class="row">
                                <div class="col">
                                    Fullname: <b>{{ $user->name }}</b><br>
                                    Username: <b>{{ $user->username }}</b><br>
                                    Nickname: <b>{{ $user->nickname }}</b><br>
                                </div>
                            </div>
                            <hr>
                            <div class="row form-group">
                                @foreach($roles as $role)
                                    @compo("$VIEW_ROOT.components.boxes.raw_checkbox", [
                                    "TITLE"         => "{$role->name}",
                                    "NAME"          => 'roles[]',
                                    "VALUE"         => $role->id,
                                    "CHECKED"       => $user->hasRole($role),
                                    ])
                                @endforeach
                            </div>
                        @endslot
                    @endcomponent
                    <div class="text-right div-submit">
                        @compo("$VIEW_ROOT.components.clickables.link",[
                            "TITLE" => '<i class="fa fa-arrow-circle-o-left"></i>&nbsp;'. __('Back'),
                            "URL" => route("{$VIEW_ROOT}.users.index"),
                            'RAW_HTML' => true,
                        ])
                        @compo("$VIEW_ROOT.components.clickables.multisubmit",[
                            "DEFAULT" => [
                                "stay" => '<i class="fa fa-check-circle-o"></i>&nbsp;'. __('Save'),
                            ],
                            'BTNCLASS' => 'btn-primary',
                            "ITEMS" =>  [
                                "back" => '<i class="fa fa-check-circle"></i>&nbsp;'. __('Save & Done'),
                            ],
                        ])
                    </div>
                </div>
            </div>
        </div>

    </form>
@endsection

