@extends("$VIEW_ROOT.layouts.create_wide",[
    'MODEL' => 'User',
    'TABLE' => $VIEW_FOLDERS[1],
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Name"),
                    "NAME"          => 'name',
                    "PLACEHOLDER"   => '',
                    "REQUIRED"      => true,
                ])

                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Username"),
                    "NAME"          => 'username',
                    "PLACEHOLDER"   => '',
                    "REQUIRED"      => true,
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'attachment_type',
                    "LABEL"         => __("Attachment Type"),
                    "PLACEHOLDER"   => __("Select Attachment Type ..."),
                    "AJAX_URL"      => route("$VIEW_ROOT.attachment_types.list.all"),
                    'CONFIG'        => [
                        'allow_null' => true,
                    ],
                ])

                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Ref Code"),
                    "NAME"          => 'ref_code',
                    "PLACEHOLDER"   => '',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Avatar"),
                    "NAME"          => 'file',
                    "TYPE"          => 'file',
                ])

                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Email"),
                    "NAME"          => 'email',
                    "PLACEHOLDER"   => '',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Mobile"),
                    "NAME"          => 'mobile',
                    "PLACEHOLDER"   => '',
                ])

                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Referer Id"),
                    "NAME"          => 'referer_id',
                    "PLACEHOLDER"   => '',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Telegram UID"),
                    "NAME"          => 'telegram_uid',
                    "PLACEHOLDER"   => '',
                ])

                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Password"),
                    "NAME"          => 'password',
                    "TYPE"          => 'password',
                    "PLACEHOLDER"   => '',
                    "REQUIRED"      => true,
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.dropdowns.select2.config_key", [
                    "NAME"          => 'status',
                    "LABEL"         => __("Status"),
                    "PLACEHOLDER"   => __("Select Status ..."),
                    "OPTIONS"       => config('enums.users_status'),
                ])

                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Re-Enter Password"),
                    "NAME"          => 'password_confirmation',
                    "TYPE"          => 'password',
                    "PLACEHOLDER"   => '',
                    "REQUIRED"      => true,
                ])
            </div>
            {{-- TODO create google_secret_key --}}
            {{-- TODO create phone_verification --}}
            {{-- TODO create email_verification --}}
            {{-- TODO create use_two_factor_auth --}}
        @endslot
    @endcomponent
@endsection
