@extends("$VIEW_ROOT.layouts.base")

@section('content')
    <div class="container">

        @include("$VIEW_ROOT.includes.alerts")

        <div class="row">
            <div class="col-md-4 mb-3">

                <div class="card">
                    <div class="card-body">
                        <div class="d-flex flex-column align-items-center text-center">
                            <img src="https://bootdey.com/img/Content/avatar/avatar7.png" alt="{{ $user->name }} Avatar" class="rounded-circle" width="150">
                            {{-- TODO fix avatar --}}
                            {{-- <img src="{{ attachmentUrl($user->avatar) }}" alt="{{ $user->name }} Avatar" class="rounded-circle" width="150">--}}
                            <div class="mt-3">
                                <h4>{{ $user->name }}</h4>
                                <p class="text-secondary mb-1">{{ trim(str_replace('"', '', $user->getRoleNames()), '[]') }}</p>
                                <p class="text-muted font-size-sm">Show City, Province, Country</p>
                            </div>
                        </div>
                    </div>
                </div>

                @php($socialLinks = $user->socialLinks)
                @if($socialLinks->count())
                    <div class="card mt-3">
                        <ul class="list-group list-group-flush">
                            @foreach($socialLinks as $socialLink)
                                @php($socialChannel = $socialLink->socialChannel)
                                @php($socialChannelAttachments = $socialChannel->attachments)

                                <li class="list-group-item">
                                    <a href="https://{{ str_replace('https://', '', $socialChannel->url) . '/' . ltrim($socialLink->connection_string, '/') }}" class="d-flex justify-content-between align-items-center flex-wrap text-decoration-none">
                                        <h6 class="mb-0 text-dark">
                                            @if($socialChannelAttachments->count())
                                                <img src="{{ attachmentUrl($socialChannelAttachments[0]) }}" alt="{{ $socialChannel->title }} logo" width="45">
                                            @endif
                                            {{ $socialChannel->title }}
                                        </h6>
                                        <span class="text-secondary">{{ $socialChannel->url }}</span>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>

            <div class="col-md-8">
                <div class="card mb-3">
                    <div class="card-body">
                        @foreach(__("models/user") as $key => $field)
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">{{ $field }}</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    {{ $user[$key] ?? '-' }}
                                </div>
                            </div>
                            <hr>
                        @endforeach

                        <div class="row">
                            <div class="col-12">
                                @compo("$VIEW_ROOT.components.actions.edit", [
                                    "VARIABLE"  => $user,
                                    "TABLE"     => $VIEW_FOLDERS[1],
                                    "CLASS"     => 'btn btn-primary',
                                    "TITLE"     => 'Edit',
                                ])

                                <form action="{{ route("$VIEW_ROOT.users.reset-password", $user) }}" method="post" class="d-inline">
                                    @csrf
                                    @compo("$VIEW_ROOT.components.clickables.link", [
                                        "TITLE"     => "<i class='fa fa-key'></i> Reset Password",
                                        "CLASS"     => 'btn btn-danger',
                                        "ONCLICK"  => "if(confirm('".__('Are you sure?')."'))$(this).closest('form').submit();",
                                        "RAW_HTML"  => true,
                                    ])
                                </form>

                                @if($user->use_two_factor_auth)
                                    <form action="{{ route("$VIEW_ROOT.users.disable_two_step_auth", $user) }}" method="post" class="d-inline">
                                        @csrf
                                        @compo("$VIEW_ROOT.components.clickables.link", [
                                            "TITLE"     => "<i class='fa fa-key'></i> Disable Two Auth",
                                            "CLASS"     => 'btn btn-warning',
                                            "ONCLICK"  => "if(confirm('".__('Are you sure?')."'))$(this).closest('form').submit();",
                                            "RAW_HTML"  => true,
                                        ])
                                    </form>
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
