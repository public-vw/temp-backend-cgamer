@if(!$variable->hasRole('sysadmin@web'))
    @compo("$VIEW_ROOT.components.actions.delete", [
        "VARIABLE"  => $variable,
        "TABLE"     => $VIEW_FOLDERS[1],
    ])

    @compo("$VIEW_ROOT.components.clickables.link", [
        "TITLE"    => "<i class='fa fa-pencil'></i> roles",
        "CLASS"    => 'btn btn-sm btn-primary',
        "RAW_HTML" => true,
        'URL'  => route('sysadmin.users.roles.index',[$variable]),
    ])

    @compo("$VIEW_ROOT.components.clickables.link", [
        "TITLE"    => "<i class='fa fa-pencil'></i> permissions",
        "CLASS"    => 'btn btn-sm btn-primary',
        "RAW_HTML" => true,
        'URL'  => route('sysadmin.users.permissions.index',[$variable]),
    ])

    @compo("$VIEW_ROOT.components.clickables.link", [
        "TITLE"    => "<i class='fa fa-user'></i> loginAs",
        "CLASS"    => 'btn btn-sm btn-info',
        "RAW_HTML" => true,
        'URL'  => route('sysadmin.users.impersonate.set',[$variable]),
    ])

    @compo("$VIEW_ROOT.components.actions.edit", [
        "VARIABLE"  => $variable,
        "CLASS"    => 'btn btn-sm btn-warning',
        "TABLE"     => $VIEW_FOLDERS[1],
    ])
@endif
