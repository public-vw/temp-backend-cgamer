@if($variable->hasParent())
@compo("$VIEW_ROOT.components.clickables.link", [
    "URL"   => route("$VIEW_ROOT.{$VIEW_FOLDERS[1]}.show", $variable->parent),
    "TITLE" => $variable->parent->name,
])
@else
---
@endif
