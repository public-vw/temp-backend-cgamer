@extends("$VIEW_ROOT.layouts.edit", [
    'MODEL'     => 'Permission',
    'TABLE'     => $VIEW_FOLDERS[1],
    'VARIABLE'  => $item,
])

@section('inside')
    <div class="row">
        @compo("$VIEW_ROOT.components.textfields.text",[
            "NAME"      => 'name',
            "LABEL"     => __('Permission Name'),
            "REQUIRED"  => true,
            "VALUE"     => $item->name,
        ])
    </div>
@endsection
