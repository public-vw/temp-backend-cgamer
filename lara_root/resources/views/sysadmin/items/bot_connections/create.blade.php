@extends("$VIEW_ROOT.layouts.create",[
    'MODEL' => 'Bot Connection',
    'TABLE' => $VIEW_FOLDERS[1],
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Title"),
                    "NAME"          => 'title',
                    "REQUIRED"      => true,
                ])
                @compo("$VIEW_ROOT.components.dropdowns.select2.config_key", [
                    "NAME"          => 'type',
                    "LABEL"         => __("Type"),
                    "PLACEHOLDER"   => __("Select Type ..."),
                    "OPTIONS"       => config('enums.bot_connections'),
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Username"),
                    "NAME"          => 'username',
                    "PLACEHOLDER"   => '',
                    "REQUIRED"      => true,
                ])
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Webhook Token"),
                    "NAME"          => 'webhook_token',
                    "PLACEHOLDER"   => '',
                    "REQUIRED"      => true,
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Robot Token"),
                    "NAME"          => 'robot_token',
                    "PLACEHOLDER"   => '',
                    "CONTAINER_CLASS"         => 'col-sm-12',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.textarea", [
                    "LABEL"         => __("Parameters"),
                    "NAME"          => 'parameters',
                    "PLACEHOLDER"   => '',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.boxes.checkbox", [
                    "TITLE"   => __("Active"),
                    "NAME"    => 'active',
                    "CLASS"   => 'custom-control-input',
                    "SWITCH"  => true,
                ])
            </div>
        @endslot
    @endcomponent
@endsection
