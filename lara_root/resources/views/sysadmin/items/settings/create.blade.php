@extends("$VIEW_ROOT.layouts.create_wide",[
    'MODEL' => 'Setting',
    'TABLE' => $VIEW_FOLDERS[1],
])

@section('inside')
    @component("$VIEW_ROOT.components.panel")
        @slot('title', '')

        @slot('body')
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Title"),
                    "NAME"          => 'title',
                    "PLACEHOLDER"   => 'e.g. Users Roles',
                    "REQUIRED"      => true,
                ])

                @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
                    "NAME"          => 'group_id',
                    "LABEL"         => __("Setting Group"),
                    "PLACEHOLDER"   => __("Select Setting Group ..."),
                    "AJAX_URL"      => route("$VIEW_ROOT.setting_groups.list.all"),
                    'CONFIG'        => [
                        'allow_null' => true,
                    ],
                ])
            </div>
            <div class="row form-group">
                {{-- TODO create sulg mutators--}}
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Slug"),
                    "NAME"          => 'slug',
                    "PLACEHOLDER"   => 'e.g. users_roles',
                    "HELPER"        => 'slug should be unique, if you don\'t set slug it will generate automatically.',
                ])
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Order"),
                    "NAME"          => 'order',
                    "TYPE"          => 'number',
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.text", [
                    "LABEL"         => __("Language"),
                    "NAME"          => 'lang',
                    "PLACEHOLDER"   => 'e.g. en',
                ])

                @compo("$VIEW_ROOT.components.dropdowns.select2.config_key", [
                    "NAME"          => 'direction',
                    "LABEL"         => __("Direction"),
                    "PLACEHOLDER"   => __("Select Direction ..."),
                    "OPTIONS"       => config('enums.settings_direction'),
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.dropdowns.select2.config_key", [
                    "NAME"          => 'mode',
                    "LABEL"         => __("Mode"),
                    "PLACEHOLDER"   => __("Select Mode ..."),
                    "OPTIONS"       => config('enums.settings_mode'),
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.boxes.checkbox", [
                    "TITLE"   => __("Hidden"),
                    "NAME"    => 'hidden',
                    "CLASS"   => 'custom-control-input',
                    "SWITCH"  => true,
                ])
            </div>
            <div class="row form-group">
                @compo("$VIEW_ROOT.components.textfields.rich_textarea", [
                    "NAME"              => 'description',
                    "LABEL"             => __('Description'),
                    "CONTAINER_CLASS"   => 'col-12',
                    "ROWS"              => '10',
                    "MORE"              => 'cols="30"',
                ])
            </div>
        @endslot
    @endcomponent
@endsection
