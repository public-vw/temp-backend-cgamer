@php($settingGroup = $variable->group)

@if($settingGroup)
    @php($route = Route::has("$VIEW_ROOT.setting_groups.show") ? route("$VIEW_ROOT.setting_groups.show", $settingGroup) : route("$VIEW_ROOT.setting_groups.edit", $settingGroup))
@endif


@compo("$VIEW_ROOT.components.clickables.link", [
    "URL"   => $route ?? '#',
    "TITLE" => $settingGroup->title ?? '',
])
