@extends("$VIEW_ROOT.layouts.base")

@section('title')
    {{ $user->name }} Profile
@endsection

@section('content')
    <form action="{{ route("$VIEW_ROOT.profile.update") }}" method="post" enctype="multipart/form-data">
        @method('PUT')
        @csrf

        @include("$VIEW_ROOT.includes.alerts")

        <div class="lang-content-box">
            <div class="row">
                <div class="col-12">

                <div class="row">
                    @compo("$VIEW_ROOT.components.textfields.text",[
                        "NAME"      => 'name',
                        "LABEL"     => __('Name'),
                        "REQUIRED"  => true,
                        "VALUE"     => $user->name,
                    ])

                    @compo("$VIEW_ROOT.components.textfields.text",[
                        "NAME"        => 'nickname',
                        "LABEL"       => __('Nickname'),
                        "VALUE"       => $user->nickname,
                        "HELPER"      => 'is visible in public view',
                    ])
                </div>
                <div class="row">
                    @compo("$VIEW_ROOT.components.textfields.text",[
                        "NAME"      => 'telegram_uid',
                        "LABEL"     => __('Telegram UID'),
                        "REQUIRED"  => false,
                        "VALUE"     => $user->telegram_uid,
                    ])

                    @compo("$VIEW_ROOT.components.textfields.text",[
                        "NAME"      => 'username',
                        "LABEL"     => __('Username'),
                        "REQUIRED"  => true,
                        "VALUE"     => $user->username,
                        "HELPER"    => 'is private, and uses for login',
                    ])
                </div>
                <div class="row">
                    @compo("$VIEW_ROOT.components.textfields.phone",[
                        "NAME"      => 'mobile',
                        "LABEL"     => __('Mobile'),
                        "REQUIRED"  => true,
                        "VALUE"     => $user->mobile,
                    ])

                    @compo("$VIEW_ROOT.components.textfields.email",[
                        "NAME"      => 'email',
                        "LABEL"     => __('Email'),
                        "REQUIRED"  => true,
                        "VALUE"     => $user->email,
                    ])
                </div>
                <div class="row">
                    @compo("$VIEW_ROOT.components.textfields.text",[
                        "NAME"      => 'ref_code',
                        "LABEL"     => __('Referring Code'),
                        "REQUIRED"  => false,
                        "VALUE"     => $user->ref_code,
                    ])

                    @compo("$VIEW_ROOT.components.dropdowns.color_palette",[
                        "NAME"      => 'random_color',
                        "LABEL"     => __('Random Color'),
                        "REQUIRED"  => true,
                        "VALUE"     => $user->random_color,
                        "COLORS"    => config('enums.random_colors'),
                    ])
                </div>


                    <div class="text-right div-submit">
                        @compo("$VIEW_ROOT.components.textfields.hidden",[
                            "NAME"  => 'after_action',
                            "VALUE" => 'stay',
                        ])
                        @compo("$VIEW_ROOT.components.clickables.submit",[
                            "TITLE" => '<i class="fa fa-check-circle-o"></i>&nbsp;'. __('Update'),
                            'CLASS' => 'btn-primary',
                        ])
                    </div>
                </div>
            </div>
        </div>

    </form>
@endsection
