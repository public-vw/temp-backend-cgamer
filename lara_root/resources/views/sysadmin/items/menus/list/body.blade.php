@extends("$VIEW_ROOT.layouts.list",[
    'MODEL' => 'Menu',
    'TABLE' => $VIEW_FOLDERS[1]
])

@section('thead')
    <tr>
        <th>Placeholder</th>
        <th>Parent</th>
        <th>Order</th>
        <th>Title</th>
        <th>URL Type</th>
        <th>URL</th>
        <th>{{__('lists.actions.header')}}</th>
    </tr>
@endsection

@section('tbody')
    @foreach($items as $item)
        @include("$VIEW_ROOT.items.menus.list.item", ['item' => $item])
    @endforeach
@endsection

@section('action')
    <div class="col-md-12">
        @compo("$VIEW_ROOT.components.clickables.link",[
        'CLASS'     => 'btn-primary btn-lg',
        'URL'       => route("{$VIEW_ROOT}.{$TABLE}.create"),
        'TITLE'     => "Create New $MODEL Item",
        ])
    </div>
@endsection
