<tr>
    <td>{{ $item->placeholder->title }}</td>
    <td>{{ $item->hasParent() ? $item->parent->title : '--' }}</td>
    <td>{{ $item->order }}</td>
    <td data-toggle="tooltip" data-placement="bottom" title="{{ $item->slug ?? '' }}">{{ $item['title'] ?? '-- Break Line --' }}</td>
    <td title="{{$item->href}}"
        data-toggle="tooltip" data-placement="bottom">{{config('enums.menus_url_type.'.$item->url_type)}}</td>
    <td>{{ $item->url }}</td>

    <td nowrap>
        @compo("$VIEW_ROOT.components.actions.actions_holder", [
            "TABLE"       => $VIEW_FOLDERS[1],
            "VARIABLE"    => $item,
            "ACTIONS"     => ['edit' => true, 'delete' => true],
        ])
    </td>
</tr>

@if($item->hasChild())
    @foreach($item->children as $menuSubItem)
        @include("$VIEW_ROOT.items.menus.list.item", ['item' => $menuSubItem])
    @endforeach
@endif
