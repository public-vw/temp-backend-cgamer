@extends("$VIEW_ROOT.layouts.edit", [
    'MODEL'     => 'Menu',
    'TABLE'     => $VIEW_FOLDERS[1],
    'VARIABLE'  => $item,
])

@php($is_breakline = is_null($item->title))

@push('js')
    <script>
        function doBreakline(obj){
            if($(obj).is(':checked')){// is breakline
                $('.no-breakline').each(function () {
                    $(this).removeAttr('required').closest('.form-group').slideUp(150);
                })
                return;
            }
            // is NOT breakline
            $('.no-breakline').each(function () {
                var requires = ['title', 'url_type'];
                $(this).closest('.form-group').slideDown(100);

                requires.forEach(function (require) {
                    $('#' + require).attr('required', true);
                })
            })
        }

        $(function (){
            doBreakline($('#breakline'));
        })
    </script>
@endpush

@section('inside')
    <div class="row">
        @compo("$VIEW_ROOT.components.boxes.checkbox",[
            "NAME"      => 'breakline',
            "TITLE"     => __('Break Line'),
            "CHECKED"   => $is_breakline,
            "ONCLICK"   => 'doBreakline(this)',
            "CLASS"     => 'custom-control-input',
            "SWITCH"    => true,
        ])
    </div>
    <div class="row">
        @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
            "ID"            => 'placeholder_id',
            "NAME"          => 'placeholder_id',
            'LABEL'         => __("Placeholder"),
            'PLACEHOLDER'   => __("Select Placeholder"),
            "AJAX_URL"      => route("$VIEW_ROOT.menu_placeholders.list.all"),
            'VALUE'         => [
                'id'   => $item->placeholder->id,
                'text' => $item->placeholder->title,
            ],
        ])

        @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
            "NAME"          => 'parent_id',
            'LABEL'         => __("Parent Menu"),
            'PLACEHOLDER'   => __("Root"),
            "AJAX_URL"      => route("$VIEW_ROOT.menus.list.all"),
            'DEPENDS_TO'    => '#placeholder_id',
            'VALUE'         => [
                'id'   => $item->parent_id ?? null,
                'text' => $item->parent->title ?? null,
            ],
            'CONFIG'        => [
                'allow_null' => true,
                'null_option_title' => '- ROOT -',
            ],
        ])
    </div>
    <div class="row">
        @compo("$VIEW_ROOT.components.textfields.text",[
            "NAME"  => 'slug',
            "LABEL" => __('Slug'),
            "CLASS" => 'no-breakline',
            "VALUE" => $is_breakline ? '' : $item->slug,
        ])

        @compo("$VIEW_ROOT.components.textfields.text",[
            "NAME"      => 'title',
            "LABEL"     => __('Title'),
            "CLASS"     => 'no-breakline',
            "REQUIRED"  => true,
            "VALUE"     => $is_breakline ? '' : $item->title,
        ])
    </div>
    <div class="row">
        @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
            "NAME"          => 'url_type',
            'LABEL'         => __("URL Type"),
            'PLACEHOLDER'   => __("Select Url Type ..."),
            "AJAX_URL"      => route("$VIEW_ROOT.menus.list.config", ['config' => 'menus_url_type']),
            'CLASS'         => 'no-breakline',
            'REQUIRED'      => true,
            'VALUE'         => [
                'id'   => $item->url_type ?? null,
                'text' => config('enums.menus_url_type.'.$item->url_type) ?? null,
            ],
        ])

        @compo("$VIEW_ROOT.components.textfields.text",[
            "NAME"      => 'url',
            "LABEL"     => __('URL'),
            "CLASS"     => 'no-breakline',
            "VALUE"     => $is_breakline ? '' : $item->url,
            "HELPER"    => $item->href,
        ])
    </div>
    <div class="row">
        @compo("$VIEW_ROOT.components.textfields.text",[
            "NAME"      => 'order',
            "TYPE"      => 'number',
            "LABEL"     => __('Order'),
            "REQUIRED"  => true,
            "VALUE"     => $item->order,
            "MORE"      => 'min="0"',
        ])

        @compo("$VIEW_ROOT.components.textfields.text",[
            "NAME"  => 'class',
            "LABEL" => __('Class'),
            "VALUE" => $item->class,
        ])
    </div>
    <div class="row">
        @compo("$VIEW_ROOT.components.textfields.text",[
            "NAME"  => 'style',
            "LABEL" => __('Style'),
            "VALUE" => $item->style,
        ])

        @compo("$VIEW_ROOT.components.textfields.text",[
            "NAME"  => 'icon',
            "LABEL" => __('Icon'),
            "CLASS" => 'no-breakline',
            "VALUE" => $is_breakline ? '' : $item->icon,
        ])
    </div>
    <div class="row">
        @compo("$VIEW_ROOT.components.boxes.checkbox",[
            "NAME"      => 'go_new_tab',
            "TITLE"     => __('Open on new tab'),
            "CLASS"     => 'no-breakline custom-control-input',
            "CHECKED"   => $is_breakline ? '' : $item->go_new_tab,
            "SWITCH"    => true,
        ])

        @compo("$VIEW_ROOT.components.boxes.checkbox",[
            "NAME"      => 'nofollow',
            "TITLE"     => __('No Follow'),
            "CLASS"     => 'no-breakline custom-control-input',
            "CHECKED"   => $is_breakline ? '' : $item->nofollow,
            "SWITCH"    => true,
        ])
    </div>
    <div class="row">
        @compo("$VIEW_ROOT.components.boxes.checkbox",[
            "NAME"      => 'active',
            "TITLE"     => __('Active'),
            "CHECKED"   => $item->active,
            "CLASS"     => 'custom-control-input',
            "SWITCH"    => true,
        ])
    </div>
@endsection
