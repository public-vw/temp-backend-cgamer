@extends("$VIEW_ROOT.layouts.create", [
    'MODEL'     => 'Menu',
    'TABLE'     => $VIEW_FOLDERS[1],
])

@push('js')
    <script>
        function doBreakline(obj){
            if($(obj).is(':checked')){// is breakline
                $('.no-breakline').each(function () {
                    $(this).removeAttr('required').closest('.form-group').slideUp(150);
                })
                return;
            }
            // is NOT breakline
            $('.no-breakline').each(function () {
                var requires = ['title', 'url_type'];
                $(this).closest('.form-group').slideDown(100);

                requires.forEach(function (require) {
                    $('#' + require).attr('required', true);
                })
            })
        }
    </script>
@endpush

@section('inside')
    <div class="row">
        @compo("$VIEW_ROOT.components.boxes.checkbox",[
            "NAME"      => 'breakline',
            "TITLE"     => __('Break Line'),
            "ONCLICK"   => 'doBreakline(this)',
            "CLASS"     => 'custom-control-input',
            "SWITCH"    => true,
        ])
    </div>
    <div class="row">
        @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
            "ID"            => 'placeholder_id',
            "NAME"          => 'placeholder_id',
            "LABEL"         => __("Placeholder"),
            'PLACEHOLDER'   => __("Select Placeholder"),
            "AJAX_URL"      => route("$VIEW_ROOT.menu_placeholders.list.all"),
        ])

        @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
            "NAME"          => 'parent_id',
            "LABEL"         => __("Parent Menu"),
            'PLACEHOLDER'   => __("Root"),
            "AJAX_URL"      => route("$VIEW_ROOT.menus.list.all"),
            'DEPENDS_TO'    => '#placeholder_id',
            'CONFIG'        => [
                'allow_null' => true,
                'null_option_title' => '- ROOT -',
            ],
        ])
    </div>
    <div class="row">
        @compo("$VIEW_ROOT.components.textfields.text",[
            "NAME"  => 'slug',
            "LABEL" => __('Slug'),
            "CLASS" => 'no-breakline',
        ])

        @compo("$VIEW_ROOT.components.textfields.text",[
            "NAME"      => 'title',
            "LABEL"     => __('Title'),
            "CLASS"     => 'no-breakline',
            "REQUIRED"  => true,
        ])
    </div>
    <div class="row">
        @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
            "NAME"          => 'url_type',
            "LABEL"         => __("URL Type"),
            'PLACEHOLDER'   => __("Select Url Type ..."),
            "AJAX_URL"      => route("$VIEW_ROOT.menus.list.config", ['config' => 'menus_url_type']),
            'CLASS'         => 'no-breakline',
            'REQUIRED'      => true,
        ])

        @compo("$VIEW_ROOT.components.textfields.text",[
            "NAME"      => 'url',
            "LABEL"     => __('URL'),
            "CLASS"     => 'no-breakline',
        ])
    </div>
    <div class="row">
        @compo("$VIEW_ROOT.components.textfields.text",[
            "NAME"      => 'order',
            "TYPE"      => 'number',
            "LABEL"     => __('Order'),
            "REQUIRED"  => true,
            "DEFAULT"   => 100,
            "MORE"      => ' min="0" ',
        ])

        @compo("$VIEW_ROOT.components.textfields.text",[
            "NAME"  => 'class',
            "LABEL" => __('Class'),
        ])
    </div>
    <div class="row">
        @compo("$VIEW_ROOT.components.textfields.text",[
            "NAME"  => 'style',
            "LABEL" => __('Style'),
        ])

        @compo("$VIEW_ROOT.components.textfields.text",[
            "NAME"  => 'icon',
            "LABEL" => __('Icon'),
            "CLASS" => 'no-breakline',
            "DEFAULT" => 'icon ion-ios-arrow-forward',
        ])
    </div>
    <div class="row">
        @compo("$VIEW_ROOT.components.boxes.checkbox",[
            "NAME"      => 'go_new_tab',
            "TITLE"     => __('Open on new tab'),
            "CLASS"     => 'no-breakline custom-control-input',
            "SWITCH"    => true,
        ])

        @compo("$VIEW_ROOT.components.boxes.checkbox",[
            "NAME"      => 'nofollow',
            "TITLE"     => __('No Follow'),
            "CLASS"     => 'no-breakline custom-control-input',
            "SWITCH"    => true,
        ])
    </div>
    <div class="row">
        @compo("$VIEW_ROOT.components.boxes.checkbox",[
            "NAME"      => 'active',
            "TITLE"     => __('Active'),
            "CHECKED"   => true,
            "CLASS"     => 'custom-control-input',
            "SWITCH"    => true,
        ])
    </div>
@endsection
