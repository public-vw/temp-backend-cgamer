@extends("$VIEW_ROOT.layouts.base")

@section('title')
    Log | Show | ID: {{ $log->id }}
@endsection
@section('right-title')
    <b>Type: </b>{{ $log->type ?? '---' }}
@endsection

@push('css')
    <style>
        tr.useless{
            background-color:#f5f5f5;
            color:#c1c1c1;
        }
    </style>
@endpush

@section('content')
    <h4>{{ $log->title }}</h4>
    <div class="row mx-5 my-5">
        <table class="table">
            <tbody>
                <tr>
                    <th>Date (m-d-y)</th><td>{{ $log->created_at->format('d M Y') }}</td>
                </tr>
                <tr>
                    <th>Time</th><td>{{ $log->created_at->format('A h:i:s').'.'.$log->mili }}</td>
                </tr>
                <tr class="@if(!isset($log->user->username)) useless @endempty">
                    <th>User</th><td>{{ $log->user->username ?? '---' }}</td>
                </tr>
                <tr class="@if(in_array($log->ip,['LOCAL','127.0.0.1'])) useless @endempty">
                    <th>IP</th><td>{{ $log->ip }}</td>
                </tr>
                <tr class="@empty($log->prev_url) useless @endempty">
                    <th>Previous URL</th><td>{{ $log->prev_url }}</td>
                </tr>
                <tr class="@empty($log->curr_url) useless @endempty">
                    <th>Current URL</th><td>{{ $log->curr_url }}</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="row mx-5">
        <h4>Message</h4>
        <div class="col border border-dark m-0 p-2 text-light bg-dark rounded">
            {!! nl2br($log->message) !!}
        </div>
    </div>
@endsection

