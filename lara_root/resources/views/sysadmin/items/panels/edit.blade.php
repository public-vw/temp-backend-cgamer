@extends("$VIEW_ROOT.layouts.edit", [
    'MODEL'     => 'Panel',
    'TABLE'     => $VIEW_FOLDERS[1],
    'VARIABLE'  => $item,
])

@section('inside')
    <div class="row">
        @compo("$VIEW_ROOT.components.textfields.text",[
            "NAME"      => 'title',
            "LABEL"     => __('Panel Title'),
            "REQUIRED"  => true,
            "VALUE"     => $item->title,
        ])
    </div>
    <div class="row">
        @compo("$VIEW_ROOT.components.boxes.checkbox",[
            "NAME"      => 'active',
            "TITLE"     => __('Active'),
            "CHECKED"   => $item->active,
            "CLASS"     => 'custom-control-input',
            "SWITCH"    => true,
        ])
    </div>
@endsection
