@extends("$VIEW_ROOT.layouts.create", [
    'MODEL'     => 'Panel',
    'TABLE'     => $VIEW_FOLDERS[1],
])

@section('inside')
    <div class="row">
        @compo("$VIEW_ROOT.components.textfields.text",[
            "NAME"      => 'title',
            "LABEL"     => __('Panel Title'),
            "REQUIRED"  => true,
        ])
    </div>
    <div class="row">
        @compo("$VIEW_ROOT.components.boxes.checkbox",[
            "NAME"      => 'active',
            "TITLE"     => __('Active'),
            "CHECKED"   => false,
            "CLASS"     => 'custom-control-input',
            "SWITCH"    => true,
        ])
    </div>
@endsection
