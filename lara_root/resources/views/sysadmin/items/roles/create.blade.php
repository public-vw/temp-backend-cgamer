@extends("$VIEW_ROOT.layouts.create", [
    'MODEL'     => 'Role',
    'TABLE'     => $VIEW_FOLDERS[1],
])

@section('inside')
    <div class="row">
        @compo("$VIEW_ROOT.components.textfields.text",[
            "NAME"      => 'name',
            "LABEL"     => __('Role Name'),
            "REQUIRED"  => true,
        ])
    </div>
    <div class="row">
        @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
            "NAME"          => 'panel_id',
            "LABEL"         => __("Panel"),
            'PLACEHOLDER'   => __("Select Panel"),
            "AJAX_URL"      => route("$VIEW_ROOT.panels.list.all"),
        ])
    </div>
@endsection
