@extends("$VIEW_ROOT.layouts.base")

@section('title')
    Permissions | Management
@endsection

@section('right-title')
    @compo("$VIEW_ROOT.components.clickables.link", [
        "TITLE"    => "<i class='fa fa-save'></i> save changes",
        "CLASS"    => 'btn btn-sm btn-success',
        "RAW_HTML" => true,
        'ONCLICK'  => 'window.sendUpdate()',
    ])
@endsection

@push('css')
    <style>
        .header{
            line-height: 40px;
            font-weight: bold;
            border-bottom: 1px solid #666;
            text-align: center;
        }
        .col{
            line-height: 40px;
        }
        .model-name{
            font-weight: bold;
            border-right: 1px solid #666;
        }
        .model-name .header{
            background-color: #ff880033;
            text-align: center;
            font-size: 18px;
            border: 0!important;
        }

        .permission img{
            cursor: pointer;
            filter: invert(100%) sepia(3%) saturate(50%) hue-rotate(64deg) brightness(114%) contrast(83%);
            transition: 0.2s all ease-in-out;
        }
        .permission.selected img{
            filter: invert(43%) sepia(93%) saturate(1007%) hue-rotate(85deg) brightness(96%) contrast(91%);
            text-decoration: underline;
            transform: scale(1.3);
        }
    </style>
@endpush

@push('js')
    <script>
        let outputPermissionsChange = {}
        $(function (){
            $('.permission').on('click',function (){
                $(this).addClass('working')
                $(this).closest('.row').find('.permission:not(.working)').removeClass('selected')
                $(this).toggleClass('selected').removeClass('working')

                outputPermissionsChange[$(this).closest('.row').attr('data-term')] = ($(this).hasClass('selected')?$(this).attr('data-perm'):null)
            })

        })
        window.sendUpdate = function(){
            console.log(outputPermissionsChange)
            $.ajax({
                type: 'POST',
                url: '{{ route('sysadmin.roles.permissions.update',[$role]) }}',
                data: {
                    'permissions': outputPermissionsChange,
                },
                dataType: 'json',
                success: function (response) {
                    console.log(response)
                },
                error: function (err) {
                    console.log(err)
                }
            })
        }
    </script>
@endpush

@section('content')
    <div class="row permission-table">
        <div class="col model-name">
                <div class="header">Role: {{ $role->name }}</div>
            @foreach($tables as $table)
                <div>{{ $table }}</div>
            @endforeach
        </div>



        @foreach(['view','edit','create','delete'] as $crud)
            <div class="col">
                <div class="header">{{ $crud }}</div>
                @foreach($tables as $table)
                    @php
                        $term = sprintf('%s.%s',$table,$crud);
                    @endphp
                    @continue(\Spatie\Permission\Models\Permission::where('name',$term.".all")->get()->isEmpty() )
                <div class="row" data-term="{{ $term }}">
                    @foreach(['all'=>'20px','branch'=>'20px','own'=>'16px'] as $territory=>$width)
                        @php
                            $perm = sprintf('%s.%s',$term,$territory);
                        @endphp
                        <div class="col-sm-4 permission @if($role->hasPermissionTo($perm)) selected @endif" data-perm="{{$perm}}">
                            <img src="/assets/sysadmin/img/permissions/{{$territory}}.svg" width="{{ $width }}" />
                        </div>
                    @endforeach
                </div>
                @endforeach
            </div>
        @endforeach
    </div>
@endsection
