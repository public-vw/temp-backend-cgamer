@compo("$VIEW_ROOT.components.actions.delete", [
    "VARIABLE"  => $variable,
    "TABLE"     => $VIEW_FOLDERS[1],
])
@compo("$VIEW_ROOT.components.clickables.link", [
    "TITLE"    => "<i class='fa fa-pencil'></i> permissions",
    "CLASS"    => 'btn btn-sm btn-primary',
    "RAW_HTML" => true,
    'URL'  => route('sysadmin.roles.permissions.index',[$variable]),
])
