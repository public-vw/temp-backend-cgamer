@if($variable->panel)
@compo("$VIEW_ROOT.components.clickables.link", [
    "URL"   => route("{$VIEW_ROOT}.panels.edit", $variable->panel),
    "TITLE" => $variable->panel->title,
])
@else
{{ '---' }}
@endif
