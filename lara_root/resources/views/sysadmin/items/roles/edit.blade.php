@extends("$VIEW_ROOT.layouts.edit", [
    'MODEL'     => 'Role',
    'TABLE'     => $VIEW_FOLDERS[1],
    'VARIABLE'  => $item,
])

@section('inside')
    <div class="row">
        @compo("$VIEW_ROOT.components.textfields.text",[
            "NAME"      => 'name',
            "LABEL"     => __('Role Name'),
            "REQUIRED"  => true,
            "VALUE"     => $item->name,
        ])
    </div>
    <div class="row">
        @compo("$VIEW_ROOT.components.dropdowns.select2.single_ajax", [
            "NAME"          => 'panel_id',
            "LABEL"         => __("Panel"),
            'PLACEHOLDER'   => __("Select Panel"),
            "AJAX_URL"      => route("$VIEW_ROOT.panels.list.all"),
            "CONFIG"      => [
                'allow_null' => true,
                'hide_searchbox' => true,
            ],
            'VALUE'         => [
                'id'   => $item->panel->id ?? '',
                'text' => $item->panel->title ?? '---',
            ],
        ])
    </div>
@endsection
