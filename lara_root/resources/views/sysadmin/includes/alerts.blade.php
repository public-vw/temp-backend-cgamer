@if($result = session('result'))
    <div class="alert alert-{{$result['alert']}} alert-dismissible">
        <strong>
            {!! $result['message'] !!}
        </strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
@endif
