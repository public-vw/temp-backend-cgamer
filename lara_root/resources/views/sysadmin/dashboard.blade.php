@extends("$VIEW_ROOT.layouts.base",
[
    'page_title' => 'Dashboard',
    'no_bread' => true
])

@section('content')
    Welcome to {{ config('app.title', 'Laravel') }}
    <hr>
    <b>Branch:</b> {{ $branch }} <br>
    <b>Version:</b> {{ Str::substr($version,0,6) }} [ <a href="{{route('sysadmin.administration.upgrade')}}">Upgrade Now!</a> ]
    <br>
    <hr>
    <b>Details:</b> <br>
    {!! nl2br($details) !!}

@endsection
