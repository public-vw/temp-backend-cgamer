@extends('admin.layouts.component')

@pushonce('css:calendar')
<link rel="stylesheet" type="text/css" href="{{asset('vendor/enBank.calendar/enBank.calendar.css')}}"/>
@endpushonce

@pushonce('js:calendar')
<script src="{{asset('vendor/enBank.calendar/calendar.js')}}"></script>
<script src="{{asset('vendor/enBank.calendar/calendar-fa.js')}}"></script>
@endpushonce

@push('js')
    <script type="text/javascript">
        $(function(){
            $('.datepicker').each(function(k,ele){
                Calendar.setup({
                    inputField: $(ele).attr('id'),
                    @if(isset($FORMAT))
                        ifFormat: "{{$FORMAT}}",
                    @else
                        ifFormat: "%Y/%m/%d"+(($(ele).attr('data-hasTime')=='1')?' %H:%M':''),
                    @endif
                    dateType: "jalali",
                    weekNumbers: false,
                    showOthers: true,
                    showsTime: ($(ele).attr('data-hasTime')=='1'),
                    singleClick: false,
                    langNumbers: false,
                    autoShowOnFocus: false
                });

            });
        });
    </script>
@endpush

@section('input-'.$rnd)
    <input id="{{$id ?? $name}}"
           type='text'
           class="form-control datepicker enRight @error($name) is-invalid @enderror"
           name="{{$name}}"
           value="{{ $value ?? old($name, $default ?? '') }}"
           data-hasTime='{{$has_time ?? '0'}}'

           @if(!isset($disable) || !$disable)
           @if(isset($required) && $required) required @endif
           placeholder="{{$placeholder ?? ''}}"
           @else
           readonly='readonly'
           disabled='disabled'
            @endif
            {!! $more ?? '' !!}
    />
@endsection
