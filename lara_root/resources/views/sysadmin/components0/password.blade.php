@extends('admin.layouts.component')

@section('input-'.$rnd)
    <input
        id="{{ $id ?? $name }}"
        name="{{ $name }}"
        type="password"
        class="form-control @error($name) is-invalid @enderror"
        value="{{ $value ?? old($name, $default ?? '') }}"
        autocomplete="{{ config('app.name', 'Laravel') }}_{{ $name }}"

        @if(!isset($disable) || !$disable)
            @if(isset($required) && $required) required @endif
            placeholder="{{$placeholder ?? ''}}"
        @else
            readonly='readonly'
            disabled='disabled'
        @endif
        {!! $more ?? '' !!}
    />
@endsection
