@extends('admin.layouts.component')

@section('input-'.$rnd)
    <div class="form-check">
        <input
            id="{{ $id ?? $name }}"
            name="{{ $name }}"
            type="checkbox"
            class="form-check-input"
            @if(isset($value) && $value) value="{{ $value }}" @endif
            {{ old($name, $default ?? '') == $name  ? 'checked' : '' }}

            @if(!isset($disable) || !$disable)
                @if(isset($required) && $required) required @endif
                placeholder="{{$placeholder ?? ''}}"
            @else
                readonly='readonly'
                disabled='disabled'
            @endif
            {!! $more ?? '' !!}
        />
        <label class="form-check-label" for="{{ $name }}">
            {{ $title }}
        </label>
    </div>
@endsection
