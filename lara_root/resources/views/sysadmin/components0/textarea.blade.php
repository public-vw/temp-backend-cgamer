@extends('admin.layouts.component')

@section('input-'.$rnd)
    <textarea rows="{{ $rows ?? 5 }}" class="form-control" style="width: 100%"
              id="{{ $name }}" name="{{ $name }}"
              placeholder="{{ $placeholder ?? '' }}"
              @if(isset($required) && $required) required @endif
              @if(isset($readonly) && $readonly) readonly="readonly" @endif
        {!! $more ?? '' !!}
        >{{$value ?? old($name, $default ?? '')}}</textarea>
@endsection
