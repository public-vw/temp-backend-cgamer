@extends('admin.layouts.component')

@section('input-'.$rnd)
    <div class="mt-radio-inline">
        @foreach($items as $key => $data)
            <label for="{{$name}}-{{$key}}" class="mt-radio">
                <input type="radio" class="@if(isset($disable) && $disable) disabled @endif"
                       id="{{$name}}-{{$key}}" @if(!isset($disable) || !$disable) name="{{ $name }}"
                       @endif value="{{ $key }}"
                       {{ old($name, $default ?? '') == $key  ? 'checked="checked"' : '' }}
                       @if(!isset($disable) || !$disable)
                       @if(isset($required) && $required) required @endif
                       @else
                       disabled='disabled'
                    @endif
                    {!! $more ?? '' !!}
                />
                {{ $data['title'] ?? "[$key] has no title!" }}
                <span></span>
            </label>
        @endforeach
    </div>
@endsection
