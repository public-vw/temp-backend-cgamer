<button type="{{ $type }}" class="btn {{ $class ?? 'btn-default' }}">
    {{ $title }}
</button>
