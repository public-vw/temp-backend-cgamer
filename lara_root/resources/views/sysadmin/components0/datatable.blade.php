@push('css')
    <style>
        table td form{
            cursor: pointer;
            display:inline-block;
        }
    </style>
@endpush

@php
if(!isset($actions)){
    $actions = ['show', 'edit', 'delete'];
}
@endphp

@if($items->count())
    <table class="table">
        <thead>
            <tr>
                @foreach($columns as $col_name=>$col_title)
                    <th>{{ $col_title }}</th>
                @endforeach

                @if(!empty($actions))
                    <th>{{__('lists.actions.header')}}</th>
                @endif
            </tr>
        </thead>
        <tbody>
        @foreach($items as $item)
            @php($action_forms = [])
            @if(in_array('show', array_keys($actions)))
                @php($action_forms[] = '<form action="'.route($view.'.show',[$item->get($action_field)]).'" method="get" class="inline_form"><a class="btn" onclick="$(this).closest(\'.inline_form\').submit()">'. __('lists.actions.show').'</a></form>')
            @endif
            @if(in_array('edit', $actions))
                @php($action_forms[] = '<form action="'.route($view.'.edit',[$item->get($action_field)]).'" method="get" class="inline_form"><a class="btn" onclick="$(this).closest(\'.inline_form\').submit()">'. __('lists.actions.edit').'</a></form>')
            @endif
            @if(in_array('delete', $actions))
                @php($action_forms[] = '<form action="'.route($view.'.destroy',[$item->get($action_field)]).'" method="post" class="inline_form" onsubmit="return confirm(\''. __('lists.actions.delete_confirm_message') .'\');">'.csrf_field() . method_field('DELETE').'<a class="btn" onclick="$(this).closest(\'.inline_form\').submit()">'. __('lists.actions.delete') .'</a></form>')
            @endif
            <tr>
                @foreach($columns as $col_name=>$col_title)
                    <td>@if(isset($render_html) && $render_html){!! $item->get($col_name) !!}@else{{ $item[$col_name] }}@endif</td>
                @endforeach

                @if(!empty($actions))
                <td nowrap>
                    {!! implode('&nbsp;|&nbsp;', $action_forms) !!}
                </td>
                @endif
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ method_exists(get_class($items),'links')? $items->links() : '' }}
@else
    <i>Nothing to show</i>
@endif
