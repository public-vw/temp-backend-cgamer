@push('css')
    <style>
        .warning{
            border:1px solid red;
        }
    </style>
@endpush

@if($errors->any())
    @foreach($errors->all() as $error)
        <div>{{$error}}</div>
    @endforeach
    <br>
@endif
