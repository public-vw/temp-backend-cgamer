## Text ##
    <div class="col-md-6">
        @include("$VIEW_ROOT.components.text",[
            'rnd' => rand(101,900).'-'.rand(101,900),
            'name' => 'first_name',
            'default' => '',
            'required' => true,
            'title' => __('messages.doctor_label'),

            'icons' => [
                'left'  => [
                    'class' => "la la-barcode",
                ],
            ],

            'buttons' => [
                'right'  => [
                    'id' => "btn-123",
                    'title' => "<i class='la la-barcode'></i>",
                ],
            ],

            'more' => "another attributes here",
        ])
    </div>

## PASSWORD ##
    <div class="col-md-6">
        @include("$VIEW_ROOT.components.password",[
            'rnd' => rand(101,900).'-'.rand(101,900),
            'name' => 'password',
            'default' => '',
            'required' => true,
            'alias' => 'password',
            'title' => __('messages.doctor_label')
        ])
    </div>

## EMAIL ##
    <div class="col-md-6">
        @include("$VIEW_ROOT.components.email",[
            'rnd' => rand(101,900).'-'.rand(101,900),
            'name' => 'email',
            'default' => '',
            'required' => true,
            'title' => __('messages.doctor_label')
        ])
    </div>

## IMAGE ##
    <div class="col-md-6">
        @include("$VIEW_ROOT.components.image",[
            'rnd' => rand(101,900).'-'.rand(101,900),
            'name' => 'image',
            'required' => true,
            'title' => __('messages.image_label')
        ])
    </div>

## RADIO GROUP ##
    <div class="col-md-6">
        @include("$VIEW_ROOT.components.radio",[
            'rnd' => rand(101,900).'-'.rand(101,900),
            'name' => 'nationality',
            'required' => true,
            'items' => [
                'iran'   => [ //KEY is VALUE of Radio
                    'title' => 'ایرانی',
                ],
                'other' => [
                    'title' => 'غیرایرانی',
                ],
            ],
            'default' => 'iran',
            'title' => __('messages.nationality_label')
        ])
    </div>

## CHECKBOX SINGLE ##

## CHECKBOX GROUP ##

## STATIC SELECT ##
    <div class="col-md-6">
        @include("$VIEW_ROOT.components.select",[
            'rnd' => rand(101,900).'-'.rand(101,900),
            'name' => 'doctor',
            'required' => true,
            'key_field' => 'id',
            'value_field' => 'id',
            'title_fields' => 'title',
            'options' => [
                ['id'=>1,'title'=>'m'],
                ['id'=>2,'title'=>'f']
                ],
                'title' => __('messages.doctor_label')
            ])
    </div>

## DYNAMIC SELECT ##
    <div class="col-md-6">
        @include("$VIEW_ROOT.components.select",[
            'rnd' => rand(101,900).'-'.rand(101,900),
            'name' => 'doctor',
            'required' => true,
            'key_field' => 'id',
            'value_field' => 'id',
            'title_fields' => 'first_name|last_name',
            'options' => $doctors,
            'title' => __('messages.doctor_label')
        ])
    </div>
