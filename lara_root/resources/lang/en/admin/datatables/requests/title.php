<?php
return [
    'subject'           => 'Subject',
    'username'          => 'Username',
    'field'             => 'Field',
    'requestable_type'  => 'Parent Type',
    'requestable_title' => 'Parent Title',
    'priority'          => 'Priority',
    'status'            => 'Status',
    'actions'           => 'Actions',
];
