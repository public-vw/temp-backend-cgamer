<?php
return [
    'name'          => 'Name',
    'username'      => 'Username',
    'role'          => 'Role',
    'ref_code'      => 'Ref Code',
    'referer_id'    => 'Referer Id',
    'actions'       => 'Actions',
];
