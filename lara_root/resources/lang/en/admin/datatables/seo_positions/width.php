<?php
return [
    'group'              => '200',
    'uri'                => '200',
    'positionable_type'  => '200',
    'positionable_title' => '200',
    'visit_bot'          => '200',
    'visit_user'         => '200',
    'seo_value'          => '200',
    'status'             => '100',
    'actions'            => '100',
];
