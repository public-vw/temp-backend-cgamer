<?php
return [
    'title'         => 'Title',
    'phone_precode' => 'Phone PreCode',
    'timezone'      => 'TimeZone',
    'actions'       => 'Actions',
];
