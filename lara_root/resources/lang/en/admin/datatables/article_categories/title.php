<?php
return [
    'title'          => 'Title',
    'parent'         => 'Parent',
    'articles_count' => 'Article Cnt.',
    'children'       => 'Children',
    'status'         => 'Status',
    'actions'        => 'Actions',
];
