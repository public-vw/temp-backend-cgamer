<?php
return [
    'title'              => 'Title',
    'addressable_type'   => 'Parent Type',
    'addressable_title'  => 'Parent Title',
    'city'               => 'City',
    'region'             => 'Region',
    'actions'            => 'Actions',
];
