<?php
return [
    'title'              => '200',
    'addressable_type'   => '200',
    'addressable_title'  => '200',
    'city'               => '200',
    'region'             => '200',
    'actions'            => '100',
];
