<?php
return [
    'title'             => 'Title',
    'emailable_type'    => 'Parent Type',
    'emailable_title'   => 'Parent Title',
    'email'             => 'Email',
    'actions'           => 'Actions',
];
