<?php
return [
    'author'        => 'Author',
    'content'       => 'Content',
    'response_to'   => 'Response To',
    'service'       => 'Service',
    'status'        => 'Status',
    'updated_at'    => 'Updated At',
    'actions'       => 'Actions',
];
