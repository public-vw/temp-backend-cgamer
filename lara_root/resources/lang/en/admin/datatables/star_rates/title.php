<?php
return [
    'rankable_title'  => 'Parent Title',
    'rankable_type'   => 'Parent Type',
    'user'            => 'User',
    'rate'            => 'Rate',
    'weight'          => 'Weight',
    'actions'         => 'Actions',
];
