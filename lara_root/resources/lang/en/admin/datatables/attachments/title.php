<?php
return [
    'title'         => 'Title',
    'file_ext'      => 'File Extension',
    'file_size'     => 'File Size',
    'type'          => 'Attachment Type',
    'quality_rank'  => 'Quality Rank',
    'actions'       => 'Actions',
];
