<?php
return [
    'heading'        => 'Heading',
    'category'       => 'Category',
    'author'         => 'Author',
    'reading_time'   => 'RTime',
    'content_length' => 'Length',
    'status'         => 'Status',
    'actions'        => 'Actions',
];
