<?php
return [
    'title'          => '200',
    'type'           => '200',
    'keywords'       => '200',
    'seoable_type'   => '200',
    'seoable_title'  => '200',
    'actions'        => '100',
];
