<?php
return [
    'title'          => 'Title',
    'type'           => 'Type',
    'keywords'       => 'Keywords',
    'seoable_type'   => 'Item Type',
    'seoable_title'  => 'Item Title',
    'actions'        => 'Actions',
];
