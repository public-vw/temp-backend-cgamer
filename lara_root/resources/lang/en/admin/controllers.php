<?php
return [
    'store' => [
        'success' => ':model [:title] created successfully',
    ],
    'update' => [
        'success' => ':model [:title] updated successfully',
    ],
    'destroy' => [
        'success' => ':model [:title] removed successfully',
    ],
    'revision' => [
        'success' => ':model [:title] version updated successfully',
    ],
    'pinged' => [
        'success' => 'Google pinged on :model [:title] successfully',
    ],
    'disableTwoStepAuth' => [
        'success' => 'User [:title] Two Step Auth disabled successfully',
    ],
    'resetPassword' => [
        'success' => 'User [:title] password reset successfully.'."<br>".'New password:'.':new_password',
    ],
];
