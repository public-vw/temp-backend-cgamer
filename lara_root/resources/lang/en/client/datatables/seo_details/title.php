<?php
return [
    'title'          => 'Title',
    'type'           => 'Type',
    'keywords'       => 'Keywords',
    'seoable_type'   => 'Parent Type',
    'seoable_title'  => 'Parent Title',
    'actions'        => 'Actions',
];
