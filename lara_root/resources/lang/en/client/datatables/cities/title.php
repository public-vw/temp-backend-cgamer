<?php
return [
    'title'         => 'Title',
    'country'       => 'Country',
    'province'      => 'Province',
    'phone_precode' => 'Phone PreCode',
    'timezone'      => 'TimeZone',
    'actions'       => 'Actions',
];
