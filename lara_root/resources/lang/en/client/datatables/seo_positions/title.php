<?php
return [
    'group'              => 'Group',
    'uri'                => 'Title',
    'positionable_type'  => 'Parent Type',
    'positionable_title' => 'Parent Title',
    'visit_bot'          => 'Visit With Bots ',
    'visit_user'         => 'Visit With Users',
    'seo_value'          => 'Seo Value',
    'status'             => 'Status',
    'actions'            => 'Actions',
];
