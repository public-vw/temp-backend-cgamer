<?php
return [
    'title'         => 'Title',
    'country'       => 'Country',
    'province'      => 'Province',
    'city'          => 'City',
    'actions'       => 'Actions',
];
