<?php
return [
    'title'         => 'Title',
    'user'          => 'User',
    'type'          => 'Type',
    'message'       => 'Message',
    'created_at'    => 'Time',
    'actions'       => 'Actions',
];
