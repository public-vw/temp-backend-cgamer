<?php
return [
    'title'            => 'Title',
    'phonable_type'    => 'Parent Type',
    'phonable_title'   => 'Parent Title',
    'number'           => 'Number',
    'city'             => 'City',
    'region'           => 'Region',
    'actions'          => 'Actions',
];
