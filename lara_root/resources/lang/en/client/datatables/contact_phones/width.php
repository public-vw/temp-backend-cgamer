<?php
return [
    'title'           => '200',
    'phonable_type'   => '200',
    'phonable_title'  => '200',
    'number'          => '200',
    'city'            => '200',
    'region'          => '200',
    'actions'         => '100',
];
