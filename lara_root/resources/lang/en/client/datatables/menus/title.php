<?php
return [
    'title'         => 'Title',
    'placeholder'   => 'Placeholder',
    'parent'        => 'Parent',
    'order'         => 'Order',
    'actions'       => 'Actions',
];
