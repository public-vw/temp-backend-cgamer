<?php
return [
    'title'         => '200',
    'file_ext'      => '200',
    'file_size'     => '200',
    'type'          => '200',
    'quality_rank'  => '200',
    'actions'       => '100',
];
