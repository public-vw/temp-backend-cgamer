<?php
return [
    'title'             => 'Title',
    'socialable_type'   => 'Parent Type',
    'socialable_title'  => 'Parent Title',
    'channel'           => 'Channel',
    'actions'           => 'Actions',
];
