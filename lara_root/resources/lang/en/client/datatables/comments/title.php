<?php
return [
    'content'           => 'Content',
    'commentable_type'  => 'Parent Type',
    'commentable_title' => 'Parent',
    'status'            => 'Status',
    'actions'           => 'Actions',
];
