<?php
return [
    'title'   => 'Title',
    'domain'  => 'Domain',
    'lang'    => 'Language',
    'order'   => 'Order',
    'actions' => 'Actions',
];
