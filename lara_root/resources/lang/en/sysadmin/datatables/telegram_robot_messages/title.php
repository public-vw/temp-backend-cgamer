<?php
return [
    'title'      => 'Title',
    'user'       => 'Username',
    'message'    => 'Message',
    'created_at' => 'Time (MDY)',
    'actions'    => 'Actions',
];
