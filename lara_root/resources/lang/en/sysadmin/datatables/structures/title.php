<?php
return [
    'parent'  => 'Parent',
    'title'   => 'Title',
    'type'    => 'Type',
    'count'   => 'Records',
    'actions' => 'Actions',
];
