<?php
return [
    'title'   => '500',
    'parent'  => '500',
    'type'    => '500',
    'count'   => '300',
    'actions' => '100',
];
