<?php
return [
    'key'        => 'Key',
    'value'      => 'Value',
    'status'     => 'Status',
    'updated_at' => 'Last Update',
    'actions'    => 'Actions',
];
