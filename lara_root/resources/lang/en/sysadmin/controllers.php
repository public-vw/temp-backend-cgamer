<?php
return [
    'store' => [
        'success' => ':model [:title] created successfully',
    ],
    'update' => [
        'success' => ':model [:title] updated successfully',
    ],
    'destroy' => [
        'success' => ':model [:title] removed successfully',
    ],
    'force_logout' => [
        'success' => ':device [:user | :token_id] logged out successfully',
    ],
];
