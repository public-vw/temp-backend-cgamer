<?php

return [

    'actions' => [
        'header' => 'Actions',
        'show' => 'Show',
        'edit' => 'Edit',
        'delete' => 'Delete',
        'delete_all' => 'Delete All',
        'delete_confirm_message' => 'Are you sure?',
    ],
    'bulk_actions' => [
        'title' => 'Bulk Actions'  ,
        'moveto' => 'Move to :Status'  ,
    ],

];
