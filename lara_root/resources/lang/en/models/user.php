<?php
return [
    'id'           => 'User ID',
    'name'         => 'Full Name',
    'parent'       => 'Parent',
    'username'     => 'User Name',
    'nickname'     => 'Nick Name',
    'ref_code'     => 'Ref Code',
    'mobile'       => 'Mobile',
    'email'        => 'Email',
    'telegram_uid' => 'Telegram UID',
    'referer_id'   => 'Referrer ID',
    'status'       => 'Status',

];
