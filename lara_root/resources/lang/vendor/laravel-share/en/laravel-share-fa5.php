<?php

return [
//    'sample' => '<li><a target="_blank" href=":url" class="social-button :class" id=":id" title=":title" rel=":rel"><span class="fab fa-facebook-square"></span></a></li>',
    'facebook' => '
        <a class="social-link facebook text-tooltip-tft" data-title="Facebook" target="_blank" href=":url" rel="nofollow">
            <svg class="icon-facebook">
                <use xlink:href="#svg-facebook"></use>
            </svg>
        </a>
    ',
    'twitter' => '
        <a class="social-link twitter text-tooltip-tft" data-title="Twitter" target="_blank" href=":url" rel="nofollow">
            <svg class="icon-twitter">
                <use xlink:href="#svg-twitter"></use>
            </svg>
        </a>
    ',
    'telegram' => '
        <a class="social-link telegram text-tooltip-tft" data-title="Telegram" target="_blank" href=":url" rel="nofollow">
            <svg class="icon-telegram">
                <use xlink:href="#svg-send-message"></use>
            </svg>
        </a>
    ',


    'linkedin' => '
                <a class="social-link linkedin text-tooltip-tft" data-title="Linkedin" href=":url">
                    <img src="/assets/interface_dark/img/socials/googlew.png" alt="linkedin_oauth">
                </a>
    ',
    'pinterest' => '
                <a class="social-link pinterest text-tooltip-tft" data-title="Pinterest" href=":url">
                    <img src="/assets/interface_dark/img/socials/googlew.png" alt="pinterest_oauth">
                </a>
    ',
    'whatsapp' => '
                <a class="social-link whatsapp text-tooltip-tft" data-title="WhatsApp" href=":url">
                    <img src="/assets/interface_dark/img/socials/googlew.png" alt="whatsapp_oauth">
                </a>
    ',
    'reddit' => '
                <a class="social-link reddit text-tooltip-tft" data-title="Reddit" href=":url">
                    <img src="/assets/interface_dark/img/socials/googlew.png" alt="reddit_oauth">
                </a>
    ',
];
