<?php
return [
    'homepage' => [
        'title' => 'صفحه اصلی',
        'keywords' => 'بازی،بلاکچین',
        'description' => 'اولین انجمن فارسی بازی‌های مبتنی بر #بلاکچین',
    ],
    'article' => 'نوشته',
    'article-category' => 'دسته‌بندی',
];
