<?php
return [
    'articles_status' => [
        'draft'     => 'پیش‌نویس',
        'requested' => 'درخواست بررسی',
        'rejected'  => 'رد شده',
        'ready'     => 'آماده SEO',
        'seo-ready'     => 'آماده انتشار',
        'active'    => 'منتشر شده',
        'paused'    => 'متوقف شده',
        'updated'   => 'دارای نسخه جدیدتر',
        'checking'  => 'در حال بررسی',
    ],

    'users_status' => [
        'tmp'     => 'موقت',
        'pending' => 'درخواست فعال‌سازی',
        'active'  => 'فعال',
        'demo'    => 'دمو',
        'suspend' => 'معلق شده',
        'storage' => 'ذخیره‌سازی',
    ],

    'genders' => [
        'male'    => 'مرد',
        'female'  => 'زن',
        'unknown' => 'نامشخص',
    ],

    'comments_status' => [
        'pending'   => 'درخواست نشر',
        'suspended' => 'معلق شده',
        'accepted'  => 'تایید شده',
    ],

    'hyperlinks_status' => [
        'requested' => 'درخواست شده',
        'active'    => 'فعال',
        'rejected'  => 'رد شده',
        'suspend'   => 'معلق',
    ],

    'article_categories_status' => [
        'draft'     => 'پیش‌نویس',
        'requested' => 'درخواست شده',
        'ready'     => 'آماده فعال‌سازی',
        'active'    => 'فعال',
    ],


    'authors_status' => [
        'draft'     => 'پیش‌نویس',
        'requested' => 'درخواست شده',
        'active'    => 'فعال',
        'suspend'   => 'معلق',
    ],

    'server_configs_status' => [
        'unknown'  => 'نامشخص',
        'checking' => 'در حال بررسی',
        'checked'  => 'بررسی شده',
    ],

    'reviews_status' => [
        'draft'     => 'پیش‌نویس',
        'pending'   => 'درخواست شده',
        'suspended' => 'معلق',
        'accepted'  => 'تایید شده',
    ],

    'attachment_types' => [
        'Image' => 'تصویر',
        'Video' => 'کلیپ',
        'Sound' => 'صدا',
    ],

    'hyperlink_type' => [
        'anchor'      => 'انکر',
        'internal'    => 'id داخلی',     // model:id
        'ex-internal' => 'url داخلی',  // internal link, but exact url, not db id
        'external'    => 'خارجی',
    ],

    'menus_url_type' => [
        'route' => 'مسیر',
        'link'  => 'لینک',
    ],

    'seo_details_type' => [
        'google'   => 'گوگل',
        'facebook' => 'فیس‌بوک',
        'twitter'  => 'توئیتر',
    ],

    'seo_positions_status' => [
        'draft'   => 'پیش‌نویس',
        'pending' => 'درخواست شده',
        'active'  => 'فعال',
    ],

    'requests_priorities' => [
        'low'  => 'کم',
        'med'  => 'متوسط',
        'high' => 'زیاد',
    ],

    'requests_status' => [
        'draft'     => 'پیش‌نویس',
        'requested' => 'درخواست شده',
        'accepted'  => 'تایید شده',
        'rejected'  => 'رد شده',
        'canceled'  => 'کنسل شده',
        'done'      => 'نهایی شده',
    ],

    'settings_mode' => [
        'text'     => 'متن',
        'html'     => 'HTML',
        'textarea' => 'فیلد متنی',
        'checkbox' => 'چک‌باکس',
        'image'    => 'تصویر',
    ],

    'settings_direction' => [
        'ltr' => 'چپ به راست',
        'rtl' => 'راست به چپ',
    ],

    'payments_status' => [
        'draft'    => 'پیش‌نویس',
        'pending'  => 'درخواست شده',
        'accepted' => 'تایید شده',
        'rejected' => 'رد شده',
    ],

    'bot_connections' => [
        'telegram'  => 'تلگرام',
        'instagram' => 'اینستاگرام',
        'facebook'  => 'فیس‌بوک',
        'skype'     => 'اسکایپ',
    ],
];
