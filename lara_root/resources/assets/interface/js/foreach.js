if (typeof window.forEach === 'undefined'){
    window.forEach = function (obj, callback, scope) {
        if(obj instanceof Array){
            for (var i = 0; i < obj.length; i++) {
                callback.call(scope, i, obj[i]);
            }
        }
        else{
            for (const i in obj) {
                callback.call(scope, i, obj[i]);
            }
        }
    }
}
