import * as $ from 'jquery';
window.$ = window.jQuery = $;

window.$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

window.htmlItems = {}

window.selectElementContents = function (el) {
    let range = document.createRange();
    range.selectNodeContents(el);
    let sel = window.getSelection();
    sel.removeAllRanges();
    sel.addRange(range);
}

// require('./article-category/croppie')
require('./article-category/save-article-category')
require('./article-category/page-element')
require('./article-category/action-buttons')
require('./article-category/inline-action-buttons')
require('./article-category/heading-action-buttons')
require('./article-category/hover-action-buttons')
require('./article-category/seo-meta-form')
require('./article-category/image-uploader')
require('./article-category/prepare-for-edit')

require('./article-category/sub-item/save-sub-item')
