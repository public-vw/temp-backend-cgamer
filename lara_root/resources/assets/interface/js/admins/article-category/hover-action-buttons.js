require('select2')
$(function () {

    let link = {
        'url':null,
        'mode':'anc', // anc|int|ext
        'new':true,
        'nof':true,
    };

    $('.hover-action-forms.set-link #anchor-target').change(function () {
        link['url'] = $(this).val()
    })

    $('.hover-action-forms.set-link #internal-target').change(function () {
        link['url'] = $(this).val()
    })

    $('.hover-action-forms.set-link #external-target').change(function () {
        link['url'] = $(this).val()
    })

    //pushing nav buttons to change link type
    $('.hover-action-forms.set-link .nav-buttons button').on('click', function () {
        link['mode'] = $(this).attr('data-bind')
        $('.row-box').hide()
        $('.row-box'+$(this).attr('data-bind')).show()
    })

    $('.hover-action-forms.set-link [type="checkbox"]').on('click', function () {
        link[$(this).attr('data-config')] = $(this).prop('checked')
    })

    //pushing hover action buttons (now only link button)
    $('#hover-action-buttons .set-link').on('click', function () {
        $('#hover-action-buttons').hide()
        initializeHoverLinkForm();
        $('.hover-action-forms.frm.set-link').show(100)
    })

    function initializeHoverLinkForm(){
        let anchorNavButton = $('.hover-action-forms .nav-buttons button[data-bind=".anchor"]');
        let headings = $('.post-open-content-body').find('h2,h3,h4');

        updateInternalSelect()

        //fill anchor list
        $('#anchor-target option:not(:first)').remove()
        if($(headings).length === 0){
            $(anchorNavButton).hide()
            $('#anchor-target').hide()
            return
        }

        $(anchorNavButton).show()
        if(
            $(anchorNavButton).hasClass('selected')
        ) $('#anchor-target').show()

        $(headings).each(function (){
            var id = '#' + $(this).attr("id");
            $('#anchor-target').append(`<option value='${id}'>${$(this).text()}</option>`)
        })

    }

    function updateInternalSelect(){
        $('#internal-target').select2({
            ajax: {
                url: window.js_route('article_search'),
                type: 'POST',
                dataType: 'json'
            }
        });
    }

    // pushing CANCEL button, on hover link form
    $('.hover-action-forms.set-link .btn-cancel').on('click', function () {
        $('.row-box').hide();
        $('.hover-action-forms').hide()
    })

    // pushing SUBMIT button, on hover link form
    $('.hover-action-forms.frm.set-link .submit .btn').on('click', function () {
        if(typeof link['url'] === 'undefined') return;
        pasteHtmlAtCaret()
        $('.hover-action-forms.frm.set-link').hide(100)
        $('.hovered').focus()
        window.handleInput('body', $('#article-content'))
    })

    function makeHref(){
        let html = window.htmlItems['link'].clone(true,true)
        $(html).attr('href',link['url'])
        $(html).attr('data-config',makeHrefConfig())
        $(html).html(selectionHTML)
        return $(html).prop('outerHTML')
    }

    function makeHrefConfig(){
        var mode = link['mode'].substring(1,4)
        var ret = ''
        switch(mode){
            case 'ext':
                ret = link['nof']?',nof':'' + ret
            case 'int':
                ret = (link['new']?',new':'') + ret
        }
        return ret
    }


    $('.post-open-content').on('mouseup', '.page-element', showHoverActions)
    $('.post-open-content').on('mousedown', '.page-element', hideHoverActions)
    $('.post-open-content').on('keyup', '.page-element', showHoverActions)

    let selection = false
    let selectionRange = false
    let selectionHTML = null
    function hideHoverActions() {
        $('.hover-action-forms.frm.set-link').hide(100)
        $('#hover-action-buttons').hide()
        selectionRange = false
        selectionHTML = null
    }

    function showHoverActions(e) {
        $('#hover-action-buttons').hide()
        if ($(e.target).attr('data-changed') !== '1') return;

        rect = isBoxSelection(e.target)
        if(rect){
            let pointX = rect.left - 41
            let pointY = window.scrollY + rect.top - 133

            $('#hover-action-buttons').css('left',Math.max(0,pointX))
            $('#hover-action-buttons').css('top',Math.max(0,pointY))

            // $('.hover-action-forms').css('left',Math.max(0,pointX))
            $('.hover-action-forms').css('top',Math.max(0,pointY + 45))

            $('#hover-action-buttons').show()
        }
    }

    function isBoxSelection(el) {
        let position = {start: 0, end: 0};
        selection = document.getSelection();
        let oRect

        if (selection.rangeCount) {
            selectionRange = selection.getRangeAt(0);
            let oRange = selectionRange.cloneRange()  // don't mess with visible cursor
            oRect = oRange.getBoundingClientRect()
            oRange.selectNodeContents(el)    // select content
            position.start = selectionRange.startOffset
            position.end = selectionRange.endOffset

            var container = document.createElement("div");
            for (var i = 0, len = selection.rangeCount; i < len; ++i) {
                container.appendChild(selection.getRangeAt(i).cloneContents());
            }
            selectionHTML = container.innerHTML;
        }
        let isBox = (position.end - position.start) > 0 && (position.end !== 1)
        return isBox? oRect : false;
    }

    function pasteHtmlAtCaret() {
        html = makeHref();
        selectionRange.deleteContents();

        const el = document.createElement("div");
        el.innerHTML = html;
        let frag = document.createDocumentFragment(),
            node,
            lastNode;
        while ((node = el.firstChild)) {
            lastNode = frag.appendChild(node);
        }
        selectionRange.insertNode(frag);

        // Preserve the selection
        if (lastNode) {
            selectionRange = selectionRange.cloneRange();
            selectionRange.setStartAfter(lastNode);
            selectionRange.collapse(true);
            selection.removeAllRanges();
            selection.addRange(selectionRange);
        }
    }

    function pasteHtmlAtCaret0(url) {
        let html = window.htmlItems['link'].clone(true,true)
        $(html).attr('href',url).html(selectionHTML)
        html = $(html).prop('outerHTML')

        // if(!selection || !selection.rangeCount) return;
        let range = selection.getRangeAt(0);
        selectionRange.deleteContents();

        const el = document.createElement("div");
        el.innerHTML = html;
        let frag = document.createDocumentFragment(),
            node,
            lastNode;
        while ((node = el.firstChild)) {
            lastNode = frag.appendChild(node);
        }
        selectionRange.insertNode(frag);

        // Preserve the selection
        if (lastNode) {
            range = selectionRange.cloneRange();
            range.setStartAfter(lastNode);
            range.collapse(true);
            selection.removeAllRanges();
            selection.addRange(range);
        }
    }
})
