$(function () {
    $('#add-image').click(function () {
        // $('#cropi').attr('data-id',$(this).closest('.cropiTrigContainer').attr('id'));

        var randId = 'img' + (Math.random() + 1).toString(36).substring(5);
        window.addPageElement('image', window.target_element,{'id': randId.toString()})
        $('#uploader').attr('data-image-id', randId)
        $('#uploader').attr('data-image-type', 'upload_image_inside')

        $('#uploader').trigger('click');
    });

    $('#add-paragraph').click(function () {
        window.addPageElement('paragraph', window.target_element)
    });

    $('#add-heading2').click(function () {
        var randId = 'h2' + '-' + (Math.random() + 1).toString(36).substring(5);
        window.addPageElement('heading2', window.target_element,{'id':randId})
    });

    $('#add-heading3').click(function () {
        var randId = 'h3' + '-' + (Math.random() + 1).toString(36).substring(5);
        window.addPageElement('heading3', window.target_element,{'id':randId})
    });

    $('#add-heading4').click(function () {
        var randId = 'h4' + '-' + (Math.random() + 1).toString(36).substring(5);
        window.addPageElement('heading4', window.target_element,{'id':randId})
    });

    $('#add-ul').click(function () {
        window.addPageElement('ul', window.target_element)
    });

    $('#add-ol').click(function () {
        window.addPageElement('ol', window.target_element)
    });

    window.addPageElement = function(key, insertAfterSelector = null, repl = null, moveActions = true){
        let tmp = window.htmlItems[key].clone(true,true)
        $.each(repl, function (k, v){
            tmp.attr(k,v)
        })

        if(insertAfterSelector === null){
            tmp.insertBefore('#action-buttons').show(500)
        }
        else{
            tmp.insertAfter(insertAfterSelector).show(500)
        }

        if(moveActions === true){
            window.showActionBars(tmp)
        }
    }
})
