$(function () {
    //catch templates, and remove them
    $('.template[data-key]').each(function () {
        let el = $(this)
        let key = $(el).attr('data-key')

        $(el).removeAttr('style')
        $(el).removeClass('template')
        $(el).addClass('page-element')

        window.htmlItems[key] = $(el).clone(true,true)

        $(el).remove()
    })

    //edit article
    $('[data-key]').each(function () {
        $(this).addClass('page-element')
    })

    //new article
    if ($('#article-body').attr('data-id') === '' || $('#article-body').attr('data-length') === '0') {
        window.addPageElement('paragraph','.placeholder',null,false )
        $('.placeholder').remove()
    }

    //removes styles in paste event
    $(window).on('paste', function(e){
        e.preventDefault();
        const text = (e.originalEvent || e).clipboardData.getData('text/plain');
        window.document.execCommand('insertText', false, text);
    })

})
