window.itemSaveModes = {
    'needSave': "<span class='btn-icon'><i class='fas fa-save'></i></span>\n" +
        " <span class='btn-text'>ذخیره کن</span>\n",

    'saving': '<span class="btn-icon"><i class="fas fa-spinner fa-spin"></i></span>\n' +
        ' <span class="btn-text">در حال ذخیره ...</span>\n',

    'saved': '<span class="btn-icon"><i class="fas fa-check"></i></span>\n' +
        ' <span class="btn-text">ذخیره شد</span>\n',

    'errorInSave': '<span class="btn-icon"><i class="fas fa-times"></i></span>\n' +
        ' <span class="btn-text">مشکل در آپلود!</span>\n',
}

window.artcileItem = {
    id: null,
    header_img: null,
    header: null,
    body: null,
}

window.handleItemInput = function (section, el, type = 'html') {
    window.artcileItem[section] = (type === 'html') ? sanitizeHtml($(el)) : $(el).text();
    $('.item-container').css('opacity','0.1')
    var container = $(el).closest('.item-container')
    container.css('opacity','1')
    window.artcileItem.id = container.attr('data-cat-id')
    container.find('.item-save-action').html(window.itemSaveModes['needSave']).show(200).attr('data-state', "needSave")
}

// if article is created already and now is under edit
if ($('#article-body').has('data-id')) {
    window.article.id = $('#article-body').attr('data-id')
    window.article.header = $('#article-header').text()
    window.article.body = sanitizeHtml($('#article-content'))
}

// $('#action-buttons').on('click','.save-action', function () {
$('.item-save-action').on('click', function () {
    window.itemSaveIt(this)
})

window.itemSaveIt = function(obj){
    if ($(obj).attr('data-state') !== 'needSave') return;

    $(obj).html(window.itemSaveModes['saving']).attr('data-state', "saving")

    console.log($(obj).attr('data-item-type'))
    $.ajax({
        type: 'POST',
        url: window.js_route('item_save',[$(obj).attr('data-item-type')]),
        data: window.artcileItem,
        success: function (response) {
            window.artcileItem = {
                id: null,
                header_img: null,
                header: null,
                body: null,
            }
            $('.item-container').css('opacity','1')
            $(obj).html(window.itemSaveModes['saved']).attr('data-state', "saved").delay(1000).hide(200)
        },
        error: function (err) {
            $(obj).html(window.itemSaveModes['errorInSave']).attr('data-state', "needSave")
            console.log(err)
        }
    })
}
