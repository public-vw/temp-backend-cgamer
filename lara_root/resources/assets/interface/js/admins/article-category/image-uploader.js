$(function () {
    $('#uploader').on('change', function () {
        // jquery trigger wouldn't work here
        // document.getElementById("popup-trigger").click()
        // another way to trig click, if above not working, check this
        // document.getElementById("popup-trigger").dispatchEvent(new Event('click'));

        let imageId = $('#uploader').attr('data-image-id')
        let file = this.files[0];

        let reader = new FileReader();
        reader.onload = function (e) {
            var image = new Image();
            image.src = reader.result;
            addImageToArticle(imageId, image)

            image.onload = function () {
                $('#uploader-form').trigger('submit');
            }

            image.onerror = function (e) {
                console.log('Local load error', e)
            }
        }
        reader.readAsDataURL(file);

        // reader.onloadstart = function(e) {
        //     var msg = "File Name: " + file.name + "<br>" +
        //         "File Size: " + file.size + "<br>" +
        //         "File Type: " + file.type;
        //     console.log("onloadstarted",msg);
        // }

        // reader.onloadend = function(e) {
        //     console.log("onloadend, state = " + reader.readyState);
        // }
    });

    function addImageToArticle(imageId, image) {
        $('#' + imageId).find('img:first').prop('src', image.src);
        $('#' + imageId).find('img:first').css('opacity', '0.4');
        $('#' + imageId + '.liquid').css('background-image', 'url('+image.src+')');
        $('#' + imageId + '.liquid').css('opacity', '0.4');
    }

    $('#uploader-form').submit(function (e) {
        let imageId = $('#uploader').attr('data-image-id')
        let imageType = $('#uploader').attr('data-image-type')

        e.preventDefault();
        let formData = new FormData(this);


        $.ajax({
            type: 'POST',
            url: window.routes[imageType],
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: (response) => {
                activateImageInArticle(imageId, response)
                this.reset();
            },
            error: function (data) {
                document.getElementById("uploader").value = "";
                console.log(data);
            }
        });
    });

    function activateImageInArticle(imageId, dbid) {
        let imageType = $('#uploader').attr('data-image-type')
        if(imageType === 'upload_image_header'){
            window.article.header_img = dbid;
        }
        else{
            $('#' + imageId).find('img:first').attr('data-db-id', dbid);
        }

        $('#' + imageId).find('img:first').css('opacity', '1');
        $('#' + imageId + '.liquid').css('opacity', '1');
        window.handleInput('body', $('#article-content'))
        $('.save-action').html(window.saveModes['needSave']).show(200).attr('data-state', "needSave")
    }

})
