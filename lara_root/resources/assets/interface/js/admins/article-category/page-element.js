window.target_element = null;
window.mouseover_timeout = null;

function getOffset(el) {
    const rect = el.getBoundingClientRect();
    return {
        left: rect.left + window.scrollX,
        right: rect.left + rect.width + window.scrollX,
        top: rect.top + window.scrollY,
        bottom: rect.top + rect.height + window.scrollY
    };
}

$(function () {
    $(window).on('keydown', function (e) {
        if ((e.ctrlKey || e.metaKey) && (e.keyCode === 13 || e.keyCode === 10)) {
            window.saveIt()
        } else if (e.which === 13) {
            $('#inline-action-buttons').hide(100);
            $('#action-buttons').hide(100);
            $(e.target).find('.hovered:first').removeClass('hovered');
        }
    });

    /*
        $('#inline-action-buttons').on('mouseover', function (e) {
            clearTimeout(window.mouseover_timeout);
            e.stopPropagation()
        })
    */

    /*
        $('.post-open-content').on('mouseover', '.page-element', function () {
            clearTimeout(window.mouseover_timeout);

            if ($(this).hasClass('hovered')) return;

            if ($('.hovered').length === 0) {
                window.showActionBars(this);
                return;
            }

            window.mouseover_timeout = setTimeout(window.showActionBars, 350, this);
        })
    */

    $(window).on('resize', function () {
        clearTimeout(window.mouseover_timeout);
        $('.page-element').removeClass('hovered');
        $('#inline-action-buttons').hide();
        $('#action-buttons').hide();
    })

    // $('.post-open-content').on('resize', '.page-element', function () {
    //     window.adjustBottomActionBar(this)
    // })

    $('.post-open-content').on('click', '.page-element', function () {
        console.log('111')
        clearTimeout(window.mouseover_timeout);
        if (!$(this).hasClass('hovered')) {
            window.showActionBars(this);
        }

        // TODO: We should find a way to change this attribute on .page-element on edits
        // Then this will work
        if ($(this).attr('data-changed') !== '1') {
            window.selectElementContents(this);
            $(this).attr('data-changed', '1')
        }
    })

    window.showActionBars = function (obj) {
        window.target_element = obj;
        $('.page-element').removeClass('hovered');
        $(obj).addClass('hovered');

        window.adjustInlineActionBar(obj)//means buttons are related to obj type
        window.adjustHoverActionBar()
        window.adjustBottomActionBar()
    }

    window.adjustInlineActionBar = function (obj) {
        //hides inline action bar and change position
        $('#inline-action-buttons').hide(100, function () {
            $('#inline-action-buttons').insertBefore($('.hovered'))
        })

        //change actions
        let element_type = $(obj).attr('data-key')
        $('#inline-action-buttons').find('.btn').hide();

        $(window.operations[element_type]).each(function () {
            $('#' + this).show();
        })
        $(window.operations['all']).each(function () {
            $('#' + this).show();
        })

        let objIndex = $(obj).index('[data-key].page-element')
        if (objIndex === 0) $('#go-up').hide();
        if (objIndex >= $('[data-key].page-element').length - 1) $('#go-down').hide();

        //shows inline action bar
        $('#inline-action-buttons').show(100)
    }

    window.adjustHoverActionBar = function () {
        //hides hover action bar and change position
        $('#hover-action-buttons').hide(100, function () {
            $('#hover-action-buttons').insertBefore($('.hovered'))
        })
    }

    window.adjustBottomActionBar = function () {
        //hides action bar and change position
        $('#action-buttons').fadeOut(100, function () {
            $('#action-buttons').insertAfter($('.hovered'))
        })

        //shows action bar
        $('#action-buttons').fadeIn(100)
    }
})
