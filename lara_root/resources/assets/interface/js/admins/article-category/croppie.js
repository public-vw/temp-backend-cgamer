$('#uploader').on('change', function () {
    makeUploadCrop()
    // jquery trigger wouldn't work here
    document.getElementById("popup-trigger").click()
    // another way to trig click, if above not working, check this
    // document.getElementById("popup-trigger").dispatchEvent(new Event('click'));

    var reader = new FileReader();
    reader.onload = function (e) {
        image_data = e.target.result
        uploadCrop.croppie('bind', {
            url: image_data
        }).then(function () {
        });
    }
    reader.readAsDataURL(this.files[0]);
});


//-------------------------------------------------- CROPPIE

var image_data = null;
var orientation = 1;
var uploadCrop;


var sizes = {
    result: {w: 690, h: 320},
    view: {w: 690},
    bound: {w: 690},
}

sizes.ratio = sizes.result.w / sizes.result.h;
sizes.bound.h = Math.round(sizes.bound.w / sizes.ratio)
sizes.view.h = Math.round(sizes.view.w / sizes.ratio)

new XM_Popup({
    'container': '#cropi_container',
    // 'trigger' : '#add-image',
    'trigger': '#popup-trigger',
    'sticky': true,
})

function makeUploadCrop() {
    uploadCrop = $('#cropi_image').croppie({
        enableResize: true,
        enableOrientation: true,
        enableExif: true,
        boundary: {
            width: sizes.bound.w,
            height: sizes.bound.h
        },
        viewport: {
            width: sizes.view.w,
            height: sizes.view.h,
            scale: 0.5
        },
        enforceBoundary: true
    });
}

function killUploadCrop() {
    uploadCrop = null;
}

$('#upload_cancel').click(function () {
    document.getElementById("xm_overlay").click()
});
$('#upload_result').on('click', function () {
    document.getElementById("xm_overlay").click()
    let cr_result = null;
    uploadCrop.croppie('result', {
        type: 'blob',
        // size: 'viewport',
        size: {
            width: sizes.result.w,
            height: sizes.result.h
        },
        format: 'jpeg',
        quality: 100
    }).then(function (e) {
        killUploadCrop()
        $(image.replace('######', e)).insertBefore('#add-image').show(500);
    });
    // $(imageParagraph).insertBefore(this).show(500);
})

//--------------------------------------------------

$('.crop-btn-rotate').click(function () {
    deg = parseInt($(this).attr('data-deg'));
    uploadCrop.croppie('rotate', deg);
});

$('.crop-btn-flip').click(function () {
    deg = parseInt($(this).attr('data-deg'));
    console.log(uploadCrop.url);
    orientation = (orientation === deg) ? 1 : deg;
    uploadCrop.croppie('bind', {
        url: image_data,
        orientation: orientation
    })
});

