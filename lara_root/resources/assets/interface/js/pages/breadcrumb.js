new Vue({
    el: '.post-open-breadcrumb',

    data: {
        visible: false,
    },

    mounted() {
        this.myEventHandler()
        window.addEventListener("resize", this.myEventHandler);
    },
    destroyed() {
        window.removeEventListener("resize", this.myEventHandler);
    },

    methods: {
        myEventHandler(e) {
            measure = this.$refs.breadcrumb.clientHeight
            this.visible = (measure < 40)
        }
    }
})
