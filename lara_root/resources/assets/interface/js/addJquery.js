import * as $ from 'jquery';
window.$ = window.jQuery = $;

window.$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

