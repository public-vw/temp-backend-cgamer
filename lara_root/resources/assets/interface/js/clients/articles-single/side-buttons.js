$(function () {
    $('.set-requested').click(function () {
        window.saveIt(
            function(){
                $.ajax({
                    type: 'POST',
                    url: js_route('articles_single_change_status',['requested']) ,
                    data: {
                        'id': window.article.id
                    },
                    success: function (response) {
                        $('.set-requested').html('درخواست بررسی ارسال شد').removeClass('set-requested')
                    },
                    error: function (err) {
                        console.log(err)
                    }
                })
            }
        )
    })

    $('.set-ready').click(function () {
        window.saveIt(
            function(){
                $.ajax({
                type: 'POST',
                url: js_route('articles_single_change_status',['ready']) ,
                data: {
                    'id': window.article.id
                },
                success: function (response) {
                    $('.announcement').html('در صف انتشار قرار گرفت')
                    $('.set-rejected,.set-seo-ready').hide(100).remove()
                },
                error: function (err) {
                    console.log(err)
                }
            })
            }
        )
    })

    $('.set-active').click(function () {
        $.ajax({
            type: 'POST',
            url: js_route('articles_single_change_status',['active']) ,
            data: {
                'id': window.article.id
            },
            success: function (response) {
                $('.announcement').html('منتشر شد')
                $('.set-active').hide(100).remove()
            },
            error: function (err) {
                console.log(err)
            }
        })
    })

    $('.set-checking').click(function () {
        window.saveIt(
            function(){
                $.ajax({
                    type: 'POST',
                    url: js_route('articles_single_change_status',['checking']) ,
                    data: {
                        'id': window.article.id
                    },
                    success: function (response) {
                        $('.announcement').html('برای بررسی قفل شد')
                        $('.set-checking').hide(100).remove()
                    },
                    error: function (err) {
                        console.log(err)
                    }
                })
            }
        )
    })

    $('.set-seo-ready').click(function () {
        window.saveIt(
            function (){
                $.ajax({
                    type: 'POST',
                    url: js_route('articles_single_change_status',['seo-ready']) ,
                    data: {
                        'id': window.article.id
                    },
                    success: function (response) {
                        $('.announcement').html('برای انتشار ارسال شد')
                        $('.set-rejected,.set-ready').hide(100).remove()
                    },
                    error: function (err) {
                        console.log(err)
                    }
                })
            }
        )
    })

    $('.set-rejected').click(function () {
        window.saveIt(
            function(){
                $.ajax({
                    type: 'POST',
                    url: js_route('articles_single_change_status',['rejected']) ,
                    data: {
                        'id': window.article.id
                    },
                    success: function (response) {
                        $('.announcement').html('برای اصلاح ارسال شد')
                        $('.set-rejected,.set-ready,.set-seo-ready').hide(100).remove()
                    },
                    error: function (err) {
                        console.log(err)
                    }
                })
            }
        )
    })
})
