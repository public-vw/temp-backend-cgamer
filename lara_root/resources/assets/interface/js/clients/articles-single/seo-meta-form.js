$(function () {

    window.meta = {}

    $('.seo-meta-forms.set-meta .row-box [data-part]').change(function () {
        var mode = $(this).attr('data-mode')
        var part = $(this).attr('data-part')

        if(typeof window.meta[mode] == 'undefined') window.meta[mode] = {}
        window.meta[mode][part] = $(this).val()
    })

    // pushing SUBMIT button, on meta form
    $('.seo-meta-forms.frm.set-meta .submit .btn').on('click', function () {
        $.ajax({
            type: 'POST',
            url: js_route('article_save_seo_meta',[$('#article-body').attr('data-id')]),
            data: window.meta,
            success: function (response) {
                $('.seo-meta-forms.frm.set-meta').hide(100)
            },
            error: function (err) {
                console.log(err)
            }
        })

    })


    // pushing seo button, below of page header
    $('#seo_meta_button').on('click', function () {
        initializeMetaForm()
        $('.seo-meta-forms.set-meta').show(150)
    })

    // pushing CANCEL button, on hover link form
    $('.seo-meta-forms.set-meta .btn-cancel').on('click', function () {
        $('.seo-meta-forms.set-meta').hide()
    })

    // pushing nav buttons to change meta type
    $('.seo-meta-forms.set-meta .nav-buttons button').on('click', function () {
        $('.seo-meta-forms.set-meta .nav-buttons button').removeClass('selected')
        $(this).addClass('selected')
        $('.row-box').hide()
        $('.row-box'+$(this).attr('data-bind')).show()
    })

    function initializeMetaForm(){
        $('.seo-meta-forms.set-meta .nav-buttons button').removeClass('selected')
        $('.seo-meta-forms .nav-buttons button[data-bind=".google"]').addClass('selected')
        $('.row-box').hide()
        $('.row-box.google').show()
    }

})
