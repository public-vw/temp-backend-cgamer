$(function () {
    $('#uploader').on('change', function () {
        // jquery trigger wouldn't work here
        // document.getElementById("popup-trigger").click()
        // another way to trig click, if above not working, check this
        // document.getElementById("popup-trigger").dispatchEvent(new Event('click'));

        let imageId = $('#uploader').attr('data-image-id')
        let file = this.files[0];

        let reader = new FileReader();
        reader.onload = function (e) {
            var image = new Image();
            image.src = reader.result;
            addImageToArticle(imageId, image)

            image.onload = function () {
                document.getElementById("uploader").value = "";
                splitFile(imageId, image, file)
            }

            image.onerror = function (e) {
                console.log('Local load error', e)
            }
        }
        reader.readAsDataURL(file);

        // reader.onloadstart = function(e) {
        //     var msg = "File Name: " + file.name + "<br>" +
        //         "File Size: " + file.size + "<br>" +
        //         "File Type: " + file.type;
        //     console.log("onloadstarted",msg);
        // }

        // reader.onloadend = function(e) {
        //     console.log("onloadend, state = " + reader.readyState);
        // }
    });

    function addImageToArticle(imageId, image) {
        $('#' + imageId).find('img:first').prop('src', image.src);
        $('#' + imageId).find('img:first').css('opacity', '0.4');
    }

    function splitFile(imageId, image, file){
        let parts = new Array();

        let fileReader = new FileReader()
        var chunked = 0
        let total = file.size
        let steps = 500 * 1024
        let blob = file.slice(0, steps)
        fileReader.readAsBinaryString(blob)
        fileReader.onload = function () {
            parts.push(fileReader.result)
            chunked += steps;

            if(chunked <= total){
                blob = file.slice(chunked,chunked + steps);  // getting next chunk
                fileReader.readAsBinaryString(blob);        //reading it through file reader which will call onload again. So it will happen recursively until file is completely uploaded.
            } else {                       // if file is uploaded completely
                sendImageToServer(imageId, image, parts)
                chunked = total;            // just changed loaded which could be used to show status.
            }
        };
    }

    function sendImageToServer(imageId, image, parts) {
        let pack = {
            'parts': parts,
            'ext': image.src.match(/^data:image\/(\w+);base64,/i)[1],
            'size': {
                'width': image.naturalWidth,
                'height': image.naturalHeight,
            }
        }
        pack['count'] = pack['parts'].length
        console.log(pack)
        sendMap(imageId, pack)
    }

    function sendMap(imageId, pack) {
        sendAjax(window.routes['upload_image_inside_map'],
            {
                'count': pack['parts'].length,
                'size': pack['size'],
                'ext': pack['ext'],
            },
            function (token) {
                console.log('send map done', token)
                sendPart(imageId, pack, token)
            },
            function (err) {
                console.log('send map error', err)
            }
        );
    }

    function sendPart(imageId, pack, token) {
        sendAjax(window.routes['upload_image_inside_chunks'].replace('#####',token),
            {
                'image_data' : pack['parts'].shift(),
            },
            function (response) {
                console.log('send part done', response)
                if (response === 'full done') activateImageInArticle(imageId, response)
                else sendPart(imageId, pack, token)
            },
            function (err) {
                console.log('send part error', err)
            },
            true
        );
    }

    function sendAjax(url, data, successCallback, failCallback, file = false) {
        let options = {
            type: 'POST',
            url: url,
            data: data,
            cache: false,
            success: successCallback,
            error: failCallback
        }

        if(file) {
            // options['enctype'] = 'multipart/form-data';
            options['contentType'] = false;
            options['processData'] = false;
        }

        $.ajax(options)
    }

    function activateImageInArticle(imageId, dbid) {
        $('#' + imageId).find('img:first').attr('data-db-id', dbid);
        $('#' + imageId).find('img:first').css('opacity', '1');
        window.handleInput('body', $('#article-content'))
        $('.save-action').html(window.saveModes['needSave']).show(200).attr('data-state', "needSave")
    }

})
