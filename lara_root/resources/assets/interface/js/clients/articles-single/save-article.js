window.saveModes = {
    'needSave': "<span class='btn-icon'><i class='fas fa-save'></i></span>\n" +
        " <span class='btn-text'>ذخیره کن</span>\n",

    'saving': '<span class="btn-icon"><i class="fas fa-spinner fa-spin"></i></span>\n' +
        ' <span class="btn-text">در حال ذخیره ...</span>\n',

    'saved': '<span class="btn-icon"><i class="fas fa-check"></i></span>\n' +
        ' <span class="btn-text">ذخیره شد</span>\n',

    'errorInSave': '<span class="btn-icon"><i class="fas fa-times"></i></span>\n' +
        ' <span class="btn-text">مشکل در آپلود!</span>\n',
}

window.article = {
    id: null,
    header_img: null,
    header: 'Sample Header',
    body: 'Sample Content',
}

window.handleInput = function (section, el, type = 'html') {
    window.article[section] = (type === 'html') ? sanitizeHtml($(el)) : $(el).text();
    $('.save-action').html(window.saveModes['needSave']).show(200).attr('data-state', "needSave")
}

// remove image data to prevent sending unnecessary data to backend
window.sanitizeHtml = function (obj) {
    var cloned = $(obj).clone()

    $(cloned).find('[contenteditable = "false"]').remove()
    $(cloned).find('.page-element').removeClass('page-element')
    $(cloned).find('.hovered').removeClass('hovered')
    $(cloned).find('[contenteditable = "true"]').removeAttr('contenteditable')
    $(cloned).find('[onchange]').removeAttr('onchange')
    $(cloned).find('.template').removeClass('template');
    $(cloned).find('.editable').removeClass('editable');
    $(cloned).find('[data-changed = "0"]').remove()
    $(cloned).find('[data-changed = "1"]').removeAttr('data-changed')
    $(cloned).find('[contenteditable = "true"]').removeAttr('data-changed')
    $(cloned).find('img').removeAttr('src').removeAttr('alt')
    $(cloned).find('[style]').removeAttr('style')
    $(cloned).find('[class=""]').removeAttr('class')
    $(cloned).find('[data-key="image"]').removeAttr('id')

    let html = cloned.html().trim()

    html = html.replaceAll("\n</li>","</li>\n").trim()

    html = html.replace(/(\S)\(/gm, `$1 (`).replace(/\)(\S)/gm, `) $1`);

    return html
}

// if article is created already and now is under edit
if ($('#article-body').has('data-id')) {
    window.article.id = $('#article-body').attr('data-id')
    window.article.header = $('#article-header').text()
    window.article.body = sanitizeHtml($('#article-content'))
}

$('<div>', {
    class: 'mobile-action-btn save-action',
    style: 'display: none',
    onclick: 'window.saveIt()'
}).append(
    $('<span>', {
        class: 'btn-icon'
    }).append(
        $('<i>', {
            class: 'fas fa-save'
        })
    )
).append(
    $('<span>', {
        class: 'btn-text'
    }).append('Save It')
).appendTo('body');


// $('#action-buttons').on('click','.save-action', function () {
$('.save-action').on('click', function () {
    window.saveIt()
})

window.saveIt = function(do_after){
    if ($('.save-action').attr('data-state') !== 'needSave') {do_after(); return;}

    $('.save-action').html(window.saveModes['saving']).attr('data-state', "saving")
    $.ajax({
        type: 'POST',
        url: window.routes['articles_single_save_draft'],
        data: window.article,
        success: function (response) {
            window.article.id = response
            $('#preview_button a').attr('href',window.js_route('preview',[response]));
            $('#preview_button').removeClass('not-visible');
            $('.save-action').html(window.saveModes['saved']).attr('data-state', "saved").delay(1000).hide(200)
            do_after();
        },
        error: function (err) {
            $('.save-action').html(window.saveModes['errorInSave']).attr('data-state', "needSave")
            console.log(err)
        }
    })}
