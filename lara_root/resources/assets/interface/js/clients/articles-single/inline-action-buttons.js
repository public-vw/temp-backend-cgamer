//select which operations can attach to buttons id
window.operations = {
    'paragraph': ['select-all-element'],
    'image': ['change-image'],
    'heading2': ['select-all-element'],
    'heading3': ['select-all-element'],
    'heading4': ['select-all-element'],
    'ul': ['select-all-element'],
    'ol': ['select-all-element'],
    'all': ['go-up', 'go-down', 'delete-item'],
}

$(function () {

    $('#inline-action-buttons #delete-item').on('click', function () {
        var target = window.target_element;

        $('#inline-action-buttons').hide();
        $(target).removeClass('hovered');
        $(target).slideUp(250, function () {
            $(target).remove()
            window.handleInput('body', $('#article-content'))
            $('.save-action').html(window.saveModes['needSave']).show(200).attr('data-state', "needSave")
        })

        window.target_element = null
    })

    $('#inline-action-buttons #change-image').on('click', function () {
        var target = window.target_element;

        var randId = 'img' + (Math.random() + 1).toString(36).substring(5);
        $(target).attr('id', randId)
        $('#uploader').attr('data-image-id', randId)
        $('#uploader').attr('data-image-type', 'upload_image_inside')

        $('#uploader').trigger('click');
    })

    $('#inline-action-buttons #select-all-element').on('click', function () {
        var target = window.target_element;

        window.selectElementContents(target)
    })

    $('#inline-action-buttons #go-up').on('click', function () {
        var target = window.target_element;

        let objIndex = $(target).index('.page-element')

        window.swapElements('.page-element', objIndex, objIndex - 1, false)
        window.showActionBars(target)
    })

    $('#inline-action-buttons #go-down').on('click', function () {
        var target = window.target_element;

        let objIndex = $(target).index('.page-element')

        window.swapElements('.page-element', objIndex, objIndex + 1, true)
        window.showActionBars(target)
    })

    window.swapElements = function (query, first, second,after = true) {
        let objects = $(query)

        if(after) {
            $(objects[first]).insertAfter(objects[second])
        }
        else{
            $(objects[first]).insertBefore(objects[second])
        }
    }

})
