require('../../addJquery')
require('../../foreach')

let inputs = {}

$('[data-col]').on('input',function (){
    var field = $(this).attr('data-col')
    inputs[field] = $(this).text().trim()
    $('.save-action').toggle(isChanged())
    $(this).removeClass('has-error')

    var errorbox = $(`.error-box[data-col='${field}']`)
    $(errorbox).slideUp(150)
    $(errorbox).find('span').text('')

    if ($(this).attr('data-cur') === inputs[field]) {
        delete inputs[field]
    }

})

$('.save-action').on('click',function (){
    var url = $(this).attr('data-url')
    if (window.debugMode) console.log('url: ' + url)

    $.ajax({
        type: 'POST',
        url: window.js_route(url),
        data: inputs,
        // contentType: 'application/json; charset=utf-8;',
        success: function (response) {
            updateCurrentVal()
        },
        error: function (err) {
            let errResponse = $.parseJSON(err.responseText);
            if(window.debugMode) console.log(err,errResponse)
            $.each(errResponse['errors'], function (field, message){
                $(`[data-col='${field}']:not(.error-box)`).addClass('has-error')
                $(`.error-box[data-col='${field}']`).slideDown(250)
                    .find('span').text(message);
            })
        }
    })
})
$('.error-box').on('click',function (){
    var field = $(this).attr('data-col')
    var target = $(`[data-col='${field}']`)
    target.removeClass('has-error').text(target.attr('data-cur'))
    delete inputs[field]
    $(this).slideUp(150)
    $(this).find('span').text('')
    $('.save-action').toggle(isChanged())
})

function isChanged() {
    var ret = false
    $.each(inputs, function (key, val) {
        var select = $(`[data-col='${key}']`)
        if (select.attr('data-cur') !== val) {
            ret = true
            return false
        }
    })
    return ret;
}

function updateCurrentVal() {
    $('[data-col]').each(function(){
        var col = $(this).attr('data-col');
        $(this).attr('data-cur', inputs[col]);
    })
    inputs = {}
    $('.save-action').toggle(false)
}
