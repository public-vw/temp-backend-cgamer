require('../addJquery')

window.htmlItems = {}

window.selectElementContents = function (el) {
    let range = document.createRange();
    range.selectNodeContents(el);
    let sel = window.getSelection();
    sel.removeAllRanges();
    sel.addRange(range);
}

// require('./articles-single/croppie')
require('./articles-single/save-article')
require('./articles-single/page-element')
require('./articles-single/action-buttons')
require('./articles-single/inline-action-buttons')
require('./articles-single/heading-action-buttons')
require('./articles-single/hover-action-buttons')
require('./articles-single/image-uploader')
require('./articles-single/prepare-for-edit')
require('./articles-single/seo-meta-form')

require('./articles-single/side-buttons')

