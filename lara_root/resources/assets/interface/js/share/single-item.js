require('../foreach')

window.forEach(document.querySelectorAll('.vue-el'), function (i, element) {
    new Vue({
        el: element,

        data: {
            item: {},
            urls: [],
            updating: false,
        },

        created() {
            this.updateData(window.routes[element.getAttribute('data-route')])
        },

        methods:{
            js_route: window.js_route,
            updateData(url){
                if(this.updating)return;
                this.updating = true;
                if(this.urls.includes(url)){
                    if(window.debugMode)console.log('stopped: '+url)
                    this.updating = false;
                    return;
                }
                axios.post(url)
                    .then(response => {
                        this.item = response.data;
                        this.urls.push(url)
                        if(window.debugMode)console.log('added: '+url)
                    })
                    .catch(error => console.log(error))
                    .then(this.updating = false);
            }
        }
    })
})
