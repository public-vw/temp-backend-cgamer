require('../foreach')

window.forEach(document.querySelectorAll('.vue-el'), function (i, element) {
    new Vue({
        el: element,

        data: {
            pagination: {
                meta: {
                    cursor: {
                        next: null,
                    }
                }
            },
            page_info: {
                next_page_url: null,
                count: 'receiving ...',
                total: null,
            },
            items: {},
            urls: [],
            updating: false,
        },

        mounted() {
            this.updateData(window.routes[element.getAttribute('data-route')])
            this.scrollCheck()
        },

        watch: {
            pagination: {
                handler(val) {
                    const that = this

                    that.page_info.next_page_url = val.meta.cursor.next
                    that.page_info.count = val.meta.cursor.count
                    if (!that.page_info.total) {
                        that.page_info.total = val.meta.cursor.count
                    }

                    let counter = Object.keys(that.items).length + 1;
                    val.data.forEach((d) => that.items[(counter++).toString().padStart(that.page_info.count.toString().length + 1, '0')] = d)
                },
            }
        },

        methods: {
            js_route: window.js_route,
            updateData(url) {
                if (this.updating) return;
                this.updating = true;
                if (this.urls.includes(url)) {
                    if (window.debugMode) console.log('stopped: ' + url)
                    this.updating = false;
                    return;
                }
                axios.post(url)
                    .then(response => {
                        this.pagination = response.data;
                        this.urls.push(url)
                        if (window.debugMode) console.log('added: ' + url)
                    })
                    .catch(error => console.log(error))
                    .then(this.updating = false);
            },
            nextPage() {
                this.updateData(this.page_info.next_page_url)
            },
            scrollCheck() {
                let already_bottom = false;
                window.onscroll = () => {
                    const topMargin = element.innerHeight - 100;
                    let bottomOfWindow =
                        (document.documentElement.scrollTop + window.innerHeight > document.documentElement.offsetHeight - $('.footer_one').innerHeight() - topMargin);
                    if (!already_bottom && this.page_info.next_page_url && bottomOfWindow) {
                        already_bottom = true;
                        that = this;
                        setTimeout(function () {
                            that.nextPage()
                            already_bottom = false;
                        }, 50);
                    }
                }
            },
        }
    })
})
