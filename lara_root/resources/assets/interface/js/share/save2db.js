require('../foreach')

window.forEach(document.querySelectorAll('.vue-el-up'), function (i, element) {
    new Vue({
        el: element,

        data: {
            inputs: {},
            showSave: false,
        },

        methods: {
            js_route: window.js_route,
            saveInputs(url) {
                axios.post(url)
                    .then(response => {
                        this.updateCurrentVal()
                        if (window.debugMode) console.log('added: ' + url)
                    })
                    .catch(error => console.log(error))
            },

            handleInput(e) {
                this.inputs[e.target.getAttribute('data-col')] = e.target.innerText
                this.showSave = this.isChanged()
            },

            isChanged() {
                window.forEach(this.inputs, function (key, val) {
                    var select = document.querySelector(`[data-col='${key}']`)
                    if (select.getAttribute('data-cur') !== val) return true;
                })
                return false;
            },

            updateCurrentVal() {
                var i, dataElements = document.querySelectorAll('[data-col]')

                for (i = 0; i < dataElements.length; i++) {
                    var col = dataElements[i].getAttribute('data-col');
                    dataElements[i].setAttribute('data-cur', this.inputs[col]);
                }

                this.showSave = false
            },
        }
    })
})
