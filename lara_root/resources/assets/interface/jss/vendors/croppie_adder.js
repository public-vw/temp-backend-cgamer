$(function(){

    var cropiClipboard_src = null;
    var cropiClipboard_id = null;
//--------------------------------------------------
    $('.cropiTrig').click(function(){
        $('#cropi').attr('data-id',$(this).closest('.cropiTrigContainer').attr('id'));
        $('#uploader').trigger('click');
        return false;
    });

//--------------------------------------------------
    $('#uploader').on('change', function () {
        $('#cropi').show();
        var reader = new FileReader();
        reader.onload = function (e) {
            uploadCrop.bind({
                url: e.target.result
            }).then(function(){});
        }
        reader.readAsDataURL(this.files[0]);
    });

//--------------------------------------------------
    $('#upload_result').on('click', function (ev) {
        var id = $('#cropi').attr('data-id');
        $("#"+id+" img").attr('src',waiting_icon);
        uploadCrop.result({
            type: 'canvas',
            size: 'viewport'
            // ,size: {width: ctrl.width, height: ctrl.height},
            // format: 'jpeg',
            // quality: 1
       })
        .then(function (resp) {
            // scope.$apply( function () {
            //     ctrl.croppedImage = resp;
            // } );

            $('#cropi').hide();
            $.ajax({
                url: upload_url,
                method: "POST",
                cache:false,
//                dataType:'json',
//                contentType: 'application/x-www-form-urlencoded',
                data: { "image":resp },
                success: function (data) {
                    if(isNaN(data))return;
                    $("#"+id+" img").attr('src',resp);
                    $("#"+id+" input").val(data);
                    $("#"+id).find('.cropiCopy').show();
                    $("#"+id).find('.cropiTrigEdit').show();

                }
            });
        });
    });

//--------------------------------------------------
    $('.cropiRemove').click(function(){
        $(this).closest('.cropiTrigContainer').find('img.cropiTrig').attr('src',$(this).attr('data-src'));
        $(this).closest('.cropiTrigContainer').find('.picture_id').val(null);
        $(this).closest('.cropiTrigContainer').find('.cropiCopy').hide();
        $(this).closest('.cropiTrigContainer').find('.cropiTrigEdit').hide();
        return false;
    });

//--------------------------------------------------
    $('.cropiCopy').click(function(){
        console.log('copy2');
        cropiClipboard_src = $(this).closest('.cropiTrigContainer').find('img.cropiTrig').attr('src');
        cropiClipboard_id = $(this).closest('.cropiTrigContainer').find('.picture_id').val();
        $('.cropiTrigContainer .cropiPaste').show();
        console.log(cropiClipboard_src,cropiClipboard_id);
        return false;
    });

//--------------------------------------------------
    $('.cropiPaste').click(function(){
        console.log('paste2');
        $(this).closest('.cropiTrigContainer').find('img.cropiTrig').attr('src',cropiClipboard_src);
        $(this).closest('.cropiTrigContainer').find('.picture_id').val(cropiClipboard_id);
        cropiClipboard_src = null;
        cropiClipboard_id = null;
        $(this).closest('.cropiTrigContainer').find('.cropiTrigEdit').show();
        $('.cropiTrigContainer .cropiPaste').hide();
        console.log(cropiClipboard_src,cropiClipboard_id);
        return false;
    });

//--------------------------------------------------
    $('.cropiTrigEdit').click(function(){
        $('#cropi').attr('data-id',$(this).closest('.cropiTrigContainer').attr('id'));
        var img_url = $(this).closest('.cropiTrigContainer').find('img.cropiTrig').attr('src');
        $('#cropi').show();

        var reader = new FileReader();
        reader.onload = function (e) {
            uploadCrop.bind({
                url: e.target.result
            }).then(function(){});
        }

        var xhr = new XMLHttpRequest();
        xhr.open('GET', img_url, true);
        xhr.responseType = 'blob';
        xhr.onload = function(e){
            if (this.status == 200) {
                var myBlob = this.response;
                reader.readAsDataURL(myBlob);
                // myBlob is now the blob that the object URL pointed to.
            }
        };
        xhr.send();

        return false;
    });


    $('.crop-btn-rotate').click(function () {
        deg = parseInt($(this).attr('data-deg'));
        console.log(deg);
        uploadCrop.rotate(deg);
    });

//--------------------------------------------------
//    uploadCrop = $('#cropi_image').croppie({
    var el = document.getElementById('cropi_image');
//    var el = $('#cropi_image');
    uploadCrop = new Croppie(el,{
        enableOrientation: true,
        enableExif: true,
        viewport: {
            width: view_width,
            height: view_height
        }
        ,boundary: {
            width: bound_width,
            height: bound_height
        }
        ,enforceBoundary: true

    });

});
