(function ($) {
    $(".form-group-item .btn-add-item").click(function () {
        var p = $(this).closest(".form-group-item").find(".g-items");

        let number = $(this).closest(".form-group-item").find(".g-items .item:last-child").data("number");
        if (number === undefined) number = 0;
        else number++;
        let extra_html = $(this).closest(".form-group-item").find(".g-more").html();
        extra_html = extra_html.replace(/__number__/gi, number);
        p.append(extra_html);

        // TODO fix it for select2
    })

    $(".form-group-item").each(function () {
        let container = $(this);
        $(this).on('click','.btn-remove-item',function () {
            $(this).closest(".item").remove();
        });
        $(this).on('press','input,select',function () {
            let value = $(this).val();
            $(this).attr("value",value);
        });
    });
})(jQuery);
