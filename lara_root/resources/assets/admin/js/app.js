require('./bootstrap');

require('select2/dist/js/select2');

window.bootbox = require('bootbox/bootbox');

require('ionicons/dist/ionicons')

require('./_base')

require('datatables.net/js/jquery.dataTables')

require('datatables.net-dt/js/dataTables.dataTables')

$(function (){
    $('[data-toggle="tooltip"]').tooltip();
})

require('domenu')

