function copyToClipboard(){
    var copyText = document.getElementById("result");

    copyText.select();
    copyText.setSelectionRange(0, 99999); /* For mobile devices */

    /* Copy the text inside the text field */
    document.execCommand("copy");
}

var result = new Object();
function populateResult(){
    $('.compo').each(function(){
        name = $(this).attr('name');
        val = $(this).val();
        required = $(this).prop('required');
        if(required || val.toString().trim().length>0) result[name] = $(this).val();
    });
}

function updateResultBox(){
    obj = '';
    $.each(result, function(k,v){
        req = $('[name='+k+']').prop('required') ;
        obj += '\t' + (req? '"': "'") +
            k.toString().toUpperCase() +
            (req? '"': "'") + ' => \'' +
            v.toString().trim() +
            '\',' + "\n";
    })

    var res = "@";
    res += "compo(\"";
    res += $('#compo_title').val();
    res += "\",[" + "\n";
    res += obj;
    res += '])';

    $('#result').val(res);
}

function makeResult(name, value){
    // console.log(name,value);
    result[name] = value;
    updateResultBox(obj);
}

$(function (){
    populateResult();
    updateResultBox();

    $('input:not([type=checkbox]),textarea').keyup(function(){
        makeResult($(this).attr('name'), $(this).val())
    })
    $('input[type=checkbox]').click(function(){
        makeResult($(this).attr('name'), $(this).prop('checked')?'1':'')
    })
})
