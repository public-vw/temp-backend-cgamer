const pluginsClass = require('../utils/plugins').default;

let plugins = new pluginsClass();

plugins.createTab({
  triggers: '.tab-box-option',
  elements: '.tab-box-item'
});
