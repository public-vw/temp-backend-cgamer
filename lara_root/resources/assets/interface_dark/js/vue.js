import Vue from 'vue/dist/vue.js'
import axios from "axios";

window.Vue = Vue;
window.axios = axios;
window.routes = {};

window.debugMode = false

require('../../interface/js/foreach')

window.addEventListener('load',function(){
    window.urlParams = new URL(location.href).pathname.split('/');
    document.querySelectorAll('script').forEach(function(el) {
        if(el.innerText.includes('removeAfterCreation')) {
            el.remove();
        }
    })
})

window.js_route = function(route, params = []){
    let route_url = window.routes[route]
    window.forEach(params,function(i, param){
        route_url = route_url.replace('#####',param)
    })
    return route_url
}
