//TODO:Remove jquery and make all these ajax requests by axios
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

new Vue({
    el: '#tools-charge-wallet',

    data: {
        loading: false,
        form: {
            date: null,
            amount: null,
            file: null,
            file_title: null,
        },
    },

    watch: {
        form: {
            deep: true,
            handler() {
                this.updateFrontend()
            },
        },
    },

    methods: {
        readImage(obj) {
            that = this

            var reader = new FileReader();
            reader.onload = function (e) {
                var image = new Image();
                image.src = e.target.result;
                image.onload = function () {
                    that.uploadImage(image)
                };
            }
            reader.readAsDataURL(obj.target.files[0]);
        },

        uploadImage(image){
            that = this
            that.loading = true
            $.ajax({
                type: 'POST',
                url: window.routes['upload_image_inside'],
                data: {
                    'img_data': image.src,
                    'size': {
                        'width': image.width,
                        'height': image.height,
                    },
                },
                success: function (response) {
                    that.form.file = image.src
                    that.form.file_id = image.response
                    that.loading = false
                },
                error: function (err) {
                    console.log(err)
                    that.loading = false
                }
            })
        },

        processForm() {
            console.log('OK! Sure! We are going to process the form!')
            document.querySelector('#trigReglogModal').click()

            axios.post(js_route('ajax_update_params'))
                .then(response => {
                    if (window.debugMode) console.log('added: ' + url)
                })
                .catch(error => {
                    console.log(error)
                })
        },

        updateFrontend() {
            amount = this.removeCommas(this.form.amount)
            this.form.amount = this.addCommas(amount)
        },

        addCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        },

        removeCommas(x) {
            return x ? parseInt(x.replace(/[,|\.]/g, '')) : 0;
        }
    }
})
