if(document.querySelector('#reglog-form') !== null) {
    new Vue({
        el: '#reglog-form',

        data: {
            auth: {
                username: '',
                password: '',
                remember: true,
                token: '',
            },
            rules: true,
            state: 10,
            minorState: 0,
            // 10,0: greeting
            // 20,0: logged in successfully
            // 40,3: wrong pass

            // 50,1: Empty user, session killed
            // 50,2: Auth2 token mismatch
            // 50,5: user registration

            // 21,1: true auth, but pending
            // 21,2: true auth, but suspended

            // 10,1: get forget password request
            // 10,2: forget password is on the way
        },

        methods: {

            resetState() {
                this.auth.password = ''
                this.auth.token = ''
                this.state = 10
                this.minorState = 0
            },

            forgetPassword() {
                this.state = 10
                this.minorState = 1
            },

            resendCode() {
                this.state = 0;

                axios.post(js_route('ajax_auth2_resend'))
                    .then(response => {
                        this.item = response.data
                        if (window.debugMode) console.log('added: ' + url)
                        this.state = response.status.toString().substr(0, 2)
                        this.minorState = response.status.toString().substr(2, 1)
                    })
                    .catch(
                        error => {
                            console.log(error)
                            this.state = error.response.status.toString().substr(0, 2)
                            this.minorState = error.response.status.toString().substr(2, 1)
                        },
                    )
            },

            sendAuthData() {
                if (!this.rules) return alert('قوانین باید تایید شود')

                this.state = 0;

                axios.post(js_route('ajax_auth_check'), this.auth)
                    .then(response => {
                        this.item = response.data
                        if (window.debugMode) console.log('added: ' + url)
                        this.state = response.status.toString().substr(0, 2)
                        this.minorState = response.status.toString().substr(2, 1)
                    })
                    .catch(
                        error => {
                            console.log(error)
                            this.state = error.response.status.toString().substr(0, 2)
                            this.minorState = error.response.status.toString().substr(2, 1)
                        },
                    )
            },

            sendForgetPasswordData() {
                this.state = 0;

                axios.post(js_route('ajax_reset_password'), this.auth)
                    .then(response => {
                        this.item = response.data
                        if (window.debugMode) console.log('added: ' + url)
                        this.state = response.status.toString().substr(0, 2)
                        this.minorState = response.status.toString().substr(2, 1)
                    })
                    .catch(
                        error => {
                            console.log(error)
                            this.state = error.response.status.toString().substr(0, 2)
                            this.minorState = error.response.status.toString().substr(2, 1)
                        },
                    )
            },
        }
    })
}
