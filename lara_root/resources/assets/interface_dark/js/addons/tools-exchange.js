new Vue({
    el: '#tools-exchange',

    data: {
        form: {
            amount: '',
            address: '',
        },
        order: {
            weth: '--',
        },
        state: 10,
        minorState: 0,
        needDegree: false,

        calcs:{
            irt2usd: 33000,
            usd2weth: 4000,

            gasfee: null,
            ourcost: null,
            saving: null,
        },

        settings:{
            afterDot: 4,

            degreeTreshold: 0.035,
            ttl: 90,
            ttlShow: 30,

            updateRequestAfter: 30,
            countToStopLimit: 100,
        },

        timer: null,
        firstLoadTimer: null,
        counter: 0,

        updateRequestCounter: 0,

        isUpdating: false,

        stopUpdate: false,
        countToStop: 0,
    },

    beforeDestroy() {
        clearInterval(this.timer)
    },
    mounted: function () {
        this.firstLoadTimer = setInterval(() => {
            this.updateParams(true)
        }, 100)

        this.timer = setInterval(() => {
            this.countDown()
        }, 1000)
    },

    watch: {
        form: {
            deep: true,
            handler() {
                this.updateFrontend()
            },
        },

    },

    methods: {
        processForm(){
            console.log('OK! Sure! We are going to process the form!')
            document.querySelector('#trigReglogModal').click()
        },

        countDown(){
            if(this.isUpdating)return;

            if(this.counter <= 0){
                this.updateParams()
            }
            else{
                this.counter--
            }

        },

        updateParams(force = false){
            if(this.stopUpdate)return;

            if(!force){
                this.updateRequestCounter++
                if(this.updateRequestCounter < this.settings.updateRequestAfter ) return
                this.updateRequestCounter = 0
            }

            this.isUpdating = true;

            if(typeof js_route('ajax_update_params') == 'undefined'){
                return
            }
            if(typeof this.firstLoadTimer !== 'undefined') {
                clearInterval(this.firstLoadTimer)
            }

            axios.post(js_route('ajax_update_params'))
                .then(response => {
                    this.isUpdating = false

                    this.counter = this.settings.ttl
                    this.updateRequestCounter = this.settings.updateRequestAfter

                    this.calcs = response.data
                    this.updateFrontend()
                    if(window.debugMode)console.log('added: '+url)

                    this.countToStop++
                    this.stopUpdate = (this.countToStop >= this.settings.countToStopLimit)
                })
                .catch(error => {
                    this.isUpdating = false
                    console.log(error)
                    this.updateRequestCounter = this.settings.updateRequestAfter
                })
        },

        resetState() {
            this.form = {}
        },

        updateFrontend(){
            this.order.weth = '--'

            amount = this.removeCommas(this.form.amount)
            this.form.amount = this.addCommas(amount)

            weth = this.calculateWETH(amount)

            if (weth > 0) this.order.weth = weth;

            this.needDegree = (weth > this.settings.degreeTreshold);
        },

        calculateWETH(irt) {
            afterDotPow = Math.pow(10,this.settings.afterDot)
            return Math.round(parseInt(irt) / this.calcs.irt2usd / this.calcs.usd2weth * afterDotPow) / afterDotPow
        },

        addCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        },

        removeCommas(x) {
            return x ? parseInt(x.replace(/[,|\.]/g, '')) : 0;
        }
    }
})
