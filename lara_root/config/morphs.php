<?php

return [
    'hyperlinkable' => [
        'article',
        'article_category',
    ],

    'attachmentable' => [
        'Payment',
        'Article',
        'User',
        'SocialChannel',
        'Badge',
        'Fact',
    ],

    'commentable' => [
        'payment',
        'article',
        'review',
    ],

    'addressable' => [
        'agency',
        'property',
    ],

    'emailable' => [
        'agency',
        'user',
        'property',
    ],

    'phonable' => [
        'agency',
        'user',
        'property',
    ],

    'importantable' => [
        'article_category',
        'article',
        'payment',
    ],

    'readable' => [
        'agency',
        'property',
    ],

    'reviewable' => [
        'article',
    ],

    'seoable' => [
        'article_category',
        'article',
        'menu',
    ],

    'socialable' => [
        'article_category',
        'article',
        'author',
    ],

    'rankable' => [
        'article_category',
        'article',
        'author',
        'comment',
        'review',
    ],

    'requestable' => [
        'article_category',
        'article',
    ],
];
