<?php

return [
    'node_path' => env('PATHS_NODE', '/usr/bin/node'),
    'npm_path'  => env('PATHS_NPM', '/usr/bin/npm'),
    'include_path'  => env('PATHS_INCLUDE', ''),
    'user_data_dir'  => env('PATHS_DATA_DIR', '/tmp/session-1'),
    'user_agent'  => env('USER_AGENT', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.54 Safari/537.36 OPR/81.0.4196.31'),

];
