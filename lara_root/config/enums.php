<?php

## get config key
#  config_key('enums.users_status','active')
## get all keys
# config_keys_all('enums.users_status')
## get value of a key
# config('enums.users_status_default')

return [


    'users_status_default' => 0,
    'users_status' => [
        'tmp',      # incomplete registration
        'pending',  # registered, waiting for activation
        'active',   # active!
        'demo',     # demo account, no or time limited database infection
        'suspend',  # suspended!
        'storage',  # no login, just for storing information.
    ],

    'genders_default' => 2,
    'genders' => [
        'male',
        'female',
        'unknown',
    ],

    'comments_status_default' => 0,
    'comments_status' => [
        'pending',
        'suspended',
        'accepted',
    ],

    'random_colors' => [ // background_color => text_color
        ['#453939' , '#eeeeee', 'Masala'],
        ['#87837A' , '#eeeeee', 'Friar Gray'],
        ['#F0E5C9' , '#333333', 'Wheatfield'],
        ['#006490' , '#eeeeee', 'Bahama Blue'],
        ['#00A1D5' , '#eeeeee', 'Cerulean'],
        ['#FF207B' , '#eeeeee', 'Wild Strawberry'],
        ['#FE5552' , '#eeeeee', 'Persimmon'],
        ['#FDD526' , '#eeeeee', 'Candlelight'],
        ['#00844B' , '#eeeeee', 'Tropical Rain Forest'],
        ['#8700B9' , '#eeeeee', 'Purple'],
        ['#714BAE' , '#eeeeee', 'Studio'],
        ['#4AE326' , '#eeeeee', 'Lima'],
    ],

    'articles_status_default' => 1,
    'articles_status' => [
        'draft',
        'requested',
        'rejected',
        'ready',// supervisor set article ready to check seo agent
        'active',
        'paused',
        'updated',
        'checking',// locks edit by author
        'seo-ready',// seo agent sent ready, postman converts this state to active
    ],

    'hyperlinks_status_default' => 1,
    'hyperlinks_status' => [
        'requested',
        'active',
        'rejected',
        'suspend',
    ],

    'article_categories_status_default' => 1,
    'article_categories_status' => [
        'draft',
        'requested',
        'ready',
        'active',
    ],


    'authors_status_default' => 1,
    'authors_status' => [
        'draft',
        'requested',
        'active',
        'suspend',
    ],

    'server_configs_status_default' => 0,
    'server_configs_status' => [
        'unknown',
        'checking',
        'checked',
    ],

    'reviews_status_default' => 0,
    'reviews_status' => [
        'draft',
        'pending',
        'suspended',
        'accepted',
    ],

    'attachment_types' => [
        'Image',
        'Video',
        'Sound',
    ],

    'hyperlink_type' => [
        'anchor',
        'internal',     // model:id
        'ex-internal',  // internal link, but exact url, not db id
        'external',
    ],

    'menus_url_type' => [
        'route',
        'link',
    ],

    'seo_details_type_default' => 0,
    'seo_details_type' => [
        'google',
        'facebook',
        'twitter',
    ],

    'seo_positions_status_default' => 0,
    'seo_positions_status' => [
        'draft',
        'pending',
        'active',
    ],

    'requests_priorities_default' => 0,
    'requests_priorities' => [
        'low',
        'med',
        'high',
    ],

    'requests_status_default' => 0,
    'requests_status' => [
        'draft',
        'requested',
        'accepted',
        'rejected',
        'canceled',
        'done'
    ],

    'settings_mode_default' => 0,
    'settings_mode' => [
        'text',
        'html',
        'textarea',
        'checkbox',
        'image',
    ],

    'settings_direction_default' => 0,
    'settings_direction' => [
        'ltr',
        'rtl',
    ],

    'payments_status_default' => 0,
    'payments_status' => [
        'draft',
        'pending',
        'accepted',
        'rejected',
    ],

    'bot_connections' => [
        'telegram',
        'instagram',
        'facebook',
        'skype',
    ],
];
