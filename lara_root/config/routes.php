<?php

return [
    'auth' => [
        'web'  => 'auth',
        'api'  => 'auth/api/v1',
        'view' => 'auth',
    ],

    'sysadmin' => [
        'web'  => 'sys',
        'api'  => 'sys/api/v1',
        'view' => 'sysadmin',
    ],

    #copy this for <NewPanel>
    'admin'    => [
        'web'  => 'admin',
        'api'  => 'admin/api/v1',
        'view' => 'admin',
    ],

    'client' => [
        'web'  => '',
        'api'  => 'usr/api/v1',
        'view' => 'interface_dark.clients',
    ],

    'interface' => [
        'web' => '', # unchangeable
        'public_api'  => 'face/api/v1',
        'secure_api'  => 'sec/api/v1',
        'view' => 'interface_dark',
    ],

];
