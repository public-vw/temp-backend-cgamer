<?php

return [
    # current locale, uses for normal local
    'locale'          => 'fa',

    # backup locale, uses when no normal or selected local available
    'fallback_locale' => 'en',
];
