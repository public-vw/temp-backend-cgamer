<?php

return [
    'clear_old_temporaries' => [
        'active'      => env('ATTACHMENTS_CLEAR_OLD_TEMP', false),

        #TODO: has no functionality yet. now it is hard coded to 1 day
        'period_days' => env('ATTACHMENTS_CLEAR_RUN_PERIOD', 1),

        'period_run_at' => env('ATTACHMENTS_CLEAR_RUN_AT', '04:00'),
        'how_olds_days' => env('ATTACHMENTS_CLEAR_HOWOLD', 7),
    ],

    'gasfeerobot' => [
        'active' => env('GASFEEROBOT_ACTIVE', false),
    ],

    'auto_backup' => [
        'db'        => [
            'active' => env('AUTO_BACKUP_DB_ACTIVE', false),
            'period_run_at' => env('AUTO_BACKUP_DB_RUN_AT', '03:00'),
        ],
        'uploads'   => [
            'active' => env('AUTO_BACKUP_UPLOADS_ACTIVE', false),
            'period_run_at' => env('AUTO_BACKUP_UPLOADS_RUN_AT', '03:00'),
        ],
    ],
];
