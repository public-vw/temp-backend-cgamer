<?php

// uri => danger level [1 lowest, 5 highest]
return [
// wordpress scanners
    '/wp'                        => 3,
    '/wordpress'                 => 4,
    'wp-login'                   => 2,
    'wp-admin'                   => 4,
    'wp-includes'                => 4,
    'xmlrpc'                     => 5,
    'wlwmanifest'                => 5,
    'admin-ajax'                 => 5,

//    laravel scanners
    '.env'                       => 5,
    'eval-stdin.php'             => 5,
    '_ignition/execute-solution' => 5,
    '/console'                   => 4,

//    experimental scannings
    'fgt_lang'                   => 5,
    'actuator/health'            => 4,
    '_additional'                => 4,
    'server-status'              => 5,
    'docs/cplugError.html'       => 5,
    'nmaplowercheck'             => 5,
    'microsoft.exchange'         => 5,
    'exporttool'                 => 5,
    'owa/auth'                   => 5,
    'sitemap.xml'                => 3,
    'pools/default/buckets'      => 5,
    'pools'                      => 4,
    'hnap1'                      => 5,
    'mhfa'                       => 5,
    'main.php'                   => 5,
    'ubnw'                       => 5,
    'portal.mwsl'                => 5,
    'owa'                        => 5,
    'credentials.txt'            => 5,
    'public_html'                => 5,
    'config.json'                => 5,
    'Autodiscover.xml'           => 5,

    'contact'      => 4, // email finders, we are using contact-us
    'login'        => 4, // we are using auth/login
    'reportserver' => 5,
    'phpmyadmin'   => 5,


// backup test
    '.gz'          => 5,
    '.tar'         => 5,
    '.zip'         => 5,
    '.rar'         => 5,
    '.sql'         => 5,

//wrong extensions
    '.shtml'       => 5,
    '.aspx'        => 5,
    '.cgi'         => 5,
    '.dll'         => 5,
    '.jsp'         => 5,
    '.jsa'         => 5,
    '.settings'    => 5,
    '.tpl'         => 5,
    '.dat'         => 5,
    '.log'         => 5,
];
