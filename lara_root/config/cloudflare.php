<?php
return [
    'api_user' => env('CLOUDFLARE_API_USER', ''),
    'api_key'  => env('CLOUDFLARE_API_KEY', ''),
];
