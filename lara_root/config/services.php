<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],


    'twitter' => [
        'client_id' => env('OAUTH_TWITTER_CLIENT_ID',''),
        'client_secret' => env('OAUTH_TWITTER_CLIENT_SECRET',''),
        'redirect' => env('OAUTH_TWITTER_CALLBACK_URL',''),
    ],
    'facebook' => [
        'client_id' => env('OAUTH_FACEBOOK_CLIENT_ID'),
        'client_secret' => env('OAUTH_FACEBOOK_CLIENT_SECRET'),
        'redirect' => env('OAUTH_FACEBOOK_CALLBACK_URL'),
    ],
    'google' => [
        'client_id' => env('OAUTH_GOOGLE_CLIENT_ID'),
        'client_secret' => env('OAUTH_GOOGLE_CLIENT_SECRET'),
        'redirect' => env('OAUTH_GOOGLE_CALLBACK_URL'),
    ],
    'linkedin' => [
        'client_id' => env('OAUTH_LINKEDIN_CLIENT_ID'),
        'client_secret' => env('OAUTH_LINKEDIN_CLIENT_SECRET'),
        'redirect' => env('OAUTH_LINKEDIN_CALLBACK_URL'),
    ],
    'telegram' => [
        'bot' => env('OAUTH_TELEGRAM_BOT_NAME'),
        'client_id' => null,
        'client_secret' => env('OAUTH_TELEGRAM_TOKEN'),
        'redirect' => env('OAUTH_TELEGRAM_CALLBACK_URL'),
    ],
    'instagrambasic' => [
        'client_id' => env('OAUTH_INSTAGRAM_CLIENT_ID'),
        'client_secret' => env('OAUTH_INSTAGRAM_CLIENT_SECRET'),
        'redirect' => env('OAUTH_INSTAGRAM_CALLBACK_URL'),
        'de_auth' => env('OAUTH_INSTAGRAM_DEAUTHORIZE_URL'),
        'del_my_data' => env('OAUTH_INSTAGRAM_DELMYDATA_URL'),
    ],
    'apple' => [
        'client_id' => env('OAUTH_APPLE_CLIENT_ID'),
        'client_secret' => env('OAUTH_APPLE_CLIENT_SECRET'),
        'redirect' => env('OAUTH_APPLE_CALLBACK_URL'),
    ],
    'youtube' => [
        'client_id' => env('OAUTH_YOUTUBE_CLIENT_ID'),
        'client_secret' => env('OAUTH_YOUTUBE_CLIENT_SECRET'),
        'redirect' => env('OAUTH_YOUTUBE_CALLBACK_URL'),
    ],
    'github' => [
        'client_id' => env('OAUTH_GITHUB_CLIENT_ID'),
        'client_secret' => env('OAUTH_GITHUB_CLIENT_SECRET'),
        'redirect' => env('OAUTH_GITHUB_CALLBACK_URL'),
    ],
    'gitlab' => [
        'client_id' => env('OAUTH_GITLAB_CLIENT_ID'),
        'client_secret' => env('OAUTH_GITLAB_CLIENT_SECRET'),
        'redirect' => env('OAUTH_GITLAB_CALLBACK_URL'),
    ],
    'steam' => [
        'client_id' => null,
        'client_secret' => env('OAUTH_STEAM_CLIENT_SECRET'),
        'redirect' => env('OAUTH_STEAM_CALLBACK_URL')
    ],
    'discord' => [
        'client_id' => env('OAUTH_DISCORD_CLIENT_ID'),
        'client_secret' => env('OAUTH_DISCORD_CLIENT_SECRET'),
        'redirect' => env('OAUTH_DISCORD_CALLBACK_URL'),

        // optional
        'allow_gif_avatars' => (bool)env('OAUTH_DISCORD_ALLOW_AVATAR_GIF', false),
        'avatar_default_extension' => env('OAUTH_DISCORD_EXTENSION_DEFAULT', 'jpg'), // only pick from jpg, png, webp
    ],

    'twitch' => [
        'client_id' => env('OAUTH_TWITCH_CLIENT_ID'),
        'client_secret' => env('OAUTH_TWITCH_CLIENT_SECRET'),
        'redirect' => env('OAUTH_TWITCH_CALLBACK_URL')
    ],
];
