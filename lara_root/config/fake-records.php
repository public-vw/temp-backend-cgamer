<?php

return [
    'user'     => 10,
    'follow'   => 10,
    'province' => 10,
    'city'     => 10,
    'region'   => 5,
    'author'   => 6,
    'category' => 30,
    'article'  => 100,
    'payment'  => 100,
];
