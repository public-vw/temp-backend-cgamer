<?php
return [
    'google' => [
        'map'        => env('GOOGLE_MAP', null),
        'recaptcha'  => env('GOOGLE_RECAPTCHA', null),
        'webmaster'  => env('GOOGLE_WEBMASTER', null),
        'analytics'  => env('GOOGLE_ANALYTICS', null),
        'tagmanager' => env('GOOGLE_TAGMANAGER', null),
        'optimize'   => env('GOOGLE_OPTIMIZE', null),
    ],

    'bing' => [
        'webmaster' => env('BING_WEBMASTER', null),
    ],

    'ir' => [
        'enamad' => env('IR_ENAMAD', null),
        'samandehi' => env('IR_SAMANDEHI', null),
    ],
];
