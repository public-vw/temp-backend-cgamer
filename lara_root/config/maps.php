<?php

return [
    'latitude'  => '35.7738263',
    'longitude' => '51.3724251',
    'map_zoom'  => '10',
];
