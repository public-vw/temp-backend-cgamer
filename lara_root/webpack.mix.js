const mix = require('laravel-mix');

// Sysadmin
mix.js('resources/assets/sysadmin/js/app.js', 'public/assets/sysadmin/js')
    .js('resources/assets/sysadmin/js/editor_config.js', 'public/assets/sysadmin/js')
    .js('resources/assets/sysadmin/js/component_generator.js', 'public/assets/sysadmin/js')
    .sass('resources/assets/sysadmin/sass/app.scss', 'public/assets/sysadmin/css')
    .copy('resources/assets/sysadmin/img', 'public/assets/sysadmin/img')
    .postCss('resources/assets/sysadmin/css/app.css', 'public/assets/sysadmin/css');

// Admin
mix.js('resources/assets/admin/js/app.js', 'public/assets/admin/js')
    .js('resources/assets/admin/js/dynamic_form.js', 'public/assets/admin/js/dynamic_form.js')
    .js('resources/assets/admin/js/editor_config.js', 'public/assets/admin/js/editor_config.js')
    .sass('resources/assets/admin/sass/app.scss', 'public/assets/admin/css')
    .postCss('resources/assets/admin/css/app.css', 'public/assets/admin/css');

// Auth
mix
    .copy('resources/assets/auth/img', 'public/assets/auth/img');



// Interface Dark Theme
mix
    .copy('resources/assets/interface_dark/img', 'public/assets/interface_dark/img')
    .js('resources/assets/interface_dark/js/app.js', 'public/assets/interface_dark/js')
    // .copy('resources/assets/interface_dark/js', 'public/assets/interface_dark/js')
    .copy('resources/assets/interface_dark/js/vendor', 'public/assets/interface_dark/js/vendor')
    .sass('resources/assets/interface_dark/sass/main.scss', 'public/assets/interface_dark/css')
    .sass('resources/assets/interface_dark/sass/homepage.scss', 'public/assets/interface_dark/css')
    .sass('resources/assets/interface_dark/sass/vendors/simplebar-rtl.scss', 'public/assets/interface_dark/css')
    .sass('resources/assets/interface_dark/sass/vendors/tiny-slider-rtl.scss', 'public/assets/interface_dark/css');

// Interface Dark Customizations
mix
    // clients | profile
    .sass('resources/assets/interface_dark/sass/clients/profile.scss', 'public/assets/interface_dark/clients/css')
    .js('resources/assets/interface/js/clients/profile.js', 'public/assets/interface/clients/js')

    // clients | articles-single
    .sass('resources/assets/interface_dark/sass/clients/articles-single.scss', 'public/assets/interface_dark/clients/css')
    .js('resources/assets/interface/js/clients/articles-single.js', 'public/assets/interface/clients/js')

    // pages | breadcrumb
    .js('resources/assets/interface/js/pages/breadcrumb.js', 'public/assets/interface/pages/js')

    // pages | articles-single
    .sass('resources/assets/interface_dark/sass/pages/articles-single.scss', 'public/assets/interface_dark/pages/css')

    // pages | article-category
    .sass('resources/assets/interface_dark/sass/pages/article-category.scss', 'public/assets/interface_dark/pages/css')
    // .js('resources/assets/interface/js/pages/article-category.js', 'public/assets/interface/pages/js')

    // pages | terms
    .sass('resources/assets/interface_dark/sass/pages/terms.scss', 'public/assets/interface_dark/pages/css')

    // admins | article-category
    .sass('resources/assets/interface_dark/sass/admins/article-category.scss', 'public/assets/interface_dark/admins/css')
    .js('resources/assets/interface/js/admins/article-category.js', 'public/assets/interface/admins/js')

    .js('resources/assets/interface_dark/js/addons/tools-exchange.js', 'public/assets/interface_dark/js')
    .js('resources/assets/interface_dark/js/addons/tools-charge-wallet.js', 'public/assets/interface_dark/js')
    .js('resources/assets/interface_dark/js/addons/jalaly-date.js', 'public/assets/interface_dark/js');

// Interface | Shared files
mix.js('resources/assets/interface/js/app.js', 'public/assets/interface/js')
    .js('resources/assets/interface/js/vue.js', 'public/assets/interface/js')
    .js('resources/assets/interface/js/share/single-item', 'public/assets/interface/js')
    .js('resources/assets/interface/js/share/list-items', 'public/assets/interface/js')
    .copy('resources/assets/interface/images', 'public/assets/interface/images');


// // Interface Light
// mix.copy('resources/assets/interface_light/js', 'public/assets/interface_light/js')
//     // .sass('resources/assets/interface_light/sass/main.scss', 'public/assets/interface_light/css')
//     .copy('resources/assets/interface_light/img', 'public/assets/interface_light/img');

// Basic Interface
// mix.js('resources/assets/basic_interface/js/app.js', 'public/assets/basic_interface/js')
//     .sass('resources/assets/basic_interface/sass/app.scss', 'public/assets/basic_interface/css')
//     .postCss('resources/assets/basic_interface/css/app.css', 'public/assets/basic_interface/css')
//     .sourceMaps();

mix.copy('node_modules/tinymce', 'public/vendor/tinymce');
