@servers(['localhost'   => '127.0.0.1'])

@setup
    $command = isset($command) ? $command : "--version";
@endsetup


@task('setup')
    composer install --prefer-dist --no-ansi --no-interaction --no-progress --no-scripts
    php artisan key:generate
    php artisan storage:link
    sudo npm install & npm run dev
@endtask

@story('fresh_db')
    composer_dump
    cache_clear
    config_clear
    down_artisan
    fresh-migrate
    seed
    up_artisan
@endstory

@story('clears')
    composer_dump
    cache_clear
    config_clear
    view_clear
    route_clear
@endstory



@task('deploy')
    composer install --optimize-autoloader --no-dev
    php artisan config:cache
    php artisan route:cache
@endtask

@task('composer_dump')
    composer dump-autoload
@endtask

@task('cache_clear')
    php artisan cache:clear
@endtask

@task('config_clear')
    php artisan config:clear
@endtask

@task('view_clear')
    php artisan view:clear
@endtask

@task('route_clear')
    php artisan route:clear
@endtask





@task('migrate')
    php artisan migrate
@endtask

@task('fresh-migrate')
    php artisan migrate:fresh
@endtask

@task('seed')
    php artisan db:seed
@endtask

@task('buss-migrate')
    count=`ls -1 database/migrations/business/*.php 2>/dev/null | wc -l`
    if [ $count != 0 ]
    then
        php artisan migrate --path="database/migrations/business"
    else
        echo "Error:" "database/migrations/business" is an Empty Folder!
    fi
@endtask

@task('buss-seed')
    count=`ls -1 database/seeds/business/*.php 2>/dev/null | wc -l`
    if [ $count != 0 ]
    then
        php artisan db:seed --class="BusinessDatabaseSeeder"
    else
        echo "Error:" "database/seeds/business" is an Empty Folder!
    fi
@endtask



@task('down_artisan')
    php artisan down
@endtask

@task('up_artisan')
    php artisan optimize
    php artisan up
@endtask


@task('xdbg2_artisan')
    {{-- Usage: envoy run xdbg2_artisan --command="generate:vc Address -r -v -f" --}}
    echo "Running: php artisan {{ $command }} with XDebug V2 Enabled";
    php  -dxdebug.remote_enable=1 -dxdebug.remote_autostart=on -dxdebug.remote_mode=req -dxdebug.idekey="PHPSTORM" -dxdebug.remote_enable=1 -dxdebug.remote_port=9000 artisan {{ $command }}
@endtask

@task('xdbg3_artisan')
    {{-- Usage: envoy run xdbg3_artisan --command="generate:vc Address -r -v -f" --}}
    echo "Running: php artisan {{ $command }} with XDebug V3 Enabled";
    php  artisan {{ $command }}
@endtask


@task('install socket.io and run /node/server.js')
npm install express redis socket.io --save
in root of project should run: node server.js
for testing push : php artisan send:notif {action} {user} {message}
@endtask



@finished
    echo "Envoy deployment complete.\r\n";
@endfinished
