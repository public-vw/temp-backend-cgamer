<?php

namespace App\Models;

use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use HasFactory;
    use CacheQueryBuilder;

    public static array $dataTableFields = [
        'title'         => [],
        'country'       => [],
        'province'      => [],
        'phone_precode' => [],
        'timezone'      => [],
        'actions'       => ['dont-all'],
    ];

    #TODO check unique fields
    public static array $validation = [
        '*'             => 'bail|string',
        'title'         => 'required',
        'province_id'   => 'required|exists:provinces,id',
        'phone_precode' => 'nullable|max:10',
        'timezone'      => 'nullable|size:6|starts_with:+,-',
    ];

    protected $fillable = [
        'title',
        'province_id',
        'phone_precode',
        'timezone',
    ];

    # -----------------------------------

    public function province()
    {
        return $this->belongsTo(Province::class)->withDefault();
    }

    public function contactAddresses()
    {
        return $this->hasMany(ContactAddress::class);
    }

    public function contactPhones()
    {
        return $this->hasMany(ContactPhone::class);
    }

    public function regions()
    {
        return $this->hasMany(Region::class);
    }

    public static function dataTableQuery(string $status = null)
    {
        return (new static)->newQuery();

//        $query = new static;
//        if (!is_null($status)) $query = $query->where($query->getTable().'.status', $status);
//
//        $query = $query->leftJoin('agency_groups', 'agencies.group_id', '=', 'agency_groups.id')
//            ->select(['agencies.*', 'agency_groups.title as agency_group']);
//        return $query;
    }

}
