<?php

namespace App\Models;

use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserMeta extends Model
{
    use HasFactory;
    use CacheQueryBuilder;

    public static array $dataTableFields = [
//        ''          => [], //options: dont-search, dont-order, dont-print, dont-export, dont-all
        'user'    => [null, 'user.username'],
        'actions' => ['dont-all'],
    ];

    public static array $validation = [
        '*'       => 'bail|string',
        'user_id' => 'required',
    ];

    protected $fillable = [
        'user_id',
        'about_me',
        'birthdate',
    ];

    # -----------------------------------

    public function user(){
        return $this->belongsTo(User::class);
    }

    public static function dataTableQuery(string $status = null)
    {
        return (new static)->newQuery();

//        $query = new static;
//        if (!is_null($status)) $query = $query->where($query->getTable().'.status', $status);
//
//        $query = $query->leftJoin('agency_groups', 'agencies.group_id', '=', 'agency_groups.id')
//            ->select(['agencies.*', 'agency_groups.title as agency_group']);
//        return $query;
    }

}
