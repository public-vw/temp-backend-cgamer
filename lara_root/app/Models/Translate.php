<?php

namespace App\Models;

use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Model;

class Translate extends Model
{
    use CacheQueryBuilder;

    protected $fillable = [
        'language_id',
        'translatable',
        'field',
        'value',
    ];

    public static array $validation = [
    ];

//------------------------------------------------------------------------------

    function language(){
        return $this->belongsTo(Language::class);
    }

    public function translatable(){
        return $this->morphTo();
    }

    public static function dataTableQuery(string $status = null)
    {
        return (new static)->newQuery();

//        $query = new static;
//        if (!is_null($status)) $query = $query->where($query->getTable().'.status', $status);
//
//        $query = $query->leftJoin('agency_groups', 'agencies.group_id', '=', 'agency_groups.id')
//            ->select(['agencies.*', 'agency_groups.title as agency_group']);
//        return $query;
    }
}
