<?php

namespace App\Models;

use App\Models\Traits\Attachmentable;
use App\Models\Traits\HasStatus;
use App\Models\Traits\Hyperlinkable;
use App\Models\Traits\Seoable;
use App\Models\Traits\Siblings;
use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ArticleCategory extends Model
{
    use HasFactory;
    use CacheQueryBuilder;
    use HasStatus;
    use Attachmentable;
    use Seoable;
    use Siblings;
    use Hyperlinkable;

    public static array $dataTableFields = [
        'slug'           => [], //options: dont-search, dont-order, dont-print, dont-export, dont-all
        'title'          => [], //options: dont-search, dont-order, dont-print, dont-export, dont-all
        'parent'         => [null, 'article_categories.title'],
        'articles_count' => ['dont-search'],
        'children'       => [null, 'children.count'],
        'status'         => [],
        'actions'        => ['dont-all'],
    ];

    public static array $validation = [
        '*'           => 'bail',
        'slug'        => 'nullable',
        'title'       => 'required|string',
        'heading'     => 'nullable|string',
        'description' => 'nullable|string',
        'summary'     => 'nullable|string',
        'parent_id'   => 'nullable|exists:article_categories,id',
        'status'      => 'required|numeric',
    ];

    protected $fillable = [
        'slug',
        'parent_id',
        'title',
        'heading',
        'description',  // shows as text of category
        'summary',      // shows as body of sub-category
        'order',
        'status',
    ];

    # -----------------------------------
    public function getSummaryAttribute($value)
    {
        if($value) return $value;

        $content = $this->description;

        $re = '/<p((?=[^>]+)[\s\S])+?class="category-description">[\s\S]+?<\/p>/m';
        preg_match($re, $content, $summary);

        return $summary[0] ?? null;
    }


    public static function roots()
    {
        return self::whereNull('parent_id')->orderBy('order')->get();
    }

    public function articles()
    {
        return $this->hasMany(Article::class, 'category_id', 'id');
    }

    public function activeArticles()
    {
        return $this->articles()->where('status',config_key('enums.articles_status','active'))->get();
    }

    public function deepArticlesCount()
    {
        $count = 0;
        $this->countArticles($this, $count);
        return $count;
    }

    protected function countArticles(ArticleCategory $category, &$count)
    {
        $count += $category->articles()->count();
        if ($category->hasChild()) {
            foreach ($category->children as $childCat)
                $this->countArticles($childCat, $count);
        }
    }

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id', 'id');
    }

    public function activeChildren()
    {
        return $this->children()->where('status',config_key('enums.article_categories_status','active'));
    }

    public function getChildrenAttribute()
    {
        return $this->children()
            ->orderBy('order', 'Asc')
            ->orderBy('created_at', 'Asc')
            ->get();
    }

    public function getActiveChildrenAttribute()
    {
        return $this->activeChildren()
            ->orderBy('order', 'Asc')
            ->orderBy('created_at', 'Asc')
            ->get();
    }

    public function hasChild()
    {
        return count($this->children);
    }

    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id', 'id');
    }

    public function hasParent()
    {
        return ($this->parent_id) == TRUE;
    }

    public function categoryChainJoin(string $field = null, string $clue = ','): string
    {
        $items = $this->categoryChain($field);
        return implode($clue, $items);
    }

    public function categoryChain($fields = null)
    {
        $fn_get_values = function ($obj) use ($fields) {
            $ret = [];
            if (is_array($fields)) {
                foreach ($fields as $field) $ret[$field] = $obj[$field];
            } else {
                $ret = $obj[$fields];
            }
            return $ret;
        };

        $items = [];
        $curr = $this;

        do {
            array_push($items, $fields ? $fn_get_values($curr) : $curr);
            $curr = $curr->parent;
        } while ($curr);

        $items = array_reverse($items);

        return $items;
    }

    public function seoUrl()
    {
        $categories = $this->categoryChainJoin('slug', '/');
        return route('footer.article_categories.single.view', [$categories]) . '/';
    }

    public static function dataTableQuery(string $status = null)
    {
        return (new static)->newQuery();

//        $query = new static;
//        if (!is_null($status)) $query = $query->where($query->getTable().'.status', $status);
//
//        $query = $query->leftJoin('agency_groups', 'agencies.group_id', '=', 'agency_groups.id')
//            ->select(['agencies.*', 'agency_groups.title as agency_group']);
//        return $query;
    }
}
