<?php

namespace App\Models;

use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Review extends Model
{
    use HasFactory;
    use CacheQueryBuilder;
    use SoftDeletes;

    public static array $dataTableFields = [
        'author'     => [],
        'content'    => [],
        'service'    => [],
        'status'     => [],
        'updated_at' => [],
        'actions'    => ['dont-all'],
    ];
    public static array $validation = [
        '*'               => 'bail|string|required',
        'reviewable_id'   => '',
        'reviewable_type' => '',
        'author_id'       => 'exists:users,id',
        'status'          => '',
        'content'         => '',
    ];
    protected $fillable = [
        'reviewable',
        'author_id',
        'content',
        'status',
    ];

    # -----------------------------------
    #TODO complete model

    public function author()
    {
        return $this->belongsTo(User::class, 'author_id', 'id');
    }

    public function reviewable()
    {
        return $this->morphTo();
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public static function dataTableQuery(string $status = null)
    {
        return (new static)->newQuery();

//        $query = new static;
//        if (!is_null($status)) $query = $query->where($query->getTable().'.status', $status);
//
//        $query = $query->leftJoin('agency_groups', 'agencies.group_id', '=', 'agency_groups.id')
//            ->select(['agencies.*', 'agency_groups.title as agency_group']);
//        return $query;
    }
}
