<?php

namespace App\Models;

use App\Models\Traits\Attachmentable;
use App\Models\Traits\HasStatus;
use App\Models\Traits\Hyperlinkable;
use App\Models\Traits\Seoable;
use App\Models\Traits\Siblings;
use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;
    use CacheQueryBuilder;
    use HasStatus;
    use Attachmentable;
    use Seoable;
    use Siblings;
    use Hyperlinkable;

    public static array $dataTableFields = [
        'slug'           => [], //options: dont-search, dont-order, dont-print, dont-export, dont-all
        'heading'        => [],
        'master'         => [],
        'author'         => [null, 'authors.title'],
        'category'       => [null, 'categories.title'],
        'reading_time'   => [],
        'content_length' => [],
        'status'         => [],
        'actions'        => ['dont-all'],
    ];

    public static array $validation = [
        '*'            => 'bail|string',
        'heading'      => 'required',
        'slug'         => 'nullable|COMB_UNIQUE:articles,[slug,master_id]',
        'author_id'    => 'nullable|exists:authors,id',
        'category_id'  => 'nullable|exists:article_categories,id',
        'reading_time' => 'nullable|numeric',
        'content'      => 'nullable',
        'summary'      => 'nullable',
        'status'       => 'nullable',
    ];

    protected $fillable = [
        'slug',
        'master_id',
        'author_id',
        'category_id',
        'heading',
        'reading_time',
        'content',
        'summary',
        'status',
    ];

    public static $PATTERN_IMG = '/\[img ([\s\S]+?)]/m';
    public static $PATTERN_HREF = '/\[href ([\s\S]+?)]([\s\S]+?)\[\/href]/m';
    public static $PATTERN_DATA_KEY = '/\s*data\-key\=([\'\"])\w+\1/m';

    # -----------------------------------
    public function master(){
        return $this->belongsTo(static::class,'master_id','id');
    }

    public function revision(){
        return $this->hasOne(static::class,'master_id','id');
    }

    public function isRevision():bool{
        return (bool)$this->master_id;
    }

    public function hasRevision(){
        return (bool)$this->revision;
    }

    public function getSummaryAttribute($value){
        if($value) return $value;

        $content = $this->publicContent(true);

        $re = '/<p((?=[^>]+)[\s\S])+?class="post-open-paragraph">[\s\S]+?<\/p>/m';
        preg_match($re,$content,$summary);

        return $summary[0] ?? null;
    }

    public function author()
    {
        return $this->belongsTo(Author::class);
    }

    public function category()
    {
        return $this->belongsTo(ArticleCategory::class, 'category_id');
    }

    public function seoUrl()
    {
        if(!$this->category) return null;

        $categories = $this->category->categoryChainJoin('slug','/');
        return route('footer.articles.single.view',[$categories, $this->slug]);
    }

    public function hardDelete()
    {
        $attachments = Attachment::
        where('attachmentable_type', 'article')
            ->where('attachmentable_id', $this->id)
            ->get();

        $that = $this;

        foreach ($attachments as $attachment) {
            $records = Attachment::
            where(function ($table) use ($that) {
                $table
                    ->where('attachmentable_type', '!=', 'article')
                    ->orWhere(function ($table) use ($that) {
                        $table->where('attachmentable_type', 'article')
                            ->where('attachmentable_id', '!=', $that->id);
                    });
            })->where('md5', $attachment->md5)->count();

            if ($records == 0) {
                mylog('Article Deletion', 'info', "ATTACHMENT HARD DELETE | " .
                    "storage_disk: \t" . $attachment->storage_disk . "\t" .
                    "file_name: \t" . $attachment->file_name . "\t" .
                    "file_ext: \t" . $attachment->file_ext . "\t" .
                    "md5: \t" . $attachment->md5 . "\t" .
                    "mimetype: \t" . $attachment->mimetype
                );
                $attachment->hardDelete();
            } else {
                mylog('Article Deletion', 'info', "ATTACHMENT SOFT DELETE | " .
                    "storage_disk: \t" . $attachment->storage_disk . "\t" .
                    "file_name: \t" . $attachment->file_name . "\t" .
                    "file_ext: \t" . $attachment->file_ext . "\t" .
                    "md5: \t" . $attachment->md5 . "\t" .
                    "mimetype: \t" . $attachment->mimetype . "\t"
                );
                $attachment->delete();
            }
        }

        return parent::delete();
    }

    public static function dataTableQuery(string $status = null)
    {
        return (new static)->newQuery();

//        $query = new static;
//        if (!is_null($status)) $query = $query->where($query->getTable().'.status', $status);
//
//        $query = $query->leftJoin('agency_groups', 'agencies.group_id', '=', 'agency_groups.id')
//            ->select(['agencies.*', 'agency_groups.title as agency_group']);
//        return $query;
    }

    public function setReadingTimeAttribute($value)
    {
        $this->attributes['reading_time'] = $value ?? max(1, ceil(count_words($this->content) / 220));
    }

    protected array $images = [];
    public function images(){
        return implode('","',$this->images);
    }

    public function publicContent($noHref = false)
    {
        $content = $this->content;

        $content = $this->fetchImages($content);
        $content = $noHref? $this->removeHrefs($content):$this->fetchHrefs($content);
        $content = $this->removeDataKey($content);

        return $content;
    }

    public function editableContent()
    {
        $content = $this->content;

        $content = $this->fetchImages($content, 'edit');
        $content = $this->fetchHrefs($content, 'edit');

        return $content;
    }

//---------------------------

    protected function fetchImages($content, $mode = 'public')
    {
        $image = fn($src, $db_id, $alt) => "<figure class='post-open-image'>" .
            "<img"
            . " src='{$src}'"
            . ($db_id ? " data-db-id='{$db_id}'" : "")
            . ($alt ? " alt='{$alt}'" : "")
            . "/>" .
            "</figure>";

        $content = preg_replace_callback(static::$PATTERN_IMG, function ($matches) use ($image, $mode) {

            $return = '';

            $attach_id = $matches[1];
            try {
                if ($attach_id === 'placeholder') {
                    $return = $image(asset('assets/interface/images/690x320.png'), "placeholder", null);
                } else {
                    $attachment = Attachment::find($attach_id);
                    $this->images[] = $attachment->url;
                    $return = $image($attachment->url,
                        $mode == 'edit' ? encrypt($attachment->id) : null,
                        $mode == 'public' ? 'connect this to attachment seo details alt item' : null,
                    );
                }
            } catch (\Exception $e) {
            } finally {
                return $return;
            }

        }, $content);
        return $content;
    }

    protected function fetchHrefs($content, $mode = 'public')
    {
        $content = preg_replace_callback(static::$PATTERN_HREF, function ($matches) use ($mode) {

            $return = '';

            $hyperlink_id = $matches[1];
            $title = $matches[2];

            try {
                $link = Hyperlink::find($hyperlink_id);
                $return = $link->generateHrefElement($mode,$title);
            } catch (\Exception $e) {
                getFormedError($e,'fetch href issue');
            } finally {
                return $return;
            }

        }, $content);
        return $content;
    }

    protected function removeHrefs($content)
    {
        $content = preg_replace(static::$PATTERN_HREF, '$1',$content);
        return $content;
    }

    protected function removeDataKey($content)
    {
        return preg_replace(static::$PATTERN_DATA_KEY, '', $content);
    }
}
