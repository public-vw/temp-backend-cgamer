<?php

namespace App\Models;

use App\Models\Traits\DeepCacheQuery;
use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Setting extends Model
{
    use HasFactory;
    use CacheQueryBuilder;

//    use DeepCacheQuery;
    static $deepCacheTTL = 0;

    public static array $validation = [
        '*'           => 'bail|string',
        'title'       => 'required',
        'group_id'    => 'required|exists:setting_groups,id',
        'slug'        => 'required|unique:settings,slug',
        'order'       => 'required',
        'lang'        => 'nullable',
        'direction'   => 'required',
        'mode'        => 'required',
        'description' => 'nullable',
        'hidden'      => 'boolean',
    ];

    public static array $dataTableFields = [
        'title'   => [],
        'group'   => [],
        'slug'    => [],
        'value'   => [],
        'actions' => ['dont-all'],
    ];

    #TODO create set value form
    protected $fillable = [
        'title',
        'group_id',
        'slug',
        'value',
        'order',
        'lang',
        'direction',
        'mode',
        'description',
        'hidden',
    ];

    # -----------------------------------

    public function setAttribute($key, $value)
    {
        Cache::forget('setting_group__as_array');
        return parent::setAttribute($key, $value);
    }

    public static function isSame($params, $checkValue)
    {
        $ret = Self::get($params);
        return (
            isset($ret) && ((
            is_array($ret) ? in_array($checkValue, $ret) : ($ret == $checkValue)
            ))
        );
    }

    public static function get($param, $default = null)
    {
        $params = explode('.', $param);
        $group = SettingGroup::where('slug', $params[0])->first();

        return ($group) ? $group->getValue($params[1]) : $default;
    }

    public static function has($param)
    {
        return !empty(Self::get($param));
    }

    public static function asArray()
    {
        if (Cache::has('setting_group__as_array')) return Cache::get('setting_group__as_array');

        $groups = Setting::groupBy('group_slug')->get(['group_slug']);
        $ret = [];
        foreach ($groups as $group) {
            $settings = SettingGroup::getAll($group->group_slug);
            foreach ($settings as $setting) {
                $ret[$group->group_slug][$setting->slug] = $setting->value;
            }
        }
        Cache::forever('setting_group__as_array', $ret);
        return $ret;
    }

    public function group()
    {
        return $this->belongsTo(SettingGroup::class, 'group_id');
    }

    public static function dataTableQuery(string $status = null)
    {
        return (new static)->newQuery();

//        $query = new static;
//        if (!is_null($status)) $query = $query->where($query->getTable().'.status', $status);
//
//        $query = $query->leftJoin('agency_groups', 'agencies.group_id', '=', 'agency_groups.id')
//            ->select(['agencies.*', 'agency_groups.title as agency_group']);
//        return $query;
    }
}
