<?php

namespace App\Models;

use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UrlRedirect extends Model
{
    use HasFactory;
    use CacheQueryBuilder;

    public static array $dataTableFields = [
        'title'              => [], //options: dont-search, dont-order, dont-print, dont-export, dont-all
        'from'               => [],
        'to'                 => [],
        'actions'            => ['dont-all'],
    ];

    public static array $validation = [
        '*'     => 'bail',
        'title' => 'required|string',
        'from' => 'required|string',
        'to' => 'required|string',
        'state_code' => 'in:301,302',
        'start_with' => '',
        'external' => '',
        'active' => '',
    ];

    protected $fillable = [
        'title',
        'from',
        'to',
        'state_code',
        'start_with',
        'external',
        'active',
    ];

    # -----------------------------------

    public static function dataTableQuery(string $status = null)
    {
        return (new static)->newQuery();

//        $query = new static;
//        if (!is_null($status)) $query = $query->where($query->getTable().'.status', $status);
//
//        $query = $query->leftJoin('agency_groups', 'agencies.group_id', '=', 'agency_groups.id')
//            ->select(['agencies.*', 'agency_groups.title as agency_group']);
//        return $query;
    }

}
