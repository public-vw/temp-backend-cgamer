<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Panel extends Model
{
    use HasFactory;

    public static array $validation = [
        '*'      => 'bail|string',
        'title'  => 'required|max:50|unique:panels,title',
        'active' => 'boolean',
    ];

    public static array $dataTableFields = [
        'title'   => [],
        'actions' => ['dont-all'],
    ];

    protected $fillable = [
        'title',
        'active',
    ];

    # -----------------------------------

    public function menus()
    {
        return $this->hasManyThrough(Menu::class, MenuPlaceholder::class, 'panel_id', 'placeholder_id', 'id')
            ->whereNull('parent_id')
            ->orderBy('order');
    }

    public function menu_placeholders()
    {
        return $this->hasMany(MenuPlaceholder::class, 'panel_id');
    }

    public static function dataTableQuery(string $status = null)
    {
        return (new static)->newQuery();

//        $query = new static;
//        if (!is_null($status)) $query = $query->where($query->getTable().'.status', $status);
//
//        $query = $query->leftJoin('agency_groups', 'agencies.group_id', '=', 'agency_groups.id')
//            ->select(['agencies.*', 'agency_groups.title as agency_group']);
//        return $query;
    }
}
