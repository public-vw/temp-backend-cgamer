<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BotGasfeeAlarm extends Model
{
    use HasFactory;

    protected $fillable = [
        'bot_user_id',
        'threshold',
        'fired',
    ];

    public function botUser(){
        return $this->belongsTo(BotUser::class,'bot_user_id');
    }

    public static function setAlarm($bot_user_id,$threshold){
        self::create([
            'bot_user_id' => $bot_user_id,
            'threshold' => $threshold,
            'fired' => false,
        ]);

        return true;
    }

    public static function removeAlarm($bot_user_id){
        self::where([
            'bot_user_id' => $bot_user_id,
            'fired' => false,
        ])->delete();

        return true;
    }

    public static function findAlarms($gasfee){
        return self::where('fired',false)->where('threshold' ,'>', $gasfee)->get();
    }

    public static function hasAlarms(){
        return self::where('fired',false)->count();
    }


}
