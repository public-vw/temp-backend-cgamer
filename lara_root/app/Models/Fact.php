<?php

namespace App\Models;

use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Fact extends Model
{
    use HasFactory;
    use CacheQueryBuilder;

    public static array $dataTableFields = [
        'summary'     => [], //options: dont-search, dont-order, dont-print, dont-export, dont-all
        'probability' => [],
        'active'      => [],
        'actions'     => ['dont-all'],
    ];

    public static array $validation = [
        '*'           => 'bail',
        'sentence'    => 'required|string',
        'probability' => 'required|numeric|min:1|max:10',
        'active'      => 'required',
    ];

    protected $fillable = [
        'sentence',
        'probability',
        'active',
    ];

    protected $appends = ['summary'];

    # -----------------------------------

    public function getSummaryAttribute($value){
        return Str::limit($this->sentence,30);
    }

    public static function selectOne():Fact{
        return static::where('active',true)->inRandomOrder()->first();
    }

    public static function dataTableQuery(string $status = null)
    {
        return (new static)->newQuery();

//        $query = new static;
//        if (!is_null($status)) $query = $query->where($query->getTable().'.status', $status);
//
//        $query = $query->leftJoin('agency_groups', 'agencies.group_id', '=', 'agency_groups.id')
//            ->select(['agencies.*', 'agency_groups.title as agency_group']);
//        return $query;
    }

}
