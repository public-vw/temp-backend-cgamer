<?php

namespace App\Models;

use App\Models\Traits\Seoable;
use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Route;

class Menu extends Model
{
    use HasFactory;
    use CacheQueryBuilder;
    use Seoable;

    const VOID_URL = 'javascript:;';

    public static array $validation = [
        '*'              => 'bail|nullable|string',
        'placeholder_id' => 'required|exists:menu_placeholders,id',
        'parent_id'      => 'exists:menus,id',
        'slug'           => 'max:100|unique:menus,slug',
        'title'          => 'max:100',
        'url_type'       => '',
        'url'            => '',
        'order'          => 'numeric|min:0',
        'class'          => 'max:80',
        'style'          => 'max:120',
        'icon'           => 'max:30',
        'go_new_tab'     => 'boolean',
        'nofollow'       => 'boolean',
        'active'         => 'boolean',
        'panel'          => '',
    ];

    public static array $dataTableFields = [
        'title'       => [],
        'placeholder' => [],
        'parent'      => [],
        'order'       => [],
        'actions'     => ['dont-all'],
    ];
    protected $fillable = [
        'placeholder_id',
        'parent_id',

        'slug',
        'title',
        'url_type',
        'url',

        'order',

        'class',
        'style',
        'icon',

        'go_new_tab',
        'nofollow',
        'active',
    ];
    protected $appends = [
        'children',
        'active_children',
        'ids',
        'href',
    ];

    # -----------------------------------

    public function placeholder()
    {
        return $this->belongsTo(MenuPlaceholder::class, 'placeholder_id');
    }

    public static function getActiveItemsByRoute($routeName)
    {
        $ret = [];

        foreach (Self::where('url', 'like', '["route",%')->get() as $item) {
            if ($item->url[1] === $routeName) {
                $ret[] = $item->id;
                if ($item->hasParent()) $ret[] = $item->parent->id;
            }
        }

        return $ret;
    }

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id', 'id');
    }


    public function hasChild()
    {
        return count($this->children);
    }


    // TODO: this function runs multi times with same result, in sidebar.blade.php
    //  Do something about this, caching in each season maybe

    public function getHrefAttribute()
    {
        if (empty($this->url)) return self::VOID_URL;

        switch (config('enums.menus_url_type.' . $this->url_type)) {
            case 'route':
                return (Route::has($this->url)) ? route($this->url) : self::VOID_URL;
                break;
            case 'link':
                return (trim($this->url)) ? url($this->url) : self::VOID_URL;
        }
    }

    public function isCurrent()
    {
        if (empty($this->url)) return false;

        switch ($this->url_type) {
            case 'route':
                return (Route::has($this->url)) && (Route::getCurrentRoute() == $this->url);
                break;
            case 'link':
                return url()->current() == $this->href;
        }
    }

    public function hasParent()
    {
        return ($this->parent_id) == TRUE;
    }

    public function getChildrenAttribute()
    {
        return $this->children()
            ->orderBy('order', 'Asc')
            ->orderBy('created_at', 'Asc')
            ->get();
    }

    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id', 'id');
    }

    public function getActiveChildrenAttribute()
    {
        return $this->children()
            ->where('active', true)
            ->orderBy('order', 'Asc')
            ->orderBy('created_at', 'Desc')
            ->get();
    }

//------------------------------------------------------------------------------

    public function getIdsAttribute()
    {
        $ret = [$this->id];
        $pid = $this->parent_id;
        while ($pid > 0) {
            $tmp = Self::where('id', $pid)->first();
            if (empty($tmp)) break;
            $ret[] = $tmp->id;
            $pid = $tmp->parent_id;
        }
        return implode('/', array_reverse($ret));
    }

    public static function dataTableQuery(string $status = null)
    {
        return (new static)->newQuery();

//        $query = new static;
//        if (!is_null($status)) $query = $query->where($query->getTable().'.status', $status);
//
//        $query = $query->leftJoin('agency_groups', 'agencies.group_id', '=', 'agency_groups.id')
//            ->select(['agencies.*', 'agency_groups.title as agency_group']);
//        return $query;
    }
}
