<?php

namespace App\Models;

use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContactPhone extends Model
{
    use HasFactory;
    use CacheQueryBuilder;

    public static array $dataTableFields = [
        'title'          => [],
        'phonable_type'  => [],
        'phonable_title' => [],
        'number'         => [],
        'city'           => [],
        'region'         => [],
        'actions'        => ['dont-all'],
    ];

    public static array $validation = [
        '*'             => 'bail|string',
        'phonable_id'   => 'required',
        'phonable_type' => 'required',
        'title'         => 'nullable',
        'number'        => 'required|unique:contact_phones,number',
        'order'         => 'required',
        'description'   => 'nullable',
        'city_id'       => 'required|exists:cities,id',
        'region_id'     => 'required|exists:regions,id',
        'default'       => 'boolean',
        'active'        => 'boolean',
    ];

    protected $fillable = [
        'phonable',
        'title',
        'number',
        'order',
        'description',
        'city_id',
        'region_id',
        'default',
        'active',
        'phone_verified_at',
    ];

    # -----------------------------------

    public function phonable()
    {
        return $this->morphTo();
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public static function dataTableQuery(string $status = null)
    {
        return (new static)->newQuery();

//        $query = new static;
//        if (!is_null($status)) $query = $query->where($query->getTable().'.status', $status);
//
//        $query = $query->leftJoin('agency_groups', 'agencies.group_id', '=', 'agency_groups.id')
//            ->select(['agencies.*', 'agency_groups.title as agency_group']);
//        return $query;
    }
}
