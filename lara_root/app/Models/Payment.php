<?php

namespace App\Models;

use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;
    use CacheQueryBuilder;

    public static array $dataTableFields = [
        'title'   => [], //options: dont-search, dont-order, dont-print, dont-export, dont-all
        'amount'  => [],
        'user'    => [null, 'user.title'],
        'status'  => [],
        'actions' => 'dont-all',
    ];

    public static array $validation = [
        '*'      => 'bail|string',
        'title'  => 'required',
        'amount' => 'required|numeric',
    ];

    protected $fillable = [
        'user_id',
        'title',
        'date_on_paper',
        'amount',
        'currency_id',
        'status',
    ];

    # -----------------------------------

    public static function dataTableQuery(string $status = null)
    {
        return (new static)->newQuery();

//        $query = new static;
//        if (!is_null($status)) $query = $query->where($query->getTable().'.status', $status);
//
//        $query = $query->leftJoin('agency_groups', 'agencies.group_id', '=', 'agency_groups.id')
//            ->select(['agencies.*', 'agency_groups.title as agency_group']);
//        return $query;
    }

}
