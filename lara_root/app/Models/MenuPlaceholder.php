<?php

namespace App\Models;

use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MenuPlaceholder extends Model
{
    use HasFactory;
    use CacheQueryBuilder;

    # TODO: fix unique together (panel_id, title)
    public static array $validation = [
        '*'        => 'bail|string|required',
        'panel_id' => 'exists:panels,id',
        'title'    => 'max:100',
    ];
    public static array $dataTableFields = [
        'title'   => [],
        'panel'   => [],
        'actions' => ['dont-all'],
    ];
    public $excludePanels = ['sysadmin'];
    protected $fillable = [
        'panel_id',
        'title',
    ];

    # -----------------------------------

    public function menus()
    {
        return $this->hasMany(Menu::class, 'placeholder_id')
            ->whereNull('parent_id')
            ->orderBy('order');
    }

    public function panel()
    {
        return $this->belongsTo(Panel::class, 'panel_id');
    }

    public static function dataTableQuery(string $status = null)
    {
        return (new static)->newQuery();

//        $query = new static;
//        if (!is_null($status)) $query = $query->where($query->getTable().'.status', $status);
//
//        $query = $query->leftJoin('agency_groups', 'agencies.group_id', '=', 'agency_groups.id')
//            ->select(['agencies.*', 'agency_groups.title as agency_group']);
//        return $query;
    }
}
