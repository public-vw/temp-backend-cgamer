<?php

namespace App\Models;

use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Hyperlink extends Model
{
    use HasFactory;
    use CacheQueryBuilder;

    public static array $dataTableFields = [
        'title'   => [], //options: dont-search, dont-order, dont-print, dont-export, dont-all
        'user'    => [null, 'user.username'],
        'status'  => [],
        'actions' => ['dont-all'],
    ];

    public static array $validation = [
        '*'     => 'bail|string',
        'title' => 'required',
    ];

    protected $fillable = [
        'hyperlinkable_id',  // Where we added this hyperlink, e.g. you can find this hyperlink in the article with id 3
        'hyperlinkable_type',
        'user_id',
        'title',
        'url_type',
        'url',
        'properties',
        'status',
    ];

    protected $casts = [
        'properties' => 'array'
    ];
    # -----------------------------------

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function hyperlinkable()
    {
        return $this->morphTo();
    }

    public function generateHrefElement($mode, $title)
    {
        $url = $this->url;
        if($this->url_type == config_key('enums.hyperlink_type','anchor') ) $url = '#' . $url;
        if($this->url_type == config_key('enums.hyperlink_type','internal') ){
            if($mode == 'edit'){
                $url = encrypt($url);
            }
            else{
                list($model,$id) = explode(':',$url);
                $model = model2class($model);
                $model = new $model;
                $url = $model::find($id)->seoUrl();
            }
        }

        $attributes = [];
        if(!$this->properties['follow']){
            $attributes['rel'][] = 'nofollow';
        }
        if($this->properties['new_tab']){
            $attributes['target'][] = '_blank';
            $attributes['rel'][] = 'noopener';
        }

        foreach($attributes as $key => $val){
            $attributes[$key] = "$key='".implode(' ',$val)."'";
        }
        $attributes = implode(' ',$attributes);

        return "<a href='{$url}' {$attributes}>{$title}</a>";
    }

    public static function extractUrl(string $url)
    {
        $parsedUrl = parse_url($url);
        $urlKeys = array_keys($parsedUrl);

        if(count($urlKeys) == 1){
            //internal (model:id)
            if(isset($parsedUrl['path'])) return ['type' => 'internal', 'path' => decrypt($parsedUrl['path'])];
            //anchor
            if(isset($parsedUrl['fragment'])) return ['type' => 'anchor', 'path' => $parsedUrl['fragment']];
        }

        //ex-internal (internal link, but exact url, not db id)
        if(Str::startsWith($url,config('app.url'))){
            if(isset($parsedUrl['path'])) return ['type' => 'ex-internal', 'path' => addslashes($parsedUrl)];
        }

        //external
        return ['type' => 'external', 'path' => addslashes($url)];
    }

    public static function dataTableQuery(string $status = null)
    {
        return (new static)->newQuery();

//        $query = new static;
//        if (!is_null($status)) $query = $query->where($query->getTable().'.status', $status);
//
//        $query = $query->leftJoin('agency_groups', 'agencies.group_id', '=', 'agency_groups.id')
//            ->select(['agencies.*', 'agency_groups.title as agency_group']);
//        return $query;
    }

}
