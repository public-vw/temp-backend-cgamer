<?php

namespace App\Models;

use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    use HasFactory;
    use CacheQueryBuilder;

    public static array $validation = [
        '*'       => 'bail|string|required',
        'title'   => '',
        'city_id' => 'exists:cities,id',
    ];

    public static array $dataTableFields = [
        'title'    => [],
        'country'  => [],
        'province' => [],
        'city'     => [],
        'actions'  => ['dont-all'],
    ];

    protected $fillable = [
        'title',
        'city_id',
    ];

    # -----------------------------------

    public function city()
    {
        return $this->belongsTo(City::class)->withDefault();
    }

    public function contactAddresses()
    {
        return $this->hasMany(ContactAddress::class);
    }

    public function contactPhones()
    {
        return $this->hasMany(ContactPhone::class);
    }


    public static function dataTableQuery(string $status = null)
    {
        return (new static)->newQuery();

//        $query = new static;
//        if (!is_null($status)) $query = $query->where($query->getTable().'.status', $status);
//
//        $query = $query->leftJoin('agency_groups', 'agencies.group_id', '=', 'agency_groups.id')
//            ->select(['agencies.*', 'agency_groups.title as agency_group']);
//        return $query;
    }
}
