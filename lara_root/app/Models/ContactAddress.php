<?php

namespace App\Models;

use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContactAddress extends Model
{
    use HasFactory;
    use CacheQueryBuilder;

    public static array $dataTableFields = [
        'title'             => [],
        'addressable_type'  => [],
        'addressable_title' => [],
        'city'              => [],
        'region'            => [],
        'actions'           => ['dont-all'],
    ];

    public static array $validation = [
        '*'                => 'bail|string',
        'addressable_id'   => 'required',
        'addressable_type' => 'required',
        'title'            => 'nullable',
        'order'            => 'required',
        'geocode'          => 'required',
        'description'      => 'nullable',
        'city_id'          => 'required|exists:cities,id',
        'region_id'        => 'required|exists:regions,id',
        'latitude'         => 'nullable',
        'longitude'        => 'nullable',
        'map_zoom'         => 'nullable',
        'default'          => 'boolean',
        'active'           => 'boolean',
    ];

    protected $fillable = [
        'addressable',
        'title',
        'order',
        'geocode',
        'description',
        'city_id',
        'region_id',
        'latitude',
        'longitude',
        'map_zoom',
        'default',
        'active',
    ];

    # -----------------------------------

    public function addressable()
    {
        return $this->morphTo();
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public static function dataTableQuery(string $status = null)
    {
        return (new static)->newQuery();

//        $query = new static;
//        if (!is_null($status)) $query = $query->where($query->getTable().'.status', $status);
//
//        $query = $query->leftJoin('agency_groups', 'agencies.group_id', '=', 'agency_groups.id')
//            ->select(['agencies.*', 'agency_groups.title as agency_group']);
//        return $query;
    }
}
