<?php

namespace App\Models;

use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Structure extends Model
{
    use HasFactory;
    use CacheQueryBuilder;

    public static array $dataTableFields = [
        'title'   => [],
        'type'    => [],
        'parent'  => ['dont-search'],
        'count'   => ['dont-search'],
        'actions' => ['dont-all'],
    ];

    public static array $validation = [
        '*'         => 'bail',
        'title'     => 'required|string',
        'parent_id' => 'nullable|string',
        'type'      => 'required|string',
        'options'   => '',
        'panels'    => '',
    ];

    protected $fillable = [
        'parent_id',
        'title',
        'type',
    ];

    protected $appends = ['count'];

    # -----------------------------------

    public function parent()
    {
        return $this->belongsTo(Structure::class, 'parent_id');
    }

    public function getCountAttribute()
    {
        if ($this->type !== 'model') return null;
        $model = model2class($this->title);
        return (new $model)->count();
    }

    public function migrations()
    {
        $term = "'" . model2table($this->title) . "', function";
        $files = filesContains(database_path('migrations'), $term, ['.php']);
        return $files;
    }

    public function models()
    {
        $term = $this->title;
        $files = folderContains(app_path('Models'), $term, ['.php']);
        return $files;
    }

    public function factories()
    {
        $term = "{$this->title}::class";
        $files = filesContains(database_path(), $term, ['.php'], 'database', 1);
        return $files;
    }

    public function routes()
    {
        $term = "('" . model2table($this->title);
        $files = filesContains(base_path('routes'), $term, ['.php'], null, 2);
        return $files;
    }

    public function controllers()
    {
        $term = model2table($this->title);
        $files = filesContains(app_path('Http'), $term, ['.php'], 'Controllers', 1);
        return $files;
    }

    public function datatables()
    {
        $term = model2table($this->title);
        $files = filesContains(app_path('DataTables'), $term, ['.php'], 'Datatables', 1);
        return $files;
    }

    public function requests()
    {
        $term = $this->title . ';';
        $files = filesContains(app_path('Http/Requests'), $term, ['.php']);
        return $files;
    }

    public function transformers()
    {
        $term = $this->title . ';';
        $files = filesContains(app_path('Http/Transformers'), $term, ['.php']);
        return $files;
    }

    public function views()
    {
        $term = model2table($this->title);
        $files = filesContains(resource_path('views'), $term, ['.php'], 'views', 1);
        return $files;
    }

    public static function dataTableQuery()
    {
        return (new static)->newQuery();

//        $query = new static;
//
//        $query = $query
//            ->leftJoin('structures as structures2', 'structures.parent_id', '=', 'structures2.id')
//            ->select(['structures.*', 'structures2.title as structures2.parent']);
//        return $query;
    }
}
