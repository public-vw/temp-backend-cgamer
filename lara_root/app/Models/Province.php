<?php

namespace App\Models;

use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    use HasFactory;
    use CacheQueryBuilder;

    public static array $dataTableFields = [
        'title'   => [],
        'country' => [],
        'actions' => ['dont-all'],
    ];
    public static array $validation = [
        '*'             => 'bail|string',
        'title'         => 'required',
        'phone_precode' => 'nullable|max:10',
        'country_id'    => 'required|exists:countries,id',
    ];
    protected $fillable = [
        'title',
        'country_id',
        'phone_precode',
    ];

    # -----------------------------------

    public function country()
    {
        return $this->belongsTo(Country::class)->withDefault();
    }

    public function cities()
    {
        return $this->hasMany(City::class);
    }

    public static function dataTableQuery(string $status = null)
    {
        return (new static)->newQuery();

//        $query = new static;
//        if (!is_null($status)) $query = $query->where($query->getTable().'.status', $status);
//
//        $query = $query->leftJoin('agency_groups', 'agencies.group_id', '=', 'agency_groups.id')
//            ->select(['agencies.*', 'agency_groups.title as agency_group']);
//        return $query;
    }
}
