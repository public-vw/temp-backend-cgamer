<?php

namespace App\Models;

use App\Models\Traits\Attachmentable;
use App\Models\Traits\Seoable;
use App\Models\Traits\Siblings;
use App\Support\Database\CacheQueryBuilder;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use phpDocumentor\Reflection\Types\Boolean;

class Attachment extends Model
{
    use HasFactory;
    use CacheQueryBuilder;
    use Attachmentable;
    use Seoable;
    use Siblings;

    public static array $dataTableFields = [
        'title'        => [],
        'file_ext'     => [],
        'file_size'    => [],
        'type'         => [],
        'quality_rank' => [],
        'actions'      => ['dont-all'],
    ];

    public static array $validation = [
        '*'            => 'bail',
        'file'         => 'file',
        'properties'   => 'nullable',
        'storage_disk' => 'nullable',
        'type_id'      => '',
    ];

    protected $fillable = [
        'attachmentable_type',
        'attachmentable_id',
        'user_id',
        'storage_disk',
        'file_name',
        'file_ext',
        'file_size',
        'properties',
        'md5',
        'quality_rank',
        'type_id',
        'mimetype',
        'original_name',
        'temporary',
    ];

    protected $appends = ['fullname', 'url', 'path'];

    protected $casts = [
        'quality_rank' => 'int',
        'properties'   => 'array',
    ];

    # -----------------------------------

    public function attachmentable()
    {
        return $this->morphTo();
    }

    public function type()
    {
        #TODO: test if we can remove the id part or not
        return $this->belongsTo(AttachmentType::class, 'type_id', 'id')->withDefault();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getFullnameAttribute()
    {
        return $this->file_name . '.' . $this->file_ext;
    }

    public function makePermanent(string $destination_storage = 'permanent'): bool
    {
        if (!$this->temporary) return true;
        if ($this->storage_disk === $destination_storage) {
            $this->temporary = false;
            $this->save();
            return true;
        }

        if (
            Storage::disk($destination_storage)->writeStream(
                $this->fullname,
                Storage::disk($this->storage_disk)->readStream($this->fullname)
            )
        ) {
            Storage::disk($this->storage_disk)->delete($this->fullname);

            $this->temporary = false;
            $this->storage_disk = $destination_storage;
            $this->save();

            return true;
        }
        return false;
    }

    public static function clearOldTemporaries($daysBefore = 7)
    {
        $temporaryRecords = self::where('temporary', true)->where('updated_at', '<', Carbon::now()->addDays(-1 * $daysBefore))->get();

        if ($temporaryRecords) {
            foreach ($temporaryRecords as $temporaryRecord) {
                $temporaryRecord->hardDelete();
            }
        }

        return $temporaryRecords->plunk('fullname')->toArray();

    }

    public function hardDelete()
    {
        $records = Attachment::where('md5', $this->md5)->where('id', '!=', $this->id)->count();

        if ($records == 0) {
            mylog('Attachment Deletion', 'info', "ATTACHMENT HARD DELETE | " .
                "storage_disk: \t" . $this->storage_disk . "\t" .
                "file_name: \t" . $this->file_name . "\t" .
                "file_ext: \t" . $this->file_ext . "\t" .
                "md5: \t" . $this->md5 . "\t" .
                "mimetype: \t" . $this->mimetype
            );
            Storage::disk($this->storage_disk)->delete($this->fullname);
        } else {
            mylog('Attachment Deletion', 'info', "ATTACHMENT SOFT DELETE | " .
                "storage_disk: \t" . $this->storage_disk . "\t" .
                "file_name: \t" . $this->file_name . "\t" .
                "file_ext: \t" . $this->file_ext . "\t" .
                "md5: \t" . $this->md5 . "\t" .
                "mimetype: \t" . $this->mimetype . "\t"
            );
        }
        return parent::delete();
    }

    public function getUrlAttribute()
    {
        return Storage::disk($this->storage_disk)->url($this->fullname);
    }

    public function getPathAttribute()
    {
        return Storage::disk($this->storage_disk)->path($this->fullname);
    }

    public function setMd5Attribute($value)
    {
        $this->attributes['md5'] = (!is_null($value)) ? $value : md5_file($this->path);
    }

    public function setOriginalNameAttribute($value)
    {
        #TODO fix $value = null
        $this->attributes['original_name'] = base64_encode($value);
    }

    public static function dataTableQuery(string $status = null)
    {
        return (new static)->newQuery();

//        $query = new static;
//        if (!is_null($status)) $query = $query->where($query->getTable().'.status', $status);
//
//        $query = $query->leftJoin('agency_groups', 'agencies.group_id', '=', 'agency_groups.id')
//            ->select(['agencies.*', 'agency_groups.title as agency_group']);
//        return $query;
    }

    //Creates new record
    public static function add(AttachmentType $fileType, string $fileName, string $fileExt, array $fileSize = null, string $originalName = NULL, bool $temporary = true)
    {
        $filePath = $fileName . '.' . $fileExt;

        $disk = $temporary ? 'temporary' : 'permanent';

        $storage = Storage::disk($disk);

        $attachment = Attachment::create([
            'attachmentable_type' => null,
            'attachmentable_id'   => null,
            'user_id'             => auth()->user()->id ?? null,
            'storage_disk'        => $disk,
            'file_name'           => $fileName,
            'file_ext'            => $fileExt,
            'file_size'           => $storage->size($filePath),
            'properties'          => $fileSize,
            'md5'                 => null,
            'quality_rank'        => null,
            'type_id'             => $fileType->id,
            'mimetype'            => $storage->mimeType($filePath),
            'original_name'       => $originalName ?? '-- RAW DATA --',
            'temporary'           => $temporary,
        ]);

        return $attachment;
    }

    //Updates record with new file data
    public function updateAs(AttachmentType $fileType, string $fileName, string $fileExt, array $fileSize = null, string $originalName = NULL, bool $temporary = true)
    {
        $filePath = $fileName . '.' . $fileExt;

        $disk = $temporary ? 'temporary' : 'permanent';

        $storage = Storage::disk($disk);

        $this->update([
            'user_id'       => auth()->user()->id ?? null,
            'storage_disk'  => $disk,
            'file_name'     => $fileName,
            'file_ext'      => $fileExt,
            'file_size'     => $storage->size($filePath),
            'properties'    => $fileSize,
            'md5'           => null,
            'quality_rank'  => null,
            'type_id'       => $fileType->id,
            'mimetype'      => $storage->mimeType($filePath),
            'original_name' => $originalName ?? '-- RAW DATA --',
            'temporary'     => $temporary,
        ]);

        return $this;
    }

    //Creates new record with copy another record file data
    public static function copyAs(AttachmentType $fileType, Attachment $srcAttachment, bool $temporary = true)
    {
        $attachment = Attachment::create([
            'attachmentable_type' => null,
            'attachmentable_id'   => null,
            'user_id'             => auth()->user()->id ?? null,
            'storage_disk'        => 'permanent',
            'file_name'           => $srcAttachment->file_name,
            'file_ext'            => $srcAttachment->file_ext,
            'file_size'           => $srcAttachment->file_size,
            'properties'          => $srcAttachment->properties,
            'md5'                 => $srcAttachment->md5,
            'quality_rank'        => $srcAttachment->quality_rank,
            'type_id'             => $fileType->id,
            'mimetype'            => $srcAttachment->mimetype,
            'original_name'       => $srcAttachment->original_name ?? '-- RAW DATA --',
            'temporary'           => $temporary,
        ]);

        return $attachment;
    }

    //Updates exist record with copy another record file data
    public function fillAs(AttachmentType $fileType, Attachment $srcAttachment, bool $temporary = true)
    {
        $this->update([
            'user_id'       => auth()->user()->id ?? null,
            'file_name'     => $srcAttachment->file_name,
            'file_ext'      => $srcAttachment->file_ext,
            'file_size'     => $srcAttachment->file_size,
            'properties'    => $srcAttachment->properties,
            'md5'           => $srcAttachment->md5,
            'quality_rank'  => $srcAttachment->quality_rank,
            'type_id'       => $fileType->id,
            'mimetype'      => $srcAttachment->mimetype,
            'original_name' => $srcAttachment->original_name ?? '-- RAW DATA --',
            'temporary'     => $temporary,
        ]);

        return $this;
    }


    public static function uploadFileByURL($url, $typeSlug, $attachmentable, $default_ext)
    {
        $raw_file = file_get_contents($url);
        $attachmentType = AttachmentType::where('slug', $typeSlug)->firstOrFail(['folder', 'id']);

        $fileext = null;
        preg_match('/image\/(\w+)/', $fileext, $fileext);
        $fileext = isset($fileext[1]) ? $fileext[1] : pathinfo($url, PATHINFO_EXTENSION);
        $fileext = empty($fileext) ? $default_ext : $fileext;
        $md5 = md5($raw_file);
        $oldAttachment = Attachment::where('storage_disk', 'permanent')->where('md5', $md5)->first();

        if ($oldAttachment) {
            $attachment = Attachment::copyAs($attachmentType, $oldAttachment, false);
        } else {
            $filename = $attachmentType->folder . '/' . Carbon::now()->format('Y-m/d-His-') . rand(101, 999);
            $filesize = null;//w&h #TODO: should update for images
            $filepath = $filename . '.' . $fileext;

            $stored = Storage::disk('permanent')->put($filepath, $raw_file);
            if ($stored) {
                $attachment = Attachment::add($attachmentType, $filename, $fileext, $filesize, null, false);
            }
        }

        $attachment->attachmentable_id = $attachmentable->id;
        $attachment->attachmentable_type = array_search(get_class($attachmentable), Relation::$morphMap) ?: null;
        $attachment->save();

        return $attachment;
    }


}
