<?php

namespace App\Models;

use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServerConfig extends Model
{
    use HasFactory;
    use CacheQueryBuilder;

    public static array $dataTableFields = [
        'key'        => [],
        'value'      => [],
        'status'     => [],
        'updated_at' => [],
        'actions'    => ['dont-all'],
    ];

    protected $fillable = [
        'key',
        'value',
        'status',
    ];

    public static function getValue(string $key)
    {
        $config = ServerConfig::where('key', $key)->first();
        return $config->value ?? false;
    }

    public static function getStatus(string $key)
    {
        $config = ServerConfig::where('key', $key)->first();
        return $config ? config("enums.server_configs_status." . $config->status) : false;
    }

    public static function setValue(string $key, string $value)
    {
        $config = ServerConfig::where('key', $key)->first();
        $config->value = $value;
        $config->status = (string)config_key('enums.server_configs_status', 'checked');
        $config->save();
    }

    public static function clearValue(string $key)
    {
        $config = ServerConfig::where('key', $key)->first();
        $config->value = null;
        $config->status = (string)config_key('enums.server_configs_status', 'checking');
        $config->save();
    }

    public static function isNull(string $key)
    {
        return is_null(ServerConfig::getValue('key'));
    }

    public static function dataTableQuery(string $status = null)
    {
        return (new static)->newQuery();

//        $query = new static;
//        if (!is_null($status)) $query = $query->where($query->getTable().'.status', $status);
//
//        $query = $query->leftJoin('agency_groups', 'agencies.group_id', '=', 'agency_groups.id')
//            ->select(['agencies.*', 'agency_groups.title as agency_group']);
//        return $query;
    }
}
