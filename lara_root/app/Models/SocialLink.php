<?php

namespace App\Models;

use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SocialLink extends Model
{
    use HasFactory;
    use CacheQueryBuilder;

    public static array $dataTableFields = [
        'title'            => [],
        'socialable_type'  => [],
        'socialable_title' => [],
        'channel'          => [],
        'actions'          => ['dont-all'],
    ];

    public static array $validation = [
        '*'                 => 'bail|string',
        'socialable_id'     => 'required',
        'socialable_type'   => 'required',
        'title'             => 'nullable',
        'channel_id'        => 'required|exists:social_channels,id',
        'connection_string' => 'required|unique:social_links,connection_string',
        'order'             => 'required',
        'description'       => 'nullable',
        'default'           => 'boolean',
        'active'            => 'boolean',
    ];

    protected $fillable = [
        'socialable',
        'title',
        'channel_id',
        'connection_string',
        'order',
        'description',
        'default',
        'active',
        'social_verified_at',
    ];

    # -----------------------------------

    public function socialable()
    {
        return $this->morphTo();
    }

    public function socialChannel()
    {
        #TODO: test if we can remove the id part or not
        return $this->belongsTo(SocialChannel::class, 'channel_id', 'id');
    }


    public static function dataTableQuery(string $status = null)
    {
        return (new static)->newQuery();

//        $query = new static;
//        if (!is_null($status)) $query = $query->where($query->getTable().'.status', $status);
//
//        $query = $query->leftJoin('agency_groups', 'agencies.group_id', '=', 'agency_groups.id')
//            ->select(['agencies.*', 'agency_groups.title as agency_group']);
//        return $query;
    }
}
