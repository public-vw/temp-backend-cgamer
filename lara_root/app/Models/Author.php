<?php

namespace App\Models;

use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    use HasFactory;
    use CacheQueryBuilder;

    public static $dataTableFields = [
        'user'          => [null, 'user'],
        'article_limit' => [],
        'actions'       => ['dont-all'],
    ];

    public static $validation = [
        '*'             => 'bail',
        'user_id'       => 'required|exists:users,id',
        'article_limit' => 'required|numeric',
        'status'        => '',
    ];

    protected $fillable = [
        'user_id',
        'article_limit',
        'status',
    ];

    # -----------------------------------

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function articles()
    {
        return $this->hasMany(Article::class);
    }

    public function activeArticles()
    {
        return $this->articles()->where('status', config_key('enums.articles_status', 'active'))->get();
    }

    public function latestActiveArticles()
    {
        return $this->articles()->where('status', config_key('enums.articles_status', 'active'))->orderBy('updated_at','desc')->take(12)->get();
    }

    public function inctiveArticles()
    {
        return $this->articles()
            ->where('status', '!=',config_key('enums.articles_status', 'active'))
            ->where('status', '!=',config_key('enums.articles_status', 'updated'))
            ->orderBy('updated_at','desc')->get();
    }

    public static function dataTableQuery(string $status = null)
    {
        return (new static)->newQuery();

//        $query = new static;
//        if (!is_null($status)) $query = $query->where($query->getTable().'.status', $status);
//
//        $query = $query->leftJoin('agency_groups', 'agencies.group_id', '=', 'agency_groups.id')
//            ->select(['agencies.*', 'agency_groups.title as agency_group']);
//        return $query;
    }
}
