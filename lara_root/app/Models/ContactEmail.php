<?php

namespace App\Models;

use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContactEmail extends Model
{
    use HasFactory;
    use CacheQueryBuilder;

    public static array $dataTableFields = [
        'title'           => [],
        'emailable_type'  => [],
        'emailable_title' => [],
        'email'           => [],
        'actions'         => ['dont-all'],
    ];

    public static array $validation = [
        '*'              => 'bail|string',
        'emailable_id'   => 'required',
        'emailable_type' => 'required',
        'title'          => 'nullable',
        'email'          => 'required|email|unique:contact_emails,email',
        'order'          => 'required',
        'description'    => 'nullable',
        'default'        => 'boolean',
        'active'         => 'boolean',
    ];

    protected $fillable = [
        'emailable',
        'title',
        'email',
        'order',
        'description',
        'default',
        'active',
        'email_verified_at',
    ];

    # -----------------------------------

    public function emailable()
    {
        return $this->morphTo();
    }

    public static function dataTableQuery(string $status = null)
    {
        return (new static)->newQuery();

//        $query = new static;
//        if (!is_null($status)) $query = $query->where($query->getTable().'.status', $status);
//
//        $query = $query->leftJoin('agency_groups', 'agencies.group_id', '=', 'agency_groups.id')
//            ->select(['agencies.*', 'agency_groups.title as agency_group']);
//        return $query;
    }

}
