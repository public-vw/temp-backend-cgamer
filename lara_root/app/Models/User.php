<?php

namespace App\Models;

use App\Models\Traits\Attachmentable;
use App\Models\Traits\Socialable;
use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasFactory, Notifiable;
    use HasApiTokens;
    use Socialable;
    use Attachmentable;
    use SoftDeletes;
    use HasRoles;
    use CacheQueryBuilder;

    public static array $dataTableFields = [
        'name'       => [],
        'parent'     => [],
        'nickname'   => [],
        'username'   => [],
        'role'       => [],
        'ref_code'   => [],
        'referer_id' => [],
        'actions'    => ['dont-all'],
    ];

    public static array $validation = [
        '*'                   => 'bail',
        'name'                => 'required|string',
        'parent_id'           => 'nullable||exists:users,id',
        'nickname'            => 'nullable|regex:/^([a-z\d\_\-]*)$/|unique:users,nickname',
        'username'            => 'required|regex:/^([a-z\d\_\-]*)$/|unique:users,username',
        'password'            => 'nullable|string|min:8|confirmed',
        'ref_code'            => 'nullable|regex:/^([a-zA-Z\d\_]*)$/|unique:users,ref_code',
        'mobile'              => 'nullable|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|unique:users,mobile',
        'email'               => 'nullable|email|unique:users,email',
        'random_color'        => 'sometimes',
        'telegram_uid'        => 'nullable|string|unique:users,telegram_uid',
        'referer_id'          => 'nullable|max:20|exists:users,id',
        'completion'          => 'required|numeric|min:0|max:100',
        'google_secret_key'   => 'nullable',
        'use_two_factor_auth' => 'nullable',
        'status'              => 'required',
    ];

    protected $fillable = [
        'parent_id',

        'name',

        'username',
        'nickname',
        'password',
        'ref_code',

        'mobile',
        'email',

        'random_color',
        'avatar_id',

        'telegram_uid',
        'referer_id',

        'completion',

        'google_secret_key',
        'use_two_factor_auth',

        'status',
    ];
    protected $hidden = [
        'password',
        'remember_token',
    ];
    protected $casts = [
        'email_verified_at'  => 'datetime',
        'mobile_verified_at' => 'datetime',
    ];

    # -----------------------------------

    public function allowedPanels(){
        foreach($this->webRoles() as $role){
            $panels[] = Role::findByName($role,'web')->panel->title ?? 'sysadmin';
        }
        $panels = array_unique($panels);
        return $panels;
    }

    public function isChildOfCurrent(){
        if(auth()->guest())return false;

        $user = auth()->user();
        return $this->isChildOf($user);
    }

    public function isSameAsCurrent(){
        if(auth()->guest())return false;

        $user = auth()->user();
        return ($this->id == $user->id);
    }

    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id', 'id');
    }

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id', 'id');
    }

    public function hasParent()
    {
        return ($this->parent_id == TRUE);
    }

    public function hasChild()
    {
        return count($this->children);
    }

    public function isChildOf(User $user): bool
    {
        return (bool)($this->parent && ($user->id == $this->parent->id));
    }

    public function isParentOf(User $user): bool
    {
        return (bool)($user->parent && ($this->id == $user->parent->id));
    }


    public function followers()
    {
        return $this->belongsToMany(User::class, Follow::class, 'master_id', 'slave_id', 'id', 'id');
    }

    public function followings()
    {
        return $this->belongsToMany(User::class, Follow::class, 'slave_id', 'master_id', 'id', 'id');
    }

    public function isFollowing(User $user): bool
    {
        return (bool)in_array($user->id, $this->followers->pluck('id')->toArray());
    }

    public function isFollowerOf(User $user): bool
    {
        return (bool)in_array($user->id, $this->followings->pluck('id')->toArray());
    }

    public function badges()
    {
        return $this->belongsToMany(Badge::class);
    }

    public function meta()
    {
        return $this->hasOne(UserMeta::class);
    }

    public function oauth()
    {
        return $this->hasOne(UserOauth::class);
    }

    public function author()
    {
        return $this->hasOne(Author::class);
    }

    public static function isTelegramMember($user_telegram_id)
    {
        return User::where('telegram_uid', $user_telegram_id)->first();
    }

    public function logs()
    {
        return $this->hasMany(Log::class);
    }

    public function starRates()
    {
        return $this->hasMany(StarRate::class);
    }

    public function importants()
    {
        return $this->hasMany(Important::class);
    }

    public function readFlags()
    {
        return $this->hasMany(ReadFlag::class);
    }

    public function commentedReviews()
    {
        return $this->hasMany(Review::class, 'author_id', 'id');
    }

    public function reviews()
    {
        return $this->morphMany(Review::class, 'reviewable');
    }

    #TODO it's for interface morph selects check if correct or not
//    public function getTitleAttribute()
//    {
//        return $this->username;
//    }

    public function attachments()
    {
        return $this->morphMany(Attachment::class, 'attachmentable');
    }

    public static function hasAnyRoles(...$roles)
    {
        return User::with("roles")->whereHas("roles", function ($q) use ($roles) {
            $q->whereIn("name", $roles);
        });
    }

    #TODO remove it
    public function avatar()
    {
        #TODO: test if we can remove the id part or not
        #TODO: check relation
        return $this->belongsTo(Attachment::class, 'avatar_id')->withDefault();
    }

    public static function dataTableQuery(string $status = null)
    {
        return (new static)->newQuery();

//        $query = new static;
//        if (!is_null($status)) $query = $query->where($query->getTable().'.status', $status);
//
//        $query = $query
//            ->leftJoin('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
//            ->where('model_has_roles.model_type','user')
//            ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
//            ->select(['users.*', 'roles.name as role']);
//        return $query;
    }

    public function webRoles(): array
    {
        $roles = $this->roles->pluck('name')->toArray();
        $roles = array_filter($roles, function ($v) {
            return strrpos($v, '@web') !== FALSE;
        });
        return $roles;
    }
}
