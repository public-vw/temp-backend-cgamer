<?php

namespace App\Models;

use App\Models\Traits\Translatable;
use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use HasFactory;
    use CacheQueryBuilder;
    use Translatable;

    public static array $dataTableFields = [
        'title'         => [],
        'phone_precode' => [],
        'timezone'      => [],
        'actions'       => ['dont-all'],
    ];
    public static array $validation = [
        '*'             => 'bail|string',
        'title.*'       => 'required',
        'phone_precode' => 'nullable|max:10|unique:countries,phone_precode',
        'flag_iso'      => 'nullable|max:5|unique:countries,flag_iso',
        'timezone'      => 'nullable|size:6|starts_with:+,-',
    ];
    protected $fillable = [
        'title',
        'phone_precode',
        'flag_iso',
        'timezone',
    ];

    protected $translatable = [
        'title',
    ];

    protected $casts = [
        'title' => 'array',
    ];

    # -----------------------------------

    public function provinces()
    {
        return $this->hasMany(Province::class);
    }

    public static function dataTableQuery(string $status = null)
    {
        return (new static)->newQuery();

//        $query = new static;
//        if (!is_null($status)) $query = $query->where($query->getTable().'.status', $status);
//
//        $query = $query->leftJoin('agency_groups', 'agencies.group_id', '=', 'agency_groups.id')
//            ->select(['agencies.*', 'agency_groups.title as agency_group']);
//        return $query;
    }
}
