<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    use HasFactory;

    public static array $validation = [
        '*'                => 'bail|string',
        'user_id'          => 'required|exists:users,id',
        'subject'          => 'required',
        'description'      => 'nullable',
        'requestable_type' => 'required',
        'requestable_id'   => 'required',
        'field'            => 'required|max:100',
        'priority'         => 'required',
        'status'           => 'required',
    ];

    public static array $dataTableFields = [
        'subject'           => [],
        'username'          => [],
        'field'             => [],
        'requestable_type'  => [],
        'requestable_title' => [],
        'priority'          => [],
        'status'            => [],
        'actions'           => ['dont-all'],
    ];

    protected $fillable = [
        'user_id',
        'subject',
        'description',
        'requestable',
        'field',
        'priority',
        'status',
    ];

    # -----------------------------------

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function requestable()
    {
        return $this->morphTo();
    }

    public function readFlag()
    {
        return $this->morphMany(ReadFlag::class, 'readable');
    }

    public static function dataTableQuery(string $status = null)
    {
        return (new static)->newQuery();

//        $query = new static;
//        if (!is_null($status)) $query = $query->where($query->getTable().'.status', $status);
//
//        $query = $query->leftJoin('agency_groups', 'agencies.group_id', '=', 'agency_groups.id')
//            ->select(['agencies.*', 'agency_groups.title as agency_group']);
//        return $query;
    }
}
