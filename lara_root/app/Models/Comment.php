<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    use HasFactory;
    use SoftDeletes;

    public static array $dataTableFields = [
        'content'           => [],
        'commentable_type'  => [],
        'commentable_title' => [],
        'status'            => [],
        'actions'           => ['dont-all'],
    ];

    public static array $validation = [
        '*'                => 'bail|string',
        'commentable_id'   => 'required',
        'commentable_type' => 'required',
        'content'          => 'required',
        'parent_id'        => 'nullable|exists:comments,id',
        'status'           => 'required',
    ];

    protected $fillable = [
        'commentable',
        'content',
        'parent_id',
        'status',
    ];

    # -----------------------------------

    public function commentable()
    {
        return $this->morphTo();
    }

    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id');
    }

    public static function dataTableQuery(string $status = null)
    {
        return (new static)->newQuery();

//        $query = new static;
//        if (!is_null($status)) $query = $query->where($query->getTable().'.status', $status);
//
//        $query = $query->leftJoin('agency_groups', 'agencies.group_id', '=', 'agency_groups.id')
//            ->select(['agencies.*', 'agency_groups.title as agency_group']);
//        return $query;
    }
}
