<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ArticleLabel extends Model
{
    use HasFactory;

    protected $fillable = [
        'group_id',
        'title',
        'order',
        'color_theme',
    ];

    public static array $validation = [
        '*'      => 'bail|string',
        'title'  => 'required',
    ];

    public function group(){
        return $this->belongsTo(ArticleLabelGroup::class);
    }
}
