<?php

namespace App\Models;

use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReadFlag extends Model
{
    use HasFactory;
    use CacheQueryBuilder;

    public static array $dataTableFields = [
        'title'   => [],
        'actions' => ['dont-all'],
    ];
    public static array $validation = [
        '*'     => 'bail|string',
        'title' => 'required|unique:countries,title',
    ];
    protected $fillable = [
        'readable',
        'user_id',
        'read_flag',
    ];

    # -----------------------------------

    public function readable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function dataTableQuery(string $status = null)
    {
        return (new static)->newQuery();

//        $query = new static;
//        if (!is_null($status)) $query = $query->where($query->getTable().'.status', $status);
//
//        $query = $query->leftJoin('agency_groups', 'agencies.group_id', '=', 'agency_groups.id')
//            ->select(['agencies.*', 'agency_groups.title as agency_group']);
//        return $query;
    }
}
