<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModelAttachmentType extends Model
{
    use HasFactory;

    public static array $dataTableFields = [
        'attachment_type' => [],
        'model_name'      => [],
        'actions'         => ['dont-all'],
    ];

    public static array $validation = [
        '*'                  => 'bail|string|required',
        'attachment_type_id' => 'exists:attachment_types,id',
        'model_name'         => '',
    ];

    protected $fillable = [
        'attachment_type_id',
        'model_name',
    ];

    # -----------------------------------

    public function attachmentType()
    {
        return $this->belongsTo(AttachmentType::class, 'attachment_type_id', 'id');
    }

    public static function dataTableQuery(string $status = null)
    {
        return (new static)->newQuery();

//        $query = new static;
//        if (!is_null($status)) $query = $query->where($query->getTable().'.status', $status);
//
//        $query = $query->leftJoin('agency_groups', 'agencies.group_id', '=', 'agency_groups.id')
//            ->select(['agencies.*', 'agency_groups.title as agency_group']);
//        return $query;
    }
}
