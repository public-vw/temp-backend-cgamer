<?php

namespace App\Models;

use App\Support\Database\CacheQueryBuilder;
use Database\Seeders\LanguagesSeeder;
use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    use CacheQueryBuilder;

    protected $fillable = [
        'title',
        'code',
        'flag_iso',
        'direction',
        'order',
        'active',
    ];

    public static array $validation = [
    ];

    public static function getList(){
        return Language::all();
    }

    public static function getCurrent(){
        return self::where('code',app()->getLocale())->first();
    }

    public static function codes():array{
        $currentLocale = app()->getLocale();
        $fallbackLocale = config('multilingual.fallback_locale');

        return [
            'currentLocale' => self::where('code',$currentLocale)->first(['code'])['code'],
            'fallbackLocale' => self::where('code',$fallbackLocale)->first(['code'])['code'],
        ];
    }

    public static function dataTableQuery(string $status = null)
    {
        return (new static)->newQuery();

//        $query = new static;
//        if (!is_null($status)) $query = $query->where($query->getTable().'.status', $status);
//
//        $query = $query->leftJoin('agency_groups', 'agencies.group_id', '=', 'agency_groups.id')
//            ->select(['agencies.*', 'agency_groups.title as agency_group']);
//        return $query;
    }
}
