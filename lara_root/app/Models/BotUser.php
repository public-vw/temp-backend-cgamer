<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BotUser extends Model
{
    use HasFactory;

    protected $fillable = [
        'robot_id',
        'user_id',

        'user_telegram_uid',
        'telegram_mobile',
        'telegram_username',
        'telegram_firstname',
        'telegram_lastname',
        'telegram_lang_code',
    ];

    public function robot(){
        return $this->belongsTo(BotConnection::class,'robot_id');
    }

    public function userByTelegramID(){
        return $this->belongsTo(User::class,'user_telegram_uid','telegram_uid');
    }

    public function userByID(){
        return $this->belongsTo(User::class);
    }

    public function user(){
        return $this->userByID ?? $this->userByTelegramID;
    }

    public function hasAlarm(){
        $alarm = BotGasfeeAlarm::where('bot_user_id', $this->id)->where('fired',false)->first();
        return $alarm->threshold ?? false;
    }

    public function syncWithUsers(){
        $count = 0;

        $botusers = self::whereNull('user_id')->get(['id','user_telegram_uid']);
        foreach($botusers as $botuser){
            $u = User::where('telegram_uid',$botuser->user_telegram_uid)->first();
            if($u) {
                $count++;
                $botuser->user_id = $u->id;
                $botuser->save();
            }
        }

        return $count;
    }
}
