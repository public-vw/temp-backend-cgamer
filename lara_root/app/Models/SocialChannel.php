<?php

namespace App\Models;

use App\Models\Traits\Attachmentable;
use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SocialChannel extends Model
{
    use HasFactory;
    use CacheQueryBuilder;
    use Attachmentable;

    public static array $dataTableFields = [
        'title'   => [],
        'url'     => [],
        'actions' => ['dont-all'],
    ];

    public static array $validation = [
        '*'      => 'bail',
        'title'  => 'required|string|max:120',
        'url'    => 'required|string|max:100|unique:social_channels,url',
        'file'   => 'file',
        'active' => 'boolean',
    ];

    protected $fillable = [
        'title',
        'url',
        'active',
    ];

    # -----------------------------------

    public function socialLinks()
    {
        #TODO: test if we can remove the id part or not
        return $this->hasMany(SocialLink::class, 'channel_id', 'id');
    }

    public function thumbnail()
    {
        $type = AttachmentType::where('slug', 'social_channel_thumbnail')->first(['id']);
        return $this->attachments()->where('type_id', $type->id)->get()->last();
    }

    public static function dataTableQuery(string $status = null)
    {
        return (new static)->newQuery();

//        $query = new static;
//        if (!is_null($status)) $query = $query->where($query->getTable().'.status', $status);
//
//        $query = $query->leftJoin('agency_groups', 'agencies.group_id', '=', 'agency_groups.id')
//            ->select(['agencies.*', 'agency_groups.title as agency_group']);
//        return $query;
    }
}
