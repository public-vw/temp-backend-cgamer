<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Domain extends Model
{
    use HasFactory;

    public static array $validation = [
        '*'              => 'bail|string',
        'title'          => 'required|max:100',
        'domain_address' => 'required|max:150',
        'active'         => 'boolean',
    ];

    public static array $dataTableFields = [
        'title'          => [],
        'domain_address' => [],
        'actions'        => ['dont-all'],
    ];

    protected $fillable = [
        'title',
        'domain_address',
        'active',
    ];

    # -----------------------------------

    public function siteBrands()
    {
        return $this->hasMany(SiteBrand::class, 'domain_id');
    }

    public static function dataTableQuery(string $status = null)
    {
        return (new static)->newQuery();

//        $query = new static;
//        if (!is_null($status)) $query = $query->where($query->getTable().'.status', $status);
//
//        $query = $query->leftJoin('agency_groups', 'agencies.group_id', '=', 'agency_groups.id')
//            ->select(['agencies.*', 'agency_groups.title as agency_group']);
//        return $query;
    }
}
