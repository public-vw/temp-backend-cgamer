<?php

namespace App\Models\Traits;

trait HasStatus
{
    public static function getStatusList(){
        $statuses = static::select('status')
            ->orderBy('status')
            ->get()
            ->countBy('status')
            ->toArray();
        $statuses['all'] = static::select('id')->count();

        return $statuses;
    }
}
