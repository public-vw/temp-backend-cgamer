<?php

namespace App\Models\Traits;

use Illuminate\Support\Facades\DB;

Trait Siblings{
    public function getPreviousIdAttribute()
    {
        return static::where('id', '<', $this->id)->max('id');
    }

    public function getNextIdAttribute()
    {
        return static::where('id', '>', $this->id)->min('id');
    }
}
