<?php


namespace App\Models\Traits;


use Illuminate\Support\Facades\Cache;

trait DeepCacheQuery
{
    public function getAttribute($key)
    {
        $cacheKey = $this->makeCacheKey($key);
        if (Cache::has($cacheKey)) return Cache::get($cacheKey);

        $attrib = parent::getAttribute($key);

        $ttl = self::$deepCacheTTL ?? config('cache.deep_cache_ttl');
        if($ttl > 0){
            Cache::put($cacheKey, $attrib, $ttl);
        }
        else{
            Cache::forever($cacheKey, $attrib);
        }

        return $attrib;
    }

    protected function makeCacheKey($key)
    {
        $cache_key = strtolower(get_class($this));
        $cache_key .= "." . $this->id;
        $cache_key .= "." . strtolower($key);

        return $cache_key;
    }

    public function setAttribute($key, $value)
    {
        Cache::forget($this->makeCacheKey($key));
        return parent::setAttribute($key, $value);
    }
}
