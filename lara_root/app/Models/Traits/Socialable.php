<?php


namespace App\Models\Traits;


use App\Models\ContactAddress;
use App\Models\ContactEmail;
use App\Models\ContactPhone;
use App\Models\SocialLink;

trait Socialable
{
    public function ContactAddresses()
    {
        return $this->morphMany(ContactAddress::class, 'addressable');
    }

    public function contactPhones()
    {
        return $this->morphMany(ContactPhone::class, 'phonable');
    }

    public function contactEmails()
    {
        return $this->morphMany(ContactEmail::class, 'emailable');
    }

    public function socialLinks()
    {
        return $this->morphMany(SocialLink::class, 'socialable');
    }
}
