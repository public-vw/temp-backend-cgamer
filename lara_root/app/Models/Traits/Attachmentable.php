<?php

namespace App\Models\Traits;

use App\Models\Attachment;
use App\Models\AttachmentType;

trait Attachmentable
{
    public function attachments()
    {
        return $this->morphMany(Attachment::class, 'attachmentable');
    }

    public function imagesCollection($toArray = false, $typeSlug = null)
    {
        return $this->getAttachments('Image',$typeSlug, $toArray);
    }

    public function videosCollection($toArray = false, $typeSlug = null)
    {
        return $this->getAttachments('Video',$typeSlug, $toArray);
    }

    public function soundsCollection($toArray = false, $typeSlug = null)
    {
        return $this->getAttachments('Sound',$typeSlug, $toArray);
    }

    public function lastImage($slug){
        return $this->lastUrl('Image',$slug);
    }

    public function lastVideo($slug){
        return $this->lastUrl('Video',$slug);
    }

    public function lastSound($slug){
        return $this->lastUrl('Sound',$slug);
    }

    protected function lastUrl($type,$slug){
        $attachments = $this->getAttachments($type,$slug);
        return $attachments ? last($attachments)['url']: null;
    }

    protected function getAttachments($type,$slug = null,$toArray = true){
        $atype = AttachmentType::where('type',config_key('enums.attachment_types',$type));
        if($slug) $atype = $atype->where('slug', $slug);
        $atype = $atype->first(['id']);
        $attachments = $this->attachments()->select(['id','storage_disk','file_name','file_ext'])
                            ->where('type_id', $atype->id)->get();
        if($toArray) $attachments = $attachments->keyBy('id')->toArray();

        return $attachments;
    }
}
