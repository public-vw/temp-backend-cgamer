<?php

namespace App\Models\Traits;

use App\Models\SeoDetail;

trait Seoable
{
    public function seoDetails()
    {
        return $this->morphMany(SeoDetail::class, 'seoable');
    }

    public function seo()
    {
        $seo = [];
        foreach(config('enums.seo_details_type') as $type){
            $seo[$type] = $this->seoDetails()
                                ->where('type',config_key('enums.seo_details_type',$type))
                                ->orderByDesc('id')
                                ->first();
        }
        return $seo;
    }
}
