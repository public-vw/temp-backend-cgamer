<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;

class UserOauth extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function hasAny(){
        $fields = Schema::getColumnListing('user_oauths');
        foreach($fields as $field){
            if(strpos($field,'_id') === false || $field == 'user_id')continue;
            if(!is_null($this[$field])) return true;
        }
        return false;
    }
}
