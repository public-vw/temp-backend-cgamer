<?php

namespace App\Models;

use App\Models\Traits\Attachmentable;
use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SeoDetail extends Model
{
    use HasFactory;
    use CacheQueryBuilder;
    use Attachmentable;

    public static array $dataTableFields = [
        'title'         => [],
        'type'          => [],
        'keywords'      => [],
        'seoable_type'  => [],
        'seoable_title' => [],
        'actions'       => ['dont-all'],
    ];

    # TODO: fix upload file
    public static array $validation = [
        '*'            => 'bail|string',
        'type'         => 'required',
        'title'        => 'nullable',
        'seoable_id'   => 'required',
        'seoable_type' => 'required',
        'keywords'     => 'nullable',
        'description'  => 'nullable',
//        'file'          => 'sometimes|file',
    ];

    protected $fillable = [
        'seoable',
        'type',
        'title',
        'keywords',
        'description',
    ];

    # -----------------------------------

    public function seoable()
    {
        return $this->morphTo();
    }

    public static function dataTableQuery(string $status = null)
    {
        return (new static)->newQuery();

//        $query = new static;
//        if (!is_null($status)) $query = $query->where($query->getTable().'.status', $status);
//
//        $query = $query->leftJoin('agency_groups', 'agencies.group_id', '=', 'agency_groups.id')
//            ->select(['agencies.*', 'agency_groups.title as agency_group']);
//        return $query;
    }
}
