<?php

namespace App\Models;

use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AttachmentType extends Model
{
    use HasFactory;
    use CacheQueryBuilder;

    public static array $dataTableFields = [
        'title'     => [],
        'type'      => [],
        'actions'   => ['dont-all'],
    ];

    # TODO: fix unique together (type, slug)
    public static array $validation = [
        '*'                 => 'bail|string',
        'title'             => 'required|max:120|unique:attachment_types,title',
        'slug'              => 'required|max:125|unique:attachment_types,slug',
        'properties_limit'  => 'nullable',
        'folder'            => 'required',
        'type'              => 'nullable',
    ];

    protected $fillable = [
        'title',
        'slug',
        'properties_limit',
        'folder',
        'type',
    ];

    # -----------------------------------

    public function attachments()
    {
        #TODO: test if we can remove the id part or not
        return $this->hasMany(Attachment::class, 'type_id', 'id');
    }

    public function modelAttachmentTypes()
    {
        return $this->hasMany(ModelAttachmentType::class, 'attachment_type_id', 'id');
    }

    public static function dataTableQuery(string $status = null)
    {
        return (new static)->newQuery();

//        $query = new static;
//        if (!is_null($status)) $query = $query->where($query->getTable().'.status', $status);
//
//        $query = $query->leftJoin('agency_groups', 'agencies.group_id', '=', 'agency_groups.id')
//            ->select(['agencies.*', 'agency_groups.title as agency_group']);
//        return $query;
    }
}
