<?php

namespace App\Models;

use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use \Illuminate\Support\Facades\Log;

class BotConnection extends Model
{
    use HasFactory;
    use CacheQueryBuilder;

    public static array $dataTableFields = [
        'title'   => [],
        'type'    => [],
        'active'  => [],
        'actions' => ['dont-all'],
    ];

    public static array $validation = [
        '*'             => 'bail|string',
        'title'         => 'required',
        'username'      => 'required',
        'type'          => 'required',
        'robot_token'   => 'nullable',
        'parameters'    => 'nullable',
        'webhook_token' => 'nullable',
        'active'        => 'required',
    ];

    protected $fillable = [
        'title',
        'username',
        'type',
        'robot_token',
        'parameters',
        'webhook_token',
        'active',
    ];

    protected $appends = ['params'];

    # -----------------------------------

    function getParamsAttribute()
    {
        $tmp = explode(',', $this->parameters);
        $ret = [];
        foreach ($tmp as $t) {
            $t = explode(':', $t);
            if (isset($t[0]) && isset($t[1]))
                $ret[$t[0]] = $t[1];
            else
                Log::warning('Bot Parameters wrong structure. parameter will be ignored.');
        }

        return $ret;
    }

    public static function dataTableQuery(string $status = null)
    {
        return (new static)->newQuery();

//        $query = new static;
//        if (!is_null($status)) $query = $query->where($query->getTable().'.status', $status);
//
//        $query = $query->leftJoin('agency_groups', 'agencies.group_id', '=', 'agency_groups.id')
//            ->select(['agencies.*', 'agency_groups.title as agency_group']);
//        return $query;
    }
}
