<?php

namespace App\Models;

use App\Models\Traits\DeepCacheQuery;
use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SettingGroup extends Model
{
    use HasFactory;
    use CacheQueryBuilder;

    # TODO fix undefined variable id
//    use DeepCacheQuery;
    static $deepCacheTTL = 0;

    public static array $validation = [
        '*'         => 'bail|string',
        'parent_id' => 'nullable|exists:setting_groups,id',
        'title'     => 'required|max:120|unique:setting_groups,title',
        'slug'      => 'required|max:80|unique:setting_groups,slug',
        'order'     => 'required',
        'hidden'    => 'boolean',
    ];

    public static array $dataTableFields = [
        'title'   => [],
        'parent'  => [],
        'slug'    => [],
        'actions' => ['dont-all'],
    ];

    protected $fillable = [
        'parent_id',
        'title',
        'slug',
        'order',
        'hidden',
    ];

    # -----------------------------------

    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id');
    }

    public static function getAll($slug)
    {
        return self::where('slug', $slug)->first()->settings;
    }

    public function settings()
    {
        return $this->hasMany(Setting::class, 'group_id');
    }


    public static function dataTableQuery(string $status = null)
    {
        return (new static)->newQuery();

//        $query = new static;
//        if (!is_null($status)) $query = $query->where($query->getTable().'.status', $status);
//
//        $query = $query->leftJoin('agency_groups', 'agencies.group_id', '=', 'agency_groups.id')
//            ->select(['agencies.*', 'agency_groups.title as agency_group']);
//        return $query;
    }
}
