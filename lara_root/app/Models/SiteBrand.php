<?php

namespace App\Models;

use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class SiteBrand extends Model
{
    use HasFactory;
    use CacheQueryBuilder;

    static $deepCacheTTL = 0;

    public static array $validation = [
        '*'           => 'bail|string',
        'domain_id'   => 'required|exists:domains,id',
        'title'       => 'required|max:120',
        'slug'        => 'required|max:80|unique:site_brands,slug',
        'order'       => 'required',
        'lang'        => 'nullable',
        'direction'   => 'required',
        'mode'        => 'required',
        'description' => 'nullable',
        'value'       => 'nullable',
    ];

    public static array $dataTableFields = [
        'title'   => [],
        'domain'  => [],
        'lang'    => [],
        'order'   => [],
        'actions' => ['dont-all'],
    ];

    protected $fillable = [
        'domain_id',
        'title',
        'slug',
        'value',
        'order',
        'lang',
        'direction',
        'mode',
        'description',
    ];

    # -----------------------------------

    public function setAttribute($key, $value)
    {
        Cache::forget('site_brands_group__as_array');
        return parent::setAttribute($key, $value);
    }

    public static function isSame($params, $checkValue)
    {
        $ret = Self::get($params);
        return (
            isset($ret) && ((
            is_array($ret) ? in_array($checkValue, $ret) : ($ret == $checkValue)
            ))
        );
    }

    public static function get($slug, $default = null)
    {
        $item = Self::where('slug', $slug)->first();

        return ($item) ? $item->getValue() : $default;
    }

    public static function has($param)
    {
        return !empty(Self::get($param));
    }

    public static function asArray()
    {
        if (Cache::has('site_brands_group__as_array')) return Cache::get('site_brands_group__as_array');

        $ret = Self::get(['slug', 'value'])->byKey('slug');

        Cache::forever('site_brands_group__as_array', $ret);
        return $ret;
    }

    public function domain()
    {
        return $this->belongsTo(Domain::class, 'domain_id');
    }

    public static function dataTableQuery(string $status = null)
    {
        return (new static)->newQuery();

//        $query = new static;
//        if (!is_null($status)) $query = $query->where($query->getTable().'.status', $status);
//
//        $query = $query->leftJoin('agency_groups', 'agencies.group_id', '=', 'agency_groups.id')
//            ->select(['agencies.*', 'agency_groups.title as agency_group']);
//        return $query;
    }
}
