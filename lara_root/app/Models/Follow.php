<?php

namespace App\Models;

use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Follow extends Model
{
    use HasFactory;
    use CacheQueryBuilder;

    public static array $dataTableFields = [
        'master_id' => [], //options: dont-search, dont-order, dont-print, dont-export, dont-all
        'slave_id'  => [],
        'actions'   => ['dont-all'],
    ];

    public static array $validation = [
        '*'         => 'bail|string',
        'master_id' => 'required',
        'slave_id'  => 'required',
    ];

    protected $fillable = [
        'master_id',
        'slave_id',
    ];

    # -----------------------------------

    public static function dataTableQuery(string $status = null)
    {
        return (new static)->newQuery();

//        $query = new static;
//        if (!is_null($status)) $query = $query->where($query->getTable().'.status', $status);
//
//        $query = $query->leftJoin('agency_groups', 'agencies.group_id', '=', 'agency_groups.id')
//            ->select(['agencies.*', 'agency_groups.title as agency_group']);
//        return $query;
    }

}
