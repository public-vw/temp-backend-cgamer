<?php

namespace App\Console\Tests;

use App\Models\Article;
use App\Models\Attachment;
use Illuminate\Support\Facades\Storage;

class CheckAttachmentsOnArticlesClass
{
    protected $report = [];

    protected $article_id = null;
    protected $article_status = null;
    protected $just_issues = false;

    public function __construct($article_id = null, $article_status = null, $just_issues = false)
    {
        $this->article_id = $article_id;
        $this->article_status = $article_status;
        $this->just_issues = $just_issues;
    }

    public function handle()
    {
        if ($this->article_id) {
            $this->analyse($this->article_id);
        } else {
            $articles = $this->article_status ? Article::where('status', config_key("enums.articles_status", $this->article_status))->get() : Article::all();
            $this->report['total_articles'] = count($articles);
            foreach ($articles as $article) {
                $this->analyse($article->id);
            }
        }
        return 0;//finished successfully
    }

    protected function analyse($article_id)
    {
        $article = Article::find($article_id);
        if (!$article) {
            $this->report['not_found'][] = ['id' => $article_id];
            return 1;
        }

        $fineState = true;
        list($attachments, $placeholders) = $this->getAttachmentsList($article);

        $this->report['articles'][$article_id] = [
            'id'     => $article_id,
            'status' => config("enums.articles_status.{$article->status}"),
        ];

        foreach ($attachments as $attachment) {
            if (!$attachment['exists']) {
                $fineState = false;
                $this->report['articles'][$article_id]['errors']['not_found'][] = [
                    'url'  => $attachment['url'],
                    'path' => $attachment['path'],
                ];
            }
        }
        if (count($placeholders)) {
            $fineState = false;
            $this->report['articles'][$article_id]['warnings'][] = [
                'placeholders' => count($placeholders),
            ];
        }

        if($this->just_issues && $fineState){
            unset($this->report['articles'][$article_id]);
        }
        else{
            $this->report['articles'][$article_id]['result'] = $fineState ? 'fine' : 'issue';
        }
    }

    protected function getAttachmentsList(Article $article)
    {
        $placeholders = [];
        $attachments = [];
        preg_replace_callback(Article::$PATTERN_IMG, function ($matches) use (&$attachments, &$placeholders) {
            $attach_id = $matches[1];
            if ($attach_id === 'placeholder') {
                $placeholders[] = 'placeholder';
            } else {
                $attachment = Attachment::find($attach_id);
                $attachments[] = [
                    'path'   => $attachment->path,
                    'url'    => $attachment->url,
                    'exists' => Storage::disk($attachment->storage_disk)->exists($attachment->fullname),
                ];
            }
        }, $article->content);

        return [$attachments, $placeholders];
    }

    public function getReport()
    {
        return $this->report;
    }
}
