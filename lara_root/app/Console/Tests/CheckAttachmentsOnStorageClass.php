<?php

namespace App\Console\Tests;

use App\Models\Article;
use App\Models\Attachment;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class CheckAttachmentsOnStorageClass
{
    #finds and returns all local files that have no record in attachment table
    #checks filename and md5

    protected $report = [
        'summary'      => [
            'files'      => 0,
            'records'    => 0,
            'errors'     => 0,
            'terminated' => 0,
        ],
        'issued_files' => [],
    ];
    protected $ignore = ['.gitignore'];

    protected $delete = false;

    public function __construct($delete)
    {
        $this->delete = $delete;
    }

    public function handle()
    {
        foreach (config('filesystems.public_disks') as $driver) {
            foreach (Storage::disk($driver)->allFiles() as $file) {
                if (in_array($file, $this->ignore)) continue;
                $this->report['summary']['files'] += 1;

                $ext = pathinfo($file, PATHINFO_EXTENSION);
                $name = Str::replaceLast(".$ext", '', $file);
                $md5 = md5_file(Storage::disk($driver)->path($file));

                $record = Attachment::where([
                    'file_name' => $name,
                    'file_ext'  => $ext,
                ])->orWhere([
                    'md5'  => $md5,
                ])
                    ->first();

                if (!$record) {
                    $this->report['summary']['errors'] += 1;
                    $this->report['issued_files'][$driver][] = $file;
                    if ($this->delete) {
                        Storage::disk($driver)->delete($file);
                        $this->report['summary']['terminated'] += 1;
                    }
                } else {
                    $this->report['summary']['records'] += 1;
                }
            }
        }

        return 0;//finished successfully
    }

    public function getReport()
    {
        return $this->report;
    }
}
