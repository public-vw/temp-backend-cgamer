<?php

namespace App\Console;

use App\Models\Attachment;
use App\Models\BotGasfeeAlarm;
use App\Models\ServerConfig;
use App\Support\GetRates\GasFee;
use App\Support\ServerConfig\ServerConfigCheck;
use App\Support\Telegram\TelegramBotOperator;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Artisan;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        if (ServerConfig::getStatus('cronjob') === 'checking') {
            $schedule->call(function () {
                ServerConfig::setValue('cronjob', 'on');
            });
        }

        if (config('cronjobs.clear_old_temporaries.active')) {
            $schedule->call(function () {
                Attachment::clearOldTemporaries(config('cronjobs.clear_old_temporaries.how_olds_days'));
            })->dailyAt(config('cronjobs.clear_old_temporaries.period_run_at'));
        }

        if (config('cronjobs.gasfeerobot.active')) {
            $schedule->call(function () {
                $chat_op = new TelegramBotOperator(2);
                $message = fn($gasfee) => '🎉 Cangragulations! 🎉'
                    . "\n" . '<b>Gas Fee</b> now is $' . $gasfee
                    . "\n===================="
                    . "\n" . "<b>Date :</b> " . getFormedDate(Carbon::now())
                    . "\n" . "<b>Time :</b> " . Carbon::now()->format('h:i:s A')
                    . "\n\n\n" . "<b>Note :</b> This is a one-time alarm."
                    . "\n" . "You can always set another <b>Alarm</b>"
                    . "\n" . "Just touch >>> <b>/start</b>";

                $commands = [];

                $gasfee = null;
                $alarms = BotGasfeeAlarm::where('fired', false);
                if ($alarms->count() <= 0) return;

                $gasfee = (new GasFee())->get();
                $alarms = $alarms->where('threshold', '>', $gasfee)->get();
                foreach ($alarms as $alarm) {
                    $alarm->update(['fired' => true]);
                    $chat_op->sendMessageInlineMarkup($alarm->botUser->user_telegram_uid, trim($message($gasfee)), $commands, 'HTML', 1);
                }
            })->everyMinute();
        }

        if (config('cronjobs.auto_backup.db.active')) {
            $schedule->call(function () {
                Artisan::call('mydb:backup -u -k');
            })->dailyAt(config('cronjobs.auto_backup.db.period_run_at'));
        }

        if (config('cronjobs.auto_backup.uploads.active')) {
            $schedule->call(function () {
                Artisan::call('myup:backup -u -k');
            })->dailyAt(config('cronjobs.auto_backup.uploads.period_run_at'));
        }

        // $schedule->command('inspire')->hourly();
    }

    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
