<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class CsfAddLog extends Command
{
    protected $signature = 'csf:add {line}';

    protected $description = 'Adds Log for CSF';

    public function handle()
    {
        $date = Carbon::now()->format('Y-m-d');
        $log_file = storage_path("logs/csf-{$date}.log");
        $line = $this->argument('line');
        File::append($log_file,$line);
        File::append($log_file,PHP_EOL);
        return 0;
    }
}
