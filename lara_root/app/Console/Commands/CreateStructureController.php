<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class CreateStructureController extends Command
{
    protected $signature = 'structure:controller {panel} {model}';

    protected $description = 'Creates Controller';

    public function handle()
    {
        $panel = $this->argument('panel');
        $panel = Str::ucfirst($panel);
        $panel = str_replace(' ', '', $panel);
        $snake_panel = Str::snake($panel);

        $model = $this->argument('model');
        $model = Str::ucfirst($model);
        $model = str_replace(' ', '', $model);
        $camelModel = Str::camel($model);

        $dest = "Http/Controllers/{$panel}/{$model}Controller.php";

        $content = file_get_contents(storage_path("templates/_Model_Controller.php"));

        ## change _Panel_
        $content = str_replace('_Panel_',$panel,$content);
        //## change _panel_
        $content = str_replace('_panel_',$snake_panel,$content);
        //## change _Model_
        $content = str_replace('_Model_',$model,$content);
        //## change _model_
        $content = str_replace('_model_',$camelModel,$content);
        ## removes comments
        $content = preg_replace('/^\#.*?\n/m','',$content);

        file_put_contents(app_path($dest),$content);

        $this->comment(
            "Controller created successfully: {$dest}"
        );
    }
}
