<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class CreateStructureDatatable extends Command
{
    protected $signature = 'structure:datatable {panel} {model}';

    protected $description = 'Creates Data Table Class';

    public function handle()
    {
        $panel = $this->argument('panel');
        $panel = Str::ucfirst($panel);
        $panel = str_replace(' ', '', $panel);

        $model = $this->argument('model');
        $model = Str::ucfirst($model);
        $model = str_replace(' ', '', $model);

        $dest = "DataTables/{$panel}/{$model}DataTable.php";

        $content = file_get_contents(storage_path("templates/_Model_DataTable.php"));

        ## change _Panel_
        $content = str_replace('_Panel_',$panel,$content);
        //## change _Model_
        $content = str_replace('_Model_',$model,$content);
        ## removes comments
        $content = preg_replace('/^\#.*?\n/m','',$content);

        file_put_contents(app_path($dest),$content);

        $this->comment(
            "Datatable created successfully: {$dest}"
        );
    }
}
