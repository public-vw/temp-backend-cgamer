<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class CreateStructureFactory extends Command
{
    protected $signature = 'structure:factory {model}';

    protected $description = 'Creates Factory Files';

    public function handle()
    {
        $model = $this->argument('model');
        $model = Str::ucfirst($model);
        $model = str_replace(' ', '', $model);

        $this->createFactory($model);
        $this->createFactorySeeder($model);
    }

    protected function createFactory($model): void
    {
        $dest = "factories/{$model}Factory.php";

        $content = file_get_contents(storage_path("templates/_Model_Factory.php"));

        //## change _Model_
        $content = str_replace('_Model_', $model, $content);
        ## removes comments
        $content = preg_replace('/^\#.*?\n/m', '', $content);

        file_put_contents(database_path($dest), $content);

        $this->comment(
            "Factory created successfully: {$dest}"
        );
    }

    protected function createFactorySeeder($model): void
    {
        $dest = "seeders/FactoringSeeders/{$model}Seeder.php";

        $content = file_get_contents(storage_path("templates/_Model_FactorySeeder.php"));

        //## change _Model_
        $content = str_replace('_Model_', $model, $content);
        ## removes comments
        $content = preg_replace('/^\#.*?\n/m', '', $content);

        file_put_contents(database_path($dest), $content);

        $this->comment(
            "Factory Seeder created successfully: {$dest}"
        );
    }
}
