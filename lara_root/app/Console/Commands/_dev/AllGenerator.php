<?php

namespace App\Console\Commands;

use File;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Str;

class AllGenerator extends Command
{
    protected $signature = 'generate:all {target} {from=1} {to=1000} {folder=.}';

    protected $description = 'Generate All Views or Controllers'
        . ' | Usage: php artisan generate:all view|controller';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $target = $this->argument('target');
        if(!in_array($target, ['view','controller'])) return $this->error('Target must be View or Controller');

        $from = max(0,intval($this->argument('from'))-1);
        $to = min(1000,intval($this->argument('to'))-1);

        $models = getModels(app_path('Models/'.$this->argument('folder')));

        foreach($models as $i=>$model){
            if($i < $from) continue;
            if($i > $to)   break;

            Artisan::call("generate:$target $model . --refresh --force", [], $this->getOutput());
        }
    }

}
