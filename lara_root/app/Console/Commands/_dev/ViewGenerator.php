<?php

namespace App\Console\Commands;

use File;
use Illuminate\Console\Command;
use Str;

class ViewGenerator extends Command
{
    protected $signature = 'generate:view {model} {folder=.} {--R|rollback} {--r|refresh} {--f|force}';

    const COMPONENTS_TEMPLATE = 'resources/views/panel/components/_templates.blade.php';

    const VIEW_BASE = 'resources/views/admin/';
    const VIEW_TEMPLATE= 'resources/views/panel/_items_template';


    protected $field_to_component = [
        'smallint' => 'NUMBER',
        'int' => 'NUMBER',
        'bigint' => 'NUMBER',
        'double' => 'NUMBER',

        'varchar' => 'TEXT',
        'text' => 'TEXTAREA',
        'longtext' => 'TEXTAREA',

        'timestamp' => 'DATETIME',
        'datetime' => 'DATETIME',
        'date' => 'DATE',

        'tinyint' => 'CHECKBOX SINGLE',
        'bool' => 'CHECKBOX SINGLE',

        'enum' => 'STATIC SELECT',
    ];

    protected $fullmodel, $fullplural, $location, $location_space;
    protected $model, $plural;
    protected $model_instance, $table, $fillables, $listables, $custom_fields;
    protected $form_elements;
    protected $has_view;

    protected $description = 'Generate View Files'
        . ' | Usage: php artisan generate:view Setting'
        . ' || php artisan generate:view Bus Business';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->setupVariables();

        if($this->option('rollback')){
            if($this->option('force') || $this->confirm('Are you sure?')) $this->rollback();
            return;
        }
        if($this->option('refresh')) {
            if ($this->option('force') || $this->confirm('Are you sure?')) {
                $this->rollback();
            }
        }

        //FCELS Method
        //F: Forms (CE)
        //C: Create
        //E: Edit
        //L: List
        //S: Show
        if( $this->has_view == 'all'
            || strpos($this->has_view,'F') !== false
            || strpos($this->has_view,'C') !== false
        ) {
            $forms = $this->makeForms();
            $this->makeViewCreate($forms);
        }

        if( $this->has_view == 'all'
            || strpos($this->has_view,'F') !== false
            || strpos($this->has_view,'E') !== false
        ) {
            $forms = $this->makeForms(true);
            $this->makeViewEdit($forms);
        }
        if($this->has_view == 'all' || strpos($this->has_view,'L') !== false ) {
            if (is_null($this->listables)) {
                $this->warn('*** Listable not found in Model');
            } else {
                $cols = $this->makeListCols();
                $this->makeViewIndex($cols);
            }
        }
        if($this->has_view == 'all' || strpos($this->has_view,'S') !== false ){
            $items = $this->makeShowItems();
            $this->makeViewShow($items);
        }
    }

    protected function setupVariables(){
        $this->model = $this->argument('model');
        $this->location = $this->argument('folder')=='.'? null : $this->argument('folder');
        $this->plural = Str::plural($this->model);

        $this->fullplural = str_replace('\\','.',strtolower(($this->location? $this->location . '\\' : '') . $this->plural));
        $this->fullmodel = ($this->location? $this->location . '\\' : '') . $this->model;
        $model = 'App\\Models\\' . $this->fullmodel;

        if (!class_exists($model)) {
            $this->error("Model [$model] not exists");
            exit(0);
            return;
        }

        $model = new $model;
        $this->model_instance = $model;
        $this->table = $model->getTable();
        $this->fillables = $model->getFillable();
        $this->listables = $model::$listables ?? null;
        $this->custom_fields = $model::$custom_fields ?? null;
        $this->location_space = ( $this->location ?  '\\' . $this->location : null );
        $this->location = ( $this->location ? $this->location . '/' : null );

        $this->has_view = $model::$generate_view ?? true;

    }

    private function makeListCols(){
        $table_cols = [];

        foreach($this->listables as $listable){
            $table_cols[$listable] = "\t\t\t\t\t\t'$listable' => __('lists.".strtolower($this->plural).".{$listable}'),";
        }

        return $table_cols;
    }

    private function makeForms($defaults = false){
        $form_elements = [];

        $components = $this->getViewComponents();
        $fields = $this->getFormElements();

        foreach ($fields as $name => $field) {
            $element = $this->fieldToComponentType($components, $name, $field['type']);

            if (!isset($field['type'])) $form_elements[] = "### Not Implemented F | FIELD NAME: {$name} ###" . var_export($field, true) . '<br/>';
            elseif (!isset($element)) $form_elements[] = "### Not Implemented FTC ({$field['type']}) | FIELD NAME: {$name} ### <br/>";
            elseif (!isset($components[$element])) $form_elements[] = "### Not Implemented C ({$field['type']} -> {$this->field_to_component[$field['type']]}) | FIELD NAME: {$name} ### <br/>";
            elseif (empty($components[$element])) $form_elements[] = "### Empty C ({$field['type']} -> {$this->field_to_component[$field['type']]}) | FIELD NAME: {$name} ### <br/>";
            else {

                $form_elements[] = $this->getElementByFieldData($components, $field, ['name' => $name], $defaults);
            }
        }
        return $form_elements;
    }

    private function makeShowItems(){
        $items = [];

        foreach ($this->fillables as $name) {
            $items[] = "\t".'<b>'.ucwords(str_replace('_',' ',$name)).' :</b>&nbsp;'."\n\t".'{{$item->'.$name.'}}<br/>';
        }
        return $items;
    }

    private function fieldToComponentType($components, $field_name, $field_type){
        $ret = null;
        $cfield = $this->custom_fields[$field_name] ?? null;
        if(isset($cfield)){
            $cfield = explode(',',$cfield);
            $ret = $cfield[0];
        }
        else {
            $ret = $this->field_to_component[$field_type];
        }

        return $ret;
    }

    private function getElementByFieldData($components , $field, $properties = [], $defaults=false){
        $element = $components[$this->  fieldToComponentType($components, $properties['name'], $field['type'])];
        $element = str_replace('NAME',$properties['name'], $element);

        $i = substr(
            $element,
            strpos($element,'['),
            strrpos($element,']')-strpos($element,'[')+1
        );

        $i = str_replace('code', '""', $i);

// TODO: What is this $i for? Complete the purpose

//            eval('$i='.$i.';');
//            $i = array_keys($i);

        $this->ReplaceElementAttr($element, 'name', $properties['name']);
        $this->ReplaceElementAttr($element, 'title', '__(\'forms.'.strtolower($this->model).'.'.$properties['name'].'\')');
        $this->ReplaceElementAttr($element, 'required', $field['details']['Null'] == "YES");
        $this->ReplaceElementAttr($element, 'maxlen', $field['length']);

        if($defaults) $this->ReplaceElementAttr($element, 'default', $defaults?'$item->'.$properties['name']:null, false);
        else $this->ReplaceElementAttr($element, 'default', null);

        return $element;
    }

    private function ReplaceElementAttr(&$element, $attr, $content, $quoted = true){
        $match = null;
        preg_match("/\'$attr\' \=\> (.*),/m", $element,$match);
        if(isset($match[1])) {
            if(isset($content) && $content){
                if($quoted) $content = "'$content'";
                $element = str_replace($match[0],str_replace($match[1],$content,$match[0]),$element);
            }
            else
                $element = str_replace($match[0],'',$element);
        }
    }

    private function getFormElements(){
        $form_elements = [];

        foreach($this->fillables as $fillable){
            if(strrpos($fillable, "able")){
                $form_elements[$fillable."_type"] = $this->getFieldStructure($this->model_instance,$fillable."_type");
                $form_elements[$fillable."_id"] = $this->getFieldStructure($this->model_instance,$fillable."_id");
            }
            else {
                $tmp = $this->getFieldStructure($this->model_instance,$fillable);

                if(empty($tmp)){
                    throw new \ErrorException('Model Field not exists in Database Table Structure: ['.$this->model.'|'.$fillable.']');
                }
                $form_elements[$fillable] = $tmp;
            }
        }

        return $form_elements;
    }

    private function getViewComponents(){
        $components = [];
        $template = file_get_contents(base_path(self::COMPONENTS_TEMPLATE));

        $matches = null;
        preg_match_all('/(.*)\n([^#]*)(?=##############)/m', $template, $matches, PREG_SET_ORDER, 0);
        foreach($matches as $match){
            $tmp = str_replace('#','', $match[1]);
            $tmp2 = str_replace('TABLE',$this->table, $match[2] ?? null);
            $components[trim($tmp)] = trim($tmp2);
        }

        return $components;
    }

    private function getFieldStructure($model_instance, $field){
        $data = $model_instance->newQuery()->fromQuery("SHOW FIELDS FROM `$this->table` where `Field` = '$field';");
        //$data = $data->toArray();

        if(!isset($data[0])) return null;

        $data = $data[0];

        $match = null;
        preg_match("/(.*)\((.*)\)/m", $data['Type'],$match);

        if(!isset($match[1]))
            preg_match("/(.*).*/m", $data['Type'],$match);

        if($match[1] == 'enum') $match[2] = explode(',',str_replace('','',$match[2]));

        return ['details' => $data, 'type' => $match[1] ?? null, 'length' => $match[2] ?? null, 'extra' => $match[3] ?? null];
    }

    private function makeViewCreate($forms){
        $template = self::VIEW_TEMPLATE . '/create.blade.php';
        $dest = self::VIEW_BASE . strtolower($this->location) . strtolower($this->plural) . '/';

        if(!File::exists($dest)) {
            File::makeDirectory($dest);
            $this->comment("Folder [$dest] Created Successfully");
        }

        $dest .= 'create.blade.php';
        File::copy($template,$dest);
        $this->comment("File [$dest] Created Successfully");

        $this->ReplaceInFile($dest, "/_Templates/m", $this->plural);
        $this->ReplaceInFile($dest, "/_Template/m", $this->model);
        $this->ReplaceInFile($dest, "/_templates/m", strtolower($this->plural));
        $this->ReplaceInFile($dest, "/_template/m", strtolower($this->model));
        $this->ReplaceInFile($dest, "/##FORM ELEMENTS##/m", implode("\n\n",$forms));

        $this->comment("Data in Create of Folder [$dest] Replaced Successfully");
    }

    private function makeViewEdit($forms){
        $template = self::VIEW_TEMPLATE.'/edit.blade.php';
        $dest = self::VIEW_BASE . strtolower($this->location) . strtolower($this->plural) . '/';

        if(!File::exists($dest)) {
            File::makeDirectory($dest);
            $this->comment("Folder [$dest] Created Successfully");
        }

        $dest .= 'edit.blade.php';
        File::copy($template,$dest);
        $this->comment("File [$dest] Created Successfully");

        $this->ReplaceInFile($dest, "/_Templates/m", $this->plural);
        $this->ReplaceInFile($dest, "/_Template/m", $this->model);
        $this->ReplaceInFile($dest, "/_templates/m", strtolower($this->plural));
        $this->ReplaceInFile($dest, "/_template/m", strtolower($this->model));
        $this->ReplaceInFile($dest, "/##FORM ELEMENTS##/m", implode("\n\n",$forms));

        $this->comment("Data in Edit of Folder [$dest] Replaced Successfully");
    }

    private function makeViewIndex($cols){
        $template = self::VIEW_TEMPLATE.'/index.blade.php';
        $dest = self::VIEW_BASE . strtolower($this->location) . strtolower($this->plural) . '/';

        if(!File::exists($dest)) {
            File::makeDirectory($dest);
            $this->comment("Folder [$dest] Created Successfully");
        }

        $dest .= 'index.blade.php';
        File::copy($template,$dest);
        $this->comment("File [$dest] Created Successfully");

        $this->ReplaceInFile($dest, "/_Templates/m", $this->plural);
        $this->ReplaceInFile($dest, "/_Template/m", $this->model);
        $this->ReplaceInFile($dest, "/_templates/m", strtolower($this->plural));
        $this->ReplaceInFile($dest, "/_template/m", strtolower($this->model));
        $this->ReplaceInFile($dest, "/##TABLE COLS##/m", implode("\n",$cols));

        $this->comment("Data in Edit of Folder [$dest] Replaced Successfully");
    }

    private function makeViewShow($items){
        $template = self::VIEW_TEMPLATE.'/show.blade.php';
        $dest = self::VIEW_BASE . strtolower($this->location) . strtolower($this->plural) . '/';

        if(!File::exists($dest)) {
            File::makeDirectory($dest);
            $this->comment("Folder [$dest] Created Successfully");
        }

        $dest .= 'show.blade.php';
        File::copy($template,$dest);
        $this->comment("File [$dest] Created Successfully");

        $this->ReplaceInFile($dest, "/_Templates/m", $this->plural);
        $this->ReplaceInFile($dest, "/_Template/m", $this->model);
        $this->ReplaceInFile($dest, "/_templates/m", strtolower($this->plural));
        $this->ReplaceInFile($dest, "/_template/m", strtolower($this->model));
        $this->ReplaceInFile($dest, "/##CONTENTS##/m", implode("\n\n",$items));

        $this->comment("Data in Show of Folder [$dest] Replaced Successfully");
    }

    private function rollback(){
        $dest = self::VIEW_BASE . strtolower($this->location) . strtolower($this->plural);
        if(!File::exists($dest)) $this->warn("%% Folder [$dest] not found");
        else{
            File::deleteDirectory($dest);
            $this->comment("[$dest] Deleted Successfully");
        }
    }

    private function ReplaceInFile($src, $search, $replace){
        $content = file_get_contents($src);
        $content = preg_replace($search, $replace, $content);
        file_put_contents($src, $content);
    }
}
