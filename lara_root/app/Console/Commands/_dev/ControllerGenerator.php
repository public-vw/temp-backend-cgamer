<?php

namespace App\Console\Commands;

use File;
use Illuminate\Console\Command;
use Str;

class ControllerGenerator extends Command
{
    protected $signature = 'generate:controller {model} {folder=.} {--R|rollback} {--r|refresh} {--f|force}';

    const COMPONENTS_TEMPLATE = 'resources/views/panel/components/_templates.blade.php';

    const CONTROLLER_BASE = 'app/Http/Controllers/Panel/';
    const CONTROLLER_TEMPLATE = 'app/Libraries/Templates/_TemplateController.php';


    protected $fullmodel, $fullplural, $location, $location_space;
    protected $model, $plural;
    protected $model_instance, $table, $fillables, $listables, $custom_fields;
    protected $form_elements;
    protected $has_controller;

    protected $description = 'Generate Controller Classes'
        . ' | Usage: php artisan generate:controller Setting'
        . ' || php artisan generate:controller Bus Business';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->setupVariables();

        if($this->option('rollback')){
            if($this->option('force') || $this->confirm('Are you sure?')) $this->rollback();
            return;
        }
        if($this->option('refresh')) {
            if ($this->option('force') || $this->confirm('Are you sure?')) {
                $this->rollback();
            }
        }

        //FCELS Method
        //F: Forms (CE)
        //C: Create
        //E: Edit
        //L: List
        //S: Show

        //TODO: Controller functions remove by FCELS Method
        if($this->has_controller){
            $this->makeController();
        }
    }

    protected function setupVariables(){
        $this->model = $this->argument('model');
        $this->location = $this->argument('folder')=='.'? null : $this->argument('folder');
        $this->plural = Str::plural($this->model);

        $this->fullplural = str_replace('\\','.',strtolower(($this->location? $this->location . '\\' : '') . $this->plural));
        $this->fullmodel = ($this->location? $this->location . '\\' : '') . $this->model;
        $model = 'App\\Models\\' . $this->fullmodel;

        if (!class_exists($model)) {
            $this->error("Model [$model] not exists");
            exit(0);
            return;
        }

        $model = new $model;
        $this->model_instance = $model;
        $this->table = $model->getTable();
        $this->fillables = $model->getFillable();
        $this->listables = $model::$listables ?? null;
        $this->custom_fields = $model::$custom_fields ?? null;
        $this->location_space = ( $this->location ?  '\\' . $this->location : null );
        $this->location = ( $this->location ? $this->location . '/' : null );

        $this->has_controller = $model::$generate_controller ?? true;

    }

    private function makeController(){
        $template = self::CONTROLLER_TEMPLATE;
        $dest = self::CONTROLLER_BASE . $this->location . ucwords($this->plural)."Controller.php";

        if(File::exists($dest)) {
            $this->error("File [$dest] already exists");
            return;
        }

        File::copy($template,$dest);

        $this->comment("Controller [$dest] Created Successfully");

        $this->ReplaceInFile($dest, "/_Location/m", $this->location_space);

        $this->ReplaceInFile($dest, "/_Full_Template/m", $this->fullmodel);
        $this->ReplaceInFile($dest, "/_full_templates/m", strtolower($this->fullplural));

        $this->ReplaceInFile($dest, "/_Templates/m", $this->plural);
        $this->ReplaceInFile($dest, "/_Template/m", $this->model);
        $this->ReplaceInFile($dest, "/_templates/m", strtolower($this->plural));
        $this->ReplaceInFile($dest, "/_template/m", strtolower($this->model));

        $this->comment("Data in File [$dest] Replaced Successfully");
    }

    private function rollback(){
        $dest = self::CONTROLLER_BASE . $this->location . ucwords($this->plural)."Controller.php";
        if(!File::exists($dest)) $this->warn("%% File [$dest] not found");
        else{
            File::delete($dest);
            $this->comment("[$dest] Deleted Successfully");
        }
    }

    private function ReplaceInFile($src, $search, $replace){
        $content = file_get_contents($src);
        $content = preg_replace($search, $replace, $content);
        file_put_contents($src, $content);
    }
}
