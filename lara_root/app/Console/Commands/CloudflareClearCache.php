<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class CloudflareClearCache extends Command
{
    protected $signature = 'cf:cache:clear';

    protected $description = 'Clears cloudflare cache';

//    protected $path = 'https://api.cloudflare.com/client/v4/zones';
    protected $path = 'https://api.cloudflare.com/client/v4/zones/:identifier/purge_cache';

    protected $identifier = 'e1ee0f938579b189f47aea83008a264e';

    public function handle()
    {
        $this->getIdentifier();

        $this->setIdentifier();

        $response = Http::withHeaders([
            'X-Auth-Email' => config('cloudflare.api_user'),
            'X-Auth-Key'   => config('cloudflare.api_key'),
            'Content-Type' => 'application/json',
        ])->post($this->path,[
            'purge_everything' => true
        ]);

        dd($response->body());

        $this->comment($response);

        return 0;
    }

    protected function setIdentifier(){
        $this->path = str_replace(':identifier',$this->identifier,$this->path);
    }

    protected function getIdentifier()
    {
        $exception = null;

        $path = substr($this->path,0,strpos($this->path,':identifier'));
        $response = Http::withHeaders([
            'X-Auth-Email' => config('cloudflare.api_user'),
            'X-Auth-Key'   => config('cloudflare.api_key'),
            'Content-Type' => 'application/json',
        ])->get($path);

        try {
            $response = json_decode($response->body());

            $this->identifier = $response->result[0]->id ?? null;
        }
        catch (\Exception $e){
            $exception = $e;
        }

        if($exception || $response->success === false){
            $this->error('Identifier issue.');
            abort(500,json_encode($response->errors));
        }
    }
}
