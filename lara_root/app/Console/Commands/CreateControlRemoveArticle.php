<?php

namespace App\Console\Commands;

use App\Models\Article;
use App\Models\Attachment;
use App\Models\FakeRecord;
use Illuminate\Console\Command;

class CreateControlRemoveArticle extends Command
{
    protected $signature = 'control:removeArticle {id}';

    protected $description = 'Removes Article Safely';

    protected int $deleted = 0;
    protected int $errors = 0;

    public function handle()
    {
        $article_id = $this->argument('id');

        $article = Article::find($article_id);
        if(!$article)return $this->error('Article Not Found'." | ID: {$article_id}");

        $article->hardDelete();
        $this->comment("Article Deleted Successfully");
    }
}
