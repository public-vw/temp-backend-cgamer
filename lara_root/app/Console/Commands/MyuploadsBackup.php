<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class MyuploadsBackup extends Command
{
    protected $signature = 'myup:backup {--u|upload} {--k|nolocal} {--s|silence}';

    protected $description = 'Makes backup from uploaded files';

    protected $rcloneConfigName = 'mr_backup_cryptobaz';

    protected array $uploadsFolders = [
        'perm' => 'public/uploads',
        'temp' => 'public/tmp',
    ];
    protected string $backupFolder = 'DB_Backups';

    public function handle()
    {
        $time = Carbon::now()->format('Ymd_His');
        $code = Str::random(3);

        $filename = fn($tag) => sprintf("ups_%s_%s_%s", $time, $code, $tag);

        $this->comment("Starting Uploads Backup");

        if ($this->option('upload')) {
            $this->comment('It will upload to remote drive');
        }

        foreach ($this->uploadsFolders as $tag => $upFolder) {
            runShell('cd ' . base_path($upFolder) . ';'
                . "tar czf {$filename($tag)}.tar.gz .;"
                . "mv {$filename($tag)}.tar.gz " . base_path($this->backupFolder) . ";");

            if ($this->option('upload')) {
                $result = runShell('cd '.base_path($this->backupFolder).';'.
                    "rclone copy {$filename($tag)}.tar.gz {$this->rcloneConfigName}:/ -v",true);
                $this->comment('output: '.$result['output']);
                $this->comment('errors: '.$result['errors']);

                if ($this->option('nolocal') && str_contains($result['errors'],'100%')) {
                    $result = runShell('cd '.base_path($this->backupFolder).';'.
                        "rm -f {$filename($tag)}.tar.gz -v");
                    $this->comment($result);
                }
            }

        }

        $this->comment("Uploaded Files Backup Created Successfully: " . $filename('*'));

        if (!$this->option('silence')) {
            alertAdminTelegram(
                "💾 <b>Uploaded Files</b> Backup created" .
                "\n🏁️ " . config('app.telegram_title', '') .
                "\n-------------------\n" .
                "\ndate: ".Carbon::now()->format('Y-m-d D') .
                "\ntime: ".Carbon::now()->format('H:i:s') .
                "\ncode: {$code}"
            );
        }

    }
}
