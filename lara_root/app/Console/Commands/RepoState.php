<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class RepoState extends Command
{
    protected $signature = 'repo:state {branch?}';

    protected $description = 'Gets state of the branch';

    public function handle()
    {
        $curr_branch = trim(runShell('git branch --show-current'));
        $branch = $this->hasArgument('branch')? $this->argument('branch'):env('DEPLOY_BRANCH',$curr_branch);
        $ret = runShell('git rev-list --format="%s | %ar" --max-count=2 '.$branch,true);
        $this->comment('output: '.$ret['output']);
        $this->comment('errors: '.$ret['errors']);
        return 0;
    }
}
