<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class RepoCommand extends Command
{
    protected $signature = 'repo:command';

    protected $description = 'Prints command of gitlab repository pulling';

    public function handle()
    {
        $branch = env('DEPLOY_BRANCH');
        $path = env('DEPLOY_PATH');
        $user = env('DEPLOY_TOKEN_USERNAME');
        $pass  = env('DEPLOY_TOKEN_PASSWORD');

        $ret = "git clone --single-branch --branch $branch https://$user:$pass@gitlab.com/$path;";
        $this->comment($ret);
        return 0;
    }
}
