<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class RepoStatus extends Command
{
    protected $signature = 'repo:status';

    protected $description = 'Queries project status';

    public function handle()
    {
        $command = "git status";
        $this->comment('CMD: ' . $command);
        $ret = runShell($command,true);
        $this->comment('output: '.$ret['output']);
        $this->comment('errors: '.$ret['errors']);
        return 0;
    }
}
