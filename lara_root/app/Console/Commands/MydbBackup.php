<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class MydbBackup extends Command
{
    #TODO: Add {filename} to customize backup filename
    protected $signature = 'mydb:backup {--u|upload} {--k|nolocal} {--s|silence}';

    protected $description = 'Makes backup from whole database';

    protected $rcloneConfigName = 'mr_backup_cryptobaz';

    protected string $backupFolder = 'DB_Backups';

    public function handle()
    {
        $dbName = config('database.connections.mysql.database');
        $dbUser = config('database.connections.mysql.username');
        $dbPass = config('database.connections.mysql.password');
        $code = Str::random(3);
        $filename = sprintf("db_%s_%s",
            Carbon::now()->format('Ymd_His'),
            $code
        );

        $this->comment("Starting DB Backup");
        $x = runShell('cd '.base_path($this->backupFolder).';'
                 ."mysqldump -u $dbUser -p\"$dbPass\" $dbName > {$filename}.sql;"
//                 ."mysqldump -u $dbUser -p\"$dbPass\" --no-create-db --no-create-info --ignore-table=$dbName.migrations $dbName > {$filename}.sql;"
                 ."tar czf {$filename}.tar.gz {$filename}.sql;"
                 ."rm -f {$filename}.sql;");
        $this->comment("DB Backup Created Successfully: ". $filename);

        if($this->option('upload')){
            $this->comment('Uploading to remote drive');
            $result = runShell('cd '.base_path($this->backupFolder).';'.
                "rclone copy {$filename}.tar.gz {$this->rcloneConfigName}:\/ -v",true);
            $this->comment('output: '.$result['output']);
            $this->comment('errors: '.$result['errors']);

            if ($this->option('nolocal') && str_contains($result['errors'],'100%')) {
                $result = runShell('cd '.base_path($this->backupFolder).';'.
                    "rm -f {$filename}.tar.gz -v");
                $this->comment($result);
            }
        }

        if (!$this->option('silence')) {
            alertAdminTelegram(
                "💾 <b>DB</b> Backup created" .
                "\n🏁️ " . config('app.telegram_title', '') .
                "\n-------------------\n" .
                "\ndate: ".Carbon::now()->format('Y-m-d D') .
                "\ntime: ".Carbon::now()->format('H:i:s') .
                "\ncode: {$code}"
            );
        }

    }
}
