<?php

namespace App\Console\Commands;

use App\Models\FakeRecord;
use Illuminate\Console\Command;

class CreateControlRemoveFakeData extends Command
{
    protected $signature = 'control:removeFakes {model?}';

    protected $description = 'Removes Fake Data Safely';

    protected int $deleted = 0;
    protected int $norec = 0;
    protected int $errors = 0;

    public function handle()
    {
        $model = $this->argument('model') ?? null;

        $fakeRecords = new FakeRecord;
        if($model) $fakeRecords = $fakeRecords->where('model','like',"%{$model}");
        $fakeRecords = $fakeRecords->get(['id']);

        foreach($fakeRecords as $record){
            $this->comment("REC {$record->id}\tDeleting ...");

            $this->delete($record->id);
        }

        $this->comment("Finished");
        $this->comment("Deleted records: {$this->deleted}");
        $this->comment("Deleted without record: {$this->norec}");
        $this->comment("Errors: {$this->errors}");
    }

    protected function delete($record_id){
        try{
            $childs = FakeRecord::where('parent_id',$record_id)->get();
            foreach($childs as $child){
                $this->comment("CHLD REC {$child->id}\tDeleting ...");
                $this->delete($child->id);
            }

            $record = FakeRecord::find($record_id);

            $model = '\\'.$record->model;
            $query = new $model;
            $query = $query->where('id',$record->record_id)->first();

            if(!$query){
                $record->delete();
                $this->norec++;
                return;
            }

            if(method_exists($model,'hardDelete')){
                $query->hardDelete();
            }
            else{
                $query->forceDelete();
            }
            $record->delete();
            $this->deleted++;
        }
        catch(\Exception $e){
            $this->error("REC: {$record_id} \t" . $e->getMessage());
            $this->errors++;
            return;
        }

        $this->comment("Deleted | " .
            "Model {$model}\t" .
            "Record {$record_id}");

    }
}
