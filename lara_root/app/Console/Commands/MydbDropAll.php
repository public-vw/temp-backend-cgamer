<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Str;

class MydbDropAll extends Command
{
    #TODO: Make --all and get table name, to better resolution
    protected $signature = 'mydb:drop {--y|yes} {--x|nobackup}';

    protected $description = 'Drop all tables';

    public function handle()
    {
        $dbName = config('database.connections.mysql.database');
        $dbUser = config('database.connections.mysql.username');
        $dbPass = config('database.connections.mysql.password');

        #TODO: Make force switch to pass this
        if(!$this->option('yes') && !$this->confirm("Are you sure you want to TRUNCATE all tables ?")){
            $this->comment('Canceled by user');
            return;
        }

        if(!$this->option('nobackup')){
            Artisan::call('mydb:backup');
        }

        $result = runShell("mysql -u '$dbUser' -p\"$dbPass\" " .
            "-Nse 'show tables' $dbName | " .
            "while read table; " .
                "do mysql -u $dbUser -p\"$dbPass\" " .
                "-e \"SET FOREIGN_KEY_CHECKS = 0; drop table \$table;\" $dbName; " .
            "done");
        $this->comment("DB \"Truncate All\" Process Finished By this result: ".$result);
    }
}
