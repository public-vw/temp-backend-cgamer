<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Permission;

class CreateStructurePermission extends Command
{
    protected $guards =      ['web', 'api'];
    protected $cruds =       ['view', 'edit', 'create', 'delete'];
    protected $territories = ['all', 'branch', 'own'];

    protected $signature = 'structure:permissions {model}';

    protected $description = 'Creates Permission for Specific Model';

    public function handle()
    {
        $model = $this->argument('model');

        foreach ($this->guards as $guard) {
            $this->addPermissions($guard,$model);
        }

        $this->comment(
            "Permissions for model {$model} created successfully."
        );
    }

    protected function addPermissions($guard,$model)
    {
        $table = model2table($model);

        $this->comment("=== guard: {$guard}");
        $this->comment("--- table: {$table}");

        foreach ($this->cruds as $crud) {
            foreach ($this->territories as $territory) {
                $perm = "{$table}.{$crud}.{$territory}";
                Permission::create(['guard_name' => $guard, 'name' => $perm]);
                $this->comment("permission: {$perm} created");
            }
        }
    }
}
