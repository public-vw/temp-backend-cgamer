<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Console\Tests\CheckAttachmentsOnArticlesClass;

class CheckAttachmentsOnArticles extends Command
{
    protected $signature = 'check:article:attachments {article_id?}';

    protected $description = 'Checks articles attachments if exists as a hard file or not';

    protected $report = [];

    public function handle()
    {
        $checker = new CheckAttachmentsOnArticlesClass($this->argument('article_id'));
        $checker->handle();
        $report = $checker->getReport();
        dump($report);
        return 0;
    }
}
