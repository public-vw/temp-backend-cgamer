<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class CreateStructureTransformer extends Command
{
    protected $signature = 'structure:transformer {model}';

    protected $description = 'Creates Transformer';

    public function handle()
    {
        $model = $this->argument('model');
        $model = Str::ucfirst($model);
        $model = str_replace(' ', '', $model);
        $snake = Str::camel($model);

        $dest = "Http/Transformers/{$model}Transformer.php";

        $content = file_get_contents(storage_path("templates/_Model_Transformer.php"));

        ## change _Model_
        $content = str_replace('_Model_',$model,$content);
        ## change _model_
        $content = str_replace('_model_',$snake,$content);
        ## removes comments
        $content = preg_replace('/^\#.*?\n/m','',$content);

        file_put_contents(app_path($dest),$content);

        $this->comment(
            "Custom Transformer created successfully: ".$dest
        );
    }
}
