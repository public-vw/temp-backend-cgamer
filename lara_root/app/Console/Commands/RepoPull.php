<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class RepoPull extends Command
{
    protected $signature = 'repo:pull {branch?} {--f|force}';

    protected $description = 'Pulls project from gitlab repository';

    public function handle()
    {
        $curr_branch = trim(runShell('git branch --show-current'));
        $branch = $this->argument('branch')? $this->argument('branch'):env('DEPLOY_BRANCH',$curr_branch);
        $force = $this->option('force') ?'--force':'';
        if($this->option('force')) $this->comment('Pull runs in force mode');

        $command = "git fetch origin $branch:$branch -u; git pull --set-upstream origin $branch $force";
        $this->comment('CMD: ' . $command);
        $ret = runShell($command,true);
        $this->comment('output: '.$ret['output']);
        $this->comment('errors: '.$ret['errors']);
        return 0;
    }
}
