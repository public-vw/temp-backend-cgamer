<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Str;

class MyuploadsRestore extends Command
{
    protected $signature = 'myup:restore {fileterm?} {--y|yes} {--d|drop}';

    protected $description = 'Restore last backup file of uploads';

    protected array $uploadsFolders = [
        'temp' => 'public/tmp',
        'perm' => 'public/uploads',
    ];
    protected string $backupFolder = 'DB_Backups';

    public function handle()
    {
        $this->comment("Restoring Uploads");

        if($this->hasArgument('fileterm')){
            $filename = $this->argument('fileterm');
            $lastFiles = runShell('cd '.base_path($this->backupFolder).';' .
                "find . -name \"ups_*{$filename}*.gz\" -type f -exec stat --format '%n' \"{}\" \; | sort -n | cut -d: -f2- | tail -3");

            if(empty(trim($lastFiles))) {
                $this->error("Backup file not found with term: {$filename}");
                return;
            }
        }else{
            $lastFiles = runShell('cd '.base_path($this->backupFolder).';' .
                "find . -name \"ups_*.gz\" -type f -exec stat --format '%n' \"{}\" \; | sort -n | cut -d: -f2- | tail -3");

            if(empty(trim($lastFiles))) {
                $this->error("Backup file not found");
                return;
            }
        }

        $files = explode("\n",trim($lastFiles));
        $filecode = explode('_',$files[0])[3];

        if(!$this->option('yes') && !$this->confirm("Are you sure you want to restore [ $filecode ] uploads ?")){
            $this->comment('Canceled by user');
            return;
        }

        if($this->option('drop')){
            $this->comment('Dropping All Uploaded Files will drop');
        }

        $this->comment("Restoring [$filecode] as uploads");

        foreach($files as $file){
            $filekey = Str::substr(explode('_',$file)[4],0,-7);

            $upFolder = $this->uploadsFolders[$filekey];

            $command = '';

            if($this->option('drop')){
                $command .= 'cd '.base_path($upFolder).';';
                $command .= "rm -fr *;";
            }
            $command .= 'cd '.base_path($this->backupFolder).';';
            $command .= "cp {$file} ".base_path($upFolder).";";
            $command .= 'cd '.base_path($upFolder).';';
            $command .= "tar xzf {$file};";
            $command .= "rm -f {$file};";

            $x = runShell($command);

            $this->comment("Folder done: {$upFolder}");
        }

        $this->comment("Uploads Restore Process Finished");
    }
}
