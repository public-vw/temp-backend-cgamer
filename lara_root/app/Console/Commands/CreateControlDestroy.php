<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class CreateControlDestroy extends Command
{
    protected $signature = 'control:destroy';

    protected $description = 'Destroys Constructor functionality';

    protected $commands = [
        'CreateStructure*',
    ];


    public function handle()
    {
        $base = app_path();

        # purge commands
        foreach($this->commands as $command){
            runShell("rm -fr ".app_path('Console/Commands')."/{$command}");
        }
        $this->comment("Commands purged successfully");

        # purge templates
        runShell("rm -fr ".storage_path('templates'));
        $this->comment("Templates purged successfully");

        # purge envoy
        runShell("rm -fr ".base_path('Envoy.blade.php'));
        $this->comment("Envoy purged successfully");
    }
}
