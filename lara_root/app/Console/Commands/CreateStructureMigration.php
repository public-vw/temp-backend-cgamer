<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class CreateStructureMigration extends Command
{
    protected $signature = 'structure:migration {model}';

    protected $description = 'Creates Migration';

    public function handle()
    {
        $model = $this->argument('model');
        $model = Str::ucfirst($model);
        $model = str_replace(' ', '', $model);
        $table = model2table($model);
        $Table = str_replace(' ','',ucwords(str_replace('_',' ',$table)));
        $datetime = date('Y_m_d_His');


        $dest = "migrations//{$datetime}_create_{$table}_table.php";

        $content = file_get_contents(storage_path("templates/_migration_.php"));

        //## change _Table_
        $content = str_replace('_Table_',$Table,$content);
        //## change _table_
        $content = str_replace('_table_',$table,$content);
        ## removes comments
        $content = preg_replace('/^\#.*?\n/m','',$content);

        file_put_contents(database_path($dest),$content);

        $this->comment(
            "Migration created successfully: {$dest}"
        );
    }
}
