<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class CsfClearLog extends Command
{
    protected $signature = 'csf:clear';

    protected $description = 'Clears Log of CSF';

    public function handle()
    {
        $log_file = storage_path("logs/csf.log");
        File::put($log_file,'');
        return 0;
    }
}
