<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class RepoStash extends Command
{
    protected $signature = 'repo:stash';

    protected $description = 'Rollbacks local changes of project';

    public function handle()
    {
        $command = "git stash";
        $this->comment('CMD: ' . $command);
        $ret = runShell($command,true);
        $this->comment('output: '.$ret['output']);
        $this->comment('errors: '.$ret['errors']);
        return 0;
    }
}
