<?php

namespace App\ViewComposers;

use Illuminate\View\View;

use App\Models\Setting;
//use App\Models\Branding;
//use App\Models\Linklist;

class AllViewsComposer
{
//    protected $category_menu;

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
//        $brandings = Branding::getArray();

        $view_folders = explode('.',$view->getName());
        $view_root = array_shift($view_folders);

        $view->with('VIEW_ROOT',$view_root)
            ->with('VIEW_FOLDERS',$view_folders)
//                ->withBrandings($brandings)
        ;
    }
}
