<?php

namespace App\ViewComposers;

use App\Models\MenuPlaceholder;
use App\Models\Panel;
use Illuminate\View\View;

class AdminPanelComposer
{
    public function compose(View $view)
    {
        $menus = Panel::where('title', 'admin')->first()->menu_placeholders()->get()->keyBy('title');
        $user = auth()->user();

        $view->withMenus($menus ?? [])->withUser($user);
    }
}
