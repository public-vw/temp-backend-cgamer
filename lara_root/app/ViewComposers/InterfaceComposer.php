<?php

namespace App\ViewComposers;

use App\Models\Fact;
use App\Models\Panel;
use Illuminate\View\View;

class InterfaceComposer
{
    public function compose(View $view)
    {
        $menus = Panel::where('title', 'interface')->first()->menu_placeholders()->get()->keyBy('title');
        $fact = Fact::selectOne()->sentence;

        $view
            ->withMenus($menus ?? [])
            ->withFact($fact);
    }
}
