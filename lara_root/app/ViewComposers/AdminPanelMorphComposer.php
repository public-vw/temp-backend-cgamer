<?php

namespace App\ViewComposers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\View\View;

class AdminPanelMorphComposer
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function compose(View $view)
    {
        if(!$this->request->has('morph-type')) return;

        $view->withMorph([
                'id'   => $this->request->get('morph-id'),
                'type' => Str::singular($this->request->get('morph-type')),
            ]);
    }
}
