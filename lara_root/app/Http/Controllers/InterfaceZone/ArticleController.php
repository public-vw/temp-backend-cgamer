<?php

namespace App\Http\Controllers\InterfaceZone;

use App\Http\Transformers\ArticleTransformer;
use App\Models\Article;
use App\Models\ArticleCategory;

class ArticleController extends _Controller
{
    public function articleListView()
    {
        return view("{$this->view}.pages.articles_list.index");
    }

    public function articleSingleView(string $categories, string $articleSlug)
    {
        $allowCheckUnActives = (auth()->check() && auth()->user()->hasRole('admin@web'));

        if($articleId = cache('article-full-url-to-id.'.crc32($categories.'/'.$articleSlug),false)){
//            Cache::forget('article-full-url-to-id.'.crc32($categories.'/'.$articleSlug));
            $article = Article::find($articleId);
            abort_if(!$allowCheckUnActives && $article->status != config_key('enums.articles_status', 'active'), 404);
            return view("{$this->view}.pages.articles_single.index")->withArticle($article);
        }

        $categoriesArray = explode('/',$categories);

        try{
            $category = new ArticleCategory;
            foreach($categoriesArray as $categorySlug){
                $category = $category->where('slug',$categorySlug);
                if(next($categoriesArray) == true){
                    $category = $category->first();
                    abort_if(!$allowCheckUnActives && (
                        !$category || $category->status != config_key('enums.article_categories_status', 'active')
                    ), 404);
                    $category = $category->children();
                }
            }
            $category = $category->first();
            abort_if(!$allowCheckUnActives && (
                !$category || $category->status != config_key('enums.article_categories_status', 'active')
            ), 404);

            $article = $category->articles()
                ->where('status',config_key('enums.articles_status', 'active'))
                ->where('slug',$articleSlug)->first();
        }
        catch (\Exception $e){
            getFormedError($e,'Interface Article Error');
            abort(404);
        }

        abort_if(!$article,404);

        cache(['article-full-url-to-id.'.crc32($categories.'/'.$articleSlug) => $article->id]);

        return view("{$this->view}.pages.articles_single.index")->withArticle($article);
    }

    # shows all articles
    public function articleList()
    {
        $items = Article::
        where('status', config_key('enums.articles_status', 'active'));

        return $this->responseByCursorFractal($items, new ArticleTransformer());
    }


        # shows one article, with id
    public function articleSingle(Article $article)
    {
        abort_if($article->status != config_key('enums.articles_status', 'active'), 404);
        return view("{$this->view}.pages.articles_single.index")->withArticle($article);
    }

}
