<?php


namespace App\Http\Controllers\InterfaceZone;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use League\Fractal\Pagination\Cursor;
use League\Fractal\TransformerAbstract;
use Spatie\Fractalistic\Fractal;
use \Illuminate\Http\JsonResponse;

abstract class _Controller extends Controller
{
    protected $view;
    protected $perPages = [5, 10, 25, 50];

    public function __construct()
    {
        $this->view = config("routes.interface.view",'interface');
    }

    protected function responseByCursorFractal(Builder $builder, TransformerAbstract $fractalManager): JsonResponse
    {
        $request = Request::capture();
        $paginator = $builder->cursorPaginate($this->perPages[$request->get('per_page_mode',0)] ?? $this->perPages[0]);
        $cursor = new Cursor(
            null,
            $paginator->previousPageUrl(),
            $paginator->nextPageUrl(),
            $builder->count(),
        );
        $fractal = Fractal::create()
            ->collection($paginator, $fractalManager)
            ->withCursor($cursor);

        return response()->json( $fractal );

    }
}
