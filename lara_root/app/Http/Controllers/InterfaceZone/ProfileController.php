<?php

namespace App\Http\Controllers\InterfaceZone;

use App\Models\User;
use Illuminate\Http\Request;
use Jorenvh\Share\Share;

class ProfileController extends _Controller
{
    protected $model = User::class;

    public function index(Request $request, User $user, string $nickname = null)
    {
        $share = new Share;
        $shareComponent = $share->page($request->url(),__('share.author.title'),[],'','')
            ->facebook()->twitter()->telegram()
//            ->linkedin()
//            ->whatsapp()
//            ->reddit()
//            ->pinterest()
        ;

        return view($this->view.'.pages.profile.index')
            ->withShareComponent($shareComponent)
            ->withUser($user);
    }
}
