<?php

namespace App\Http\Controllers\InterfaceZone;

use App\Models\ArticleCategory;

class RouteController extends _Controller
{
    public function index()
    {
        $starter_category = ArticleCategory::where('slug','games')->first();
        $categories = ArticleCategory::where('slug','games')->first()->children()
            ->where('status',config_key('enums.article_categories_status','active'))
            ->orderBy('order')->get();

        return view("{$this->view}.pages.homepage.index")
            ->withStarterCategory($starter_category)
            ->withCategories($categories);
    }

    public function terms()
    {
        return view("{$this->view}.pages.terms.index");
    }

    public function termsEuropean()
    {
        return view("{$this->view}.pages.terms.european.index");
    }

    public function termsNonEuropean()
    {
        return view("{$this->view}.pages.terms.non_european.index");
    }

    public function aboutus()
    {
        return view("{$this->view}.pages.aboutus");
    }
}
