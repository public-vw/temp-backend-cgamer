<?php


namespace App\Http\Controllers\InterfaceZone\API;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use League\Fractal\Pagination\Cursor;
use League\Fractal\TransformerAbstract;
use Spatie\Fractalistic\Fractal;
use \Illuminate\Http\JsonResponse;

abstract class _Controller extends Controller
{
    protected $perPages = [5, 10, 25, 50];

    protected function responseByCursorFractal(Builder $builder, TransformerAbstract $fractalManager): JsonResponse
    {
        $request = Request::capture();
        $paginator = $builder->cursorPaginate($this->perPages[$request->get('per_page_mode',0)] ?? $this->perPages[0]);
        $cursor = new Cursor(
            null,
            $this->getCursorParameter($paginator->previousPageUrl()),
            $this->getCursorParameter($paginator->nextPageUrl()),
            $builder->count(),
        );
        $fractal = Fractal::create()
            ->collection($paginator, $fractalManager)
            ->withCursor($cursor);

        return response()->json( $fractal );

    }
    protected function responseByFirstFractal(?Model $item, TransformerAbstract $fractalManager)
    {
        if(is_null($item)){
            return response('Requested ID is not available',421);
        }
//        dd(get_class($item));

        $fractal = Fractal::create()
            ->collection($item, $fractalManager);

        return response()->json( $fractal );

    }

    private function getCursorParameter(?string $cursorUrl): ?string{
        return preg_match('/\?cursor=(.*)/', $cursorUrl, $out) ? $out[1] : null;
    }
}
