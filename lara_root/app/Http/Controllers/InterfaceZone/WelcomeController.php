<?php

namespace App\Http\Controllers\InterfaceZone;

use App\Http\Controllers\Controller;
use \Illuminate\Support\Facades\Session;
use PragmaRX\Google2FA\Google2FA;

class WelcomeController extends Controller
{
    public function index($key = ''){
        $branch = trim(runShell('git branch --show-current'));

        $details = ($this->validateToken($key)) ?
            runShell('git rev-list --format="%s | %ar" --max-count=2 '.$branch):'';

        $version = runShell('git rev-list --max-count=1 '.$branch);

        return view('basic_interface.home')
            ->with('branch',$branch)
            ->with('details',$details)
            ->with('version',$version);
    }

    public function upgrade($key = ''){
        if($this->validateToken($key)) runShell('git pull');
        return redirect()->route('homepage');
    }

    private function validateToken($token){
        if(!Session::has('dev_token') || (time() - Session::get('dev_token') > 60 || Session::get('dev_token_cnt')>=50 )){
            Session::forget('dev_token');
            Session::put('dev_token_cnt',0);

            $google2fa = new Google2FA();
            $valid = $google2fa->verifyKey(config('app.gcode'), $token);
            if(!$valid) return false;
        }
        Session::put('dev_token_cnt',Session::get('dev_token_cnt') + 1);
        Session::put('dev_token',time());
        return true;
    }

}
