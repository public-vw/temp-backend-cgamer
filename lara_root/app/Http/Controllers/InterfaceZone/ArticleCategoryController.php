<?php

namespace App\Http\Controllers\InterfaceZone;

use App\Http\Transformers\ArticleTransformer;
use App\Models\ArticleCategory;

class ArticleCategoryController extends _Controller
{
    public function articleCategoryListView()
    {
        return view("{$this->view}.pages.article_categories_list.index");
    }

    public function articleCategorySingleView(string $categories)
    {
        if($categoryId = cache('category-full-url-to-id.'.crc32($categories),false)){
//            Cache::forget('category-full-url-to-id.'.crc32($categories));
            $articleCategory = ArticleCategory::find($categoryId);
            if(auth()->guest() || (!auth()->user()->hasRole('admin@web') && !auth()->user()->hasPermissionTo('articles.see_inactives'))){
                abort_if($articleCategory->status != config_key('enums.article_categories_status', 'active'), 404);
            }
            return view("{$this->view}.pages.article_category.index")->withCategory($articleCategory);
        }

        $categoriesArray = explode('/',$categories);

        try{
            $category = new ArticleCategory;
            foreach($categoriesArray as $categorySlug){
                $category = $category->where('slug',$categorySlug);
                if(next($categoriesArray) == true){
                    $category = $category->first();
                    abort_if(!$category , 405);
                    if(auth()->guest() || (!auth()->user()->hasRole('admin@web') && !auth()->user()->hasPermissionTo('articles.see_inactives'))){
                        abort_if($category->status != config_key('enums.article_categories_status', 'active'), 404);
                    }
                    $category = $category->children();
                }
            }
            $category = $category->first();
            abort_if(!$category , 404);
            if(auth()->guest() || (!auth()->user()->hasRole('admin@web') && !auth()->user()->hasPermissionTo('articles.see_inactives'))){
                abort_if($category->status != config_key('enums.article_categories_status', 'active'), 404);
            }
        }
        catch (\Exception $e){
            getFormedError($e,'Interface Category Article Error');
            abort(503);
        }

        cache(['category-full-url-to-id.'.crc32($categories) => $category->id]);

        return view("{$this->view}.pages.article_category.index")->withCategory($category);
    }

    # shows all article categories
    public function articleCategoryList()
    {
        $items = ArticleCategory::
        where('status', config_key('enums.article_categories_status', 'active'));

        return $this->responseByCursorFractal($items, new ArticleTransformer());
    }


        # shows one article category, with id
    public function articleCategorySingle(ArticleCategory $articleCategory)
    {
        abort_if($articleCategory->status != config_key('enums.article_categories_status', 'active'), 404);
        return view("{$this->view}.pages.article_category.index")->withCategory($articleCategory);
    }

}
