<?php

namespace App\Http\Controllers\Auth\Oauth;

class FacebookController extends _OauthController
{
    protected string $driver = 'facebook';
    protected string $db_field = 'facebook_id';

    public function deauthorize(){
        return redirect()->route('public.homepage');
    }

    public function deletemydata(){
        return redirect()->route('public.homepage');
    }
}
