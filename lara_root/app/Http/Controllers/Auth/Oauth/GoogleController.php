<?php

namespace App\Http\Controllers\Auth\Oauth;

class GoogleController extends _OauthController
{
    protected string $driver = 'google';
    protected string $db_field = 'google_id';
}
