<?php

namespace App\Http\Controllers\Auth\Oauth;

class TelegramController extends _OauthController
{
    protected string $driver = 'telegram';
    protected string $db_field = 'telegram_id';
}
