<?php

namespace App\Http\Controllers\Auth\Oauth;

class TwitchController extends _OauthController
{
    protected string $driver = 'twitch';
    protected string $db_field = 'twitch_id';
}
