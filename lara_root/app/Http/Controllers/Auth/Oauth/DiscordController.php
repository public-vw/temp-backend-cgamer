<?php

namespace App\Http\Controllers\Auth\Oauth;

class DiscordController extends _OauthController
{
    protected string $driver = 'discord';
    protected string $db_field = 'discord_id';
}
