<?php

namespace App\Http\Controllers\Auth\Oauth;

class AppleController extends _OauthController
{
    protected string $driver = 'apple';
    protected string $db_field = 'apple_id';
}
