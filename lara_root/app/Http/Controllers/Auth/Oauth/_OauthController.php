<?php

namespace App\Http\Controllers\Auth\Oauth;

use App\Event\UserEntered;
use App\Http\Controllers\Controller;
use App\Models\Attachment;
use App\Models\Request;
use App\Models\Role;
use App\Models\User;
use App\Models\UserOauth;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;

abstract class _OauthController extends Controller
{
    protected string $driver;
    protected string $db_field;

    public function redirect(Request $request)
    {
        session(['url_callback' => url()->previous()]);
        return Socialite::driver($this->driver)->redirect();
    }

    public function callback()
    {
        return auth()->check()?$this->connect():$this->registerLogin();
    }

    protected function registerLogin()
    {
        $dbUser = null;

        try {
            $user = Socialite::driver($this->driver)->user();
            $userOauth = UserOauth::where($this->db_field, $user->getId())->first();
            if ($userOauth) {
                Auth::login($userOauth->user);
                event(new UserEntered('Logged In', $userOauth->user, $this->driver));
                return redirect(url(session('url_callback',route('public.homepage'))));
            }

            if (isset($user->email)) {
                $dbUser = User::where('email', $user->getEmail())->first();
            }

            if ($dbUser) {
                event(new UserEntered('Logged In By New Channel', $dbUser, $this->driver));
            } else {
                $name = $user->name ?? $user->nickname ?? 'RND-'.Str::random(10);
                $nickname = $user->nickname ?? $user->name ?? 'RND-'.Str::random(10);

                $dbUser = User::create([
                    'name'               => $name,
                    'email'              => $user->email ?? null,
                    'nickname'           => Str::slug($nickname),
                    'username'           => Str::slug($nickname) . rand(100, 999),
                    'ref_code'           => Str::random(10),
                    'mobile'             => null,
                    'mobile_verified_at' => null,
                    'random_color'       => (string)rand(0, count(config_keys_all('enums.random_colors')) - 1),
                    'telegram_uid'       => null,
                    'referer_id'         => null,
                    'email_verified_at'  => now(),
                    'password'           => Hash::make(Str::random(20)),
                    'remember_token'     => Str::random(10),
                    'status'             => (string)config_key('enums.users_status', 'active'),
                ]);
                event(new UserEntered('Registration', $dbUser, $this->driver));
                @$dbUser->badges()->attach(1);
                Role::assignRoleByName($dbUser, 'author', 'web');
                if (isset($user->avatar)) {
                    Attachment::uploadFileByURL($user->avatar, 'Avatar', $dbUser, 'jpg');
                }
            }

            UserOauth::updateOrCreate(
                ['user_id' => $dbUser->id],
                [$this->db_field => $user->getId()]
            );

            Auth::login($dbUser);

            return redirect(url(session('url_callback',route('public.homepage'))))
                ->withLoggenIn(true);

        } catch (\Exception $e) {
            getFormedError($e, 'Oauth ' . Str::ucfirst($this->driver) . ' Issue');
        }
        //failed to login/register with oauth2
        return redirect(url(session('url_callback',route('public.homepage'))))
            ->withLoggenIn(false);
    }

    protected function connect()
    {
        $dbUser = auth()->user();

        try {
            $user = Socialite::driver($this->driver)->user();
            $userOauth = UserOauth::where($this->db_field, $user->getId())->first();
            if (!$userOauth) {
                UserOauth::updateOrCreate(
                    ['user_id' => $dbUser->id],
                    [$this->db_field => $user->getId()]
                );
                event(new UserEntered('Connecting', $dbUser, $this->driver));
                if (is_null($dbUser->lastImage('avatar')) && isset($user->avatar)) {
                    Attachment::uploadFileByURL($user->avatar, 'Avatar', $dbUser, 'jpg');
                }
            }
            return redirect(url(session('url_callback',route('client.profile.index'))));
        } catch (\Exception $e) {
            getFormedError($e, 'Oauth ' . Str::ucfirst($this->driver) . ' Issue');
        }

        return redirect(url(session('url_callback',route('client.profile.index'))))
            ->with(['alert'=>'connection failed']);
    }
}
