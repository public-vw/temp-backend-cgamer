<?php

namespace App\Http\Controllers\Auth\Oauth;

class TwitterController extends _OauthController
{
    protected string $driver = 'twitter';
    protected string $db_field = 'twitter_id';
}
