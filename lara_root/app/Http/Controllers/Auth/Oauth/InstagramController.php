<?php

namespace App\Http\Controllers\Auth\Oauth;

use App\Models\Request;
use Laravel\Socialite\Facades\Socialite;

class InstagramController extends _OauthController
{
    protected string $driver = 'instagrambasic';
    protected string $db_field = 'instagram_id';

    public function redirect(Request $request)
    {
        return Socialite::driver('instagrambasic')
            ->setScopes(['user_profile'])
//            ->setScopes(['user_profile','user_media'])
            ->redirect();
    }

    public function deauthorize(){
        return redirect()->route('public.homepage');
    }

    public function deletemydata(){
        return redirect()->route('public.homepage');
    }
}
