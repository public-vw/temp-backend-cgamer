<?php

namespace App\Http\Controllers\Auth\Oauth;

class YoutubeController extends _OauthController
{
    protected string $driver = 'youtube';
    protected string $db_field = 'youtube_id';
}
