<?php

namespace App\Http\Controllers\Auth\Oauth;

class SteamController extends _OauthController
{
    protected string $driver = 'steam';
    protected string $db_field = 'steam_id';
}
