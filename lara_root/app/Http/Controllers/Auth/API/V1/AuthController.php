<?php

namespace App\Http\Controllers\Auth\API\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\API\AuthLoginRequest;
use App\Http\Requests\Auth\API\AuthRegisterRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    public function register(AuthRegisterRequest $request): array
    {
        $requestArr = $request->validated();
        $requestArr['username'] = Str::random(5).Str::substr($request->mobile,-3);

        $user = User::create($requestArr);
        return [
            'api_token' => $user->createToken($request->device_name)->plainTextToken
        ];
    }

    #TODO: for who has two step verification, needs application single password
    public function login(AuthLoginRequest $request): array
    {
        $user = User::where('mobile', $request->mobile)->first();

        if (! $user || ! Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages([
                'The provided credentials are incorrect.',
            ]);
        }

        return [
            'api_token' => $user->createToken($request->device_name)->plainTextToken
        ];
    }

    public function logout(Request $request)
    {
        $deviceName = $request->user()->currentAccessToken()->name;

        $request->user()->currentAccessToken()->delete();
        return [
            'device_name' => $deviceName,
            'message' => 'Logged out successfully',
        ];
    }

    public function logoutAll(Request $request)
    {
        $request->user()->tokens()->delete();
        return [
            'message' => 'All devices logged out successfully',
        ];
    }
}
