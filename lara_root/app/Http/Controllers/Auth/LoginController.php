<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    protected $redirectTo = RouteServiceProvider::HOME;

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        return 'username';
    }

    protected function credentials(Request $request)
    {
        $req = $request->only($this->username(), 'password');

        //TODO: use mobile verification function here
        if(is_numeric($req[$this->username()])){
            $this->change_key($req, $this->username(),'mobile');
        }
        elseif (filter_var($req[$this->username()], FILTER_VALIDATE_EMAIL)) {
            $this->change_key($req, $this->username(),'email');
        }

        return $req;
    }

    private function change_key(&$arr, $oldkey, $newkey){
        $arr[$newkey] = $arr[$oldkey];
        unset($arr[$oldkey]);
    }

    protected function authenticated(Request $request, User $user)
    {
//        $user = $this->guard()->user();

        if($user->use_two_factor_auth){
            session(['auth2.user' => $user]);
            $this->guard()->logout();
            return redirect()->route('auth.auth2');
        }
    }
}
