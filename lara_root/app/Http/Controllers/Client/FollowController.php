<?php

namespace App\Http\Controllers\Client;

use App\Models\Follow;
use App\Models\User;
use Illuminate\Http\Request;

class FollowController extends _Controller
{
    protected $model = Follow::class;

    public function follow(User $user)
    {
        $curr_user = auth()->user();
        if($curr_user->id == $user->id) { // user trying follow himself
            return back();
        }

        if($curr_user->isFollowing($user)){
            return back();
        }

        $user->followings()->attach($curr_user->id);

        return back();
    }

    public function unfollow(User $user)
    {
        $curr_user = auth()->user();
        if($curr_user->id == $user->id) { // user trying follow himself
            return back();
        }

        if(!$curr_user->isFollowing($user)){
            return back();
        }

        $user->followings()->detach($curr_user->id);

        return back();
    }
}
