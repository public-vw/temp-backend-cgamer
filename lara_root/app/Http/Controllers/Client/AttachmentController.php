<?php

namespace App\Http\Controllers\Client;

use App\Models\Attachment;
use App\Models\AttachmentType;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AttachmentController extends _Controller
{
    protected $model = Attachment::class;

    public function uploadImageData(Request $request, string $type)
    {
        $territory = $this->checkPermission('create');

        request()->validate([
            'file'  => 'required|mimes:jpg,jpeg,png,gif,webp|max:20480',
        ]);

        $type = AttachmentType::where('slug', $type)->firstOrFail(['folder', 'id']);

        $image = $request->file('file');
        $fileext = $image->getMimeType();
        preg_match('/image\/(\w+)/', $fileext, $fileext);
        $fileext = $fileext[1];

        $md5 = md5($image->get());
        $oldAttachment = Attachment::where('storage_disk','permanent')->where('md5',$md5)->first();

        if($oldAttachment){
            $attachment = Attachment::copyAs($type, $oldAttachment);
            return response()->json(encrypt($attachment->id));
        }
        else{
            $filename = $type->folder . '/' . Carbon::now()->format('Y-m/d-His-') . rand(101, 999);
            $filesize = $request->size ?? null;
            $filepath = $filename . '.' . $fileext;

            $stored = Storage::disk('temporary')->put($filepath, $image->get());
            if ($stored) {
                $attachment = Attachment::add($type, $filename, $fileext, $filesize);
                return response()->json(encrypt($attachment->id));
            }
        }

        return response('failed', 501);
    }

}
