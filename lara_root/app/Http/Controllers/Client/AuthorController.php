<?php

namespace App\Http\Controllers\Client;

use App\Models\Author;

class AuthorController extends _Controller
{
    protected $model = Author::class;

    public function suspended()
    {
        return view("{$this->view}.author_suspended.index");
    }

}
