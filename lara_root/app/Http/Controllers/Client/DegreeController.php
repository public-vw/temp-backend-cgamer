<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;

class DegreeController extends Controller
{
    protected $view = 'interface_dark.clients';

    public function index()
    {
        return view("{$this->view}.upload-degrees.index");
    }

}
