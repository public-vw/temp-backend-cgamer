<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Support\GetRates\GasFee;
use App\Support\GetRates\IrtRate;

class ExchangeController extends Controller
{
    protected $view = 'interface_dark';

    # shows exchange tools page, no auth needed in routes
    public function exchange()
    {
        return view("{$this->view}.clients.exchange.index");
    }
    public function updateParameters()
    {
        $change = rand(0,100) / 100 / 100;

        $params = [
            'irt2usd' => (new IrtRate())->irtToUSD(),
            'usd2weth' => 4100,
            'gasfee' => (new GasFee())->get(),
        ];

        $params['ourcost'] = $params['gasfee'] / (rand(29,31) / 10);
        $params['ourcost'] = ceil($params['ourcost'] * 100 ) / 100;

//        ############ I should work on this
//        # fake calculate on params
//        $params = array_map(function($val) use ($change){
//            $val -= $val * $change;
//            return $val;
//        },$params);
//        ##################################


        $params['saving'] = $params['gasfee'] - $params['ourcost'];

        # normalize params
        $params = array_map(function($val) use ($change){
            return ceil($val * 100) / 100;
        },$params);

        return response()->json($params);
    }

    # shows charge wallet tool page
    public function chargeWallet()
    {
        return view("{$this->view}.clients.charge_wallet.index");
    }

}
