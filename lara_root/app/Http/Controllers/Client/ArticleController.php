<?php

namespace App\Http\Controllers\Client;

use App\Event\ArticleCreated;
use App\Event\ArticleContentChanged;
use App\Event\ArticleStatusChanged;
use App\Http\Requests\Client\SearchArticlesRequest;
use App\Http\Transformers\Select2ArticleTransformer;
use App\Http\Transformers\Serializers\Select2Serializer;
use App\Jobs\ArticleActivated;
use App\Models\Article;
use App\Models\Attachment;
use App\Models\Hyperlink;
use App\Models\User;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Spatie\Fractal\Fractal;

class ArticleController extends _Controller
{
    protected $model = Article::class;

    # shows new article
    public function articleSingleNew()
    {
        $territory = $this->checkPermission('view');

        $author = auth()->user()->author ?? auth()->user()->author()->create(['article_limit' => 3]);

        if ($author->status == config_key('enums.authors_status','suspend')) {
            return redirect()->route('client.authors.suspended');
        }

        if ($author->article_limit > 0) {
            $waitingArticles = $author->articles()
                ->whereIn('status', config_keys('enums.articles_status', ['draft','requested']))
                ->count();

            if ($waitingArticles >= $author->article_limit) {
                return redirect()->route('client.articles.limit');
            }

            $checkingArticles = $author->articles()
                ->whereIn('status', config_keys('enums.articles_status', ['checking','rejected']))
                ->count();

            if ($checkingArticles >= 2 * $author->article_limit) {
                return redirect()->route('client.articles.limit');
            }
        }

        return view("{$this->view}.articles_single.index");
    }

    public function reachedLimit()
    {
        return view("{$this->view}.articles_limit.index");
    }

    # shows preview of article
    public function articleSinglePreview(Article $article)
    {
        $territory = $this->checkPermission('view');

        $curr_user = auth()->user();
        if (
            !auth()->user()->hasRole('admin@web')
            && $article->author->id != $curr_user->author->id
            && !$article->author->user->isChildOf($curr_user)
        ) {
            abort(403, 'Access Denied');
        }

        $seoUrl = $article->seoUrl();

        if($seoUrl) return redirect($seoUrl);

        return view("interface_dark.pages.articles_single.index")
            ->withArticle($article)
            ->withPreview(true);
    }

    # shows edit article
    public function articleSingleEdit(Article $article)
    {
        $territory = $this->checkPermission('edit');

        abort_if(!auth()->user()->hasRole('admin@web')
            && $article->author->user->isSameAsCurrent()
            && in_array($article->status, config_keys('enums.articles_status', ['checking', 'paused']))
            , 403, 'Access Denied');

        abort_if(!auth()->user()->hasRole('admin@web')
            && $article->author->user->isChildOfCurrent()
            && in_array($article->status, config_keys('enums.articles_status', ['seo-ready']))
            , 403, 'Access Denied');

        if ($article->author->user->isChildOfCurrent()
            && in_array($article->status, config_keys('enums.articles_status', ['draft', 'requested']))
        ) {
            $article->update([
                'status' => config_key('enums.articles_status', 'checking'),
            ]);
        }

        return view("{$this->view}.articles_single.index")->withArticle($article);
    }

    # save article as draft
    public function articleSingleSave(Request $request)
    {
        $territory = $this->checkPermission('create');

        $curr_user = auth()->user();
        $author = $curr_user->author;

        $article = Article::find((int)$request->id);
        if (!$article) {
            $article = Article::create([
                'master_id'    => null,
                'author_id'    => $author->id,
                'category_id'  => null,
                'heading'      => $request->get('header'),
                'reading_time' => null,
                'content'      => $this->sanitizeContentForDB($request->get('body')),
                'status'       => config_key('enums.articles_status', 'draft'),
            ]);
            event(new ArticleCreated($curr_user, $article));
        } else {
            if (
                !auth()->user()->hasRole('admin@web')
                && $article->author->id != $curr_user->author->id
                && !$article->author->user->isChildOf($curr_user)
            ) {
                abort(403, 'Access Denied');
            }
            if ($article->master_id // means it is review version, not the master one.
                || (!in_array( // means it not activated previously, and can be changed
                    $article->status,
                    config_keys('enums.articles_status', ['active', 'paused']),
                ))
                || auth()->user()->hasRole('admin@web') // admin can update active articles without revision
            ) {
                $article->update([
                    'heading'      => $request->get('header'),
                    'reading_time' => null,
                    'content'      => $this->sanitizeContentForDB($request->get('body')),
                ]);
            } else {

                $article = Article::where([
                    'master_id' => $article->id,
                    'slug'      => $article->slug,
                ])->first();
                $article = $article ?? Article::create([
                    'master_id' => $article->id,
                    'slug'      => $article->slug,

                    'author_id'    => $author->id,
                    'category_id'  => $article->category_id,
                    'heading'      => $request->get('header'),
                    'reading_time' => null,
                    'content'      => $this->sanitizeContentForDB($request->get('body')),
                    'status'       => config_key('enums.articles_status', 'draft'),
                ]);
                event(new ArticleContentChanged($curr_user, $article));
            }
        }

        $this->getHeaderImageData($request);

        $this->connectAttachments($article);
        $this->connectHyperlinks($article);

        return response()->json($article->id);
    }

    # save article as requested
    public function articleChangeStatus(Request $request, string $status)
    {
        $territory = $this->checkPermission('edit');

        $curr_user = auth()->user();

        $article = Article::find((int)$request->id);
        abort_if(!$article, 404);
        abort_if(!in_array($status, ['requested', 'ready','checking', 'rejected','seo-ready','active']), 403, 'You Are Not Allowed');
        abort_if($status == 'active' && !in_array($article->status, config_keys('enums.articles_status', [
                'seo-ready',
            ])), 403);
        abort_if($status == 'checking' && !in_array($article->status, config_keys('enums.articles_status', [
                'requested',
            ])), 403);
        abort_if($status == 'requested' && !in_array($article->status, config_keys('enums.articles_status', [
                'draft', 'rejected',
            ])), 403);
        abort_if($status == 'ready' && !in_array($article->status, config_keys('enums.articles_status', [
                'checking',
            ])), 403);
        abort_if($status == 'rejected' && !in_array($article->status, config_keys('enums.articles_status', [
                'checking','ready',
            ])), 403);
        abort_if($status == 'seo-ready' && !in_array($article->status, config_keys('enums.articles_status', [
                'ready',
            ])), 403);

        abort_if($status == 'requested' && !(auth()->user()->hasRole('admin@web') || auth()->user()->hasRole('author_supervisor@web')), 403);
        abort_if($status == 'ready' && !(auth()->user()->hasRole('admin@web') || auth()->user()->hasRole('author_supervisor@web')), 403);
        abort_if($status == 'rejected' && !(auth()->user()->hasRole('admin@web') || auth()->user()->hasRole('author_supervisor@web')), 403);
//        abort_if($status == 'requested' && (!auth()->user()->hasRole('admin@web') || (auth()->user()->hasRole('author_supervisor@web') && !$article->author->user->isChildOfCurrent())), 403);
//        abort_if($status == 'ready' && (!auth()->user()->hasRole('admin@web') || (auth()->user()->hasRole('author_supervisor@web') && !$article->author->user->isChildOfCurrent())), 403);
//        abort_if($status == 'rejected' && (!auth()->user()->hasRole('admin@web') || (auth()->user()->hasRole('author_supervisor@web') && !$article->author->user->isChildOfCurrent())), 403);

        abort_if($status == 'seo-ready' && !auth()->user()->hasRole('admin@web'), 403);
        abort_if($status == 'active' && !auth()->user()->hasRole('admin@web'), 403);

        $oldStatus = $article->status;
        $article->update([
            'status' => config_key('enums.articles_status', $status),
        ]);
        if($status == 'active') ArticleActivated::dispatch($article);
        event(new ArticleStatusChanged($curr_user, $article,$oldStatus));

    }

    protected array $attachments = [];
    protected array $hyperlinks = [];

    protected function sanitizeContentForDB(string $content): string
    {
        $content = trim($content, " \n\t\r");

        $content = $this->packImages($content);
        $content = $this->packHrefs($content);

        return $content;
    }

    protected function connectAttachments(Article $article)
    {
        foreach ($this->attachments as $attachment) {
            $attachment->fill([
                'attachmentable_type' => array_search(Article::class, Relation::$morphMap) ?: null,
                'attachmentable_id'   => $article->id ?? null,

            ]);
            $attachment->save();
        }
    }

    protected function connectHyperlinks(Article $article)
    {
        foreach ($this->hyperlinks as $hyperlink) {
            $hyperlink->fill([
                'hyperlinkable_type' => array_search(Article::class, Relation::$morphMap) ?: null,
                'hyperlinkable_id'   => $article->id ?? null,

            ]);
            $hyperlink->save();
        }
    }

    protected function getHeaderImageData(Request $request): void
    {
        if ($attach_id = $request->get('header_img')) {
            $attach_id = decrypt($attach_id);

            $attachment = Attachment::findOrFail($attach_id);
            $attachment->makePermanent();
            $this->attachments[] = $attachment;
        }
    }

    protected function packImages(string $content)
    {
        $re = '/<figure[\s\S]+?>\s*?<img[\s\S]+?data-db-id="([\s\S]+?)"[\s\S]*?>\s*?<\/figure>/m';

        $content = preg_replace_callback($re, function ($matches) {
            $return = '';

            $attach_id = $matches[1];

            try {
                if ($attach_id == 'placeholder') {
                    $return = "[img placeholder]";
                } else {
                    $attach_id = decrypt($attach_id);

                    $attachment = Attachment::findOrFail($attach_id);
                    $attachment->makePermanent();
                    $this->attachments[] = $attachment;

                    $return = "[img " . $attachment->id . "]";
                }
            } catch (\Exception $e) {
                getFormedError($e, 'attachment permanent issue');
            } finally {
                return $return;
            }
        }, $content);

        return $content;
    }

    protected function packHrefs(string $content)
    {
        $status = auth()->user()->hasRole('admin@web') ? 'active' : 'requested';

        $re = '/<a[\s\S]+?href="([\s\S]+?)"[\s\S]*?data-config="([\s\S]*?)"[\s\S]*?>([\s\S]*?)<\/a>/m';

        $content = preg_replace_callback($re, function ($matches) use ($status) {
            $return = '';

            $url = Hyperlink::extractUrl($matches[1]);
            $config = $matches[2]; # [ new , nof ]
            $title = $matches[3];

            try {
                $link = Hyperlink::create([
                    'user_id'    => auth()->user()->id ?? null,
                    'title'      => $title,
                    'url_type'   => config_key('enums.hyperlink_type', $url['type']),
                    'url'        => $url['path'],
                    'properties' => [
                        'new_tab' => Str::contains($config, ['new']),
                        'follow'  => !Str::contains($config, ['nof']),
                    ],
                    'status'     => config_key('enums.hyperlinks_status', $status),
                ]);

                $this->hyperlinks[] = $link;

                $return = "[href " . $link->id . "]" . $title . "[/href]";
            } catch (\Exception $e) {
                getFormedError($e, 'hyperlink issue');
            } finally {
                return $return;
            }
        }, $content);

        return $content;
    }

    public function jsonSearch(SearchArticlesRequest $request)
    {
        $territory = $this->checkPermission('view');

        $query = new Article;

        $query = $query->where(function ($query) use ($request) {
            $query = $query->where('heading', 'like', '%' . trim($request->term) . '%');
            $query = $query->orWhere('slug', 'like', '%' . trim($request->term) . '%');
            return $query;
        });

//        $query = $query->where('status', config_key('enums.articles_status','active'));

        $query = $query->get();

        $fractal = Fractal::create()
            ->serializeWith(Select2Serializer::class)
            ->collection($query, new Select2ArticleTransformer());
        return response()->json($fractal);
    }

    # save article category inner items (sub-category, post)
    public function articleSaveMeta(Request $request, Article $article)
    {
        $territory = $this->checkPermission('edit');

        $req = $request->only(config('enums.seo_details_type'));


        foreach($req as $type => $details){
            if($article->seo()[$type]){
                $article->seo()[$type]->update($details);
            }
            else{
                $seoDetail = $article->seoDetails()->create(['type' => config_key('enums.seo_details_type',$type)]);
                $seoDetail->update($details);
            }
        }

        return response('meta saved',200);
    }

}
