<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;

class AccountingController extends Controller
{
    protected $view = 'interface_dark.clients';

    public function index()
    {
        return view("{$this->view}.accounting.index");
    }

}
