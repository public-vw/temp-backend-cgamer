<?php

namespace App\Http\Controllers\Client;

use App\Models\Attachment;
use App\Models\AttachmentType;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class AttachmentChunkController extends _Controller
{
    protected $model = Attachment::class;

    public function uploadImageData(Request $request, string $type)
    {
        $territory = $this->checkPermission('create');

        $type = AttachmentType::where('slug', $type)->firstOrFail(['folder', 'id']);

        $image = $this->extractImageData($request->img_data);

        $md5 = md5($image['data']);
        $oldAttachment = Attachment::where('storage_disk', 'permanent')->where('md5', $md5)->first();

        if ($oldAttachment) {
            $attachment = Attachment::copyAsTempImage($type, $oldAttachment);
            return response()->json(encrypt($attachment->id));
        } else {
            $filename = $type->folder . '/' . Carbon::now()->format('Y-m/d-His-') . rand(101, 999);
            $fileext = $image['type'];
            $filesize = $request->size ?? null;
            $filepath = $filename . '.' . $fileext;

            $stored = Storage::disk('temporary')->put($filepath, $image['data']);
            if ($stored) {
                $attachment = Attachment::addTempImage($type, $filename, $fileext, $filesize);
                return response()->json(encrypt($attachment->id));
            }
        }


        return response('failed', 501);
    }

    public function uploadImageDataChunkMap(Request $request, string $type)
    {
        $territory = $this->checkPermission('create');

        $type = AttachmentType::where('slug', $type)->firstOrFail(['folder', 'id']);

        $token = Carbon::now()->format('Y-m/d-His-') . rand(101, 999);

//        $image = $this->extractImageData($request->base);

        $filename = $type->folder . '/' . $token;
        $fileext = $request->ext;
        $filesize = $request->size ?? null;
        $filepath = $filename . '.' . $fileext;

        $stored = Storage::disk('temporary')->put($filepath,'');
        if (!$stored) {
            return response('failed base save', 512);
        }

        $attachment = Attachment::addTempImage($type, $filename, $fileext, $filesize);

        $map = [
            'file'    => $filepath,
            'type'    => $type,
            'record'  => $attachment,
            'count'   => $request->count,
            'size'    => $request->size,
            'arrived' => 1,
        ];

        session(["upload_attachment.by_data.$token" => $map]);


        return response()->json(encrypt($token));
    }

    public function uploadImageDataChunk(Request $request, string $key)
    {
        $territory = $this->checkPermission('create');

//        request()->validate([
//            'file'  => 'required|mimes:doc,docx,pdf,txt|max:2048',
//        ]);

        $token = decrypt($key);
        $map = session("upload_attachment.by_data.$token", false);

        if (!$map) return response('token not found', 409);
        var_dump($request->all());
        return response('dev stop', 408);

        $stored = Storage::disk('temporary')->append($map['file'], $request->file(), null);
        if (!$stored) {
            return response('failed save', 512);
        }

        $map['arrived']++;
        if($map['arrived'] == $map['count']){
            Session::forget("upload_attachment.by_data.$token");
            return $this->uploadImageDone();
        }

        session(["upload_attachment.by_data.$token" => $map]);
        return response('part done');
    }

    protected function uploadImageDone()
    {
        return response('full done');
        $image = $this->extractImageData($request->img_data);

        $md5 = md5($image['data']);
        $oldAttachment = Attachment::where('storage_disk','permanent')->where('md5',$md5)->first();

        if($oldAttachment){
            $attachment = Attachment::copyAsTempImage($type, $oldAttachment);
            return response()->json(encrypt($attachment->id));
        }
        else{
            return response()->json(encrypt($attachment->id));
        }


        return response('failed', 501);
    }

    protected function extractImageData($data)
    {
        $type = null;
        if (preg_match('/^data:image\/(\w+);base64,/', $data, $type)) {
            $data = substr($data, strpos($data, ',') + 1);
            $type = strtolower($type[1]); // jpg, png, gif

            if (!in_array($type, ['jpg', 'jpeg', 'gif', 'png', 'webp'])) {
                throw new \Exception('invalid image type');
            }
            $data = str_replace(' ', '+', $data);
            $data = base64_decode($data);

            if ($data === false) {
                throw new \Exception('base64_decode failed');
            }
        } else {
            throw new \Exception('did not match data URI with image data');
        }

        return ['data' => $data, 'type' => $type];
    }

}
