<?php
namespace App\Http\Controllers\Client\Traits;

use Illuminate\Http\Request;

trait DbList
{
    #TODO: We have issue with translatable items. e.g. Country
    private function dbPrepare()
    {
        # add titleField variable to controller class, if title field is not <title> e.g. <name>
        $title = $this->titleField ?? 'title';

        $fields = ['id', $title . ' as text']; # text is keyword of select2
        $fields += $request->extraFields ?? [];

        return [$title, $fields];
    }

    public function dbList(Request $request)
    {
        #TODO check this
//        $this->getPanelFromSession();
        #TODO fix it for property form
        list($title,$fields) = $this->dbPrepare();

        $items = new $this->model;
        # add dependsVal variable to controller class, for constant dependencies, to make safety
        if (isset($this->dependsOn)) {
            $items = $items->where($this->dependsOn, $request->depends_val ?? $this->dependsVal);
        }
        if ($request->has('term')) {
            $items = $items->where($title, 'LIKE', "%$request->term%");
        }

        $items = $items->get($fields);

        return response()->json($items);
    }
}
