<?php
namespace App\Http\Controllers\Client\Traits;

use Illuminate\Http\Request;

trait BulkStatusActions
{
    public function bulkActions(Request $request)
    {
        $availableActions = array_keys(($this->model)::getStatusList());
        $availableActions['delete'] = true;

        $requestedAction = $request->action;
        $ids = json_decode($request->ids);

        if (empty($ids) or !is_array($ids)) {
            return redirect()->back()->withResult(self::fail('No items selected!'));
        }
        if (empty($requestedAction) || !isset($availableActions[$requestedAction])) {
            return redirect()->back()->withResult(self::fail('Action not supported!'));
        }

        $message = ($requestedAction == 'delete')?
            $this->deleteAction($ids)
            :$this->changeStatus($ids, $requestedAction);

        return redirect()->back()
            ->withResult(self::success($message));
    }

    public function changeStatus($ids, $newStatus)
    {
        $this->model::whereIn('id', $ids)->update(['status' => $newStatus]);
        return count($ids)." item(s) status changed";
    }

    #TODO: redundancy in delete function, one here, one in controller.
    #maybe it is good to use api as a core processor, and make these to call that
    #or maybe using listener is better
    public function deleteAction($ids)
    {
        $this->model::whereIn('id', $ids)->delete();
        return count($ids)." item(s) deleted successfully";
    }
}
