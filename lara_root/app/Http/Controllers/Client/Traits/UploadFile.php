<?php

namespace App\Http\Controllers\Client\Traits;

use App\Models\Attachment;
use App\Models\AttachmentType;
use Illuminate\Support\Facades\Storage;

trait UploadFile
{
    public function uploadFile($file, $attachmentType, $remoteBase = null)
    {
        if ($file == null) return null;
        #TODO check md5 and return thumbnail_id
        $attachmentType = AttachmentType::find($attachmentType);
        Storage::disk('permanent')->put();
        $file->store('public/' . ltrim($attachmentType->folder, '/'));

        return Attachment::create($this->setValues($file, $attachmentType, $remoteBase));
    }

    public function setValues($file, $attachmentType, $remoteBase)
    {
        #TODO check file_name
        return [
            'storage_disk'   => $remoteBase,
            'file_name'     => $file->hashName(),
            'file_ext'      => $file->extension(),
            'file_size'     => $file->getSize(),
            'type_id'       => $attachmentType->id,
            'mimetype'      => $file->getMimeType(),
            'md5'           => null,
            'original_name' => null,
            'thumbnail_id'  => null,
        ];

    }
}
