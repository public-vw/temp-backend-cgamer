<?php
namespace App\Http\Controllers\Client\Traits;

trait RoleJoint
{
    protected $role = 'client';
    protected $view;

    public function setRoleView()
    {
        $this->view = config("routes.{$this->role}.view",$this->role);
    }
}
