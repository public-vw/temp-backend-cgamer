<?php

namespace App\Http\Controllers\Client;

use App\Http\Requests\Client\PasswordRequest;
use App\Models\User;
use App\Models\UserMeta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use PragmaRX\Google2FA\Google2FA;
use SimpleSoftwareIO\QrCode\Facades\QrCode;


class ProfileController extends _Controller
{
    protected $model = User::class;

    public function index(Request $request)
    {
        $territory = $this->checkPermission();

        $user = auth()->user();

        return view($this->view . '.profile.index')
            ->withUser($user);
    }

    # shows user data
    public function showAjax()
    {
        $territory = $this->checkPermission('view');

        $user = auth()->user();
        return response()->json($user);
    }

    public function showPasswordForm(Request $request)
    {
        $territory = $this->checkPermission('edit');

        $user = auth()->user();

        $google_token = null;
        if (!$user->use_two_factor_auth) {
            $google2fa = new Google2FA();
            $google_token = $google2fa->getQRCodeUrl(
                config('app.name'),
                $user->email,
                $user->google_secret_key
            );
            $google_token = QrCode::size('150')->eye('circle')->errorCorrection('H')->style('dot')->generate($google_token);
        }


        return view('client.items.profile.change_password')
            ->withUser($user)
            ->withGoogleToken($google_token);
    }

    public function update(Request $request)
    {
        $territory = $this->checkPermission();

        $user = auth()->user();
        $user->update($request->all());

        return redirect()->back()
            ->withResult(self::success(
                __('client/controllers.update.success', ['model' => 'User', 'title' => $user->name])
            ));
    }

    public function ajaxUpdate(Request $request)
    {
        $territory = $this->checkPermission('edit');

        $user = auth()->user();

        $validator = Validator::make($request->all(), [
                'nickname' => 'sometimes|regex:/^([a-z\d\_]*)$/|max:20|unique:users,nickname',
            'about_me' => 'sometimes|max:500',
            'username' => 'sometimes|regex:/^([a-z\d\_]*)$/|max:30|unique:users,username',
            'mobile'   => 'sometimes|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|unique:users,mobile',
            'email'    => 'sometimes|email|unique:users,email',
            'ref_code' => 'sometimes|regex:/^([a-z\d\_]*)$/|max:20|unique:users,ref_code',
        ],[
            'nickname.unique' => 'تکراری است',
            'nickname.max' => 'حداکثر ۲۰ حرف',
            'nickname.regex' => 'کاراکتر غیر مجاز',

            'username.unique' => 'تکراری است',
            'username.max' => 'حداکثر ۳۰ حرف',
            'username.regex' => 'کاراکتر غیر مجاز',

            'mobile.unique' => 'تکراری است',
            'mobile.*' => 'شماره نامعتر است',

            'ref_code.unique' => 'مجاز نیست',
            'ref_code.max' => 'حداکثر ۲۰ حرف',
            'ref_code.regex' => 'کاراکتر غیر مجاز',

            'email.unique' => 'تکراری است',
            'email.*' => 'ایمیل نامعتبر است',

            'about_me.max' => 'حداکثر ۵۰۰ حرف مجاز است',
        ]);

        if ($validator->fails()) {
            return response()
                ->json(['errors' => $validator->errors()],400);
        }

        $req = $request->except('about_me');
        if (isset($req['mobile']) && $user->mobile != $req['mobile']) {
            $req['mobile_verified_at'] = null;
        }
        if (isset($req['email']) && $user->email != $req['email']) {
            $req['email_verified_at'] = null;
        }


        $user->update($req);
        UserMeta::updateOrCreate(['user_id'=>$user->id],['about_me'=>$request->get('about_me')]);

        return response()->json($request->all());
    }

    public function changePassword(PasswordRequest $request)
    {
        $territory = $this->checkPermission('edit');

        $user = auth()->user();

        if (!Hash::check($request->curr_password, $user->getAuthPassword())) {
            return redirect()->back()
                ->withResult(self::fail(
                    __('Access denied!')
                ));
        }

        $user->password = Hash::make($request->new_password);
        $user->save();

        return redirect()->back()
            ->withResult(self::success(
                __('client/controllers.update.success', ['model' => 'User', 'title' => $user->name])
            ));

    }

    public function setTwoStepAuth(Request $request)
    {
        $territory = $this->checkPermission('edit');

        $user = auth()->user();

        $google2fa = new Google2FA();
        if (!$google2fa->verifyKey($user->google_secret_key, $request->gtoken)) {
            return redirect()->back()
                ->withResult(self::fail(
                    __('Google Token Mismatch!')
                ));
        }

        $user->use_two_factor_auth = true;
        $user->save();

        return redirect()->back()
            ->withResult(self::success(
                __('client/controllers.update.success', ['model' => 'User', 'title' => $user->name])
            ));
    }

    public function removeTwoStepAuth(Request $request)
    {
        $territory = $this->checkPermission('edit');

        $user = auth()->user();

        $google2fa = new Google2FA();
        if (!$google2fa->verifyKey($user->google_secret_key, $request->gtoken)) {
            return redirect()->back()
                ->withResult(self::fail(
                    __('Google Token Mismatch!')
                ));
        }

        $user->use_two_factor_auth = false;
        $user->save();

        return redirect()->back()
            ->withResult(self::success(
                __('client/controllers.update.success', ['model' => 'User', 'title' => $user->name])
            ));
    }

}
