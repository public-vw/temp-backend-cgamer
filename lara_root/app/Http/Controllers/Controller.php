<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Gate;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        if (method_exists(static::class, 'setRoleView')) $this->setRoleView();
    }

    static protected function success($message): array
    {
        return self::alert('success', $message);
    }

    static protected function fail($message): array
    {
        return self::alert('danger', $message);
    }

    static protected function alert($type, $message): array
    {
        return [
            'alert'   => $type,
            'message' => $message,
        ];
    }

    static protected function smartRedirect($request, $route_header, $obj = null)
    {
        switch ($request->get('after_action')) {
            case 'stay':
                return redirect()->back();
                break;
            case 'back':
                return redirect()->route($route_header . '.index');
                break;
            case 'edit':
                return redirect()->route($route_header . '.edit', [$obj]);
                break;
        }
    }

    protected function checkPermission($spesific_action = null)
    {
        if(auth()->guest()) return false;

        $user = auth()->user();
        $dbt = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 2);
        list($guard, $panel, $table, $action) = $this->parseTrace($dbt[1]);
        $term = sprintf("%s.%s", $table, $spesific_action ?? $action);
        if(config('app.permissions_debug')){
            dump($guard, $panel, $table, $action, $term);
        }
        $territories = ['all', 'branch', 'own'];

        $all_perms = array_map(function ($item) use ($term) {
            return $term . "." . $item;
        }, $territories);
        $all_perms = array_combine($territories, $all_perms);

        if(config('app.permissions_debug')) {
            foreach($all_perms as $tperm){
                dump("$tperm : ". ($user->hasPermissionTo($tperm,$guard)?'yes':'no'));
            }
            dump("any : ". (Gate::forUser($user)->any($all_perms)?'yes':'no'));
            dd("none : ". (Gate::forUser($user)->none($all_perms)?'yes':'no'));
        }
        foreach ($all_perms as $territory => $perm){
            if($user->hasPermissionTo($perm,$guard)) return $territory;
        }

        abort(403,'Permission Unauthorized');
    }

    private function parseTrace($dbt)
    {
        $methodTranslations = [
            'index'   => 'view',
            'show'    => 'view',
            'list'    => 'view',
            'create'  => 'create',
            'store'   => 'create',
            'edit'    => 'edit',
            'update'  => 'edit',
            'destroy' => 'delete',
        ];

        $is_api = false;
        preg_match('/API\\\\V\d+\\\\/',$dbt['class'],$is_api);
        $dbt['class'] = preg_replace('/API\\\\V\d+\\\\/','',$dbt['class']);

        $class = explode('\\', $dbt['class']);
        $panel = strtolower($class[3]); //Admin, Client, ...
        $class = str_replace('Controller', '', $class[4]);
        $table = (new $dbt['class'])->table ?? model2table($class);
        $action = $methodTranslations[$dbt['function']] ?? $dbt['function'];

        return [$is_api?'api':'web', $panel, $table, $action];
    }
}
