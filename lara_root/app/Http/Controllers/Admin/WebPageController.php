<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\DataTables\Admin\WebPageDataTable;
use App\Http\Requests\Admin\WebPageRequest;
use App\Models\WebPage;

class WebPageController extends _Controller
{
    protected $model = WebPage::class;

    public function index(WebPageDataTable $dataTable)
    {
        $territory = $this->checkPermission();

        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function create(Request $request)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(WebPageRequest $request)
    {
        $territory = $this->checkPermission();

        $item = WebPage::create($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $item)
            ->withResult(self::success(
                __('admin/controllers.store.success', ['model' => $this->modelName, 'title' => $item->id])
            ));
    }

    public function show(WebPage $webPage)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.show")
            ->withWebPage($webPage);
    }

    public function edit(Request $request, WebPage $webPage)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.edit")
                ->withWebPage($webPage);
    }

    public function update(WebPageRequest $request, WebPage $webPage)
    {
        $territory = $this->checkPermission();

        $webPage->update($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $webPage)
            ->withResult(self::success(
                __('admin/controllers.update.success', ['model' => $this->modelName, 'title' => $webPage->id])
            ));
    }

    public function destroy(WebPage $webPage)
    {
        $territory = $this->checkPermission();

        $webPage->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('admin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $webPage->id])
            ));
    }
}
