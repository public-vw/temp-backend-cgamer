<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\DataTables\Admin\AttachmentTypeDataTable;
use App\Http\Controllers\Admin\Traits\DbList;
use App\Http\Requests\Admin\AttachmentTypeRequest;
use App\Models\AttachmentType;

class AttachmentTypeController extends _Controller
{
    use DbList;

    protected $model = AttachmentType::class;

    public function index(AttachmentTypeDataTable $dataTable)
    {
        $territory = $this->checkPermission();

        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function create(Request $request)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(AttachmentTypeRequest $request)
    {
        $territory = $this->checkPermission();

        $item = AttachmentType::create($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $item)
            ->withResult(self::success(
                __('admin/controllers.store.success', ['model' => $this->modelName, 'title' => $item->title])
            ));
    }

    public function edit(Request $request, AttachmentType $attachmentType)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.edit")
                ->withAttachmentType($attachmentType);
    }

    public function update(AttachmentTypeRequest $request, AttachmentType $attachmentType)
    {
        $territory = $this->checkPermission();

        $attachmentType->update($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $attachmentType)
            ->withResult(self::success(
                __('admin/controllers.update.success', ['model' => $this->modelName, 'title' => $attachmentType->title])
            ));
    }

    public function destroy(AttachmentType $attachmentType)
    {
        $territory = $this->checkPermission();

        $attachmentType->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('admin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $attachmentType->title])
            ));
    }
}
