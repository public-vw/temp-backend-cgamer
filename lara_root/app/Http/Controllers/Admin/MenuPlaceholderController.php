<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\DataTables\Admin\MenuPlaceholderDataTable;
use App\Http\Controllers\Admin\Traits\DbList;
use App\Http\Requests\Admin\MenuPlaceholderRequest;
use App\Models\MenuPlaceholder;

class MenuPlaceholderController extends _Controller
{
    use DbList;

    protected $model = MenuPlaceholder::class;
    protected $dependsOn = 'panel_id';

    public function index(MenuPlaceholderDataTable $dataTable)
    {
        $territory = $this->checkPermission();

        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function create(Request $request)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(MenuPlaceholderRequest $request)
    {
        $territory = $this->checkPermission();

        $item = MenuPlaceholder::create($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $item)
            ->withResult(self::success(
                __('admin/controllers.store.success', ['model' => $this->modelName, 'title' => $item->title])
            ));
    }

    public function edit(Request $request, MenuPlaceholder $menuPlaceholder)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.edit")
                ->withMenuPlaceholder($menuPlaceholder);
    }

    public function update(MenuPlaceholderRequest $request, MenuPlaceholder $menuPlaceholder)
    {
        $territory = $this->checkPermission();

        $menuPlaceholder->update($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $menuPlaceholder)
            ->withResult(self::success(
                __('admin/controllers.update.success', ['model' => $this->modelName, 'title' => $menuPlaceholder->title])
            ));

    }

    public function destroy(MenuPlaceholder $menuPlaceholder)
    {
        $territory = $this->checkPermission();

        $menuPlaceholder->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('admin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $menuPlaceholder->title])
            ));
    }
}
