<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\DataTables\Admin\UserMetaDataTable;
use App\Http\Requests\Admin\UserMetaRequest;
use App\Models\UserMeta;

class UserMetaController extends _Controller
{
    protected $model = UserMeta::class;

    public function index(UserMetaDataTable $dataTable)
    {
        $territory = $this->checkPermission();

        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function create(Request $request)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(UserMetaRequest $request)
    {
        $territory = $this->checkPermission();

        $item = UserMeta::create($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $item)
            ->withResult(self::success(
                __('admin/controllers.store.success', ['model' => $this->modelName, 'title' => $item->id])
            ));
    }

    public function show(UserMeta $userMeta)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.show")
            ->withUserMeta($userMeta);
    }

    public function edit(Request $request, UserMeta $userMeta)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.edit")
                ->withUserMeta($userMeta);
    }

    public function update(UserMetaRequest $request, UserMeta $userMeta)
    {
        $territory = $this->checkPermission();

        $userMeta->update($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $userMeta)
            ->withResult(self::success(
                __('admin/controllers.update.success', ['model' => $this->modelName, 'title' => $userMeta->id])
            ));
    }

    public function destroy(UserMeta $userMeta)
    {
        $territory = $this->checkPermission();

        $userMeta->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('admin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $userMeta->id])
            ));
    }
}
