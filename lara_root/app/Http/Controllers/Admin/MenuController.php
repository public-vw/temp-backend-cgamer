<?php

namespace App\Http\Controllers\Admin;

use App\Models\MenuPlaceholder;
use App\Models\Panel;
use Illuminate\Http\Request;
use App\DataTables\Admin\MenuDataTable;
use App\Http\Controllers\Admin\Traits\DbList;
use App\Http\Requests\Admin\MenuRequest;
use App\Models\Menu;

class MenuController extends _Controller
{
    use DbList;

    protected $model = Menu::class;
    protected $excludeInAdmin = ['sysadmin', 'admin'];

# BEGIN domenu()
    public function index()
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.index");
    }

    public function json()
    {
        $territory = $this->checkPermission('view');

        #TODO: We have to get menuplaceholder from client
        # then all these 2 lines should be removed
        $panel = Panel::where('title', 'interface')->first();
        $menuPlaceholder = MenuPlaceholder::where('title', 'top_menu')->first();
        session(['menu_edit.placeholder_id' => $menuPlaceholder->id]);

        $data = [];
        $this->getItemsFromDB($menuPlaceholder->menus, $data);

        $data = json_encode($data);
        return response()->json($data);
    }

    protected function getItemsFromDB($items, &$data): array
    {
        $territory = $this->checkPermission('view');

        foreach ($items as $item) {
            $tmp = [
                'id'                    => $item->id,
                'title'                 => $item->title,
                "customSelect"          => "select something...",
                "select2ScrollPosition" => ["x" => 0, "y" => 0],
            ];
            if ($item->hasChild()) {
                $tmp['children'] = [];
                $this->getItemsFromDB($item->children, $tmp['children']);
            }
            $data[] = $tmp;
        }
        return $data;
    }

    public function jsonSave(Request $request)
    {
        $territory = $this->checkPermission('create');

        $this->updateItemsOnDB($request->list);

        return response(true);
    }

    protected function updateItemsOnDB(array $list, int $parent_id = null)
    {
        foreach ($list as $order => $item) {
            $item = ($this->model)::firstOrNew(
                [
                    'id' => $item['id'],
                ],
                [
                    'title'          => $item['title'],
                    'order'          => $order,
                    'placeholder_id' => session('menu_edit.placeholder_id'),
                    'parent_id'      => $parent_id,
//                    'status' => config_key('enums.article_categories_status','active'),
                ]
            );
//            $item->status = config_key('enums.article_categories_status','active');
            $item->save();

            if (isset($item['children'])) {
                $this->updateItemsOnDB($item['children'], $item['id']);
            }
        }

    }

# END domenu()


    public function tableIndex(MenuDataTable $dataTable)
    {
        $territory = $this->checkPermission('view');

        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }


    public function create(Request $request)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.create")
            ->withExcludeInAdmin(json_encode($this->excludeInAdmin));
    }

    public function store(MenuRequest $request)
    {
        $territory = $this->checkPermission();

        $item = Menu::create($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $item)
            ->withResult(self::success(
                __('admin/controllers.store.success', ['model' => $this->modelName, 'title' => $item->title])
            ));
    }

    public function edit(Request $request, Menu $menu)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.edit")
            ->withExcludeInAdmin(json_encode($this->excludeInAdmin))
            ->withMenu($menu);
    }

    public function update(MenuRequest $request, Menu $menu)
    {
        $territory = $this->checkPermission();

        $menu->update($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $menu)
            ->withResult(self::success(
                __('admin/controllers.update.success', ['model' => $this->modelName,
                                                        'title' => $menu->title ?? $menu->id])
            ));
    }

    public function destroy(Menu $menu)
    {
        $territory = $this->checkPermission();

        $menu->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('admin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $menu->title])
            ));
    }
}
