<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\DataTables\Admin\ProvinceDataTable;
use App\Http\Controllers\Admin\Traits\DbList;
use App\Http\Requests\Admin\ProvinceRequest;
use App\Models\Province;

class ProvinceController extends _Controller
{
    use DbList;

    protected $model = Province::class;
    protected $dependsOn = 'country_id';

    public function index(ProvinceDataTable $dataTable)
    {
        $territory = $this->checkPermission();

        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function create(Request $request)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(ProvinceRequest $request)
    {
        $territory = $this->checkPermission();

        $province = Province::create($request->validated());

        return self::smartRedirect($request,"{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('admin/controllers.store.success', ['model' => $this->modelName, 'title' => $province->title])
            ));
    }

    public function edit(Request $request, Province $province)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.edit")
            ->withProvince($province);
    }

    public function update(ProvinceRequest $request, Province $province)
    {
        $territory = $this->checkPermission();

        $province->update($request->validated());

        return self::smartRedirect($request,"{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('admin/controllers.update.success', ['model' => $this->modelName, 'title' => $province->title])
            ));
    }

    public function destroy(Province $province)
    {
        $territory = $this->checkPermission();

        $province->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('admin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $province->title])
            ));
    }
}
