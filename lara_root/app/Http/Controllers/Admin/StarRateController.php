<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\DataTables\Admin\StarRateDataTable;
use App\Http\Requests\Admin\StarRateRequest;
use App\Models\StarRate;

class StarRateController extends _Controller
{
    protected $model = StarRate::class;
    protected $morph = 'rankable';

    public function index(StarRateDataTable $dataTable)
    {
        $territory = $this->checkPermission();

        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function create(Request $request)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(StarRateRequest $request)
    {
        $territory = $this->checkPermission();

        $item = StarRate::forceCreate($request->validated());

        if($request->rankable_type && $request->rankable_id){
            $rankable = modelObj($request->rankable_type);
            $rankable = $rankable->find($request->rankable_id);

            $item->rankable()->associate($rankable)->save();
        }

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $item)
            ->withResult(self::success(
                __('admin/controllers.store.success', ['model' => $this->modelName, 'title' => $item->id])
            ));
    }

    public function show(StarRate $starRate)
    {
        $territory = $this->checkPermission();

    }

    public function edit(Request $request, StarRate $starRate)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.edit")
                ->withStarRate($starRate);
    }

    public function update(StarRateRequest $request, StarRate $starRate)
    {
        $territory = $this->checkPermission();

        $starRate->update($request->validated());

        #TODO!: morph update issue again
        if(
            ($request->rankable_type && $request->rankable_id)
            &&
            (
                $request->rankable_type != $starRate->rankable_type
                ||
                $request->rankable_id != $starRate->rankable_id
            )
        ){
            $starRate->rankable()->dissociate();

            $rankable = modelObj($request->rankable_type);
            $rankable = $rankable->find($request->rankable_id);

            $starRate->rankable()->associate($rankable)->save();
        }

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $starRate)
            ->withResult(self::success(
                __('admin/controllers.update.success', ['model' => $this->modelName, 'title' => $starRate->id])
            ));
    }

    public function destroy(StarRate $starRate)
    {
        $territory = $this->checkPermission();

        $starRate->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('admin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $starRate->id])
            ));
    }
}
