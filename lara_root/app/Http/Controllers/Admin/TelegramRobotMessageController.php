<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\DataTables\Admin\TelegramRobotMessageDataTable;
use App\Http\Requests\Admin\TelegramRobotMessageRequest;
use App\Models\TelegramRobotMessage;

class TelegramRobotMessageController extends _Controller
{
    # Uses database listing
    # use DbList;

    protected $model = TelegramRobotMessage::class;

    public function index(TelegramRobotMessageDataTable $dataTable)
    {
        $territory = $this->checkPermission();

        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function create(Request $request)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(TelegramRobotMessageRequest $request)
    {
        $territory = $this->checkPermission();

        $item = TelegramRobotMessage::create($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $item)
            ->withResult(self::success(
                __('admin/controllers.store.success', ['model' => $this->modelName, 'title' => $item->id])
            ));
    }

    public function show(TelegramRobotMessage $telegramRobotMessage)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.show")
            ->withTelegramRobotMessage($telegramRobotMessage);
    }

    public function edit(Request $request, TelegramRobotMessage $telegramRobotMessage)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.edit")
                ->withTelegramRobotMessage($telegramRobotMessage);
    }

    public function update(TelegramRobotMessageRequest $request, TelegramRobotMessage $telegramRobotMessage)
    {
        $territory = $this->checkPermission();

        $telegramRobotMessage->update($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $telegramRobotMessage)
            ->withResult(self::success(
                __('admin/controllers.update.success', ['model' => $this->modelName, 'title' => $telegramRobotMessage->id])
            ));
    }

    public function destroy(TelegramRobotMessage $telegramRobotMessage)
    {
        $territory = $this->checkPermission();

        $telegramRobotMessage->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('admin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $telegramRobotMessage->id])
            ));
    }
}
