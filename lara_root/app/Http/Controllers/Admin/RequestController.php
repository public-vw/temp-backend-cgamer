<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\DataTables\Admin\RequestDataTable;
use App\Http\Controllers\Admin\Traits\DbList;
use App\Http\Requests\Admin\RequestRequest;
use App\Models\Request as Requesti;

class RequestController extends _Controller
{
    use DbList;

    protected $model = Requesti::class;
    protected $morph = 'requestable';

    public function index(RequestDataTable $dataTable)
    {
        $territory = $this->checkPermission();

        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function create(Request $request)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(RequestRequest $request)
    {
        $territory = $this->checkPermission();

        $item = Requesti::forceCreate($request->validated());

        if($request->requestable_type && $request->requestable_id){
            $requestable = modelObj($request->requestable_type);
            $requestable = $requestable->find($request->requestable_id);

            $item->requestable()->associate($requestable)->save();
        }

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $item)
            ->withResult(self::success(
                __('admin/controllers.store.success', ['model' => $this->modelName, 'title' => $item->id])
            ));
    }

    public function edit(Request $request, Requesti $requesti)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.edit")
            ->withRequesti($requesti);
    }

    public function update(RequestRequest $request, Requesti $requesti)
    {
        $territory = $this->checkPermission();

        $requesti->update($request->validated());

        #TODO!: morph update issue again
        if(
            ($request->requestable_type && $request->requestable_id)
            &&
            (
                $request->requestable_type != $requesti->requestable_type
                ||
                $request->requestable_id != $requesti->requestable_id
            )
        ){
            $requesti->requestable()->dissociate();

            $requestable = modelObj($request->requestable_type);
            $requestable = $requestable->find($request->requestable_id);

            $requesti->requestable()->associate($requestable)->save();
        }

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $requesti)
            ->withResult(self::success(
                __('admin/controllers.update.success', ['model' => $this->modelName, 'title' => $requesti->id])
            ));
    }

    public function destroy(Requesti $request)
    {
        $territory = $this->checkPermission();

        $request->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('admin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $request->id])
            ));
    }
}
