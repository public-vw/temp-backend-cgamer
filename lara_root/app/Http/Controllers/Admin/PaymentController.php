<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Traits\DbList;
use Illuminate\Http\Request;
use App\DataTables\Admin\PaymentDataTable;
use App\Http\Requests\Admin\PaymentRequest;
use App\Models\Payment;

class PaymentController extends _Controller
{
    use DbList;

    protected $model = Payment::class;

    public function index(PaymentDataTable $dataTable)
    {
        $territory = $this->checkPermission();

        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function create(Request $request)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(PaymentRequest $request)
    {
        $territory = $this->checkPermission();

        $item = Payment::create($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $item)
            ->withResult(self::success(
                __('admin/controllers.store.success', ['model' => $this->modelName, 'title' => $item->id])
            ));
    }

    public function show(Payment $payment)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.show")
            ->withPayment($payment);
    }

    public function edit(Request $request, Payment $payment)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.edit")
                ->withPayment($payment);
    }

    public function update(PaymentRequest $request, Payment $payment)
    {
        $territory = $this->checkPermission();

        $payment->update($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $payment)
            ->withResult(self::success(
                __('admin/controllers.update.success', ['model' => $this->modelName, 'title' => $payment->id])
            ));
    }

    public function destroy(Payment $payment)
    {
        $territory = $this->checkPermission();

        $payment->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('admin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $payment->id])
            ));
    }
}
