<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\LogDataTable;
use App\Models\Log;

class LogController extends _Controller
{
    protected $model = Log::class;

    public function index(LogDataTable $dataTable)
    {
        $territory = $this->checkPermission();

        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function show(Log $log)
    {
        $territory = $this->checkPermission();

    }

    public function destroy(Log $log)
    {
        $territory = $this->checkPermission();

    }
}
