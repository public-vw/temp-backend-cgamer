<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\DataTables\Admin\AttachmentDataTable;
use App\Http\Controllers\Admin\Traits\UploadFile;
use App\Http\Requests\Admin\AttachmentRequest;
use App\Models\Attachment;

class AttachmentController extends _Controller
{
    use UploadFile;

    protected $model = Attachment::class;

    public function index(AttachmentDataTable $dataTable)
    {
        $territory = $this->checkPermission();

        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function create(Request $request)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(AttachmentRequest $request)
    {
        $territory = $this->checkPermission();

        $class = model2class($request->attachmentable_type);
        $item = (new $class)->find($request->attachmentable_id);

        $attachment = $this->checkFileField($request, 'file', $item);
        $attachment->quality_rank = $request->quality_rank;
        $attachment->properties = $request->properties;
        $attachment->save();

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $attachment)
            ->withResult(self::success(
                __('admin/controllers.store.success', ['model' => $this->modelName, 'title' => $attachment->id])
            ));
    }

    public function edit(Request $request, Attachment $attachment)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.edit")
                ->withAttachment($attachment);
    }

    public function update(AttachmentRequest $request, Attachment $attachment)
    {
        $territory = $this->checkPermission();

        $class = model2class($request->attachmentable_type);
        $item = (new $class)->find($request->attachmentable_id);

        $attachment = $this->checkFileField($request, 'file', $item, $attachment);
        $attachment->quality_rank = $request->quality_rank;
        $attachment->properties = $request->properties;
        $attachment->save();

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $attachment)
            ->withResult(self::success(
                __('admin/controllers.update.success', ['model' => $this->modelName, 'title' => $attachment->id])
            ));
    }

    public function destroy(Attachment $attachment)
    {
        $territory = $this->checkPermission();

        $attachment->hardDelete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('admin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $attachment->id])
            ));
    }
}
