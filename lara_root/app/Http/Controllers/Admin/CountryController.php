<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\DataTables\Admin\CountryDataTable;
use App\Http\Controllers\Admin\Traits\DbList;
use App\Http\Requests\Admin\CountryRequest;
use App\Models\Country;

class CountryController extends _Controller
{
    use DbList;

    protected $model = Country::class;

    public function index(CountryDataTable $dataTable)
    {
        $territory = $this->checkPermission();

        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function create(Request $request)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(CountryRequest $request)
    {
        $territory = $this->checkPermission();

        $item = Country::create($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $item)
            ->withResult(self::success(
                __('admin/controllers.store.success', ['model' => $this->modelName, 'title' => $item->title])
            ));
    }

    public function edit(Request $request, Country $country)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.edit")
            ->withCountry($country);
    }

    public function update(CountryRequest $request, Country $country)
    {
        $territory = $this->checkPermission();

        $country->update($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $country)
            ->withResult(self::success(
                __('admin/controllers.update.success', ['model' => $this->modelName, 'title' => $country->title])
            ));
    }

    public function destroy(Country $country)
    {
        $territory = $this->checkPermission();

        $country->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('admin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $country->title])
            ));
    }
}
