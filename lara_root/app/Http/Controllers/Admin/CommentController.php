<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Traits\DbList;
use Illuminate\Http\Request;
use App\DataTables\Admin\CommentDataTable;
use App\Http\Requests\Admin\CommentRequest;
use App\Models\Comment;

class CommentController extends _Controller
{
    use DbList;

    protected $model = Comment::class;
    protected $morph = 'commentable';
    protected $titleField = 'content';

    # TODO: fix store and update(morph type)
    public function index(CommentDataTable $dataTable)
    {
        $territory = $this->checkPermission();

        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function create(Request $request)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(CommentRequest $request)
    {
        $territory = $this->checkPermission();

        $item = Comment::forceCreate($request->validated());

        if($request->commentable_type && $request->commentable_id){
            $commentable = modelObj($request->commentable_type);
            $commentable = $commentable->find($request->commentable_id);

            $item->commentable()->associate($commentable)->save();
        }

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $item)
            ->withResult(self::success(
                __('admin/controllers.store.success', ['model' => $this->modelName, 'title' => $item->id])
            ));
    }

    public function show(Comment $comment)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.show")
            ->withComment($comment);
    }

    public function edit(Request $request, Comment $comment)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.edit")
                ->withComment($comment);
    }

    public function update(CommentRequest $request, Comment $comment)
    {
        $territory = $this->checkPermission();

        $comment->update($request->validated());

        #TODO!: Is there any need to check these? Needs Test and then remove all
        if(
            ($request->commentable_type && $request->commentable_id)
            &&
            (
                $request->commentable_type != $comment->commentable_type
                ||
                $request->commentable_id != $comment->commentable_id
            )
        ){
            $comment->commentable()->dissociate();

            $commentable = modelObj($request->commentable_type);
            $commentable = $commentable->find($request->commentable_id);

            $comment->commentable()->associate($commentable)->save();
        }

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $comment)
            ->withResult(self::success(
                __('admin/controllers.update.success', ['model' => $this->modelName, 'title' => $comment->id])
            ));
    }

    public function destroy(Comment $comment)
    {
        $territory = $this->checkPermission();

        $comment->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('admin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $comment->id])
            ));
    }
}
