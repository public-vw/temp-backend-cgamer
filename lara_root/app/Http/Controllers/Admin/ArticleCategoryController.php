<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Traits\UploadFile;
use Illuminate\Http\Request;
use App\DataTables\Admin\ArticleCategoryDataTable;
use App\Http\Requests\Admin\ArticleCategoryRequest;
use App\Models\ArticleCategory;

class ArticleCategoryController extends _Controller
{
    use UploadFile;

    protected $model = ArticleCategory::class;

# BEGIN domenu()
    public function index()
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.index");
    }

    public function json()
    {
        $territory = $this->checkPermission('view');

        $categories = ArticleCategory::roots();

        $data = [];
        $this->getItemsFromDB($categories, $data);

        $data = json_encode($data);
        return response()->json($data);
    }

    protected function getItemsFromDB($items, &$data): array
    {
        foreach ($items as $item) {
            $tmp = [
                'id'                    => $item->id,
                'title'                 => $item->title,
                'heading'               => $item->heading,
                'childrenCount'         => $item->deepArticlesCount(),
                "customSelect"          => "select something...",
                "select2ScrollPosition" => ["x" => 0, "y" => 0],
            ];
            if ($item->hasChild()) {
                $tmp['children'] = [];
                $this->getItemsFromDB($item->children, $tmp['children']);
            }
            $data[] = $tmp;
        }
        return $data;
    }

    public function jsonSave(Request $request)
    {
        $territory = $this->checkPermission('create');

        $this->updateItemsOnDB($request->list);

        return response(true);
    }

    protected function updateItemsOnDB(array $list, int $parent_id = null)
    {
        foreach ($list as $order => $item) {
            $item = ($this->model)::firstOrNew(
                [
                    'id' => $item['id'],
                ],
                [
                    'title'     => $item['title'],
                    'order'     => $order,
                    'heading' => $item['heading'],
                    'parent_id' => $parent_id,
//                    'status' => config_key('enums.article_categories_status','active'),
                ]
            );
            $item->status = $item->status ??  config_key('enums.article_categories_status','ready');
            $item->save();

            if (isset($item['children'])) {
                $this->updateItemsOnDB($item['children'], $item['id']);
            }
        }

    }

# END domenu()

    public function tableIndex(ArticleCategoryDataTable $dataTable)
    {
        $territory = $this->checkPermission('view');

        return $dataTable->render("{$this->view}.items.{$this->table}.table_index");
    }


    public function create(Request $request)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(ArticleCategoryRequest $request)
    {
        $territory = $this->checkPermission();

        $item = ArticleCategory::create($request->validated());

        $this->checkFileField($request, 'headerImage', $item);

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $item)
            ->withResult(self::success(
                __('admin/controllers.store.success', ['model' => $this->modelName, 'title' => $item->id])
            ));
    }

    public function show(ArticleCategory $articleCategory)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.show")
            ->withArticleCategory($articleCategory);
    }

    public function edit(Request $request, ArticleCategory $articleCategory)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.edit")
            ->withArticleCategory($articleCategory);
    }

    public function update(ArticleCategoryRequest $request, ArticleCategory $articleCategory)
    {
        $territory = $this->checkPermission();

        $articleCategory->update($request->validated());

        $this->checkFileField($request, 'headerImage', $articleCategory);

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $articleCategory)
            ->withResult(self::success(
                __('admin/controllers.update.success', ['model' => $this->modelName, 'title' => $articleCategory->id])
            ));
    }

    public function destroy(ArticleCategory $articleCategory)
    {
        $territory = $this->checkPermission();

        $articleCategory->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('admin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $articleCategory->id])
            ));
    }

}
