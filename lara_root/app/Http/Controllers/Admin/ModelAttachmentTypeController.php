<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\DataTables\Admin\ModelAttachmentTypeDataTable;
use App\Http\Requests\Admin\ModelAttachmentTypeRequest;
use App\Models\ModelAttachmentType;

class ModelAttachmentTypeController extends _Controller
{
    protected $model = ModelAttachmentType::class;

    public function index(ModelAttachmentTypeDataTable $dataTable)
    {
        $territory = $this->checkPermission();

        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function create(Request $request)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(ModelAttachmentTypeRequest $request)
    {
        $territory = $this->checkPermission();

        $item = ModelAttachmentType::create($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $item)
            ->withResult(self::success(
                __('admin/controllers.store.success', ['model' => $this->modelName, 'title' => $item->id])
            ));
    }

    public function show(ModelAttachmentType $model_attachment_type)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.show")
            ->withModelAttachmentType($model_attachment_type);
    }

    public function edit(Request $request, ModelAttachmentType $model_attachment_type)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.edit")
                ->withModelAttachmentType($model_attachment_type);
    }

    public function update(ModelAttachmentTypeRequest $request, ModelAttachmentType $model_attachment_type)
    {
        $territory = $this->checkPermission();

        $model_attachment_type->update($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $model_attachment_type)
            ->withResult(self::success(
                __('admin/controllers.update.success', ['model' => $this->modelName, 'title' => $model_attachment_type->id])
            ));
    }

    public function destroy(ModelAttachmentType $model_attachment_type)
    {
        $territory = $this->checkPermission();

        $model_attachment_type->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('admin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $model_attachment_type->id])
            ));
    }
}
