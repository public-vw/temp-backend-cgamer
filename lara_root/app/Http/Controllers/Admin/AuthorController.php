<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Traits\DbList;
use Illuminate\Http\Request;
use App\DataTables\Admin\AuthorDataTable;
use App\Http\Requests\Admin\AuthorRequest;
use App\Models\Author;

class AuthorController extends _Controller
{
    use DbList;

    protected $model = Author::class;

    public function index(AuthorDataTable $dataTable)
    {
        $territory = $this->checkPermission();

        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function create(Request $request)
    {
        $territory = $this->checkPermission();

        abort_if($territory != 'all', 404);

        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(AuthorRequest $request)
    {
        $territory = $this->checkPermission();

        abort_if($territory != 'all', 403);

        $item = Author::create($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $item)
            ->withResult(self::success(
                __('admin/controllers.store.success', ['model' => $this->modelName, 'title' => $item->id])
            ));
    }

    public function show(Author $author)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.show")
            ->withAuthor($author);
    }

    public function edit(Request $request, Author $author)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.edit")
                ->withAuthor($author);
    }

    public function update(AuthorRequest $request, Author $author)
    {
        $territory = $this->checkPermission();

        $author->update($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $author)
            ->withResult(self::success(
                __('admin/controllers.update.success', ['model' => $this->modelName, 'title' => $author->id])
            ));
    }

    public function destroy(Author $author)
    {
        $territory = $this->checkPermission();

        $author->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('admin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $author->id])
            ));
    }
}
