<?php

namespace App\Http\Controllers\Admin\Traits;

use App\Models\Attachment;
use App\Models\AttachmentType;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

trait UploadFile
{
    protected function checkFileField(Request $request, string $fieldName, $item, Attachment $attachment = null){
        if ($request->hasFile($fieldName)){
            $attachmentType = ($request->has('attachment_type_id')) ?
                  AttachmentType::find($request->attachment_type_id)
                : AttachmentType::where('slug', $request->attachment_type_slug)->firstOrFail(['folder', 'id']);

            return $this->uploadFile($request->file($fieldName), $attachmentType, $item, $attachment);
        }
        return $attachment;
    }

    private function uploadFile(UploadedFile $file, AttachmentType $attachmentType, $item, Attachment $attachment = null)
    {
        $originalName = $file->getClientOriginalName();
        $fileext = $file->getClientOriginalExtension();

        $md5 = md5($file->get());
        $oldAttachment = Attachment::where('storage_disk','permanent')->where('md5',$md5)->first();

        if($oldAttachment){
            $attachment = $attachment? $attachment->fillAs($attachmentType, $oldAttachment, false)
                :Attachment::copyAs($attachmentType, $oldAttachment, false);
        }
        else{
            $filename = $attachmentType->folder . '/' . Carbon::now()->format('Y-m/d-His-') . rand(101, 999);
            $filesize = null;//w&h #TODO: should update for images
            $filepath = $filename . '.' . $fileext;

            $stored = Storage::disk('permanent')->put($filepath, $file->get());
            if ($stored) {
                $attachment = $attachment? $attachment->updateAs($attachmentType, $filename, $fileext, $filesize, $originalName,false)
                    :Attachment::add($attachmentType, $filename, $fileext, $filesize, $originalName,false);
            }
        }

        $attachment->attachmentable_id = $item->id;
        $attachment->attachmentable_type = array_search(get_class($item), Relation::$morphMap) ?: null;
        $attachment->save();

        return $attachment;
    }

}
