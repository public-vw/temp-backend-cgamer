<?php
namespace App\Http\Controllers\Admin\Traits;

trait RoleJoint
{
    protected $role = 'admin';
    protected $view;

    public function setRoleView()
    {
        $this->view = config("routes.{$this->role}.view",$this->role);
    }
}
