<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\DataTables\Admin\RegionDataTable;
use App\Http\Controllers\Admin\Traits\DbList;
use App\Http\Requests\Admin\RegionRequest;
use App\Models\Region;

class RegionController extends _Controller
{
    use DbList;

    protected $model = Region::class;
    protected $dependsOn = 'city_id';

    public function index(RegionDataTable $dataTable)
    {
        $territory = $this->checkPermission();

        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function create(Request $request)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(RegionRequest $request)
    {
        $territory = $this->checkPermission();

        $region = Region::create($request->validated());

        return self::smartRedirect($request,"{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('admin/controllers.store.success', ['model' => $this->modelName, 'title' => $region->title])
            ));
    }

    public function edit(Request $request, Region $region)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.edit")
            ->withRegion($region);
    }

    public function update(RegionRequest $request, Region $region)
    {
        $territory = $this->checkPermission();

        $region->update($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $region)
            ->withResult(self::success(
                __('admin/controllers.update.success', ['model' => $this->modelName, 'title' => $region->title])
            ));
    }

    public function destroy(Region $region)
    {
        $territory = $this->checkPermission();

        $region->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('admin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $region->title])
            ));
    }
}
