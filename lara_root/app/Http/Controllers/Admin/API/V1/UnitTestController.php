<?php

namespace App\Http\Controllers\Admin\API\V1;

use App\Console\Tests\CheckAttachmentsOnArticlesClass;
use App\Console\Tests\CheckAttachmentsOnStorageClass;
use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Attachment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Symfony\Component\Console\Output\BufferedOutput;

class UnitTestController extends Controller
{
    public function checkAttachedFiles(Request $request)
    {
        $checker = new CheckAttachmentsOnStorageClass($request->has('delete') && $request->delete === 'true');
        $checker->handle();
        $report = $checker->getReport();

        return response()->json($report);
    }

    public function checkAttachmentsOnArticles(Request $request)
    {
        $checker = new CheckAttachmentsOnArticlesClass($request->id ?? null,$request->status ?? null,($request->just_issues && $request->just_issues == "true") ?? false);
        $checker->handle();
        $report = $checker->getReport();

        return response()->json($report);
    }

}
