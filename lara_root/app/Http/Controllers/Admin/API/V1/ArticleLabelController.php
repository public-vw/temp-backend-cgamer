<?php

namespace App\Http\Controllers\Admin\API\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\API\CategoryRequest;
use App\Http\Requests\Admin\API\LabelRequest;
use App\Http\Requests\Admin\API\ListArticleCategoriesRequest;
use App\Http\Requests\Admin\API\ListArticleLabelsRequest;
use App\Http\Transformers\ArticleCategoryTransformer;
use App\Http\Transformers\ArticleLabelTransformer;
use App\Models\ArticleCategory;
use App\Models\ArticleLabel;
use App\Models\ArticleLabelGroup;
use Spatie\Fractal\Fractal;

class ArticleLabelController extends Controller
{
    public function create(LabelRequest $request)
    {
        $territory = $this->checkPermission();

        $group = ArticleLabelGroup::firstOrCreate(['title' => $request->group_title]);

        $label = ArticleLabel::create([
            'group_id'    => $group->id,
            'title'       => $request->title,
            'order'       => $request->order ?? 1,
            'color_theme' => rand(0, count(config('enums.random_colors')) - 1),
        ]);

        return response('Label created successfully | ' .
            'group_id: ' . $label->group->id .
            'id: ' . $label->id
        );
    }

    public function list(ListArticleLabelsRequest $request)
    {
        $territory = $this->checkPermission();

        $query = new ArticleLabel;

        if ($request->id) {
            $query = $query->where('id', $request->id);
        }
        if ($request->group_id) {
            $query = $query->where('group_id', $request->group_id);
        }
        if ($request->term) {
            $query = $query->where('title', 'like', '%' . trim($request->term) . '%');
        }

        $query = $query->get();

        $fractal = Fractal::create()->collection($query, new ArticleLabelTransformer());
        return response()->json($fractal);
    }


}
