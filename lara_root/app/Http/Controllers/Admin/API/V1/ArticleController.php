<?php

namespace App\Http\Controllers\Admin\API\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\API\ChangeArticleAuthorByAdminRequest;
use App\Http\Requests\Admin\API\ChangeArticleCategoryByAdminRequest;
use App\Http\Requests\Admin\API\ChangeArticleLabelByAdminRequest;
use App\Http\Requests\Admin\API\ListArticlesRequest;
use App\Http\Transformers\ArticleTransformer;
use App\Models\Article;
use Illuminate\Http\Request;
use Spatie\Fractal\Fractal;

class ArticleController extends Controller
{
    public function changeAuthor(ChangeArticleAuthorByAdminRequest $request, Article $article)
    {
        $territory = $this->checkPermission('edit');

        $article->update(['author_id' => $request->author_id]);
        return response('Author changed successfully');
    }

    public function changeCategory(ChangeArticleCategoryByAdminRequest $request, Article $article)
    {
        $territory = $this->checkPermission('edit');

        $article->update(['category_id' => $request->category_id]);
        return response('Category changed successfully');
    }

    public function changeLabels(ChangeArticleLabelByAdminRequest $request, Article $article)
    {
        $territory = $this->checkPermission('edit');

        return response('Under Construction!');
        $article->update(['label_id' => $request->label_id]);
        return response('Label changed successfully');
    }

    public function list(ListArticlesRequest $request)
    {
        $territory = $this->checkPermission();

        $query = new Article;

        if ($request->id) {
            $query = $query->where('id', $request->id);
        }
        if ($request->term) {
            if ($request->term_type) {
                $query = $query->where(trim($request->term_type), 'like', '%' . trim($request->term) . '%');
            } else {
                $query = $query->where(function ($query) use ($request) {
                    $query = $query->where('heading', 'like', '%' . trim($request->term) . '%');
                    $query = $query->orWhere('content', 'like', '%' . trim($request->term) . '%');
                    return $query;
                });
            }
        }

        if ($request->status) {
            $query = $query->where('status', $request->status);
        }

        if ($request->author_id) {
            $query = $query->where('author_id', $request->author_id);
        }

        if ($request->category_id) {
            $query = $query->where('category_id', $request->category_id);
        }

        $query = $query->get();

        $fractal = Fractal::create()->collection($query, new ArticleTransformer());
        return response()->json( $fractal );
    }
}
