<?php

namespace App\Http\Controllers\Admin\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Symfony\Component\Console\Output\BufferedOutput;

class BackupController extends Controller
{
    protected $rcloneConfigName = 'mr_backup_cryptobaz';

    public function dbBackup(Request $request)
    {
        $ret = new BufferedOutput();
        Artisan::call('mydb:backup -u -k',[],$ret);
        return $ret->fetch();
    }

    public function uploadsBackup(Request $request)
    {
        $ret = new BufferedOutput();
        Artisan::call('myup:backup -u -k',[],$ret);
        return $ret->fetch();
    }
}
