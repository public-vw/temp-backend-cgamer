<?php

namespace App\Http\Controllers\Admin\API\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\API\CategoryRequest;
use App\Http\Requests\Admin\API\ListArticleCategoriesRequest;
use App\Http\Transformers\ArticleCategoryTransformer;
use App\Models\ArticleCategory;
use Spatie\Fractal\Fractal;

class ArticleCategoryController extends Controller
{
    public function create(CategoryRequest $request)
    {
        $territory = $this->checkPermission();

        $parent = $request->parent_id ?? null;
        $status = config_key('enums.article_categories_status', $request->status ?? 'active');

        $category = ArticleCategory::create([
            'title'     => $request->title,
            'heading'     => $request->title,
            'parent_id' => $parent,
            'status'    => (int)$status,
        ]);

        return response('Category created successfully | id: ' . $category->id);
    }

    public function list(ListArticleCategoriesRequest $request)
    {
        $territory = $this->checkPermission();

        $query = new ArticleCategory;

        if ($request->id) {
            $query = $query->where('id', $request->id);
        }
        if ($request->parent_id) {
            $query = $query->where('parent_id', $request->parent_id);
        }
        if ($request->term) {
            $query = $query->where(function($query) use ($request){
                $query = $query->where('title', 'like', '%' . trim($request->term) . '%');
                $query = $query->orWhere('heading', 'like', '%' . trim($request->term) . '%');
            });
        }
        if ($request->status) {
            $query = $query->where('status', config_key('enums.article_categories_status', trim($request->status)));
        }

        $query = $query->get();

        $fractal = Fractal::create()->collection($query, new ArticleCategoryTransformer());
        return response()->json( $fractal );
    }


}
