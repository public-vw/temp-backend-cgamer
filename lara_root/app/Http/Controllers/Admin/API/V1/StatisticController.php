<?php

namespace App\Http\Controllers\Admin\API\V1;

use App\Console\Tests\CheckAttachmentsOnArticlesClass;
use App\Console\Tests\CheckAttachmentsOnStorageClass;
use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\ArticleCategory;
use App\Models\Attachment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Symfony\Component\Console\Output\BufferedOutput;

class StatisticController extends Controller
{
    public function articlesStatus(Request $request)
    {
        $show_details = ($request->details && $request->details == "true") ?? false;
        $filter = $request->status ?? false;

        $summary = Article::
            select(['status',DB::raw('count(*) as cnt')])
            ->groupBy('status')
            ->get()->keyBy('status')
            ->pluck('cnt','status')
            ->toArray();

        $details = [];

        foreach($summary as $key => $value){
            unset($summary[$key]);
            $skey = config('enums.articles_status.'.$key);
            $summary[$key.'|'.$skey] = $value;

            if($show_details){
                if((($filter !== FALSE) && ($skey === $filter || $key == $filter)) || $filter === FALSE){
                    $details[$key.'|'.$skey] = Article::where('status',(string)$key)
                        ->orderByDesc('updated_at')->get(['id'])->pluck('id')->toArray();
                    if(!count($details[$key.'|'.$skey])) unset($details[$key.'|'.$skey]);
                }
            }
        }

        $report['summary'] = $summary;
        $show_details ? $report['details'] = $details : '';

        return response()->json($report);
    }

    public function articleCategoriesStatus(Request $request)
    {
        $show_details = ($request->details && $request->details == "true") ?? false;
        $filter = $request->status ?? false;

        $summary = ArticleCategory::
            select(['status',DB::raw('count(*) as cnt')])
            ->groupBy('status')
            ->get()->keyBy('status')
            ->pluck('cnt','status')
            ->toArray();

        $details = [];

        foreach($summary as $key => $value){
            unset($summary[$key]);
            $skey = config('enums.article_categories_status.'.$key);
            $summary[$key.'|'.$skey] = $value;

            if($show_details){
                if((($filter !== FALSE) && ($skey === $filter || $key == $filter)) || $filter === FALSE){
                    $details[$key.'|'.$skey] = ArticleCategory::where('status',(string)$key)
                        ->orderByDesc('updated_at')->get(['id'])->pluck('id')->toArray();
                    if(!count($details[$key.'|'.$skey])) unset($details[$key.'|'.$skey]);
                }
            }
        }

        $report['summary'] = $summary;
        $show_details ? $report['details'] = $details : '';

        return response()->json($report);
    }

}
