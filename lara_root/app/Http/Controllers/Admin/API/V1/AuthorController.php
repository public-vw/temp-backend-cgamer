<?php

namespace App\Http\Controllers\Admin\API\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\API\AuthorByAdminRequest;
use App\Http\Requests\Admin\API\ListAuthorsRequest;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AuthorController extends Controller
{
    public function create(AuthorByAdminRequest $request)
    {
        $territory = $this->checkPermission();

        User::create([
            'name'               => $request->name,
            'username'           => $request->username,
            'ref_code'           => null,
            'mobile'             => null,
            'email'              => null,
            'password'           => Hash::make(Str::random(10)),
            'mobile_verified_at' => null,
            'email_verified_at'  => null,

            'random_color' => rand(0, count(config('enums.random_colors')) - 1),
            'telegram_uid' => null,

            'use_two_factor_auth' => false,
            'google_secret_key'   => null,
            'status'              => config_key('enums.users_status', 'storage'),
        ])->assignRole('author@web');

        return response('Author created successfully');
    }

    public function list(ListAuthorsRequest $request)
    {
        $territory = $this->checkPermission();

        $query = User::hasAnyRoles('author@web','author@api')->where('username', '<>', 'sysadmin');

        if ($request->term) {
            if ($request->term_type) {
                $query = $query->where(trim($request->term_type), 'like', '%' . trim($request->term) . '%');
            } else {
                $query = $query->where(function ($query) use ($request) {
                    $query = $query->where('name', 'like', '%' . trim($request->term) . '%');
                    $query = $query->orWhere('username', 'like', '%' . trim($request->term) . '%');
                    $query = $query->orWhere('mobile', 'like', '%' . trim($request->term) . '%');
                    return $query;
                });
            }
        }

        $query = $query->get(['id', 'name', 'username', 'mobile']);

        return response()->json($query->toArray());
    }
}
