<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\FactRequest;
use Illuminate\Http\Request;
use App\DataTables\Admin\FactDataTable;
use App\Models\Fact;

class FactController extends _Controller
{
    protected $model = Fact::class;

    public function index(FactDataTable $dataTable)
    {
        $territory = $this->checkPermission();

        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function create(Request $request)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(FactRequest $request)
    {
        $territory = $this->checkPermission();

        $item = Fact::create($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $item)
            ->withResult(self::success(
                __('admin/controllers.store.success', ['model' => $this->modelName, 'title' => $item->id])
            ));
    }

    public function show(Fact $fact)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.show")
            ->withFact($fact);
    }

    public function edit(Request $request, Fact $fact)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.edit")
                ->withFact($fact);
    }

    public function update(FactRequest $request, Fact $fact)
    {
        $territory = $this->checkPermission();

        $fact->update($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $fact)
            ->withResult(self::success(
                __('admin/controllers.update.success', ['model' => $this->modelName, 'title' => $fact->id])
            ));
    }

    public function destroy(Fact $fact)
    {
        $territory = $this->checkPermission();

        $fact->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('admin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $fact->id])
            ));
    }
}
