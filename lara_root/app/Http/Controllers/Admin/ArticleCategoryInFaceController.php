<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\SearchArticlesRequest;
use App\Http\Transformers\Select2ArticleCategoryTransformer;
use App\Http\Transformers\Serializers\Select2Serializer;
use App\Models\Article;
use App\Models\ArticleCategory;
use App\Models\Attachment;
use App\Models\Hyperlink;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Spatie\Fractal\Fractal;

class ArticleCategoryInFaceController extends _Controller
{
    protected $model = ArticleCategory::class;


    # shows preview of article category
    public function articleCategoryPreview(ArticleCategory $articleCategory)
    {
        $territory = $this->checkPermission('view');

        return view("interface_dark.admins.article_category.index")
            ->withCategory($articleCategory)
            ->withPreview(true);
    }


    # shows edit article
    public function articleCategoryEdit(ArticleCategory $articleCategory)
    {
        $territory = $this->checkPermission('edit');

        return view("interface_dark.admins.article_category.index")->withCategory($articleCategory);
    }


    # save article category
    public function articleCategorySaveDraft(Request $request)
    {
        $territory = $this->checkPermission('create');

        $articleCategory = ArticleCategory::findOrFail((int)$request->id);
        $articleCategory->update([
            'heading'     => $request->get('header'),
            'description' => $this->sanitizeContentForDB($request->get('body')),
        ]);

        $this->getHeaderImageData($request);

        $this->connectAttachments($articleCategory);
        $this->connectHyperlinks($articleCategory);

        return response()->json($articleCategory->id);
    }

    # save article category inner items (sub-category, post)
    public function articleCategorySaveItem(Request $request, string $itemType)
    {
        $territory = $this->checkPermission('edit');

        $update = [];
        switch ($itemType) {
            case 'ArticleCategory':
                $item = ArticleCategory::findOrFail((int)$request->id);
                break;
            case 'Article':
                $item = Article::findOrFail((int)$request->id);
                break;
            default:
                abort(403, 'Item Type Not Found');
        }

        $request->header ? $update['heading'] = $request->header : null;
        $request->body ? $update['summary'] = $request->body : null;
        $update ? $item->update($update) : null;

//        $this->getHeaderImageData($request);

//        $this->connectAttachments($articleCategory);

        return response()->json($item->id);
    }

    # save article category inner items (sub-category, post)
    public function articleCategorySaveMeta(Request $request, ArticleCategory $articleCategory)
    {
        $territory = $this->checkPermission('edit');

        $req = $request->only(config('enums.seo_details_type'));


        foreach($req as $type => $details){
            if($articleCategory->seo()[$type]){
                $articleCategory->seo()[$type]->update($details);
            }
            else{
                $seoDetail = $articleCategory->seoDetails()->create(['type' => config_key('enums.seo_details_type',$type)]);
                $seoDetail->update($details);
            }
        }

        return response('meta saved',200);
    }

    protected array $attachments = [];
    protected array $hyperlinks = [];

    protected function sanitizeContentForDB(string $content): string
    {
        $content = trim($content, " \n\t\r");

        $content = $this->packImages($content);
        $content = $this->packHrefs($content);

        return $content;
    }

    protected function connectAttachments(ArticleCategory $articleCategory)
    {
        foreach ($this->attachments as $attachment) {
            $attachment->fill([
                'attachmentable_type' => array_search(ArticleCategory::class, Relation::$morphMap) ?: null,
                'attachmentable_id'   => $articleCategory->id ?? null,

            ]);
            $attachment->save();
        }
    }

    protected function connectHyperlinks(ArticleCategory $articleCategory)
    {
        foreach ($this->hyperlinks as $hyperlink) {
            $hyperlink->fill([
                'hyperlinkable_type' => array_search(ArticleCategory::class, Relation::$morphMap) ?: null,
                'hyperlinkable_id'   => $articleCategory->id ?? null,

            ]);
            $hyperlink->save();
        }
    }

    protected function getHeaderImageData(Request $request): void
    {
        if ($attach_id = $request->get('header_img')) {
            $attach_id = decrypt($attach_id);

            $attachment = Attachment::findOrFail($attach_id);
            $attachment->makePermanent();
            $this->attachments[] = $attachment;
        }
    }

    protected function packImages(string $content)
    {
        $re = '/<figure[\s\S]+?>\s*?<img[\s\S]+?data-db-id="([\s\S]+?)"[\s\S]*?>\s*?<\/figure>/m';

        $content = preg_replace_callback($re, function ($matches) {
            $return = '';

            $attach_id = $matches[1];

            try {
                if ($attach_id == 'placeholder') {
                    $return = "[img placeholder]";
                } else {
                    $attach_id = decrypt($attach_id);

                    $attachment = Attachment::findOrFail($attach_id);
                    $attachment->makePermanent();
                    $this->attachments[] = $attachment;

                    $return = "[img " . $attachment->id . "]";
                }
            } catch (\Exception $e) {
                getFormedError($e, 'attachment permanent issue');
            } finally {
                return $return;
            }
        }, $content);

        return $content;
    }

    protected function packHrefs(string $content)
    {
        $status = 'requested';

        $re = '/<a[\s\S]+?href="([\s\S]+?)"[\s\S]*?data-config="([\s\S]*?)"[\s\S]*?>([\s\S]*?)<\/a>/m';

        $content = preg_replace_callback($re, function ($matches) use ($status) {
            $return = '';

            $url = Hyperlink::extractUrl($matches[1]);
            $config = $matches[2]; # [ new , nof ]
            $title = $matches[3];

            try {
                $link = Hyperlink::create([
                    'user_id'    => auth()->user()->id ?? null,
                    'title'      => $title,
                    'url_type'   => config_key('enums.hyperlink_type', $url['type']),
                    'url'        => $url['path'],
                    'properties' => [
                        'new_tab' => Str::contains($config, ['new']),
                        'follow'  => !Str::contains($config, ['nof']),
                    ],
                    'status'     => config_key('enums.hyperlinks_status', $status),
                ]);

                $this->hyperlinks[] = $link;

                $return = "[href " . $link->id . "]" . $title . "[/href]";
            } catch (\Exception $e) {
                getFormedError($e, 'hyperlink issue');
            } finally {
                return $return;
            }
        }, $content);

        return $content;
    }

    public function jsonSearch(SearchArticlesRequest $request)
    {
        $territory = $this->checkPermission('view');

        $query = new ArticleCategory;

        $query = $query->where(function ($query) use ($request) {
            $query = $query->where('heading', 'like', '%' . trim($request->term) . '%');
            $query = $query->orWhere('slug', 'like', '%' . trim($request->term) . '%');
            return $query;
        });

//        $query = $query->where('status', config_key('enums.articles_status','active'));

        $query = $query->get();

        $fractal = Fractal::create()
            ->serializeWith(Select2Serializer::class)
            ->collection($query, new Select2ArticleCategoryTransformer());
        return response()->json($fractal);
    }

}
