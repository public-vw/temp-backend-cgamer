<?php

namespace App\Http\Controllers\Admin;

use App\Event\ArticleCreated;
use App\Event\ArticleReversioned;
use App\Event\ArticleStatusChanged;
use App\Event\ArticleContentChanged;
use App\Jobs\SendGooglePing;
use App\Models\SeoDetail;
use Illuminate\Http\Request;
use App\DataTables\Admin\ArticleDataTable;
use App\Http\Requests\Admin\ArticleRequest;
use App\Models\Article;

class ArticleController extends _Controller
{
    protected $model = Article::class;

    public function index(ArticleDataTable $dataTable)
    {
        $territory = $this->checkPermission();

        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function create(Request $request)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(ArticleRequest $request)
    {
        $territory = $this->checkPermission();

        $item = Article::create($request->validated());

        event(new ArticleCreated(auth()->user(),$item));

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $item)
            ->withResult(self::success(
                __('admin/controllers.store.success', ['model' => $this->modelName, 'title' => $item->id])
            ));
    }

    public function show(Article $article)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.show")
            ->withArticle($article);
    }

    public function edit(Request $request, Article $article)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.edit")
                ->withArticle($article);
    }

    public function update(ArticleRequest $request, Article $article)
    {
        $territory = $this->checkPermission();

        $oldStatus = $article->status;

        $article->update($request->validated());

        if($request->status && $request->status != $oldStatus){
            event(new ArticleStatusChanged(auth()->user(),$article,$oldStatus));
        }

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $article)
            ->withResult(self::success(
                __('admin/controllers.update.success', ['model' => $this->modelName, 'title' => $article->id])
            ));
    }

    public function destroy(Article $article)
    {
        $territory = $this->checkPermission();

        abort_if(
            !auth()->user()->hasRole('sysadmin@web')
            && $territory == 'own'
            && (
                !$article->author->user->isSameAsCurrent()
                || !in_array($article->status,config_keys('enums.articles_status',['draft']))
            )
        ,403);

        abort_if(
            !auth()->user()->hasRole('sysadmin@web')
            && $territory == 'branch'
            && (
                !$article->author->user->isChildOfCurrent()
                || !in_array($article->status,config_keys('enums.articles_status',['draft','requested','rejected']))
            )
        ,403);

        $article->hardDelete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('admin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $article->id])
            ));
    }

    public function acceptRevision(Article $article){
        $territory = $this->checkPermission('edit');

        $article->master->update(['status' => config_key('enums.articles_status','updated')]);
        $article->update(['status' => config_key('enums.articles_status','active')]);
        $article->master->seoDetails()->update(['seoable_id' => $article->id]);

        event(new ArticleReversioned(auth()->user(),$article));

        $categories = $article->category->categoryChainJoin('slug','/');
        cache(['article-full-url-to-id.'.crc32($categories.'/'.$article->slug) => $article->id]);

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('admin/controllers.revision.success', ['model' => $this->modelName, 'title' => $article->id])
            ));
    }

    public function sendGooglePing(Article $article){
        if($article->status == config_key('enums.articles_status','active')){
            SendGooglePing::dispatch($article);
        }

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('admin/controllers.pinged.success', ['model' => $this->modelName, 'title' => $article->id])
            ));
    }
}
