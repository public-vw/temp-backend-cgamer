<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\DataTables\Admin\HyperlinkDataTable;
use App\Http\Requests\Admin\HyperlinkRequest;
use App\Models\Hyperlink;

class HyperlinkController extends _Controller
{

    protected $model = Hyperlink::class;

    public function index(HyperlinkDataTable $dataTable)
    {
        $territory = $this->checkPermission();

        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function create(Request $request)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(HyperlinkRequest $request)
    {
        $territory = $this->checkPermission();

        $item = Hyperlink::create($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $item)
            ->withResult(self::success(
                __('admin/controllers.store.success', ['model' => $this->modelName, 'title' => $item->id])
            ));
    }

    public function show(Hyperlink $hyperlink)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.show")
            ->withHyperlink($hyperlink);
    }

    public function edit(Request $request, Hyperlink $hyperlink)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.edit")
                ->withHyperlink($hyperlink);
    }

    public function update(HyperlinkRequest $request, Hyperlink $hyperlink)
    {
        $territory = $this->checkPermission();

        $hyperlink->update($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $hyperlink)
            ->withResult(self::success(
                __('admin/controllers.update.success', ['model' => $this->modelName, 'title' => $hyperlink->id])
            ));
    }

    public function destroy(Hyperlink $hyperlink)
    {
        $territory = $this->checkPermission();

        $hyperlink->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('admin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $hyperlink->id])
            ));
    }
}
