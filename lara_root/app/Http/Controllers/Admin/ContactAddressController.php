<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\DataTables\Admin\ContactAddressDataTable;
use App\Http\Requests\Admin\ContactAddressRequest;
use App\Models\ContactAddress;

class ContactAddressController extends _Controller
{
    protected $model = ContactAddress::class;
    protected $morph = 'addressable';

    public function index(ContactAddressDataTable $dataTable)
    {
        $territory = $this->checkPermission();

        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function create(Request $request)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(ContactAddressRequest $request)
    {
        $territory = $this->checkPermission();

        $item = ContactAddress::forceCreate($request->validated());

        if($request->addressable_type && $request->addressable_id){
            $addressable = modelObj($request->addressable_type);
            $addressable = $addressable->find($request->addressable_id);

            $item->addressable()->associate($addressable)->save();
        }

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $item)
            ->withResult(self::success(
                __('admin/controllers.store.success', ['model' => $this->modelName, 'title' => $item->title])
            ));

    }

    public function edit(Request $request, ContactAddress $contactAddress)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.edit")
                ->withContactAddress($contactAddress);
    }

    public function update(ContactAddressRequest $request, ContactAddress $contactAddress)
    {
        $territory = $this->checkPermission();

        $contactAddress->update($request->validated());

        #TODO!: morph update issue again
        if(
            ($request->addressable_type && $request->addressable_id)
            &&
            (
                $request->addressable_type != $contactAddress->addressable_type
                ||
                $request->addressable_id != $contactAddress->addressable_id
            )
        ){
            $contactAddress->addressable()->dissociate();

            $addressable = modelObj($request->addressable_type);
            $addressable = $addressable->find($request->addressable_id);

            $contactAddress->addressable()->associate($addressable)->save();
        }

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $contactAddress)
            ->withResult(self::success(
                __('admin/controllers.update.success', ['model' => $this->modelName, 'title' => $contactAddress->title])
            ));
    }

    public function destroy(ContactAddress $contactAddress)
    {
        $territory = $this->checkPermission();

        $contactAddress->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('admin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $contactAddress->title])
            ));
    }
}
