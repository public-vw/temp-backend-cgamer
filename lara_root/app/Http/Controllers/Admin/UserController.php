<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\DataTables\Admin\UserDataTable;
use App\Http\Controllers\Admin\Traits\DbList;
use App\Http\Controllers\Admin\Traits\UploadFile;
use App\Http\Requests\Admin\UserRequest;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Illuminate\Support\Str;

class UserController extends _Controller
{
    use DbList;

    protected $model = User::class;
    protected $titleField = 'username';

    public function index(UserDataTable $dataTable)
    {
        $territory = $this->checkPermission();

        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function create(Request $request)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(UserRequest $request)
    {
        $territory = $this->checkPermission();

        $values = $request->validated();

        $values['password'] = (isset($values['password'])) ? Hash::make($values['password']) : Hash::make(Str::random(10));
        $values['random_color'] = (string) rand(0, count(config_keys_all('enums.random_colors')) - 1);

        $item = User::create($values);

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $item)
            ->withResult(self::success(
                __('admin/controllers.store.success', ['model' => $this->modelName, 'title' => $item->name])
            ));

    }

    public function show(User $user)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.show")
            ->withUser($user);
    }

    public function edit(Request $request, User $user)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.edit")
            ->withUser($user);
    }

    public function update(UserRequest $request, User $user)
    {
        $territory = $this->checkPermission();

        $values = $request->validated();
        if(isset($values['password'])) {
            $values['password'] = Hash::make($values['password']);
        }
        else{
            unset($values['password']);
        }
        $user->update($values);

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $user)
            ->withResult(self::success(
                __('admin/controllers.update.success', ['model' => $this->modelName, 'title' => $user->name])
            ));
    }

    public function destroy(User $user)
    {
        $territory = $this->checkPermission();

        if ($user->hasRole('sysadmin@web')) return redirect()->back()->withResult(self::fail('You cannot delete this user!'));
        $user->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('admin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $user->name])
            ));
    }


    public function disableTwoStepAuth(User $user)
    {
        $territory = $this->checkPermission('edit');

        $user->update(['use_two_factor_auth' => false]);

        return redirect()->back()
            ->withResult(self::success(
                __('admin/controllers.disableTwoStepAuth.success', ['title' => $user->name])
            ));
    }

    public function resetPassword(User $user)
    {
        $territory = $this->checkPermission('edit');

        $newPass = randStr(10, [1, 1, 1, 1]);

        $user->update(['password' => Hash::make($newPass)]);

        return redirect()->back()
            ->withResult(self::success(
                __('admin/controllers.resetPassword.success', ['title' => $user->name, 'new_password' => $newPass])
            ));
    }
}
