<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\DataTables\Admin\SeoDetailDataTable;
use App\Http\Controllers\Admin\Traits\UploadFile;
use App\Http\Requests\Admin\SeoDetailRequest;
use App\Models\SeoDetail;

class SeoDetailController extends _Controller
{
    use UploadFile;

    protected $model = SeoDetail::class;
    protected $morph = 'seoable';

    public function index(SeoDetailDataTable $dataTable)
    {
        $territory = $this->checkPermission();

        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function create(Request $request)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(SeoDetailRequest $request)
    {
        $territory = $this->checkPermission();

        $item = SeoDetail::forceCreate($request->validated());

        # TODO: fix upload file
//        $values['thumbnail_id'] = $this->uploadFile($request->file('file'), $request->attachment_type, $request->storage_disk ?? null)->id;

        if($request->seoable_type && $request->seoable_id){
            $seoable = modelObj($request->seoable_type);
            $seoable = $seoable->find($request->seoable_id);

            $item->seoable()->associate($seoable)->save();
        }

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $item)
            ->withResult(self::success(
                __('admin/controllers.store.success', ['model' => $this->modelName, 'title' => $item->title])
            ));
    }

    public function edit(Request $request, SeoDetail $seoDetail)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.edit")
                ->withSeoDetail($seoDetail);
    }

    public function update(SeoDetailRequest $request, SeoDetail $seoDetail)
    {
        $territory = $this->checkPermission();

        # TODO: fix upload file
//        $update = $request->validated();
//        if ($request->hasFile('file')){
//            $update['thumbnail_id'] = $this->uploadFile($request->file('file'), $request->attachment_type, $request->storage_disk ?? null)->id;
//        }

        $seoDetail->update($request->validated());

        #TODO!: morph update issue again
        if(
            ($request->seoable_type && $request->seoable_id)
            &&
            (
                $request->seoable_type != $seoDetail->seoable_type
                ||
                $request->seoable_id != $seoDetail->seoable_id
            )
        ){
            $seoDetail->seoable()->dissociate();

            $seoable = modelObj($request->seoable_type);
            $seoable = $seoable->find($request->seoable_id);

            $seoDetail->seoable()->associate($seoable)->save();
        }

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $seoDetail)
            ->withResult(self::success(
                __('admin/controllers.update.success', ['model' => $this->modelName, 'title' => $seoDetail->title])
            ));
    }

    public function destroy(SeoDetail $seoDetail)
    {
        $territory = $this->checkPermission();

        $seoDetail->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('admin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $seoDetail->title])
            ));
    }
}
