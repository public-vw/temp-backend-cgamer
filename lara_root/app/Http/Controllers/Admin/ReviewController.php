<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Traits\DbList;
use Illuminate\Http\Request;
use App\DataTables\Admin\ReviewDataTable;
use App\Http\Requests\Admin\ReviewRequest;
use App\Models\Review;

class ReviewController extends _Controller
{
    use DbList;

    protected $model = Review::class;
    protected $morph = 'reviewable';

    public function index(ReviewDataTable $reviewDataTable)
    {
        $territory = $this->checkPermission();

        return $reviewDataTable->render('admin.items.reviews.index');
    }

    public function bulk()
    {
        //
    }

    public function search()
    {
        //
    }

    public function create(Request $request)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(ReviewRequest $request)
    {
        $territory = $this->checkPermission();

        $item = Review::forceCreate($request->validated());

        if($request->reviewable_type && $request->reviewable_id){
            $reviewable = modelObj($request->reviewable_type);
            $reviewable = $reviewable->find($request->reviewable_id);

            $item->reviewable()->associate($reviewable)->save();
        }

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $item)
            ->withResult(self::success(
                __('admin/controllers.store.success', ['model' => $this->modelName, 'title' => $item->id])
            ));
    }

    public function show(Review $review)
    {
        $territory = $this->checkPermission();

    }

    public function edit(Request $request, Review $review)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.edit")
            ->withReview($review);
    }

    public function update(ReviewRequest $request, Review $review)
    {
        $territory = $this->checkPermission();

        $review->update($request->validated());

        #TODO!: morph update issue again
        if(
            ($request->reviewable_type && $request->reviewable_id)
            &&
            (
                $request->reviewable_type != $review->reviewable_type
                ||
                $request->reviewable_id != $review->reviewable_id
            )
        ){
            $review->reviewable()->dissociate();

            $reviewable = modelObj($request->reviewable_type);
            $reviewable = $reviewable->find($request->reviewable_id);

            $review->reviewable()->associate($reviewable)->save();
        }

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $review)
            ->withResult(self::success(
                __('admin/controllers.update.success', ['model' => $this->modelName, 'title' => $review->id])
            ));
    }

    public function destroy(Review $review)
    {
        $territory = $this->checkPermission();

        $review->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('admin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $review->id])
            ));

    }
}
