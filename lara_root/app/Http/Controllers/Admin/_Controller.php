<?php


namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Traits\RoleJoint;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

abstract class _Controller extends Controller
{
    use RoleJoint;

    protected $model;       // Full name with namespace
    protected $modelName;   // Just name
    protected $table;

    # TODO: we have to check if we can remove this `modelNamespace` property
    protected $modelNamespace;

    public function __construct()
    {
        parent::__construct();

        $this->modelName = class_basename($this->model);
        $this->table = (new $this->model)->getTable();
        $this->modelNamespace = Str::substr($this->model, 0, -1 * Str::length($this->modelName));
    }
}
