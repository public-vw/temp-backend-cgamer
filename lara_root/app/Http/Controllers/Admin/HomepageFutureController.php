<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\DataTables\Admin\HomepageFutureDataTable;
use App\Http\Requests\Admin\HomepageFutureRequest;
use App\Models\HomepageFuture;

class HomepageFutureController extends _Controller
{
    protected $model = HomepageFuture::class;

    public function index(HomepageFutureDataTable $dataTable)
    {
        $territory = $this->checkPermission();

        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function create(Request $request)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(HomepageFutureRequest $request)
    {
        $territory = $this->checkPermission();

        $item = HomepageFuture::create($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $item)
            ->withResult(self::success(
                __('admin/controllers.store.success', ['model' => $this->modelName, 'title' => $item->id])
            ));
    }

    public function show(HomepageFuture $homepageFuture)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.show")
            ->withHomepageFuture($homepageFuture);
    }

    public function edit(Request $request, HomepageFuture $homepageFuture)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.edit")
                ->withHomepageFuture($homepageFuture);
    }

    public function update(HomepageFutureRequest $request, HomepageFuture $homepageFuture)
    {
        $territory = $this->checkPermission();

        $homepageFuture->update($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $homepageFuture)
            ->withResult(self::success(
                __('admin/controllers.update.success', ['model' => $this->modelName, 'title' => $homepageFuture->id])
            ));
    }

    public function destroy(HomepageFuture $homepageFuture)
    {
        $territory = $this->checkPermission();

        $homepageFuture->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('admin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $homepageFuture->id])
            ));
    }
}
