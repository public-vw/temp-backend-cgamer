<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\DataTables\Admin\HomepageSliderDataTable;
use App\Http\Requests\Admin\HomepageSliderRequest;
use App\Models\HomepageSlider;

class HomepageSliderController extends _Controller
{
    protected $model = HomepageSlider::class;

    public function index(HomepageSliderDataTable $dataTable)
    {
        $territory = $this->checkPermission();

        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function create(Request $request)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(HomepageSliderRequest $request)
    {
        $territory = $this->checkPermission();

        $item = HomepageSlider::create($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $item)
            ->withResult(self::success(
                __('admin/controllers.store.success', ['model' => $this->modelName, 'title' => $item->id])
            ));
    }

    public function show(HomepageSlider $homepageSlider)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.show")
            ->withHomepageSlider($homepageSlider);
    }

    public function edit(Request $request, HomepageSlider $homepageSlider)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.edit")
            ->withHomepageSlider($homepageSlider);
    }

    public function update(HomepageSliderRequest $request, HomepageSlider $homepageSlider)
    {
        $territory = $this->checkPermission();

        $homepageSlider->update($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $homepageSlider)
            ->withResult(self::success(
                __('admin/controllers.update.success', ['model' => $this->modelName, 'title' => $homepageSlider->id])
            ));
    }

    public function destroy(HomepageSlider $homepageSlider)
    {
        $territory = $this->checkPermission();

        $homepageSlider->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('admin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $homepageSlider->id])
            ));
    }
}
