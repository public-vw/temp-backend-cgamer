<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\DataTables\Admin\CityDataTable;
use App\Http\Controllers\Admin\Traits\DbList;
use App\Http\Requests\Admin\CityRequest;
use App\Models\City;

class CityController extends _Controller
{
    use DbList;

    protected $model = City::class;
    protected $dependsOn = 'province_id';

    public function index(CityDataTable $dataTable)
    {
        $territory = $this->checkPermission();

        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function create(Request $request)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(CityRequest $request)
    {
        $territory = $this->checkPermission();

        $item = City::create($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $item)
            ->withResult(self::success(
                __('admin/controllers.store.success', ['model' => $this->modelName, 'title' => $item->title])
            ));
    }

    public function edit(Request $request, City $city)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.edit")
            ->withCity($city);
    }

    public function update(CityRequest $request, City $city)
    {
        $territory = $this->checkPermission();

        $city->update($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $city)
            ->withResult(self::success(
                __('admin/controllers.update.success', ['model' => $this->modelName, 'title' => $city->title])
            ));
    }

    public function destroy(City $city)
    {
        $territory = $this->checkPermission();

        $city->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('admin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $city->title])
            ));
    }
}
