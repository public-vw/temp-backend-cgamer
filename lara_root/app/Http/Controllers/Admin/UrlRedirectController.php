<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\DataTables\Admin\UrlRedirectDataTable;
use App\Http\Requests\Admin\UrlRedirectRequest;
use App\Models\UrlRedirect;

class UrlRedirectController extends _Controller
{
    protected $model = UrlRedirect::class;

    public function index(UrlRedirectDataTable $dataTable)
    {
        $territory = $this->checkPermission();

        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function create(Request $request)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(UrlRedirectRequest $request)
    {
        $territory = $this->checkPermission();

        $item = UrlRedirect::create($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $item)
            ->withResult(self::success(
                __('admin/controllers.store.success', ['model' => $this->modelName, 'title' => $item->id])
            ));
    }

    public function show(UrlRedirect $urlRedirect)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.show")
            ->withUrlRedirect($urlRedirect);
    }

    public function edit(Request $request, UrlRedirect $urlRedirect)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.edit")
                ->withUrlRedirect($urlRedirect);
    }

    public function update(UrlRedirectRequest $request, UrlRedirect $urlRedirect)
    {
        $territory = $this->checkPermission();

        $urlRedirect->update($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $urlRedirect)
            ->withResult(self::success(
                __('admin/controllers.update.success', ['model' => $this->modelName, 'title' => $urlRedirect->id])
            ));
    }

    public function destroy(UrlRedirect $urlRedirect)
    {
        $territory = $this->checkPermission();

        $urlRedirect->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('admin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $urlRedirect->id])
            ));
    }
}
