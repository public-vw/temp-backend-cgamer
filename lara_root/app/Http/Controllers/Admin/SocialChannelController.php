<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\DataTables\Admin\SocialChannelDataTable;
use App\Http\Controllers\Admin\Traits\DbList;
use App\Http\Controllers\Admin\Traits\UploadFile;
use App\Http\Requests\Admin\SocialChannelRequest;
use App\Models\SocialChannel;

class SocialChannelController extends _Controller
{
    use DbList;
    use UploadFile;

    protected $model = SocialChannel::class;

    public function index(SocialChannelDataTable $dataTable)
    {
        $territory = $this->checkPermission();

        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function create(Request $request)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(SocialChannelRequest $request)
    {
        $territory = $this->checkPermission();

        $item = SocialChannel::create($request->validated());

        $this->checkFileField($request, 'thumbnail', $item);

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $item)
            ->withResult(self::success(
                __('admin/controllers.store.success', ['model' => $this->modelName, 'title' => $item->title])
            ));

    }

    public function edit(Request $request, SocialChannel $socialChannel)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.edit")
                ->withSocialChannel($socialChannel);
    }

    public function update(SocialChannelRequest $request, SocialChannel $socialChannel)
    {
        $territory = $this->checkPermission();

        $socialChannel->update($request->validated());

        $this->checkFileField($request, 'thumbnail', $socialChannel);

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $socialChannel)
            ->withResult(self::success(
                __('admin/controllers.update.success', ['model' => $this->modelName, 'title' => $socialChannel->title])
            ));
    }

    public function destroy(SocialChannel $socialChannel)
    {
        $territory = $this->checkPermission();

        $socialChannel->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('admin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $socialChannel->title])
            ));
    }
}
