<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\DataTables\Admin\SocialLinkDataTable;
use App\Http\Requests\Admin\SocialLinkRequest;
use App\Models\SocialLink;

class SocialLinkController extends _Controller
{
    protected $model = SocialLink::class;
    protected $morph = 'socialable';

    public function index(SocialLinkDataTable $dataTable)
    {
        $territory = $this->checkPermission();

        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function create(Request $request)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(SocialLinkRequest $request)
    {
        $territory = $this->checkPermission();

        $item = SocialLink::forceCreate($request->validated());

        if($request->socialable_type && $request->socialable_type){
            $socialable = modelObj($request->socialable_type);
            $socialable = $socialable->find($request->socialable_id);

            $socialable->socialable()->associate($item)->save();
        }

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $item)
            ->withResult(self::success(
                __('admin/controllers.store.success', ['model' => $this->modelName, 'title' => $item->title])
            ));

    }

    public function show(SocialLink $socialLink)
    {
        $territory = $this->checkPermission();

    }

    public function edit(Request $request, SocialLink $socialLink)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.edit")
            ->withSocialLink($socialLink);
    }

    public function update(SocialLinkRequest $request, SocialLink $socialLink)
    {
        $territory = $this->checkPermission();

        $socialLink->update($request->validated());

        #TODO!: morph update issue again
        if(
            ($request->socialable_type && $request->socialable_id)
            &&
            (
                $request->socialable_type != $socialLink->socialable_type
                ||
                $request->socialable_id != $socialLink->socialable_id
            )
        ){
            $socialLink->socialable()->dissociate();

            $socialable = modelObj($request->socialable_type);
            $socialable = $socialable->find($request->socialable_id);

            $socialLink->socialable()->associate($socialable)->save();
        }

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $socialLink)
            ->withResult(self::success(
                __('admin/controllers.update.success', ['model' => $this->modelName, 'title' => $socialLink->title])
            ));
    }

    public function destroy(SocialLink $socialLink)
    {
        $territory = $this->checkPermission();

        $socialLink->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('admin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $socialLink->title])
            ));
    }
}
