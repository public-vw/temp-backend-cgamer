<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\DataTables\Admin\ContactEmailDataTable;
use App\Http\Requests\Admin\ContactEmailRequest;
use App\Models\ContactEmail;

class ContactEmailController extends _Controller
{
    protected $model = ContactEmail::class;
    protected $morph = 'emailable';

    public function index(ContactEmailDataTable $dataTable)
    {
        $territory = $this->checkPermission();

        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function create(Request $request)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(ContactEmailRequest $request)
    {
        $territory = $this->checkPermission();

        $item = ContactEmail::forceCreate($request->validated());

        if($request->emailable_type && $request->emailable_id){
            $emailable = modelObj($request->emailable_type);
            $emailable = $emailable->find($request->emailable_id);

            $item->emailable()->associate($emailable)->save();
        }

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $item)
            ->withResult(self::success(
                __('admin/controllers.store.success', ['model' => $this->modelName, 'title' => $item->title])
            ));

    }

    public function edit(Request $request, ContactEmail $contactEmail)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.edit")
                ->withContactEmail($contactEmail);
    }

    public function update(ContactEmailRequest $request, ContactEmail $contactEmail)
    {
        $territory = $this->checkPermission();

        $contactEmail->update($request->validated());

        #TODO!: morph update issue again
        if(
            ($request->emailable_type && $request->emailable_id)
            &&
            (
                $request->emailable_type != $contactEmail->emailable_type
                ||
                $request->emailable_id != $contactEmail->emailable_id
            )
        ){
            $contactEmail->emailable()->dissociate();

            $emailable = modelObj($request->emailable_type);
            $emailable = $emailable->find($request->emailable_id);

            $contactEmail->emailable()->associate($emailable)->save();
        }

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $contactEmail)
            ->withResult(self::success(
                __('admin/controllers.update.success', ['model' => $this->modelName, 'title' => $contactEmail->title])
            ));
    }

    public function destroy(ContactEmail $contactEmail)
    {
        $territory = $this->checkPermission();

        $contactEmail->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('admin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $contactEmail->title])
            ));
    }
}
