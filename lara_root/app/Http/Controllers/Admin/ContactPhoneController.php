<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\DataTables\Admin\ContactPhoneDataTable;
use App\Http\Requests\Admin\ContactPhoneRequest;
use App\Models\ContactPhone;

class ContactPhoneController extends _Controller
{
    protected $model = ContactPhone::class;
    protected $morph = 'phonable';

    public function index(ContactPhoneDataTable $dataTable)
    {
        $territory = $this->checkPermission();

        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function create(Request $request)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(ContactPhoneRequest $request)
    {
        $territory = $this->checkPermission();

        $item = ContactPhone::forceCreate($request->validated());

        if($request->phonable_type && $request->phonable_id){
            $phonable = modelObj($request->phonable_type);
            $phonable = $phonable->find($request->phonable_id);

            $item->phonable()->associate($phonable)->save();
        }

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $item)
            ->withResult(self::success(
                __('admin/controllers.store.success', ['model' => $this->modelName, 'title' => $item->title])
            ));
    }

    public function edit(Request $request, ContactPhone $contactPhone)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.edit")
                ->withContactPhone($contactPhone);
    }

    public function update(ContactPhoneRequest $request, ContactPhone $contactPhone)
    {
        $territory = $this->checkPermission();

        $contactPhone->update($request->validated());

        #TODO!: morph update issue again
        if(
            ($request->phonable_type && $request->phonable_id)
            &&
            (
                $request->phonable_type != $contactPhone->phonable_type
                ||
                $request->phonable_id != $contactPhone->phonable_id
            )
        ){
            $contactPhone->phonable()->dissociate();

            $phonable = modelObj($request->phonable_type);
            $phonable = $phonable->find($request->phonable_id);

            $contactPhone->phonable()->associate($phonable)->save();
        }

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $contactPhone)
            ->withResult(self::success(
                __('admin/controllers.update.success', ['model' => $this->modelName, 'title' => $contactPhone->title])
            ));
    }

    public function destroy(ContactPhone $contactPhone)
    {
        $territory = $this->checkPermission();

        $contactPhone->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('admin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $contactPhone->title])
            ));

    }
}
