<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Traits\RoleJoint;
use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\ArticleCategory;
use App\Models\Author;

class DashboardController extends Controller
{
    use RoleJoint;

    public function index()
    {
        $data = [
            'categories'   =>
                [
                    'all'       => ArticleCategory::count(),
                    'requested' => ArticleCategory::where('status', config_key('enums.article_categories_status', 'requested'))->count(),
                    'ready' => ArticleCategory::where('status', config_key('enums.article_categories_status', 'ready'))->count(),
                    'active' => ArticleCategory::where('status', config_key('enums.article_categories_status', 'active'))->count(),
                ],
            'articles'   =>
                [
                    'all'       => Article::count(),
                    'requested' => Article::where('status', config_key('enums.articles_status', 'requested'))->count(),
                    'ready' => Article::where('status', config_key('enums.articles_status', 'ready'))->count(),
                    'active' => Article::where('status', config_key('enums.articles_status', 'active'))->count(),
                ],
            'authors' =>
                [
                    'all'       => Author::count(),
                    'requested' => Author::where('status', config_key('enums.authors_status', 'requested'))->count(),
                ],
        ];

        return view("{$this->view}.dashboard")
            ->withData($data);
    }
}
