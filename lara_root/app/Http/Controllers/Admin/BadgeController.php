<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\DataTables\Admin\BadgeDataTable;
use App\Http\Requests\BadgeRequest;
use App\Models\Badge;

class BadgeController extends _Controller
{
    # Uses database listing
    # use DbList;

    protected $model = Badge::class;

    public function index(BadgeDataTable $dataTable)
    {
        $territory = $this->checkPermission();

        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function create(Request $request)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(BadgeRequest $request)
    {
        $territory = $this->checkPermission();

        $item = Badge::create($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $item)
            ->withResult(self::success(
                __('admin/controllers.store.success', ['model' => $this->modelName, 'title' => $item->id])
            ));
    }

    public function show(Badge $badge)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.show")
            ->withBadge($badge);
    }

    public function edit(Request $request, Badge $badge)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.edit")
                ->withBadge($badge);
    }

    public function update(BadgeRequest $request, Badge $badge)
    {
        $territory = $this->checkPermission();

        $badge->update($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $badge)
            ->withResult(self::success(
                __('admin/controllers.update.success', ['model' => $this->modelName, 'title' => $badge->id])
            ));
    }

    public function destroy(Badge $badge)
    {
        $territory = $this->checkPermission();

        $badge->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('admin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $badge->id])
            ));
    }
}
