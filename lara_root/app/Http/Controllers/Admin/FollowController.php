<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\DataTables\Admin\FollowDataTable;
use App\Http\Requests\FollowRequest;
use App\Models\Follow;

class FollowController extends _Controller
{
    # Uses database listing
    # use DbList;

    protected $model = Follow::class;

    public function index(FollowDataTable $dataTable)
    {
        $territory = $this->checkPermission();

        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function create(Request $request)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(FollowRequest $request)
    {
        $territory = $this->checkPermission();

        $item = Follow::create($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $item)
            ->withResult(self::success(
                __('admin/controllers.store.success', ['model' => $this->modelName, 'title' => $item->id])
            ));
    }

    public function show(Follow $follow)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.show")
            ->withFollow($follow);
    }

    public function edit(Request $request, Follow $follow)
    {
        $territory = $this->checkPermission();

        return view("{$this->view}.items.{$this->table}.edit")
                ->withFollow($follow);
    }

    public function update(FollowRequest $request, Follow $follow)
    {
        $territory = $this->checkPermission();

        $follow->update($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $follow)
            ->withResult(self::success(
                __('admin/controllers.update.success', ['model' => $this->modelName, 'title' => $follow->id])
            ));
    }

    public function destroy(Follow $follow)
    {
        $territory = $this->checkPermission();

        $follow->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('admin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $follow->id])
            ));
    }
}
