<?php

namespace App\Http\Controllers\Develop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use PragmaRX\Google2FA\Google2FA;

class WebController extends Controller
{
    function command(Request $request, $key, $command=null, $param=null){
        if(!$this->validateToken($key)) abort(404);

        $ret = null;
        $c = null;
        $have_artisan = false;
        $cmds = [
            'phpinfo'=>'phpinfo',
            'mydbBackup'=>'mydb:backup',
            'version'=>'--version',
            'custom'=>'[Your Wroten Code]',
            'removeDevIPFromVisitors'=>'removeDevIPFromVisitors',
            'addMeAsDev'=>'[Adds your IP as Developer IP]',
            'delIPAsDev'=>'[Remove one IP as Developer IP]',
            'help'=>'help/{command}',
            'git status'=>'status',
            'git pull'=>'pull',
            'br',
            'mailable_email'=>'make:mail',
            'migrateReset'=>'migrate:reset',
            'migrateRefresh'=>'migrate:refresh',
            'migrateRollback'=>'migrate:rollback',
            'migrate'=>'migrate',
            'migrate_creator'=>'migrate creator/{table_name}',
            'migrate_updater'=>'migrate_updater/{table_name}',
            'makeModel'=>'make:model/{model_name}',
            'seed'=>'db:seed',
            'EnvoyFreshDb'=>'envoy run fresh_db',
            'one'=>'migrate:rollback + migrate',
            'all'=>'migrate:refresh + db:seed',
            'br',
            'makeMiddleware'=>'make:middleware/{name}',
            'makeCommand'=>'make:command/{command_title}/{command_folder}/{command_name}',
            'mydbRestore'=>'mydb:restore',
            'changePass'=>'user_id,new_password',
            'br',
            'Database'=>'/qry',
            'Saved Queries'=>'/sqr',
            'Sheller'=>'/shell',
            'DB Executer'=>'/dbq',
            'Laravel Configs'=>'/configs',
            'br',
            'Logout'=>'/logout',

            // 'routeList'=>'route:list',
        ];
        switch ($command) {
            case 'version':
                Artisan::call('',['--version'=>true]);
                $have_artisan = true;
                break;
            case 'changePass':
                if($param) {
                    $params = explode(',',$param);
                    dd($params);
                    $user = \App\User::find($params[0]);
                    if($user->isEmpty()){
                        $c = "User Not Found!<br/>Param1:$params[0]|Param2:$params[1]";
                        break;
                    }
                    $user->password = \Hash::make(trim($params[1]));
                    $user->save();
                    $c = "USER Fullname: ".$user->fullname.'<br/>';
                    $c .= "Password Changed.";
                }
                else $c = "USAGE: ".$cmds[$command];

                break;
            case 'custom':
//                    $usr = \App\User::find(1);
//                    $usr->password = Hash::make('admin1234');
//                    $c = $usr->save();
                break;
            case 'removeDevIPFromVisitors':
                $dev_ips = Branding::getVal('developer_ip');
                $dev_ips2 = \App\Models\PanelVisit::get(['visitor_ip'])->toArray();
                $dev_ips2 = array_map('current',$dev_ips2);
                $dev_ips2 = array_values(array_unique($dev_ips2));
                $dev_ips = array_values(array_unique(array_merge($dev_ips,$dev_ips2)));

                $data = \App\Models\InterfaceVisit::where('is_developer',0)->get(['id','visitor_ip','is_developer']);
                $c = 0;
                foreach($data as $dt)
                {
                    if(!in_array($dt->visitor_ip,$dev_ips))continue;
                    $dt->is_developer = 1;
                    $dt->save();
                    $c++;
                }
                break;
            case 'Database':
                return redirect('qry/'.$key);
                break;
            case 'Sheller':
                return redirect('shell/'.$key);
                break;
            case 'Laravel Configs':
                return redirect('dev/configs/'.$key);
                break;
            case 'DB Executer':
                return redirect('dbq/'.$key);
                break;
            case 'Saved Queries':
                return redirect('sqr/'.$key);
                break;
            case 'phpinfo':
                phpinfo();
                $ret = true;
                break;
            case 'mydbBackup':
                Artisan::call('mydb:backup');
                $have_artisan = true;
                break;
            case 'mydbRestore':
                Artisan::call('mydb:restore');
                $have_artisan = true;
                break;
            case 'makeCommand':
//                  if($param1&&$param2&&$param3) $c = Artisan::call('make:command',['name'=>$param1,'--command'=>$param2.':'.$param3]);
                if($param) {Artisan::call('make:command',['name'=>$param]);$have_artisan = true;}
                else $c = "USAGE: ".$cmds[$command];
                break;
            case 'mailable_email':
                if($param) {Artisan::call('make:mail',['name'=>$param]);$have_artisan = true;}
                else $c = "USAGE: ".$cmds[$command];
                break;
            case 'makeMiddleware':
                if($param) {Artisan::call('make:middleware',['name'=>$param]);$have_artisan = true;}
                else $c = "USAGE: ".$cmds[$command];
                break;
            case 'help':
                if($param) {Artisan::call('help',['name'=>$param]);$have_artisan = true;}
                else $c = "USAGE: ".$cmds[$command];
                break;
            case 'migrateReset':
                Artisan::call('migrate:reset');
                $have_artisan = true;
                break;
            case 'migrateRefresh':
                Artisan::call('migrate:refresh');
                $have_artisan = true;
                break;
            case 'migrateRollback':
                Artisan::call('mydb:backup');
                Artisan::call('migrate:rollback');
                $have_artisan = true;
                break;
            case 'migrate':
                Artisan::call('migrate');
                $have_artisan = true;
                break;
            case 'migrate_creator':
                if($param) {
                    Artisan::call('make:migration',['name'=>"create_{$param}_table",'--create'=>$param]);$have_artisan = true;
                    Artisan::call('mydb:backup');
                }
                else $c = "USAGE: ".$cmds[$command];
                break;
            case 'migrate_updater':
                if($param) {
//                    Artisan::call('mydb:backup');
                    Artisan::call('make:migration',['name'=>"update_{$param}_table",'--table'=>$param]);$have_artisan = true;
                }
                else $c = "USAGE: ".$cmds[$command];
                break;
            case 'makeModel':
                if($param) {Artisan::call('make:model',['name'=>"$param", '-m'=>true]);$have_artisan = true;}
                else $c = "USAGE: ".$cmds[$command];
                break;
            case 'seed':
                Artisan::call('db:seed');
                $have_artisan = true;
                break;
            case 'EnvoyFreshDb':
                runShell('envoy run fresh_db');
                $have_artisan = false;
                break;
            case 'addMeAsDev':
                $tmp = Branding::getVal('developer_ip');
                $c = (isset($_SERVER['HTTP_CF_CONNECTING_IP']))?$_SERVER['HTTP_CF_CONNECTING_IP']:$request->ip();
                if($param) $c = trim($param);
                if(!Branding::isSame('developer_ip',$c))
                {
                    $tmp[] = $c;
                    $tmp = implode(',', $tmp);
                    Branding::where('name','developer_ip')->update(['value'=>$tmp]);
                    return "Added successfully.<br/>Developers: $tmp";
                }
                else{
                    return "Nothing to add!<br/>Developers: ".implode(',',$tmp);
                }
                break;
            case 'delIPAsDev':
                $tmp = Branding::getVal('developer_ip');
                if(!$param) return "USAGE: /[ip]" . "<br/>Current Developers: ".implode(',',$tmp);

                $c = explode(',',$param);
                $tmp = array_diff($tmp, $c);
                $tmp = implode(',', $tmp);
                Branding::where('name','developer_ip')->update(['value'=>$tmp]);
                return "Removed successfully.<br/>Developers: $tmp";
                break;
            case 'one':
                Artisan::call('migrate:rollback');
                Artisan::call('migrate');
                $have_artisan = true;
                break;
            case 'all':
                Artisan::call('mydb:backup');
                Artisan::call('migrate:refresh');
                Artisan::call('db:seed');
                $have_artisan = true;
                break;
            case 'routeList':
                Artisan::call('route:list');
                $have_artisan = true;
                break;
            case 'Logout':
                Session::forget('dev_token');
                Session::put('dev_token_cnt',100000);
                $ret = '<h3>You have logged out successfuly</h3>';
                break;
            case 'git status':
                $ret = runShell('git status');
                $have_artisan = false;
                break;
            case 'git pull':
                $ret = runShell('git pull');
                $have_artisan = false;
                break;
            default:
                $ret="<ul>";
                foreach($cmds as $i=>$desc)
                    $ret.=$desc=='br'?"<br>":"<li><a href='".route('dev.main',[$key,$i])."?rnd=".rand(100,999)."'>$i</a><span style='color:gray; font-size:12px; font-family:arial; margin-left:10px;'>[ $desc ]</span></li>";
                $ret.="</ul>";

        }
        if($ret)return $ret;
        if($have_artisan)dd($c,Artisan::output());
        else dd($c);

    }

    function database(Request $request, $key,$mod = '', $where = null, $order = null){
        if(!$this->validateToken($key)) return abort(404);

        $c = 'Err';
        $ret = null;
        switch ($mod) {
            case 'custom':
                // $c = DB::select('alter table `shopping_bags` change `sessionid` `sessionid` integer(11) ;');
                $c = DB::statement('update products set `active` = true ;');
                break;
            case 'Saved Queries':
                return redirect('sqr/'.$key);
                break;
            case 'Command':
                return redirect('cmd/'.$key);
                break;
            case '':
            case 'tables':
                $c = DB::select('show tables;');
                $c = array_map('current', $c);
                $ret = "<ol>";
                foreach($c as &$i)
                {
                    $cols = DB::select("select column_name from information_schema.columns where table_name='$i';");
                    $cols = array_map('current', $cols);
                    $cols = implode(' | ', $cols);
                    $count = DB::select("select count(*) from $i;");
                    $count = array_map('current', $count);
                    $ret .= "<li><a href='".route('dev.qry',[$key,$i])."?rnd=".rand(100,999)."'>$i </a>(<span style='color:red'>{$count[0]}</span>)<span style='color:gray; font-size:12px; font-family:arial; margin-left:10px;'>[ $cols ]</span></li>";
                }
                $ret .= "<hr/>";
                $ret .= "<li><a href='".route('dev.qry',[$key,"custom"])."'>Custom</a></li>";
                $ret .= "<hr/>";
                $ret .= "<li><a href='".route('dev.main',[$key])."'>Commands</a></li>";
                break;
            default:
                $c = DB::select('select * from '.$mod.(isset($where)?' where '.$where:'').(isset($order)?' order by '.$order:''));
                break;
        }
        if(isset($ret))return $ret;
        dd($c);
    }

    function storedQueries(Request $request, $key, $command=null, $params=null){
        if(!$this->validateToken($key)) return abort(404);

        $ret = null;
        $c = null;
        $have_artisan = false;
        $cmds = [
            'Find Bad Positions'=>'',
            'Postman - Next Products'=>'',
            'Postman - Next Writings'=>'',
            'FileMap - Make Stable All'=>'filemap_query',
            'Keywords Analytics'=>'',
            'Visited Robots'=>'',
            'br',
            'Command'=>'/cmd',
            'Database'=>'/qry',
        ];
        switch ($command) {
            case 'Visited Robots':
                $c = DB::statement(
                    'SELECT user_agent FROM `interface_visits` where user_agent like "%bot%" group by user_agent'
                );
                break;
            case 'Keywords Analytics':
                $c = DB::statement(
                    'SELECT `keyword`, sum(`clicks`),sum(`impressions`) FROM `lgk_keywords`'
                    . 'Group By `keyword` '
                    . 'ORDER BY sum(`clicks`) DESC,sum(`impressions`) DESC'
                );
                break;
            case 'FileMap - Make Stable All':
                $c = DB::statement('UPDATE file_maps SET flag="stable" WHERE flag="new" OR  flag="changed";');
                break;
            case 'Find Bad Positions':
                $c = [
                    DB::select('SELECT pr.id as pr_id,pr.cat_id,pr.postman_active, se.id as se_id, se.item_id, se.created_at FROM `seo_positions` as se left JOIN products as pr on se.item_id=pr.id where pr.cat_id is null;')
                    , DB::select('SELECT pr.id as pr_id,pr.cat_id,pr.postman_active, se.id as se_id, se.item_id, se.created_at FROM `seo_positions` as se JOIN products as pr on se.item_id=pr.id where length(se.cat_id)=0;')
                ];
                break;
            case 'Postman - Next Products':
                $c = \App\Models\Product::where('onair',false)->where('postman_active',true)->orderBy('updated_at','ASC')->skip(0)->take(\App\Models\PostmanConfig::getConfig('Product_Count'))->get(['id','title']);
                $c = array_combine($c->pluck('id')->toArray(),$c->pluck('title')->toArray());
                $c = [$c , 'NextRun'=>\App\Models\PostmanConfig::getConfig('Product_NextRun')];
                break;
            case 'Postman - Next Writings':
                $c = \App\Models\Writing::where('active',false)->where('postman_active',true)->orderBy('updated_at','ASC')->skip(0)->take(\App\Models\PostmanConfig::getConfig('Writing_Count'))->get(['id','title']);
                $c = array_combine($c->pluck('id')->toArray(),$c->pluck('title')->toArray());
                $c = [$c , 'NextRun'=>\App\Models\PostmanConfig::getConfig('Writing_NextRun')];
                break;
            case 'Command':
                return redirect('cmd/'.$key);
                break;
            case 'Database':
                return redirect('qry/'.$key);
                break;
            default:
                $ret="<ul>";
                foreach($cmds as $i=>$desc)
                    $ret.=$desc=='br'?"<br>":"<li><a href='".route('dev.sqr',[$key,$i])."?rnd=".rand(100,999)."'>$i</a></li>";
                $ret.="</ul>";
                break;
        }
        if(isset($ret))return $ret;
        dd($c);
    }

    function sheller(Request $request,$token) {
        if(!$this->validateToken($token)) return abort(404);

        $cmds = ['shell_exec','exec','system','passthru'];
        $funcs = [];foreach($cmds as $cm) $funcs[$cm] = isEnabledFunc($cm);

        $ret = null;
        if($request->has('cm')) {
            switch($request->func){
                case 'shell_exec':
                    $ret = shell_exec(trim($request->get('cm')));
                    break;
                case 'exec':
                    $ret = exec(trim($request->get('cm')));
                    break;
                case 'system':
                    $ret = system(trim($request->get('cm')));
                    break;
                case 'passthru':
                    $ret = passthru(trim($request->get('cm')));
                    break;
                default:
                    $ret = runShell(trim($request->get('cm')));
            }
        }
        return view('dev.commander',['result'=>$ret,'funcs'=>$funcs,'cmd'=>trim($request->get('cm'))]);
    }

    function queryExecuter(Request $request,$token) {
        if(!$this->validateToken($token)) return abort(404);

        $ret = null;
        if($request->has('qry')) {
            $ret = DB::select(DB::raw(trim($request->get('qry'))));
        }
        return view('dev.dbquery',['result'=>var_dump($ret,true),'qry'=>trim($request->get('qry'))]);
    }

    function showConfigs(Request $request,$token) {
        if(!$this->validateToken($token)) return abort(404);

        return view('dev.configs')
            ->withEnvs($_ENV)
            ->withConfigs(Config::all());
    }

    function generateGCode(Request $request,$key) {
        if($key != '123124') return abort(404);

        $google2fa = new PragmaRX\Google2FA\Google2FA();
        $secretKey = $google2fa->generateSecretKey();

        $inlineUrl = $google2fa->getQRCodeInline(
            'Karin Group G-Code',
            'karingroup2015@gmail.com',
            $secretKey
        );

        $ret = '';

        $ret .= '<center>';
        $ret .= "<img src='$inlineUrl'><br/>";
        $ret .= "<input type='text' value='$secretKey' style='text-align:center'/>";
        $ret .= '</center>';

        return $ret;
    }

    function generateTempToken(Request $request,$resp=null) {
        $ret = '';

        if(is_null($resp) || !Session::has('temp_token'))
        {
            $key = 0;
            if(!Session::has('temp_token') || (time() - Session::get('temp_token') > 60 )){
                $key = rand(1,100);
                Session::put('temp_token',time());
                Session::put('temp_token_key',$key);
            }

            $ret .= '<center>';
            $ret .= "<input type='text' value='$key' style='text-align:center'/>";
            $ret .= '</center>';
        }
        else
        {
            $key = Session::get('temp_token_key');
            if(intval($resp) + $key == 200)         // <-- Validation Rule (Make this more complicated)
            {                                       // Passed
                Session::put('dev_token_cnt',0);
                Session::put('dev_token',time());
                return redirect('/cmd/000000');
            }
            else
            {                                       // NOT Passed
                Session::forget('temp_token');
                Session::forget('temp_token_key');
                return redirect('/token/123124');
            }
        }

        return $ret;
    }

    private function validateToken($token){
        if(!Session::has('dev_token') || (time() - Session::get('dev_token') > 60 || Session::get('dev_token_cnt')>=50 )){
            Session::forget('dev_token');
            Session::put('dev_token_cnt',0);

            $google2fa = new Google2FA();
            $valid = $google2fa->verifyKey(config('app.gcode'), $token);
            if(!$valid) return false;
        }
        Session::put('dev_token_cnt',Session::get('dev_token_cnt') + 1);
        Session::put('dev_token',time());
        return true;
    }

}
