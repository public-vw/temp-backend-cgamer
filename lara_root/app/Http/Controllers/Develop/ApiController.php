<?php

namespace App\Http\Controllers\Develop;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use PragmaRX\Google2FA\Google2FA;

class ApiController extends Controller
{
    public function clock(Request $request)
    {
        if(!$this->validateToken($request)) return abort(404);

        return response()->json([
            'server'   => runShell('date'),
            'timezone' => config('app.timezone'),
            'date'     => Carbon::now()->format('Y-m-d'),
            'time'     => Carbon::now()->format('H:i:s.z'),
        ]);
    }

    private function validateToken(Request $request){
        if(!Cache::has('dev_token') || Cache::get('dev_token_cnt')>=50 ){
            Cache::put('dev_token_cnt',0);

            if(!$request->has('token')) return false;

            $google2fa = new Google2FA();
            $valid = $google2fa->verifyKey(config('app.gcode'), $request->token);
            if(!$valid) return false;
        }
        Cache::put('dev_token_cnt',(Cache::get('dev_token_cnt') ?? 0) + 1);
        Cache::put('dev_token',time(),60);
        return true;
    }

}
