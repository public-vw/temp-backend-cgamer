<?php

namespace App\Http\Controllers\Sysadmin;

use App\Http\Controllers\Controller;

class CompoGenController extends Controller
{
    protected $component_groups = [
        'dropdowns' => [
            'Drop Downs',
            'children' => [
                'select'  => 'Simple Select',
                'select2' => 'Select2'
            ],
        ],
    ];


    public function index(){
        return view('sysadmin.items.compo_gen.index')->withComponentGroups($this->component_groups);
    }

    public function form($component_address){
        $component = explode('.',$component_address);
        $title = $this->component_groups[$component[0]]['children'][$component[1]];

        return view('sysadmin.items.compo_gen.'.$component_address)
            ->withComponentTitle($title);
    }


}
