<?php

namespace App\Http\Controllers\Sysadmin;

use Illuminate\Http\Request;
use App\DataTables\Sysadmin\SiteBrandDataTable;
use App\Http\Requests\Sysadmin\SiteBrandRequest;
use App\Models\SiteBrand;

class SiteBrandController extends _Controller
{
    protected $model = SiteBrand::class;

    public function index(SiteBrandDataTable $dataTable)
    {
        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function create(Request $request)
    {
        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(SiteBrandRequest $request)
    {
        $item = SiteBrand::create($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $item)
            ->withResult(self::success(
                __('sysadmin/controllers.store.success', ['model' => $this->modelName, 'title' => $item->id])
            ));
    }

    public function show(SiteBrand $site_brand)
    {
        return view("{$this->view}.items.{$this->table}.show")
            ->withSiteBrand($site_brand);
    }

    public function edit(Request $request, SiteBrand $site_brand)
    {
        return view("{$this->view}.items.{$this->table}.edit")
                ->withSiteBrand($site_brand);
    }

    public function update(SiteBrandRequest $request, SiteBrand $site_brand)
    {
        $site_brand->update($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $site_brand)
            ->withResult(self::success(
                __('sysadmin/controllers.update.success', ['model' => $this->modelName, 'title' => $site_brand->id])
            ));
    }

    public function destroy(SiteBrand $site_brand)
    {
        $site_brand->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('sysadmin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $site_brand->id])
            ));
    }
}
