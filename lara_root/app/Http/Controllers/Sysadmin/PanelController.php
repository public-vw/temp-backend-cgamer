<?php

namespace App\Http\Controllers\Sysadmin;

use App\DataTables\Sysadmin\PanelDataTable;
use App\Http\Controllers\Sysadmin\Traits\DbList;
use App\Http\Requests\Sysadmin\PanelRequest;
use App\Models\Panel;
use Illuminate\Http\Request;

class PanelController extends _Controller
{
    use DbList;

    protected $model = Panel::class;

    public function index(PanelDataTable $dataTable)
    {
        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function create(Request $request)
    {
        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(PanelRequest $request)
    {
        return back()->withResult(self::fail('Not operational yet!'));
    }

    public function show(Panel $panel)
    {
        session(['panel' => $panel]);

        return redirect()->route('sysadmin.home');
    }

    public function edit(Request $request, Panel $panel)
    {
        return view("{$this->view}.items.{$this->table}.edit")->withItem($panel);
    }

    public function update(PanelRequest $request, Panel $panel)
    {
        $panel->update($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $panel)
            ->withResult(self::success(
                __('sysadmin/controllers.update.success', ['model' => $this->modelName, 'title' => $panel->id])
            ));
    }

    public function destroy(Panel $panel)
    {
        return back()->withResult(self::fail('Not operational yet!'));
    }


    public function exit(Request $request)
    {
        $request->session()->forget('panel');

        return redirect()->route('sysadmin.home');
    }

}
