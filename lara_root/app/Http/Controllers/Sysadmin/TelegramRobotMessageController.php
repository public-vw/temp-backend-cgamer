<?php

namespace App\Http\Controllers\Sysadmin;

use Illuminate\Http\Request;
use App\DataTables\Sysadmin\TelegramRobotMessageDataTable;
use App\Http\Requests\Sysadmin\TelegramRobotMessageRequest;
use App\Models\TelegramRobotMessage;

class TelegramRobotMessageController extends _Controller
{
    protected $model = TelegramRobotMessage::class;

    public function index(TelegramRobotMessageDataTable $dataTable)
    {
        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function create(Request $request)
    {
        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(TelegramRobotMessageRequest $request)
    {
        $item = TelegramRobotMessage::create($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $item)
            ->withResult(self::success(
                __('sysadmin/controllers.store.success', ['model' => $this->modelName, 'title' => $item->id])
            ));
    }

    public function show(TelegramRobotMessage $telegramRobotMessage)
    {
        return view("{$this->view}.items.{$this->table}.show")
            ->withTelegramRobotMessage($telegramRobotMessage);
    }

    public function edit(Request $request, TelegramRobotMessage $telegramRobotMessage)
    {
        return view("{$this->view}.items.{$this->table}.edit")
                ->withTelegramRobotMessage($telegramRobotMessage);
    }

    public function update(TelegramRobotMessageRequest $request, TelegramRobotMessage $telegramRobotMessage)
    {
        $telegramRobotMessage->update($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $telegramRobotMessage)
            ->withResult(self::success(
                __('sysadmin/controllers.update.success', ['model' => $this->modelName, 'title' => $telegramRobotMessage->id])
            ));
    }

    public function destroy(TelegramRobotMessage $telegramRobotMessage)
    {
        $telegramRobotMessage->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('sysadmin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $telegramRobotMessage->id])
            ));
    }
}
