<?php

namespace App\Http\Controllers\Sysadmin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Sysadmin\PasswordRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use PragmaRX\Google2FA\Google2FA;
use SimpleSoftwareIO\QrCode\Facades\QrCode;


class ProfileController extends Controller
{
    public function edit(Request $request)
    {
        $user = auth()->user();

        return view('sysadmin.items.profile.information')
            ->withUser($user);
    }

    public function showPasswordForm(Request $request)
    {
        $user = auth()->user();

        $google_token = null;
        if(!$user->use_two_factor_auth){

            $google2fa = new Google2FA();
            if(!$user->google_secret_key){
                $user->update(['google_secret_key'=>$google2fa->generateSecretKey()]);
            }

            $google_token = $google2fa->getQRCodeUrl(
                config('app.name'),
                $user->email,
                $user->google_secret_key
            );
            $google_token = QrCode::size('150')->eye('circle')->errorCorrection('H')->style('dot')->generate($google_token);
        }


        return view('sysadmin.items.profile.change_password')
            ->withUser($user)
            ->withGoogleToken($google_token);
    }

    public function update(Request $request)
    {
        $user = auth()->user();
        $user->update($request->all());

        return redirect()->back()
            ->withResult(self::success(
                __('sysadmin/controllers.update.success', ['model' => 'User', 'title' => $user->name])
            ));
    }

    public function changePassword(PasswordRequest $request){
        $user = auth()->user();

        if(!Hash::check($request->curr_password, $user->getAuthPassword())) {
            return redirect()->back()
                ->withResult(self::fail(
                    __('Access denied!')
                ));
        }

        $user->password = Hash::make($request->new_password);
        $user->save();

        return redirect()->back()
            ->withResult(self::success(
                __('sysadmin/controllers.update.success', ['model' => 'User', 'title' => $user->name])
            ));

    }

    public function setTwoStepAuth(Request $request){
        $user = auth()->user();

        $google2fa = new Google2FA();
        if(!$google2fa->verifyKey($user->google_secret_key, $request->gtoken)){
            return redirect()->back()
                ->withResult(self::fail(
                    __('Google Token Mismatch!')
                ));
        }

        $user->use_two_factor_auth = true;
        $user->save();

        return redirect()->back()
            ->withResult(self::success(
                __('sysadmin/controllers.update.success', ['model' => 'User', 'title' => $user->name])
            ));
    }

    public function removeTwoStepAuth(Request $request){
        $user = auth()->user();

        $google2fa = new Google2FA();
        if(!$google2fa->verifyKey($user->google_secret_key, $request->gtoken)){
            return redirect()->back()
                ->withResult(self::fail(
                    __('Google Token Mismatch!')
                ));
        }

        $user->google_secret_key = null;
        $user->use_two_factor_auth = false;
        $user->save();

        return redirect()->back()
            ->withResult(self::success(
                __('sysadmin/controllers.update.success', ['model' => 'User', 'title' => $user->name])
            ));
    }


}
