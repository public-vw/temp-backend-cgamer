<?php

namespace App\Http\Controllers\Sysadmin;

use Illuminate\Http\Request;
use App\DataTables\Sysadmin\FailedJobDataTable;
use App\Http\Requests\Sysadmin\FailedJobRequest;
use App\Models\FailedJob;

class FailedJobController extends _Controller
{
    protected $model = FailedJob::class;

    public function index(FailedJobDataTable $dataTable)
    {
        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function create(Request $request)
    {
        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(FailedJobRequest $request)
    {
        $item = FailedJob::create($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $item)
            ->withResult(self::success(
                __('sysadmin/controllers.store.success', ['model' => $this->modelName, 'title' => $item->id])
            ));
    }

    public function show(FailedJob $failed_job)
    {
        return view("{$this->view}.items.{$this->table}.show")
            ->withFailedJob($failed_job);
    }

    public function edit(Request $request, FailedJob $failed_job)
    {
        return view("{$this->view}.items.{$this->table}.edit")
                ->withFailedJob($failed_job);
    }

    public function update(FailedJobRequest $request, FailedJob $failed_job)
    {
        $failed_job->update($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $failed_job)
            ->withResult(self::success(
                __('sysadmin/controllers.update.success', ['model' => $this->modelName, 'title' => $failed_job->id])
            ));
    }

    public function destroy(FailedJob $failed_job)
    {
        $failed_job->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('sysadmin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $failed_job->id])
            ));
    }
}
