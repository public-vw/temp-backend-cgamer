<?php

namespace App\Http\Controllers\Sysadmin;

use App\Http\Controllers\Sysadmin\Traits\ConfigList;
use App\Http\Controllers\Sysadmin\Traits\DbList;
use App\Http\Requests\Sysadmin\MenuRequest;
use App\Models\Menu;
use Illuminate\Http\Request;

class MenuController extends _Controller
{
    use DbList,ConfigList;

    protected $model = Menu::class;
    protected $dependsOn = 'placeholder_id';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            $panel = session('panel',false);
            if($panel === false) return redirect()->route('sysadmin.home');
            $request->attributes->set('panel' , $panel);
            return $next($request);
        });
    }

    public function index(Request $request)
    {
        $panel = $request->attributes->get('panel');

        $items = $panel->menus()->paginate(5)->withQueryString();
        return view("{$this->view}.items.{$this->table}.list.body")
            ->withItems($items);
    }

    public function create(Request $request)
    {
        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(MenuRequest $request)
    {
        $item = Menu::create($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $item)
            ->withResult(self::success(
                __('sysadmin/controllers.store.success', ['model' => $this->modelName, 'title' => $item->title])
            ));
    }

    public function edit(Request $request, Menu $menu)
    {
        return view("{$this->view}.items.{$this->table}.edit")
            ->withItem($menu)
            ->withQueryString(['page']);
    }

    public function update(MenuRequest $request, Menu $menu)
    {
        $menu->update($request->validated());

//        TODO: check if possible to remove or not
//        $update = $request->validated();
//        $nullables = ['placeholder_id', 'parent_id', 'url_type'];
//        foreach ($nullables as $nullable) {
//            $update[$nullable] ??= null;
//        }

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $menu)
            ->withResult(self::success(
                __('sysadmin/controllers.update.success', ['model' => $this->modelName, 'title' => $menu->id])
            ));
    }

    public function destroy(Menu $menu)
    {
        $menu->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('sysadmin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $menu->title])
            ));
    }
}
