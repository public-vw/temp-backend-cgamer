<?php

namespace App\Http\Controllers\Sysadmin;

use Illuminate\Http\Request;
use App\DataTables\Sysadmin\BotConnectionDataTable;
use App\Http\Requests\Sysadmin\BotConnectionRequest;
use App\Models\BotConnection;

class BotConnectionController extends _Controller
{
    protected $model = BotConnection::class;

    public function index(BotConnectionDataTable $dataTable)
    {
        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function create(Request $request)
    {
        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(BotConnectionRequest $request)
    {
        $item = BotConnection::create($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $item)
            ->withResult(self::success(
                __('sysadmin/controllers.store.success', ['model' => $this->modelName, 'title' => $item->id])
            ));
    }

    public function show(BotConnection $botConnection)
    {
        return view("{$this->view}.items.{$this->table}.show")
            ->withBotConnection($botConnection);
    }

    public function edit(Request $request, BotConnection $botConnection)
    {
        return view("{$this->view}.items.{$this->table}.edit")
                ->withBotConnection($botConnection);
    }

    public function update(BotConnectionRequest $request, BotConnection $botConnection)
    {
        $botConnection->update($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $botConnection)
            ->withResult(self::success(
                __('sysadmin/controllers.update.success', ['model' => $this->modelName, 'title' => $botConnection->id])
            ));
    }

    public function destroy(BotConnection $botConnection)
    {
        $botConnection->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('sysadmin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $botConnection->id])
            ));
    }
}
