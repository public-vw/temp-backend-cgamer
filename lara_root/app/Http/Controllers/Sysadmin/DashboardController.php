<?php

namespace App\Http\Controllers\Sysadmin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Sysadmin\Traits\RoleJoint;

class DashboardController extends Controller
{
    use RoleJoint;

    public function index()
    {
        $branch = trim(runShell('git branch --show-current'));
        $version = runShell('git rev-list --max-count=1 '.$branch);
        $details = runShell('git rev-list --format="%s | %ar" --max-count=4 '.$branch);

        return view("{$this->view}.dashboard")
            ->with('branch',$branch)
            ->with('details',$details)
            ->with('version',$version);
    }
}
