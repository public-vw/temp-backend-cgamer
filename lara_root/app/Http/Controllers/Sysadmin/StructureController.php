<?php

namespace App\Http\Controllers\Sysadmin;

use App\Http\Controllers\Sysadmin\Traits\DbList;
use App\Models\Panel;
use Illuminate\Http\Request;
use App\DataTables\Sysadmin\StructureDataTable;
use App\Http\Requests\Sysadmin\StructureRequest;
use App\Models\Structure;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Str;

class StructureController extends _Controller
{
    use DbList;

    protected $dependsOn = 'type';

    protected $model = Structure::class;

    public function index(StructureDataTable $dataTable)
    {
        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function createModel(Request $request)
    {
        $panels = Panel::all();
        return view("{$this->view}.items.{$this->table}.createModel")
            ->withPanels($panels);
    }

    public function createField(Request $request)
    {
        return view("{$this->view}.items.{$this->table}.createField");
    }

    public function storeModel(StructureRequest $request)
    {
        $requests = $request->validated();

        $model = Str::ucfirst(str_replace(' ','',$requests['title']));
        $panels = $requests['panels'] ?? [];
        $options = $requests['options'] ?? [];

        $this->createController($options, $panels, $model);
        $this->createRoute($options, $panels, $model);
        $this->createView($options, $panels, $model);
        $this->createModelFile($options, $model);
        $this->createMigration($options, $model);
        $this->createFactory($options, $model);
        $this->createSeeder($options, $model);
        $this->createPermissions($options, $model);

        $item = Structure::create($requests);

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $model)
            ->withResult(self::success(
                __('sysadmin/controllers.store.success', ['model' => $this->modelName, 'title' => $item->id])
            ));
    }

    protected function createController($options, $panels, $model)
    {
        if (!isset($options['controller'])) return false;

        if (isset($options['transformer'])) {
            $this->runArtisan('transformer', [$model]);
        }

        if (empty($panels)) return true;

        foreach ($panels as $panel) {
            if (isset($options['request'])) {
                $this->runArtisan('request', [$panel, $model]);
            }

            if (isset($options['datatable'])) {
                $this->runArtisan('datatable', [$panel, $model]);
            }
            $this->runArtisan('controller', [$panel, $model]);
        }

        return true;
    }

    protected function createRoute($options, $panels, $model)
    {
        if (!isset($options['route'])) return false;
        if (empty($panels)) return false;

        foreach ($panels as $panel) {
            $this->runArtisan('route', [$panel, $model]);
        }

        return true;
    }

    protected function createView($options, $panels, $model)
    {
        if (!isset($options['view'])) return false;
        if (empty($panels)) return false;

        foreach ($panels as $panel) {
            $this->runArtisan('views', [$panel, $model]);
        }

        return true;
    }

    protected function createPermissions($options, $model)
    {
        if (!isset($options['permissions'])) return false;
        $this->runArtisan('permissions', [$model]);
        return true;
    }

    protected function createMigration($options, $model)
    {
        if (!isset($options['migration'])) return false;
        $this->runArtisan('migration', [$model]);
        return true;
    }

    protected function createSeeder($options, $model)
    {
        if (!isset($options['seeder'])) return false;
        $this->runArtisan('seeder', [$model]);
        return true;
    }

    protected function createFactory($options, $model)
    {
        if (!isset($options['factory'])) return false;
        $this->runArtisan('factory', [$model]);
        return true;
    }

    protected function createModelFile($options, $model)
    {
        if (!isset($options['model'])) return false;
        $this->runArtisan('model', [$model]);
        return true;
    }

    protected function show(Structure $structure){
        $panels = Panel::all(['title'])->pluck('title')->toArray();
        array_push($panels, 'sysadmin');

        return view("{$this->view}.items.{$this->table}.check")
            ->withPanels($panels)
            ->withItem($structure);
    }

    /*
        public function edit(Request $request, Structure $structure)
        {
            return view("{$this->view}.items.{$this->table}.edit")
                    ->withStructure($structure);
        }

        public function update(StructureRequest $request, Structure $structure)
        {
            $structure->update($request->validated());

            return self::smartRedirect($request, "{$this->role}.{$this->table}", $structure)
                ->withResult(self::success(
                    __('sysadmin/controllers.update.success', ['model' => $this->modelName, 'title' => $structure->id])
                ));
        }

        public function destroy(Structure $structure)
        {
            $structure->delete();

            return redirect()->route("{$this->role}.{$this->table}.index")
                ->withResult(self::success(
                    __('sysadmin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $structure->id])
                ));
        }*/

    protected function runArtisan(string $command, array $params): string
    {
        $params = implode(' ', $params);
        $command = "structure:$command $params";
        return Artisan::call($command);
    }

}
