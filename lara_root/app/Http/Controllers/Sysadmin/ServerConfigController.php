<?php

namespace App\Http\Controllers\Sysadmin;

use App\DataTables\Sysadmin\ServerConfigDataTable;
use App\Models\ServerConfig;
use App\Support\ServerConfig\ServerConfigCheck;
use Illuminate\Http\Request;

class ServerConfigController extends _Controller
{
    protected $model = ServerConfig::class;

    public function index(ServerConfigDataTable $dataTable)
    {
        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function create(Request $request)
    {
        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(Request $request)
    {
        $item = ServerConfig::create($request->all());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $item)
            ->withResult(self::success(
                __('sysadmin/controllers.store.success', ['model' => $this->modelName, 'title' => $item->key])
            ));
    }

    public function show(ServerConfig $serverConfig)
    {
        return back()->withResult(self::fail('Not operational yet!'));
    }

    public function destroy(ServerConfig $serverConfig)
    {
        $serverConfig->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('sysadmin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $serverConfig->key])
            ));
    }

    public function check(ServerConfig $serverConfig)
    {
        ServerConfigCheck::check($serverConfig->key);
        return back()->withResult(self::success('State of ' . $serverConfig->key . 'updated successfully'));
    }
}
