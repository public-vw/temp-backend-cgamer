<?php

namespace App\Http\Controllers\Sysadmin\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Symfony\Component\Console\Output\BufferedOutput;

class BackupController extends Controller
{
    protected $rcloneConfigName = 'mr_backup_cryptobaz';

    public function dbBackup(Request $request)
    {
        $ret = new BufferedOutput();
        Artisan::call('mydb:backup -u -k',[],$ret);
        return $ret->fetch();
    }

    public function uploadsBackup(Request $request)
    {
        $ret = new BufferedOutput();
        Artisan::call('myup:backup -u -k',[],$ret);
        return $ret->fetch();
    }

    public function deleteLocalBackup(Request $request)
    {
        $simulate = $request->simulate ?? false;

        $query = "*{$request->filecode}*.tar.gz";

        if($simulate == 'on'){
            $ret = runShell(
                'cd '.base_path('DB_Backups').'; ' .
                "find . -name \"{$query}\" -type f -exec stat --format '%n' \"{}\" \; | sort -n | cut -d: -f2-"
                ,true);
            return !empty($ret['errors']) ? $ret['errors'] : "These backups will be deleted:\n".$ret['output'];
        }

        return runShell('cd '.base_path('DB_Backups').'; ' .
            "rm -f -v \"{$query}\""
        ,true);
    }

    public function listLocalBackup(Request $request)
    {
        $mode = $request->mode ?? '*'; // ups | db | null = all
        $term = $request->filecode ? $request->filecode . '*' : '';

        return runShell(
            'cd '.base_path('DB_Backups').'; ' .
            "find . -name \"{$mode}_*{$term}.tar.gz\" -type f -exec stat --format '%n' \"{}\" \; | sort -n | cut -d: -f2-"
            ,true);
    }

    public function uploadToRemoteBackup(Request $request)
    {
        $mode = $request->mode ?? '*'; // ups | db | null = all
        $term = $request->filecode ?? '';

        $simulate = $request->simulate ?? false;

        $query = "{$mode}_*{$term}*.tar.gz";

        if($simulate == 'on') {
            $ret = runShell(
                'cd ' . base_path('DB_Backups') . '; ' .
                "find . -name \"{$query}\" -type f -exec stat --format '%n' \"{}\" \; | sort -n | cut -d: -f2-"
                , true);

            return !empty($ret['errors']) ? $ret['errors'] : "These backups will be uploaded to remote:\n".$ret['output'];
        }

        return runShell(
            'cd '.base_path('DB_Backups').'; ' .
            "rclone copy . --include \"{$query}\" {$this->rcloneConfigName}:/ -v ".($simulate == 'on'?'--dry-run':'')
            ,true);
    }
}
