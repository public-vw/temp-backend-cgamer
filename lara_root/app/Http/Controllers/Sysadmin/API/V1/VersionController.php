<?php

namespace App\Http\Controllers\Sysadmin\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Artisan;
use Symfony\Component\Console\Output\BufferedOutput;

class VersionController extends Controller
{
    public function state()
    {
        $ret = new BufferedOutput();
        Artisan::call('repo:state',[],$ret);
        return $ret->fetch();
    }

    public function status()
    {
        $ret = new BufferedOutput();
        Artisan::call('repo:status',[],$ret);
        return $ret->fetch();
    }

    public function pull()
    {
        $ret = new BufferedOutput();
        Artisan::call('repo:pull',[],$ret);
        return $ret->fetch();
    }

    public function stash()
    {
        $ret = new BufferedOutput();
        Artisan::call('repo:stash',[],$ret);
        return $ret->fetch();
    }
}
