<?php

namespace App\Http\Controllers\Sysadmin\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function test(Request $request)
    {
        return $request->user();
    }
}
