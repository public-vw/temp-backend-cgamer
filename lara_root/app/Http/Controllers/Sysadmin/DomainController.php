<?php

namespace App\Http\Controllers\Sysadmin;

use App\Http\Controllers\Sysadmin\Traits\DbList;
use Illuminate\Http\Request;
use App\DataTables\Sysadmin\DomainDataTable;
use App\Http\Requests\Sysadmin\DomainRequest;
use App\Models\Domain;

class DomainController extends _Controller
{
    use DbList;

    protected $model = Domain::class;

    public function index(DomainDataTable $dataTable)
    {
        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function create(Request $request)
    {
        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(DomainRequest $request)
    {
        $item = Domain::create($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $item)
            ->withResult(self::success(
                __('sysadmin/controllers.store.success', ['model' => $this->modelName, 'title' => $item->id])
            ));
    }

    public function show(Domain $domain)
    {
        return view("{$this->view}.items.{$this->table}.show")
            ->withDomain($domain);
    }

    public function edit(Request $request, Domain $domain)
    {
        return view("{$this->view}.items.{$this->table}.edit")
                ->withDomain($domain);
    }

    public function update(DomainRequest $request, Domain $domain)
    {
        $domain->update($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $domain)
            ->withResult(self::success(
                __('sysadmin/controllers.update.success', ['model' => $this->modelName, 'title' => $domain->id])
            ));
    }

    public function destroy(Domain $domain)
    {
        $domain->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('sysadmin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $domain->id])
            ));
    }
}
