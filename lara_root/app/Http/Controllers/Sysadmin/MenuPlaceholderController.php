<?php

namespace App\Http\Controllers\Sysadmin;

use App\DataTables\Sysadmin\MenuPlaceholderDataTable;
use App\Http\Controllers\Sysadmin\Traits\DbList;
use App\Http\Requests\Sysadmin\MenuPlaceholderRequest;
use App\Models\MenuPlaceholder;
use Illuminate\Http\Request;

class MenuPlaceholderController extends _Controller
{
    use DbList;

    protected $model = MenuPlaceholder::class;
    protected $dependsOn = 'panel_id';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {
            $panel = session('panel',false);
            if($panel === false) return redirect()->route('sysadmin.home');
            $request->attributes->set('panel' , $panel);
            return $next($request);
        });
    }

    public function index(Request $request, MenuPlaceholderDataTable $dataTable)
    {
        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function create(Request $request)
    {
        return view("{$this->view}.items.{$this->table}.create");
    }

    public function store(MenuPlaceholderRequest $request)
    {
        $panel = $request->attributes->get('panel');

        $item = $panel->menu_placeholders()->create($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $item)
            ->withResult(self::success(
                __('sysadmin/controllers.store.success', ['model' => $this->modelName, 'title' => $item->title])
            ));
    }

    public function edit(Request $request, MenuPlaceholder $menuPlaceholder)
    {
        return view("{$this->view}.items.{$this->table}.edit")
                ->withItem($menuPlaceholder);
    }

    public function update(MenuPlaceholderRequest $request, MenuPlaceholder $menuPlaceholder)
    {
        $menuPlaceholder->update($request->validated());

        return self::smartRedirect($request, "{$this->role}.{$this->table}", $menuPlaceholder)
            ->withResult(self::success(
                __('sysadmin/controllers.update.success', ['model' => $this->modelName, 'title' => $menuPlaceholder->id])
            ));
    }

    public function destroy(MenuPlaceholder $menuPlaceholder)
    {
        $menuPlaceholder->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('sysadmin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $menuPlaceholder->title])
            ));
    }
}
