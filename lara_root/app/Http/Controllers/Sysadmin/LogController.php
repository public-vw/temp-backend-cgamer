<?php

namespace App\Http\Controllers\Sysadmin;

use App\DataTables\Sysadmin\LogDataTable;
use App\Models\Log;

class LogController extends _Controller
{
    protected $model = Log::class;

    public function index(LogDataTable $dataTable)
    {
        return $dataTable->render("{$this->view}.items.{$this->table}.index");
    }

    public function show(Log $log)
    {
        return view("{$this->view}.items.{$this->table}.show")
            ->withLog($log);
    }

    public function destroy(Log $log)
    {
        $log->delete();

        return redirect()->route("{$this->role}.{$this->table}.index")
            ->withResult(self::success(
                __('sysadmin/controllers.destroy.success', ['model' => $this->modelName, 'title' => $log->id])
            ));
    }
}
