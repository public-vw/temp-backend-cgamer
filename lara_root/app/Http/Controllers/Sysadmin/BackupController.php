<?php

namespace App\Http\Controllers\Sysadmin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Sysadmin\Traits\RoleJoint;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Symfony\Component\Console\Output\BufferedOutput;

class BackupController extends Controller
{
    use RoleJoint;

    public function index()
    {
        return view("{$this->view}.items.backups.index");
    }

    public function backup(Request $request)
    {
        $command = 'mydb:backup';
        $command .= ($request->has('upload')) ? ' -u' : '';
        $command .= ($request->has('nolocal')) ? ' -k' : '';
        $command .= ($request->has('silence')) ? ' -s' : '';

        $result = $this->runArtisanCommand($command);

        return view("{$this->view}.items.backups.index")->withResult($result);
    }

    public function restore(Request $request)
    {
        $command = 'mydb:restore ';
        $command .= $request->fileterm ?: '';
        $command .= ' -y';
        $command .= ($request->has('drop')) ? ' -d' : '';
        $command .= ($request->has('nobackup')) ? ' -x' : '';

        $result = $this->runArtisanCommand($command);

        return view("{$this->view}.items.backups.index")->withResult($result);
    }

    protected function runArtisanCommand(string $command): string
    {
        $ret = new BufferedOutput();
        Artisan::call($command, [], $ret);
        return $ret->fetch();
    }
}
