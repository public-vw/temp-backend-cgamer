<?php
namespace App\Http\Controllers\Sysadmin\Traits;

trait RoleJoint
{
    protected $role = 'sysadmin';
    protected $view;

    public function setRoleView()
    {
        $this->view = config("routes.{$this->role}.view",$this->role);
    }
}
