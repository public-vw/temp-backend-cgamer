<?php
namespace App\Http\Controllers\Sysadmin\Traits;

use Illuminate\Http\Request;

trait ConfigList
{
    private function configPrepare(Request $request){
        return config('enums.' . $request->config);
    }

    public function configList(Request $request)
    {
        $defaultConfigs = $this->configPrepare($request);

        $configs = [];
        foreach ($defaultConfigs as $key => $defaultConfig) {
            $configs[] = ['id' => $key, 'text' => $defaultConfig];
        }

        $configs = collect($configs);

        if($request->has('term')){
            $configs = $configs->filter(function ($item) use ($request) {
                return false !== stristr($item['text'], $request->term);
            });
        }

        return response()->json($configs);
    }
}
