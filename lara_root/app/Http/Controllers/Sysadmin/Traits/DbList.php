<?php

namespace App\Http\Controllers\Sysadmin\Traits;

use Illuminate\Http\Request;

trait DbList
{
    private function dbPrepare()
    {
        # add titleField variable to controller class, if title field is not <title> e.g. <name>
        $title = $this->titleField ?? 'title';

        $fields = ['id', $title . ' as text']; # text is keyword of select2
        $fields += $request->extraFields ?? [];

        /*
            [
                "id"       => 1,
                "text"     => 'Italy',
                "selected" => false,
                "search"   => '',
            ],
            "pagination" => [
                "more" => false
            ]
        */

        return [$title, $fields];
    }

    public function dbList(Request $request)
    {
        $panel = $request->attributes->get('panel') ?? null;
        $this->dependsVal = $panel->id ?? null;

        list($title, $fields) = $this->dbPrepare();

        $items = new $this->model;
        # add dependsVal variable to controller class, for constant dependencies, to make safety
        if (isset($this->dependsOn)) {
            $items = $items->where($this->dependsOn, $request->depends_val ?? $this->dependsVal);
        }
        if ($request->has('term')) {
            $items = $items->where($title, 'LIKE', "%$request->term%");
        }

        $items = $items->get($fields)->toArray();

        return response()->json($items);
    }
}
