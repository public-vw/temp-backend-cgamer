<?php

namespace App\Http\Controllers\Sysadmin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Sysadmin\Traits\RoleJoint;
use Laravel\Sanctum\PersonalAccessToken;

class ApiTokenController extends Controller
{
    use RoleJoint;

    public function index()
    {
        $tokens = PersonalAccessToken::paginate(10);
        return view("{$this->view}.items.api_tokens.index")->withItems($tokens);
    }

    public function show(PersonalAccessToken $apiToken)
    {
        return view("{$this->view}.items.api_tokens.show")
            ->withApiToken($apiToken);
    }

    public function forceLogout(PersonalAccessToken $token)
    {
        $token->delete();
        return redirect()->route("{$this->role}.api_tokens.index")
            ->withResult(self::success(
                __('sysadmin/controllers.force_logout.success', [
                    'device' => $token->name,
                    'token_id' => $token->id,
                    'user' => $token->tokenable->name,
                ])
            ));
    }
}
