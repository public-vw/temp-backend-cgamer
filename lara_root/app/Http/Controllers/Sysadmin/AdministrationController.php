<?php

namespace App\Http\Controllers\Sysadmin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Artisan;

class AdministrationController extends Controller
{
    public function upgrade(){
        Artisan::call('repo:pull');
        return redirect()->route('sysadmin.home');
    }
}
