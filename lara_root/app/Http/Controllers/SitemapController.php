<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\ArticleCategory;
use Illuminate\Support\Str;

class SitemapController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'role:admin'])->except(['index']);
    }

    public function index(string $passcode = null)
    {
        abort_if($passcode != config('sitemap.postfix'), '404');

        $app_name_slug = Str::slug(config('app.name'));

        $sitemap = app()->make("sitemap");

//        Cache::forget($app_name_slug.'.sitemap');
        // set cache key (string), duration in minutes (Carbon|Datetime|int), turn on/off (boolean)
        $sitemap->setCache($app_name_slug.'.sitemap', 600);

        if (!$sitemap->isCached()) {
            $this->makeSitemap($sitemap);
        }
        // show your sitemap (options: 'xml' (default), 'html', 'txt', 'ror-rss', 'ror-rdf')
        return $sitemap->render('xml');
    }

    public function links()
    {
        $app_name_slug = Str::slug(config('app.name'));

        //    Cache::forget($app_name_slug.'.sitemap');
        return view('panel.sitemap_links', ['sitemaps' => $this->makeSitemap()]);
    }

    public function images()
    {
        $app_name_slug = Str::slug(config('app.name'));

        //    Cache::forget($app_name_slug.'.sitemap');
        return view('panel.sitemap_images', ['sitemaps' => $this->makeSitemap()]);
    }


    protected function makeSitemap(&$sitemap)
    {
        $authors = [];

        // add item to the sitemap (url, date, priority, freq) // daily, weekly, monthly
        $sitemap->add(url('/'), '2022-01-01' . 'T' . date('H:i:s', time()) . '+03:30', '1.0', 'weekly', [], null, []);

//Categories -------------------------------------
        ArticleCategory::where('status', config_key('enums.article_categories_status', 'active'))->chunk(100, function ($items) use ($sitemap) {
            foreach ($items as $item) {

                $attachedImages = [];
                if (($images = $item->imagesCollection())->count()) {
                    foreach ($images as $image)
                        $attachedImages[] = ['url' => $image['url'], 'title' => $image->seo()['google']->title ?? $image->filename];
                }

                $attachedVideos = [];
                if (($videos = $item->videosCollection())->count()) {
                    foreach ($videos as $video)
                        $attachedVideos[] = ['url' => $video['url'], 'title' => $video->seo()['google']->title ?? $video->filename];
                }

                $sitemap->add($item->seoUrl(), $item->updated_at, '0.7', 'weekly', $attachedImages, null, $attachedVideos);
            }
        });

//Articles -------------------------------------
        Article::where('status', config_key('enums.articles_status', 'active'))->chunk(100, function ($items) use ($sitemap, &$authors) {
            foreach ($items as $item) {
                $authors[$item->author->id] = $item->author;

                $attachedImages = [];
                if (($images = $item->imagesCollection())->count()) {
                    foreach ($images as $image)
                        $attachedImages[] = ['url' => $image['url'], 'title' => $image->seo()['google']->title ?? $image->filename];
                }

                $attachedVideos = [];
                if (($videos = $item->videosCollection())->count()) {
                    foreach ($videos as $video)
                        $attachedVideos[] = ['url' => $video['url'], 'title' => $video->seo()['google']->title ?? $video->filename];
                }

                $sitemap->add($item->seoUrl(), $item->updated_at, '0.9', 'weekly', $attachedImages, null, $attachedVideos);
            }
        });

//Authors -------------------------------------
        foreach ($authors as $author) {
            $image = [];
            if ($image = $author->user->imagesCollection(false, 'avatar')->last()) {
                $image = [
                    'url' => $image->url,
                    'title' => $image->seo()['google']->title ?? $image->filename,
                ];
            }

            $sitemap->add(route('public.profile.index',['user'=>$author->user,'nickname'=>$author->user->nickname]), $author->updated_at, '0.5', 'monthly', $image?[$image]:[]);
        }
    }

}
