<?php

namespace App\Http\Controllers\Webhook;

use App\Http\Controllers\Controller;
use App\Models\BotConnection;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class TwitterController extends Controller
{
    public function index(Request $request, string $token)
    {
        $bot = BotConnection::where('type', 'twitter')->where('active', true)->where('webhook_token', $token)->first();

        if(!$bot){
            mylog('Wrong Bot Token','Warning',"Encoded Bot Token: ". base64_encode($token));
            return response('done',200);
        }

        mylog('Bot Hooked','Info',
            "Bot Name: " . $bot->title . '<br/>'.
            "Bot Username:  " . $bot->username . '<br/>'.
            "Bot Classname: " . Str::ucfirst(Str::camel($bot->username)) . '<br/>'.
            "Bot Token: " . $token);

        $robotClass = 'App\\Http\\Controllers\\System\\TwitterBots\\'.Str::ucfirst(Str::camel($bot->username));
        new $robotClass($request, $bot);

        return response('done',200);
    }
}
