<?php

namespace App\Http\Controllers\System\TelegramBots;

use App\Models\BotGasfeeAlarm;
use App\Models\BotUser;
use App\Support\GetRates\GasFee;
use App\Support\Telegram\BotMachine as BM;
use App\Support\Telegram\BotState;

class GasFeeRobot extends BM
{
    const DEBUG_MODE = false;

    static $base_state = 'welcome';
    static $temp = null;
    static $bot_id = 2;

//==================================================================================================
    protected static function begin()
    {
        define('LANGUAGE', 'en');
    }

    protected static function reboot_state()
    {
        if (BM::$input_text === '/start') {
            return null;
        }
        if (BM::$input_btn === 'gobase') {
            return null;
        }
        return false;
    }

//==================================================================================================
    protected static function config()
    {

        static::$states['welcome'] = new class extends BotState {
            function start()
            {
                $botUser = BM::getBotUser();

                $hasAlarm = $botUser ? $botUser->hasAlarm() : null;

                $this->base_message = "Choose an <b>action</b>";

                $this->buttons[] = [
                    '🛢 Get Gas Fee'                   => ['callback_data' => 'gasfeenow'],
                    '📣 Set Alarm'                     => ['callback_data' => 'set_alarm', 'condition' => !$hasAlarm],
                    '🗑 Remove Alarm on $' . $hasAlarm => ['callback_data' => 'remove_alarm', 'condition' => $hasAlarm],
                ];
            }

            function action()
            {
                switch (BM::$input_text) {
                    case '🛢 Get Gas Fee':
                        BM::setState('gasfeenow');
                        break;
                    case '📣 Set Alarm':
                        BM::setState('set_alarm');
                        break;
                    default:
                        if (str_contains(BM::$input_text, 'Remove Alarm')) {
                            $botUser = BM::getBotUser();

                            if ($botUser && BotGasfeeAlarm::removeAlarm($botUser->id)) {
                                BM::setState('welcome', 'Your Alarm Removed!');
                            }
                        }
                }
            }
        };

//==================================================================================================

        static::$states['gasfeenow'] = new class extends BotState {
            var $btn_type = 'footer';

            function start()
            {
                $gasfee = (new GasFee())->get();
                $botUser = BM::getBotUser();

                $hasAlarm = $botUser ? $botUser->hasAlarm() : null;

                $this->base_message = "Lower <b>Gas Fee</b> is \${$gasfee} right now.";

                $this->buttons[] = [
                    '🛢 Get Gas Fee'                   => ['callback_data' => 'gasfeenow'],
                    '📣 Set Alarm'                     => ['callback_data' => 'set_alarm', 'condition' => !$hasAlarm],
                    '🗑 Remove Alarm on $' . $hasAlarm => ['callback_data' => 'remove_alarm', 'condition' => $hasAlarm],
                ];

                BM::setState('welcome');
            }
        };

//==================================================================================================

        static::$states['set_alarm'] = new class extends BotState {
            var $btn_type = 'inline';

            function start()
            {
                BM::setCache('dont_clear_btn', false);

                $gasfee = (new GasFee())->get();
                $this->base_message = "Lower <b>Gas Fee</b> is \${$gasfee} right now.";
                $this->base_message .= "\nIf the <b>Gas Fee</b> is less than how much should I tell you ?";

                $this->buttons[] = [
                    '$ 10' => ['callback_data' => '10', 'condition' => ($gasfee > 10)],
                    '$ 25' => ['callback_data' => '25', 'condition' => ($gasfee > 25)],
                ];
                $this->buttons[] = [
                    '$ 50' => ['callback_data' => '50', 'condition' => ($gasfee > 50)],
                    '$ 75' => ['callback_data' => '75', 'condition' => ($gasfee > 75)],
                ];
                $this->buttons[] = [
                    '$ 100' => ['callback_data' => '100', 'condition' => ($gasfee > 100)],
                    '$ 125' => ['callback_data' => '125', 'condition' => ($gasfee > 125)],
                ];
                $this->buttons[] = [
                    '$ 150' => ['callback_data' => '150', 'condition' => ($gasfee > 150)],
                    '$ 175' => ['callback_data' => '175', 'condition' => ($gasfee > 175)],
                ];
                $this->buttons[] = [
                    '$ 200' => ['callback_data' => '200', 'condition' => ($gasfee > 200)],
                    '$ 225' => ['callback_data' => '225', 'condition' => ($gasfee > 225)],
                ];
                $this->buttons[] = [
                    '$ 250' => ['callback_data' => '250', 'condition' => ($gasfee > 250)],
                    '$ 275' => ['callback_data' => '275', 'condition' => ($gasfee > 275)],
                ];
                $this->buttons[] = [
                    '$ 300' => ['callback_data' => '300', 'condition' => ($gasfee > 300)],
                    '$ 325' => ['callback_data' => '325', 'condition' => ($gasfee > 325)],
                ];
                $this->buttons[]['Back'] = ['callback_data' => 'back'];
            }

            function action()
            {
                switch (BM::$input_btn) {
                    case 'back':
                        return BM::setState('welcome');
                        break;
                    default:
                        $botUser = BM::getBotUser();

                        if (BotGasfeeAlarm::setAlarm($botUser->id, intval(BM::$input_btn))) {
                            return BM::setState('welcome', 'Alarm Configured To $' . BM::$input_btn);
                        }

                }
            }
        };

//==================================================================================================
//==================================================================================================
//==================================================================================================
// [Sample State] ==================================================================================
        /*
                static::$states['new_state'] = new class extends BotState{

                    var $btn_type = 'inline';

                    function start(){
                        $this->base_message = "Dynamic Message";
                        $this->buttons[]['button_title'] = ['callback_data'=>'button_data','condition'=>true];

                        //do somthing on start (before first message, at object creation)
                    }

                    function action(){
                        return BM::backState();

                        switch(BM::$input_text){
                            case '/new':
                                return BM::setState('other_state');
                            break;
                            case '/back':
                                return BM::backState();
                            break;
                            case '/addMessage':
                                $this->addMessage("Hi!");
                            break;
                            default:
                                $this->addMessage("Unknown Command\n".BM::$input_text);
                        }
                    }

                };
        */
//==================================================================================================
    }


}
