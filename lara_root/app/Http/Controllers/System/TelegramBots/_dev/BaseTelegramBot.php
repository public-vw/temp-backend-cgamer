<?php

namespace App\Http\Controllers\TelegramBots\Bases;
use Illuminate\Http\Request;
use \GuzzleHttp\Client;
use Cache;

abstract class BaseTelegramBot
{
    const CACHE_LIFE_TIME = 30;
    const DEBUG_MODE = false;
    const DEBUG_USR_ID = ['91173754','115724262'];
    const HELLO_TIME = 7200;
    
    protected $bot = null;
    protected $request = null;
    protected $reply = null;
    protected $reply_text = null;
    protected $reply_cmd = null;
    protected $reply_param = null;
    protected $reply_type = null;
    protected $last_params = null;
    protected $msg_type = null;
    protected $user_id = null;
    protected $chat_id = null;
    protected $response_id = null;
    protected $reply_id = null;
    protected $inline_reply = null;
    
    protected $data = null;
    
    function __construct(Request $request,$bot) {
        try{
            $this->bot = $bot;
            $this->request = $request;
//mylog('info','0',var_export([$request->all()],true));
            $this->reply_type = $this->request->reply_type;
            $this->reply = $this->request->tlg_reply[$this->reply_type];
            $this->reply_text = isset($this->reply['text'])? $this->reply['text']:null;
            $this->user_id = isset($this->reply['from']['id'])?$this->reply['from']['id']:$this->reply['message']['from']['id'];
            $this->chat_id = isset($this->reply['chat']['id'])?$this->reply['chat']['id']:$this->reply['message']['chat']['id'];
            $this->response_id = isset($this->reply['id'])?$this->reply['id']:null;
            $this->reply_id = isset($this->reply['message']['message_id'])?$this->reply['message']['message_id']:$this->response_id;
//mylog('info','0',var_export([$this->reply,$this->chat_id,$this->response_id],true));
        
            $this->data = Cache::get('bot_data_'.$this->chat_id);

            if(isset($this->reply['text'])){
                $text = $this->reply['text'];
                $this->inline_reply = substr($text, 0, (strpos($text, '@'))?strpos($text, '@'):strlen($text));
                $this->reply_param = (strpos($text, '/')===0 && strpos($text, ' ')>0)? trim(substr($text, strpos($text, ' ')) ):null;
                $this->reply_cmd = (strpos($text, '/')===0)? trim(substr($text, 0, strpos($text, ' ')>0?strpos($text, ' '):strlen($text)) ):null;
            }

            $this->msg_type = isset($this->reply['entities'][0]['type'])?$this->reply['entities'][0]['type']
                                :(isset($this->reply['photo'])?'photo'
                                    :(isset($this->reply['location'])?'location'
                                        :(isset($this->reply['contact'])?'contact'
                                            :(isset($this->reply['text'])?'text':'unknown')
                            )));

            $this->handler();
            $this->data['last_seen'] = time();

//mylog('info','0',var_export([$this::DEBUG_MODE,$this::DEBUG_USR_ID],true));
            if($this::DEBUG_MODE && in_array($this->user_id , $this::DEBUG_USR_ID)){
                mylog('warning','0',var_export(['OBJECT INSIDE',$this->user_id,$this->reply_type,$this->last_params,$this->data],true));
                $this->sendMessage("---[DEBUGER]---\n\n"
                        ."<b>MESSAGE TYPE</b>\n"
                        ."<pre>".var_export($this->msg_type,true)
                        ."</pre>\n\n"
                        ."<b>DATA</b>\n"
                        ."<pre>".var_export($this->data,true)
                        ."</pre>"
                        ."\n\n-------------------",null,true);
            }
        
            Cache::put('bot_data_'.$this->chat_id,$this->data,$this::CACHE_LIFE_TIME);
        }
        catch(Exception $e){
            getFormedError($e);
        }
    }
    
    abstract function handler();
    
    protected function commandCheck($inp){
//        mylog('warning',1,var_export([$this->reply_cmd, $inp],true));
        return ($this->reply_cmd == $inp);
    }
    
//    protected function Commands($commands){
//        $commands = ['/hello', '/about','/telltime'];
//    }
//    
//    protected function Markup(){
//        $markup = ['reply_markup' => [
//                        'keyboard' => [$commands],
//                        'one_time_keyboard' => true,
//                        'resize_keyboard' => true
//                        ]
//                    ];
//        
//    }
    
//------------------------------------------------------------------------------
    protected function deleteLastMessage(){
        $tmp = [
                    'chat_id' => $this->chat_id, 
                    "message_id" => $this->reply_id,
        ];
        
      $this->apiRequest("deleteMessage", $tmp );
    }

//------------------------------------------------------------------------------
    protected function sendPhoto($caption,$src=null,$deleteLast = false){
        if($deleteLast) $this->deleteLastMessage();
        $tmp = [
                    'chat_id' => $this->chat_id, 
                    'caption'=> $caption,
                    'photo'=>$src,
        ];
        
        $tmp['disable_notification'] = false;
        $tmp['parse_mode'] = 'html';
        $this->last_params = $tmp;
        
//        $this->apiRequestJson("sendPhoto", $tmp);
      $this->apiRequest("sendPhoto", $tmp );
    }

//------------------------------------------------------------------------------
    protected function sendChatAction($action){
        //Actions
        //typing for text messages, 
        //upload_photo for photos, 
        //record_video or upload_video for videos, 
        //record_audio or upload_audio for audio files, 
        //upload_document for general files, 
        //find_location for location data, 
        //record_video_note or upload_video_note for video notes

        $tmp = [
                    'chat_id' => $this->chat_id, 
                    'action'=> $action,
        ];
        
        $this->last_params = $tmp;
        
//        $this->apiRequestJson("sendPhoto", $tmp);
      $this->apiRequest("sendChatAction", $tmp );
    }

//------------------------------------------------------------------------------
    protected function sendMessage($content,$commands=null,$html=false,$cols=2,$deleteLast = false){//commands : ['com1'=>null,'com2'=>'request_contact','com3'=>'request_location']

        if($deleteLast) $this->deleteLastMessage();
        $tmp = [
                    'chat_id' => $this->chat_id, 
                    "text" => $content,
        ];
        
        if($html) $tmp['parse_mode'] = 'html';
        $tmp['disable_web_page_preview'] = true;
        if($commands) {
            $tmp['reply_markup']['keyboard'] = [];

            $c = 1;
            $kb = [];
            $kbs = [];
            foreach($commands as $command=>$type)
            {
                if(isset($params['condition'])){
                    if($params['condition'] === false) continue;
                    unset($params['condition']);
                }
                
                $t = ['text' => $command];
                if($type) $t[$type] = true;
                $kb[] = ($type)?$t:$t['text'];
                
                if($c++%$cols == 0){$kbs[] = $kb;$kb=[];}
            }
            
            if(!empty($kb)){
                $kbs[] = $kb;
            }
            
            if(!empty($kbs)){
                $tmp['reply_markup']['keyboard'] = $kbs;
                $tmp['reply_markup']['one_time_keyboard'] = true;
                $tmp['reply_markup']['resize_keyboard'] = true;
            }
        }
        elseif(is_array($commands)){
            $tmp['reply_markup']['remove_keyboard'] = true;
//            $tmp['reply_markup'] = 'replyKeyboardHide';
        }
        
        $this->last_params = $tmp;
        
//        $this->apiRequestJson("sendMessage", $tmp);
      $this->apiRequest("sendMessage", $tmp );
    }

//------------------------------------------------------------------------------
    protected function sendMessageInlineMarkup($content,$commands=null,$html=false,$cols=2,$deleteLast = false){//commands : ['com1'=>null,'com2'=>'request_contact','com3'=>'request_location']
        if($deleteLast) $this->deleteLastMessage();
        $tmp = [
                    'chat_id' => $this->chat_id, 
                    "text" => $content,
        ];
        
        if($html) $tmp['parse_mode'] = 'html';
        $tmp['disable_web_page_preview'] = true;
        if($commands) {
            $tmp['reply_markup']['inline_keyboard'] = [];

            $c = 1;
            $kb = [];
            $kbs = [];
            foreach($commands as $command=>$params)
            {
                if(isset($params['condition'])){
                    if($params['condition'] === false) continue;
                    unset($params['condition']);
                }
                
                $t = ['text' => $command];
                $t = array_merge($t,$params);
                $kb[] = $t;
                
                if($c++%$cols == 0){$kbs[] = $kb;$kb=[];}
            }
            
            if(!empty($kb)){
                $kbs[] = $kb;
            }
            
            if(!empty($kbs)){
                $tmp['reply_markup']['inline_keyboard'] = $kbs;
//                $tmp['reply_markup']['one_time_keyboard'] = true;
//                $tmp['reply_markup']['resize_keyboard'] = true;
            }
        }
        elseif(is_array($commands)){
            $tmp['reply_markup']['remove_keyboard'] = true;
//            $tmp['reply_markup'] = 'replyKeyboardHide';
        }
        
        $this->last_params = $tmp;
        
//        $this->apiRequestJson("sendMessage", $tmp);
      $this->apiRequest("sendMessage", $tmp );
    }
    
//------------------------------------------------------------------------------
    protected function answerCallbackQuery($content,$commands=null,$html=false,$cols=2,$deleteLast = false){//commands : ['com1'=>null,'com2'=>'request_contact','com3'=>'request_location']
        if($deleteLast) $this->deleteLastMessage();
        $tmp = [
                    'callback_query_id' => $this->response_id, 
                    "text" => $content,
        ];
        
//        if($html) $tmp['parse_mode'] = 'html';
//        $tmp['disable_web_page_preview'] = true;
        if($commands) {
            $tmp['reply_markup']['inline_keyboard'] = [];

            $c = 1;
            $kb = [];
            $kbs = [];
            foreach($commands as $command=>$params)
            {
                if(isset($params['condition'])){
                    if($params['condition'] === false) continue;
                    unset($params['condition']);
                }
                
                $t = ['text' => $command];
                $t = array_merge($t,$params);
                $kb[] = $t;
                
                if($c++%$cols == 0){$kbs[] = $kb;$kb=[];}
            }
            
            if(!empty($kb)){
                $kbs[] = $kb;
            }
            
            if(!empty($kbs)){
                $tmp['reply_markup']['inline_keyboard'] = $kbs;
//                $tmp['reply_markup']['one_time_keyboard'] = true;
//                $tmp['reply_markup']['resize_keyboard'] = true;
            }
        }
//        elseif(is_array($commands)){
////            $tmp['reply_markup']['remove_keyboard'] = true;
////            $tmp['reply_markup'] = 'replyKeyboardHide';
//        }
        
        $this->last_params = $tmp;
        
//        $this->apiRequestJson("sendMessage", $tmp);
      $this->apiRequest("answerCallbackQuery", $tmp );
    }
    
//------------------------------------------------------------------------------
    protected function editMessageReplyMarkup($commands=null,$cols=2,$deleteLast = false){//commands : ['com1'=>null,'com2'=>'request_contact','com3'=>'request_location']
        if($deleteLast) $this->deleteLastMessage();
        $tmp = [
                    'message_id' => $this->response_id, 
        ];
        
        if($commands) {
            $tmp['reply_markup']['inline_keyboard'] = [];

            $c = 1;
            $kb = [];
            $kbs = [];
            foreach($commands as $command=>$params)
            {
                if(isset($params['condition'])){
                    if($params['condition'] === false) continue;
                    unset($params['condition']);
                }
                
                $t = ['text' => $command];
                $t = array_merge($t,$params);
                $kb[] = $t;
                
                if($c++%$cols == 0){$kbs[] = $kb;$kb=[];}
            }
            
            if(!empty($kb)){
                $kbs[] = $kb;
            }
            
            if(!empty($kbs)){
                $tmp['reply_markup']['inline_keyboard'] = $kbs;
//                $tmp['reply_markup']['one_time_keyboard'] = true;
//                $tmp['reply_markup']['resize_keyboard'] = true;
            }
        }
        elseif(is_array($commands)){
//            $tmp['reply_markup']['remove_keyboard'] = true;
//            $tmp['reply_markup'] = 'replyKeyboardHide';
        }
        
        $this->last_params = $tmp;
        
//        $this->apiRequestJson("sendMessage", $tmp);
      $this->apiRequest("editMessageReplyMarkup", $tmp );
    }
    
//------------------------------------------------------------------------------
    protected function calendar($message,$YMDHIS,$page=null,$rowsPerPage=3,$colsPerRow=1,$count = 5,$base=null,$deleteLast = false){
        $main_base = \App\Libraries\jDateTime::date('Y|m|d|w|H',strtotime("+2 hours", time()),false);
//mylog('warning','1',var_export(
//        [
//            $YMDHIS
//            , $base 
//            , $main_base
//            , compareDates($base , $main_base, 'Y|m|d|w|H|i')
//            , isset($this->data['dates'])?$this->data['dates']:'no_dates'
//            , isset($this->data['cal_page'])?$this->data['cal_page']:'no_cal_page'
//],true));
//mylog('warning','0',var_export( [$base, $main_base,compareDates($base , $main_base, 'Y|m|d|w|H|i')] ,true));
        if(!isset($base) || compareDates($base , $main_base, 'Y|m|d|w|H|i')==1 ) $base = $main_base;
        $base = explode('|',$base);
        $base = array_map('intval',$base);
        if(!isset($base[5]))$base[5] = 0;
        $max = [0,31,31,31,31,31,31,30,30,30,30,30,29];

        if(isset($this->data['force_del']) && $this->data['force_del']){
            $deleteLast = true;
            unset($this->data['force_del']);
        }
//mylog('warning','1',var_export( $base ,true));
        
        if($base[3] == 9){
            $greg = \App\Libraries\jDateTime::jalaliToGreg_arr($base[0].'/'.$base[1].'/'.$base[2]);
            $greg = mktime(1,0,0,$greg[1],$greg[2],$greg[0]);
            $base[3] = (date('w', $greg)+8)%7;
        }
//mylog('warning','2',var_export( $base ,true));

        if(!isset($page)) $page = (isset($this->data['cal_page']))?$this->data['cal_page']:1;
        
        $arr = [];
        switch($YMDHIS){
            case 'Y':
                for($i=0; $i < $count; $i++)$arr['Y'][] = $base[0]+$i;
            break;
            case 'M':
                for($i=0; $i < $count; $i++){
                    $arr['Y'][] = $base[0];
                    $arr['M'][] = $base[1];
                    $base[1]++;
                    if($base[1]>12){$base[1]=1;$base[0]++;}
                }
            break;
            case 'D':
                for($i=$base[2]; $i <= $max[$base[1]]; $i++){
                    $arr['Y'][] = $base[0];
                    $arr['M'][] = $base[1];
                    $arr['D'][] = $i;
                    $arr['W'][] = (7+$base[3]-$base[2]+$i)%7;
                }
//                for($i=0; $i < $count; $i++){
//                    $arr['Y'][] = $base[0];
//                    $arr['M'][] = $base[1];
//                    $arr['D'][] = $base[2];
//                    
//                    $base[2]++;
//                    if($base[2]>$max[base[1]]){$base[2]=1;$base[1]++;}
//                    if($base[1]>12){$base[1]=1;$base[0]++;}
//                }
            break;
            case 'H':
                for($i=$base[4]; $i <= 23; $i++){
                    $arr['Y'][] = $base[0];
                    $arr['M'][] = $base[1];
                    $arr['D'][] = $base[2];
//                    $arr['W'][] = (7+$base[3]-$base[2])%7;
                    $arr['W'][] = $base[3];
                    $arr['H'][] = $i;
                }
//                for($i=0; $i < $count; $i++){
//                    if($base[3]+$i>23)continue;
//                    $arr['H'][] = $base[3]+$i;
//                }
            break;
            case 'I':
                $quanta = 30;
                for($i=ceil($base[5]/$quanta)*$quanta; $i < 60; $i+=$quanta){
                    $arr['Y'][] = $base[0];
                    $arr['M'][] = $base[1];
                    $arr['D'][] = $base[2];
//                    $arr['W'][] = (7+$base[3]-$base[2])%7;
                    $arr['W'][] = $base[3];
                    $arr['H'][] = $base[4];
                    $arr['I'][] = substr('0'.$i,-2);
                }
            break;
        }
        
        if($page == 'last') {
            $page = ceil(count($arr[$YMDHIS])/$rowsPerPage/$colsPerRow);
            $this->data['cal_page'] = $page;
        }
        $commands = [];
        
        for($i=($page-1)*$rowsPerPage*$colsPerRow; $i < min(count($arr[$YMDHIS]),$page*$rowsPerPage*$colsPerRow); $i+=$colsPerRow){
            $tmp = [];
            for($j=$colsPerRow-1; $j >= 0; $j--){
                if(isset($arr[$YMDHIS][$i+$j])){
                    switch($YMDHIS){
                        case 'Y':
                            $tmp[] = [
                                'text'=>$arr['Y'][$i+$j],
                                'callback_data'=>'Y'.$arr['Y'][$i+$j]
                            ];
                        break;
                        case 'M':
                            $tmp[] = [
                                'text'=>__('dates.months.'.$arr['M'][$i+$j]).' '.$arr['Y'][$i+$j],
                                'callback_data'=>'Y'.$arr['Y'][$i+$j].'|M'.$arr['M'][$i+$j]
                            ];
                        break;
                        case 'D':
                            $tmp[] = [
                                'text'=>__('dates.days.'.$arr['W'][$i+$j]).', '.$arr['D'][$i+$j].' '.__('dates.months.'.$arr['M'][$i+$j]),
                                'callback_data'=>'Y'.$arr['Y'][$i+$j].'|M'.$arr['M'][$i+$j].'|D'.$arr['D'][$i+$j].'|W'.$arr['W'][$i+$j]
                            ];
                        break;
                        case 'H':
                            $tmp[] = [
            //                    'text'=>__('dates.days.'.$arr['W'][$i+$j]).', '.$arr['D'][$i+$j].' '.__('dates.months.'.$arr['M'][$i+$j]),
                                'text'=>__('dates.hours.title').' '.(($arr['H'][$i+$j]-1)%12+1).' '.__('dates.hours.'.$arr['H'][$i+$j]),
                                'callback_data'=>'Y'.$arr['Y'][$i+$j].'|M'.$arr['M'][$i+$j].'|D'.$arr['D'][$i+$j].'|W'.$arr['W'][$i+$j].'|H'.$arr['H'][$i+$j]
                            ];
                        break;
                        case 'I':
                            $tmp[] = [
            //                    'text'=>__('dates.days.'.$arr['W'][$i+$j]).', '.$arr['D'][$i+$j].' '.__('dates.months.'.$arr['M'][$i+$j]),
                                'text'=>__('dates.hours.title').' '.$arr['H'][$i+$j].':'.$arr['I'][$i+$j].' '.__('dates.hours.'.$arr['H'][$i+$j]),
                                'callback_data'=>'Y'.$arr['Y'][$i+$j].'|M'.$arr['M'][$i+$j].'|D'.$arr['D'][$i+$j].'|W'.$arr['W'][$i+$j].'|H'.$arr['H'][$i+$j].'|I'.$arr['I'][$i+$j]
                            ];
                        break;
                    }
                }
            }

            if(!empty($tmp))$commands[] =  $tmp;
        }

        $tmp=[];
        if(count($arr[$YMDHIS])>$page*$rowsPerPage*$colsPerRow){
            $tmp[] = [
                'text'=>'«',
                'callback_data'=>'last_page'
            ];
            $tmp[] = [
                'text'=>'<',
                'callback_data'=>'next_page'
            ];
        }
        if($page > 1){
            $tmp[] = [
                'text'=>'>',
                'callback_data'=>'prev_page'
            ];
            $tmp[] = [
                'text'=>'»',
                'callback_data'=>'first_page'
            ];
        }
        $commands[] = $tmp;
        //-----------------------------
        if($deleteLast) $this->deleteLastMessage();
        $tmp = [
            'chat_id' => $this->chat_id, 
            "text" => $message,
        ];
        
        $tmp['parse_mode'] = 'html';
        $tmp['disable_web_page_preview'] = true;
        $tmp['reply_markup']['inline_keyboard'] = $commands;
//        $tmp['reply_markup']['one_time_keyboard'] = true;
        $tmp['reply_markup']['resize_keyboard'] = true;
        
        $this->last_params = $tmp;
        
        $this->apiRequest("sendMessage", $tmp );
        
    }
    
//==============================================================================
    protected function apiRequest($method, $parameters) {
        if (!is_string($method)) {
            Log::error("Method name must be a string\n");
            return false;
        }

        if (!$parameters) {
            $parameters = array();
        } else if (!is_array($parameters)) {
            Log::error("Parameters must be an array\n");
            return false;
        }
        foreach ($parameters as $key => &$val) {
// encoding to JSON array parameters, for example reply_markup
            if (!is_numeric($val) && !is_string($val)) {
                $val = json_encode($val);
            }
        }

        if($this::DEBUG_MODE && in_array($this->user_id , $this::DEBUG_USR_ID))
            mylog('warning','1',var_export(['SENT DATA',$method,$parameters],true));

        
        return $this->sendGetRequest($method,$parameters);
//        $url = API_URL_TELEGRAM . $this->bot->robotoken . '/'.$method . '?' . http_build_query($parameters);
//        $handle = curl_init($url);
//        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 5);
//        curl_setopt($handle, CURLOPT_TIMEOUT, 60);
//
//        return $this->exec_curl_request($handle);
    }

//------------------------------------------------------------------------------
    protected function apiRequestJson($method, $parameters) {
        if (!is_string($method)) {
            Log::error("Method name must be a string\n");
            return false;
        }

        if (!$parameters) {

            $parameters = array();
        } else if (!is_array($parameters)) {
            Log::error("Parameters must be an array\n");
            return false;
        }

        $parameters["method"] = $method;

        return $this->sendPostRequest($method,$parameters);
//        $handle = curl_init(API_URL_TELEGRAM . $this->bot->robotoken . '/');
//        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 5);
//        curl_setopt($handle, CURLOPT_TIMEOUT, 60);
//        curl_setopt($handle, CURLOPT_POSTFIELDS, json_encode($parameters));
//        curl_setopt($handle, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
//
//        return $this->exec_curl_request($handle);
    }
    
//------------------------------------------------------------------------------
    protected function sendPostRequest($url, $params){
        try{
            $client = new Client(['exceptions'=>false,'verify'=>false]);
            $res = $client->post(API_URL_TELEGRAM . $this->bot->robotoken . '/'.$url, ['form_params'=>$params]);
//            if($res->getStatusCode() == '200') $webhook->update_status('trigged');
//            if($res->getStatusCode() !== '200') {
//                $webhook->update_status('resp:'.$res->getStatusCode(),'received');
//            }
//    //        echo $res->getHeader('content-type');
//    //        echo $res->getBody();        
        }
        catch(Exception $e){
            getFormedError($e);
        }
        
//        return $res;
        return $res->getStatusCode();
    }
    
//------------------------------------------------------------------------------
    protected function sendGetRequest($url, $params){
//mylog('error','1',API_URL_TELEGRAM . $this->bot->robo_token . '/'.$url);        
        try{
            $client = new Client(['exceptions'=>false,'verify'=>false]);
            $res = $client->get(API_URL_TELEGRAM . $this->bot->robotoken . '/'.$url, ['form_params'=>$params]);
//            if($res->getStatusCode() == '200') $webhook->update_status('trigged');
//            if($res->getStatusCode() !== '200') {
//                $webhook->update_status('resp:'.$res->getStatusCode(),'received');
//            }
//    //        echo $res->getHeader('content-type');
//    //        echo $res->getBody();        
        }
        catch(Exception $e){
            getFormedError($e);
        }
        
//        return $res;
        return $res->getStatusCode();
    }

    
}
