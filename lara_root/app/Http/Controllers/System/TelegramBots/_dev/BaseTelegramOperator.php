<?php

namespace App\Http\Controllers\System\TelegramBots\Bases;
use App\Jobs\TelegramSend;
use App\Libraries\TelegramPost;
use App\Models\Eventt;
use App\Models\EventtCategoryBG;
use App\Models\TelegramRobotMessage;
use Log;
use IntImage;

class BaseTelegramOperator
{

    const CACHE_LIFE_TIME = 30;
    const DEBUG_MODE = false;
    const DEBUG_USRS_ID = ['91173754','115724262'];

    protected $bot = null;
    protected $title = null;

    function __construct($bot_id,$title=null) {
        $this->bot = \App\Models\BotConnection::findOrFail($bot_id);
        $this->title = $title;
    }
//------------------------------------------------------------------------------
    public function deleteLastMessage($chat_id,$reply_id){
        $tmp = [
            'chat_id' => $chat_id,
            "message_id" => $reply_id,
        ];

        $this->TelegramSendJob( "deleteMessage", $tmp );
    }

//------------------------------------------------------------------------------
    public function sendMessage($chat_id,$content,$commands=null,$html_mark='HTML',$cols=2,$deleteLast = false){//commands : ['com1'=>null,'com2'=>'request_contact','com3'=>'request_location']
//        if($deleteLast) $this->deleteLastMessage($chat_id,$reply_id);
        $tmp = [
            'chat_id' => $chat_id,
            "text" => $content,
        ];

        if($html_mark) $tmp['parse_mode'] = $html_mark;
        $tmp['disable_web_page_preview'] = true;
        if($commands) {
            $tmp['reply_markup']['keyboard'] = [];

            $c = 1;
            $kb = [];
            $kbs = [];
            foreach($commands as $command=>$params)
            {
                if(isset($params['condition'])){
                    if($params['condition'] === false) continue;
                    unset($params['condition']);
                }

                $t = ['text' => $command];
                if(isset($params['type'])) $t[$params['type']] = true;
                $kb[] = (isset($params['type']))?$t:$t['text'];

                if($c++%$cols == 0){$kbs[] = $kb;$kb=[];}
            }

            if(!empty($kb)){
                $kbs[] = $kb;
            }

            if(!empty($kbs)){
                $tmp['reply_markup']['keyboard'] = $kbs;
                $tmp['reply_markup']['one_time_keyboard'] = true;
                $tmp['reply_markup']['resize_keyboard'] = true;
            }
        }
        elseif(is_array($commands)){
            $tmp['reply_markup']['remove_keyboard'] = true;
//            $tmp['reply_markup'] = 'replyKeyboardHide';
        }



        $this->TelegramSendJob( "sendMessage", $tmp );
    }

//------------------------------------------------------------------------------
    public function sendMessageInlineMarkup($chat_id,$content,$commands=null,$html_mark='HTML',$cols=2,$deleteLast = false){//commands : ['com1'=>null,'com2'=>'request_contact','com3'=>'request_location']
//        if($deleteLast) $this->deleteLastMessage($chat_id,$reply_id);
        $tmp = [
            'chat_id' => $chat_id,
            "text" => $content,
        ];

        if($html_mark) $tmp['parse_mode'] = $html_mark;//HTML || Markdown || null
        $tmp['disable_web_page_preview'] = true;
        if($commands) {
            $tmp['reply_markup']['inline_keyboard'] = [];

            $c = 1;
            $kb = [];
            $kbs = [];
            foreach($commands as $command=>$params)
            {
                if(isset($params['condition'])){
                    if($params['condition'] === false) continue;
                    unset($params['condition']);
                }

                $t = ['text' => $command];
                $t = array_merge($t,$params);
                $kb[] = $t;

                if($c++%$cols == 0){$kbs[] = $kb;$kb=[];}
            }

            if(!empty($kb)){
                $kbs[] = $kb;
            }

            if(!empty($kbs)){
                $tmp['reply_markup']['inline_keyboard'] = $kbs;
//                $tmp['reply_markup']['one_time_keyboard'] = true;
//                $tmp['reply_markup']['resize_keyboard'] = true;
            }
        }
        elseif(is_array($commands)){
            $tmp['reply_markup']['remove_keyboard'] = true;
//            $tmp['reply_markup'] = 'replyKeyboardHide';
        }



        $this->TelegramSendJob( "sendMessage", $tmp );
    }

//------------------------------------------------------------------------------
    // $commands_type = footer | inline | text(not implemented yet)
    // footer btns : 'btn1'=>null | 'btn2'=>'request_contact' | 'btn3'=>'request_location'
    // inline btns : 'btn1'=>['callback_data'=>'callback_value'] | 'btn2'=>['url'=>'https:// | tg://'] | 'btn3'=>['.....','condition'=>boolean_phrase ]
    // [ btn1 , btn2 ] , [ btn3 ] , [ btn4 , btn5 , btn6 ] , ...
    public function clearFooterButtons($chat_id){
        $this->mainSendMessage($chat_id,"_",[],'footer');
    }

    public function mainSendMessage($chat_id,$content,$commands=null,$commands_type=null,$html_mark='HTML',$is_update = false){

        $tmp = [
            'chat_id' => $chat_id,
            'text' => ($content?$content:"_"),
        ];
//        if($is_update === true) {
//            $tmp['message_id'] = $this->last_message_id;
//        }
//        else
        if($is_update !== false) {
            $tmp['message_id'] = $is_update;
        }

//        if($content) $tmp["text"] = $content;

        if($html_mark) $tmp['parse_mode'] = $html_mark;//HTML || Markdown || null
        $tmp['disable_web_page_preview'] = true;

        switch($commands_type){
            case 'footer_sticky':
            case 'footer':
                if(!empty($commands)) {
                    $tmp['reply_markup']['keyboard'] = [];

                    $c = 1;
                    $kbs = [];
                    foreach($commands as $row){
                        $kb = [];
                        foreach($row as $command=>$params)
                        {
                            if(isset($params['condition'])){
                                if($params['condition'] === false) continue;
                                unset($params['condition']);
                            }

                            $t = ['text' => $command];
                            if(isset($params['type'])) $t[$params['type']] = true;
                            $kb[] = (isset($params['type']))?$t:$t['text'];

                        }
                        if(!empty($kb))$kbs[] = $kb;
                    }

                    if(!empty($kbs)){
                        $tmp['reply_markup']['keyboard'] = $kbs;
                        $tmp['reply_markup']['one_time_keyboard'] = ($commands_type !== 'footer_sticky');
                        $tmp['reply_markup']['resize_keyboard'] = true;
                    }
                }
                elseif(is_array($commands)){
                    $tmp['reply_markup']['remove_keyboard'] = true;
//                    $tmp['reply_markup'] = 'replyKeyboardHide';
                }
            break;

            case 'inline':
                if(!empty($commands)) {
                    $tmp['reply_markup']['inline_keyboard'] = [];

                    $c = 1;
                    $kbs = [];
                    foreach($commands as $row){
                        $kb = [];
                        foreach($row as $command=>$params)
                        {
                            if(isset($params['condition'])){
                                if($params['condition'] === false) continue;
                                unset($params['condition']);
                            }

                            $t = ['text' => $command];
                            $t = array_merge($t,$params);
                            $kb[] = $t;

                        }
                        if(!empty($kb))$kbs[] = $kb;
                    }

                    if(!empty($kbs)){
                        $tmp['reply_markup']['inline_keyboard'] = $kbs;
        //                $tmp['reply_markup']['one_time_keyboard'] = true;
        //                $tmp['reply_markup']['resize_keyboard'] = true;
                    }
                }
            break;
        }

        $this->TelegramSendJob( ($is_update)?"editMessageText":"sendMessage", $tmp);
//        return $this->apiRequestJson(($is_update)?"editMessageText":"sendMessage", $tmp );

//        $this->last_message_id = 1;
    }

//------------------------------------------------------------------------------
    public function answerCallbackQuery($response_id,$content,$commands=null,$html_mark='HTML',$cols=2,$deleteLast = false){//commands : ['com1'=>null,'com2'=>'request_contact','com3'=>'request_location']
//        if($deleteLast) $this->deleteLastMessage($chat_id,$reply_id);
        $tmp = [
                    'callback_query_id' => $response_id,
                    "text" => $content,
        ];

//        if($html) $tmp['parse_mode'] = 'html';
//        $tmp['disable_web_page_preview'] = true;
        if($commands) {
            $tmp['reply_markup']['inline_keyboard'] = [];

            $c = 1;
            $kb = [];
            $kbs = [];
            foreach($commands as $command=>$params)
            {
                if(isset($params['condition'])){
                    if($params['condition'] === false) continue;
                    unset($params['condition']);
                }

                $t = ['text' => $command];
                $t = array_merge($t,$params);
                $kb[] = $t;

                if($c++%$cols == 0){$kbs[] = $kb;$kb=[];}
            }

            if(!empty($kb)){
                $kbs[] = $kb;
            }

            if(!empty($kbs)){
                $tmp['reply_markup']['inline_keyboard'] = $kbs;
//                $tmp['reply_markup']['one_time_keyboard'] = true;
//                $tmp['reply_markup']['resize_keyboard'] = true;
            }
        }
//        elseif(is_array($commands)){
////            $tmp['reply_markup']['remove_keyboard'] = true;
////            $tmp['reply_markup'] = 'replyKeyboardHide';
//        }



        $this->TelegramSendJob( "sendMessage", $tmp);
    }

//------------------------------------------------------------------------------
    public function editMessageReplyMarkup($response_id, $commands=null,$cols=2,$deleteLast = false){//commands : ['com1'=>null,'com2'=>'request_contact','com3'=>'request_location']
//        if($deleteLast) $this->deleteLastMessage();
        $tmp = [
            'message_id' => $response_id,
        ];

        if($commands) {
            $tmp['reply_markup']['inline_keyboard'] = [];

            $c = 1;
            $kb = [];
            $kbs = [];
            foreach($commands as $command=>$params)
            {
                if(isset($params['condition'])){
                    if($params['condition'] === false) continue;
                    unset($params['condition']);
                }

                $t = ['text' => $command];
                $t = array_merge($t,$params);
                $kb[] = $t;

                if($c++%$cols == 0){$kbs[] = $kb;$kb=[];}
            }

            if(!empty($kb)){
                $kbs[] = $kb;
            }

            if(!empty($kbs)){
                $tmp['reply_markup']['inline_keyboard'] = $kbs;
//                $tmp['reply_markup']['one_time_keyboard'] = true;
//                $tmp['reply_markup']['resize_keyboard'] = true;
            }
        }
        elseif(is_array($commands)){
//            $tmp['reply_markup']['remove_keyboard'] = true;
//            $tmp['reply_markup'] = 'replyKeyboardHide';
        }



        $this->TelegramSendJob( "editMessageReplyMarkup", $tmp );
    }


    /*
     * mode: MonYear, Day, HourMin
     * base: Current Input, Y|m|d|H|i
     * fromPoint: Start From
     * endPoint: Upper Bound Limit, null to Infinite Upper Bound
     * page: send current page
     * itemRowsPerPage: each page has # rows of items (not nav) {Today & Tommorow needs One Row Eachself, Other has two col in one row}
     *
     * fromPoint <= base <= endPoint
     */
    public function dateTimeStream($chat_id,$mode,$base=null,$fromPoint=null,$endPoint=null,$page=1,$itemRowsPerPage=3){
        $ret = ['items'=> null, 'navs'=> null, 'page'=>0];
        $today = \App\Libraries\jDateTime::date('Y|m|d',time(),false);
        $main_point = \App\Libraries\jDateTime::date('Y|m|d|w|H|i',strtotime("+2 hours", time()),false);
        if(!isset($fromPoint) || compareDates($fromPoint, $main_point, 'Y|m|d|w|H|i')==1 ) $fromPoint = $main_point;
        if(!isset($base) || compareDates($base , $fromPoint, 'Y|m|d|w|H|i')==1 ) $base = $fromPoint;
        $fromPoint = explode('|',$fromPoint);
        $fromPoint = array_map('intval',$fromPoint);
        $base = explode('|',$base);
        $base = array_map('intval',$base);
        $today = explode('|',$today);
        $today = array_map('intval',$today);
        if(!isset($base[5]))$base[5] = 0;
        $max = [0,31,31,31,31,31,31,30,30,30,30,30,29];

        if($base[3] == 9){
            $greg = \App\Libraries\jDateTime::jalaliToGreg_arr($base[0].'/'.$base[1].'/'.$base[2]);
            $greg = mktime(1,0,0,$greg[1],$greg[2],$greg[0]);
            $base[3] = (date('w', $greg)+8)%7;
        }

        if($base[4]==23 && $base[5]==30){
            $base[5]=0;
            $base[4]=0;
            $base[2]++;
            $base[3] = ($base[3]+1)%7;
            if($base[2] > $max[$base[1]]){
                $base[1]++;
                $base[2]=1;
                if($base[1] > 12){
                    $base[0]++;
                    $base[1]=1;
                }
            }
        }

//mylog('warning',1,[$mode,$today,$base,$fromPoint,$endPoint,$page,$itemRowsPerPage]);


        $arr = [];
        $YMDHIS=null;
//        $colsPerRow=2;
        $base[5]=59;
        switch($mode){
            case 'MonYear':// اردیبهشت ۱۳۹۷
                $colsPerRow = 2;
                $YMDHIS = 'M';

                for($i=0; $i < $page*$itemRowsPerPage*$colsPerRow; $i++){
                    $arr['Y'][] = $base[0];
                    $arr['M'][] = $base[1];
                    $base[1]++;
                    if($base[1]>12){$base[1]=1;$base[0]++;}
                }
            break;
            //-----------------------
            case 'Day':// سه شنبه، ۱۴ اردیبهشت-  امروز-  فردا، ۱۵ اردیبهشت
                $colsPerRow = 2;
                $YMDHIS = 'D';

                for($i=$base[2]; $i <= $max[$base[1]]; $i++){
                    $arr['Y'][] = $base[0];
                    $arr['M'][] = $base[1];
                    $arr['D'][] = $i;
                    $arr['W'][] = (7+$base[3]-$base[2]+$i)%7;
                }
            break;
            //-----------------------
            case 'HourMin': // ۱۲:۴۵ ظهر
                $colsPerRow = 2;
                $divEachHour = 2;
                $YMDHIS = 'I';

                $quanta = 60 / $divEachHour;
                for($j=ceil($base[5]/$quanta)*$quanta; $j < 60; $j+=$quanta){
                    $arr['Y'][] = $base[0];
                    $arr['M'][] = $base[1];
                    $arr['D'][] = $base[2];
//                      $arr['W'][] = (7+$base[3]-$base[2])%7;
                    $arr['W'][] = $base[3];
                    $arr['H'][] = $base[4];
                    $arr['I'][] = substr('0'.$j,-2);
                }
                for($i=$base[4]+1; $i <= 23; $i++){
                    for($j=0; $j < 60; $j+=$quanta){
                        $arr['Y'][] = $base[0];
                        $arr['M'][] = $base[1];
                        $arr['D'][] = $base[2];
//                      $arr['W'][] = (7+$base[3]-$base[2])%7;
                        $arr['W'][] = $base[3];
                        $arr['H'][] = $i;
                        $arr['I'][] = substr('0'.$j,-2);
                    }
                }
            break;
        }


        if($page == 'last') {
            $page = ceil(count($arr[$YMDHIS])/$itemRowsPerPage/$colsPerRow);
        }
        else {
            $page = min($page,ceil(count($arr[$YMDHIS])/$itemRowsPerPage/$colsPerRow));
        }

        switch($mode){
            case 'MonYear':// اردیبهشت ۱۳۹۷
                for(
                    $i=($page-1)*$itemRowsPerPage*$colsPerRow;
                    $i < min(count($arr[$YMDHIS]),$page*$itemRowsPerPage*$colsPerRow);
                    $i += $colsPerRow){

                        $tmp = [];
                        for($j=$colsPerRow-1; $j >= 0; $j--){
                            if(isset($arr[$YMDHIS][$i+$j])){
                                $tmp[__('dates.months.'.$arr['M'][$i+$j]).' '.$arr['Y'][$i+$j]] = [
                                    'callback_data'=>$mode.'|Y'.$arr['Y'][$i+$j].'|M'.$arr['M'][$i+$j]
                                ];
                            }
                        }
                        if(!empty($tmp)) $ret['items'][] =  $tmp;
                }
            break;
            //-----------------------
            case 'Day':// سه شنبه، ۱۴ اردیبهشت-  امروز-  فردا، ۱۵ اردیبهشت
                for(
                    $i=($page-1)*$itemRowsPerPage*$colsPerRow;
                    $i < min(count($arr[$YMDHIS]),$page*$itemRowsPerPage*$colsPerRow);
                    $i += $colsPerRow){
                        $tmp = [];
                        $tmp2 = [];
                        for($j=$colsPerRow-1; $j >= 0; $j--){
                            if(isset($arr[$YMDHIS][$i+$j])){
                                $is_today = ($today[0] == $arr['Y'][$i+$j]) &&
                                        ($today[1] == $arr['M'][$i+$j]) &&
                                        ($today[2] == $arr['D'][$i+$j]);

                                $is_tomorrow = ($today[0] == $arr['Y'][$i+$j]) &&
                                        ($today[1] == $arr['M'][$i+$j]) &&
                                        ($today[2]+1 == $arr['D'][$i+$j]);


                                $tmp[($is_today?__('dates.days.today').', ':(
                                        $is_tomorrow?__('dates.days.tomorrow').', ':''))
                                    .__('dates.days.'.$arr['W'][$i+$j])
                                        .', '.$arr['D'][$i+$j].' '.__('dates.months.'.$arr['M'][$i+$j])
                                    ] = [
                                            'callback_data'=>$mode.'|Y'.$arr['Y'][$i+$j].'|M'.$arr['M'][$i+$j].'|D'.$arr['D'][$i+$j].'|W'.$arr['W'][$i+$j]
                                        ];
                                if($is_today){
                                    $tmp2['today'] =  $tmp;
                                    $tmp = [];
                                }
                                if($is_tomorrow){
                                    $tmp2['tomorrow'] =  $tmp;
                                    $tmp = [];
                                }
                            }
                        }
                        if(isset($tmp2['today'])) {
                            $ret['items'][] =  $tmp2['today'];
                        }
                        if(isset($tmp2['tomorrow'])) {
                            $ret['items'][] =  $tmp2['tomorrow'];
                        }
                        if(!empty($tmp)) $ret['items'][] =  $tmp;
                }
            break;
            //-----------------------
            case 'HourMin': // ۱۲:۴۵ ظهر
                for(
                    $i=($page-1)*$itemRowsPerPage*$colsPerRow;
                    $i < min(count($arr[$YMDHIS]),$page*$itemRowsPerPage*$colsPerRow);
                    $i += $colsPerRow){

                        $tmp = [];
                        for($j=$colsPerRow-1; $j >= 0; $j--){
                            if(isset($arr[$YMDHIS][$i+$j])){
                                $tmp[
//                                    __('dates.hours.title').' '.
                                    __('dates.hour_emojy.'.(($arr['H'][$i+$j]-1)%12+1).':'.$arr['I'][$i+$j]).' '.(($arr['H'][$i+$j]-1)%12+1).':'.$arr['I'][$i+$j].' '.__('dates.hours.'.$arr['H'][$i+$j])] = [
                                        'callback_data'=>$mode.'|Y'.$arr['Y'][$i+$j].'|M'.$arr['M'][$i+$j].'|D'.$arr['D'][$i+$j].'|W'.$arr['W'][$i+$j].'|H'.$arr['H'][$i+$j].'|I'.$arr['I'][$i+$j]
                                ];
                            }
                        }
                        if(!empty($tmp)) $ret['items'][] =  $tmp;
                }
            break;
        }

        $tmp=[];
        $att=[];
        switch($mode){
            case 'MonYear':// اردیبهشت ۱۳۹۷
                $tmp[__('composers_bots.buttons.next_page')] = ['callback_data'=>'next_page','condition'=>($page < 3)];
                $tmp[__('composers_bots.buttons.prev_page')] = ['callback_data'=>'prev_page','condition'=>($page > 1)];
                $ret['navs'][] = $tmp; $tmp=[];
            break;
            //-----------------------
            case 'Day':// سه شنبه، ۱۴ اردیبهشت-  امروز-  فردا، ۱۵ اردیبهشت
                $att[1]['Y'] = $arr['Y'][0];
                $att[1]['M'] = $arr['M'][0]+1;
                if($att[1]['M']>12){
                    $att[1]['Y']++;
                    $att[1]['M'] = 1;
                }
                $att[0]['Y'] = $arr['Y'][0];
                $att[0]['M'] = $arr['M'][0]-1;
                if($att[0]['M']<1){
                    $att[0]['Y']--;
                    $att[0]['M'] = 12;
                }

                $tmp[__('composers_bots.buttons.next_page')] = ['callback_data'=>'next_page'
                    ,'condition'=>(count($arr[$YMDHIS])>$page*$itemRowsPerPage*$colsPerRow)];
                $tmp[__('composers_bots.buttons.prev_page')] = ['callback_data'=>'prev_page'
                    ,'condition'=>($page > 1)];
                $ret['navs'][] = $tmp; $tmp=[];

                $tmp[__('composers_bots.buttons.select_mon')] = ['callback_data'=>'select_mon'];
                $tmp[__('composers_bots.buttons.next_mon',['month'=>__('dates.months.'.$att[1]['M']).' '.$att[1]['Y']])] = ['callback_data'=>'next_mon|Y'.$att[1]['Y'].'|M'.$att[1]['M']
                    ,'condition'=>($att[1]['Y']<=$fromPoint[0]+2)];
                $tmp[__('composers_bots.buttons.prev_mon',['month'=>__('dates.months.'.$att[0]['M']).' '.$att[0]['Y']])] = ['callback_data'=>'prev_mon|Y'.$att[0]['Y'].'|M'.$att[0]['M']
                    ,'condition'=>(($att[0]['Y']>$fromPoint[0])||($att[0]['Y']==$fromPoint[0] && $att[0]['M']>=$fromPoint[1])) ];
                $ret['navs'][] = $tmp; $tmp=[];
            break;
            //-----------------------
            case 'HourMin': // ۱۲:۴۵ ظهر
                $tmp[__('composers_bots.buttons.next_page')] = ['callback_data'=>'next_page','condition'=>(count($arr[$YMDHIS])>$page*$itemRowsPerPage*$colsPerRow)];
                $tmp[__('composers_bots.buttons.prev_page')] = ['callback_data'=>'prev_page','condition'=>($page > 1)];
                $ret['navs'][] = $tmp; $tmp=[];

                $tmp[__('composers_bots.buttons.select_day')] = ['callback_data'=>'select_day'];
                $tmp[__('composers_bots.buttons.select_mon')] = ['callback_data'=>'select_mon'];
                $ret['navs'][] = $tmp; $tmp=[];
            break;
        }

        $ret['page'] = $page;

        return $ret;
    }

//------------------------------------------------------------------------------
    public function createBanner0($eventt_id,$language,$ret_image=false){
        try{
            $event_data = Eventt::findOrFail($eventt_id);
//            $bg = \App\Models\EventtCategoryBG::find($event_data->category->id == 6 ?2:1);
            $bg = $event_data->BannerTheme;

            $manager = new \Intervention\Image\ImageManager(array('driver' => 'imagick'));
            $img = $manager->make($bg->image->path);

            $pos_y = $bg->positions['top_margin'];
            $pos_x_step = $bg->positions['x_step'];
            $pos_y_step1 = $bg->positions['y_step_small'];
            $pos_y_step2 = $bg->positions['y_step_big'];
            $height = 0;

            $pos_x = $img->getWidth() - $bg->positions['x_margin'];//persian text
            $size = get_best_fit("دورهمی".$event_data->title[$language],$img->getWidth()-3*$pos_x_step-75);
            $sz = set_text_persian($img, $pos_x, $pos_y, $size, $bg->colors['header'],"دورهمی"); $pos_x -= $sz['width'] + $pos_x_step;
            $sz = set_text_persian($img, $pos_x, $pos_y, $size, $bg->colors['highlight'],$event_data->title[$language]);
//                $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['header'], $bg->colors['header'],"دورهمی"); $pos_x -= $sz['width'] + $pos_x_step;
//                $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['header'], $bg->colors['highlight'],$event_data->title[$language]);


            $tmp = $event_data['summary'];
            if(empty($tmp))$tmp = $event_data['description'];
            $pos_x = $img->getWidth() - $bg->positions['x_margin'];//persian text
            $pos_y += $pos_y_step2 + $sz['height']; $height += $pos_y_step2 + $sz['height'];
            $tmp = explode('|||',mb_wordwrap($tmp,60,"|||"));

            foreach($tmp as $t){
                $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['value'], $bg->colors['value'],trim($t));
                $pos_y += (end($tmp) == $t)?$pos_y_step2:$pos_y_step1;
                $height += (end($tmp) == $t)?$pos_y_step2:$pos_y_step1;
            }

            $pos_x = $img->getWidth() - $bg->positions['x_margin'];//persian text
            $pos_y += $pos_y_step2; $height += $pos_y_step2;
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['label'], $bg->colors['label'],"این دورهمی رو");  $pos_x -= $sz['width'] + $pos_x_step;
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['highlight'], $bg->colors['highlight'],$event_data->composer->fullname);  $pos_x -= $sz['width'] + $pos_x_step;
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['label'], $bg->colors['label'],"برگزار می کنه");

            $pos_x = $img->getWidth() - $bg->positions['x_margin'];//persian text
            $pos_y += $pos_y_step1; $height += $pos_y_step1;
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['highlight'], $bg->colors['highlight'],$event_data->composer->fullname);  $pos_x -= $sz['width'] + $pos_x_step;
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['label'], $bg->colors['label'],"تا حالا");  $pos_x -= $sz['width'] + $pos_x_step;
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['highlight'], $bg->colors['highlight'], $event_data->composer->events()->count()." دورهمی");  $pos_x -= $sz['width'] + $pos_x_step;
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['label'], $bg->colors['label'],"اجرا کرده");

            $pos_x = $img->getWidth() - $bg->positions['x_margin'];//persian text
            $pos_y += 2*$pos_y_step2; $height += 2*$pos_y_step2;
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['label'], $bg->colors['label'], "مهلت ثبت نام");
            $pos_y += $pos_y_step1; $height += $pos_y_step1;
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['value'], $bg->colors['value'],Eventt::dateBeautifier($event_data->register_deadline));

            $pos_x = $img->getWidth() - $bg->positions['x_margin'];//persian text
            $pos_y += $pos_y_step2; $height += $pos_y_step2;
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['label'], $bg->colors['label'], "زمان شروع");
            $pos_y += $pos_y_step1; $height += $pos_y_step1;
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['value'], $bg->colors['value'],Eventt::dateBeautifier($event_data->start_date));

            $pos_x = ceil(($img->getWidth() - $bg->positions['x_margin'])/2);
            $pos_y -= $pos_y_step1;
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['label'], $bg->colors['label'], "مدت دورهمی");
            $pos_y += $pos_y_step1;
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['value'], $bg->colors['value'],$event_data->duration);

            $pos_x = $img->getWidth() - $bg->positions['x_margin'];//persian text
            $pos_y += $pos_y_step2; $height += $pos_y_step2;
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['label'], $bg->colors['label'], "حداقل تعداد");
            $pos_y += $pos_y_step1; $height += $pos_y_step1;
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['value'], $bg->colors['value'],
                    (($event_data['capacity']['min'] == 0)?__('composers_bots.buttons.no_min_cap'):$event_data['capacity']['min']." نفر")
                );

            $pos_x = ceil(($img->getWidth() - $bg->positions['x_margin'])/2);
            $pos_y -= $pos_y_step1;
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['label'], $bg->colors['label'], "حداکثر تعداد");
            $pos_y += $pos_y_step1;
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['value'], $bg->colors['value'],
                    (($event_data['capacity']['max'] == 0)?__('composers_bots.buttons.no_max_cap'):$event_data['capacity']['max']." نفر")
                );

            if(isset($event_data['base_location']['title']['fa'])){
                $pos_x = $img->getWidth() - $bg->positions['x_margin'];//persian text
                $pos_y += $pos_y_step2; $height += $pos_y_step2;
                $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['label'], $bg->colors['label'], "عنوان محل قرار");
                $pos_y += $pos_y_step1; $height += $pos_y_step1;
                $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['value'], $bg->colors['value'],$event_data['base_location']['title']['fa']);
            }

            $pos_x = $img->getWidth() - $bg->positions['x_margin'];//persian text
            $pos_y += $pos_y_step2; $height += $pos_y_step2;
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['label'], $bg->colors['label'], "آدرس محل قرار");
            $pos_y += $pos_y_step1; $height += $pos_y_step1;
            $tmp = explode('|||',mb_wordwrap($event_data['base_location']['geocode'],50,"|||"));
            foreach($tmp as $t){
                $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['value'], $bg->colors['value'],trim($t));
                $pos_y += (end($tmp) == $t)?0:$pos_y_step1;
                $height += (end($tmp) == $t)?0:$pos_y_step1;
            }

            $pos_x = $img->getWidth() - $bg->positions['x_margin'];//persian text
            $pos_y += $pos_y_step2; $height += $pos_y_step2;
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['label'], $bg->colors['label'], "هزینه");
            $pos_y += $pos_y_step1; $height += $pos_y_step1;
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['value'], $bg->colors['value'],
                    $event_data['cost']==0?'رایگان':($event_data['cost']<0?'دونگی':number_format($event_data['cost']+$event_data['wage'])." تومان "));

            $pos_x = $img->getWidth() - $bg->positions['x_margin'];//persian text
            $pos_y += $pos_y_step2; $height += $pos_y_step2;
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['highlight'], $bg->colors['header'], "برای شرکت در این دورهمی به روبوت "); $pos_x -= $sz['width'] + $pos_x_step;
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['highlight'], $bg->colors['highlight'], "@".$event_data->category->bot->username);
            $pos_x = $img->getWidth() - $bg->positions['x_margin'];//persian text
            $pos_y += $pos_y_step1; $height += $pos_y_step1;
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['highlight'], $bg->colors['header'], "در تلگرام مراجعه کن و بسادگی ثبت نام کن .");

            $pos_x = $img->getWidth() - $bg->positions['x_margin'];//persian text
            $pos_y += $pos_y_step2; $height += $pos_y_step2;
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['highlight'], $bg->colors['highlight'], "امنیت و مدیریت آسون دورهمی"); $pos_x -= $sz['width'] + $pos_x_step;
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['highlight'], $bg->colors['header'], "و"); $pos_x -= $sz['width'] + $pos_x_step;
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['highlight'], $bg->colors['highlight'], "کلی امتیاز و جایزه");

            $pos_x = $img->getWidth() - $bg->positions['x_margin'];//persian text
            $pos_y += $pos_y_step1; $height += $pos_y_step1;
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['highlight'], $bg->colors['header'], "رو با"); $pos_x -= $sz['width'] + $pos_x_step;
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['highlight'], $bg->colors['highlight'], "تیم حرفه ای دورهمی ها"); $pos_x -= $sz['width'] + $pos_x_step;
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['highlight'], $bg->colors['header'], "تجربه کن");
            $height += $sz['height']+$pos_y_step2+$bg->positions['top_margin'];

            $sz = set_text_english($img, 50, 280, $bg->font_sizes['robot'], $bg->colors['robot'],"@".$event_data->category->bot->username,270);

            $path = 'uploads/cards/img_'.time().'.png';
            $img->crop(700,$height,0,0);
            $img->interlace();
            $img->save(public_path($path));
        }
        catch(Exception $e){
            getFormedError($e);
        }

        return ($ret_image?$img:url(assetlink($path)));
    }
//------------------------------------------------------------------------------
    public function createBanner($eventt_id,$language,$ret_image=false){
        try{
            $event_data = Eventt::findOrFail($eventt_id);
            $bg = $event_data->BannerTheme;

//            $manager = new \Intervention\Image\ImageServiceProvider(array('driver' => 'imagick'));
            $img_bk = IntImage::make($bg->image->path);
            $img = IntImage::canvas($img_bk->getWidth(),$img_bk->getHeight());

            $pos_y = $bg->positions['top_margin'];
            $pos_x_step = $bg->positions['x_step'];
            $pos_y_step1 = $bg->positions['y_step_small'];
            $pos_y_step2 = $bg->positions['y_step_big'];
            $height = 0;
            $bkcolor['header'] = 'rgba('.implode(',',colorHex2RGB(substr($bg->backmarks['header_color'],1))).','.$bg->backmarks['header_opacity'].')';
            $bkcolor['content'] = 'rgba('.implode(',',colorHex2RGB(substr($bg->backmarks['content_color'],1))).','.$bg->backmarks['content_opacity'].')';
//______________

            $pos_x = $img->getWidth() - $bg->positions['x_margin'];//persian text
    $box['x1'] = 0;
    $box['y1'] = $pos_y - $bg->backmarks['margin_y'];
            $size = get_best_fit("دورهمی".$event_data->title[$language],$img->getWidth()-3*$pos_x_step-75);
            $sz = set_text_persian($img, $pos_x, $pos_y, $size, $bg->colors['header_norm'],"دورهمی"); $pos_x -= $sz['width'] + $pos_x_step;
            $sz = set_text_persian($img, $pos_x, $pos_y, $size, $bg->colors['header'],$event_data->title[$language]);

    $box['x2'] = $img_bk->getWidth();
    $box['y2'] = $box['y1']+$sz['height']+$bg->backmarks['margin_y'];
    $img_bk->rectangle($box['x1'], $box['y1'], $box['x2'], $box['y2'], function ($draw) use ($bkcolor) {
        $draw->background($bkcolor['header']);
    });
//______________

            $tmp = $event_data['summary'];
            if(empty($tmp))$tmp = $event_data['description'];
            $pos_x = $img->getWidth() - $bg->positions['x_margin'];//persian text
            $pos_y += $pos_y_step2 + $sz['height']; $height += $pos_y_step2 + $sz['height'];
    $box['x1'] = $pos_x + $bg->backmarks['margin_x'];
    $box['y1'] = $pos_y - $bg->backmarks['margin_y'];
            $tmp = explode('|||',mb_wordwrap($tmp,60,"|||"));
            foreach($tmp as $t){
                $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['value'], $bg->colors['value'],trim($t));
                $pos_y += (end($tmp) == $t)?$pos_y_step2:$pos_y_step1;
                $height += (end($tmp) == $t)?$pos_y_step2:$pos_y_step1;
            }

    $box['x2'] = $bg->positions['x_margin'] - $bg->backmarks['margin_x'];
    $box['y2'] = $pos_y - $pos_y_step2 + $sz['height'] + $bg->backmarks['margin_y'];
    $img_bk->rectangle($box['x1'], $box['y1'], $box['x2'], $box['y2'], function ($draw) use ($bkcolor) {
        $draw->background($bkcolor['content']);
    });
//______________

            $pos_x = $img->getWidth() - $bg->positions['x_margin'];//persian text
            $pos_y += $pos_y_step2; $height += $pos_y_step2;
    $box['x1'] = $pos_x + $bg->backmarks['margin_x'];
    $box['y1'] = $pos_y - $bg->backmarks['margin_y'];
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['label'], $bg->colors['label'],"این دورهمی رو");  $pos_x -= $sz['width'] + $pos_x_step;
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['highlight'], $bg->colors['highlight'],$event_data->composer->fullname);  $pos_x -= $sz['width'] + $pos_x_step;
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['label'], $bg->colors['label'],"برگزار می کنه");

            $pos_x = $img->getWidth() - $bg->positions['x_margin'];//persian text
            $pos_y += $pos_y_step1; $height += $pos_y_step1;
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['highlight'], $bg->colors['highlight'],$event_data->composer->fullname);  $pos_x -= $sz['width'] + $pos_x_step;
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['label'], $bg->colors['label'],"تا حالا");  $pos_x -= $sz['width'] + $pos_x_step;
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['highlight'], $bg->colors['highlight'], $event_data->composer->events()->count()." دورهمی");  $pos_x -= $sz['width'] + $pos_x_step;
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['label'], $bg->colors['label'],"اجرا کرده");

    $box['x2'] = $bg->positions['x_margin'] - $bg->backmarks['margin_x'];
    $box['y2'] = $pos_y + $sz['height'] + $bg->backmarks['margin_y'];
    $img_bk->rectangle($box['x1'], $box['y1'], $box['x2'], $box['y2'], function ($draw) use ($bkcolor) {
        $draw->background($bkcolor['content']);
    });
//______________

            $pos_x = $img->getWidth() - $bg->positions['x_margin'];//persian text
            $pos_y += 2*$pos_y_step2; $height += 2*$pos_y_step2;
    $box['x1'] = $pos_x + $bg->backmarks['margin_x'];
    $box['y1'] = $pos_y - $bg->backmarks['margin_y'];
            $sz1 = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['label'], $bg->colors['label'], "مهلت ثبت نام");
            $pos_y += $pos_y_step1; $height += $pos_y_step1;
            $sz2 = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['value'], $bg->colors['value'],Eventt::dateBeautifier($event_data->register_deadline));
    $box['x2'] = $bg->positions['x_margin'] - $bg->backmarks['margin_x'];
    $box['y2'] = $pos_y + $sz['height'] + $bg->backmarks['margin_y'];
    $img_bk->rectangle($box['x1'], $box['y1'], $box['x2'], $box['y2'], function ($draw) use ($bkcolor) {
        $draw->background($bkcolor['content']);
    });
//______________

            $pos_x = $img->getWidth() - $bg->positions['x_margin'];//persian text
            $pos_y += $pos_y_step2; $height += $pos_y_step2;
    $box['x1'] = $pos_x + $bg->backmarks['margin_x'];
    $box['y1'] = $pos_y - $bg->backmarks['margin_y'];
            $sz1 = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['label'], $bg->colors['label'], "زمان شروع");
            $pos_y += $pos_y_step1; $height += $pos_y_step1;
            $sz2 = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['value'], $bg->colors['value'],Eventt::dateBeautifier($event_data->start_date));
    $box['x2'] = $bg->positions['x_margin'] - $bg->backmarks['margin_x'];
    $box['y2'] = $pos_y + $sz['height'] + $bg->backmarks['margin_y'];
    $img_bk->rectangle($box['x1'], $box['y1'], $box['x2'], $box['y2'], function ($draw) use ($bkcolor) {
        $draw->background($bkcolor['content']);
    });
//______________

            $pos_x = ceil(($img->getWidth() - $bg->positions['x_margin'])/2);
            $pos_y -= $pos_y_step1;
    $box['x1'] = $pos_x + $bg->backmarks['margin_x'];
    $box['y1'] = $pos_y - $bg->backmarks['margin_y'];
            $sz1 = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['label'], $bg->colors['label'], "مدت دورهمی");
            $pos_y += $pos_y_step1;
            $sz2 = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['value'], $bg->colors['value'],$event_data->duration);
    $box['x2'] = $bg->positions['x_margin'] - $bg->backmarks['margin_x'];
    $box['y2'] = $pos_y + $sz['height'] + $bg->backmarks['margin_y'];
    $img_bk->rectangle($box['x1'], $box['y1'], $box['x2'], $box['y2'], function ($draw) use ($bkcolor) {
        $draw->background($bkcolor['content']);
    });
//______________

            $pos_x = $img->getWidth() - $bg->positions['x_margin'];//persian text
            $pos_y += $pos_y_step2; $height += $pos_y_step2;
    $box['x1'] = $pos_x + $bg->backmarks['margin_x'];
    $box['y1'] = $pos_y - $bg->backmarks['margin_y'];
            $sz1 = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['label'], $bg->colors['label'], "حداقل تعداد");
            $pos_y += $pos_y_step1; $height += $pos_y_step1;
            $sz2 = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['value'], $bg->colors['value'],
                    (($event_data['capacity']['min'] == 0)?__('composers_bots.buttons.no_min_cap'):$event_data['capacity']['min']." نفر")
                );
    $box['x2'] = $bg->positions['x_margin'] - $bg->backmarks['margin_x'];
    $box['y2'] = $pos_y + $sz['height'] + $bg->backmarks['margin_y'];
    $img_bk->rectangle($box['x1'], $box['y1'], $box['x2'], $box['y2'], function ($draw) use ($bkcolor) {
        $draw->background($bkcolor['content']);
    });
//______________

            $pos_x = ceil(($img->getWidth() - $bg->positions['x_margin'])/2);
            $pos_y -= $pos_y_step1;
    $box['x1'] = $pos_x + $bg->backmarks['margin_x'];
    $box['y1'] = $pos_y - $bg->backmarks['margin_y'];
            $sz1 = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['label'], $bg->colors['label'], "حداکثر تعداد");
            $pos_y += $pos_y_step1;
            $sz2 = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['value'], $bg->colors['value'],
                    (($event_data['capacity']['max'] == 0)?__('composers_bots.buttons.no_max_cap'):$event_data['capacity']['max']." نفر")
                );
    $box['x2'] = $bg->positions['x_margin'] - $bg->backmarks['margin_x'];
    $box['y2'] = $pos_y + $sz['height'] + $bg->backmarks['margin_y'];
    $img_bk->rectangle($box['x1'], $box['y1'], $box['x2'], $box['y2'], function ($draw) use ($bkcolor) {
        $draw->background($bkcolor['content']);
    });
//______________

            if(isset($event_data['base_location']['title']['fa'])){
                $pos_x = $img->getWidth() - $bg->positions['x_margin'];//persian text
                $pos_y += $pos_y_step2; $height += $pos_y_step2;
    $box['x1'] = $pos_x + $bg->backmarks['margin_x'];
    $box['y1'] = $pos_y - $bg->backmarks['margin_y'];
                $sz1 = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['label'], $bg->colors['label'], "عنوان محل قرار");
                $pos_y += $pos_y_step1; $height += $pos_y_step1;
                $sz2 = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['value'], $bg->colors['value'],$event_data['base_location']['title']['fa']);
    $box['x2'] = $bg->positions['x_margin'] - $bg->backmarks['margin_x'];
    $box['y2'] = $pos_y + $sz['height'] + $bg->backmarks['margin_y'];
    $img_bk->rectangle($box['x1'], $box['y1'], $box['x2'], $box['y2'], function ($draw) use ($bkcolor) {
        $draw->background($bkcolor['content']);
    });
            }
//______________

            $pos_x = $img->getWidth() - $bg->positions['x_margin'];//persian text
            $pos_y += $pos_y_step2; $height += $pos_y_step2;
    $box['x1'] = $pos_x + $bg->backmarks['margin_x'];
    $box['y1'] = $pos_y - $bg->backmarks['margin_y'];
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['label'], $bg->colors['label'], "آدرس محل قرار");
            $pos_y += $pos_y_step1; $height += $pos_y_step1;
            $tmp = explode('|||',mb_wordwrap($event_data['base_location']['geocode'],50,"|||"));
            foreach($tmp as $t){
                $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['value'], $bg->colors['value'],trim($t));
                $pos_y += (end($tmp) == $t)?0:$pos_y_step1;
                $height += (end($tmp) == $t)?0:$pos_y_step1;
            }
    $box['x2'] = $bg->positions['x_margin'] - $bg->backmarks['margin_x'];
    $box['y2'] = $pos_y + $sz['height'] + $bg->backmarks['margin_y'];
    $img_bk->rectangle($box['x1'], $box['y1'], $box['x2'], $box['y2'], function ($draw) use ($bkcolor) {
        $draw->background($bkcolor['content']);
    });
//______________

            $pos_x = $img->getWidth() - $bg->positions['x_margin'];//persian text
            $pos_y += $pos_y_step2; $height += $pos_y_step2;
    $box['x1'] = $pos_x + $bg->backmarks['margin_x'];
    $box['y1'] = $pos_y - $bg->backmarks['margin_y'];
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['label'], $bg->colors['label'], "هزینه");
            $pos_y += $pos_y_step1; $height += $pos_y_step1;
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['value'], $bg->colors['value'],
                    $event_data['pay_type ']=='free'?'رایگان':($event_data['pay_type'] == 'dongi'?'دونگی':(
                        number_format($event_data['cost']+$event_data['wage'])." تومان ".($event_data['pay_type'] == 'online'?'درگاه پرداخت مطمئن':'نقدی در محل'))
                    ) );
    $box['x2'] = $bg->positions['x_margin'] - $bg->backmarks['margin_x'];
    $box['y2'] = $pos_y + $sz['height'] + $bg->backmarks['margin_y'];
    $img_bk->rectangle($box['x1'], $box['y1'], $box['x2'], $box['y2'], function ($draw) use ($bkcolor) {
        $draw->background($bkcolor['content']);
    });
//______________

            $pos_x = $img->getWidth() - $bg->positions['x_margin'];//persian text
            $pos_y += 2*$pos_y_step2; $height += 2*$pos_y_step2;
    $box['x1'] = 0;
    $box['y1'] = $pos_y - $bg->backmarks['margin_y'];
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['highlight'], $bg->colors['header'], "برای شرکت در این دورهمی به روبوت "); $pos_x -= $sz['width'] + $pos_x_step;
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['highlight'], $bg->colors['highlight'], "@".$event_data->category->bot->username);
            $pos_x = $img->getWidth() - $bg->positions['x_margin'];//persian text
            $pos_y += $pos_y_step1; $height += $pos_y_step1;
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['highlight'], $bg->colors['header'], "در تلگرام مراجعه کن و بسادگی ثبت نام کن .");
    $box['x2'] = $img_bk->getWidth();
    $box['y2'] = $pos_y + $sz['height'] + $bg->backmarks['margin_y'];
    $img_bk->rectangle($box['x1'], $box['y1'], $box['x2'], $box['y2'], function ($draw) use ($bkcolor) {
        $draw->background($bkcolor['content']);
    });
//______________

            $pos_x = $img->getWidth() - $bg->positions['x_margin'];//persian text
            $pos_y += $pos_y_step2; $height += $pos_y_step2;
    $box['x1'] = 0;
    $box['y1'] = $pos_y - $bg->backmarks['margin_y'];
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['highlight'], $bg->colors['highlight'], "امنیت و مدیریت آسون دورهمی"); $pos_x -= $sz['width'] + $pos_x_step;
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['highlight'], $bg->colors['header'], "و"); $pos_x -= $sz['width'] + $pos_x_step;
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['highlight'], $bg->colors['highlight'], "کلی امتیاز و جایزه");

            $pos_x = $img->getWidth() - $bg->positions['x_margin'];//persian text
            $pos_y += $pos_y_step1; $height += $pos_y_step1;
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['highlight'], $bg->colors['header'], "رو با"); $pos_x -= $sz['width'] + $pos_x_step;
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['highlight'], $bg->colors['highlight'], "تیم حرفه ای دورهمی ها"); $pos_x -= $sz['width'] + $pos_x_step;
            $sz = set_text_persian($img, $pos_x, $pos_y, $bg->font_sizes['highlight'], $bg->colors['header'], "تجربه کن");
            $height += $sz['height']+$pos_y_step2+$bg->positions['top_margin'];
    $box['x2'] = $img_bk->getWidth();
    $box['y2'] = $pos_y + $sz['height'] + $bg->backmarks['margin_y'];
    $img_bk->rectangle($box['x1'], $box['y1'], $box['x2'], $box['y2'], function ($draw) use ($bkcolor) {
        $draw->background($bkcolor['content']);
    });
//______________

            $sz = set_text_english($img, 50, 280, $bg->font_sizes['robot'], $bg->colors['robot'],"@".$event_data->category->bot->username,270);
            $sz = set_text_english($img_bk, 50, 270, $bg->font_sizes['robot']+2, '#000000',"@".$event_data->category->bot->username,270);

            $path = 'uploads/cards/img_'.time().'.png';
            $img_bk->crop(700,$height,0,0);
            $img_bk->insert($img);
            $img_bk->interlace();
            $img_bk->save(public_path($path));
        }
        catch(Exception $e){
            getFormedError($e);
        }

        return ($ret_image?$img_bk:url(assetlink($path)));
    }
//------------------------------------------------------------------------------
    public function sendBanner($chat_id,$eventt_id,$language,$message,$ret_image=false,$forward_chat_id=null){
        $event_data = Eventt::findOrFail($eventt_id);

        $image_path = $event_data->banner_bg_type == 'automatic'?
            $this->createBanner($eventt_id,$language,$ret_image)
            :url($event_data->image->path);

        $this->sendPhoto($chat_id
            ,$message
            ,$image_path
            ,null
            ,$forward_chat_id);
    }
//------------------------------------------------------------------------------
    public function createBannerAlbum($chat_id,$cat_id,$language,$ret_image=false){
        $path = 'uploads/catAlbums/alb_cat_'.$cat_id.'.jpg';
        $img = null;
        if($ret_image || !file_exists(public_path($path))){
            try{
                $W = 70*6;
                $H = 130*6;
                $c = 2;
                $r = 3;
                $wd = 1;
                $hd = 1/3;

                $box = [20,20,'rgba(30,0,30, 0.6)'];//text bottom margin | box bottom margin | background

                $line_w = 2*5;
                $line_col = '#000';
                $border = false;

                $text_m = 40;
                $font_size = 25;
                $font_color = ['#f09','#eee'];
                $fh = 0;

                $bg = EventtCategoryBG::asArray($cat_id,$c*$r);

                $manager = new \Intervention\Image\ImageManager(array('driver' => 'imagick'));
                $img = $manager->canvas($W*$wd*$c,$H*$hd*$r,'#fff');

                $fh = getTextSize(($language=='fa'?"سایز عمودی قلم":"Vertical Font Size"),$font_size,$language,'height');
                $box_mpad = $fh+$box[0];

                for($i=0; $i<$r; $i++){
                    for($j=0; $j<$c; $j++){
                        if(!isset($bg[$i*$c+$j]))continue;
                        $img_tmp = $manager->make($bg[$i*$c+$j]->image->path)->resize($W,$H)->crop($W*$wd,$H*$hd,0,0);
                        $img->insert($img_tmp,'top-left',$j*$W*$wd,$i*$H*$hd);

                        $img->rectangle($j*$W*$wd, ($i+1)*$H*$hd-$box_mpad-$box[1], ($j+1)*$W*$wd, ($i+1)*$H*$hd-$box[1], function ($draw) use ($box) {
                            $draw->background($box[2]);
                        });

                        if($language == 'fa'){
                            $sz = set_text_persian($img,
                                    ($j+1)*$W*$wd-$text_m,
                                    (($i+1)*$H*$hd-$box[1]+($i+1)*$H*$hd-$box_mpad-$box[1])/2-$fh/2,
                                    $font_size,
                                    $font_color[0],"نام قالب :");
                            set_text_persian($img,
                                    ($j+1)*$W*$wd-$text_m-$sz['width'],
                                    (($i+1)*$H*$hd-$box[1]+($i+1)*$H*$hd-$box_mpad-$box[1])/2-$fh/2,
                                    $font_size,
                                    $font_color[1],$bg[$i*$c+$j]->title[$language]);
                        }
                        else{
                            $sz = set_text_english($img,
                                    $j*$W*$wd+$text_m,
                                    (($i+1)*$H*$hd-$box[1]+($i+1)*$H*$hd-$box_mpad-$box[1])/2-$fh/2,
                                    $font_size,
                                    $font_color[0],"Theme Title:");
                            set_text_english($img,
                                    $j*$W*$wd-$text_m-$sz['width'],
                                    (($i+1)*$H*$hd-$box[1]+($i+1)*$H*$hd-$box_mpad-$box[1])/2-$fh/2,
                                    $font_size,
                                    $font_color[1],$bg[$i*$c+$j]->title[$language]);
                        }

                    }
                }

                for($i=($border?0:1); $i<($border?$r+1:$r); $i++){
                    $img->line(0,$i*$H*$hd, $W*$wd*$c,$i*$H*$hd, function ($draw) use ($line_w,$line_col) {
                        $draw->color($line_col);
                        $draw->width($line_w);
                    });
                }
                for($j=($border?0:1); $j<($border?$c+1:$c); $j++){
                    $img->line($j*$W*$wd , 0,$j*$W*$wd, $H*$hd*$r, function ($draw) use ($line_w,$line_col) {
                        $draw->color($line_col);
                        $draw->width($line_w);
                    });
                }

                $img->interlace();
                $img->save(public_path($path));

    //            alertAdminTelegramImage('test',url(assetlink($path,true)));
            }
            catch(Exception $e){
                getFormedError($e);
            }
        }

        $ret = null;
        if(!is_null($chat_id)){$ret = $this->sendPhoto($chat_id,null,assetlink($path,false));}
        return ($ret_image?$img:$ret);
    }

//------------------------------------------------------------------------------
    public function sendPhoto($chat_id,$caption,$src=null,$html_mark=null,$forward_chat_id=null){

        $tmp = [
            'chat_id' => $chat_id,
            'photo'=>$src,
        ];

        if(!is_null($caption)){$tmp['caption'] = $caption;}

        $tmp['disable_notification'] = false;
        if($html_mark) $tmp['parse_mode'] = $html_mark;


        $this->TelegramSendJob( "sendPhoto", $tmp, $forward_chat_id );
    }

//------------------------------------------------------------------------------
    public function sendFile($chat_id,$caption,$type,$src=null,$html_mark=null,$forward_chat_id=null){

        $available_types = [
            'Photo',
            'MediaGroup',

            'Audio',
            'Document',
            'Voice',

            'Video',
            'Animation',

            'VideoNote',
            'Location',
        ];

        if(!in_array(ucwords($type), $available_types)) {
            mylog('error',0,'SendFileTelegram :: Wrong Type | '.$type);
            return false;
        }

        $tmp = [
            'chat_id' => $chat_id,
        ];
        $tmp[strtolower($type)] = $src;

//        switch($type){
//            case 'video':
//                $tmp['video'] = $src;
//                break;
//            case 'animation':
//                $tmp['animation'] = $src;
//                break;
//            case 'document':
//                $tmp['document'] = $src;
//                break;
//            case 'audio':
//                $tmp['audio'] = $src;
//                break;
//        }

        if(!is_null($caption)){$tmp['caption'] = $caption;}

        $tmp['disable_notification'] = false;
        if($html_mark) $tmp['parse_mode'] = $html_mark;


        $this->TelegramSendJob( "send".ucwords($type), $tmp, $forward_chat_id );
    }

//------------------------------------------------------------------------------
    public function getFile($chat_id, $file_id){
        return TelegramPost::getFileRequest($this->bot, $file_id);
    }

//------------------------------------------------------------------------------
    public function getPhoto($chat_id, $file_id){
        return TelegramPost::getPhotoRequest($this->bot, $file_id);
    }

//------------------------------------------------------------------------------
    public function sendVideo($chat_id,$caption,$src=null,$thumb_src=null,$html_mark='HTML',$deleteLast = false){
//        if($deleteLast) $this->deleteLastMessage($chat_id,$reply_id);
        $tmp = [
            'chat_id' => $chat_id,
            'video'=>$src,
        ];

        if(!is_null($thumb_src)){$tmp['thumb'] = $thumb_src;}
        if(!is_null($caption)){$tmp['caption'] = $caption;}

        $tmp['disable_notification'] = false;
//        $tmp['supports_streaming'] = false;
        if($html_mark) $tmp['parse_mode'] = $html_mark;

        $this->TelegramSendJob( "sendVideo", $tmp );
    }

//------------------------------------------------------------------------------
    public function TelegramSendJob($method, $params, $forward_chat_id=null){
        $msg = TelegramRobotMessage::handleSend($this->title,['method'=>$method, 'params'=>$params], $this->bot->id);
        dispatch(new TelegramSend($this->bot, $method, $params, $msg->id, $forward_chat_id));
//        TelegramSend::dispatch($this->bot, $method, $params, $msg->id, $forward_chat_id)->onQueue('rabbitmq');
    }



}
