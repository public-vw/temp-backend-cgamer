<?php

namespace App\Http\Controllers\TelegramBots\Bases;

use App\Http\Controllers\TelegramBots\Bases\BotMachine as BM;
use App\Models\Address;
use App\Models\Album;
use App\Models\Asset;
use App\Models\Eventt;
use App\Models\EventtCategory;
use App\Models\EventtCategoryBG;
use App\Models\Setting;
use App\User;

class ComposerBots extends BM
{
    const CACHE_LIFE_TIME = 1000;
    const MAX_SUBMIT_STEPS = 13;
    const SYMBOLS = 10;
    const PROGRESS_DIR = 'LTR';
    const SHOWTIME = false;
//:white_check_mark:✅
//:heavy_minus_sign:➖
//:white_small_square:▫
//:white_medium_small_square:◽
//:white_medium_square:◻
//:white_large_square:⬜
//:white_square_button:🔳
//:black_small_square:▪
//:black_medium_small_square:◾
//:black_medium_square:◼
//:black_large_square:⬛
//:black_square_button:🔲
//:white_circle:⚪
//:large_blue_circle:🔵
//:red_circle:🔴
//:black_circle:⚫
//    const progress_zero = '░';
//    const progress_one = '▒';
//    const progress_one = '▓';
    const progress_zero = '⚪';
    const progress_one = '🔵';

    static $base_state = 'wellcome';
    static $temp = null;

//==================================================================================================
//==================================================================================================

    protected static function reboot_state()
    {
        if (BM::$input_text === __('composers_bots.buttons.return_base')){
            return null;
        }
        if (BM::$input_btn === 'gobase') {
            return null;
        }
        if (!BM::hasCache('user_data') && !BM::hasCache('telegram_id')) {
            return null;
        }
        if (isset(BM::$input_param)) {
            return 'start_with_param';
        }
        if (isset(BM::$outter_cmd)) {
            return 'start_with_outter'; // Out of current session
        }
        return false;
    }

//==================================================================================================

    protected static function config()
    {

        static::$states['wellcome'] = new class extends State
        {
            var $btn_type = 'inline';

            function start()
            {
//                BM::clearCache();

                $user_data = null;
                if(!BM::hasCache('user_data')){
                    $user_data = ComposerBots::loadUserData();
                }
                else{
                    $user_data = BM::getCache('user_data');
                }
                if (isset($user_data)) { // Member
                    BM::setCache('user_data', $user_data);
                    $this->base_message = (
                        (time() - BM::getCache('last_seen', 0) > BM::HELLO_TIME) ?
                            __('composers_bots.messages.greeting.known', ['name' => $user_data->fullname])
                            : ''
                        )
                        . __('composers_bots.messages.select_one_item');
                    //                    . "نمایش امتیاز";

                    $this->buttons[][__('composers_bots.buttons.live_events_list')] = ['callback_data' => 'live_events_list', 'condition' => ($user_data->getStatedEvents('live')['total'] > 0)];
                    $this->buttons[][__('composers_bots.buttons.draft_events_list')] = ['callback_data' => 'draft_events_list', 'condition' => ($user_data->getStatedEvents(['draft', 'testaccepted'])['total'] > 0)];
                    $this->buttons[][__('composers_bots.buttons.create_event')] = ['callback_data' => 'create_event'];
                    $this->buttons[][__('composers_bots.buttons.events_list')] = ['callback_data' => 'accepted_events_list', 'condition' => ($user_data->getStatedEvents(['accepted', 'closed_list'])['total'] > 0)];
                    $this->buttons[][__('composers_bots.buttons.archived_events_list')] = ['callback_data' => 'archived_events_list', 'condition' => ($user_data->getStatedEvents(['finished', 'archived'])['total'] > 0)];
                    //                $this->buttons[][__('composers_bots.buttons.set_ref')] = ['callback_data'=>'add_ref','condition'=>!isset($user_data->referer_id)];
                    $this->buttons[][__('composers_bots.buttons.toturials_btn')] = ['callback_data' => 'toturials'];
                } else { // New User
                    BM::setCache('telegram_id', BM::$user_id);
                    $this->base_message = __('composers_bots.messages.greeting.unknown');

                    $this->buttons[][__('composers_bots.buttons.create_event')] = ['callback_data' => 'new_user_create_event'];
                    $this->buttons[][__('composers_bots.buttons.easy_register')] = ['callback_data' => 'easy_register'];
                    $this->buttons[][__('composers_bots.buttons.toturials_btn')] = ['callback_data' => 'toturials'];
                }

                BM::setState('wellcome_after');
            }
        };

//==================================================================================================

        static::$states['wellcome_after'] = new class extends State
        {
            var $btn_type = 'inline';

            function start()
            {
            }

            function action()
            {
                switch (BM::$input_btn) {
                    case 'toturials':
                        return BM::setState('toturials_list');
                        break;
                    case 'live_events_list':
                        BM::setCache('events_mode', 'live');
                        return BM::setState('events_list');
                        break;
                    case 'accepted_events_list':
                        BM::setCache('events_mode', 'accepted');
                        return BM::setState('events_list');
                        break;
                    case 'draft_events_list':
                        BM::setCache('events_mode', 'draft');
                        return BM::setState('events_list');
                        break;
                    case 'archived_events_list':
                        BM::setCache('events_mode', 'finished');
                        return BM::setState('events_list');
                        break;
                    case 'create_event':
                        return BM::setState('create_cat_1',__('composers_bots.alerts.creation_warning'));
                        break;
                    case 'easy_register':
                        BM::setCache('mode', 'register');
                        return BM::setState('register_mobile');
                        break;
                    case 'new_user_create_event':
                        BM::setCache('mode', 'event_make');
                        return BM::setState('register_mobile');
                        break;

                    default:
                        if (DEBUG_MODE && in_array(BM::$user_id, BM::DEBUG_USRS_ID)) {
                            $this->addMessage("محتوای وارد شده معتبر نمی باشد" . "<pre>" . var_export(BM::$input_btn, true) . "</pre>");
                        }
                        return BM::setState('wellcome');
                }
            }
        };

//==================================================================================================

        static::$states['start_with_param'] = new class extends State
        {

            var $btn_type = 'inline';

            function action()
            {
                BM::setCache('user_data', ComposerBots::loadUserData());

                $tmp = null;
                if (strpos(BM::$input_param, 'P') === 0) {// P [3 random digits] [process_id] [2 random digits]
                    $pid = trim(substr(BM::$input_param, 1)); // Remove 'P'
                    $tmp['process'] = substr($pid, 3, -2);// Remove Random Digits
                }
                /*                elseif (strpos(BM::$input_param, 'D') === 0) { // D [length of referer id] [event_id] [referer_id] [length of event id]
                                    $d = [substr(trim(BM::$input_param), -1, 1), substr(trim(BM::$input_param), 1, 1)];
                                    $evid = substr(trim(BM::$input_param), 2, $d[0]);
                                    $refid = substr(trim(BM::$input_param), -2, $d[1]);
                                    $tmp['event'] = Eventt::find($evid);
                                    $tmp['referer'] = User::find($refid);
                                }*/

                /*                if (isset($tmp['referer'])) {
                                    BM::setCache('event_referer', $tmp['referer']->id);
                                }*/

                if (isset($tmp['process'])) {
                    BM::setCache('process', $tmp['process']);
                    BM::setState('manage_process');

                }

                return BM::setState('wellcome', __('composers_bots.messages.param_not_found'));
            }
        };

        // OC||state_name||command||csv_of_input_parameters
        static::$states['start_with_outter'] = new class extends State
        {

            var $btn_type = 'inline';

            function action()
            {
                BM::setCache('user_data', TheTimeRobot::loadUserData());


                $tmp = explode('|', BM::$outter_cmd);
                array_push($tmp, explode(',', array_pop($tmp)));
                $tmp = array_combine(['state', 'command', 'params'], $tmp);

                BM::setCache('OutterData', $tmp);

                return BM::setState($tmp['state']) ? true : BM::setState('welcome', __('bots.messages.event_not_found'));
            }
        };

        static::$states['manage_process'] = new class extends State
        {

            var $btn_type = 'inline';

            function action()
            {
                return BM::setState('wellcome', __('composers_bots.messages.param_not_found'));
            }
        };

//==================================================================================================
//==================================================================================================

        static::$states['toturials_list'] = new class extends State
        {

            var $btn_type = 'inline';

            function start()
            {
                $this->base_message = __('composers_bots.messages.select_one_item');
                $this->buttons[][__('composers_bots.buttons.toturials.how_it_works_btn')] = ['callback_data' => 'how_it_works', 'condition' => BM::getCache('video_link', true)];
                $this->buttons[][__('composers_bots.buttons.return_base')] = ['callback_data' => 'gobase'];
            }

            function action()
            {
                switch (BM::$input_btn) {
                    case 'how_it_works':
                        BM::setCache('video_link', false);
                        $user = BM::getCache('user_data', null);
                        BM::operate(
                            'InState::' . BM::$curr_state,
                            'sendVideo',
                            [
                                __('composers_bots.messages.composer_robot_link', ['param' => isset($user) ? '?start=U-' . $user->ref_code : '']),
                                assetlink('defaults/videos/Dorehamiha_HowItWorks_' . LANGUAGE . '.mp4'),
                                null
                            ]
                        );
                        break;
                }
            }

        };

//==================================================================================================

        //region Register Event Steps

        static::$states['create_done_15'] = new class extends State
        {

            var $btn_type = 'inline';

            function start()
            {
                $this->base_message = __('composers_bots.messages.event.state_' . BM::getCache('newevent')->status);

                $this->buttons[][__('composers_bots.buttons.main_menu')] = ['callback_data' => 'gobase'];
            }

            function action()
            {
            }

        };

//==================================================================================================

        static::$states['create_check_14'] = new class extends State
        {

            var $btn_type = 'inline';

            function start()
            {
                $this->base_message =
                    __('composers_bots.messages.event.make_check')
                    . "\n\n"
                    . "\n" . ComposerBots::getPreview(BM::getFullCache('newevent'));

                $this->buttons[][__('composers_bots.buttons.event_accept')] = ['callback_data' => 'accept', 'condition' => BM::setCache('newevent', null, 'summary') !== 'nodata'];
                $this->buttons[][__('composers_bots.buttons.event_accept_test')] = ['callback_data' => 'testaccept', 'condition' => in_array(BM::getCache('user_data', null)->group_id, [1, 2]) && BM::setCache('newevent', null, 'summary') !== 'nodata'];
                $this->buttons[][__('composers_bots.buttons.event_draft')] = ['callback_data' => 'draft'];
                $this->buttons[][__('composers_bots.buttons.event_cancel')] = ['callback_data' => 'cancel'];
                BM::setCache('dont_clear_btn', false);
            }

            function action()
            {
                if (BM::$input_text === __('composers_bots.buttons.back')) {
                    return BM::backState();
                }

                switch (BM::$input_btn) {
                    case 'testaccept':
                        BM::setCache('newevent', !ComposerBots::SHOWTIME && in_array(BM::getCache('user_data', null)->group_id, [1, 2]) ? 'testaccepted' : 'requested', 'status');
                        break;
                    case 'accept':
                        BM::setCache('newevent', !ComposerBots::SHOWTIME && in_array(BM::getCache('user_data', null)->group_id, [1, 2, 6]) ? 'accepted' : 'requested', 'status');
                        break;
                    case 'draft':
                        BM::setCache('newevent', 'draft', 'status');
                        break;
                    case 'cancel':
                        BM::setCache('newevent', 'canceled', 'status');
                        break;
                }

                ComposerBots::createEvent();
                return BM::setState('create_done_15');

            }

        };

//==================================================================================================

        static::$states['create_pay_card_13_4'] = new class extends State
        {

            var $btn_type = 'inline';

            function start()
            {
                $this->base_message =
                    ComposerBots::get_progress(BM::getCache('curr_step', 1), ComposerBots::MAX_SUBMIT_STEPS)
                    . __('composers_bots.messages.event.send_field', ['field' => __('composers_bots.messages.fields.pay_card')]);

//                $this->buttons[] = [__('composers_bots.buttons.online_pay') => ['callback_data' => 'online']];
            }

            function action()
            {
                if (BM::$input_text === __('composers_bots.buttons.back')) {
                    return BM::setState('create_pay_type_13_2');
                }

                $pay_type = BM::getCache('newevent', null, 'pay_type');

                if ($pay_type != 'card' && (BM::getCache('Position', null) == 'EditEvent')) {
                    $cost = BM::getCache('newevent', null, 'cost');
                    $cat = BM::getCache('selected_event')->category;
                    $wage = ((!Setting::isSame('get_wage', '1')) || ($pay_type !== 'online')) ?
                        0 :
                        (
                        $cost <= floatval($cat->cost_divider) ?
                            floatval($cat->cost_fix) :
                            ceil(ceil(floatval($cat->cost_percent) * $cost / 100) / 100) * 100
                        );

                    BM::getCache('selected_event')->update(['cost' => $cost, 'wage' => $wage, 'pay_type' => $pay_type]);
                    BM::removeCache('curr_step');
                    BM::removeCache('newevent');
                    return BM::setState('one_event_manage_' . BM::getCache('events_mode'), __('composers_bots.messages.change_done'));
                } else {
                    BM::setCache('newevent', BM::$input_text, 'pay_card');
                    return BM::setState('create_check_14');
                }
            }

            function error_state()
            {
                return (BM::$input_type != 'callback_query' && BM::$input_text !== __('composers_bots.buttons.back'));
            }

            function error()
            {
                $this->addMessage(__('composers_bots.messages.just.select_item'));
            }
        };

//==================================================================================================

        static::$states['create_pay_type_13_2'] = new class extends State
        {

            var $btn_type = 'inline';

            function start()
            {
                $this->base_message =
                    ComposerBots::get_progress(BM::getCache('curr_step', 1), ComposerBots::MAX_SUBMIT_STEPS)
                    . __('composers_bots.messages.event.send_field', ['field' => __('composers_bots.messages.fields.pay_type')]);

                $this->buttons[] = [__('composers_bots.buttons.online_pay') => ['callback_data' => 'online']];
                $this->buttons[] = [__('composers_bots.buttons.card_pay') => ['callback_data' => 'card', 'condition' => in_array(BM::getCache('user_data')->group_id, [1, 2, 6])]];
                $this->buttons[] = [__('composers_bots.buttons.cash_pay') => ['callback_data' => 'cash']];
            }

            function action()
            {
                if (BM::$input_text === __('composers_bots.buttons.back')) {
                    BM::decCache('curr_step');
                    return BM::setState('create_cost_13');
                }

                $pay_type = BM::$input_btn;

                if ($pay_type != 'card' && (BM::getCache('Position', null) == 'EditEvent')) {
                    $cost = BM::getCache('newevent', null, 'cost');
                    $cat = BM::getCache('selected_event')->category;
                    $wage = ((!Setting::isSame('get_wage', '1')) || ($pay_type !== 'online')) ?
                        0 :
                        (
                        $cost <= floatval($cat->cost_divider) ?
                            floatval($cat->cost_fix) :
                            ceil(ceil(floatval($cat->cost_percent) * $cost / 100) / 100) * 100
                        );

                    BM::getCache('selected_event')->update(['cost' => $cost, 'wage' => $wage, 'pay_type' => $pay_type]);
                    BM::removeCache('curr_step');
                    BM::removeCache('newevent');
                    return BM::setState('one_event_manage_' . BM::getCache('events_mode'), __('composers_bots.messages.change_done'));
                } else {
                    BM::setCache('newevent', $pay_type, 'pay_type');
                    return BM::setState(($pay_type == 'card') ? 'create_pay_card_13_4' : 'create_check_14');
                }
            }

            function error_state()
            {
                return (BM::$input_type != 'callback_query' && BM::$input_text !== __('composers_bots.buttons.back'));
            }

            function error()
            {
                $this->addMessage(__('composers_bots.messages.just.select_item'));
            }
        };

//==================================================================================================

        static::$states['create_cost_13'] = new class extends State
        {

            var $btn_type = 'inline';

            function start()
            {
                $this->base_message =
                    ComposerBots::get_progress(BM::getCache('curr_step', 1), ComposerBots::MAX_SUBMIT_STEPS)
                    . __('composers_bots.messages.event.send_field', ['field' => __('composers_bots.messages.fields.cost')])
                    . "\n"
                    . __('composers_bots.messages.event.or_select_btns');

                $this->buttons[] =
                    [
                        __('composers_bots.buttons.dongi') => ['callback_data' => 'dongi'],
                        __('composers_bots.buttons.free')  => ['callback_data' => 'free'],
                    ];
            }

            function action()
            {
                if ((BM::getCache('Position', null) == 'EditEvent')) {
                    if (BM::$input_text === __('composers_bots.buttons.back')) {
                        return BM::backState();
                    }
                } else {
                    if (BM::$input_text === __('composers_bots.buttons.back')) {
                        BM::decCache('curr_step');
                        return BM::setState('create_capacity_max_12');
                    }
                }

                $pay_type = null;
                $cost = intval(toAscii(BM::$input_text));
                switch (BM::$input_btn) {
                    case 'free':
                        $cost = 0;
                        $pay_type = 'free';
                        break;
                    case 'dongi':
                        $cost = 0;
                        $pay_type = 'dongi';
                        break;
                }

                if (!is_null($pay_type)) {
                    BM::incCache('curr_step');
                }

                BM::setCache('newevent', $cost, 'cost');
                BM::setCache('newevent', $pay_type, 'pay_type');
                if (is_null($pay_type)) return BM::setState('create_pay_type_13_2');

                if ((BM::getCache('Position', null) == 'EditEvent')) {
                    BM::getCache('selected_event')->update(['cost' => 0, 'wage' => 0, 'pay_type' => $pay_type]);
                    BM::removeCache('curr_step');
                    BM::removeCache('newevent');
                    return BM::setState('one_event_manage_' . BM::getCache('events_mode'), __('composers_bots.messages.change_done'));
                } else return BM::setState('create_check_14');
            }

        };

//==================================================================================================

        static::$states['create_capacity_max_12'] = new class extends State
        {

            var $btn_type = 'inline';

            function start()
            {
                $this->base_message =
                    ComposerBots::get_progress(BM::getCache('curr_step', 1), ComposerBots::MAX_SUBMIT_STEPS)
                    . __('composers_bots.messages.event.send_field', ['field' => __('composers_bots.messages.fields.capacity_max')])
                    . "\n"
                    . __('composers_bots.messages.event.or_select_btn');

                $this->buttons[][__('composers_bots.buttons.same_as_min', ['value' => BM::getCache('newevent', 0, 'capacity_min')])] = ['callback_data' => 'same_as_min', 'condition' => BM::getCache('newevent', 0, 'capacity_min') > 0];
                $this->buttons[][__('composers_bots.buttons.no_max_cap')] = ['callback_data' => 'no_max_cap'];
            }

            function action()
            {
                if (BM::$input_text === __('composers_bots.buttons.back')) {
                    BM::decCache('curr_step');
                    return BM::setState('create_capacity_min_11');
                }

                $max = intval(toAscii(BM::$input_text));

                switch (BM::$input_btn) {
                    case 'no_max_cap':
                        $max = 0;
                        break;
                    case 'same_as_min':
                        $max = BM::getCache('newevent', 0, 'capacity_min');
                        break;
                }

                if ((BM::getCache('Position', null) == 'EditEvent')) {
                    BM::getCache('selected_event')->update(['capacity' => [
                        'min'   => BM::getCache('newevent', null, 'capacity_min'),
                        'max'   => $max,
                        'extra' => BM::getCache('newevent', null, 'capacity_min')
                    ]
                    ]);
                    BM::removeCache('curr_step');
                    BM::removeCache('newevent');
                    return BM::setState('one_event_manage_' . BM::getCache('events_mode'), __('composers_bots.messages.change_done'));
                } else {
                    BM::incCache('curr_step');
                    BM::setCache('newevent', $max, 'capacity_max');
                    return BM::setState('create_cost_13');
                }
            }

            function error_state()
            {
                return (
                    (intval(toAscii(BM::$input_text)) > 0)
                    &&
                    (intval(toAscii(BM::$input_text)) < BM::getCache('newevent', 0, 'capacity_min'))
                );
            }

            function error()
            {
                $this->addMessage(__('composers_bots.messages.just.currect_capacity'));
            }

        };

//==================================================================================================

        static::$states['create_capacity_min_11'] = new class extends State
        {

            var $btn_type = 'inline';

            function start()
            {
                $this->base_message =
                    ComposerBots::get_progress(BM::getCache('curr_step', 1), ComposerBots::MAX_SUBMIT_STEPS)
                    . __('composers_bots.messages.event.send_field', ['field' => __('composers_bots.messages.fields.capacity_min')])
                    . "\n"
                    . __('composers_bots.messages.event.or_select_btn');

                $this->buttons[][__('composers_bots.buttons.no_min_cap')] = ['callback_data' => 'no_min_cap'];
            }

            function action()
            {
                if ((BM::getCache('Position', null) == 'EditEvent')) {
                    if (BM::$input_text === __('composers_bots.buttons.back')) {
                        return BM::backState();
                    }
                } else {
                    if (BM::$input_text === __('composers_bots.buttons.back')) {
                        BM::decCache('curr_step');
                        return BM::setState('create_address_10');
                    }
                }

                $min = (BM::$input_btn == __('composers_bots.buttons.no_min_cap')) ? 0 : intval(toAscii(BM::$input_text));

                BM::incCache('curr_step');

                BM::setCache('newevent', $min, 'capacity_min');
                return BM::setState('create_capacity_max_12');
            }

        };

//==================================================================================================

        static::$states['create_address_title_10_5'] = new class extends State
        {

            var $btn_type = 'inline';

            function start()
            {
                $this->base_message =
                    __('composers_bots.messages.event.just_auto_eventor') . "\n"
                    . __('composers_bots.messages.event.send_field', ['field' => __('composers_bots.messages.fields.address_title')]);

                $this->buttons[][__('composers_bots.buttons.no_address_title')] = ['callback_data' => 'nodata'];
            }

            function action()
            {
                if ((BM::getCache('Position', null) == 'EditEvent')) {
                    if (BM::$input_text === __('composers_bots.buttons.back')) {
                        return BM::backState();
                    }
                    BM::getCache('selected_event')->base_location->update(['title' => [LANGUAGE => trim(BM::$input_text)]]);
                    return BM::backState(__('composers_bots.messages.change_done'));
                } else {
                    if (BM::$input_text === __('composers_bots.buttons.back')) {
                        return BM::setState('create_address_10');
                    }

                    BM::incCache('curr_step');

                    BM::setCache('newevent', (BM::$input_btn == 'nodata') ? null : trim(BM::$input_text), 'address_title');
                    return BM::setState('create_capacity_min_11');
                }
            }

        };

//==================================================================================================

        static::$states['create_address_10'] = new class extends State
        {
            var $btn_type = 'inline';

            function start()
            {
                $this->base_message =
                    ComposerBots::get_progress(BM::getCache('curr_step', 1), ComposerBots::MAX_SUBMIT_STEPS)
                    . __('composers_bots.messages.event.send_field', ['field' => __('composers_bots.messages.fields.address')]);
            }

            function action()
            {
                if ((BM::getCache('Position', null) == 'EditEvent')) {
                    if (BM::$input_text === __('composers_bots.buttons.back')) {
                        return BM::backState();
                    }

                    BM::getCache('selected_event')->base_location->update(['geocode' => trim(BM::$input_text)]);
                    return BM::backState(__('composers_bots.messages.change_done'));
                } else {
                    if (BM::$input_text === __('composers_bots.buttons.back')) {
                        if (!in_array(BM::getCache('user_data')->group_id, [1, 2, 6])) BM::decCache('curr_step');
                        return BM::setState('create_description_9');
                    }

                    if (!in_array(BM::getCache('user_data')->group_id, [1, 2, 6])) BM::incCache('curr_step');

                    BM::setCache('newevent', trim(BM::$input_text), 'address');
                    return (!ComposerBots::SHOWTIME && in_array(BM::getCache('user_data')->group_id, [1, 2, 6]) ?
                        BM::setState('create_address_title_10_5')
                        : BM::setState('create_capacity_min_11'));
                }
            }

        };

//==================================================================================================

        static::$states['create_summary_9_5'] = new class extends State
        {

            var $btn_type = 'inline';

            function start()
            {
                $this->base_message =
                    __('composers_bots.messages.event.just_auto_eventor') . "\n"
                    . __('composers_bots.messages.event.send_field', ['field' => __('composers_bots.messages.fields.summary')]);

                $this->buttons[][__('composers_bots.buttons.no_summary')] = ['callback_data' => 'nodata'];
            }

            function action()
            {
                if ((BM::getCache('Position', null) == 'EditEvent')) {
                    if (BM::$input_text === __('composers_bots.buttons.back')) {
                        return BM::backState();
                    }
                    BM::getCache('selected_event')->update(['summary' => (BM::$input_btn == 'nodata') ? 'nodata' : trim(BM::$input_text)]);
                    return BM::backState(__('composers_bots.messages.change_done'));
                } else {
                    if (BM::$input_text === __('composers_bots.buttons.back')) {
                        return BM::setState('create_description_9');
                    }

                    BM::incCache('curr_step');

                    BM::setCache('newevent', (BM::$input_btn == 'nodata') ? 'nodata' : trim(BM::$input_text), 'summary');
                    return BM::setState('create_address_10');
                }
            }

        };

//==================================================================================================

        static::$states['create_description_9'] = new class extends State
        {

            var $btn_type = 'inline';

            function start()
            {
                $this->base_message =
                    ComposerBots::get_progress(BM::getCache('curr_step', 1), ComposerBots::MAX_SUBMIT_STEPS)
                    . __('composers_bots.messages.event.send_field', ['field' => __('composers_bots.messages.fields.description')]);
            }

            function action()
            {
                if ((BM::getCache('Position', null) == 'EditEvent')) {
                    if (BM::$input_text === __('composers_bots.buttons.back')) {
                        return BM::backState();
                    }
                    BM::getCache('selected_event')->update(['description' => trim(BM::$input_text)]);
                    return BM::backState(__('composers_bots.messages.change_done'));
                } else {
                    if (BM::$input_text === __('composers_bots.buttons.back')) {
                        BM::decCache('curr_step');
                        return BM::setState('create_minimals_8');
                    }

                    if (!in_array(BM::getCache('user_data')->group_id, [1, 2, 6])) BM::incCache('curr_step');

                    BM::setCache('newevent', trim(BM::$input_text), 'description');
                    return (!ComposerBots::SHOWTIME && in_array(BM::getCache('user_data')->group_id, [1, 2, 6]) ?
                        BM::setState('create_summary_9_5')
                        : BM::setState('create_address_10'));
                }
            }

        };

//==================================================================================================

        static::$states['create_minimals_8'] = new class extends State
        {

            var $btn_type = 'inline';

            function start()
            {
                $this->base_message =
                    ComposerBots::get_progress(BM::getCache('curr_step', 1), ComposerBots::MAX_SUBMIT_STEPS)
                    . __('composers_bots.messages.event.send_field', ['field' => __('composers_bots.messages.fields.minimals')]);

                $this->base_message .= "\n" . __('composers_bots.messages.event.or_select_btn');
                $this->buttons[][__('composers_bots.buttons.nothing')] = ['callback_data' => 'nothing'];
            }

            function action()
            {
                if ((BM::getCache('Position', null) == 'EditEvent')) {
                    if (BM::$input_text === __('composers_bots.buttons.back')) {
                        return BM::backState();
                    }
                    BM::getCache('selected_event')->update(['minimals' => trim(BM::$input_text)]);
                    return BM::backState(__('composers_bots.messages.change_done'));
                } else {
                    if (BM::$input_text === __('composers_bots.buttons.back')) {
                        BM::decCache('curr_step');
                        return BM::setState('create_deadline_7');
                    }

                    BM::incCache('curr_step');

                    if (BM::$input_btn == 'nothing') {
                        BM::setCache('newevent', null, 'minimals');
                    } else {
                        BM::setCache('newevent', trim(BM::$input_text), 'minimals');
                    }

                    return BM::setState('create_description_9');
                }
            }

        };

//==================================================================================================

        static::$states['create_deadline_7'] = new class extends State
        {

            var $btn_type = 'inline';

            function start()
            {
                $this->base_message =
                    ComposerBots::get_progress(BM::getCache('curr_step', 1), ComposerBots::MAX_SUBMIT_STEPS)
                    . __('composers_bots.messages.event.send_field', ['field' => __('composers_bots.messages.fields.deadline')]);

                $this->buttons[][__('composers_bots.buttons.as_startdate')] = ['callback_data' => 'equal'];

                $st_date = BM::getCache('newevent', null, 'startdate');
                $st_date['m'] = $st_date['M'];
                $st_date['d'] = $st_date['D'];
                $st_date['i'] = $st_date['I'];

                $date = chShamsi($st_date, 'Y|m|d|H|i', $ch = "-1 Hours");
                $this->buttons[][__('composers_bots.buttons.1hour_before')] = ['callback_data' => '1hour', 'condition' => compareDates($date, null, 'Y|m|d|w|H|i') < 1];

                $date = chShamsi($st_date, 'Y|m|d|H|i', $ch = "-2 Hours");
                $this->buttons[][__('composers_bots.buttons.2hours_before')] = ['callback_data' => '2hours', 'condition' => compareDates($date, null, 'Y|m|d|w|H|i') < 1];

                $date = chShamsi($st_date, 'Y|m|d|H|i', $ch = "-12 Hours");
                $this->buttons[][__('composers_bots.buttons.12hours_before')] = ['callback_data' => '12hours', 'condition' => compareDates($date, null, 'Y|m|d|w|H|i') < 1];

                $date = chShamsi($st_date, 'Y|m|d|H|i', $ch = "-1 Days");
                $this->buttons[][__('composers_bots.buttons.1day_before')] = ['callback_data' => '1day', 'condition' => compareDates($date, null, 'Y|m|d|w|H|i') < 1];

                $date = chShamsi($st_date, 'Y|m|d|H|i', $ch = "-2 Days");
                $this->buttons[][__('composers_bots.buttons.2days_before')] = ['callback_data' => '2days', 'condition' => compareDates($date, null, 'Y|m|d|w|H|i') < 1];

                $date = chShamsi($st_date, 'Y|m|d|H|i', $ch = "-1 Weeks");
                $this->buttons[][__('composers_bots.buttons.1week_before')] = ['callback_data' => '1week', 'condition' => compareDates($date, null, 'Y|m|d|w|H|i') < 1];

                $date = chShamsi($st_date, 'Y|m|d|H|i', $ch = "-2 Weeks");
                $this->buttons[][__('composers_bots.buttons.2weeks_before')] = ['callback_data' => '2weeks', 'condition' => compareDates($date, null, 'Y|m|d|w|H|i') < 1];

                $date = chShamsi($st_date, 'Y|m|d|H|i', $ch = "-1 Months");
                $this->buttons[][__('composers_bots.buttons.1month_before')] = ['callback_data' => '1month', 'condition' => compareDates($date, null, 'Y|m|d|w|H|i') < 1];

                $date = chShamsi($st_date, 'Y|m|d|H|i', $ch = "-2 Months");
                $this->buttons[][__('composers_bots.buttons.2months_before')] = ['callback_data' => '2months', 'condition' => compareDates($date, null, 'Y|m|d|w|H|i') < 1];

                $date = chShamsi($st_date, 'Y|m|d|H|i', $ch = "-6 Months");
                $this->buttons[][__('composers_bots.buttons.6months_before')] = ['callback_data' => '6months', 'condition' => compareDates($date, null, 'Y|m|d|w|H|i') < 1];
//                $this->buttons[][__('composers_bots.buttons.exact')] = ['callback_data'=>'exact'];
            }

            function action()
            {
                if (BM::$input_text === __('composers_bots.buttons.back')) {
                    BM::decCache('curr_step');
                    return BM::setState('create_enddate_6');
                }

                $date = BM::getCache('newevent', null, 'startdate');
                $date['m'] = $date['M'];
                $date['d'] = $date['D'];
                $date['i'] = $date['I'];

                switch (BM::$input_btn) {
                    case 'equal':
                        $date = chShamsi($date, 'Y|m|d|H|i');
                        break;
                    case '1hour':
                        $date = chShamsi($date, 'Y|m|d|H|i', $ch = "-1 Hours");
                        break;
                    case '2hours':
                        $date = chShamsi($date, 'Y|m|d|H|i', $ch = "-2 Hours");
                        break;
                    case '12hours':
                        $date = chShamsi($date, 'Y|m|d|H|i', $ch = "-12 Hours");
                        break;
                    case '1day':
                        $date = chShamsi($date, 'Y|m|d|H|i', $ch = "-1 Days");
                        break;
                    case '2days':
                        $date = chShamsi($date, 'Y|m|d|H|i', $ch = "-2 Days");
                        break;
                    case '1week':
                        $date = chShamsi($date, 'Y|m|d|H|i', $ch = "-1 Weeks");
                        break;
                    case '2weeks':
                        $date = chShamsi($date, 'Y|m|d|H|i', $ch = "-2 Weeks");
                        break;
                    case '1month':
                        $date = chShamsi($date, 'Y|m|d|H|i', $ch = "-1 Months");
                        break;
                    case '2months':
                        $date = chShamsi($date, 'Y|m|d|H|i', $ch = "-2 Months");
                        break;
                    case '6months':
                        $date = chShamsi($date, 'Y|m|d|H|i', $ch = "-6 Months");
                        break;
                }
                $date = dateStrToArray($date, 'Y|M|D|H|I');
                if (!is_null(BM::getCache('Position', null))) {
                    switch (BM::getCache('Position', null)) {
                        case 'CopyToNew':
                            BM::getCache('copied_event')->update(['start_date' => dateArrayToStr(BM::getCache('newevent', null, 'startdate'), 'Y-M-D H:I')]);
                            BM::getCache('copied_event')->update(['end_date' => dateArrayToStr(BM::getCache('newevent', null, 'enddate'), 'Y-M-D H:I')]);
                            BM::getCache('copied_event')->update(['register_deadline' => dateArrayToStr($date, 'Y-M-D H:I')]);
                            BM::removeCache('newevent');
                            BM::removeCache('curr_step');
                            return BM::setState('wellcome', __('composers_bots.messages.copy_done'));
                            break;
                        case 'EditEvent':
                            BM::getCache('selected_event')->update(['start_date' => dateArrayToStr(BM::getCache('newevent', null, 'startdate'), 'Y-M-D H:I')]);
                            BM::getCache('selected_event')->update(['end_date' => dateArrayToStr(BM::getCache('newevent', null, 'enddate'), 'Y-M-D H:I')]);
                            BM::getCache('selected_event')->update(['register_deadline' => dateArrayToStr($date, 'Y-M-D H:I')]);
                            BM::removeCache('newevent');
                            BM::removeCache('curr_step');
                            return BM::setState('one_event_manage_' . BM::getCache('events_mode'), __('composers_bots.messages.change_done'));
                            break;
                    }
                } else {
                    BM::setCache('newevent', $date, 'deadline');

                    BM::incCache('curr_step');

                    return BM::setState('create_minimals_8');
                }
            }

            function error_state()
            {
                return (BM::$input_type != 'callback_query' && BM::$input_text !== __('composers_bots.buttons.back'));
            }

            function error()
            {
                $this->addMessage(__('composers_bots.messages.just.select_item'));
            }

        };

//==================================================================================================

        static::$states['create_enddate_6'] = new class extends State
        {

            var $btn_type = 'inline';

            function start()
            {
                $this->base_message =
                    ComposerBots::get_progress(BM::getCache('curr_step', 1), ComposerBots::MAX_SUBMIT_STEPS)
                    . __('composers_bots.messages.event.send_field', ['field' => __('composers_bots.messages.fields.enddate')]);

                $startdate = BM::getCache('newevent', null, 'startdate');

                $arr = ComposerBots::operate(
                    'InState::' . BM::$curr_state,
                    'dateTimeStream', [
                    BM::getCache('date', 'Day', 'mode'),
                    BM::getCache('date', implode('|', $startdate), 'base'),
                    BM::getCache('date', implode('|', $startdate), 'from'),
                    BM::getCache('date', null, 'end'),
                    BM::getCache('date', 1, 'page'),
                    BM::getCache('date', 3, 'perpage')
                ]);

                $this->buttons = array_merge($arr['items'], $arr['navs']);
                BM::setCache('date', $arr['page'], 'page');
            }

            function action()
            {
                $this->update = true;

                if (BM::$input_text === __('composers_bots.buttons.back')) {
                    BM::decCache('curr_step');
                    BM::removeCache('date');
                    return BM::setState('create_startdate_5');
                }

                switch (BM::$input_btn) {
                    case 'prev_page':
                        return BM::decCache('date', 'page');
                        break;
                    case 'next_page':
                        return BM::incCache('date', 'page');
                        break;
                    case 'select_mon':
                        BM::setCache('date', 1, 'page');
                        BM::setCache('date', null, 'base');
                        BM::setCache('date', 3, 'perpage');
                        return BM::setCache('date', 'MonYear', 'mode');
                        break;
                    case 'select_day':
                        BM::setCache('date', 1, 'page');
                        BM::setCache('date', 3, 'perpage');
                        return BM::setCache('date', 'Day', 'mode');
                        break;
                    case 'first_page':
                        return BM::setCache('date', 1, 'page');
                        break;
                    case 'last_page':
                        return BM::setCache('date', 'last', 'page');
                        break;
                }

                $match = null;
                if (preg_match("#^Day\|(.*)$#i", BM::$input_btn, $match) !== 0) {
                    $date = null;
                    $tmp = explode('|', $match[1]);
                    foreach ($tmp as $t) $date[substr($t, 0, 1)] = substr($t, 1);
                    BM::setCache('date', implode('|', $date) . '|0', 'base');
                    BM::setCache('date', 'HourMin', 'mode');
                    BM::setCache('date', 6, 'perpage');
                    BM::setCache('date', 'last', 'page');
                } elseif (preg_match("#^next_mon\|(.*)$#i", BM::$input_btn, $match) !== 0
                    || preg_match("#^prev_mon\|(.*)$#i", BM::$input_btn, $match) !== 0
                    || preg_match("#^MonYear\|(.*)$#i", BM::$input_btn, $match) !== 0) {
                    $date = null;
                    $tmp = explode('|', $match[1]);
                    foreach ($tmp as $t) $date[substr($t, 0, 1)] = substr($t, 1);
                    BM::setCache('date', implode('|', $date) . '|1|9|0', 'base');
                    BM::setCache('date', 'Day', 'mode');
                    BM::setCache('date', 2, 'perpage');
                    BM::setCache('date', 1, 'page');
                } elseif (preg_match("#^HourMin\|(.*)$#i", BM::$input_btn, $match) !== 0) {
                    $date = null;
                    $tmp = explode('|', $match[1]);
                    foreach ($tmp as $t) $date[substr($t, 0, 1)] = intval(substr($t, 1));

                    if (DEBUG_MODE && (empty(BM::DEBUG_CODES) || in_array(215, BM::DEBUG_CODES))) {
                        mylog('info', 215, $date);
                    }

                    BM::removeCache('date');

                    BM::incCache('curr_step');

                    BM::setCache('newevent', $date, 'enddate');
                    return BM::setState('create_deadline_7');
                }

            }

            function error_state()
            {
                return (BM::$input_type != 'callback_query' && BM::$input_text !== __('composers_bots.buttons.back'));
            }

            function error()
            {
                $this->addMessage(__('composers_bots.messages.just.select_item'));
            }
        };

//==================================================================================================

        static::$states['create_startdate_5'] = new class extends State
        {

            var $btn_type = 'inline';

            function start()
            {
                $this->base_message =
                    ComposerBots::get_progress(BM::getCache('curr_step', 1), ComposerBots::MAX_SUBMIT_STEPS)
                    . __('composers_bots.messages.event.send_field', ['field' => __('composers_bots.messages.fields.startdate')]);

                $arr = ComposerBots::operate(
                    'InState::' . BM::$curr_state,
                    'dateTimeStream', [
                    BM::getCache('date', 'Day', 'mode'),
                    BM::getCache('date', null, 'base'),
                    BM::getCache('date', null, 'from'),
                    BM::getCache('date', null, 'end'),
                    BM::getCache('date', 1, 'page'),
                    BM::getCache('date', 3, 'perpage')
                ]);
                $this->buttons = array_merge($arr['items'], $arr['navs']);
                BM::setCache('date', $arr['page'], 'page');

            }

            function action()
            {
                $this->update = true;

                if (!is_null(BM::getCache('Position', null))) {
                    if (BM::$input_text === __('composers_bots.buttons.back')) {
                        return BM::backState();
                    }
                } elseif (BM::$input_text === __('composers_bots.buttons.back')) {
                    BM::decCache('curr_step');
                    BM::removeCache('date');
                    return BM::setState('create_gender_4');
                }

                switch (BM::$input_btn) {
                    case 'prev_page':
                        return BM::decCache('date', 'page');
                        break;
                    case 'next_page':
                        return BM::incCache('date', 'page');
                        break;
                    case 'select_mon':
                        BM::setCache('date', 1, 'page');
                        BM::setCache('date', null, 'base');
                        BM::setCache('date', 3, 'perpage');
                        return BM::setCache('date', 'MonYear', 'mode');
                        break;
                    case 'select_day':
                        BM::setCache('date', 1, 'page');
                        BM::setCache('date', 3, 'perpage');
                        return BM::setCache('date', 'Day', 'mode');
                        break;
                    case 'first_page':
                        return BM::setCache('date', 1, 'page');
                        break;
                    case 'last_page':
                        return BM::setCache('date', 'last', 'page');
                        break;
                }

                $match = null;
                if (preg_match("#^Day\|(.*)$#i", BM::$input_btn, $match) !== 0) {
                    $date = null;
                    $tmp = explode('|', $match[1]);
                    foreach ($tmp as $t) $date[substr($t, 0, 1)] = substr($t, 1);
                    BM::setCache('date', implode('|', $date) . '|0', 'base');
                    BM::setCache('date', 'HourMin', 'mode');
                    BM::setCache('date', 6, 'perpage');
                    BM::setCache('date', 2, 'page');
                } elseif (preg_match("#^next_mon\|(.*)$#i", BM::$input_btn, $match) !== 0
                    || preg_match("#^prev_mon\|(.*)$#i", BM::$input_btn, $match) !== 0
                    || preg_match("#^MonYear\|(.*)$#i", BM::$input_btn, $match) !== 0) {
                    $date = null;
                    $tmp = explode('|', $match[1]);
                    foreach ($tmp as $t) $date[substr($t, 0, 1)] = substr($t, 1);
                    BM::setCache('date', implode('|', $date) . '|1|9|0', 'base');
                    BM::setCache('date', 'Day', 'mode');
                    BM::setCache('date', 2, 'perpage');
                    BM::setCache('date', 1, 'page');
                } elseif (preg_match("#^HourMin\|(.*)$#i", BM::$input_btn, $match) !== 0) {
                    $date = null;
                    $tmp = explode('|', $match[1]);
                    foreach ($tmp as $t) $date[substr($t, 0, 1)] = intval(substr($t, 1));

                    if (DEBUG_MODE && (empty(BM::DEBUG_CODES) || in_array(215, BM::DEBUG_CODES))) {
                        mylog('info', 215, $date);
                    }
                    BM::removeCache('date');

                    BM::incCache('curr_step');

                    BM::setCache('newevent', $date, 'startdate');
                    return BM::setState('create_enddate_6');
                }

            }

            function error_state()
            {
                return (BM::$input_type != 'callback_query' && BM::$input_text !== __('composers_bots.buttons.back'));
            }

            function error()
            {
                $this->addMessage(__('composers_bots.messages.just.select_item'));
            }
        };

//==================================================================================================

        static::$states['create_gender_4'] = new class extends State
        {

            var $btn_type = 'inline';

            function start()
            {
                $this->base_message =
                    ComposerBots::get_progress(BM::getCache('curr_step', 1), ComposerBots::MAX_SUBMIT_STEPS)
                    . __('composers_bots.messages.event.send_field', ['field' => __('composers_bots.messages.fields.gender')]);

                $this->buttons[][__('composers_bots.buttons.private')] = ['callback_data' => 'private', 'condition' => in_array(BM::getCache('user_data')->group_id, [1, 2, 6])];
                $this->buttons[][__('composers_bots.buttons.male')] = ['callback_data' => 'male'];
                $this->buttons[][__('composers_bots.buttons.female')] = ['callback_data' => 'female'];
                $this->buttons[][__('composers_bots.buttons.both')] = ['callback_data' => 'both'];
                //BM::setCache('dont_clear_btn',false);//in last level, to kill footer btns
            }

            function action()
            {
                if ((BM::getCache('Position', null) == 'EditEvent')) {
                    if (BM::$input_text === __('composers_bots.buttons.back')) {
                        return BM::backState();
                    }
                    BM::getCache('selected_event')->update(['gender' => BM::$input_btn]);
                    return BM::backState(__('composers_bots.messages.change_done'));
                } else {
                    if (BM::$input_text === __('composers_bots.buttons.back')) {
                        BM::decCache('curr_step');
                        return BM::setState('create_title_3');
                    }

                    BM::incCache('curr_step');

                    BM::setCache('newevent', BM::$input_btn, 'gender');
                    return BM::setState('create_startdate_5');
                }
            }

            function error_state()
            {
                return (BM::$input_type != 'callback_query' && BM::$input_text !== __('composers_bots.buttons.back'));
            }

            function error()
            {
                $this->addMessage(__('composers_bots.messages.just.select_item'));
            }

        };

//==================================================================================================

        static::$states['create_title_3'] = new class extends State
        {

            var $btn_type = 'footer_sticky';

            function start()
            {
                $this->base_message =
                    ComposerBots::get_progress(BM::getCache('curr_step', 1), ComposerBots::MAX_SUBMIT_STEPS)
                    . __('composers_bots.messages.event.send_field', ['field' => __('composers_bots.messages.fields.title')]);

/////                $this->buttons[][__('composers_bots.buttons.asprev_event')] = ['callback_data' => 'btn_asprev_event','condition'=>(BM::getCache('Position',null) == 'CopyToNew')];
                $this->buttons[][__('composers_bots.buttons.back')] = null;
                BM::setCache('dont_clear_btn', true);
            }

            function action()
            {
                if (!is_null(BM::getCache('Position', null))) {
                    if (BM::$input_text === __('composers_bots.buttons.back')) {
                        return BM::backState();
                    }

                    if (BM::$input_btn === 'btn_asprev_event') {
                        return BM::setState('create_startdate_5');
                    }

                    switch (BM::getCache('Position', null)) {
                        case 'CopyToNew':
                            BM::getCache('copied_event')->update(['title' => [LANGUAGE => trim(BM::$input_text)]]);
                            return BM::setState('create_startdate_5');
                            break;
                        case 'EditEvent':
                            BM::getCache('selected_event')->update(['title' => [LANGUAGE => trim(BM::$input_text)]]);
                            return BM::backState(__('composers_bots.messages.change_done'));
                            break;
                    }
                } else {
                    if (BM::$input_text === __('composers_bots.buttons.back')) {
                        BM::decCache('curr_step');
                        return BM::setState('create_theme_2');
                    }

                    BM::incCache('curr_step');

                    BM::setCache('newevent', trim(BM::$input_text), 'title');
                    return BM::setState('create_gender_4');
                }
            }

        };

//==================================================================================================

        static::$states['create_own_banner_2_5'] = new class extends State
        {

            var $btn_type = 'footer_sticky';

            function start()
            {
                $this->base_message = __('composers_bots.messages.own_banner');

                $this->buttons[][__('composers_bots.buttons.back')] = null;
                BM::setCache('dont_clear_btn', true);
            }

            function action()
            {
                if (BM::$input_text === __('composers_bots.buttons.back')) {
                    return BM::setState('create_theme_2');
                }

                $last = end(BM::$input['photo']);
                $image = BM::operate('Get Photo', 'getPhoto', [$last['file_id']]);

                if ($image['status'] == 200) {

                    if (!is_null(BM::getCache('Position', null))) {
                        BM::removeCache('curr_step');
                        BM::removeCache('newevent');
                        switch (BM::getCache('Position', null)) {
                            case 'CopyToNew':
                                BM::getCache('copied_event')->update(['banner_bg_type' => 'full', 'banner_bg_id' => 0, 'image_id' => $image['data']]);
                                return BM::setState('create_title_3');
                                break;
                            case 'EditEvent':
                                BM::getCache('selected_event')->update(['banner_bg_type' => 'full', 'banner_bg_id' => 0, 'image_id' => $image['data']]);
                                return BM::setState('one_event_manage_' . BM::getCache('events_mode'), __('composers_bots.messages.change_done'));
                                break;
                        }
                    } else {
                        BM::setCache('newevent', $image['data'], 'image');
                        BM::setCache('newevent', 'full', 'background_type');

                        BM::incCache('curr_step');

                        return BM::setState('create_title_3');
                    }
                }
            }

            function error_state()
            {
                return (BM::$input_type != 'callback_query' && BM::$input_subtype != 'photo');
            }

            function error()
            {
                $this->addMessage(__('composers_bots.messages.just.upload_image'));
            }

        };

//==================================================================================================

        static::$states['create_theme_2'] = new class extends State
        {

            var $btn_type = 'inline';

            function start()
            {
                BM::setCache('curr_step', 1);

                $cat = (!is_null(BM::getCache('Position', null))) ? BM::getCache('selected_event')->category : BM::getCache('newevent', null, 'category');

                $this->base_message =
                    ComposerBots::get_progress(BM::getCache('curr_step', 1), ComposerBots::MAX_SUBMIT_STEPS)
                    . __('composers_bots.messages.event.send_field', ['field' => __('composers_bots.messages.fields.bg')]);

                BM::operate(
                    'InState::' . BM::$curr_state,
                    'createBannerAlbum', [$cat->id, LANGUAGE]);

                $k = [];
                foreach (EventtCategoryBG::asArray($cat->id) as $theme) {
                    $k[$theme->title[LANGUAGE]] = ['callback_data' => 'bg_' . $theme->id];
                    if (count($k) >= 2) {
                        $this->buttons[] = $k;
                        $k = [];
                    }
                }
                if (count($k) > 0) {
                    $this->buttons[] = $k;
                }

                $this->buttons[][__('composers_bots.buttons.own_banner')] = ['callback_data' => 'btn_own_banner'];
                $this->buttons[][__('composers_bots.buttons.asprev_banner')] = ['callback_data' => 'btn_asprev_banner', 'condition' => (BM::getCache('Position', null) == 'CopyToNew')];
            }

            function action()
            {
                if (!is_null(BM::getCache('Position', null))) {
                    if (BM::$input_text === __('composers_bots.buttons.back')) {
                        return BM::backState();
                    }
                    if (BM::$input_btn === 'btn_asprev_banner') {
                        return BM::setState('create_title_3');
                    }
                } else {
                    if (BM::$input_text === __('composers_bots.buttons.back')) {
//                    BM::decCache('curr_step');
                        return BM::setState('create_cat_1');
                    }
                }

                if (BM::$input_btn === 'btn_own_banner') {
                    return BM::setState('create_own_banner_2_5');
                }

                $bgid = substr(BM::$input_btn, 3);
                $bg = EventtCategoryBG::find($bgid);

                if (empty($bg)) {
                    return;
                }

                if (!is_null(BM::getCache('Position', null))) {
                    switch (BM::getCache('Position', null)) {
                        case 'CopyToNew':
                            BM::getCache('copied_event')->update(['banner_bg_type' => 'automatic', 'banner_bg_id' => $bgid, 'image_id' => null]);
                            return BM::setState('create_title_3');
                            break;
                        case 'EditEvent':
                            BM::getCache('selected_event')->update(['banner_bg_type' => 'automatic', 'banner_bg_id' => $bgid, 'image_id' => null]);
                            return BM::setState('one_event_manage_' . BM::getCache('events_mode'), __('composers_bots.messages.change_done'));
                            break;
                    }
                } else {
                    BM::incCache('curr_step');
                    BM::setCache('newevent', $bg, 'background');
                    BM::setCache('newevent', 'automatic', 'background_type');
                    return BM::setState('create_title_3');
                }
            }

            function error_state()
            {
                return (BM::$input_type != 'callback_query' && BM::$input_text !== __('composers_bots.buttons.back'));
            }

            function error()
            {
                $this->addMessage(__('composers_bots.messages.just.select_item'));
            }
        };

//==================================================================================================

        static::$states['create_cat_1'] = new class extends State
        {

            var $btn_type = 'inline';

            function start()
            {
                $this->base_message = __('composers_bots.messages.event.set_cat');

                foreach (EventtCategory::asArray() as $cat) {
                    $this->buttons[][$cat['title'][LANGUAGE] . ' ' . $cat['emojy']] = ['callback_data' => 'type_' . $cat['id']];
                }
                $this->buttons[][__('composers_bots.buttons.cancel')] = ['callback_data' => 'cancel'];

                BM::setCache('dont_clear_btn', false);
            }

            function action()
            {
                if ((BM::getCache('Position', null) == 'EditEvent')) {
                    if (BM::$input_btn === 'cancel') {
                        return BM::backState();
                    }
                    BM::getCache('selected_event')->update(['category_id' => substr(BM::$input_btn, 5)]);
                    return BM::backState(__('composers_bots.messages.change_done'));
                } else {
                    if (BM::$input_btn === 'cancel') {
                        return BM::setState('wellcome');
                    }

                    $id = substr(BM::$input_btn, 5);
                    $cat = EventtCategory::find($id);

                    if (empty($cat)) {
                        return;
                    }

                    BM::setCache('newevent', $cat, 'category');
                    return BM::setState('create_theme_2');
                }
            }

            function error_state()
            {
                return (BM::$input_type != 'callback_query');
            }

            function error()
            {
                $this->addMessage(__('composers_bots.messages.just.select_item'));
            }
        };

        //endregion

//==================================================================================================

        static::$states['events_list'] = new class extends State
        {

            var $btn_type = 'inline';

            function start()
            {
                BM::setIfNotCache('page', 1);
                $items = ComposerBots::getItems(BM::getCache('events_mode'));
                $this->addMessage($items['message']);
                $this->buttons = $items['markups'];
            }

            function action()
            {
                switch (BM::$input_btn) {
                    case 'nextpage':
                        BM::setCache('page', min(BM::getCache('page', 1) + 1, BM::getCache('maxpages', 1)));
                        return;
                        break;
                    case 'prevpage':
                        BM::setCache('page', max(1, BM::getCache('page', 1) - 1));
                        return;
                        break;
                }

                $event = Eventt::find(trim(BM::$input_btn));

                if (empty($event)) {
                    return BM::backState();
                }

                BM::setCache('selected_event', $event);
                return BM::setState('one_event_manage_' . BM::getCache('events_mode'), BM::getCache('events_mode') == 'finished' ? ComposerBots::getSummary() : '');
            }

            function error_state()
            {
                return (BM::$input_type != 'callback_query');
            }

            function error()
            {
                $this->addMessage(__('composers_bots.messages.just.select_item'));
            }

        };

//==================================================================================================

        static::$states['one_event_manage_accepted'] = new class extends State
        {

            var $btn_type = 'inline';

            function start()
            {
                BM::removeCache('Position');

                $event = BM::getCache('selected_event');

                $this->base_message = ComposerBots::getSummary();
                $this->buttons[] = [
                    __('composers_bots.buttons.get_details') => ['callback_data' => 'get_details'],
                ];
                $this->buttons[] = [
                    __('composers_bots.buttons.get_banner') => ['callback_data' => 'get_banner', 'condition' => in_array($event->status, ['accepted', 'testaccepted', 'closed_list'])],
                ];
                $this->buttons[] = [
                    __('composers_bots.buttons.broadcastmsg') => ['callback_data' => 'broadcastmsg', 'condition' => in_array($event->status, ['accepted', 'testaccepted', 'closed_list'])],
                ];
                $this->buttons[] = [
                    __('composers_bots.buttons.users_list') => ['callback_data' => 'users_list', 'condition' => in_array($event->status, ['accepted', 'testaccepted', 'closed_list'])],
                ];
                $this->buttons[] = [
                    __('composers_bots.buttons.close_list') => ['callback_data' => 'close_list_status', 'condition' => in_array($event->status, ['accepted', 'testaccepted'])],
                ];
                $this->buttons[] = [
                    __('composers_bots.buttons.change_category') => ['callback_data' => 'ch_cat_1', 'condition' => in_array($event->status, ['draft'])],
                    __('composers_bots.buttons.change_title')    => ['callback_data' => 'ch_title_3', 'condition' => in_array($event->status, ['draft'])],
                ];

                $this->buttons[] = [
                    __('composers_bots.buttons.change_dates') => ['callback_data' => 'ch_dates'],//alert of accepted
                ];

                $this->buttons[] = [
                    __('composers_bots.buttons.change_minimals')    => ['callback_data' => 'ch_minimals_8'],
                    __('composers_bots.buttons.change_description') => ['callback_data' => 'ch_description_9', 'condition' => in_array($event->status, ['draft'])],
                ];
                $this->buttons[] = [
                    __('composers_bots.buttons.change_summary') => ['callback_data' => 'ch_summary_9_5', 'condition' => in_array(BM::getCache('user_data')->group_id, [1, 2, 6]) && in_array($event->status, ['draft'])],
                ];

                $this->buttons[] = [
                    __('composers_bots.buttons.change_address')       => ['callback_data' => 'ch_address_10'],//alert on accepted
                    __('composers_bots.buttons.change_address_title') => ['callback_data' => 'ch_address_title_10_5', 'condition' => in_array(BM::getCache('user_data')->group_id, [1, 2, 6])],//alert on ACCEPTED
                ];

                $this->buttons[] = [
                    __('composers_bots.buttons.change_capacities') => ['callback_data' => 'ch_capacities', 'condition' => !in_array($event->status, ['closed_list'])],
                ];

                $this->buttons[] = [
                    __('composers_bots.buttons.change_cost') => ['callback_data' => 'ch_costs', 'condition' => in_array($event->status, ['draft'])],
                ];

                $this->buttons[] = [
                    __('composers_bots.buttons.change_gender') => ['callback_data' => 'ch_gender_4', 'condition' => in_array($event->status, ['draft'])],
                    __('composers_bots.buttons.change_bg')     => ['callback_data' => 'ch_theme_2', 'condition' => in_array($event->status, ['draft'])],
                ];

                $this->buttons[] = [
                    __('composers_bots.buttons.open_list') => ['callback_data' => 'open_list_status', 'condition' => in_array($event->status, ['closed_list'])],
                ];

                $this->buttons[] = [
                    __('composers_bots.buttons.change_state_draft') => ['callback_data' => 'ch_state', 'condition' => in_array($event->status, ['draft'])],
                ];
                $this->buttons[][__('composers_bots.buttons.return_base')] = ['callback_data' => 'gobase'];
            }

            function action()
            {
                if (BM::$input_btn == 'back') return BM::setState('events_list');

                BM::setCache('Position', 'EditEvent');

                switch (BM::$input_btn) {
                    case 'get_details':
                        return $this->addMessage(ComposerBots::getDetails());
                        break;
                    case 'get_banner':
                        $req = BM::getCache('selected_event');
                        try {
                            BM::operate(
                                'Composer::Resend Poster',
                                'sendBanner', [
                                    $req->id,
                                    LANGUAGE,
                                    __('composers_bots.messages.photo_link', [
                                        'title'  => $req->title[LANGUAGE],
                                        'params' => $req->category->bot->username . '?start=M-' . $req->slogan
                                    ]),
                                ]
                            );
                        } catch (\Exception $e) {
                            getFormedError($e);
                        }
                        return;
                        break;
                    case 'broadcastmsg':
                        return BM::setState("create_broadcastmsg");
                        break;
                    case 'users_list':
                        return BM::setState("get_users_list");
                        break;
                    case 'ch_dates':
                        return BM::setState("create_startdate_5");
                        break;
                    case 'ch_capacities':
                        return BM::setState("create_capacity_min_11");
                        break;
                    case 'ch_costs':
                        return BM::setState("create_cost_13");
                        break;
                    case 'ch_state':
                        if (in_array(BM::getCache('user_data')->group_id, [1, 2, 6])) {
                            BM::getCache('selected_event')->update(['status' => 'accepted']);
                            try {
                                $req = BM::getCache('selected_event');
                                BM::operate(
                                    'Composer::Create Event',
                                    'sendBanner', [
                                        $req->id,
                                        LANGUAGE,
                                        __('composers_bots.messages.photo_link', [
                                            'title'  => $req->title[LANGUAGE],
                                            'params' => $req->category->bot->username . '?start=M-' . $req->slogan
                                        ]),
                                        false,
                                        [//forward to this chat id and composers channel
                                            BM::$chat_id
                                            , '-1001255760808'
                                        ]
                                    ]
                                    , null, null, '-1001327534278'
                                );
                            } catch (\Exception $e) {
                                getFormedError($e);
                            }
                            return BM::setState('wellcome', __('composers_bots.messages.event.state_accepted'));
                        } else {
                            BM::getCache('selected_event')->update(['status' => 'requested']);
                            return BM::setState('wellcome', __('composers_bots.messages.event.state_requested'));
                        }
                        break;
                    case 'close_list_status':
                        BM::getCache('selected_event')->update(['status' => 'closed_list']);
                        return $this->addMessage(__('composers_bots.messages.event.state_closed_list'));
                        break;
                    case 'open_list_status':
                        BM::getCache('selected_event')->update(['status' => 'accepted']);
                        return $this->addMessage(__('composers_bots.messages.event.state_opened_list'));
                        break;
                }

                $field = substr(BM::$input_btn, 3);
                return BM::setState("create_{$field}");
            }

            function error_state()
            {
                return (BM::$input_type != 'callback_query');
            }

            function error()
            {
                $this->addMessage(__('composers_bots.messages.just.select_item'));
            }

        };

//==================================================================================================

        static::$states['one_event_manage_draft'] = new class extends State
        {

            var $btn_type = 'inline';

            function start()
            {
                BM::removeCache('Position');

                $event = BM::getCache('selected_event');

                $this->base_message = ComposerBots::getSummary();
                $this->buttons[] = [
                    __('composers_bots.buttons.get_banner') => ['callback_data' => 'get_banner', 'condition' => in_array($event->status, ['accepted', 'testaccepted', 'closed_list'])],
                ];
                $this->buttons[] = [
                    __('composers_bots.buttons.broadcastmsg') => ['callback_data' => 'broadcastmsg', 'condition' => in_array($event->status, ['accepted', 'testaccepted', 'closed_list'])],
                ];
                $this->buttons[] = [
                    __('composers_bots.buttons.users_list') => ['callback_data' => 'users_list', 'condition' => in_array($event->status, ['accepted', 'testaccepted', 'closed_list'])],
                ];
                $this->buttons[] = [
                    __('composers_bots.buttons.close_list') => ['callback_data' => 'close_list_status', 'condition' => in_array($event->status, ['accepted', 'testaccepted'])],
                ];
                $this->buttons[] = [
                    __('composers_bots.buttons.change_category') => ['callback_data' => 'ch_cat_1', 'condition' => in_array($event->status, ['draft'])],
                    __('composers_bots.buttons.change_title')    => ['callback_data' => 'ch_title_3', 'condition' => in_array($event->status, ['draft'])],
                ];

                $this->buttons[] = [
                    __('composers_bots.buttons.change_dates') => ['callback_data' => 'ch_dates'],//alert of accepted
                ];

                $this->buttons[] = [
                    __('composers_bots.buttons.change_minimals')    => ['callback_data' => 'ch_minimals_8'],
                    __('composers_bots.buttons.change_description') => ['callback_data' => 'ch_description_9', 'condition' => in_array($event->status, ['draft'])],
                ];
                $this->buttons[] = [
                    __('composers_bots.buttons.change_summary') => ['callback_data' => 'ch_summary_9_5', 'condition' => in_array(BM::getCache('user_data')->group_id, [1, 2, 6]) && in_array($event->status, ['draft'])],
                ];

                $this->buttons[] = [
                    __('composers_bots.buttons.change_address')       => ['callback_data' => 'ch_address_10'],//alert on accepted
                    __('composers_bots.buttons.change_address_title') => ['callback_data' => 'ch_address_title_10_5', 'condition' => in_array(BM::getCache('user_data')->group_id, [1, 2, 6])],//alert on ACCEPTED
                ];

                $this->buttons[] = [
                    __('composers_bots.buttons.change_capacities') => ['callback_data' => 'ch_capacities', 'condition' => !in_array($event->status, ['closed_list'])],
                ];

                $this->buttons[] = [
                    __('composers_bots.buttons.change_cost') => ['callback_data' => 'ch_costs', 'condition' => in_array($event->status, ['draft'])],
                ];

                $this->buttons[] = [
                    __('composers_bots.buttons.change_gender') => ['callback_data' => 'ch_gender_4', 'condition' => in_array($event->status, ['draft'])],
                    __('composers_bots.buttons.change_bg')     => ['callback_data' => 'ch_theme_2', 'condition' => in_array($event->status, ['draft'])],
                ];

                $this->buttons[] = [
                    __('composers_bots.buttons.open_list') => ['callback_data' => 'open_list_status', 'condition' => in_array($event->status, ['closed_list'])],
                ];

                $this->buttons[] = [
                    __('composers_bots.buttons.change_state_draft') => ['callback_data' => 'ch_state', 'condition' => in_array($event->status, ['draft'])],
                ];
                $this->buttons[][__('composers_bots.buttons.return_base')] = ['callback_data' => 'gobase'];
            }

            function action()
            {
                if (BM::$input_btn == 'back') return BM::setState('events_list');

                BM::setCache('Position', 'EditEvent');

                switch (BM::$input_btn) {
                    case 'get_banner':
                        $req = BM::getCache('selected_event');
                        try {
                            BM::operate(
                                'Composer::Resend Poster',
                                'sendBanner', [
                                    $req->id,
                                    LANGUAGE,
                                    __('composers_bots.messages.photo_link', [
                                        'title'  => $req->title[LANGUAGE],
                                        'params' => $req->category->bot->username . '?start=M-' . $req->slogan
                                    ]),
                                ]
                            );
                        } catch (\Exception $e) {
                            getFormedError($e);
                        }
                        return;
                        break;
                    case 'broadcastmsg':
                        return BM::setState("create_broadcastmsg");
                        break;
                    case 'users_list':
                        return BM::setState("get_users_list");
                        break;
                    case 'ch_dates':
                        return BM::setState("create_startdate_5");
                        break;
                    case 'ch_capacities':
                        return BM::setState("create_capacity_min_11");
                        break;
                    case 'ch_costs':
                        return BM::setState("create_cost_13");
                        break;
                    case 'ch_state':
                        if (in_array(BM::getCache('user_data')->group_id, [1, 2, 6])) {
                            BM::getCache('selected_event')->update(['status' => 'accepted']);
                            try {
                                $req = BM::getCache('selected_event');
                                BM::operate(
                                    'Composer::Create Event',
                                    'sendBanner', [
                                        $req->id,
                                        LANGUAGE,
                                        __('composers_bots.messages.photo_link', [
                                            'title'  => $req->title[LANGUAGE],
                                            'params' => $req->category->bot->username . '?start=M-' . $req->slogan
                                        ]),
                                        false,
                                        [//forward to this chat id and composers channel
                                            BM::$chat_id
                                            , '-1001255760808'
                                        ]
                                    ]
                                    , null, null, '-1001327534278'
                                );
                            } catch (\Exception $e) {
                                getFormedError($e);
                            }
                            return BM::setState('wellcome', __('composers_bots.messages.event.state_accepted'));
                        } else {
                            BM::getCache('selected_event')->update(['status' => 'requested']);
                            return BM::setState('wellcome', __('composers_bots.messages.event.state_requested'));
                        }
                        break;
                    case 'close_list_status':
                        BM::getCache('selected_event')->update(['status' => 'closed_list']);
                        return $this->addMessage(__('composers_bots.messages.event.state_closed_list'));
                        break;
                    case 'open_list_status':
                        BM::getCache('selected_event')->update(['status' => 'accepted']);
                        return $this->addMessage(__('composers_bots.messages.event.state_opened_list'));
                        break;
                }

                $field = substr(BM::$input_btn, 3);
                return BM::setState("create_{$field}");
            }

            function error_state()
            {
                return (BM::$input_type != 'callback_query');
            }

            function error()
            {
                $this->addMessage(__('composers_bots.messages.just.select_item'));
            }

        };

//==================================================================================================

        static::$states['one_event_manage_live'] = new class extends State
        {

            var $btn_type = 'inline';

            function start()
            {
                $event = BM::getCache('selected_event');

                $this->base_message = ComposerBots::getSummary();
                $this->buttons[] = [
                    __('composers_bots.buttons.broadcastmsg') => ['callback_data' => 'broadcastmsg'],
                ];
                $this->buttons[] = [
                    __('composers_bots.buttons.users_list') => ['callback_data' => 'users_list'],
                ];
                $this->buttons[] = [
                    __('composers_bots.buttons.send_clip')  => ['callback_data' => 'send_clip'],
                    __('composers_bots.buttons.send_image') => ['callback_data' => 'send_image'],
                ];

                $this->buttons[] = [
                    __('composers_bots.buttons.send_itinary') => ['callback_data' => 'send_itinary'],
                ];

                $this->buttons[][__('composers_bots.buttons.return_base')] = ['callback_data' => 'gobase'];
            }

            function action()
            {
                if (BM::$input_btn == 'back') return BM::setState('events_list');

                $event = BM::getCache('selected_event');
                switch (BM::$input_btn) {
                    case 'send_itinary':
                        return BM::setState('send_itinary');
                        break;
                    case 'send_image':
                        return BM::setState('send_image');
                        break;
                    case 'send_clip':
                        return BM::setState('send_clip');
                        break;

                    case 'broadcastmsg':
                        return BM::setState("create_broadcastmsg");
                        break;
                    case 'users_list':
                        return BM::setState("get_users_list");
                        break;
                }
            }

            function error_state()
            {
                return (BM::$input_type != 'callback_query');
            }

            function error()
            {
                $this->addMessage(__('composers_bots.messages.just.select_item'));
            }

        };

//==================================================================================================

        static::$states['one_event_manage_finished'] = new class extends State
        {

            var $btn_type = 'inline';

            function start()
            {
                $event = BM::getCache('selected_event');

                $this->base_message = __('composers_bots.messages.just_four_days');
                $this->buttons[] = [
                    __('composers_bots.buttons.send_itinary') => ['callback_data' => 'send_itinary', 'condition' => $event->status == 'finished'],
                ];

                $this->buttons[] = [
                    __('composers_bots.buttons.send_clip')  => ['callback_data' => 'send_clip', 'condition' => $event->status == 'finished'],
                    __('composers_bots.buttons.send_image') => ['callback_data' => 'send_image', 'condition' => $event->status == 'finished'],
                ];

                $this->buttons[] = [
                    __('composers_bots.buttons.hide_event')  => ['callback_data' => 'hide_event', 'condition' => $event->status == 'archived'],
                    __('composers_bots.buttons.get_details') => ['callback_data' => 'get_details'],
                ];

                $this->buttons[] = [
                    __('composers_bots.buttons.copytonew') => ['callback_data' => 'copytonew'],
                ];
//                $this->buttons[] = [
//                    __('composers_bots.buttons.scores')    => ['callback_data' => 'scores'],
//                ];

                $this->buttons[][__('composers_bots.buttons.return_base')] = ['callback_data' => 'gobase'];
            }

            function action()
            {
                if (BM::$input_btn == 'back') return BM::setState('events_list');

                $event = BM::getCache('selected_event');
                switch (BM::$input_btn) {
                    case 'send_itinary':
                        if ($event->status != 'finished') {
                            $this->addMessage(__('composers_bots.messages.finished_intinary_limit'));
                            break;
                        }
                        return BM::setState('send_itinary');
                        break;
                    case 'send_image':
                        if ($event->status != 'finished') {
                            $this->addMessage(__('composers_bots.messages.finished_image_limit'));
                            break;
                        }
                        return BM::setState('send_image');
                        break;
                    case 'send_clip':
                        if ($event->status != 'finished') {
                            $this->addMessage(__('composers_bots.messages.finished_clip_limit'));
                            break;
                        }
                        return BM::setState('send_clip');
                        break;

                    case 'hide_event':
                        $event->update(['status' => 'fullyarchived']);
                        return BM::setState('wellcome', __('composers_bots.messages.event.hide_done', ['event_title' => $event->title[LANGUAGE]]));
                        break;
                    case 'copytonew':
                        $event = $event->toArray();
                        $event['duration'] = null;
                        $event['status'] = 'draft';
                        $event = Eventt::create($event);
                        BM::setCache('copied_event', $event);

                        BM::setCache('Position', 'CopyToNew');
                        return BM::setState('create_theme_2');
                        break;
                    case 'get_details':
                        return $this->addMessage(ComposerBots::getDetails());
                        break;
                }
            }

            function error_state()
            {
                return (BM::$input_type != 'callback_query');
            }

            function error()
            {
                $this->addMessage(__('composers_bots.messages.just.select_item'));
            }

        };

//==================================================================================================

        //region Send Files

        static::$states['send_image'] = new class extends State
        {

            var $btn_type = 'footer_sticky';

            function start()
            {
                $images = BM::getCache('images', []);
                $this->base_message = empty($images)?__('composers_bots.messages.send_image'):__('composers_bots.messages.next_send_image');

                $this->buttons[] = [
                    __('composers_bots.buttons.cancel') => ['condition' => empty($images)],
                    __('composers_bots.buttons.done')   => ['condition' => !empty($images)],
                ];

                BM::setCache('dont_clear_btn', false);
            }

            function action()
            {
                if (BM::$input_text === __('composers_bots.buttons.cancel')) {
                    BM::removeCache('images');
                    return BM::backState();
                }

                if (BM::$input_text === __('composers_bots.buttons.done')) {
                    BM::removeCache('images');
                    return BM::backState(__('composers_bots.messages.image_done'));
                }

                $last = end(BM::$input['photo']);
                $image = BM::operate('Get Photo', 'getPhoto', [$last['file_id']]);

                if ($image['status'] == 200) {
                    $event = BM::getCache('selected_event');
                    $user = BM::getCache('user_data');

                    $tmp = Album::create([
                        'user_id'        => $user->id,
                        'event_id'       => $event->id,
                        'attach_id'      => $image['data'],
                        'attach_model'   => 'Image',
                        'source'         => 'Telegram',
                        'type_in_source' => BM::$input_subtype,
                        'order'          => null,
                        'status'         => 'requested',//'accepted' for admins & private_events
                    ]);

                    $message = "";
                    if (is_null(BM::getCache('images', null))) {
                        $message =
                            "🎉 <b>New Image</b>\n"
                            . "-------------------"
                            . "\nOwner: {$user->fullname}"
                            . "\nCellphone: " . $user->cellphone
                            . "\nRobot: " . str_replace('App\Http\Controllers\TelegramBots\\', '', BM::$CLASS_NAME)
                            . "\n\n-------------------"
                            . "\nTitle: " . $event->title[LANGUAGE]
                            . "\nCategory: " . $event->category->title[LANGUAGE]
                            . "\n\n-----[ Time ]-----"
                            . "\nStart: " . $event->start_date
                            . "\nEnd: " . $event->end_date
                            . "\nDuration: " . $event->duration
                            . "\n\n-----[ Oth ]-----"
                            . "\n\nDescription: \n" . $event->description
                            . "\n\n-----[ State ]-----"
                            . "\nState: \n" . ucwords($event->status)
                            . "\n-------------------";
                    } else {
                        $message =
                            "🎉 <b>Another New Image</b>\n"
                            . "-------------------"
                            . "\nOwner: {$user->fullname}"
                            . "\nCellphone: " . $user->cellphone
                            . "\nTitle: " . $event->title[LANGUAGE]
                            . "\n-------------------";
                    }

                    @alertAdminTelegramImage($message, url($tmp->file->path)
                        , [
                            'Accept'     => ['url' => url(trans('panels.admin') . '/albums/list?action=do_accept&rec_id=' . $event->id), 'condition' => $event->status == 'requested'],
                            'Reject'     => ['url' => url(trans('panels.admin') . '/albums/list?action=do_reject&rec_id=' . $event->id), 'condition' => $event->status == 'requested'],
                            'Reject All' => ['url' => url(trans('panels.admin') . '/albums/form?action=do_rejectall&rec_id=' . $event->id), 'condition' => $event->status == 'requested'],
                        ]
                    );


                    BM::pushCache('images', $tmp);
                }
            }

            function error_state()
            {
                return (BM::$input_text !== __('composers_bots.buttons.cancel') && BM::$input_text !== __('composers_bots.buttons.done')
                    &&
                    !in_array(ucwords(BM::$input_subtype) , [
                        'Photo',
                        'MediaGroup',
                    ]) );
            }

            function error()
            {
                $this->addMessage(__('composers_bots.messages.just.upload_image'));
            }

        };

        static::$states['send_clip'] = new class extends State
        {

            var $btn_type = 'footer_sticky';

            function start()
            {
                $clips = BM::getCache('clips', []);
                $this->base_message = empty($clips)?__('composers_bots.messages.send_clip'):__('composers_bots.messages.next_send_clip');

                $this->buttons[] = [
                    __('composers_bots.buttons.cancel') => ['condition' => empty($clips)],
                    __('composers_bots.buttons.done')   => ['condition' => !empty($clips)],
                ];

                BM::setCache('dont_clear_btn', false);
            }

            function action()
            {
                if (BM::$input_text === __('composers_bots.buttons.cancel')) {
                    BM::removeCache('clips');
                    return BM::backState();
                }

                if (BM::$input_text === __('composers_bots.buttons.done')) {
                    BM::removeCache('clips');
                    return BM::backState(__('composers_bots.messages.clip_done'));
                }

                $record = BM::$input[BM::$input_subtype];
                $file = BM::operate('Get Video', 'getFile', [$record['file_id']]);

                if ($file['status'] == 200) {
                    $event = BM::getCache('selected_event');
                    $user = BM::getCache('user_data');

                    $tmp = Album::create([
                        'user_id'        => $user->id,
                        'event_id'       => $event->id,
                        'attach_id'      => $file['data'],
                        'attach_model'   => 'Attachment',
                        'source'         => 'Telegram',
                        'type_in_source' => BM::$input_subtype,
                        'order'          => null,
                        'status'         => 'requested',//'accepted' for admins & private_events
                    ]);

                    $message = "";
                    if (is_null(BM::getCache('clips', null))) {
                        $message =
                            "🎉 <b>New Clip</b>\n"
                            . "-------------------"
                            . "\nOwner: {$user->fullname}"
                            . "\nCellphone: " . $user->cellphone
                            . "\nRobot: " . str_replace('App\Http\Controllers\TelegramBots\\', '', BM::$CLASS_NAME)
                            . "\n\n-------------------"
                            . "\nTitle: " . $event->title[LANGUAGE]
                            . "\nCategory: " . $event->category->title[LANGUAGE]
                            . "\n\n-----[ Time ]-----"
                            . "\nStart: " . $event->start_date
                            . "\nEnd: " . $event->end_date
                            . "\nDuration: " . $event->duration
                            . "\n\n-----[ Oth ]-----"
                            . "\n\nDescription: \n" . $event->description
                            . "\n\n-----[ State ]-----"
                            . "\nState: \n" . ucwords($event->status)
                            . "\n-------------------";
                    } else {
                        $message =
                            "🎉 <b>Another New Clip</b>\n"
                            . "-------------------"
                            . "\nOwner: {$user->fullname}"
                            . "\nCellphone: " . $user->cellphone
                            . "\nTitle: " . $event->title[LANGUAGE]
                            . "\n-------------------";
                    }

                    @alertAdminTelegramFile(BM::$input_subtype, $message, url($tmp->file->path)
                        , [
                            'Accept'     => ['url' => url(trans('panels.admin') . '/albums/list?action=do_accept&rec_id=' . $event->id), 'condition' => $event->status == 'requested'],
                            'Reject'     => ['url' => url(trans('panels.admin') . '/albums/list?action=do_reject&rec_id=' . $event->id), 'condition' => $event->status == 'requested'],
                            'Reject All' => ['url' => url(trans('panels.admin') . '/albums/form?action=do_rejectall&rec_id=' . $event->id), 'condition' => $event->status == 'requested'],
                        ]
                    );


                    BM::pushCache('clips', $tmp);
                }
            }

            function error_state()
            {
                return (BM::$input_text !== __('composers_bots.buttons.cancel') && BM::$input_text !== __('composers_bots.buttons.done')
                    &&
                    !in_array(ucwords(BM::$input_subtype) , [
                        'Video',
                        'Animation',
                    ]) );
            }

            function error()
            {
                $this->addMessage(__('composers_bots.messages.just.upload_video'));
            }

        };

        static::$states['send_itinary'] = new class extends State
        {

            var $btn_type = 'footer_sticky';

            function start()
            {
                $files = BM::getCache('files', []);
                $this->base_message = empty($files)?__('composers_bots.messages.send_itinary'):__('composers_bots.messages.next_send_itinary');

                $this->buttons[] = [
                    __('composers_bots.buttons.cancel') => ['condition' => empty($files)],
                    __('composers_bots.buttons.done')   => ['condition' => !empty($files)],
                ];

                BM::setCache('dont_clear_btn', false);
            }

            function action()
            {
                if (BM::$input_text === __('composers_bots.buttons.cancel')) {
                    BM::removeCache('files');
                    return BM::backState();
                }

                if (BM::$input_text === __('composers_bots.buttons.done')) {
                    BM::removeCache('files');
                    return BM::backState(__('composers_bots.messages.itinary_done'));
                }

                $record = BM::$input[BM::$input_subtype];
                $file = BM::operate('Get ّtinary', 'getFile', [$record['file_id']]);

                if ($file['status'] == 200) {
                    $event = BM::getCache('selected_event');
                    $user = BM::getCache('user_data');

                    $tmp = Album::create([
                        'user_id'        => $user->id,
                        'event_id'       => $event->id,
                        'attach_id'      => $file['data'],
                        'attach_model'   => 'Attachment',
                        'source'         => 'Telegram',
                        'type_in_source' => BM::$input_subtype,
                        'order'          => null,
                        'status'         => 'requested',//'accepted' for admins & private_events
                    ]);

                    $message = "";
                    if (is_null(BM::getCache('files', null))) {
                        $message =
                            "🎉 <b>New Itinary</b>\n"
                            . "-------------------"
                            . "\nOwner: {$user->fullname}"
                            . "\nCellphone: " . $user->cellphone
                            . "\nRobot: " . str_replace('App\Http\Controllers\TelegramBots\\', '', BM::$CLASS_NAME)
                            . "\n\n-------------------"
                            . "\nTitle: " . $event->title[LANGUAGE]
                            . "\nCategory: " . $event->category->title[LANGUAGE]
                            . "\n\n-----[ Time ]-----"
                            . "\nStart: " . $event->start_date
                            . "\nEnd: " . $event->end_date
                            . "\nDuration: " . $event->duration
                            . "\n\n-----[ Oth ]-----"
                            . "\n\nDescription: \n" . $event->description
                            . "\n\n-----[ State ]-----"
                            . "\nState: \n" . ucwords($event->status)
                            . "\n-------------------";
                    } else {
                        $message =
                            "🎉 <b>Another New Itinary</b>\n"
                            . "-------------------"
                            . "\nOwner: {$user->fullname}"
                            . "\nCellphone: " . $user->cellphone
                            . "\nTitle: " . $event->title[LANGUAGE]
                            . "\n-------------------";
                    }

                    @alertAdminTelegramFile(BM::$input_subtype, $message, url($tmp->file->path)
                        , [
                            'Accept'     => ['url' => url(trans('panels.admin') . '/albums/list?action=do_accept&rec_id=' . $event->id), 'condition' => $event->status == 'requested'],
                            'Reject'     => ['url' => url(trans('panels.admin') . '/albums/list?action=do_reject&rec_id=' . $event->id), 'condition' => $event->status == 'requested'],
                            'Reject All' => ['url' => url(trans('panels.admin') . '/albums/form?action=do_rejectall&rec_id=' . $event->id), 'condition' => $event->status == 'requested'],
                        ]
                    );


                    BM::pushCache('files', $tmp);
                }
            }

            function error_state()
            {
                return (BM::$input_text !== __('composers_bots.buttons.cancel') && BM::$input_text !== __('composers_bots.buttons.done')
                    &&
                    !in_array(ucwords(BM::$input_subtype) , [
                        'Audio',
                        'Document',
                        'Voice',
                    ]) );
            }

            function error()
            {
                $this->addMessage(__('composers_bots.messages.just.upload_itinary'));
            }

        };

        //endregion

//==================================================================================================
//==================================================================================================

        static::$states['create_broadcastmsg'] = new class extends State
        {

            var $btn_type = 'inline';

            function start()
            {
                $event = BM::getCache('selected_event');

                $this->base_message = __('composers_bots.messages.write_broadcastmsg');

                $this->buttons[][__('composers_bots.buttons.back')] = ['callback_data' => 'back'];
            }

            function action()
            {
                if (BM::$input_btn == 'back') return BM::backState();

                $event = BM::getCache('selected_event');
                $attendees = $event->attendees;

                foreach ($attendees as $user) {
                    BM::operate('BROADCAST', 'mainSendMessage', [__('composers_bots.messages.broadcastmsg_from_composer', ['eventor_name' => $event->composer->fullname, 'eventor_tlg' => $event->composer->telegram_uid, 'event_name' => $event->title[LANGUAGE]]) . trim(BM::$input_text)], $event->category->bot_id, $user->id, null);
                }

                return BM::backState(__('composers_bots.messages.broadcastmsg_done'));
            }
        };

//==================================================================================================

        static::$states['get_users_list'] = new class extends State
        {

            var $btn_type = 'inline';

            function start()
            {
                $this->base_message = __('composers_bots.messages.users_list');

                $attendees = BM::getCache('selected_event')->attendees;

                if ($attendees->isEmpty()) {
                    $this->base_message .= __('composers_bots.messages.empty_users_list');
                } else {
                    $msg = [];
                    foreach ($attendees as $i => $user) {
                        $msg[] = ($i + 1) . ". {$user->fullname} | {$user->cellphone}";
//                        $msg[] = ($i+1).". <a href=\"tg://user?id={$user->telegram_uid}\">{$user->fullname}<\a>";
                    }

                    $this->base_message .= implode("\n-------------\n", $msg);
                }

                $this->buttons[][__('composers_bots.buttons.back')] = ['callback_data' => 'back'];
            }

            function action()
            {
                if (BM::$input_btn == 'back') return BM::backState();
            }
        };

//==================================================================================================
//==================================================================================================

        static::$states['register_mobile'] = new class extends State
        {

            var $btn_type = 'footer';

            function start()
            {
                $this->base_message = __('composers_bots.messages.register.step_1_mobile');

                $this->buttons[][__('composers_bots.buttons.send_mobile')] = ['type' => 'request_contact'];
            }

            function error_state()
            {
                return (!in_array(BM::$msg_type, ['contact', 'phone_number']));
            }

            function error()
            {
                $this->addMessage(__('composers_bots.messages.just.get_contact', ['contact_button' => __('composers_bots.buttons.send_mobile')]));
            }

            function action()
            {
                $tmp = BM::$input['contact']['phone_number'];

//          $tmp = preg_replace("/^98/m","+98",$tmp);
//          $tmp = preg_replace("/^0/m","+98",$tmp);
                $tmp = preg_replace("/^\+98/m", "0", $tmp);
                $tmp = preg_replace("/^98/m", "0", $tmp);

                BM::setCache('registration', BM::$user_id, 'telegram_uid');
                BM::setCache('registration', $tmp, 'cellphone');

                if (ComposerBots::makeUser('check_cellphone', $tmp)) {
                    if (BM::getCache('mode') == 'event_make') {
                        return BM::setState('create_cat_1',
                            __('composers_bots.messages.greeting.known', ['name' => BM::getCache('user_data')['fullname']])
                            . "\n" . __('composers_bots.messages.greeting.site_member'));
                    } else {

                        return BM::setState('wellcome',
                            __('composers_bots.messages.greeting.known', ['name' => BM::getCache('user_data')['fullname']])
                            . "\n" . __('composers_bots.messages.greeting.site_member'));
                    }
                } else {
                    return BM::setState('register_fullname');
                }

            }

        };

//==================================================================================================

        static::$states['register_fullname'] = new class extends State
        {

            function start()
            {
                $this->base_message = __('composers_bots.messages.register.step_2_name');
            }

            function error_state()
            {
                return (BM::$msg_type != 'text');
            }

            function error()
            {
                $this->addMessage(__('composers_bots.messages.just.get_field', ['field' => __('composers_bots.fields.fullname')]));
            }

            function action()
            {
                BM::setCache('registration', BM::$input_text, 'fullname');
                $newpass = ComposerBots::makeUser('password');

                if (BM::getCache('mode') == 'event_make') {
                    return BM::setState('create_cat_1', __('composers_bots.messages.register.done'));
                } else {
                    return BM::setState('wellcome', __('composers_bots.messages.register.done'));
                }
            }

        };

    }
//==================================================================================================
//==================================================================================================

    static function loadUserData()
    {
        $tmp = User::isTelegramMember(BM::$user_id);
        return $tmp;
//        return ((!empty($tmp)) ? $tmp : false);
    }

    static function getPreview()
    {

        $event_data = BM::getFullCache('newevent');

        $event_data['startdate'] = dateArrayToStr($event_data['startdate'], 'Y|M|D|H|I');
        $event_data['enddate'] = dateArrayToStr($event_data['enddate'], 'Y|M|D|H|I');
        $event_data['deadline'] = dateArrayToStr($event_data['deadline'], 'Y|M|D|H|I');
        $low = $event_data['capacity_min'];
        $high = $event_data['capacity_max'];

        try {
            $ret = __('composers_bots.preview', [
                'event_title'       => $event_data['title'],
                'event_location'    => (isset($event_data['address_title']) && !empty($event_data['address_title'])?'('.$event_data['address_title'].') ':'').$event_data['address'],
                'event_deadline'    => Eventt::dateBeautifier($event_data['deadline'], 'Y|m|d|H|i'),
//                'event_deadline' => Eventt::dateBeautifier($event_data['deadline']),
                'event_start'       => Eventt::dateBeautifier($event_data['startdate'], 'Y|m|d|H|i'),
//                'event_start' => Eventt::dateBeautifier($event_data['startdate']),
                'event_end'         => Eventt::dateBeautifier($event_data['enddate'], 'Y|m|d|H|i'),
//                'event_end' => Eventt::dateBeautifier($event_data['enddate']),
                'event_cost'        => ($event_data['pay_type'] == 'free' ?
                    __('composers_bots.messages.free')
                    : ($event_data['pay_type'] == 'dongi' ? __('composers_bots.messages.dongi')
                        : number_format($event_data['cost']) . ' تومان')
                ),
                'event_pay_type'    => __('composers_bots.pay_type.' . $event_data['pay_type']),
                'event_description' => $event_data['description'],
                'cap_min'           => ($low == 0 ? __('composers_bots.buttons.no_min_cap') : __('composers_bots.messages.capacity.count', ['count' => $low])),
                'cap_max'           => ($high == 0 ? __('composers_bots.buttons.no_max_cap') : __('composers_bots.messages.capacity.count', ['count' => $high])),
            ]);

            $ret .= __('composers_bots.event.minimal', ["minimal" => ($event_data['minimals'] ? $event_data['minimals'] : __('composers_bots.messages.minimals_nothing'))]);
            $ret .= __('composers_bots.event.limits', ["gender_type" => __('composers_bots.messages.gender.' . $event_data['gender'])]);

            return $ret;
        } catch (Exception $e) {
            getFormedError($e);
        }
    }

    static function createEvent()
    {
        $req = static::getFullCache('newevent', null);
        if (is_null($req)) return;

        $address['geocode'] = $req['address'];
        if (isset($req['address_title'])) $address['title'][LANGUAGE] = $req['address_title'];

        $tmp = Address::create($address);
        $req['base_location_id'] = $tmp->id;

        $user_data = static::getCache('user_data');
        $req['title'] = [LANGUAGE => $req['title']];
        $req['composer_id'] = $user_data->id;
        $req['price_type'] = 1;
        $req['banner_bg_type'] = isset($req['background_type']) ? $req['background_type'] : 'automatic';
        $req['image_id'] = (isset($req['background_type']) && ($req['background_type'] != 'automatic')) ? $req['image'] : null;
        $req['banner_bg_id'] = (isset($req['background_type']) && ($req['background_type'] == 'automatic')) ? $req['background']->id : 0;
        $req['category_id'] = $req['category']->id;
        $req['capacity'] = ['min' => $req['capacity_min'], 'max' => $req['capacity_max'], 'extra' => $req['capacity_min']];
        $req['register_deadline'] = dateArrayToStr($req['deadline'], 'Y-M-D H:I');
        $req['start_date'] = dateArrayToStr($req['startdate'], 'Y-M-D H:I');
        $req['end_date'] = dateArrayToStr($req['enddate'], 'Y-M-D H:I');
        $req['wage'] = ((!Setting::isSame('get_wage', '1')) || ($req['pay_type'] !== 'online')) ?
            0 :
            (
            $req['cost'] <= floatval($req['category']->cost_divider) ?
                floatval($req['category']->cost_fix) :
                ceil(ceil(floatval($req['category']->cost_percent) * $req['cost'] / 100) / 100) * 100
            );

        $req = Eventt::create($req);
        $req['slogan'] = randStr(3, [0, 0, 1, 0], false) . $req->id . randStr(2, [0, 0, 1, 0], false);
        $req->save();
        static::resetCache('newevent', $req);

        if ($req->status == 'accepted') {
            try {
                BM::operate(
                    'Composer::Create Event',
                    'sendBanner', [
                        $req->id,
                        LANGUAGE,
                        __('composers_bots.messages.photo_link', [
                            'title'  => $req->title[LANGUAGE],
                            'params' => $req->category->bot->username . '?start=M-' . $req->slogan
                        ]),
                        false,
                        [//forward to this chat id and composers channel
                            BM::$chat_id
                            , '-1001255760808'
                        ]
                    ]
                    , null, null, '-1001327534278'
                );
            } catch (\Exception $e) {
                getFormedError($e);
            }
        }

        if ($req->status == 'testaccepted') {

            try {
                BM::operate(
                    'Composer::Test Accept',
                    'sendBanner', [
                        $req->id,
                        LANGUAGE,
                        __('composers_bots.messages.photo_link', [
                            'title'  => $req->title[LANGUAGE],
                            'params' => $req->category->bot->username . '?start=M-' . $req->slogan
                        ]),
                    ]
                );
            } catch (\Exception $e) {
                getFormedError($e);
            }
        }

        @alertAdminTelegram("🎉 <b>New Event</b>\n"
            . "-------------------"
            . "\nComposer: {$user_data->fullname}"
            . "\nCellphone: " . $user_data->cellphone
            . "\nRobot: " . str_replace('App\Http\Controllers\TelegramBots\\', '', BM::$CLASS_NAME)
            . "\nCategory: " . $req->category->title[LANGUAGE]
            . "\n\n-------------------"
            . "\nTitle: " . $req->title[LANGUAGE]
            . "\nLocation: " . $req->base_location->geocode
            . "\nCapacity: {" . $req->capacity['min'] . "," . $req->capacity['max'] . "+" . $req->capacity['extra'] . "}"
            . "\n\n-----[ Money ]-----"
            . "\nCost: " . ($req['pay_type'] == 'free' ?
                __('composers_bots.messages.free')
                : ($req['pay_type'] == 'dongi' ? __('composers_bots.messages.dongi')
                    : number_format($req['cost']))
            )
            . "\nWage: " . $req->wage
            . "\nCurrency: " . $req->PriceTypeRecord->title['en']
            . "\n\n-----[ Time ]-----"
            . "\nCreation: " . $req->created_at
            . "\nDeadLine: " . $req->register_deadline
            . "\nStart: " . $req->start_date
            . "\nEnd: " . $req->end_date
            . "\nDuration: " . $req->duration
            . "\n\n-----[ Oth ]-----"
            . "\nGender: " . $req->gender
            . "\nMinimals: \n" . $req->minimals
            . "\n\nDescription: \n" . $req->description
            . "\n\n-----[ State ]-----"
            . "\nState: \n" . ucwords($req->status)
            , [
                'Accept' => ['url' => url(trans('panels.admin') . '/events/list?action=do_accept&rec_id=' . $req->id), 'condition' => $req->status == 'requested'],
                'Edit'   => ['url' => url(trans('panels.admin') . '/events/form?action=edit&rec_id=' . $req->id)],
                'Reject' => ['url' => url(trans('panels.admin') . '/events/list?action=do_reject&rec_id=' . $req->id), 'condition' => $req->status == 'requested'],
            ]
        );
        return true;
    }

    static function get_progress($curr, $total)
    {
        if (!is_null(BM::getCache('Position', null))) return '';

//        $count = static::MAX_SUBMIT_STEPS;
        $count = static::SYMBOLS;
        $percent = floor($curr / $total * $count);
        $bar = '';

        if (static::PROGRESS_DIR == 'RTL') {
            for ($i = 1; $i <= $count; $i++) {
                $bar = (($i <= $percent) ? static::progress_one : static::progress_zero) . $bar;
            }
        } else {
            for ($i = $count; $i >= 1; $i--) {
                $bar = (($i <= $percent) ? static::progress_one : static::progress_zero) . $bar;
            }
        }

        return __('composers_bots.messages.progress', ['bar' => $bar, 'percent' => floor($curr / $total * 100)]);
    }

    static function getItems($mode)
    {
        $page = BM::getCache('page', 1);

        $events = [];
        switch ($mode) {
            case 'draft':
                $events = BM::getCache('user_data')->getStatedEvents(['draft', 'testaccepted'], null, null, $page, 5);
                break;
            case 'accepted':
                $events = BM::getCache('user_data')->getStatedEvents(['accepted', 'closed_list'], null, null, $page, 5);
                break;
            case 'finished':
                $events = BM::getCache('user_data')->getStatedEvents(['finished', 'archived'], ['updated_at' => 'DESC'], null, $page, 5);
                break;
            case 'live':
                $events = BM::getCache('user_data')->getStatedEvents('live', null, null, $page, 5);
                break;
        }
        $items = ['message' => '', 'markups' => [], 'maxpages' => $events['pages']];
        BM::setCache('maxpages', $events['pages']);

        if (count($events['items']) > 0) {
            foreach ($events['items'] as $event) {
                $items['markups'][] = [$event->title[LANGUAGE] => ['callback_data' => "$event->id"]];
            }
            if ($page < $events['pages']) $items['markups'][] = [__('composers_bots.buttons.nextpage') => ['callback_data' => 'nextpage']];
            if ($page > 1) $items['markups'][] = [__('composers_bots.buttons.prevpage') => ['callback_data' => 'prevpage']];
            $items['markups'][] = [__('composers_bots.buttons.return_base') => ['callback_data' => 'gobase']];

            $items['message'] = __('composers_bots.messages.' . ($mode == 'draft' ? 'click_to_edit' : 'click_to_manage'));
        }

        return $items;
    }

    static function getSummary()
    {
        $event_data = BM::getCache('selected_event');

        $event = Eventt::findOrFail($event_data->id);

        try {
            $ret = __('composers_bots.summary', [
                'event_title'    => $event->title[LANGUAGE],
                'event_cat'      => $event->category->title[LANGUAGE],
                'event_start'    => Eventt::dateBeautifier($event->start_date),
                'attendee_count' => $event->attendeesCount(),
//                'event_location'    => (isset($event->base_location->title[LANGUAGE])?$event->base_location->title[LANGUAGE].' : ':'').$event->base_location->geocode,
                'status'         => __('composers_bots.states.' . $event->status),
            ]);

            return $ret;
        } catch (Exception $e) {
            getFormedError($e);
        }
    }

    static function getDetails()
    {
        $event_data = BM::getCache('selected_event');

        $event = Eventt::findOrFail($event_data->id);

        $low = $event->capacity['min'];
        $high = $event->capacity['max'];

        try {
            $ret = __('composers_bots.details', [
                'event_title'       => $event->title[LANGUAGE],
                'event_cat'         => $event->category->title[LANGUAGE],
                'eventor_gender'    => __('defaults.gender.other.' . $event->composer->sex),
                'eventor_name'      => $event->composer->fullname,
                'eventor_telegram'  => 'tg://user?id=' . $event->composer->telegram_uid,
                'attendee_count'    => $event->attendeesCount(),
                'event_location'    => (isset($event->base_location->title[LANGUAGE]) ? '('.$event->base_location->title[LANGUAGE] . ') ' : '') . $event->base_location->geocode,
                'event_deadline'    => Eventt::dateBeautifier($event->register_deadline),
                'event_start'       => Eventt::dateBeautifier($event->start_date),
                'event_end'         => Eventt::dateBeautifier($event->end_date),
                'event_duration'    => $event->duration,
                'event_cost'        => ($event->pay_type == 'free' ?
                    __('composers_bots.messages.free')
                    : ($event->pay_type == 'dongi' ? __('composers_bots.messages.dongi')
                        : number_format($event['cost'] + $event['wage']) . ' ' . $event->PriceTypeRecord->title[LANGUAGE])
                ),
                'event_pay_type'    => __('composers_bots.pay_type.' . $event->pay_type),
                'event_description' => $event->description,
                'cap_min'           => ($low == 0 ? __('composers_bots.buttons.no_min_cap') : __('composers_bots.messages.capacity.count', ['count' => $low])),
                'cap_max'           => ($high == 0 ? __('composers_bots.buttons.no_max_cap') : __('composers_bots.messages.capacity.count', ['count' => $high])),
                'status'            => __('composers_bots.states.' . $event->status),
            ]);

            if (!empty($event->minimals)) {
                $ret .= __('composers_bots.event.minimal', ["minimal" => ($event->minimals ? $event->minimals : __('composers_bots.messages.minimals_nothing'))]);
            }
            if ($event->gender != 'both') {
                $ret .= __('composers_bots.event.limits', ["gender_type" => __('composers_bots.messages.gender.' . $event->gender)]);
            }

            return $ret;
        } catch (Exception $e) {
            getFormedError($e);
        }
    }

//==================================================================================================

    static function makeUser($mode, $data = null)
    {
        switch ($mode) {
            case 'check_cellphone':
                $tmp = User::where('cellphone', $data)->first();
                if (!empty($tmp)) {
                    $tmp->update(['telegram_uid' => BM::getCache('registration', null, 'telegram_uid')]);
                    BM::setCache('user_data', $tmp);
                    return true;
                }
                return false;
                break;
            case 'password':
                $newpass = randStr(6, [0, 0, 1, 0], true);

                $req = BM::getFullCache('registration');

                $req['group_id'] = 5;
                $req['random_color'] = rand(1, count(trans('nologo.colors'))) - 1;
                $req['sex'] = 'unknown';
                $req['commission_percent'] = null;
                $req['password'] = \Hash::make($newpass);
                $req['ref_code'] = randStr(4, [0, 0, 1, 0]);
                $req = User::create($req);
                $req->ref_code = $req->ref_code . $req->id;
                $req->save();
                Asset::Create(['user_id' => $req->id]);

                BM::removeCache('telegram_id');
                BM::setCache('user_data', $req);
                @alertAdminTelegram("🚻 <b>New User @ " . str_replace('App\Http\Controllers\TelegramBots\\', '', BM::$CLASS_NAME) . "</b>"
                    . "\n-------------------"
                    . "\nfullname: $req->fullname"
                    . "\ncellphone: $req->cellphone" . "\nrobot: " . str_replace('App\Http\Controllers\TelegramBots\\', '', BM::$CLASS_NAME));

                return $newpass;
                break;
            case 'refcode':
                $user_data = BM::getCache('user_data');
                $ref = User::where('ref_code', $data)->orWhere('cellphone', $data)->first();
                if (empty($ref) || $ref->cellphone == $user_data->cellphone) return false;
                $user_data->update(['referer_id' => $ref->id]);
                @alertAdminTelegram("➡ <b>Ref to User</b>\n" . "-------------------" . "\nfullname: {$user_data->fullname}"
                    . "\nrefto: $ref->fullname" . "\nrobot: " . str_replace('App\Http\Controllers\TelegramBots\\', '', BM::$CLASS_NAME));
                return true;
                break;
        }
    }

}
