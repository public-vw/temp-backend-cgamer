<?php

namespace App\Http\Controllers\System\TelegramBots;

use App\Http\Controllers\TelegramBots\Bases\BotMachine as BM;
use App\Http\Controllers\TelegramBots\Bases\State;
use App\Models\PhsycoMbtiKey;
use App\Models\PhsycoMbtiMode;
use App\Models\PhsycoMbtiValue;
use App\Models\PhsycoPerson;

class JimiTheBot extends BM
{
//    const DEBUG_MODE = false; // tasiri nadare?! az define estefade mikone!!!

    static $base_state = 'atyourservice';
    static $temp = null;

//==================================================================================================
    protected static function reboot_state()
    {
        if (BM::$input_text === __('attendees_bots.buttons.return_base')) {
            return null;
        }
        if (BM::$input_btn === 'gobase') {
            return null;
        }
        return false;
    }

//==================================================================================================
    protected static function config()
    {


        static::$states['atyourservice'] = new class extends State
        {
            var $btn_type = 'inline';

            function start()
            {
                if (!in_array(BM::$user_id, BM::DEBUG_USRS_ID)) {
                    $this->base_message = "<b>سلام، خوش اومدی</b>\n" . 'مهندسین در حال تکمیل من هستن' . "\nو بزودی فعالم می کنن";
                    return;
                }

                $this->base_message = "خوشحالم که باز میبینمت رئیس" . " :)\n";
                $this->base_message .= "لطفا یک بخش رو انتخاب کن";
                $this->buttons[]['MBTI'] = ['callback_data' => 'mbti'];
            }

            function action()
            {
                switch (BM::$input_btn) {
                    case 'mbti':
                        return BM::setState('mbti_section');
                        break;
                }
            }
        };

//==================================================================================================
//==================================================================================================

        static::$states['mbti_section'] = new class extends State
        {

            var $btn_type = 'inline';

            function start()
            {
                $this->base_message = "<b>MBTI</b>\n" . "چطور میتونم کمکی کنم؟";
                $this->buttons[]['آرشیو محتوا'] = ['callback_data' => 'mbti_data'];
                $this->buttons[]['آرشیو اشخاص'] = ['callback_data' => 'mbti_person'];
                $this->buttons[]['ثبت شخص جدید'] = ['callback_data' => 'mbti_reg_start'];
            }

            function action()
            {
                return BM::setState(BM::$input_btn);
            }

        };
//==================================================================================================

        static::$states['mbti_person'] = new class extends State
        {

            var $btn_type = 'inline';

            function start()
            {
                $this->base_message .= "لطفا یک نفر را انتخاب کن";

                $i = 0;
                $btn = [];
                foreach (PhsycoPerson::orderByDesc('created_at')->get() as $item){
                    $btn[$item->name.' | '.$item->mbti_mode_stream] = ['callback_data' => 'pers_' . $item->id];
                    if($i % 2 == 0){
                        $i++;
                    }
                    else{
                        $this->buttons[] = $btn;
                        $i=0;
                        $btn = [];
                    }
                }
                if(!empty($btn)) $this->buttons[] = $btn;
                $this->buttons[]['Main Menu'] = ['callback_data' => 'gobase'];
            }

            function action()
            {
                BM::setCache('person', PhsycoPerson::findOrFail(substr(BM::$input_btn,5)),'all');
                return BM::setState('mbti_keys');
            }

        };
//==================================================================================================

        static::$states['mbti_reg_start'] = new class extends State
        {

            function start()
            {
                $this->base_message = "اسم کامل شخص";
            }

            function action()
            {
                BM::setCache('person', trim(BM::$input_text), 'name');
                return BM::setState('mbti_reg_ie');
            }

        };
//==================================================================================================

        static::$states['mbti_reg_ie'] = new class extends State
        {

            var $btn_type = 'inline';

            function start()
            {
                $this->base_message = "How to <b>Energizing</b>?\n\n";
                $this->base_message .= "<b>I</b>nterovert\nor\n<b>E</b>xtrovert";
                $this->buttons[] = [
                    '[I] nterovert' => ['callback_data' => 'i'],
                    '[E] xtrovert'  => ['callback_data' => 'e']
                ];
            }

            function action()
            {
                BM::setCache('person', trim(BM::$input_btn), 'ie');
                return BM::setState('mbti_reg_ns');
            }

        };

//==================================================================================================

        static::$states['mbti_reg_ns'] = new class extends State
        {

            var $btn_type = 'inline';

            function start()
            {
                $this->base_message = "How to taking <b>Information</b>?\n\n";
                $this->base_message .= "<b>S</b>ensing\nor\ni<b>N</b>tutation";
                $this->buttons[] = [
                    '[S] ensing'     => ['callback_data' => 's'],
                    'i [N] tutation' => ['callback_data' => 'n']
                ];
            }

            function action()
            {
                BM::setCache('person', trim(BM::$input_btn), 'ns');
                return BM::setState('mbti_reg_tf');
            }

        };

//==================================================================================================

        static::$states['mbti_reg_tf'] = new class extends State
        {

            var $btn_type = 'inline';

            function start()
            {
                $this->base_message = "How to making <b>Desision</b>?\n\n";
                $this->base_message .= "<b>T</b>hinking\nor\n<b>F</b>eeling";
                $this->buttons[] = [
                    '[T] hinking' => ['callback_data' => 't'],
                    '[F] eeling'  => ['callback_data' => 'f']
                ];
            }

            function action()
            {
                BM::setCache('person', trim(BM::$input_btn), 'tf');
                return BM::setState('mbti_reg_jp');
            }

        };

//==================================================================================================

        static::$states['mbti_reg_jp'] = new class extends State
        {

            var $btn_type = 'inline';

            function start()
            {
                $this->base_message = "How to <b>Orientation</b>?\n\n";
                $this->base_message .= "<b>J</b>udging\nor\n<b>P</b>erceiving";
                $this->buttons[] = [
                    '[J] udging'    => ['callback_data' => 'j'],
                    '[P] erceiving' => ['callback_data' => 'p']
                ];
            }

            function action()
            {
                BM::setCache('person', trim(BM::$input_btn), 'jp');

                $data = BM::getFullCache('person');
                $pers = PhsycoPerson::create($data);
                PhsycoMbtiMode::create([
                    'person_id' => $pers->id,
                    'I'         => ($data['ie'] == 'i' ? 100 : 0),
                    'E'         => ($data['ie'] == 'e' ? 100 : 0),
                    'N'         => ($data['ns'] == 'n' ? 100 : 0),
                    'S'         => ($data['ns'] == 's' ? 100 : 0),
                    'T'         => ($data['tf'] == 't' ? 100 : 0),
                    'F'         => ($data['tf'] == 'f' ? 100 : 0),
                    'J'         => ($data['jp'] == 'j' ? 100 : 0),
                    'P'         => ($data['jp'] == 'p' ? 100 : 0),
                ]);
                BM::setCache('person', $pers, 'all');
                return BM::setState('mbti_keys');
            }

        };
//==================================================================================================

        static::$states['mbti_keys'] = new class extends State
        {

            var $btn_type = 'inline';

            function start()
            {
                $preson = BM::getCache('person', null, 'all');
                $this->base_message = "نام شخص: " . "<b>" . $preson->name . "</b>\n";
                $this->base_message .= "تیپ شخصیتی: " . "<b>" . $preson->mbti_mode_stream . "</b>\n";
                $this->base_message .= "لطفا یک بخش را انتخاب کن";
                foreach (PhsycoMbtiKey::all() as $item)
                    $this->buttons[][$item->title] = ['callback_data' => 'key_' . $item->id];
                $this->buttons[]['Main Menu'] = ['callback_data' => 'gobase'];
            }

            function action()
            {
                BM::setCache('selected_key', BM::$input_btn);
                return BM::setState('mbti_values');
            }

        };

//==================================================================================================

        static::$states['mbti_values'] = new class extends State
        {

            var $btn_type = 'inline';

            function start()
            {
                $selected = substr(BM::getCache('selected_key'), 4);
                $selected = PhsycoMbtiKey::findOrFail($selected);

                $value = $selected->getValueOfPerson(BM::getCache('person', null, 'all')['id']);
                $this->base_message = "<b>{$selected->title} | {$value->mbti_mode}</b>\n\n";
                if (isset($value->statement)) $this->base_message .= "<b>" . $value->statement . "</b>\n\n";
                $this->base_message .= $value->content;
                $this->buttons[]['Back'] = ['callback_data' => 'goback'];
                $this->buttons[]['Main Menu'] = ['callback_data' => 'gobase'];
            }

            function action()
            {
                BM::removeCache('selected_key');
                return BM::setState('mbti_keys');
            }

        };

//==================================================================================================
//==================================================================================================

        static::$states['mbti_data'] = new class extends State
        {

            var $btn_type = 'inline';

            function start()
            {
                $this->base_message .= "لطفا یک تیپ را انتخاب کن";

                $i = 0;
                $btn = [];
                foreach (\App\Models\PhsycoMbtiValue::tips() as $item){
                    $btn[$item] = ['callback_data' => 'tip_' . $item];
                    if($i % 2 == 0){
                        $i++;
                    }
                    else{
                        $this->buttons[] = $btn;
                        $i=0;
                        $btn = [];
                    }
                }
                if(!empty($btn)) $this->buttons[] = $btn;
                $this->buttons[]['Main Menu'] = ['callback_data' => 'gobase'];
            }

            function action()
            {
                BM::setCache('tip', substr(BM::$input_btn,4));
                return BM::setState('mbti_data_keys');
            }

        };

//==================================================================================================
        static::$states['mbti_data_keys'] = new class extends State
        {

            var $btn_type = 'inline';

            function start()
            {
                $tip = BM::getCache('tip', null);
                $this->base_message = "تیپ شخصیتی: " . "<b>" . $tip . "</b>\n";
                $this->base_message .= "لطفا یک بخش را انتخاب کن";
                foreach (PhsycoMbtiKey::all() as $item)
                    $this->buttons[][$item->title] = ['callback_data' => 'key_' . $item->id];
                $this->buttons[]['Main Menu'] = ['callback_data' => 'gobase'];
            }

            function action()
            {
                BM::setCache('selected_tip_key', substr(BM::$input_btn,4));
                return BM::setState('mbti_data_values');
            }

        };

//==================================================================================================

        static::$states['mbti_data_values'] = new class extends State
        {

            var $btn_type = 'inline';

            function start()
            {
                $tip = BM::getCache('tip');
                $val = BM::getCache('selected_tip_key');
                $selected = PhsycoMbtiKey::findOrFail($val);


                $value = PhsycoMbtiValue::where('mbti_mode',$tip)->first();

                $this->base_message = "<b>{$selected->title} | {$tip}</b>\n\n";
                if (isset($value->statement)) $this->base_message .= "<b>" . $value->statement . "</b>\n\n";
                $this->base_message .= $value->content;
                $this->buttons[]['Back'] = ['callback_data' => 'goback'];
                $this->buttons[]['Main Menu'] = ['callback_data' => 'gobase'];
            }

            function action()
            {
                BM::removeCache('selected_key');
                return BM::setState('mbti_data_keys');
            }

        };

//==================================================================================================

//==================================================================================================
/*        static::$states['new_state'] = new class extends State
        {

            function start()
            {
                $this->base_message = "Dynamic Message";
                //do somthing on start (before first message, at object creation)
            }

            function action()
            {
                return BM::backState();

                switch (BM::$input_text) {
                    case '/new':
                        return BM::setState('other_state');
                        break;
                    case '/back':
                        return BM::backState();
                        break;
                    case '/addMessage':
                        $this->addMessage("Hi!");
                        break;
                    default:
                        $this->addMessage("Unknown Order\n" . BM::$input_text);
                }
            }

        };*/


//==================================================================================================


//==================================================================================================
//==================================================================================================
//==================================================================================================
// [Sample State] ==================================================================================
        /*
                static::$states['new_state'] = new class extends State{

                    function start(){
                        $this->base_message = "Dynamic Message";
                        //do somthing on start (before first message, at object creation)
                    }
                    function action(){
                        return BM::backState();

                        switch(BM::$input_text){
                            case '/new':
                                return BM::setState('other_state');
                            break;
                            case '/back':
                                return BM::backState();
                            break;
                            case '/addMessage':
                                $this->addMessage("Hi!");
                            break;
                            default:
                                $this->addMessage("Unknown Order\n".BM::$input_text);
                        }
                    }

                };
        */
//==================================================================================================
    }
//==================================================================================================

}
