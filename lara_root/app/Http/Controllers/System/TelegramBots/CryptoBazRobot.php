<?php

namespace App\Http\Controllers\System\TelegramBots;

use App\Support\Telegram\BotMachine as BM;
use App\Support\Telegram\BotState;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Artisan;
use Symfony\Component\Console\Output\BufferedOutput;

class CryptoBazRobot extends BM
{
    const DEBUG_MODE = false;

    static $base_state = 'welcome';
    static $temp = null;
    static $bot_id = 3;

//==================================================================================================
    protected static function begin()
    {
        define('LANGUAGE', 'fa');
    }

    protected static function reboot_state()
    {
        if (BM::$input_text === '/start') {
            return null;
        }
        if (BM::$input_btn === 'gobase') {
            return null;
        }
        return false;
    }

//==================================================================================================
    protected static function config()
    {

        static::$states['welcome'] = new class extends BotState {
            var $btn_type = 'inline';

            function start()
            {
                $botUser = BM::getBotUser();

                if(!$botUser->user() || !$botUser->user()->hasRole('sysadmin@web|admin@web|sysadmin@api|admin@api|author_supervisor@web')){
                    $this->base_message = "سلام <b>" . BM::$input['from']['first_name'] . "</b> عزیز";
                    $this->base_message .= "\n\nمن روبوت cryptobaz.info".' هستم و هنوز در حال ساخته شدن هستم'.' 😊';
                    $this->base_message .= "\nهر زمان که آماده کار شدم خودم بهت اطلاع میدم".' 👋🏻';
                    return;
                }

                $role = Arr::first($botUser->user()->roles->pluck('name')->toArray());
                BM::setCache('role',$role);

                $this->base_message = "Hello <b>" . $botUser->user()->name . "</b>"." 🌹";
                $this->base_message .= "\n";

                if($role === 'sysadmin'){
                    $this->buttons[] = [
                        'DEV Server' => ['callback_data' => 'get_dev_sever'],
                        'MAIN Server' => ['callback_data' => 'get_main_sever'],
                    ];
                    $this->buttons[] = [
                        'OS Check' => ['callback_data' => 'get_os_check'],
                    ];
                    $this->buttons[]['Cloudflare'] = ['callback_data' => 'get_cloudflare'];
                }

                $this->buttons[]['Get DB Backup'] = ['callback_data' => 'get_db_backup'];
                $this->buttons[]['Get Uploads Backup'] = ['callback_data' => 'get_uploads_backup'];

                BM::setState('choose_server');

            }
        };

//==================================================================================================

        static::$states['choose_server'] = new class extends BotState {
            var $btn_type = 'inline';

            function start()
            {
                $role = BM::getCache('role');

                $this->base_message = "Choose an action";

                if($role === 'sysadmin'){
                    $this->buttons[] = [
                        'DEV Server' => ['callback_data' => 'get_dev_sever'],
                        'MAIN Server' => ['callback_data' => 'get_main_sever'],
                    ];
                    $this->buttons[] = [
                        'OS Check' => ['callback_data' => 'get_os_check'],
                    ];
                    $this->buttons[]['Cloudflare'] = ['callback_data' => 'get_cloudflare'];
                }
                $this->buttons[]['Get DB Backup'] = ['callback_data' => 'get_db_backup'];
                $this->buttons[]['Get Uploads Backup'] = ['callback_data' => 'get_uploads_backup'];

                if($role == 'admin'){
                    BM::setState('main_server_access');
                }

            }

            function action()
            {
                $this->update = true;

                switch (BM::$input_btn) {
                    case 'get_dev_sever':
                        BM::setState('dev_server_access');
                        break;
                    case 'get_cloudflare':
                        BM::setState('dev_cloudflare');
                        break;
                    case 'get_main_sever':
                        BM::setState('main_server_access');
                        break;
                    case 'get_os_check':
                        BM::setState('os_check');
                        break;

                    case 'get_db_backup':
                        $command = 'cd ../../main-website; php artisan mydb:backup -u -k';
                        $this->addMessage(BM::runShellCommand("DB Backup:\n",$command));
//                        $ret = new BufferedOutput();
//                        Artisan::call('mydb:backup -u -k',[],$ret);
//                        $this->addMessage("DB Backup Result:\n" . "<pre>" . $ret->fetch() . "</pre>");
                        break;
                    case 'get_uploads_backup':
                        $command = 'cd ../../main-website; php artisan myup:backup -u -k';
                        $this->addMessage(BM::runShellCommand("UPLOAD Backup:\n",$command));
//                        $ret = new BufferedOutput();
//                        Artisan::call('myup:backup -u -k',[],$ret);
//                        $this->addMessage("UPLOAD Backup Result:\n" . "<pre>" . $ret->fetch() . "</pre>");
                        break;
                }
            }
        };

//==================================================================================================

        static::$states['main_server_access'] = new class extends BotState {
            var $btn_type = 'inline';

            function start()
            {

                $role = BM::getCache('role');

                $this->base_message = "You are in 🟠<b>MAIN Server</b> control panel";
                $this->base_message .= "\n\nChoose an <b>action</b>";


                $buttons = [];
                $buttons['Git Branch State'] = ['callback_data' => 'git_state'];
                $buttons['Git Status'] = ['callback_data' => 'git_status'];
                $this->buttons[] = $buttons;

                $buttons = [];
                $buttons['Git Pull'] = ['callback_data' => 'git_pull'];
                $buttons['Git Stash'] = ['callback_data' => 'git_stash'];
                $this->buttons[] = $buttons;

                $this->buttons[]['🟢 DEV Server'] = ['callback_data' => 'get_dev_sever'];

                $this->buttons[]['Get Back'] = ['callback_data' => 'get_back'];


            }

            function action()
            {
                switch (BM::$input_btn) {
                    case 'get_back':
                        BM::setState('choose_server');
                        break;
                    case 'git_state':
                        $command = 'cd ../../main-website; git rev-list --format="%s | %ar" --max-count=2 '.'master';
                        $this->addMessage(BM::runShellCommand('Git Branch State',$command));
                        break;
                    case 'git_status':
                        $command = "cd ../../main-website; git status";
                        $this->addMessage(BM::runShellCommand('Git Status',$command));
                        break;
                    case 'git_stash':
                        $command = "cd ../../main-website; git stash";
                        $this->addMessage(BM::runShellCommand('Git Stash',$command));
                        break;
                    case 'git_pull':
                        $command = "cd ../../main-website; git pull";
                        $this->addMessage(BM::runShellCommand('Git Pull',$command));
                        break;
                    case 'get_dev_sever':
                        BM::setState('dev_server_access');
                        break;
                }
            }
        };

//==================================================================================================

        static::$states['dev_server_access'] = new class extends BotState {
            var $btn_type = 'inline';

            function start()
            {
                $role = BM::getCache('role');

                $this->base_message = "You are in 🟢<b>DEV Server</b> control panel";
                $this->base_message .= "\n\nChoose an <b>action</b>";


                $buttons = [];
                $buttons['Git Branch State'] = ['callback_data' => 'git_state'];
                $buttons['Git Status'] = ['callback_data' => 'git_status'];
                $this->buttons[] = $buttons;

                $buttons = [];
                $buttons['Git Pull'] = ['callback_data' => 'git_pull'];
                $buttons['Git Stash'] = ['callback_data' => 'git_stash'];
                $this->buttons[] = $buttons;

                $this->buttons[]['🟠 MAIN Server'] = ['callback_data' => 'get_main_sever'];

                $this->buttons[]['Get Back'] = ['callback_data' => 'get_back'];


            }

            function action()
            {
                switch (BM::$input_btn) {
                    case 'get_back':
                        BM::setState('choose_server');
                        break;
                    case 'git_state':
                        $command = 'cd ../../dev-website; git rev-list --format="%s | %ar" --max-count=2 '.'develop';
                        $this->addMessage(BM::runShellCommand('Git State',$command));
                        break;
                    case 'git_status':
                        $command = "cd ../../dev-website; git status";
                        $this->addMessage(BM::runShellCommand('Git Status',$command));
                        break;
                    case 'git_stash':
                        $command = "cd ../../dev-website; git stash";
                        $this->addMessage(BM::runShellCommand('Git Stash',$command));
                        break;
                    case 'git_pull':
                        $command = "cd ../../dev-website; git pull";
                        $this->addMessage(BM::runShellCommand('Git Pull',$command));
                        break;
                    case 'get_main_sever':
                        BM::setState('main_server_access');
                        break;
                }
            }
        };

//==================================================================================================

        static::$states['os_check'] = new class extends BotState {
            var $btn_type = 'inline';

            function start()
            {
                $this->base_message = "🔳 <b>OS</b> control panel";
                $this->base_message .= "\n\nChoose an <b>action</b>";


                $buttons = [];
                $buttons['W Check'] = ['callback_data' => 'get_w'];
                $buttons['Services Check'] = ['callback_data' => 'get_services'];
                $this->buttons[] = $buttons;

                $buttons = [];
                $buttons['PHP Check'] = ['callback_data' => 'get_php'];
                $buttons['Nginx Status'] = ['callback_data' => 'get_nginx_status'];
                $this->buttons[] = $buttons;

                $this->buttons[]['Get Back'] = ['callback_data' => 'get_back'];
            }

            function action()
            {
                switch (BM::$input_btn) {
                    case 'get_back':
                        BM::setState('choose_server');
                        break;
                    case 'get_w':
                        $this->addMessage(BM::runShellCommand('W','w'));
                        break;
                    case 'get_services':
                        $message = '';
                        foreach(['php-fpm','mariadb','redis','nginx'] as $service){
                            $message .= BM::runShellCommand(null,"systemctl is-active --quiet {$service} && echo {$service} is up \& running");
                            $message .= "\n============\n";
                        }
                        $this->addMessage("Services Result:\n" . $message);
                        break;
                    case 'get_php':
                        $this->addMessage(BM::runShellCommand('PHP Version','php -v'));
                        break;
                    case 'get_nginx_status':
                        $command = 'curl -sS --user '.config('app.basic_auth').' '.config('app.main_url').'/nginx_status_124';
                        $this->addMessage(BM::runShellCommand('Nginx Status',$command));
                        break;
                }
            }
        };

//==================================================================================================

        static::$states['dev_cloudflare'] = new class extends BotState {
            var $btn_type = 'inline';

            function start()
            {
                $this->base_message = "🛡 <b>Cloudflare</b> control panel";
                $this->base_message .= "\n\nChoose an <b>action</b>";


                $this->buttons[]['Clear All Cache'] = ['callback_data' => 'clear_all_cache'];

                $this->buttons[]['Get Back'] = ['callback_data' => 'get_back'];
            }

            function action()
            {
                switch (BM::$input_btn) {
                    case 'get_back':
                        BM::setState('choose_server');
                        break;
                    case 'clear_all_cache':
                        $ret = new BufferedOutput();
                        Artisan::call('cf:cache:clear',[],$ret);
                        $this->addMessage("Clearing All Cloudflare Caches:\n" . "<pre>" . $ret->fetch() . "</pre>");
                        break;
                }
            }

        };

//==================================================================================================


//==================================================================================================
//==================================================================================================
//==================================================================================================
// [Sample State] ==================================================================================
        /*
                static::$states['new_state'] = new class extends BotState{

                    var $btn_type = 'inline';

                    function start(){
                        $this->base_message = "Dynamic Message";
                        $this->buttons[]['button_title'] = ['callback_data'=>'button_data','condition'=>true];

                        //do somthing on start (before first message, at object creation)
                    }

                    function action(){
                        return BM::backState();

                        switch(BM::$input_text){
                            case '/new':
                                return BM::setState('other_state');
                            break;
                            case '/back':
                                return BM::backState();
                            break;
                            case '/addMessage':
                                $this->addMessage("Hi!");
                            break;
                            default:
                                $this->addMessage("Unknown Command\n".BM::$input_text);
                        }
                    }

                };
        */
//==================================================================================================
    }

}
