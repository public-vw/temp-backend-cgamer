<?php

namespace App\Http\Requests\Auth\API;

use App\Http\Requests\Admin\API\_CustomApiRequest;

class AuthRegisterRequest extends _CustomApiRequest
{
    protected $stopOnFirstFailure = true;

    public function rules()
    {
        return [
            'name' => 'required|min:3',
            'mobile' => 'required|min:10|regex:/^([0-9\s\-\+\(\)]*)$/|unique:users,mobile',
            'password' => 'required',
            'device_name' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Enter full name',
            'name.min' => 'Enter a valid full name',
            'mobile.required' => 'Enter your mobile number',
            'mobile.unique' => 'The mobile number already exists, use recovery password',
            'mobile.*' => 'Enter a valid mobile number',
            'password.required' => 'Enter password',
            'device_name.required' => 'Enter device name',
        ];
    }
}
