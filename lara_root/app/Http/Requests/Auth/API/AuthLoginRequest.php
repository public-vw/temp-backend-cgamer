<?php

namespace App\Http\Requests\Auth\API;

use App\Http\Requests\Admin\API\_CustomApiRequest;

class AuthLoginRequest extends _CustomApiRequest
{
    protected $stopOnFirstFailure = true;

    public function rules()
    {
        return [
            'mobile' => 'required|min:10|regex:/^([0-9\s\-\+\(\)]*)$/',
            'password' => 'required',
            'device_name' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'mobile.required' => 'Enter your mobile number',
            'mobile.*' => 'Enter a valid mobile number',
            'password.required' => 'Enter password',
            'device_name.required' => 'Enter device name',
        ];
    }
}
