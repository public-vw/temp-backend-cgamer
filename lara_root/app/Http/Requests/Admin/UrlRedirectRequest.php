<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\_CustomFormRequest;
use App\Models\UrlRedirect;

class UrlRedirectRequest extends _CustomFormRequest
{
    protected function prepareProperties()
    {
        $this->model = UrlRedirect::class;
        $this->mainEntity = $this->url_redirect;
    }
}
