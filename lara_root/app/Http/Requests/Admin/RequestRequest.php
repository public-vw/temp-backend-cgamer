<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\_CustomFormRequest;
use App\Models\Request;

class RequestRequest extends _CustomFormRequest
{
    protected function prepareProperties()
    {
        $this->model = Request::class;
        $this->mainEntity = $this->request;
    }
}
