<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\_CustomFormRequest;
use App\Models\SocialChannel;

class SocialChannelRequest extends _CustomFormRequest
{
    protected function prepareProperties()
    {
        $this->model = SocialChannel::class;
        $this->mainEntity = $this->social_channel;
    }
}
