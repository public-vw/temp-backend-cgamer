<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\_CustomFormRequest;
use App\Models\Author;

class AuthorRequest extends _CustomFormRequest
{
    protected function prepareProperties()
    {
        $this->model = Author::class;
        $this->mainEntity = $this->author;
    }
}
