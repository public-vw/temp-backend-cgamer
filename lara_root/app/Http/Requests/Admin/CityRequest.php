<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\_CustomFormRequest;
use App\Models\City;

class CityRequest extends _CustomFormRequest
{
    protected function prepareProperties()
    {
        $this->model = City::class;
        $this->mainEntity = $this->city;
    }
}
