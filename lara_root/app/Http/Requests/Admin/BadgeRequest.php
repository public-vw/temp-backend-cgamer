<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\_CustomFormRequest;
use App\Models\Badge;

class BadgeRequest extends _CustomFormRequest
{
    protected function prepareProperties()
    {
        $this->model = Badge::class;
        $this->mainEntity = $this->badge;
    }
}
