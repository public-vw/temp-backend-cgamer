<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class PasswordRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'curr_password'  => 'required',
            'new_password'      => 'required|min:8|confirmed',
        ];
    }
}
