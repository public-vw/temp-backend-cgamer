<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\_CustomFormRequest;
use App\Models\Menu;

class MenuRequest extends _CustomFormRequest
{
    protected function prepareProperties()
    {
        $this->model = Menu::class;
        $this->mainEntity = $this->menu;
    }
}
