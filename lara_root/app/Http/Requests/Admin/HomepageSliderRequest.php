<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\_CustomFormRequest;
use App\Models\HomepageSlider;

class HomepageSliderRequest extends _CustomFormRequest
{
    protected function prepareProperties()
    {
        $this->model = HomepageSlider::class;
        $this->mainEntity = $this->homepage_slider;
    }
}
