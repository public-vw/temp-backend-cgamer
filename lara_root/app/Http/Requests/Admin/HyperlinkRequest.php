<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\_CustomFormRequest;
use App\Models\Hyperlink;

class HyperlinkRequest extends _CustomFormRequest
{
    protected function prepareProperties()
    {
        $this->model = Hyperlink::class;
        $this->mainEntity = $this->hyperlink;
    }
}
