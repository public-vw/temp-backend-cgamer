<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\_CustomFormRequest;
use App\Models\Follow;

class FollowRequest extends _CustomFormRequest
{
    protected function prepareProperties()
    {
        $this->model = Follow::class;
        $this->mainEntity = $this->follow;
    }
}
