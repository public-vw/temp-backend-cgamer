<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\_CustomFormRequest;
use App\Models\ContactPhone;

class ContactPhoneRequest extends _CustomFormRequest
{
    protected function prepareProperties()
    {
        $this->model = ContactPhone::class;
        $this->mainEntity = $this->contact_phone;
    }
}
