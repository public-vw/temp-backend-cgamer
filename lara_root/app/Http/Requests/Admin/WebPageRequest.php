<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\_CustomFormRequest;
use App\Models\WebPage;

class WebPageRequest extends _CustomFormRequest
{
    protected function prepareProperties()
    {
        $this->model = WebPage::class;
        $this->mainEntity = $this->web_page;
    }
}
