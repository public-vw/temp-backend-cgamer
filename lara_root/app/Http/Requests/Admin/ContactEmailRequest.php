<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\_CustomFormRequest;
use App\Models\ContactEmail;

class ContactEmailRequest extends _CustomFormRequest
{
    protected function prepareProperties()
    {
        $this->model = ContactEmail::class;
        $this->mainEntity = $this->contact_email;
    }
}
