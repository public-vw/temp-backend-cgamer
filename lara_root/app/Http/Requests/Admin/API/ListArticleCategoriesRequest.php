<?php

namespace App\Http\Requests\Admin\API;

use App\Models\ArticleCategory;

class ListArticleCategoriesRequest extends _CustomApiRequest
{
    protected $stopOnFirstFailure = true;

    public function rules()
    {
        return [
            'id'        => 'sometimes|exists:article_categories,id',
            'term'      => 'sometimes',
            'parent_id' => 'sometimes|exists:article_categories,id',
            'status'    => 'sometimes|in:draft,requested,active',
        ];
    }
}
