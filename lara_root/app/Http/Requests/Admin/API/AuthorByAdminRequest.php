<?php

namespace App\Http\Requests\Admin\API;

class AuthorByAdminRequest extends _CustomApiRequest
{
    protected $stopOnFirstFailure = true;

    public function rules()
    {
        return [
            'name' => 'bail|string|required',
            'username' => 'bail|string|required|unique:users,username',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Full-name required',
            'username.required' => 'Social username required',
        ];
    }
}
