<?php

namespace App\Http\Requests\Admin\API;

class ChangeArticleCategoryByAdminRequest extends _CustomApiRequest
{
    protected $stopOnFirstFailure = true;

    public function rules()
    {
        return [
            'category_id' => 'required|exists:article_categories,id',
        ];
    }
}
