<?php

namespace App\Http\Requests\Admin\API;

class ChangeArticleAuthorByAdminRequest extends _CustomApiRequest
{
    protected $stopOnFirstFailure = true;

    public function rules()
    {
        return [
            'author_id' => 'required|exists:users,id',
        ];
    }
}
