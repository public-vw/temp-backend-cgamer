<?php

namespace App\Http\Requests\Admin\API;

class ChangeArticleLabelByAdminRequest extends _CustomApiRequest
{
    protected $stopOnFirstFailure = true;

    public function rules()
    {
        return [
            'label_id' => 'required|exists:article_labels,id',
        ];
    }
}
