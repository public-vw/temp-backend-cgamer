<?php

namespace App\Http\Requests\Admin\API;

class ListAuthorsRequest extends _CustomApiRequest
{
    protected $stopOnFirstFailure = true;

    public function rules()
    {
        return [
            'term' => 'sometimes',
            'term_type' => 'sometimes|in:name,username,mobile',
        ];
    }
}
