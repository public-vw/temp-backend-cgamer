<?php

namespace App\Http\Requests\Admin\API;

class LabelRequest extends _CustomApiRequest
{
    protected $stopOnFirstFailure = true;

    public function rules()
    {
        return [
            'group_title' => 'required',
            'title' => 'required',
            'order' => 'sometimes|numeric',
        ];
    }
}
