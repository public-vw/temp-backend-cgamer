<?php

namespace App\Http\Requests\Admin\API;

use App\Models\ArticleCategory;

class CategoryRequest extends _CustomApiRequest
{
    protected $stopOnFirstFailure = true;

    public function rules()
    {
        return ArticleCategory::$validation;
    }

    public function messages()
    {
        return [
            'title.required' => 'Title required',
        ];
    }
}
