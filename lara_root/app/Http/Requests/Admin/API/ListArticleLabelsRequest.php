<?php

namespace App\Http\Requests\Admin\API;

class ListArticleLabelsRequest extends _CustomApiRequest
{
    protected $stopOnFirstFailure = true;

    public function rules()
    {
        return [
            'id'        => 'sometimes|exists:article_labels,id',
            'group_id'  => 'sometimes|exists:article_label_groups,id',
            'term'      => 'sometimes',
        ];
    }
}
