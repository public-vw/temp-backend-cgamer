<?php

namespace App\Http\Requests\Admin\API;

class ListArticlesRequest extends _CustomApiRequest
{
    protected $stopOnFirstFailure = true;

    public function rules()
    {
        return [
            'id'          => 'sometimes|exists:articles,id',
            'category_id' => 'sometimes|exists:article_categories,id',
            'author_id'   => 'sometimes|exists:users,id',
            'status'      => 'sometimes|numeric',
            'term'        => 'sometimes',
            'term_type'   => 'sometimes|in:heading,content',
        ];
    }
}
