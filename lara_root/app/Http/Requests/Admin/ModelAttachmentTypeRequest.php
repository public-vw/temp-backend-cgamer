<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\_CustomFormRequest;
use App\Models\ModelAttachmentType;

class ModelAttachmentTypeRequest extends _CustomFormRequest
{
    protected function prepareProperties()
    {
        $this->model = ModelAttachmentType::class;
        $this->mainEntity = $this->model_attachment_type;
    }
}
