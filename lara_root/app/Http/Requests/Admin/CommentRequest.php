<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\_CustomFormRequest;
use App\Models\Comment;

class CommentRequest extends _CustomFormRequest
{
    protected function prepareProperties()
    {
        $this->model = Comment::class;
        $this->mainEntity = $this->comment;
    }
}
