<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\_CustomFormRequest;
use App\Models\Country;

class CountryRequest extends _CustomFormRequest
{
    protected function prepareProperties()
    {
        $this->model = Country::class;
        $this->mainEntity = $this->country;
    }
}
