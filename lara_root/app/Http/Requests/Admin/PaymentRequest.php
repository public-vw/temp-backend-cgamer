<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\_CustomFormRequest;
use App\Models\Payment;

class PaymentRequest extends _CustomFormRequest
{
    protected function prepareProperties()
    {
        $this->model = Payment::class;
        $this->mainEntity = $this->payment;
    }
}
