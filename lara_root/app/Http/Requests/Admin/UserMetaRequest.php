<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\_CustomFormRequest;
use App\Models\UserMeta;

class UserMetaRequest extends _CustomFormRequest
{
    protected function prepareProperties()
    {
        $this->model = UserMeta::class;
        $this->mainEntity = $this->user_meta;
    }
}
