<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\_CustomFormRequest;
use App\Models\Region;

class RegionRequest extends _CustomFormRequest
{
    protected function prepareProperties()
    {
        $this->model = Region::class;
        $this->mainEntity = $this->region;
    }
}
