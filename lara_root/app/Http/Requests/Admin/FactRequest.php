<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\_CustomFormRequest;
use App\Models\Fact;

class FactRequest extends _CustomFormRequest
{
    protected function prepareProperties()
    {
        $this->model = Fact::class;
        $this->mainEntity = $this->fact;
    }
}
