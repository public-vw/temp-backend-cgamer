<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\_CustomFormRequest;
use App\Models\Attachment;

class AttachmentRequest extends _CustomFormRequest
{
    protected function prepareProperties()
    {
        $this->model = Attachment::class;
        $this->mainEntity = $this->attachment;
    }
}
