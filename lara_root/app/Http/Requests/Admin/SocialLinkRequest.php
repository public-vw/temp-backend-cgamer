<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\_CustomFormRequest;
use App\Models\SocialLink;

class SocialLinkRequest extends _CustomFormRequest
{
    protected function prepareProperties()
    {
        $this->model = SocialLink::class;
        $this->mainEntity = $this->social_link;
    }
}
