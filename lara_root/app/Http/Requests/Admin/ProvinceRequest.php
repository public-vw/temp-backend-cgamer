<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\_CustomFormRequest;
use App\Models\Province;

class ProvinceRequest extends _CustomFormRequest
{
    protected function prepareProperties()
    {
        $this->model = Province::class;
        $this->mainEntity = $this->province;
    }
}
