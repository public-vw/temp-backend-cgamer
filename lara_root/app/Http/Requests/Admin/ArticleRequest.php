<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\_CustomFormRequest;
use App\Models\Article;

class ArticleRequest extends _CustomFormRequest
{
    protected function prepareProperties()
    {
        $this->model = Article::class;
        $this->mainEntity = $this->article;
    }
}
