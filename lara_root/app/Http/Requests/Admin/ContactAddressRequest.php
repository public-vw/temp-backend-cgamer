<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\_CustomFormRequest;
use App\Models\ContactAddress;

class ContactAddressRequest extends _CustomFormRequest
{
    protected function prepareProperties()
    {
        $this->model = ContactAddress::class;
        $this->mainEntity = $this->contact_address;
    }
}
