<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\_CustomFormRequest;
use App\Models\StarRate;

class StarRateRequest extends _CustomFormRequest
{
    protected function prepareProperties()
    {
        $this->model = StarRate::class;
        $this->mainEntity = $this->star_rate;
    }
}
