<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\_CustomFormRequest;
use App\Models\AttachmentType;

class AttachmentTypeRequest extends _CustomFormRequest
{
    protected function prepareProperties()
    {
        $this->model = AttachmentType::class;
        $this->mainEntity = $this->attachment_type;
    }
}
