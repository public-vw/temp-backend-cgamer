<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\_CustomFormRequest;
use App\Models\Review;

class ReviewRequest extends _CustomFormRequest
{
    protected function prepareProperties()
    {
        $this->model = Review::class;
        $this->mainEntity = $this->review;
    }
}
