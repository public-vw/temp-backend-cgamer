<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\_CustomFormRequest;
use App\Models\HomepageFuture;

class HomepageFutureRequest extends _CustomFormRequest
{
    protected function prepareProperties()
    {
        $this->model = HomepageFuture::class;
        $this->mainEntity = $this->homepage_future;
    }
}
