<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\_CustomFormRequest;
use App\Models\ArticleCategory;

class ArticleCategoryRequest extends _CustomFormRequest
{
    protected function prepareProperties()
    {
        $this->model = ArticleCategory::class;
        $this->mainEntity = $this->article_category;
    }
}
