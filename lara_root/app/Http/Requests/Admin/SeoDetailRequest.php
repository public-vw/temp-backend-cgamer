<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\_CustomFormRequest;
use App\Models\SeoDetail;

class SeoDetailRequest extends _CustomFormRequest
{
    protected function prepareProperties()
    {
        $this->model = SeoDetail::class;
        $this->mainEntity = $this->seo_detail;
    }
}
