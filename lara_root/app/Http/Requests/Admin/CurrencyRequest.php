<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\_CustomFormRequest;
use App\Models\Currency;

class CurrencyRequest extends _CustomFormRequest
{
    protected function prepareProperties()
    {
        $this->model = Currency::class;
        $this->mainEntity = $this->currency;
    }
}
