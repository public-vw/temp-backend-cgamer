<?php

namespace App\Http\Requests\Sysadmin;

use App\Http\Requests\_CustomFormRequest;
use App\Models\BotConnection;

class BotConnectionRequest extends _CustomFormRequest
{
    protected function prepareProperties()
    {
        $this->model = BotConnection::class;
        $this->mainEntity = $this->bot_connection;
    }
}
