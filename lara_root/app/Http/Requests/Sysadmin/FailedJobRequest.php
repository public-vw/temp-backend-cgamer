<?php

namespace App\Http\Requests\Sysadmin;

use App\Http\Requests\_CustomFormRequest;
use App\Models\FailedJob;

class FailedJobRequest extends _CustomFormRequest
{
    protected function prepareProperties()
    {
        $this->model = FailedJob::class;
        $this->mainEntity = $this->failed_job;
    }
}
