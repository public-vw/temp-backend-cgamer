<?php

namespace App\Http\Requests\Sysadmin;

use App\Http\Requests\_CustomFormRequest;
use App\Models\MenuPlaceholder;

class MenuPlaceholderRequest extends _CustomFormRequest
{
    protected function prepareProperties()
    {
        $this->model = MenuPlaceholder::class;
        $this->mainEntity = $this->menu_placeholder;
    }
}
