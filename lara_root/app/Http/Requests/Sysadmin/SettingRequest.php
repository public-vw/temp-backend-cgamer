<?php

namespace App\Http\Requests\Sysadmin;

use App\Http\Requests\_CustomFormRequest;
use App\Models\Setting;

class SettingRequest extends _CustomFormRequest
{
    protected function prepareProperties()
    {
        $this->model = Setting::class;
        $this->mainEntity = $this->setting;
    }
}
