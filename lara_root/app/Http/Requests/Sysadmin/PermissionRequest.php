<?php

namespace App\Http\Requests\Sysadmin;

use Illuminate\Foundation\Http\FormRequest;

class PermissionRequest extends FormRequest
{
    protected $rules = [
        '*'    => 'bail|string',
        'name' => 'required|unique:permissions,name',
    ];

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = $this->renderRules();

        if (isset($this->permission->id)) {
            foreach ($rules as $key => $rule) {
                if (strpos($rule, 'unique') !== FALSE) {
                    $rules[$key] .= ",{$this->permission->id},id";
                }
            }
        }

        return $rules;
    }

    protected function renderRules()
    {
        $rules = $this->rules;

        // removes * rule for safety (resolves input injection)
        $trailing_rules = $rules['*'] ?? '';
        unset($rules['*']);

        // adds trailing rules to each rule
        foreach ($rules as &$rule) {
            $rule = $trailing_rules . '|' . $rule;
        }

        return $rules;
    }
}
