<?php

namespace App\Http\Requests\Sysadmin;

use App\Http\Requests\_CustomFormRequest;
use App\Models\Structure;

class StructureRequest extends _CustomFormRequest
{
    protected function prepareProperties()
    {
        $this->model = Structure::class;
        $this->mainEntity = $this->structure;
    }
}
