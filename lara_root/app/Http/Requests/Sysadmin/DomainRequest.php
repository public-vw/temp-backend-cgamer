<?php

namespace App\Http\Requests\Sysadmin;

use App\Http\Requests\_CustomFormRequest;
use App\Models\Domain;

class DomainRequest extends _CustomFormRequest
{
    protected function prepareProperties()
    {
        $this->model = Domain::class;
        $this->mainEntity = $this->domain;
    }
}
