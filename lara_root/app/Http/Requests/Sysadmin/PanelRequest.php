<?php

namespace App\Http\Requests\Sysadmin;

use App\Http\Requests\_CustomFormRequest;
use App\Models\Panel;

class PanelRequest extends _CustomFormRequest
{
    protected function prepareProperties()
    {
        $this->model = Panel::class;
        $this->mainEntity = $this->panel;
    }
}
