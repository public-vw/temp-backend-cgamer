<?php

namespace App\Http\Requests\Sysadmin;

use App\Http\Requests\_CustomFormRequest;
use App\Models\SiteBrand;

class SiteBrandRequest extends _CustomFormRequest
{
    protected function prepareProperties()
    {
        $this->model = SiteBrand::class;
        $this->mainEntity = $this->site_brand;
    }
}
