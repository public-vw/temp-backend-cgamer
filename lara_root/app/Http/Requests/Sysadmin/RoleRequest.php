<?php

namespace App\Http\Requests\Sysadmin;

use Illuminate\Foundation\Http\FormRequest;

class RoleRequest extends FormRequest
{
    protected $rules = [
        '*'        => 'bail|string',
        'name'     => 'required|unique:roles,name',
        'panel_id' => 'nullable|exists:panels,id',
    ];

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = $this->renderRules();

        if (isset($this->role->id)) {
            foreach ($rules as $key => $rule) {
                if (strpos($rule, 'unique') !== FALSE) {
                    $rules[$key] .= ",{$this->role->id},id";
                }
            }
        }

        return $rules;
    }

    protected function renderRules()
    {
        $rules = $this->rules;

        // removes * rule for safety (resolves input injection)
        $trailing_rules = $rules['*'] ?? '';
        unset($rules['*']);

        // adds trailing rules to each rule
        foreach ($rules as &$rule) {
            $rule = $trailing_rules . '|' . $rule;
        }

        return $rules;
    }
}
