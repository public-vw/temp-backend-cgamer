<?php

namespace App\Http\Requests\Sysadmin;

use App\Http\Requests\_CustomFormRequest;
use App\Models\User;

class UserRequest extends _CustomFormRequest
{
    protected function prepareProperties()
    {
        $this->model = User::class;
        $this->mainEntity = $this->user;
    }
}
