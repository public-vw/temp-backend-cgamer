<?php

namespace App\Http\Requests\Sysadmin;

use App\Http\Requests\_CustomFormRequest;
use App\Models\SettingGroup;

class SettingGroupRequest extends _CustomFormRequest
{
    protected function prepareProperties()
    {
        $this->model = SettingGroup::class;
        $this->mainEntity = $this->setting_group;
    }
}
