<?php

namespace App\Http\Transformers;

use App\Models\ArticleCategory;
use League\Fractal\TransformerAbstract;

class Select2ArticleCategoryTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        //
    ];

    protected $availableIncludes = [
        //
    ];


    public function transform(ArticleCategory $articleCategory)
    {
        return [

            'id'   => encrypt("article_category:{$articleCategory->id}"),
            'text' => $articleCategory->heading,
        ];
    }
}
