<?php

namespace App\Http\Transformers;

use App\Models\UrlRedirect;
use League\Fractal\TransformerAbstract;

class UrlRedirectTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        //
    ];

    protected $availableIncludes = [
        //
    ];

    public function transform(UrlRedirect $urlRedirect)
    {
        return [
            'id'    => $urlRedirect->id,
            'title' => $urlRedirect->title,
        ];
    }
}
