<?php

namespace App\Http\Transformers;

use App\Models\ArticleCategory;
use App\Models\ArticleLabel;
use League\Fractal\TransformerAbstract;

class ArticleLabelTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        //
    ];

    protected $availableIncludes = [
        //
    ];

    public function transform(ArticleLabel $label)
    {
        return [
            'id'    => $label->id,
            'group' => [
                'id'    => $label->group_id,
                'title' => $label->group->title,
                'order' => $label->group->order,
            ],
            'title' => $label->title,
            'order' => $label->order,
        ];
    }
}
