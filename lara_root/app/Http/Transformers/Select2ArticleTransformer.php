<?php

namespace App\Http\Transformers;

use App\Models\Article;
use League\Fractal\TransformerAbstract;

class Select2ArticleTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        //
    ];

    protected $availableIncludes = [
        //
    ];


    public function transform(Article $article)
    {
        return [

            'id'      => encrypt("article:{$article->id}"),
            'text' => $article->heading,
        ];
    }
}
