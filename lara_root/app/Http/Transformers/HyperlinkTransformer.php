<?php

namespace App\Http\Transformers;

use App\Models\Hyperlink;
use League\Fractal\TransformerAbstract;

class HyperlinkTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        //
    ];

    protected $availableIncludes = [
        //
    ];

    public function transform(Hyperlink $hyperlink)
    {
        return [
            'id'    => $hyperlink->id,
            'title' => $hyperlink->title,
        ];
    }
}
