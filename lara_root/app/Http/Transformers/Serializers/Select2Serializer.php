<?php

namespace App\Http\Transformers\Serializers;

use League\Fractal\Serializer\SerializerAbstract;

class Select2Serializer extends SerializerAbstract
{
    public function collection($resourceKey, array $data)
    {
        return ['results' => $data];
    }

    public function item($resourceKey, array $data)
    {
        return $data;
    }

    public function null()
    {
        return [];
    }

    public function includedData(\League\Fractal\Resource\ResourceInterface $resource, array $data)
    {
        return $data;
    }

    public function meta(array $meta)
    {
        if (empty($meta)) {
            return [];
        }

        return ['meta' => $meta];
    }

    public function paginator(\League\Fractal\Pagination\PaginatorInterface $paginator)
    {
        $currentPage = (int) $paginator->getCurrentPage();
        $lastPage = (int) $paginator->getLastPage();

        $pagination = [
            'total' => (int) $paginator->getTotal(),
            'count' => (int) $paginator->getCount(),
            'per_page' => (int) $paginator->getPerPage(),
            'current_page' => $currentPage,
            'total_pages' => $lastPage,
        ];

        $pagination['links'] = [];

        if ($currentPage > 1) {
            $pagination['links']['previous'] = $paginator->getUrl($currentPage - 1);
        }

        if ($currentPage < $lastPage) {
            $pagination['links']['next'] = $paginator->getUrl($currentPage + 1);
        }

        if (empty($pagination['links'])) {
            $pagination['links'] = (object) [];
        }

        return ['pagination' => $pagination];
    }

    public function cursor(\League\Fractal\Pagination\CursorInterface $cursor)
    {
        $cursor = [
            'current' => $cursor->getCurrent(),
            'prev' => $cursor->getPrev(),
            'next' => $cursor->getNext(),
            'count' => (int) $cursor->getCount(),
        ];

        return ['cursor' => $cursor];
    }
}
