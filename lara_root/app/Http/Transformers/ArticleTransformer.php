<?php

namespace App\Http\Transformers;

use App\Models\Article;
use App\Models\Attachment;
use League\Fractal\TransformerAbstract;

class ArticleTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        //
    ];

    protected $availableIncludes = [
        //
    ];

    public function transform(Article $article)
    {
        $attachments = $this->getAttachments($article);

        return [
            'id'          => $article->id,
            'author'      => $article->author_id ? [
                'id'    => $article->author_id,
                'article_limit' => $article->author->article_limit,
            ] : null,
            'category'    => $article->category_id ? [
                'id'    => $article->category_id,
                'title' => $article->category->title,
            ] : null,
            'heading'     => $article->heading,
            'attachments' => [
                'count'   => count($attachments),
                'details' => $attachments,
            ],
            'status'      => $article->status . "|" . config("enums.articles_status.{$article->status}"),
        ];
    }

    protected function getAttachments(Article $article)
    {
        $re = '/\[img ([\s\S]+?)]/m';

        $attachments = [];
        $matches = null;
        preg_match_all($re, $article->content, $matches, PREG_SET_ORDER);

        foreach ($matches as $match) {
            $id = (int)$match[1];
            $attachment = Attachment::find($id);
            $attachments[] = !$attachment?"record {$id} not found in DB!":[
                'id'           => $attachment->id,
                'storage_disk' => $attachment->storage_disk,
                'file_name'    => $attachment->file_name,
                'file_ext'     => $attachment->file_ext,
                'mimetype'     => $attachment->mimetype,
                'url'          => $attachment->url,
                'md5'          => $attachment->md5,
                'type'         => [
                    'title'      => $attachment->type->title,
                    'properties' => $attachment->type->properties_limit,
                ],
                'temporary'    => $attachment->temporary ? 'yes' : 'no',
            ];
        }

        return $attachments;
    }
}
