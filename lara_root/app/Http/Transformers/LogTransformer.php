<?php

namespace App\Http\Transformers;

use App\Models\Log;
use League\Fractal\TransformerAbstract;

class LogTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        //
    ];

    protected $availableIncludes = [
        //
    ];

    public function transform(Log $log)
    {
        return [
            'id'    => $log->id,
            'title' => $log->title,
        ];
    }
}
