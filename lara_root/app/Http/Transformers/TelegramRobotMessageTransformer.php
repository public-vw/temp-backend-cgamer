<?php

namespace App\Http\Transformers;

use App\Models\TelegramRobotMessage;
use League\Fractal\TransformerAbstract;

class TelegramRobotMessageTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        //
    ];

    protected $availableIncludes = [
        //
    ];

    public function transform(TelegramRobotMessage $telegramRobotMessage)
    {
        return [
            'id'    => $telegramRobotMessage->id,
            'title' => $telegramRobotMessage->title,
        ];
    }
}
