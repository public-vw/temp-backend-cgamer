<?php

namespace App\Http\Transformers;

use App\Models\BotConnection;
use League\Fractal\TransformerAbstract;

class BotConnectionTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        //
    ];

    protected $availableIncludes = [
        //
    ];

    public function transform(BotConnection $botConnection)
    {
        return [
            'id'    => $botConnection->id,
            'title' => $botConnection->title,
        ];
    }
}
