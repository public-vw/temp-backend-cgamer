<?php

namespace App\Http\Transformers;

use App\Models\Author;
use League\Fractal\TransformerAbstract;

class AuthorTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        //
    ];

    protected $availableIncludes = [
        //
    ];

    public function transform(Author $author)
    {
        return [
            'id'       => $author->id,
            'nickname' => $author->user->nickname,
        ];
    }
}
