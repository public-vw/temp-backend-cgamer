<?php

namespace App\Http\Transformers;

use App\Models\HomepageFuture;
use League\Fractal\TransformerAbstract;

class HomepageFutureTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        //
    ];

    protected $availableIncludes = [
        //
    ];

    public function transform(HomepageFuture $homepageFuture)
    {
        return [
            'id'    => $homepageFuture->id,
            'title' => $homepageFuture->title,
        ];
    }
}
