<?php

namespace App\Http\Transformers;

use App\Models\Payment;
use League\Fractal\TransformerAbstract;

class PaymentTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        //
    ];

    protected $availableIncludes = [
        //
    ];

    public function transform(Payment $payment)
    {
        return [
            'id'    => $payment->id,
            'title' => $payment->title,
        ];
    }
}
