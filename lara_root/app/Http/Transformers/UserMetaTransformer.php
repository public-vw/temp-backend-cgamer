<?php

namespace App\Http\Transformers;

use App\Models\UserMeta;
use League\Fractal\TransformerAbstract;

class UserMetaTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        //
    ];

    protected $availableIncludes = [
        //
    ];

    public function transform(UserMeta $userMeta)
    {
        return [
            'id'    => $userMeta->id,
            'title' => $userMeta->title,
        ];
    }
}
