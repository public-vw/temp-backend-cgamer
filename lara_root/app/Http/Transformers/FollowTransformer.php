<?php

namespace App\Http\Transformers;

use App\Models\Follow;
use League\Fractal\TransformerAbstract;

class FollowTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        //
    ];

    protected $availableIncludes = [
        //
    ];

    public function transform(Follow $follow)
    {
        return [
            'id'    => $follow->id,
            'title' => $follow->title,
        ];
    }
}
