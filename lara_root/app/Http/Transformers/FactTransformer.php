<?php

namespace App\Http\Transformers;

use App\Models\Fact;
use League\Fractal\TransformerAbstract;

class FactTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        //
    ];

    protected $availableIncludes = [
        //
    ];

    public function transform(Fact $fact)
    {
        return [
            'id'    => $fact->id,
            'title' => $fact->title,
        ];
    }
}
