<?php

namespace App\Http\Transformers;

use App\Models\Badge;
use League\Fractal\TransformerAbstract;

class BadgeTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        //
    ];

    protected $availableIncludes = [
        //
    ];

    public function transform(Badge $badge)
    {
        return [
            'id'    => $badge->id,
            'title' => $badge->title,
        ];
    }
}
