<?php

namespace App\Http\Transformers;

use App\Models\ArticleCategory;
use League\Fractal\TransformerAbstract;

class ArticleCategoryTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        //
    ];

    protected $availableIncludes = [
        //
    ];

    public function transform(ArticleCategory $category)
    {
        return [
            'id'     => $category->id,
            'parent' => $category->parent_id ? [
                'id'    => $category->parent_id,
                'title' => $category->parent->title ?? '',
            ] : null,
            'title'  => $category->title,
            'status' => $category->status . "|" . config("enums.article_categories_status.{$category->status}"),
        ];
    }
}
