#!/bin/sh
tail --bytes=+4 $1 > tmp
chown bigsho.psacln tmp
chmod 644 tmp
rm -f $1
cat tmp | tr -d '\r' > $1 
rm -f tmp

