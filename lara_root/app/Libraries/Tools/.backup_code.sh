#!/bin/sh
tar czf wholesite.$(date +%Y-%m-%d_%H%M%S).tar.gz \
    app \
    config \
    database \
    dbbackups \
    resources \
    storage/data \
    .env \

