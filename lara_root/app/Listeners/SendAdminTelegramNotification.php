<?php

namespace App\Listeners;

use App\Event\_TelegramAlert;
use App\Models\User;

class SendAdminTelegramNotification
{
    protected $botid;
    protected \App\Support\Telegram\TelegramBotOperator $chat_op;

    public function __construct($botid = 1)
    {
        $this->chat_op = new \App\Support\Telegram\TelegramBotOperator($botid);
    }

    public function handle(_TelegramAlert $event)
    {
/*        alertAdminTelegram(
            "{$event->eventHeader['icon']} <b>{$event->eventHeader['title']} @ "
            . config('app.telegram_title', '') . '</b>'
            . "\n" . "<b>Date :</b> " . getFormedDate($event->time)
            . "\n" . "<b>Time :</b> " . $event->time->format('h:i:s A')
            . "\n" . $event->details
            , []
        );*/

        foreach ($event->targetUsersIDs as $id) {
            $user = User::find($id);
            if(!$user) continue;

            $this->sendTelegramAlert($user->telegram_uid, $this->makeMessage($event));
        }
    }

    protected function makeMessage($event){
        $message = "{$event->eventHeader['icon']} <b>{$event->eventHeader['title']} @ ";
        $message .= config('app.telegram_title', '') . '</b>';
        $message .= "\n" . "<b>Date :</b> " . getFormedDate($event->time);
        $message .= "\n" . "<b>Time :</b> " . $event->time->format('h:i:s A');
        $message .= "\n" . $event->details;

        return trim($message);
    }

    protected function sendTelegramAlert($telegram_uid, $message, $commands = [], $html_mark = 'HTML'){
        $this->chat_op->sendMessageInlineMarkup($telegram_uid, trim($message), $commands, $html_mark, 1);
    }
}
