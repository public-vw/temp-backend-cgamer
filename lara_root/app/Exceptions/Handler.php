<?php

namespace App\Exceptions;

use App\Jobs\CsfLogOnExceptions;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cache;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            $code = md5($e->getFile().'.'.$e->getLine());
            if(cache("error_$code",false)){
                cache(["error_$code" => true],10);
                $this->report($e);
            }
        });
    }

    public function report(Throwable $exception)
    {
        if ($exception instanceof NotFoundHttpException) {

            $statusCode = $exception->getStatusCode();
            $uri = parse_url(url()->current())['path'];
            $ip = get_ip();
            $now = Carbon::now()->format('Ymd-His');

            dispatch(new CsfLogOnExceptions($ip, $uri, $statusCode, $now));

        }

//        #bypass log to db
//        parent::report($exception);
//        return;

        if(function_exists('mylog')){
            $log = mylog('Exception Error','error',
                '<b>File:</b> '.substr($exception->getFile(),0,150).(strlen($exception->getFile())>150?' ...':'')
                ."\n<b>Line:</b> ".substr($exception->getLine(),0,20).(strlen($exception->getLine())>20?' ...':'')
                ."\n<b>Exception:</b> ".substr($exception->getMessage(),0,1000).(strlen($exception->getMessage())>1000?' ...':'')
                ,false
            );
        }

        parent::report($exception);
    }
}
