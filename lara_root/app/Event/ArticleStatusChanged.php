<?php

namespace App\Event;

use App\Models\Article;
use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ArticleStatusChanged extends _TelegramAlert implements ShouldQueue
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    const EVENT_TITLE = "Article Status Changed";
    const EVENT_ICON = "🧬";
    public array $targetUsersIDs = [1,2];

    protected User $user;
    public Article $article;
    public string $oldStatus;

    public function __construct(User $user, Article $article, $oldStatus)
    {
        $this->user = $user;
        $this->article = $article;
        $this->oldStatus = $oldStatus;

        parent::__construct();
    }

    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }

    protected function setDetails(){
        $this->details = "<b>User :</b> {$this->user->nickname}@{$this->user->username}";
        $this->details .= "\n" . "<b>Article :</b> {$this->article->title}[{$this->article->id}]";
        $this->details .= "\n";
        $this->details .= "\n" . "<b>Old Status :</b> ";
        $this->details .= config('enums.articles_status.'.$this->oldStatus);
        $this->details .= "\n" . "<b>New Status :</b> ";
        $this->details .= config('enums.articles_status.'.$this->article->status);
    }
}
