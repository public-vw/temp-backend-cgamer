<?php

namespace App\Event;

use App\Models\Article;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ArticleReversioned extends _TelegramAlert implements ShouldQueue
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    const EVENT_TITLE = "Reversion Article";
    const EVENT_ICON = "🧬";
    public array $targetUsersIDs = [1,2];

    protected User $user;
    protected  Article $article;

    public function __construct(User $user, Article $article)
    {
        $this->user = $user;
        $this->article = $article;

        parent::__construct();
    }

    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }

    protected function setDetails(){
        $this->details = "<b>User :</b> {$this->user->nickname}@{$this->user->username}";
        $this->details .= "\n" . "<b>Master Article :</b> {$this->article->master->title}[{$this->article->master->id}]";
        $this->details .= "\n" . "<b>Status :</b> ";
        $this->details .= config('enums.articles_status.'.$this->article->master->status);
        $this->details .= "\n";
        $this->details .= "\n" . "<b>New Article :</b> {$this->article->title}[{$this->article->id}]";
        $this->details .= "\n" . "<b>Status :</b> ";
        $this->details .= config('enums.articles_status.'.$this->article->status);
    }
}
