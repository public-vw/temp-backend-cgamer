<?php

namespace App\Event;

use Carbon\Carbon;

abstract class _TelegramAlert
{
    const EVENT_TITLE = '';
    const EVENT_ICON = '';

    public Carbon $time;
    public array $eventHeader;
    public string $details;

    public array $targetUsersIDs = [1];

    public function __construct()
    {
        $this->time = Carbon::now();
        $this->eventHeader['title'] = static::EVENT_TITLE;
        $this->eventHeader['icon'] = static::EVENT_ICON;

        $this->setDetails();
    }

    abstract protected function setDetails();
}
