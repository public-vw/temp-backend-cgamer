<?php

namespace App\Event;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UserEntered extends _TelegramAlert implements ShouldQueue
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    const EVENT_TITLE = "Entrance";
    const EVENT_ICON = "👤";
    public array $targetUsersIDs = [1,2];

    protected User $user;
    protected string $channel;

    public function __construct(string $title, User $user, $channel)
    {
        $this->user = $user;
        $this->channel = $channel;

        parent::__construct();
    }

    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }

    protected function setDetails(){
        $this->details = "<b>Name :</b> " . $this->user->name;
        $this->details .= "\n" . "<b>Channel :</b> " . $this->channel;
    }
}
