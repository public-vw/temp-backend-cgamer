<?php

namespace App\Providers;

use App\Models\Article;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class BladeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::if('multirole',function(){
            return (auth()->check() && auth()->user()->roles->pluck('name')->count() > 1);
        });

        Blade::if('author',function(Article $article){
            return (
                auth()->check()
                && (
                    auth()->user()->role('admin@web')
                    || (
                        auth()->user()->role('author@web')
                        && (
                                $article->author->user->isSameAsCurrent()
                                || $article->author->user->isChildOfCurrent()
                            )
                        )
                    )
                );
        });

        Blade::directive('includeRelativeSection',function($expression){
            list($expression, $params) = explode(',',$expression);
            $expression = preg_replace("/[\"\']/",'',$expression);
            list($view_root, $extra_table) = explode('.',$expression);
            $expression = "\"$view_root.items.$extra_table.inline_section.base\",$params\n]";

            return "<?php echo \$__env->make({$expression}, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>";
        });

        Blade::directive('compo', function ($expression) {
            if (Str::startsWith($expression, '(')) {
                $expression = substr($expression, 1, -1);
            }

            $expression = preg_replace(
                '/'.preg_quote('[', '/').'/',
                "[ 'rnd' => random_int(101, 900).'-'.random_int(101, 900),",
                $expression,
                1);

            return "<?php echo \$__env->make({$expression}, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>";
        });
    }
}
