<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    public const HOME = '/auth/switch-panel';

    protected $namespace = '\\App\\Http\\Controllers';

    private function sysadminWebRoutes($nsFolder): void
    {
        $namespace = $this->namespace . '\\' . $nsFolder;

        Route::middleware('web', 'auth', 'role:sysadmin@web')
            ->namespace($namespace)
            ->name('sysadmin.')
            ->prefix(config('routes.sysadmin.web'))
            ->group(base_path('routes/sysadmin/web/web.php'));
    }
    private function sysadminApiRoutes($nsFolder): void
    {
        $namespace = $this->namespace . '\\' . $nsFolder;

        Route::middleware('api', 'auth:api', 'auth:sanctum', 'role:sysadmin@api')
            ->namespace($namespace . "\\API\\V1")
            ->name('sysadmin.api.v1.')
            ->prefix(config('routes.sysadmin.api'))
            ->group(base_path('routes/sysadmin/api/v1/api.php'));
    }

    #copy this for <NewPanel>
    private function adminWebRoutes($nsFolder): void
    {
        $namespace = $this->namespace . '\\' . $nsFolder;

        Route::middleware('web', 'auth', 'role:admin@web|author_supervisor@web')
            ->namespace($namespace)
            ->name('admin.')
            ->prefix(config('routes.admin.web'))
            ->group(base_path('routes/admin/web/web.php'));
    }
    private function adminApiRoutes($nsFolder): void
    {
        $namespace = $this->namespace . '\\' . $nsFolder;

        Route::middleware('api', 'auth:api', 'auth:sanctum', 'role:admin@api')
            ->namespace($namespace . "\\API\\V1")
            ->name('admin.api.v1.')
            ->prefix(config('routes.admin.api'))
            ->group(base_path('routes/admin/api/v1/api.php'));
    }

    private function clientWebRoutes($nsFolder): void
    {
        $namespace = $this->namespace . '\\' . $nsFolder;

        Route::middleware('web', 'auth')
            ->namespace($namespace)
            ->name('client.')
            ->prefix(config('routes.client.web'))
            ->group(base_path('routes/client/web/web.php'));
    }
    private function clientApiRoutes($nsFolder): void
    {
        $namespace = $this->namespace . '\\' . $nsFolder;

        Route::middleware('api', 'auth:api', 'auth:sanctum')
            ->namespace($namespace . "\\API\\V1")
            ->name('client.api.v1.')
            ->prefix(config('routes.client.api'))
            ->group(base_path('routes/client/api/v1/api.php'));
    }

    public function boot()
    {
//        Route::pattern('lang','('.implode('|',config('app.languages')).')');
//        Route::pattern('tail','('.implode('|',getAllTranslates('routes.list',true)).')');

        $this->configureRateLimiting();

        $this->headerRoutes();

            $this->sysadminApiRoutes('Sysadmin');
            $this->sysadminWebRoutes('Sysadmin');

            #copy this for <NewPanel>
            $this->adminApiRoutes('Admin');
            $this->adminWebRoutes('Admin');

            $this->clientApiRoutes('Client');
            $this->clientWebRoutes('Client');

            $this->authApiRoutes();
            $this->authWebRoutes();

            $this->developerApiRoutes('Develop');
            $this->developerWebRoutes('Develop');

            # should be at the end
            if(config('app.basic_interface',false)){
                $this->basicInterfaceWebRoutes('InterfaceZone');
#TODO: Activate basic interface api
//                $this->basicInterfaceApiRoutes('InterfaceZone');
            }
            else{
                $this->interfacePublicApiRoutes('InterfaceZone');
                $this->interfaceSecureApiRoutes('InterfaceZone');
                $this->interfaceWebRoutes('InterfaceZone');
            }

            $this->webhookRoutes();
            $this->sitemapRoutes();

        $this->footerRoutes();

    }

    private function interfaceWebRoutes($nsFolder): void
    {
        $namespace = $this->namespace . '\\' . $nsFolder;

        Route::middleware('web')
            ->namespace($namespace)
            ->name('public.')
            ->group(base_path('routes/interface/web/web.php'));
    }
    private function interfaceSecureApiRoutes($nsFolder): void
    {
        $namespace = $this->namespace . '\\' . $nsFolder;

        Route::middleware('api', 'auth:sanctum')
            ->namespace($namespace . "\\API\\V1")
            ->name('public.secure.api.v1.')
            ->prefix(config('routes.interface.secure_api'))
            ->group(base_path('routes/interface/secure_api/v1/api.php'));
    }
    private function interfacePublicApiRoutes($nsFolder): void
    {
        $namespace = $this->namespace . '\\' . $nsFolder;

        Route::middleware('api')
            ->namespace($namespace . "\\API\\V1")
            ->name('public.api.v1.')
            ->prefix(config('routes.interface.public_api'))
            ->group(base_path('routes/interface/public_api/v1/api.php'));
    }

    private function basicInterfaceWebRoutes($nsFolder): void
    {
        $namespace = $this->namespace . '\\' . $nsFolder;

        Route::middleware('web')
            ->namespace($namespace)
            ->name('public.')
            ->group(base_path('routes/basic_interface/web/web.php'));
    }
    private function basicInterfaceApiRoutes($nsFolder): void
    {
        $namespace = $this->namespace . '\\' . $nsFolder;

        Route::middleware('api', 'auth:sanctum')
            ->namespace($namespace . "\\API\\V1")
            ->name('public.api.v1.')
            ->prefix(config('routes.interface.api'))
            ->group(base_path('routes/basic_interface/api/v1/api.php'));
    }

    private function authWebRoutes(): void
    {
        # WARNING: adding Auth at the end of this namespace causes issue
        $namespace = $this->namespace;

        Route::middleware('web')
            ->namespace($namespace)
            ->name('auth.')
            ->prefix(config('routes.auth.web'))
            ->group(base_path('routes/auth/web/web.php'));
    }
    private function authApiRoutes(): void
    {
        # WARNING: adding Auth at the end of this namespace causes issue
        $namespace = $this->namespace;

        Route::middleware('api')
            ->namespace($namespace . "\\Auth\\API\\V1")
            ->name('auth.api.v1.')
            ->prefix(config('routes.auth.api'))
            ->group(base_path('routes/auth/api/v1.php'));
    }

    private function developerWebRoutes($nsFolder): void
    {
        $namespace = $this->namespace . '\\' . $nsFolder;

        Route::middleware('web')
            ->namespace($namespace)
            ->name('dev.')
            ->prefix('dev')
            ->group(app_path('Libraries/DeveloperCommands/web.php'));
    }
    private function developerApiRoutes($nsFolder): void
    {
        $namespace = $this->namespace . '\\' . $nsFolder;

        Route::middleware('api', 'auth:sanctum')
            ->namespace($namespace)
            ->name('dev.api.')
            ->prefix('api/dev')
            ->group(app_path('Libraries/DeveloperCommands/api.php'));
    }

    private function webhookRoutes(): void
    {
        $namespace = $this->namespace . '\\Webhook';

        Route::middleware('api')
            ->namespace($namespace)
            ->name('webhooks.')
            ->prefix('webhooks')
            ->group(base_path('routes/webhooks/routes.php'));
    }

    private function sitemapRoutes(): void
    {
        $namespace = $this->namespace;

        Route::middleware('web')
            ->namespace($namespace)
            ->name('sitemap.')
            ->prefix('sitemaps')
            ->group(base_path('routes/sitemap/web.php'));
    }

    protected function configureRateLimiting()
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by(optional($request->user())->id ?: get_ip());
        });
    }

    protected function headerRoutes(): void
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->name('header.')
            ->group(base_path('routes/header.php'));
    }

    protected function footerRoutes(): void
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->name('footer.')
            ->group(base_path('routes/footer.php'));
    }
}
