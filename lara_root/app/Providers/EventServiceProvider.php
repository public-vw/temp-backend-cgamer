<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        \SocialiteProviders\Manager\SocialiteWasCalled::class => [
            \SocialiteProviders\Twitter\TwitterExtendSocialite::class.'@handle',
            \SocialiteProviders\InstagramBasic\InstagramBasicExtendSocialite::class.'@handle',
            \SocialiteProviders\Telegram\TelegramExtendSocialite::class.'@handle',
            \SocialiteProviders\YouTube\YouTubeExtendSocialite::class.'@handle',
            \SocialiteProviders\Apple\AppleExtendSocialite::class.'@handle',
            \SocialiteProviders\Discord\DiscordExtendSocialite::class.'@handle',
            \SocialiteProviders\Steam\SteamExtendSocialite::class.'@handle',
            \SocialiteProviders\Twitch\TwitchExtendSocialite::class.'@handle',
        ],
        'App\Event\UserEntered' => [
            'App\Listeners\SendAdminTelegramNotification'
        ],
        'App\Event\ArticleCreated' => [
            'App\Listeners\SendAdminTelegramNotification'
        ],
        'App\Event\ArticleContentChanged' => [
            'App\Listeners\SendAdminTelegramNotification'
        ],
        'App\Event\ArticleReversioned' => [
            'App\Listeners\SendAdminTelegramNotification'
        ],
        'App\Event\ArticleStatusChanged' => [
            'App\Listeners\SendAdminTelegramNotification'
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
