<?php

namespace App\Providers;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;
use Laravel\Sanctum\Sanctum;

class AppServiceProvider extends ServiceProvider
{
    public function register()
    {
        Sanctum::ignoreMigrations();
    }

    public function boot()
    {
        \Schema::defaultStringLength(191);
        Paginator::useBootstrap();

        ## Disables lazy loading on a model to find N+1 problem,
        # stops app just in non-production mode
        # Model::preventLazyLoading(! app()->isProduction());

        app()->alias('App/Models/Country', 'Country');

        if (!defined('API_URL_TELEGRAM')) {
            define('API_URL_TELEGRAM', 'https://api.telegram.org/bot');
        }
        if (!defined('API_URL_TELEGRAM_FILE')) {
            define('API_URL_TELEGRAM_FILE', 'https://api.telegram.org/file/bot');
        }

        Relation::morphMap([
            'author'           => 'App\Models\Author',
            'user'             => 'App\Models\User',
            'social_channel'   => 'App\Models\SocialChannel',
            'article_category' => 'App\Models\ArticleCategory',
            'article'          => 'App\Models\Article',
            'comment'          => 'App\Models\Comment',
            'review'           => 'App\Models\Review',
            'payment'          => 'App\Models\Payment',
            'menu'             => 'App\Models\Menu',
            'hyperlink'        => 'App\Models\Hyperlink',
            'badge'            => 'App\Models\Badge',
            'fact'             => 'App\Models\Fact',
        ]);
    }
}
