<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    public function boot()
    {
        # add new panel's composer here
        $this->adminComposers();

        $this->interfaceComposers();

        View::composer(
            ['*'], 'App\ViewComposers\AllViewsComposer'
        );
    }

    protected function adminComposers(): void
    {
        View::composer(
            ['admin.*'], 'App\ViewComposers\AdminPanelComposer'
        );
        View::composer(
            ['admin.*.create'], 'App\ViewComposers\AdminPanelMorphComposer'
        );
        View::composer(
            ['admin.*.edit'], 'App\ViewComposers\AdminPanelMorphComposer'
        );
    }

    protected function interfaceComposers(): void
    {
        View::composer(
            ['interface.*','interface_*.*'], 'App\ViewComposers\InterfaceComposer'
        );
    }
}
