<?php

namespace App\Support\GetRates;

use Illuminate\Support\Facades\Cache;
use Spatie\Browsershot\Browsershot;

class IrtRate
{
    protected int $cache_ttl = 3600;
    protected float $safetyMargin = 1.05;
    protected array $minimum = [
        'USD' => 27500,
        'EUR' => 32500,
    ];
    protected float $USDRate = 1; # 1 USD = cr IRT
    protected float $EURRate = 1; # 1 Euro = cr IRT

    public function __construct()
    {
        $this->USDRate = $this->getRate('USD');
        $this->EURRate = $this->getRate('EUR');
    }

    function irtToEuro()
    {
        $price = ceil($this->EURRate * $this->safetyMargin);
        return $price;
    }

    function irtToUSD()
    {
        $price = ceil($this->USDRate * $this->safetyMargin);
        return $price;
    }

    protected function getRate($currency)
    {
        return max($this->minimum[$currency], $this->getTgjuRate($currency), $this->getBonbastRate($currency));
    }

    protected function getBonbastRate($currency)
    {
        $rate = 1;

        $page = $this->getPage('https://www.bonbast.com');
        \phpQuery::newDocumentHTML($page);


        switch ($currency) {
            case 'USD':
                $data = pq('#usd1_top')->text();
                $rate = (int)preg_replace('/[\.|,]/', '', $data);
                break;
            case 'EUR':
                $data = pq('#eur1_top')->text();
                $rate = (int)preg_replace('/[\.|,]/', '', $data);
                break;
        }

        return $rate;
    }

    protected function getTgjuRate($currency)
    {
        $rate = 1;

        $page = $this->getPage('https://www.tgju.org');
        \phpQuery::newDocumentHTML($page);

        switch ($currency) {
            case 'USD':
                $data = pq('tr[data-market-row="price_dollar_rl"] td:first')->text();
                $rate = (int)preg_replace('/[\.|,]/', '', $data);
                $rate = ceil($rate / 10);
                break;
            case 'EUR':
                $data = pq('tr[data-market-row="price_eur"] td:first')->text();
                $rate = (int)preg_replace('/[\.|,]/', '', $data);
                $rate = ceil($rate / 10);
                break;
        }

        return $rate;
    }

    protected function getPage($page): string
    {
        $cache_key = __METHOD__.'_'.crc32($page);
        if(Cache::has($cache_key)){
            return Cache::get($cache_key);
        }

        $ret = Browsershot::url($page)
            ->setNodeBinary(config('browsershot.node_path'))
            ->setNpmBinary(config('browsershot.npm_path'))
            ->setIncludePath('$PATH:' . config('browsershot.include_path'))
            ->setOption('args', ['--disable-web-security'])
            ->userAgent(config('browsershot.user_agent'))
            ->waitUntilNetworkIdle()
            ->noSandbox()
            ->timeout(30)
            ->ignoreHttpsErrors()
            ->delay(10)
            ->usePipe()
            ->userDataDir(config('browsershot.user_data_dir'))
            ->preventUnsuccessfulResponse()
//            ->setExtraNavigationHttpHeaders(['Custom-Header-Name' => 'Custom-Header-Value'])
//            ->useCookies(['Cookie-Key' => 'Cookie-Value'])
            ->bodyHtml();

        Cache::put($cache_key , $ret, $this->cache_ttl);
        return $ret;
    }
}
