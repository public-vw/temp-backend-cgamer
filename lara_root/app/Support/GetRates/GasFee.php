<?php

namespace App\Support\GetRates;

use GuzzleHttp\Client;

class GasFee
{
    function get()
    {
        $response = $this->readUrl();
        $lowPrice = $this->getPrice($response, 'lowPrice');

        return $lowPrice;
    }

    protected function readUrl()
    {
        $endpoint = "https://etherscan.io/autoUpdateGasTracker.ashx?sid=0e6bcf1d2d59132d43a216e5471ac99d";
        $headers = [
            'Content-Type' => 'application/json',
            'Cookie'       =>
                '__stripe_sid=83ed2127-f6cd-4e10-862a-9cd54ffa8d563bac0a;' .
                '__cuid=cad0c6bb894642ddb05a18dcfa3cf015;' .
                '_ga=GA1.2.1512482383.1649567750;' .
                'ASP.NET_SessionId=bt1kbbs30ogbc2xm10obgayf;' .
                '__cflb=02DiuFnsSsHWYH8WqVXaqGvd6BSBaXQLTMW4itVrzGb6L;' .
                '__cf_bm=sdNtbzBb7gNjUu5WXQWV7tXV_5yLFEOH9nhtmER3I28-1668486946-0-Aatqv1+M3L67xma5Ia9qf9Uthc2joPuI9mG97r4+L5TqoTsNYFCqV+2W1FkWv8fCT1U2x37MsvhXhFHG8KQyhW7MgGZxDz/DCnkAstoDveDxAGV/Dv5mzALYLdY5aufmDQ==;' .
                '_gid=GA1.2.1577138361.1668486947;' .
                'etherscan_cookieconsent=True;' .
                'amp_fef1e8=6f2701b4-5c48-4dee-9bca-3db08f8af37eR...1ghsp5vh1.1ghsp60gf.1.1.2;' .
                '_gat_gtag_UA_46998878_6=1' .
        '__stripe_mid=d45502e1-b03e-4147-8662-dd46696f3677bf131d; ' .
        'etherscan_userid=aghasemi2021; ' .
        'etherscan_pwd=4792:Qdxb:CZ/dGgJrqq+VIXlShUi7nKtfzI4ly30qoSZ8AvGeEAA=; ' .
        'etherscan_autologin=True',
        ];

        $client = new Client([
            'headers' => $headers,
        ]);

        $response = $client->request('GET', $endpoint, []);
        $statusCode = $response->getStatusCode();
        $response = $response->getBody()->getContents();

        return $response;
    }

    protected function getPrice($response, $key){
        $response = json_decode($response, true);
        return $response[$key];
    }

}
