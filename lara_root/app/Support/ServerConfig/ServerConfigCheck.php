<?php


namespace App\Support\ServerConfig;


use App\Jobs\CheckJobService;
use App\Models\ServerConfig;
use Carbon\Carbon;

class ServerConfigCheck
{
    public static function check(string $configName){
        return (new self)->$configName();
    }

    public function queue_service()
    {
        ServerConfig::clearValue('queue_service');
        CheckJobService::dispatch();
    }

    public function cronjob()
    {
        ServerConfig::clearValue('cronjob');
    }

    public function timezone()
    {
        $timezone = Carbon::now()->timezone->toOffsetTimeZone();
        ServerConfig::setValue('timezone',$timezone);
    }
}
