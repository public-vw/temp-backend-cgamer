<?php

namespace App\DataTables\Author;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Author\Traits\RoleJoint;
use App\Models\SeoPosition;
use Illuminate\Http\Request;

class SeoPositionDataTable extends CustomDataTable
{
    use RoleJoint;

    protected $model = SeoPosition::class;

    public function query(SeoPosition $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
