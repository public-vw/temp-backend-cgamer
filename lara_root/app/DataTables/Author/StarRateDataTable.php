<?php

namespace App\DataTables\Author;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Author\Traits\RoleJoint;
use App\Models\StarRate;
use Illuminate\Http\Request;

class StarRateDataTable extends CustomDataTable
{
    use RoleJoint;

    protected $model = StarRate::class;

    public function query(StarRate $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
