<?php

namespace App\DataTables\Author;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Author\Traits\RoleJoint;
use App\Models\Location;
use Illuminate\Http\Request;

class LocationDataTable extends CustomDataTable
{
    use RoleJoint;

    protected $model = Location::class;

    public function query(Location $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
