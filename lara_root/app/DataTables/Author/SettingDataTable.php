<?php

namespace App\DataTables\Author;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Author\Traits\RoleJoint;
use App\Models\Setting;
use Illuminate\Http\Request;

class SettingDataTable extends CustomDataTable
{
    use RoleJoint;

    protected $model = Setting::class;

    public function query(Setting $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
