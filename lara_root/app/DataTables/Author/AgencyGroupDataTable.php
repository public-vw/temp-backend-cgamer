<?php

namespace App\DataTables\Author;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Author\Traits\RoleJoint;
use App\Models\AgencyGroup;
use Illuminate\Http\Request;

class AgencyGroupDataTable extends CustomDataTable
{
    use RoleJoint;

    protected $model = AgencyGroup::class;

    public function query(AgencyGroup $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
