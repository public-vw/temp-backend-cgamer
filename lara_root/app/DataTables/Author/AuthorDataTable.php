<?php

namespace App\DataTables\Author;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Author\Traits\RoleJoint;
use App\Models\Author;
use Illuminate\Http\Request;

class AuthorDataTable extends CustomDataTable
{
    use RoleJoint;

    protected $model = Author::class;

    public function query(Author $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
