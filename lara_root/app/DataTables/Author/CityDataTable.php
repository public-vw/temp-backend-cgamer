<?php

namespace App\DataTables\Author;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Author\Traits\RoleJoint;
use App\Models\City;
use Illuminate\Http\Request;

class CityDataTable extends CustomDataTable
{
    use RoleJoint;

    protected $model = City::class;

    public function query(City $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
