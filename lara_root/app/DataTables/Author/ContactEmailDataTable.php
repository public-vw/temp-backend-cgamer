<?php

namespace App\DataTables\Author;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Author\Traits\RoleJoint;
use App\Models\ContactEmail;
use Illuminate\Http\Request;

class ContactEmailDataTable extends CustomDataTable
{
    use RoleJoint;

    protected $model = ContactEmail::class;

    public function query(ContactEmail $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
