<?php

namespace App\DataTables\Author;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Author\Traits\RoleJoint;
use App\Models\Province;
use Illuminate\Http\Request;

class ProvinceDataTable extends CustomDataTable
{
    use RoleJoint;

    protected $model = Province::class;

    public function query(Province $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
