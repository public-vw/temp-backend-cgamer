<?php

namespace App\DataTables\Author;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Author\Traits\RoleJoint;
use App\Models\PropertyGroup;
use Illuminate\Http\Request;

class PropertyGroupDataTable extends CustomDataTable
{
    use RoleJoint;

    protected $model = PropertyGroup::class;

    public function query(PropertyGroup $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
