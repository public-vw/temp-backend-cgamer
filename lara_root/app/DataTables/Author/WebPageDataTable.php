<?php

namespace App\DataTables\Author;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Author\Traits\RoleJoint;
use App\Models\WebPage;
use Illuminate\Http\Request;

class WebPageDataTable extends CustomDataTable
{
    use RoleJoint;

    protected $model = WebPage::class;

    public function query(WebPage $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
