<?php

namespace App\DataTables\Author;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Author\Traits\RoleJoint;
use App\Models\TelegramRobotMessage;
use Illuminate\Http\Request;

class TelegramRobotMessageDataTable extends CustomDataTable
{
    use RoleJoint;

    protected $model = TelegramRobotMessage::class;

    public function query(TelegramRobotMessage $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
