<?php

namespace App\DataTables\Author;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Author\Traits\RoleJoint;
use App\Models\ConstructorGroup;
use Illuminate\Http\Request;

class ConstructorGroupDataTable extends CustomDataTable
{
    use RoleJoint;

    protected $model = ConstructorGroup::class;

    public function query(ConstructorGroup $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
