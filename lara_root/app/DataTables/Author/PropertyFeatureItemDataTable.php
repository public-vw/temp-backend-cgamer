<?php

namespace App\DataTables\Author;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Author\Traits\RoleJoint;
use App\Models\PropertyFeatureItem;
use Illuminate\Http\Request;

class PropertyFeatureItemDataTable extends CustomDataTable
{
    use RoleJoint;

    protected $model = PropertyFeatureItem::class;

    public function query(PropertyFeatureItem $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
