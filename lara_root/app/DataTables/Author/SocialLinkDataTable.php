<?php

namespace App\DataTables\Author;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Author\Traits\RoleJoint;
use App\Models\SocialLink;
use Illuminate\Http\Request;

class SocialLinkDataTable extends CustomDataTable
{
    use RoleJoint;

    protected $model = SocialLink::class;

    public function query(SocialLink $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
