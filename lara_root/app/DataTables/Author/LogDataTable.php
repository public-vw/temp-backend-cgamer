<?php

namespace App\DataTables\Author;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Author\Traits\RoleJoint;
use App\Models\Log;
use Illuminate\Http\Request;

class LogDataTable extends CustomDataTable
{
    use RoleJoint;

    protected $model = Log::class;

    public function query(Log $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
