<?php

namespace App\DataTables\Author;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Author\Traits\RoleJoint;
use App\Models\HomepageSlider;
use Illuminate\Http\Request;

class HomepageSliderDataTable extends CustomDataTable
{
    use RoleJoint;

    protected $model = HomepageSlider::class;

    public function query(HomepageSlider $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
