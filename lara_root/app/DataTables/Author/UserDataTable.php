<?php

namespace App\DataTables\Author;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Author\Traits\RoleJoint;
use App\Models\User;
use Illuminate\Http\Request;

class UserDataTable extends CustomDataTable
{
    use RoleJoint;

    protected $model = User::class;

    public function query(User $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
