<?php

namespace App\DataTables\Author;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Author\Traits\RoleJoint;
use App\Models\Panel;
use Illuminate\Http\Request;

class PanelDataTable extends CustomDataTable
{
    use RoleJoint;

    protected $model = Panel::class;

    public function query(Panel $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
