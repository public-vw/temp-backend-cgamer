<?php

namespace App\DataTables\Author;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Author\Traits\RoleJoint;
use App\Models\SeoDetail;
use Illuminate\Http\Request;

class SeoDetailDataTable extends CustomDataTable
{
    use RoleJoint;

    protected $model = SeoDetail::class;

    public function query(SeoDetail $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
