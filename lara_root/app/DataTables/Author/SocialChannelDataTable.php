<?php

namespace App\DataTables\Author;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Author\Traits\RoleJoint;
use App\Models\SocialChannel;
use Illuminate\Http\Request;

class SocialChannelDataTable extends CustomDataTable
{
    use RoleJoint;

    protected $model = SocialChannel::class;

    public function query(SocialChannel $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
