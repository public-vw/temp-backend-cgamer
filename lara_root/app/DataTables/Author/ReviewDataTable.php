<?php

namespace App\DataTables\Author;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Author\Traits\RoleJoint;
use App\Models\Review;
use Illuminate\Http\Request;

class ReviewDataTable extends CustomDataTable
{
    use RoleJoint;

    protected $model = Review::class;

    public function query(Review $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
