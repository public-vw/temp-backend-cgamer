<?php

namespace App\DataTables\Author;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Author\Traits\RoleJoint;
use App\Models\ContactPhone;
use Illuminate\Http\Request;

class ContactPhoneDataTable extends CustomDataTable
{
    use RoleJoint;

    protected $model = ContactPhone::class;

    public function query(ContactPhone $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
