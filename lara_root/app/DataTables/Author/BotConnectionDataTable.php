<?php

namespace App\DataTables\Author;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Author\Traits\RoleJoint;
use App\Models\BotConnection;
use Illuminate\Http\Request;

class BotConnectionDataTable extends CustomDataTable
{
    use RoleJoint;

    protected $model = BotConnection::class;

    public function query(BotConnection $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
