<?php

namespace App\DataTables\Author;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Author\Traits\RoleJoint;
use App\Models\PropertyFeatureValue;
use Illuminate\Http\Request;

class PropertyFeatureValueDataTable extends CustomDataTable
{
    use RoleJoint;

    protected $model = PropertyFeatureValue::class;

    public function query(PropertyFeatureValue $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
