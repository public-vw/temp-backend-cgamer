<?php

namespace App\DataTables\Author;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Author\Traits\RoleJoint;
use App\Models\ContactAddress;
use Illuminate\Http\Request;

class ContactAddressDataTable extends CustomDataTable
{
    use RoleJoint;

    protected $model = ContactAddress::class;

    public function query(ContactAddress $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
