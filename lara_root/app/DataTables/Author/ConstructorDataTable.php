<?php

namespace App\DataTables\Author;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Author\Traits\RoleJoint;
use App\Models\Constructor;
use Illuminate\Http\Request;

class ConstructorDataTable extends CustomDataTable
{
    use RoleJoint;

    protected $model = Constructor::class;

    public function query(Constructor $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
