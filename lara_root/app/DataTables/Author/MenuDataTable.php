<?php

namespace App\DataTables\Author;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Author\Traits\RoleJoint;
use App\Models\Menu;
use Illuminate\Http\Request;

class MenuDataTable extends CustomDataTable
{
    use RoleJoint;

    protected $model = Menu::class;

    #TODO don't show sysauthor menus
    public function query(Menu $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
