<?php

namespace App\DataTables\Author;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Author\Traits\RoleJoint;
use App\Models\MenuPlaceholder;
use Illuminate\Http\Request;

class MenuPlaceholderDataTable extends CustomDataTable
{
    use RoleJoint;

    protected $model = MenuPlaceholder::class;

    public function query(MenuPlaceholder $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
