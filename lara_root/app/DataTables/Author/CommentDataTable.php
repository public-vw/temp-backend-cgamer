<?php

namespace App\DataTables\Author;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Author\Traits\RoleJoint;
use App\Models\Comment;
use Illuminate\Http\Request;

class CommentDataTable extends CustomDataTable
{
    use RoleJoint;

    protected $model = Comment::class;

    public function query(Comment $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
