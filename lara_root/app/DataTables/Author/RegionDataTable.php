<?php

namespace App\DataTables\Author;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Author\Traits\RoleJoint;
use App\Models\Region;
use Illuminate\Http\Request;

class RegionDataTable extends CustomDataTable
{
    use RoleJoint;

    protected $model = Region::class;

    public function query(Region $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
