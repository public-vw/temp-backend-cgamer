<?php

namespace App\DataTables\Author;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Author\Traits\RoleJoint;
use App\Models\SettingGroup;
use Illuminate\Http\Request;

class SettingGroupDataTable extends CustomDataTable
{
    use RoleJoint;

    protected $model = SettingGroup::class;

    public function query(SettingGroup $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
