<?php

namespace App\DataTables\Author;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Author\Traits\RoleJoint;
use App\Models\Property;
use Illuminate\Http\Request;

class PropertyDataTable extends CustomDataTable
{
    use RoleJoint;

    protected $model = Property::class;

    public function query(Property $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
