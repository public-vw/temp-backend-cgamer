<?php

namespace App\DataTables\Author;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Author\Traits\RoleJoint;
use App\Models\Attachment;
use Illuminate\Http\Request;

class AttachmentDataTable extends CustomDataTable
{
    use RoleJoint;

    protected $model = Attachment::class;

    public function query(Attachment $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
