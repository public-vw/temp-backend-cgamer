<?php

namespace App\DataTables\Author;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Author\Traits\RoleJoint;
use App\Models\AttachmentType;
use Illuminate\Http\Request;

class AttachmentTypeDataTable extends CustomDataTable
{
    use RoleJoint;

    protected $model = AttachmentType::class;

    public function query(AttachmentType $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
