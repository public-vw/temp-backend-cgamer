<?php

namespace App\DataTables\Author;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Author\Traits\RoleJoint;
use App\Models\SeoPositionGroup;
use Illuminate\Http\Request;

class SeoPositionGroupDataTable extends CustomDataTable
{
    use RoleJoint;

    protected $model = SeoPositionGroup::class;

    public function query(SeoPositionGroup $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
