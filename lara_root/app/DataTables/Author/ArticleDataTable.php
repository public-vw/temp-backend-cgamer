<?php

namespace App\DataTables\Author;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Author\Traits\RoleJoint;
use App\Models\Article;
use Illuminate\Http\Request;


class ArticleDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = Article::class;

    public function query(Article $model, Request $request): \Illuminate\Database\Eloquent\Builder
    {
        return $model->dataTableQuery($request->status);
    }
}
