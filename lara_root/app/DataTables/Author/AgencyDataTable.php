<?php

namespace App\DataTables\Author;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Author\Traits\RoleJoint;
use App\Models\Agency;
use App\Models\AgencyGroup;
use Illuminate\Http\Request;

class AgencyDataTable extends CustomDataTable
{
    use RoleJoint;

    protected $model = Agency::class;

    public function query(Agency $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
