<?php

namespace App\DataTables\Author;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Author\Traits\RoleJoint;
use App\Models\ModelAttachmentType;
use Illuminate\Http\Request;

class ModelAttachmentTypeDataTable extends CustomDataTable
{
    use RoleJoint;

    protected $model = ModelAttachmentType::class;

    public function query(ModelAttachmentType $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
