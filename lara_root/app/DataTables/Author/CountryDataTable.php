<?php

namespace App\DataTables\Author;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Author\Traits\RoleJoint;
use App\Models\Country;
use Illuminate\Http\Request;

class CountryDataTable extends CustomDataTable
{
    use RoleJoint;

    protected $model = Country::class;

    public function query(Country $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }


}
