<?php

namespace App\DataTables\Author;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Author\Traits\RoleJoint;
use App\Models\Request;

class RequestDataTable extends CustomDataTable
{
    use RoleJoint;

    protected $model = Request::class;

    public function query(Request $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
