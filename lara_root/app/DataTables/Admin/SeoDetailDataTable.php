<?php

namespace App\DataTables\Admin;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Admin\Traits\RoleJoint;
use App\Models\SeoDetail;
use Illuminate\Http\Request;

class SeoDetailDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = SeoDetail::class;

    public function query(SeoDetail $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
