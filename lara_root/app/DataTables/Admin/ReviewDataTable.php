<?php

namespace App\DataTables\Admin;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Admin\Traits\RoleJoint;
use App\Models\Review;
use Illuminate\Http\Request;

class ReviewDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = Review::class;

    public function query(Review $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
