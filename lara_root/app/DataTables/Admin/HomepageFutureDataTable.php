<?php

namespace App\DataTables\Admin;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Admin\Traits\RoleJoint;
use App\Models\HomepageFuture;
use Illuminate\Http\Request;


class HomepageFutureDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = HomepageFuture::class;

    public function query(HomepageFuture $model, Request $request): \Illuminate\Database\Eloquent\Builder
    {
        return $model->dataTableQuery($request->status);
    }
}
