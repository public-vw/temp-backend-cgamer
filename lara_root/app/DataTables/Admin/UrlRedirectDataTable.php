<?php

namespace App\DataTables\Admin;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Admin\Traits\RoleJoint;
use App\Models\UrlRedirect;
use Illuminate\Http\Request;


class UrlRedirectDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = UrlRedirect::class;

    public function query(UrlRedirect $model, Request $request): \Illuminate\Database\Eloquent\Builder
    {
        return $model->dataTableQuery($request->status);
    }
}
