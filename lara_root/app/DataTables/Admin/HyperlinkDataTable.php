<?php

namespace App\DataTables\Admin;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Admin\Traits\RoleJoint;
use App\Models\Hyperlink;
use Illuminate\Http\Request;


class HyperlinkDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = Hyperlink::class;

    public function query(Hyperlink $model, Request $request): \Illuminate\Database\Eloquent\Builder
    {
        return $model->dataTableQuery($request->status);
    }
}
