<?php

namespace App\DataTables\Admin;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Admin\Traits\RoleJoint;
use App\Models\UserMeta;
use Illuminate\Http\Request;


class UserMetaDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = UserMeta::class;

    public function query(UserMeta $model, Request $request): \Illuminate\Database\Eloquent\Builder
    {
        return $model->dataTableQuery($request->status);
    }
}
