<?php

namespace App\DataTables\Admin;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Admin\Traits\RoleJoint;
use App\Models\StarRate;
use Illuminate\Http\Request;

class StarRateDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = StarRate::class;

    public function query(StarRate $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
