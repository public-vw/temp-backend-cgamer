<?php

namespace App\DataTables\Admin;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Admin\Traits\RoleJoint;
use App\Models\ArticleCategory;
use Illuminate\Http\Request;


class ArticleCategoryDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = ArticleCategory::class;

    public function query(ArticleCategory $model, Request $request): \Illuminate\Database\Eloquent\Builder
    {
        return $model->dataTableQuery($request->status);
    }
}
