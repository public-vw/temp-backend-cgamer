<?php

namespace App\DataTables\Admin;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Admin\Traits\RoleJoint;
use App\Models\Badge;
use Illuminate\Http\Request;


class BadgeDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = Badge::class;

    public function query(Badge $model, Request $request): \Illuminate\Database\Eloquent\Builder
    {
        return $model->dataTableQuery($request->status);
    }
}
