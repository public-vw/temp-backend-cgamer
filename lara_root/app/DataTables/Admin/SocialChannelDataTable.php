<?php

namespace App\DataTables\Admin;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Admin\Traits\RoleJoint;
use App\Models\SocialChannel;
use Illuminate\Http\Request;

class SocialChannelDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = SocialChannel::class;

    public function query(SocialChannel $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
