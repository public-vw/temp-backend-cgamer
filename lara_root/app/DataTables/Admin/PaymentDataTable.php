<?php

namespace App\DataTables\Admin;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Admin\Traits\RoleJoint;
use App\Models\Payment;
use Illuminate\Http\Request;


class PaymentDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = Payment::class;

    public function query(Payment $model, Request $request): \Illuminate\Database\Eloquent\Builder
    {
        return $model->dataTableQuery($request->status);
    }
}
