<?php

namespace App\DataTables\Admin;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Admin\Traits\RoleJoint;
use App\Models\Request as Requesti;
use Illuminate\Http\Request;


class RequestDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = Requesti::class;

    public function query(Requesti $model, Request $request): \Illuminate\Database\Eloquent\Builder
    {
        return $model->dataTableQuery($request->status);
    }
}
