<?php

namespace App\DataTables\Admin;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Admin\Traits\RoleJoint;
use App\Models\HomepageSlider;
use Illuminate\Http\Request;

class HomepageSliderDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = HomepageSlider::class;

    public function query(HomepageSlider $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
