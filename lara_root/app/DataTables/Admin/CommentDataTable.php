<?php

namespace App\DataTables\Admin;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Admin\Traits\RoleJoint;
use App\Models\Comment;
use Illuminate\Http\Request;

class CommentDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = Comment::class;

    public function query(Comment $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
