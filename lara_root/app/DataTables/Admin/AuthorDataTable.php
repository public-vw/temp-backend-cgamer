<?php

namespace App\DataTables\Admin;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Admin\Traits\RoleJoint;
use App\Models\Author;
use Illuminate\Http\Request;

class AuthorDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = Author::class;

    public function query(Author $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
