<?php

namespace App\DataTables\Admin;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Admin\Traits\RoleJoint;
use App\Models\SocialLink;
use Illuminate\Http\Request;

class SocialLinkDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = SocialLink::class;

    public function query(SocialLink $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
