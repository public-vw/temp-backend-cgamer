<?php

namespace App\DataTables\Admin;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Admin\Traits\RoleJoint;
use App\Models\ContactPhone;
use Illuminate\Http\Request;

class ContactPhoneDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = ContactPhone::class;

    public function query(ContactPhone $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
