<?php

namespace App\DataTables\Admin;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Admin\Traits\RoleJoint;
use App\Models\ContactEmail;
use Illuminate\Http\Request;

class ContactEmailDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = ContactEmail::class;

    public function query(ContactEmail $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
