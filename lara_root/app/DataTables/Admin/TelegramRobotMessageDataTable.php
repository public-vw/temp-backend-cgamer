<?php

namespace App\DataTables\Admin;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Admin\Traits\RoleJoint;
use App\Models\TelegramRobotMessage;
use Illuminate\Http\Request;

class TelegramRobotMessageDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = TelegramRobotMessage::class;

    public function query(TelegramRobotMessage $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
