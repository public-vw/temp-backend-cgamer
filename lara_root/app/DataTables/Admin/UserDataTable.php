<?php

namespace App\DataTables\Admin;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Admin\Traits\RoleJoint;
use App\Models\User;
use Illuminate\Http\Request;

class UserDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = User::class;

    public function query(User $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
