<?php

namespace App\DataTables\Admin;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Admin\Traits\RoleJoint;
use App\Models\ModelAttachmentType;
use Illuminate\Http\Request;

class ModelAttachmentTypeDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = ModelAttachmentType::class;

    public function query(ModelAttachmentType $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
