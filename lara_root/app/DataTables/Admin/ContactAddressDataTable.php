<?php

namespace App\DataTables\Admin;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Admin\Traits\RoleJoint;
use App\Models\ContactAddress;
use Illuminate\Http\Request;

class ContactAddressDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = ContactAddress::class;

    public function query(ContactAddress $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
