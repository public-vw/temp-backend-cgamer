<?php

namespace App\DataTables\Admin;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Admin\Traits\RoleJoint;
use App\Models\Fact;
use Illuminate\Http\Request;


class FactDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = Fact::class;

    public function query(Fact $model, Request $request): \Illuminate\Database\Eloquent\Builder
    {
        return $model->dataTableQuery($request->status);
    }
}
