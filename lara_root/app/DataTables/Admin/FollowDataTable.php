<?php

namespace App\DataTables\Admin;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Admin\Traits\RoleJoint;
use App\Models\Follow;
use Illuminate\Http\Request;


class FollowDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = Follow::class;

    public function query(Follow $model, Request $request): \Illuminate\Database\Eloquent\Builder
    {
        return $model->dataTableQuery($request->status);
    }
}
