<?php

namespace App\DataTables\Admin;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Admin\Traits\RoleJoint;
use App\Models\Attachment;
use Illuminate\Http\Request;

class AttachmentDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = Attachment::class;

    public function query(Attachment $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
