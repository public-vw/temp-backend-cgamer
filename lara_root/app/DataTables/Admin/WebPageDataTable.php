<?php

namespace App\DataTables\Admin;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Admin\Traits\RoleJoint;
use App\Models\WebPage;
use Illuminate\Http\Request;

class WebPageDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = WebPage::class;

    public function query(WebPage $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
