<?php

namespace App\DataTables;

use Carbon\Carbon;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
use Illuminate\Database\Eloquent\Model;


abstract class CustomDataTable extends DataTable
{
    protected $fields;
    protected $table;
    protected string $model;

    public function __construct()
    {
        $this->table = (new $this->model)->getTable();
        $this->fields = $this->removeNoViewFields($this->model::$dataTableFields);
    }

    protected function removeNoViewFields($fields)
    {
        foreach ($fields as $field => $properties) {
            $view = "{$this->role}.items.{$this->table}.datatable.$field";
            if (!View::exists($view)) {
                unset($fields[$field]);
            }
        }
        return $fields;
    }

    public function dataTable($query)
    {
        $fields = array_keys($this->fields);
        $table = datatables()->eloquent($query);

        foreach ($fields as $i => $field) {
            $view = "{$this->role}.items.{$this->table}.datatable.$field";
            $table = $table->editColumn($field, function (Model $model) use ($field, $view) {
                return view($view)
                    ->with('variable', $model)
                    ->render();
            });
        }
        $table->rawColumns($fields);
        return $table;
    }

    public function html()
    {
        return $this->builder()
            ->setTableId("{$this->table}-table")
            ->columns($this->getColumns())
            ->addCheckbox(['class' => 'check-all pl-0'], 0)
            ->scrollX(false)
            ->scrollY(false)
            ->minifiedAjax();
    }

    protected function getColumns()
    {
        $columns = [];
        foreach ($this->fields as $field => $properties) {
            if($field == 'checkbox') continue;
/*            if($field == 'checkbox') {
                $html = '<a href="javascript:;" class="rev-all pl-0"><i class="ion ion-ios-repeat"></i></a>';
                $columns[] = Column::make($field)
                    ->title($html)->width(20)
                    ->searchable(false)->exportable(false)
                    ->printable(false)->orderable(false);
                continue;
            }*/
            $features = explode('|', $properties[0] ?? null);
            $columns[] = Column::make($field)
                ->name($properties[1] ?? $field)
                ->data($properties[2] ?? $field)
                ->title(__("{$this->role}/datatables/{$this->table}/title.$field"))
                ->width(__("{$this->role}/datatables/{$this->table}/width.$field"))
                ->searchable(!$this->hasProperties('search', $features))
                ->exportable(!$this->hasProperties('export', $features))
                ->printable(!$this->hasProperties('print', $features))
                ->orderable(!$this->hasProperties('order', $features));
        }

        return $columns;
    }

    protected function hasProperties($type, $properties){
        return !(
            in_array("dont-{$type}", $properties)
            || !in_array('dont-all', $properties)
        );
    }

    protected function filename()
    {
        return Str::ucfirst($this->role) . '/' . $this->table . '_' . Carbon::now('YmdHis');
    }
}
