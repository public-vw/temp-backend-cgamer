<?php

namespace App\DataTables\Client;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Client\Traits\RoleJoint;
use App\Models\SettingGroup;
use Illuminate\Http\Request;

class SettingGroupDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = SettingGroup::class;

    public function query(SettingGroup $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
