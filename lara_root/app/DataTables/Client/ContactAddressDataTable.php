<?php

namespace App\DataTables\Client;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Client\Traits\RoleJoint;
use App\Models\ContactAddress;
use Illuminate\Http\Request;

class ContactAddressDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = ContactAddress::class;

    public function query(ContactAddress $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
