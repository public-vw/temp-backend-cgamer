<?php

namespace App\DataTables\Client;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Client\Traits\RoleJoint;
use App\Models\Currency;
use Illuminate\Http\Request;

class CurrencyDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = Currency::class;

    public function query(Currency $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
