<?php

namespace App\DataTables\Client;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Client\Traits\RoleJoint;
use App\Models\Author;
use Illuminate\Http\Request;

class AuthorDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = Author::class;

    public function query(Author $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
