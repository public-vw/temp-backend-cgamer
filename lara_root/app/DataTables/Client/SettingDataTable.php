<?php

namespace App\DataTables\Client;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Client\Traits\RoleJoint;
use App\Models\Setting;
use Illuminate\Http\Request;

class SettingDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = Setting::class;

    public function query(Setting $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
