<?php

namespace App\DataTables\Client;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Client\Traits\RoleJoint;
use App\Models\TelegramRobotMessage;
use Illuminate\Http\Request;

class TelegramRobotMessageDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = TelegramRobotMessage::class;

    public function query(TelegramRobotMessage $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
