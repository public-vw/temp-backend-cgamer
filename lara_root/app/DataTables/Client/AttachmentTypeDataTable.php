<?php

namespace App\DataTables\Client;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Client\Traits\RoleJoint;
use App\Models\AttachmentType;
use Illuminate\Http\Request;

class AttachmentTypeDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = AttachmentType::class;

    public function query(AttachmentType $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
