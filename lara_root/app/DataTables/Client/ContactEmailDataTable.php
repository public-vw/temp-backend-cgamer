<?php

namespace App\DataTables\Client;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Client\Traits\RoleJoint;
use App\Models\ContactEmail;
use Illuminate\Http\Request;

class ContactEmailDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = ContactEmail::class;

    public function query(ContactEmail $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
