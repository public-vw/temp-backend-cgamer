<?php

namespace App\DataTables\Client;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Client\Traits\RoleJoint;
use App\Models\StarRate;
use Illuminate\Http\Request;

class StarRateDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = StarRate::class;

    public function query(StarRate $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
