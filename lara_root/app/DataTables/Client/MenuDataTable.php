<?php

namespace App\DataTables\Client;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Client\Traits\RoleJoint;
use App\Models\Menu;
use Illuminate\Http\Request;

class MenuDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = Menu::class;

    #TODO don't show sysclient menus
    public function query(Menu $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
