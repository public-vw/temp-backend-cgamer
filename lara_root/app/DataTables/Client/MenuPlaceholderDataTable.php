<?php

namespace App\DataTables\Client;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Client\Traits\RoleJoint;
use App\Models\MenuPlaceholder;
use Illuminate\Http\Request;

class MenuPlaceholderDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = MenuPlaceholder::class;

    public function query(MenuPlaceholder $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
