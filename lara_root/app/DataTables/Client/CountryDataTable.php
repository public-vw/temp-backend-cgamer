<?php

namespace App\DataTables\Client;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Client\Traits\RoleJoint;
use App\Models\Country;
use Illuminate\Http\Request;

class CountryDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = Country::class;

    public function query(Country $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }


}
