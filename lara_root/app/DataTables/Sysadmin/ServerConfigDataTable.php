<?php

namespace App\DataTables\Sysadmin;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Sysadmin\Traits\RoleJoint;
use App\Models\ServerConfig;
use Illuminate\Http\Request;

class ServerConfigDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = ServerConfig::class;

    public function query(ServerConfig $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
