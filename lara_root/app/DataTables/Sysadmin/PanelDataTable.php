<?php

namespace App\DataTables\Sysadmin;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Sysadmin\Traits\RoleJoint;
use App\Models\Panel;
use Illuminate\Http\Request;

class PanelDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = Panel::class;

    public function query(Panel $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
