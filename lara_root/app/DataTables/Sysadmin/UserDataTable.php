<?php

namespace App\DataTables\Sysadmin;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Sysadmin\Traits\RoleJoint;
use App\Models\User;
use Illuminate\Http\Request;

class UserDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = User::class;

    public function query(User $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
