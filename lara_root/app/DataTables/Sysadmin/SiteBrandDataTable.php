<?php

namespace App\DataTables\Sysadmin;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Sysadmin\Traits\RoleJoint;
use App\Models\SiteBrand;
use Illuminate\Http\Request;

class SiteBrandDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = SiteBrand::class;

    public function query(SiteBrand $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
