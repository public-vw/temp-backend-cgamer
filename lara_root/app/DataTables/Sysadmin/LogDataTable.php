<?php

namespace App\DataTables\Sysadmin;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Sysadmin\Traits\RoleJoint;
use App\Models\Log;
use Illuminate\Http\Request;

class LogDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = Log::class;

    public function query(Log $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
