<?php

namespace App\DataTables\Sysadmin;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Sysadmin\Traits\RoleJoint;
use App\Models\Role;


class RoleDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = Role::class;

    public function __construct()
    {
        $this->fields = [
            'title'   => [],
            'panel'   => [],
            'actions' => ['dont-all'],
        ];

        $this->table = (new $this->model)->getTable();
    }

    public function query(Role $model)
    {
        return $model->newQuery();
    }
}
