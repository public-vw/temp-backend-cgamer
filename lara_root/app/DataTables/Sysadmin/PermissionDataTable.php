<?php

namespace App\DataTables\Sysadmin;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Sysadmin\Traits\RoleJoint;
use Spatie\Permission\Models\Permission;

class PermissionDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = Permission::class;

    public function __construct()
    {
        $this->fields = [
            'title'   => [],
            'actions' => ['dont-all'],
        ];

        $this->table = (new $this->model)->getTable();
    }

    public function query(Permission $model)
    {
        return $model->newQuery();
    }
}
