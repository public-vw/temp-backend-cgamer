<?php

namespace App\DataTables\Sysadmin;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Sysadmin\Traits\RoleJoint;
use App\Models\MenuPlaceholder;
use Illuminate\Http\Request;

class MenuPlaceholderDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = MenuPlaceholder::class;

    public function __construct()
    {
        parent::__construct();

        unset($this->fields['panel']);
    }

    public function query(Request $request, MenuPlaceholder $model)
    {
        $panel = $request->attributes->get('panel');

        return $panel->menu_placeholders();
    }
}
