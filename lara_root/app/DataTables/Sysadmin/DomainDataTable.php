<?php

namespace App\DataTables\Sysadmin;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Sysadmin\Traits\RoleJoint;
use App\Models\Domain;
use Illuminate\Http\Request;

class DomainDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = Domain::class;

    public function query(Domain $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
