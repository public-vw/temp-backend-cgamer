<?php

namespace App\DataTables\Sysadmin;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Sysadmin\Traits\RoleJoint;
use App\Models\Structure;
use Illuminate\Http\Request;

class StructureDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = Structure::class;

    public function query(Structure $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
