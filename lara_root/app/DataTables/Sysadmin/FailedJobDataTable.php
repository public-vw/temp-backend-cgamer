<?php

namespace App\DataTables\Sysadmin;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Sysadmin\Traits\RoleJoint;
use App\Models\FailedJob;
use Illuminate\Http\Request;

class FailedJobDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = FailedJob::class;

    public function query(FailedJob $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
