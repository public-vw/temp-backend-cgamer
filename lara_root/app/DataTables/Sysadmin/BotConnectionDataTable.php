<?php

namespace App\DataTables\Sysadmin;

use App\DataTables\CustomDataTable;
use App\Http\Controllers\Sysadmin\Traits\RoleJoint;
use App\Models\BotConnection;
use Illuminate\Http\Request;

class BotConnectionDataTable extends CustomDataTable
{
    use RoleJoint;

    protected string $model = BotConnection::class;

    public function query(BotConnection $model, Request $request)
    {
        return $model->dataTableQuery($request->status);
    }
}
