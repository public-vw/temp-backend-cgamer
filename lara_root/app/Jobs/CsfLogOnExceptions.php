<?php

namespace App\Jobs;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Artisan;

class CsfLogOnExceptions implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $ip;
    protected $uri;
    protected $statusCode;
    protected $time;

    public function __construct($ip, $uri, $statusCode, $time)
    {
        $this->ip = $ip;
        $this->uri = $uri;
        $this->statusCode = $statusCode;
        $this->time = $time;
    }

    public function handle()
    {
        $dangerLevel = 1;
        $knownDangers = config('csf');

        $detections = array_filter($knownDangers, function($key){
            return (stripos($this->uri,$key) !== FALSE);
        }, ARRAY_FILTER_USE_KEY);

        arsort($detections);

        $dangerLevel = array_shift($detections) ?? $dangerLevel;

//      E404 |L1 |IP[00.00.00.00] |Tyyyymmdd-hhiiss|uri
        Artisan::queue("csf:add \"E{$this->statusCode}\t|L{$dangerLevel}\t|IP[{$this->ip}]\t|T{$this->time}|".addslashes($this->uri)."\"");

//      E404|L1|IP[00.00.00.00]|uri
//        Artisan::call("csf:add E{$this->statusCode}|L{$dangerLevel}|IP[{$this->ip}]|".addslashes($this->uri));
    }
}
