<?php

namespace App\Jobs;

use App\Models\ServerConfig;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendGooglePing implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $seoable;

    public function __construct($seoable)
    {
        $this->seoable = $seoable;
    }

    public function handle()
    {
        $client = new \Google_Client();

        $client->setAuthConfig(storage_path('tokens/cryptobaz-51d39-4979ece5d5d6.json'));
        $client->addScope('https://www.googleapis.com/auth/indexing');

        $httpClient = $client->authorize();
        $endpoint = 'https://indexing.googleapis.com/v3/urlNotifications:publish';

        $content = json_encode([
            "url"  => $this->seoable->seoUrl(),
            "type" => "URL_UPDATED",
        ]);

        $response = $httpClient->post($endpoint, [ 'body' => $content ]);
        $status_code = $response->getStatusCode();

        mylog('Google Pinged','ding',
            "💡 Google Pinged on activation/update" .
            "\n<b>URL: </b>{$this->seoable->seoUrl()}" .
            "\n<b>Heading: </b>{$this->seoable->heading}" .
            "\n<b>Response Code: </b>{$status_code}"
            ,false);
    }
}
