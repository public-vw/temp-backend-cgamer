<?php

namespace Database\Factories;

use App\Models\_Model_;
use Illuminate\Database\Eloquent\Factories\Factory;

class _Model_Factory extends Factory
{
    protected $model = _Model_::class;

    public function definition()
    {
        return [
            'title' => $this->faker->unique()->company,
        ];
    }
}
