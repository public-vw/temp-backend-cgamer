<?php

namespace Database\Seeders\FactoringSeeders;

class _Model_Seeder extends FactoringSeeder
{
    protected string $model = \App\Models\_Model_::class;

    public function postRun($fakerRecord){
        $relations = new _RelatedItems($fakerRecord);

        $relations->makeComment(10);
    }
}
