<?php
## Replace these to make controller operable
# _Panel_    -> Admin, Sysadmin, ... (role name, capital case)
# _Model_ -> Agency, PropertyGroup, ... (model name)
# _model_ -> agency, property_group, ... (model name, snake case)
#

namespace App\Http\Requests\_Panel_;

use App\Http\Requests\_CustomFormRequest;
use App\Models\_Model_;

class _Model_Request extends _CustomFormRequest
{
    protected function prepareProperties()
    {
        $this->model = _Model_::class;
        $this->mainEntity = $this->_model_;
    }
}
