<?php

namespace App\Models;

use App\Support\Database\CacheQueryBuilder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class _Model_ extends Model
{
    use HasFactory;
    use CacheQueryBuilder;

    public static array $dataTableFields = [
        'title'          => [], //options: dont-search, dont-order, dont-print, dont-export, dont-all
        '_model__group'  => [null, '_model__groups.title'],
        '_model__group2' => [null, '_model__groups.title','_model__groups.data'],
        'actions'        => ['dont-all'],
    ];

    public static array $validation = [
        '*'             => 'bail|string',
        'title'         => 'required',
    ];

    protected $fillable = [
        'title',
    ];

    # -----------------------------------

    public static function dataTableQuery(string $status = null)
    {
        return (new static)->newQuery();

//        $query = new static;
//        if (!is_null($status)) $query = $query->where($query->getTable().'.status', $status);
//
//        $query = $query->leftJoin('agency_groups', 'agencies.group_id', '=', 'agency_groups.id')
//            ->select(['agencies.*', 'agency_groups.title as agency_group']);
//        return $query;
    }

}
