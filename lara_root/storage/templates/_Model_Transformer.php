<?php

namespace App\Http\Transformers;

use App\Models\_Model_;
use League\Fractal\TransformerAbstract;

class _Model_Transformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        //
    ];

    protected $availableIncludes = [
        //
    ];

    public function transform(_Model_ $_model_)
    {
        return [
            'id'    => $_model_->id,
            'title' => $_model_->title,
        ];
    }
}
