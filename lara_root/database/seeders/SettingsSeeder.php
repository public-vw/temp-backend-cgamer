<?php

namespace Database\Seeders;

use App\Models\SettingGroup;
use App\Models\Setting;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use phpDocumentor\Reflection\Types\Integer;

class SettingsSeeder extends Seeder
{
    protected $groups = [
        'alerts' => [
            'sysadmin',
            'admin',
        ],
    ];

    protected $items = [
        [
            'alerts.sysadmin' => [
                    'visits' => null,
                    'errors' => ['pusher', 'telegram'],
                ],
            'alerts.admin' => [
                'visits' => ['pusher'],
                'errors' => ['telegram'],
            ],
        ]
    ];

    protected Integer $order = 1;
    protected array $groupIds = [];

    public function run()
    {
        $this->makeGroup('', $this->groups, $tmp->id);
    }


    protected function makeGroup($parentTitle, $item, $parentGroupId = null){
        foreach ($item as $title => $childs){
            $tmp = SettingGroup::create([
                'title' => $title,
                'parent_id' => $parentGroupId,
                'slug' => Str::slug($title),
                'order' => $this->order++,
                'hidden' => false,
            ]);
            if(!empty($childs)){
                foreach($childs as $child){
                    $this->makeGroup($title, $child, $tmp->id);
                }
            }
            $this->groupIds["$parentTitle.$title"] = $tmp->id;
        }
    }
}
