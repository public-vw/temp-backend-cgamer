<?php

namespace Database\Seeders;

use App\Models\AttachmentType;
use App\Models\ModelAttachmentType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class AttachmentTypeSeeder extends Seeder
{
    protected $images = [
        'Logo'   => [
            "[width|~,50],[height|~,50],[size|50]",
            "SiteBrand",
        ],
        'Header' => [
            "[box|1920,500],[size|600]",
            "WebPage",
        ],

        'Avatar' => [
            "[box|40,44|30,32],[size|30]",
            "User",
        ],

        'Article Header' => [
            "[box|1280,560|1024,-|-,320],[size|1024]",
            "Article",
        ],

        'Article Default Header' => [
            "[box|1280,560|1024,-|-,320],[size|1024]",
            "ArticleCategory",
        ],

        'Article Inside' => [
            "[box|690,320|500,-|340,-|250,-],[size|720]",
            "Article",
        ],

        'Article Category Header' => [
            "[box|1140,300],[size|2048]",
            "ArticleCategory",
        ],

        'Article Category Homepage Thumbnail' => [
            "[box|560,420],[size|2048]",
            "ArticleCategory",
            'thumbnail',
        ],

        'Payment Paper' => [
            "[box|1000,-],[size|500]",
            "Payment",
        ],

        'Social Channel Thumbnail' => [
            "[box|300,-],[size|500]",
            "SocialChannel",
            'thumbnail',
        ],

        'Homepage Future' => [
            "[box|300,-],[size|500]",
            "HomepageFuture",
        ],
    ];
    protected $videos = [
        'HomePage Slider'  => "[box|1920,800]",
        'Property Content' => "[box|1920,940]",
    ];

    public function run()
    {
        foreach ($this->images as $title => $props) {
            $att = AttachmentType::create([
                'title'            => $title,
                'slug'             => Str::slug($title, '_'),
                'properties_limit' => $props[0],
                'type'             => (string)config_key('enums.attachment_types', 'Image'),
                'folder'             => $props[2] ?? 'images',
            ]);
            $models = explode('|', $props[1]);
            foreach ($models ?? [] as $model)
                ModelAttachmentType::create([
                    'attachment_type_id' => $att->id,
                    'model_name'         => $model,
                ]);
        }
        foreach ($this->videos as $title => $prop) {
            $att = AttachmentType::create([
                'title'            => $title,
                'slug'             => Str::slug($title, '_'),
                'properties_limit' => $prop,
                'type'             => (string)config_key('enums.attachment_types', 'Video'),
                'folder'             => $props[2] ?? 'videos',
            ]);
            $models = explode('|', $props[1]);
            foreach ($models ?? [] as $model)
                ModelAttachmentType::create([
                    'attachment_type_id' => $att->id,
                    'model_name'         => $model,
                ]);
        }
    }
}
