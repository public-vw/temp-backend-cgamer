<?php

namespace Database\Seeders;

use App\Models\BotConnection;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BotConnectionSeeder extends Seeder
{
    public function run()
    {
//        DB::table('bot_connections')->truncate();//causes issue

        BotConnection::create([
            'title'           => 'Alert Sender Robot',
            'username'        => 'WebAlertRobot',
            'type'            => (string)config_key('enums.bot_connections', 'telegram'),
            'robot_token'     => '2018720703:AAEqQurNbj063U0WhbbpQtw-RC2wkJryqqE',
            'parameters'      => null,
            'webhook_token'   => null,
            'active'          => false,
        ]);
        BotConnection::create([
            'title'           => 'Gas Fee Robot',
            'username'        => 'GasFeeRobot',
            'type'            => (string)config_key('enums.bot_connections', 'telegram'),
            'robot_token'     => '2110047611:AAHBv3TJ1O_QlwytPnel5RSQPHcMOvoIGgY',
            'parameters'      => null,
            'webhook_token'   => 'ea_ngew8aGho8Ew',
            'active'          => true,
        ]);
        BotConnection::create([
            'title'           => 'CryptoBaz Assitant',
            'username'        => 'CryptoBazRobot',
            'type'            => (string)config_key('enums.bot_connections', 'telegram'),
            'robot_token'     => '2053690270:AAGegXGWXL3aCaasDt4Gi9sUuTUpkWf9kx0',
            'parameters'      => null,
            'webhook_token'   => 'oof8ieCh7siGhoo',
            'active'          => true,
        ]);
    }
}
