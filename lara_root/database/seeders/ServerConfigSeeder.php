<?php

namespace Database\Seeders;

use App\Models\ServerConfig;
use Illuminate\Database\Seeder;

class ServerConfigSeeder extends Seeder
{
    public function run()
    {
        $configs = [
            'timezone',
            'cronjob',
            'queue_service',
        ];

        foreach ($configs as $config) {
            ServerConfig::create([
                'key'   => $config,
                'value' => null,
            ]);
        }
    }
}
