<?php

namespace Database\Seeders;

use App\Models\Badge;
use Illuminate\Database\Seeder;

class BadgeSeeder extends Seeder
{
    public function run()
    {
        $items = [
            'تازه‌وارد',
            'نویسنده',
            'همراه همیشگی',
            'نویسنده تخصصی',
        ];

        foreach ($items as $item) {
            Badge::create([
                'title'    => $item,
            ]);
        }
    }
}
