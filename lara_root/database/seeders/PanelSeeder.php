<?php

namespace Database\Seeders;

use App\Models\Panel;
use Illuminate\Database\Seeder;

class PanelSeeder extends Seeder
{
    public function run()
    {
        $panels = [
            'admin',
            'author',
            'interface',
        ];

        foreach ($panels as $panel) {
            Panel::create([
                'title'  => $panel,
                'active' => true,
            ]);
        }
    }
}
