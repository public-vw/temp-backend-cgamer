<?php

namespace Database\Seeders;

use App\Models\Country;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class CountrySeeder extends Seeder
{
    public function run()
    {
        DB::table('countries')->truncate();
        DB::table('translates')->truncate();

        $countries = $this->prepareData();
        foreach ($countries as $data) {
            if(count($data) != 3) continue;
            Country::create([
                'title'    => ['en' => $data[0], 'fa' => $data[1]],
                'flag_iso' => $data[2],
            ]);
        }
    }

    protected function prepareData()
    {
        $content = Storage::disk('data')->get('CountriesFlags.csv');
        $content = explode("\n", $content);
        $content = array_map(function ($line) {
            return explode('|', $line);
        }, $content);
        return $content;
    }
}
