<?php

namespace Database\Seeders;

use App\Models\Menu;
use App\Models\MenuPlaceholder;
use App\Models\Panel;
use Illuminate\Database\Seeder;

class AdminPanelSidebarSeeder extends Seeder
{
    const DEFAULT_ICON = 'icon ion-ios-arrow-forward';
    const PANEL = 'admin';
    const PLACEHOLDER = 'sidebar';

    protected $panel;
    protected $placeholder;

    public function __construct()
    {
        $this->panel = Panel::where('title', self::PANEL)->first();
    }

    protected function truncate(){
        $placeholder = MenuPlaceholder::where('title', self::PLACEHOLDER)->first();
        if(!$placeholder)return;
        $placeholder->menus()->delete();
        $placeholder->delete();
    }

    public function run()
    {
        $sidebar = [
            'dashboard'    => ['Dashboard', 'fa fa-home', 'home'],
            'break1'       => null,
            'article_categories'     => ['Categories'],
            'articles'     => ['Articles'],
            'authors'       => ['Authors'],
            'break2'       => null,
            'requests'     => ['Requests'],
            'url_redirects'     => ['Redirections'],
            'break3'       => null,
            'content'   =>[
                'Content',
                'children' => [
                    'badges' => ['Badges'],
                    'facts' => ['Facts'],
                ],
            ],
            'break4'     => null,
            'information'  => [
                'Inforamtion',
                'children' => [
                    'locations'  => [
                        'Location',
                        'children' => [
                            'locations' => ['Place'],
                            'countries' => ['Country'],
                            'provinces' => ['Province'],
                            'cities'    => ['City'],
                            'regions'   => ['Region'],
                        ],
                    ],
                    'break1'     => null,
                    'contacts'   => [
                        'Contacts',
                        'children' => [
                            'contact_addresses' => ['Contact Address'],
                            'contact_phones'    => ['Contact Phone'],
                            'contact_emails'    => ['Contact Email'],
                            'social_channels'   => ['Social Channel'],
                            'social_links'      => ['Social Link'],
                        ],
                    ],
                    'break2'     => null,
                    'star_rates' => ['Star Rate'],
                ],
            ],
            'configs'      => [
                'Configs',
                'children' => [
                    'attachment_types'       => ['Attachment Type'],
                    'model_attachment_types' => ['Models Attachment Types'],
                    'attachments'            => ['Attachment'],
                ],
            ],
            'seo'          => [
                'SEO',
                'seo_details'         => ['Seo Detail'],
            ],
            'interface'    => [
                'Interface',
                'children' => [
                    'homepage_sliders' => ['Homepage Sliders'],
                    'homepage_futures' => ['Homepage Futures'],
                    'web_pages'        => ['Web Pages'],
                    'menus'            => [
                        'Menus',
                        'children' => [
                            'menu_placeholders' => ['Menu Placeholders'],
                            'menus'             => ['Menu'],
                        ],
                    ],
                ],
            ],
            'logs'         => ['Log'],
            'break5'       => null,
            'users'        => ['User'],
            'reviews'      => ['Reviews'],
            'comments'     => ['Comments'],
        ];

        $this->truncate();

        $this->placeholder = MenuPlaceholder::create([
            'panel_id' => $this->panel->id,
            'title'    => self::PLACEHOLDER,
        ]);

        $s = 0;
        $parent = null;
        foreach ($sidebar as $k => $item) {
            $this->createItem(null, $item, $s, $k);
        }

    }

    protected function createItem($parent, $item, &$s, $k)
    {
        if (is_null($item)) { // Separator - Level 0
            Menu::create([
                'placeholder_id' => $this->placeholder->id,
                'parent_id'      => $parent->id ?? null,
                'slug'           => null,
                'title'          => null,
                'order'          => $s++,
                'icon'           => null,
                'active'         => true
            ]);
            return;
        }

        if (isset($item['children'])) {// Have Child - Level 0
            $parent = Menu::create([
                'placeholder_id' => $this->placeholder->id,
                'parent_id'      => $parent->id ?? null,
                'slug'           => null,
                'title'          => $item[0],
                'order'          => $s++,
                'icon'           => (!isset($item[1])) ? self::DEFAULT_ICON : $item[1],
                'active'         => true
            ]);
            foreach ($item['children'] as $j => $subItem) {
                $this->createItem($parent, $subItem, $s, $j);
            }
            return;
        }

        // No Child - Level 0
        Menu::create([
            'placeholder_id' => $this->placeholder->id,
            'parent_id'      => $parent->id ?? null,
            'slug'           => null,
            'title'          => $item[0],
            'url_type'       => (string)config_key('enums.menus_url_type', 'route'),
            'url'            => self::PANEL . '.' . ($item[2] ?? "$k.index"),
            'order'          => $s++,
            'icon'           => (!isset($item[1])) ? self::DEFAULT_ICON : $item[1],
            //                    'badge_class'=>(!isset($item[3]))?null:$item[3],
            //                    'badge_php_code'=>(!isset($item[2]))?null:$item[2],
            'active'         => true
        ]);

    }
}
