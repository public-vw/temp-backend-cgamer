<?php

namespace Database\Seeders;

use App\Models\Currency;
use Illuminate\Database\Seeder;

class CurrenciesSeeder extends Seeder
{
    public function run()
    {
        $base_currencies = [
            'Euro'   => ['EUR', '€', 1, true],
            'Dollar' => ['USD', '$', 1.2],
            'Lira'   => ['TL',  '₺', 9.9],
            'Toman'  => ['IRT', 'تومان', 33000],
        ];

        $order = 1;
        foreach ($base_currencies as $title => $details)
            Currency::create([
                'title'          => $title,
                'code'           => $details[0],
                'sign'           => $details[1],
                'is_origin'      => $details[3] ?? false,
                'each_to_origin' => $details[2],
            ]);
    }
}
