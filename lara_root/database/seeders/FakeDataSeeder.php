<?php

namespace Database\Seeders;

use Database\Seeders\FactoringSeeders\ArticleCategorySeeder;
use Database\Seeders\FactoringSeeders\ArticleSeeder;
use Database\Seeders\FactoringSeeders\AuthorSeeder;
use Database\Seeders\FactoringSeeders\CitySeeder;
use Database\Seeders\FactoringSeeders\FollowSeeder;
use Database\Seeders\FactoringSeeders\PaymentSeeder;
use Database\Seeders\FactoringSeeders\ProvinceSeeder;
use Database\Seeders\FactoringSeeders\RegionSeeder;
use Database\Seeders\FactoringSeeders\UserSeeder;
use Illuminate\Database\Seeder;

class FakeDataSeeder extends Seeder
{
    public function run()
    {
        $this->call(ProvinceSeeder::class, false, ['count' => config('fake-records.province')]);
        $this->call(CitySeeder::class, false, ['count' => config('fake-records.city')]);
        $this->call(RegionSeeder::class, false, ['count' => config('fake-records.region')]);

        $this->call(UserSeeder::class, false, ['count' => config('fake-records.user')]);
        $this->call(FollowSeeder::class, false, ['count' => config('fake-records.follow')]);
        $this->call(AuthorSeeder::class, false, ['count' => config('fake-records.author')]);
        $this->call(ArticleCategorySeeder::class, false, ['count' => config('fake-records.category')]);
        $this->call(ArticleSeeder::class, false, ['count' => config('fake-records.article')]);

        $this->call(PaymentSeeder::class, false, ['count' => config('fake-records.payment')]);
    }
}
