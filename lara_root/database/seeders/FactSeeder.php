<?php

namespace Database\Seeders;

use App\Models\Fact;
use Illuminate\Database\Seeder;

class FactSeeder extends Seeder
{
    public function run()
    {
        $items = [
            'تعداد بازیکن‌های فعال بازی تتان‌آرنا از مرز ۲۲ میلیون نفر هم گذشت !',
            'بیل‌گیتس گفته؛ در آینده نزدیک، شغل شما بازی‌کردن خواهد شد',
        ];

        foreach ($items as $item) {
            Fact::create([
                'sentence'    => $item,
                'probability' => 5,
                'active'      => true,
            ]);
        }
    }
}
