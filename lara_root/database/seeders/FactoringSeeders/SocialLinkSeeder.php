<?php

namespace Database\Seeders\FactoringSeeders;

use App\Models\SocialLink;
use Illuminate\Database\Seeder;

class SocialLinkSeeder extends FactoringSeeder
{
    protected string $model = \App\Models\SocialLink::class;
}
