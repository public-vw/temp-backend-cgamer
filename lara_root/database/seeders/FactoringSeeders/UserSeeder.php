<?php

namespace Database\Seeders\FactoringSeeders;

use App\Models\ModelAttachmentType;
use Illuminate\Database\Eloquent\Relations\Relation;

class UserSeeder extends FactoringSeeder
{
    protected string $model = \App\Models\User::class;

    public function postRun($fakerRecord)
    {
        $related = new _RelatedItems($fakerRecord);

        $related->makeAttachment(1);
    }
}
