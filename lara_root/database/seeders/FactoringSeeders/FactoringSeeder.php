<?php


namespace Database\Seeders\FactoringSeeders;

use App\Models\FakeRecord;
use Illuminate\Database\Seeder;

abstract class FactoringSeeder extends Seeder
{
    protected string $model;

    public function run($count = 1, $faker_id = null, $dependency = null)
    {
        for ($i = 0; $i < $count; $i++) {
            $item = (new $this->model)->factory()->create($dependency ?? null);

            $fakeRecord = FakeRecord::create([
                'model'     => $this->model,
                'record_id' => $item->id,
                'parent_id' => $faker_id,
            ]);

            if(method_exists(static::class,'postRun')){
                $this->postRun($fakeRecord);
            }
        }

    }

}
