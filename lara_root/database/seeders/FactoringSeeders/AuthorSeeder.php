<?php

namespace Database\Seeders\FactoringSeeders;

class AuthorSeeder extends FactoringSeeder
{
    protected string $model = \App\Models\Author::class;

    public function postRun($fakerRecord){
        $relations = new _RelatedItems($fakerRecord);

        $relations->makeAddress(1);
        $relations->makePhone(1,1);
        $relations->makeEmail(1);
        $relations->makeSocialLink(1);

//        $relations->makeAttachment(5);
        $relations->makeComment(10);
        $relations->makeStarRate(20);

    }
}

