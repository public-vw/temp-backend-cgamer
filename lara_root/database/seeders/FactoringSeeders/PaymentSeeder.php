<?php

namespace Database\Seeders\FactoringSeeders;

class PaymentSeeder extends FactoringSeeder
{
    protected string $model = \App\Models\Payment::class;

    public function postRun($fakerRecord){
        $relations = new _RelatedItems($fakerRecord);

        $relations->makeComment(3);
    }
}
