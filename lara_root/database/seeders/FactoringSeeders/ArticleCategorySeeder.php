<?php

namespace Database\Seeders\FactoringSeeders;

class ArticleCategorySeeder extends FactoringSeeder
{
    protected string $model = \App\Models\ArticleCategory::class;
}
