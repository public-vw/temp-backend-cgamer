<?php

namespace Database\Seeders\FactoringSeeders;

class CommentSeeder extends FactoringSeeder
{
    protected string $model = \App\Models\Comment::class;

    public function postRun($fakerRecord)
    {
        $related = new _RelatedItems($fakerRecord);

        $related->makeStarRate(5);
    }
}
