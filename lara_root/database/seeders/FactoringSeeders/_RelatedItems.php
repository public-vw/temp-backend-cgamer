<?php


namespace Database\Seeders\FactoringSeeders;


use App\Models\ModelAttachmentType;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Seeder;

class _RelatedItems extends Seeder
{
    protected $fakerRecord;

    const SILENCE = false;

    public function __construct($fakerRecord)
    {
        $this->fakerRecord = $fakerRecord;
    }

    public function makeAddress($max, $min = 0): void
    {
        $this->call(ContactAddressSeeder::class, self::SILENCE, [
            rand($min, $max),
            $this->fakerRecord->id,
            [
                'addressable_type' => $this->modelToMorphMap(),
                'addressable_id'   => $this->fakerRecord->record_id,
            ],
        ]);
    }

    protected function modelToMorphMap(): ?string
    {
        return array_search($this->fakerRecord->model, Relation::$morphMap) ?: null;
    }

    public function makeEmail($max, $min = 0): void
    {
        $this->call(ContactEmailSeeder::class, self::SILENCE, [
            rand($min, $max),
            $this->fakerRecord->id,
            [
                'emailable_type' => $this->modelToMorphMap(),
                'emailable_id'   => $this->fakerRecord->record_id,
            ],
        ]);
    }

    public function makePhone($max, $min = 0): void
    {
        $this->call(ContactPhoneSeeder::class, self::SILENCE, [
            rand($min, $max),
            $this->fakerRecord->id,
            [
                'phonable_type' => $this->modelToMorphMap(),
                'phonable_id'   => $this->fakerRecord->record_id,
            ],
        ]);
    }

    public function makeSocialLink($max, $min = 0): void
    {
        $this->call(SocialLinkSeeder::class, self::SILENCE, [
            rand($min, $max),
            $this->fakerRecord->id,
            [
                'socialable_type' => $this->modelToMorphMap(),
                'socialable_id'   => $this->fakerRecord->record_id,
            ],
        ]);
    }

    public function makeAttachment($max, $min = 0): void
    {
        $types = ModelAttachmentType::where('model_name', $this->modelToJustName())->get(['attachment_type_id'])->pluck('attachment_type_id')->toArray();
        $this->call(AttachmentSeeder::class, self::SILENCE, [
            rand($min, $max),
            $this->fakerRecord->id,
            [
                'attachmentable_type' => $this->modelToMorphMap(),
                'attachmentable_id'   => $this->fakerRecord->record_id,
                'type_id'             => count($types) ? array_rand(array_flip($types)) : null,
            ],
        ]);
    }

    public function makeComment($max, $min = 0): void
    {
        $this->call(CommentSeeder::class, self::SILENCE, [
            rand($min, $max),
            $this->fakerRecord->id,
            [
                'commentable_type' => $this->modelToMorphMap(),
                'commentable_id'   => $this->fakerRecord->record_id,
            ],
        ]);
    }

    public function makeStarRate($max, $min = 0): void
    {
        $this->call(StarRateSeeder::class, self::SILENCE, [
            rand($min, $max),
            $this->fakerRecord->id,
            [
                'rankable_type' => $this->modelToMorphMap(),
                'rankable_id'   => $this->fakerRecord->record_id,
            ],
        ]);
    }

    protected function modelToJustName(): ?string
    {
        $model = $this->fakerRecord->model;
        return substr($model, strrpos($model, '\\') + 1);
    }

}
