<?php

namespace Database\Seeders\FactoringSeeders;

use App\Models\ContactEmail;
use Illuminate\Database\Seeder;

class ContactEmailSeeder extends FactoringSeeder
{
    protected string $model = \App\Models\ContactEmail::class;
}
