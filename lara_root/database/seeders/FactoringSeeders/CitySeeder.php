<?php

namespace Database\Seeders\FactoringSeeders;

class CitySeeder extends FactoringSeeder
{
    protected string $model = \App\Models\City::class;
}
