<?php

namespace Database\Seeders\FactoringSeeders;

class AttachmentSeeder extends FactoringSeeder
{
    protected string $model = \App\Models\Attachment::class;
}
