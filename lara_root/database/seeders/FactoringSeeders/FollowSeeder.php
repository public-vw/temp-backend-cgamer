<?php

namespace Database\Seeders\FactoringSeeders;

class FollowSeeder extends FactoringSeeder
{
    protected string $model = \App\Models\Follow::class;

    public function postRun($fakerRecord){
        $relations = new _RelatedItems($fakerRecord);
    }
}
