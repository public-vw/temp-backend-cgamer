<?php

namespace Database\Seeders\FactoringSeeders;

class ArticleSeeder extends FactoringSeeder
{
    protected string $model = \App\Models\Article::class;

    public function postRun($fakerRecord){
        $relations = new _RelatedItems($fakerRecord);

        $relations->makeComment(10);
    }
}
