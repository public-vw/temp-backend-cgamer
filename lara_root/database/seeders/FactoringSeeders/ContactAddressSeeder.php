<?php

namespace Database\Seeders\FactoringSeeders;

use App\Models\ContactAddress;
use Illuminate\Database\Seeder;

class ContactAddressSeeder extends FactoringSeeder
{
    protected string $model = \App\Models\ContactAddress::class;
}
