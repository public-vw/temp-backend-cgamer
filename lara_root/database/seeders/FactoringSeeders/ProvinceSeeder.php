<?php

namespace Database\Seeders\FactoringSeeders;

class ProvinceSeeder extends FactoringSeeder
{
    protected string $model = \App\Models\Province::class;
}
