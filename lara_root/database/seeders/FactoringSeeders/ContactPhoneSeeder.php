<?php

namespace Database\Seeders\FactoringSeeders;

use App\Models\ContactPhone;
use Illuminate\Database\Seeder;

class ContactPhoneSeeder extends FactoringSeeder
{
    protected string $model = \App\Models\ContactPhone::class;
}
