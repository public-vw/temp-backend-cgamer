<?php

namespace Database\Seeders\FactoringSeeders;

class RegionSeeder extends FactoringSeeder
{
    protected string $model = \App\Models\Region::class;
}
