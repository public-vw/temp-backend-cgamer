<?php

namespace Database\Seeders;

use App\Models\SocialChannel;
use Illuminate\Database\Seeder;

class SocialChannelSeeder extends Seeder
{
    public $channels = [
        ['title' => 'Facebook',     'url' => 'www.facebook.com',    'active' => 1],
        ['title' => 'Twitter',      'url' => 'www.twitter.com',     'active' => 1],
        ['title' => 'Instagram',    'url' => 'www.instagram.com',   'active' => 1],
        ['title' => 'Telegram',     'url' => 'www.telegram.org',    'active' => 1],
        ['title' => 'YouTube',      'url' => 'www.youtube.com',     'active' => 1],
        ['title' => 'TikTok',       'url' => 'www.tiktok.com',      'active' => 1],
        ['title' => 'LinkedIn',     'url' => 'www.linkedin.com',    'active' => 1],
    ];

    public function run()
    {
        foreach ($this->channels as $channel){
            SocialChannel::create($channel);
        }
    }
}
