<?php

namespace Database\Seeders;

use App\Models\Domain;
use App\Models\SiteBrand;
use Illuminate\Database\Seeder;

class SiteBrandSeeder extends Seeder
{
    public function run()
    {
        $items = [
            'site_title'       => [
                'Site Title',
                'Crypto Gamers',
                'text',
                'ltr',
                'en',
                'Site title in the interface',
            ],
            'logo_normal'      => [
                'Logo | Normal',
                null,
                'image',
                'ltr',
                'en',
            ],
            'logo_transparent' => [
                'Logo | Transparent',
                null,
                'image',
                'ltr',
                'en',
            ],
            'footer_copyright' => [
                'Copyright on Footer',
                '2021 © {{ site_title }} by <a href="https://datarivers.org" target="_blank">Datarivers Team</a>',
                'html',
                'ltr',
                'en',
                'The copyright content that places under main footer',
            ],
        ];

        $domains = Domain::all();

        foreach($domains as $domain){
            $order = 0;
            foreach ($items as $slag => $details) {
                SiteBrand::create([
                    'domain_id'   => $domain->id,
                    'title'       => $details[0],
                    'slug'        => $slag,
                    'value'       => $details[1],
                    'order'       => $order++,
                    'lang'        => $details[4] ?? 'en',
                    'direction'   => (string)config_key('enums.settings_direction', $details[3]),
                    'mode'        => (string)config_key('enums.settings_mode', $details[2]),
                    'description' => $details[5] ?? null,
                ]);
            }
        }
    }
}
