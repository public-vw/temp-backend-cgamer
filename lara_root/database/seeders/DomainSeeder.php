<?php

namespace Database\Seeders;

use App\Models\Domain;
use Illuminate\Database\Seeder;

class DomainSeeder extends Seeder
{
    public function run()
    {
        $domains = [
            'English Domain' => 'cryptogamers.info',
            'Persian Domain' => 'cryptobaz.info',
        ];

        foreach ($domains as $title => $domain) {
            Domain::create([
                'title'          => $title,
                'domain_address' => $domain,
                'active'         => true,
            ]);
        }
    }
}
