<?php

namespace Database\Seeders;

use App\Models\Menu;
use App\Models\MenuPlaceholder;
use App\Models\Panel;
use Illuminate\Database\Seeder;

class AuthorPanelSidebarSeeder extends Seeder
{
    const DEFAULT_ICON = 'icon ion-ios-arrow-forward';
    const PANEL = 'author';

    protected $panel;
    protected $placeholder;

    public function __construct()
    {
        $this->panel = Panel::where('title',self::PANEL)->first();
    }

    public function run()
    {
        $sidebar = [
            'dashboard'   => ['Dashboard', 'fa fa-home', 'home'],
        ];

        $this->placeholder = MenuPlaceholder::create([
            'panel_id' => $this->panel->id,
            'title' => 'sidebar',
        ]);

        $s = 0;
        $parent = null;
        foreach ($sidebar as $k => $item) {
            $this->createItem(null, $item, $s, $k);
        }

    }

    protected function createItem($parent, $item, &$s, $k)
    {
        if (is_null($item)) { // Separator - Level 0
            Menu::create([
                'placeholder_id' => $this->placeholder->id,
                'parent_id'      => $parent->id ?? null,
                'slug'           => null,
                'title'          => null,
                'order'          => $s++,
                'icon'           => null,
                'active'         => true
            ]);
            return;
        }

        if (isset($item['children'])) {// Have Child - Level 0
            $parent = Menu::create([
                'placeholder_id' => $this->placeholder->id,
                'parent_id'      => $parent->id ?? null,
                'slug'           => null,
                'title'          => $item[0],
                'order'          => $s++,
                'icon'           => (!isset($item[1])) ? self::DEFAULT_ICON : $item[1],
                'active'         => true
            ]);
            foreach ($item['children'] as $j => $subItem) {
                $this->createItem($parent, $subItem, $s, $j);
            }
            return;
        }

        // No Child - Level 0
        Menu::create([
            'placeholder_id' => $this->placeholder->id,
            'parent_id'      => $parent->id ?? null,
            'slug'           => null,
            'title'          => $item[0],
            'url_type'       => (string)config_key('enums.menus_url_type', 'route'),
            'url'            => self::PANEL . '.' . ($item[2] ?? "$k.index"),
            'order'          => $s++,
            'icon'           => (!isset($item[1])) ? self::DEFAULT_ICON : $item[1],
            //                    'badge_class'=>(!isset($item[3]))?null:$item[3],
            //                    'badge_php_code'=>(!isset($item[2]))?null:$item[2],
            'active'         => true
        ]);

    }
}
