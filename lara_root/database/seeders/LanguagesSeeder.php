<?php

namespace Database\Seeders;

use App\Models\Language;
use Illuminate\Database\Seeder;

class LanguagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $base_languages = [
            'English' => ['en', 'ltr', 'gb'],
            'Persian' => ['fa', 'rtl', 'ir'],
        ];

        $order = 1;
        foreach ($base_languages as $title => $details)
            Language::create([
                'title'     => $title,
                'code'      => $details[0],
                'flag_iso'  => $details[2],
                'direction' => $details[1],
                'order'     => $order++,
                'active'    => true,
            ]);
    }
}
