<?php

namespace Database\Seeders;

use App\Models\Panel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use \Spatie\Permission\Models\Role;

class RolePermissionSeeder extends Seeder
{
    protected $guards = ['web', 'api'];
    protected $cruds = ['view', 'edit', 'create', 'delete'];
    protected $territories = ['all', 'branch', 'own'];

    protected $roles = [];
    protected $default_role_map = [
        'admin'             => [
            'ArticleCategory'      => 'aaaa',
            'ArticleLabelGroup'    => 'aaaa',
            'ArticleLabel'         => 'aaaa',
            'Article'              => 'aaaa',
            'ArticleTag'           => 'aaaa',
            'Attachment'           => 'aaaa',
            'AttachmentType'       => 'aaaa',
            'Author'               => 'aaaa',
            'Badge'                => 'aaaa',
            'BotConnection'        => '----',
            'BotGasfeeAlarm'       => '----',
            'BotUser'              => 'aa-a',
            'City'                 => 'aaaa',
            'Comment'              => 'aaaa',
            'ContactAddress'       => 'aaaa',
            'ContactEmail'         => 'aaaa',
            'ContactPhone'         => 'aaaa',
            'Country'              => 'aaaa',
            'Currency'             => 'aaaa',
            'Domain'               => 'a---',
            'Fact'                 => 'aaaa',
            'FailedJob'            => '----',
            'FakeRecord'           => 'a---',
            'HomepageFuture'       => 'aaaa',
            'HomepageSlider'       => 'aaaa',
            'Hyperlink'            => 'aaaa',
            'Important'            => 'aa--',//not create other users
            'Language'             => 'aa--',
            'Log'                  => 'a---',
            'Menu'                 => 'aaaa',//not for sysadmin
            'MenuPlaceholder'      => 'aaaa',//not for sysadmin
            'ModelAttachmentType'  => 'aaaa',
            'Panel'                => 'a---',//not for sysadmin
            'Payment'              => 'aaaa',
            'Province'             => 'aaaa',
            'ReadFlag'             => 'aa--',//not change other users
            'Region'               => 'aaaa',
            'Request'              => 'aaaa',
            'Review'               => 'aaaa',
            'Role'                 => '----',
            'SeoDetail'            => 'aaaa',
            'ServerConfig'         => '----',
            'SettingGroup'         => '----',
            'Setting'              => '----',
            'SiteBrand'            => '----',
            'SocialChannel'        => 'aaaa',
            'SocialLink'           => 'aaaa',
            'StarRate'             => 'aaaa',
            'Structure'            => '----',
            'TelegramRobotMessage' => 'aaaa',
            'Translate'            => 'aaaa',
            'UrlRedirect'          => 'aaaa',
            'UserMeta'             => 'aaaa',
            'UserOauth'            => 'aaaa',
            'User'                 => 'aaaa',//not for sysadmins and other admins
            'WebPage'              => 'aaaa',
        ],
        'author_supervisor' => [
            'ArticleCategory'   => 'abb-',
            'ArticleLabelGroup' => 'abb-',
            'ArticleLabel'      => 'abb-',
            'Article'           => 'abb-',
            'ArticleTag'        => 'abb-',
            'Attachment'        => 'bbo-',
            'Author'            => 'abb-',
            'Comment'           => 'bbb-',
            'Hyperlink'         => 'bbb-',
            'Important'         => 'bbbb',
            'Request'           => 'booo',
            'Review'            => 'bbb-',
            'UserMeta'          => 'boo-',
            'User'              => 'bo--',
        ],
        'author'            => [
            'ArticleCategory'   => 'aoo-',
            'ArticleLabelGroup' => 'a---',
            'ArticleLabel'      => 'a---',
            'Article'           => 'aoo-',
            'ArticleTag'        => 'aoo-',
            'Attachment'        => 'ooo-',
            'Comment'           => 'oo--',
            'Hyperlink'         => 'ooo-',
            'Important'         => 'oooo',
            'Request'           => 'oooo',
            'Review'            => 'oo--',
            'UserMeta'          => 'ooo-',
            'User'              => 'oo--',
        ],
    ];


    protected function truncateAll()
    {
        $tableNames = config('permission.table_names');
        if (empty($tableNames)) {
            throw new \Exception('Error: config/permission.php not loaded. Run [php artisan config:clear] and try again.');
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        foreach ($tableNames as $key => $tableName) {
            if ($key == 'model_has_roles') continue;
            DB::table($tableName)->truncate();
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

    public function run()
    {
        $this->truncateAll();

        foreach ($this->guards as $guard) {
            $this->command->comment("=========== guard: {$guard}");

            $this->roles['sysadmin'][$guard] = Role::create(['guard_name' => $guard, 'name' => "sysadmin@{$guard}"]);

            $this->roles['admin'][$guard] = Role::create(['guard_name' => $guard, 'name' => "admin@{$guard}"]);
            if($guard == 'web') $this->roles['admin'][$guard]->update(['panel_id' => Panel::where('title', 'admin')->first()->id]);

            $this->roles['author_supervisor'][$guard] = Role::create(['guard_name' => $guard,
                                                                      'name'       => "author_supervisor@{$guard}"]);
            if($guard == 'web') $this->roles['author_supervisor'][$guard]->update(['panel_id' => Panel::where('title', 'admin')->first()->id]);

            $this->roles['author'][$guard] = Role::create(['guard_name' => $guard, 'name' => "author@{$guard}"]);
            $this->roles['author'][$guard]->update(['panel_id' => Panel::where('title', 'author')->first()->id]);

            $this->addPermissions($guard);
        }

    }

    protected function addPermissions($guard)
    {
        $models = getModels();
        foreach ($models as $model) {
            $table = model2table($model);

            $this->command->comment("table: {$table}");

            foreach ($this->cruds as $crud) {
                foreach ($this->territories as $territory) {
                    $perm = "{$table}.{$crud}.{$territory}";
                    Permission::create(['guard_name' => $guard, 'name' => $perm]);
                    $this->connectPermissionToRoles($guard, $perm);
                }
            }
        }
    }

    protected function connectPermissionToRoles($guard, $perm)
    {
        list($table, $crud, $territory) = explode('.', $perm);
        $crud_index = array_search($crud, $this->cruds);
        $territory = $territory[0];

        foreach ($this->roles as $name => $role) {
            if ($name == 'sysadmin') {
                $role[$guard]->givePermissionTo($perm);
                continue;
            }
            if (!isset($this->default_role_map[$name][table2model($table)])) continue;
            $signs = str_split($this->default_role_map[$name][table2model($table)]);
            if ($signs[$crud_index] == $territory) {
                $role[$guard]->givePermissionTo($perm);
                $this->command->comment("Role [{$role[$guard]->name}@{$guard}] granted Permission [{$perm}]");
            }
        }
    }
}
