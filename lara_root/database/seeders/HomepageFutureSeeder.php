<?php

namespace Database\Seeders;

use App\Models\HomepageFuture;
use Illuminate\Database\Seeder;

class HomepageFutureSeeder extends Seeder
{
    public function run()
    {
        $items = [
        ];

        foreach ($items as $item) {
            HomepageFuture::create([
                'title'    => $item,
            ]);
        }
    }
}
