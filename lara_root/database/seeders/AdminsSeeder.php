<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Hash;

class AdminsSeeder extends Seeder
{
    public function run()
    {
        $this->createSysadmin();
        $this->createAdmin();
    }

    protected function createSysadmin(): void
    {
        $u = User::create([
            'name'               => 'System Administrator',
            'username'           => 'sysadmin',
            'ref_code'           => null,
            'mobile'             => '+905528910289',
            'email'              => 'armin.ghassemi@gmail.com',
            'password'           => Hash::make('admin1234'),
            'mobile_verified_at' => \Carbon\Carbon::now(),
            'email_verified_at'  => \Carbon\Carbon::now(),

            'random_color' => (string)rand(0, count(config('enums.random_colors')) - 1),
            'telegram_uid' => '91173754',

            'use_two_factor_auth' => false,
            'google_secret_key'   => 'OX7K2OYA3ROGPTHW',
            'status'              => config_key('enums.users_status', 'active'),
        ]);

        $this->setRoles($u, ['sysadmin@web','sysadmin@api', 'admin@web','admin@api', 'author@web', 'author@api']);
        $u->badges()->attach(2);
    }

    protected function createAdmin(): void
    {
        $u = User::create([
            'name'               => 'Yousef Yas',
            'username'           => 'joe',
            'ref_code'           => null,
            'mobile'             => '+989394610489',
            'email'              => 'yousef.yas0080@gmail.com',
            'password'           => Hash::make('admin1234'),
            'mobile_verified_at' => \Carbon\Carbon::now(),
            'email_verified_at'  => \Carbon\Carbon::now(),

            'random_color' => (string)rand(0, count(config('enums.random_colors')) - 1),
            'telegram_uid' => '607160922',

            'use_two_factor_auth' => false,
            'google_secret_key'   => null,
            'status'              => config_key('enums.users_status', 'active'),
        ]);

        $this->setRoles($u, ['admin@api', 'author_supervisor@web']);
        $u->badges()->attach(2);
    }

    protected function setRoles(User $user, $roles)
    {
        foreach ($roles as $role) {
            $guard = last(explode('@',$role));
            $r = Role::where(['guard_name' => $guard, 'name' => $role])->first();
            $user->assignRole($r);
        }
    }
}
