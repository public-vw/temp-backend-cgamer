<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(StructureSeeder::class);
        $this->call(LanguagesSeeder::class);
        $this->call(CurrenciesSeeder::class);
//        $this->call(SettingsSeeder::class);

        $this->call(PanelSeeder::class);
        $this->call(RolePermissionSeeder::class);
        $this->call(DomainSeeder::class);
        $this->call(SiteBrandSeeder::class);
        $this->call(BadgeSeeder::class);
        $this->call(AdminsSeeder::class);
        $this->call(AdminPanelSidebarSeeder::class);
        $this->call(AuthorPanelSidebarSeeder::class);
        $this->call(ServerConfigSeeder::class);

        $this->call(AttachmentTypeSeeder::class);

        $this->call(InterfaceTopMenuSeeder::class);
        $this->call(InterfaceTopMenuMobileSeeder::class);

        $this->call(SocialChannelSeeder::class);
        $this->call(BotConnectionSeeder::class);

        $this->call(CountrySeeder::class);

        if((bool)config('app.fake_data',false) === true){
            $this->call(FakeDataSeeder::class);
        }
    }
}
