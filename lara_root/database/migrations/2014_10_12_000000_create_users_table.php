<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{

    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');

            $table->string('username');
            $table->string('password');
            $table->string('ref_code',20)->nullable()->unique();

            $table->string('mobile')->unique()->nullable();
            $table->timestamp('mobile_verified_at')->nullable();
            $table->string('email')->unique()->nullable();
            $table->timestamp('email_verified_at')->nullable();

            $table->enum('random_color', config_keys_all('enums.random_colors'));
// attachment: avatar_id

            $table->string('telegram_uid',20)->nullable()->default(null);
            $table->unsignedInteger('referer_id')->nullable()->index();
//            $table->foreign('referer_id')->nullable()->index()->refrences('id')->on('users')->onDelete('set null');

            $table->string('google_secret_key',20)->nullable();
            $table->boolean('use_two_factor_auth')->nullable()->default(null);

            $table->enum('status', config_keys_all('enums.users_status'))->default(config('enums.users_status_default'));

            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('users');
    }
}
