<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuthorsTable extends Migration
{

    public function up()
    {
        Schema::create('authors', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->nullable()->default(NULL)->constrained('users','id')->nullOnDelete();

            $table->unsignedInteger('article_limit')->default(3);
            $table->enum('status', config_keys_all('enums.authors_status'))->default(config('enums.authors_status_default'));

            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('authors');
    }
}
