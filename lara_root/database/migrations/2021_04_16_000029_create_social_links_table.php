<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocialLinksTable extends Migration
{

    public function up()
    {
        Schema::create('social_links', function (Blueprint $table) {
            $table->engine = 'MyISAM';

            $table->id();
            $table->morphs('socialable');

            $table->string('title')->nullable()->default(null);

            $table->foreignId('channel_id')->nullable()->constrained('social_channels');
            $table->mediumText('connection_string');

            $table->unsignedSmallInteger('order')->default(0);

            $table->text('description')->nullable()->default(null)->comment('اطلاعات اضافی در مورد ایمیل');

            $table->boolean('default')->default(false);
            $table->boolean('active')->default(true);

            $table->timestamp('social_verified_at')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('social_links');
    }
}
