<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSummaryToArticleCategoriesTable extends Migration
{
    public function up()
    {
        Schema::table('article_categories', function (Blueprint $table) {
            $table->text('summary')->nullable()->after('description');
        });
    }

    public function down()
    {
        Schema::table('article_categories', function (Blueprint $table) {
            $table->dropColumn('summary');
        });
    }
}
