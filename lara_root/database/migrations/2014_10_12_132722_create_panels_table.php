<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePanelsTable extends Migration
{

    public function up()
    {
        Schema::create('panels', function (Blueprint $table) {
            $table->engine = "MyIsam";

            $table->id();
            $table->string('title',50)->unique()->comment('use snake_case');
            $table->boolean('active')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('panels');
    }
}
