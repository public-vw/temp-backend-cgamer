<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountriesTable extends Migration
{

    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->engine = 'MyISAM';

            $table->id();

            $table->string('title');
            $table->string('phone_precode',10)->unique()->nullable()->default(NULL);
            $table->string('flag_iso',5)->unique()->nullable()->default(null);
            $table->string('timezone',10)->nullable()->default(null);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
