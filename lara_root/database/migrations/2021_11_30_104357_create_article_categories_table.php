<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticleCategoriesTable extends Migration
{
    public function up()
    {
        Schema::create('article_categories', function (Blueprint $table) {
            $table->id();
            $table->foreignId('parent_id')->nullable()->default(null)->constrained('article_categories')->nullOnDelete();

            $table->string('title');
            $table->enum('status', config_keys_all('enums.article_categories_status'))->default(config('enums.article_categories_status_default'));
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('article_categories');
    }
}
