<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReadFlagsTable extends Migration
{

    public function up()
    {
        Schema::create('read_flags', function (Blueprint $table) {
            $table->id();
            $table->morphs('readable');
            $table->foreignId('user_id')->nullable()->constrained()->nullOnDelete();
            $table->boolean('read_flag')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('read_flags');
    }
}
