<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeoDetailsTable extends Migration
{

    public function up()
    {
        Schema::create('seo_details', function (Blueprint $table) {
            $table->engine = 'MyISAM';

            $table->id();
            $table->morphs('seoable');
            $table->enum('type', config_keys_all('enums.seo_details_type'))->default(config('enums.seo_details_type_default'));
            $table->string('title')->nullable();
            $table->text('keywords')->nullable();
            $table->longText('description')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('seo_details');
    }
}
