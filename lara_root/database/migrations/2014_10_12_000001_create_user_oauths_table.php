<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserOauthsTable extends Migration
{
    public function up()
    {
        Schema::create('user_oauths', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained()->cascadeOnDelete();

            $table->string('twitter_id')->nullable()->default(null);
            $table->string('facebook_id')->nullable()->default(null);
            $table->string('instagram_id')->nullable()->default(null);
            $table->string('telegram_id')->nullable()->default(null);
            $table->string('google_id')->nullable()->default(null);
            $table->string('youtube_id')->nullable()->default(null);
            $table->string('apple_id')->nullable()->default(null);
            $table->string('github_id')->nullable()->default(null);
            $table->string('gitlab_id')->nullable()->default(null);
            $table->string('linkedin_id')->nullable()->default(null);
            $table->string('discord_id')->nullable()->default(null);
            $table->string('steam_id')->nullable()->default(null);
            $table->string('twitch_id')->nullable()->default(null);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_oauths');
    }
}
