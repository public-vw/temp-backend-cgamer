<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->id();

            $table->string('slug',150)->nullable()->default(NULL);
            $table->foreignId('master_id')->nullable()->default(NULL)->constrained('articles','id')->cascadeOnDelete();

            $table->foreignId('author_id')->nullable()->constrained('authors')->nullOnDelete();
            $table->foreignId('category_id')->nullable()->constrained('article_categories','id')->nullOnDelete();

            $table->string('heading');
            $table->unsignedSmallInteger('reading_time')->nullable()->default(null)->comment('reading duration time in minute');
            $table->longText('content')->nullable()->default(null);
            $table->text('summary')->nullable();

            $table->enum('status', config_keys_all('enums.articles_status'))->default(config('enums.articles_status_default'));

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
