<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBotGasfeeAlarmsTable extends Migration
{
    public function up()
    {
        Schema::create('bot_gasfee_alarms', function (Blueprint $table) {
            $table->id();
            $table->foreignId('bot_user_id')->constrained('bot_users')->cascadeOnDelete();
            $table->unsignedInteger('threshold')->default(100);
            $table->boolean('fired')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('bot_gasfee_alarms');
    }
}
