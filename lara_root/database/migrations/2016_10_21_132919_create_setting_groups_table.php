<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingGroupsTable extends Migration
{

    public function up()
    {
        Schema::create('setting_groups', function (Blueprint $table) {
            $table->engine = "MyIsam";

            $table->id();
            $table->foreignId('parent_id')->constrained('settings')->cascadeOnDelete();
            $table->string('title',120);
            $table->string('slug',80)->unique();
            $table->unsignedSmallInteger('order')->default(100);
            $table->boolean('hidden')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('setting_groups');
    }
}
