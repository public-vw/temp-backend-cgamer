<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSlugToArticleCategoriesTable extends Migration
{
    public function up()
    {
        Schema::table('article_categories', function (Blueprint $table) {
            $table->string('slug',150)->nullable()->default(NULL)->after('id');
            $table->unique(['parent_id','slug']);
        });
    }

    public function down()
    {
        Schema::table('article_categories', function (Blueprint $table) {
//            $table->dropUnique(['parent_id','slug']);
            $table->removeColumn('slug');
        });
    }
}
