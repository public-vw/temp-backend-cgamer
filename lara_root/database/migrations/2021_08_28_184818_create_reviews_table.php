<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewsTable extends Migration
{

    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->id();

            $table->morphs('reviewable');//agency, property, ...

            $table->foreignId('author_id')->nullable()->constrained('users','id')->nullOnDelete();
            $table->longText('content');
            $table->enum('status', config_keys_all('enums.reviews_status'))->default(config('enums.reviews_status_default'));

            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
