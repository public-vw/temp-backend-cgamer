<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{

    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->engine = 'MyISAM';

            $table->id();
            $table->string('title', 120);
            $table->foreignId('group_id')->constrained('setting_groups')->cascadeOnDelete();
            $table->string('slug', 80);
            $table->text('value')->nullable();
            $table->unsignedSmallInteger('order')->default(100);
            $table->string('lang', 3)->nullable()->default('en');// null = All Other Languages that has no value
            $table->enum('direction', config_keys_all('enums.settings_direction'))->default(config('enums.settings_direction_default'));
            $table->enum('mode', config_keys_all('enums.settings_mode'))->default(config('enums.settings_mode_default'));
            $table->text('description')->nullable();
            $table->boolean('hidden')->default(false);
            $table->timestamps();

            $table->unique(['group_id', 'slug']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
