<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactAddressesTable extends Migration
{

    public function up()
    {
        Schema::create('contact_addresses', function (Blueprint $table) {
            $table->engine = 'MyISAM';

            $table->id();
            $table->morphs('addressable');

            $table->string('title')->nullable()->default(null);

            $table->unsignedSmallInteger('order')->default(0);

            $table->text('geocode')->comment('متن آدرس');
            $table->text('description')->nullable()->default(null)->comment('اطلاعات اضافی در مورد آدرس');

            $table->foreignId('city_id')->nullable()->default(null)->constrained()->nullOnDelete();
            $table->foreignId('region_id')->nullable()->default(null)->constrained()->nullOnDelete();

            $table->double('latitude',12,8)->nullable()->default(null);
            $table->double('longitude',12,8)->nullable()->default(null);
            $table->unsignedTinyInteger('map_zoom')->nullable()->default(null);

            $table->boolean('default')->default(false);
            $table->boolean('active')->default(true);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('contact_addresses');
    }
}
