<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequestsTable extends Migration
{

    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users')->cascadeOnDelete();

            $table->MediumText('subject');
            $table->text('description')->nullable()->default(null);

            $table->morphs('requestable');
            $table->string('field',100);

            $table->enum('priority', config_keys_all('enums.requests_priorities'))->default(config('enums.requests_priorities_default'));
            $table->enum('status', config_keys_all('enums.requests_status'))->default(config('enums.requests_status_default'));

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('requests');
    }
}
