<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUrlRedirectsTable extends Migration
{
    public function up()
    {
        Schema::create('url_redirects', function (Blueprint $table) {
            $table->engine = 'MyISAM';

            $table->id();

            $table->string('title',120)->nullable();
            $table->string('from');
            $table->string('to');
            $table->enum('state_code',['301','302'])->default('301');
            $table->boolean('start_with')->default(false);
            $table->boolean('external')->default(false);
            $table->boolean('active')->default(false);

            $table->timestamps();

            $table->unique(['from','active']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('url_redirects');
    }
}
