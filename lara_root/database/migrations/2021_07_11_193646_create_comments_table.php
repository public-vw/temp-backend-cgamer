<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->id();
            $table->morphs('commentable');
            $table->text('content');
            $table->foreignId('parent_id')->nullable()->default(NULL)
                ->constrained('comments')->cascadeOnUpdate()->nullOnDelete();
            $table->enum('status',config_keys_all('enums.comments_status'))->default(config('enums.comments_status_default'));

            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
