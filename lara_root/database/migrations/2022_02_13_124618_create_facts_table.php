<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFactsTable extends Migration
{
    public function up()
    {
        Schema::create('facts', function (Blueprint $table) {
            $table->engine = 'MyISAM';

            $table->id();
            $table->string('sentence');
            $table->unsignedTinyInteger('probability')->default(5)->comment('1 to 10, 10 means highest probability to show');
            $table->boolean('active')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('facts');
    }
}
