<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactPhonesTable extends Migration
{

    public function up()
    {
        Schema::create('contact_phones', function (Blueprint $table) {
            $table->engine = 'MyISAM';

            $table->id();
            $table->morphs('phonable');

            $table->string('title')->nullable()->default(null);
            $table->string('number',30);

            $table->unsignedSmallInteger('order')->default(0);

            $table->text('description')->nullable()->default(null)->comment('اطلاعات اضافی در مورد ایمیل');

            $table->foreignId('city_id')->nullable()->default(null)->constrained()->nullOnDelete();
            $table->foreignId('region_id')->nullable()->default(null)->constrained()->nullOnDelete();

            $table->boolean('default')->default(false);
            $table->boolean('active')->default(true);

            $table->timestamp('phone_verified_at')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('contact_phones');
    }
}
