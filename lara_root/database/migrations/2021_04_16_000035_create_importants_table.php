<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImportantsTable extends Migration
{

    public function up()
    {
        Schema::create('importants', function (Blueprint $table) {
            $table->engine = "MyIsam";

            $table->id();
            $table->morphs('importantable');
            $table->foreignId('user_id')->nullable()->constrained()->cascadeOnDelete();
            $table->boolean('active')->default(true);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('importants');
    }
}
