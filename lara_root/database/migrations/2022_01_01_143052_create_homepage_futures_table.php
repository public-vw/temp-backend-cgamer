<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHomepageFuturesTable extends Migration
{
    public function up()
    {
        Schema::create('homepage_futures', function (Blueprint $table) {
            $table->engine = "MyISAM";

            $table->id();
            $table->string('title',200);
            $table->tinyText('content')->nullable();
            $table->unsignedSmallInteger('order')->default(100);
            $table->boolean('active')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('homepage_futures');
    }
}
