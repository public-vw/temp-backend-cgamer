<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBotUsersTable extends Migration
{
    # shows which users are connected to which robots
    # users maybe has account on the site or not

    public function up()
    {
        Schema::create('bot_users', function (Blueprint $table) {
            $table->id();

            $table->foreignId('robot_id')->constrained('bot_connections')->cascadeOnDelete();
            $table->foreignId('user_id')->nullable()->default(null)->constrained('users')->cascadeOnDelete();
            $table->string('user_telegram_uid',20);
            $table->string('telegram_mobile',20)->nullable();
            $table->string('telegram_username',100)->nullable();
            $table->string('telegram_firstname',150)->nullable();
            $table->string('telegram_lastname',150)->nullable();
            $table->string('telegram_lang_code',5);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('bot_users');
    }
}
