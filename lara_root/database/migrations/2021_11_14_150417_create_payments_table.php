<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->engine = 'MyIsam';

            $table->id();

            $table->foreignId('user_id')->nullable()->constrained('users')->nullOnDelete();

            $table->string('title',200)->nullable();

            $table->string('date_on_paper',20);
            $table->unsignedBigInteger('amount');
            $table->foreignId('currency_id')->constrained('currencies')->nullOnDelete();

            $table->enum('status', config_keys_all('enums.payments_status'))->default(config('enums.payments_status_default'));


            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
