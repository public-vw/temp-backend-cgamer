<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFakeRecordsTable extends Migration
{

    public function up()
    {
        Schema::create('fake_records', function (Blueprint $table) {
            $table->id();
            $table->string('model');
            $table->foreignId('parent_id')->nullable()->default(null);
            $table->foreignId('record_id');
            $table->timestamp('created_at');
        });
    }

    public function down()
    {
        Schema::dropIfExists('fake_records');
    }
}
