<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStarRatesTable extends Migration
{

    public function up()
    {
        Schema::create('star_rates', function (Blueprint $table) {
            $table->id();
            $table->morphs('rankable');
            $table->foreignId('user_id')->nullable()->constrained('users')->nullOnDelete();
            $table->ipAddress('ip')->nullable();
            $table->smallInteger('rate')->unsigned()->default(5);
            $table->smallInteger('weight')->unsigned()->default(1);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('star_rates');
    }
}
