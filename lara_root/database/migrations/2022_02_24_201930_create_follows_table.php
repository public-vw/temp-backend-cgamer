<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFollowsTable extends Migration
{
    public function up()
    {
        Schema::create('follows', function (Blueprint $table) {
            $table->id();

            $table->foreignId('master_id')->nullable()->constrained('users','id')->cascadeOnDelete();
            $table->foreignId('slave_id')->nullable()->constrained('users','id')->cascadeOnDelete();
            $table->unique(['master_id','slave_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('follows');
    }
}
