<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHomepageSlidersTable extends Migration
{
    public function up()
    {
        Schema::create('homepage_sliders', function (Blueprint $table) {
            $table->engine = "MyISAM";

            $table->id();
            $table->string('title');
            $table->string('sub_title')->nullable();
//image as attachment
            $table->unsignedSmallInteger('order')->default(100);
            $table->boolean('active')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('homepage_sliders');
    }
}
