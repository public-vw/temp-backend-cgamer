<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiteBrandsTable extends Migration
{

    public function up()
    {
        Schema::create('site_brands', function (Blueprint $table) {
            $table->engine = 'MyISAM';

            $table->id();
            $table->foreignId('domain_id')->constrained('domains')->cascadeOnDelete();
            $table->string('title', 120);
            $table->string('slug', 80);
            $table->text('value')->nullable();
            $table->unsignedSmallInteger('order')->default(100);
            $table->string('lang', 3)->nullable()->default('en');// null = All Other Languages that has no value
            $table->enum('direction', config_keys_all('enums.settings_direction'))->default(config('enums.settings_direction_default'));
            $table->enum('mode', config_keys_all('enums.settings_mode'))->default(config('enums.settings_mode_default'));
            $table->text('description')->nullable();
            $table->timestamps();

            $table->unique(['domain_id', 'slug']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('site_brands');
    }
}
