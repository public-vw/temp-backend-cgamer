<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegionsTable extends Migration
{

    public function up()
    {
        Schema::create('regions', function (Blueprint $table) {
            $table->engine = 'MyISAM';

            $table->id();

            $table->string('title');
            $table->foreignId('city_id')->nullable()->default(null)->constrained()->nullOnDelete();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('regions');
    }
}
