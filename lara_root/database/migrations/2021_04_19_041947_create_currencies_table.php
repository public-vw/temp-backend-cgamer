<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCurrenciesTable extends Migration
{
    public function up()
    {
        Schema::create('currencies', function (Blueprint $table) {
            $table->engine = "MyISAM";

            $table->id();

            $table->string('title',20)->unique();
            $table->string('code',3)->unique();
            $table->string('sign',5);

            $table->boolean('is_origin')->default(FALSE);
            $table->unsignedDouble('each_to_origin',20,5);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currencies');
    }
}
