<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCitiesTable extends Migration
{

    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->engine = 'MyISAM';

            $table->id();

            $table->string('title');
            $table->foreignId('province_id')->nullable()->default(null)->constrained()->nullOnDelete();
            $table->string('phone_precode',10)->nullable()->default(NULL);
            $table->string('timezone',10)->nullable()->default(null);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('cities');
    }
}
