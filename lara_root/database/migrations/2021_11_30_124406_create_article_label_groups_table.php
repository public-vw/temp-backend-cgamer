<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticleLabelGroupsTable extends Migration
{
    public function up()
    {
        Schema::create('article_label_groups', function (Blueprint $table) {
            $table->id();
            $table->string('title')->unique();
            $table->unsignedSmallInteger('order')->default(100);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('article_label_groups');
    }
}
