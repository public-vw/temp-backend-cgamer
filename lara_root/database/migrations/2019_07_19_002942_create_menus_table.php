<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->engine = 'MyISAM';

            $table->id();
            $table->foreignId('placeholder_id')->nullable()->index()->constrained('menu_placeholders')->nullOnDelete();
            $table->foreignId('parent_id')->nullable()->default(NULL)->constrained('menus','id')->onDelete('set null');

            $table->string('slug',100)->nullable();
            $table->unique(['placeholder_id','slug']);

            $table->string('title',100)->nullable();
            $table->enum('url_type', config_keys_all('enums.menus_url_type'))->nullable()->default(NULL);
            $table->string('url')->nullable()->default(NULL);

            $table->unsignedTinyInteger('order')->default(0);

            $table->string('class',80)->nullable()->default(null);
            $table->string('style',120)->nullable()->default(null);

            #TODO: move icon default to configs
            $table->string('icon',30)->nullable()->default('icon ion-ios-arrow-forward');

            $table->boolean('go_new_tab')->default(false);
            $table->boolean('nofollow')->default(false);

            $table->boolean('active')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
