<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStructuresTable extends Migration
{
    public function up()
    {
        Schema::create('structures', function (Blueprint $table) {
            $table->id();
            $table->foreignId('parent_id')->nullable()->default(null)->constrained('structures')->cascadeOnDelete();
            $table->string('title', 80);
            $table->enum('type', ['model', 'field'])->default('model');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('structures');
    }
}
