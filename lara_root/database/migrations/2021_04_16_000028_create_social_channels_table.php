<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocialChannelsTable extends Migration
{

    public function up()
    {
        Schema::create('social_channels', function (Blueprint $table) {
            $table->engine = 'MyISAM';

            $table->id();
            $table->string('title',120);
            $table->string('url',100)->nullable();
            $table->boolean('active')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('social_channels');
    }
}
