<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('languages', function (Blueprint $table) {
            # $table->engine = "MyIsam";
            # Q: Why this is ignored now?
            # A: Mysql can not support foreign id to this table if not be innodb

            $table->id();
            $table->string('title',100);
            $table->string('code',3)->nullable()->default(NULL);
            $table->string('flag_iso',5)->unique()->nullable()->default(null);
            $table->enum('direction',['ltr','rtl']);
            $table->unsignedTinyInteger('order');
            $table->boolean('active')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('languages');
    }
}
