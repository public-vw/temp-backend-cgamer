<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttachmentTypesTable extends Migration
{

    public function up()
    {
        Schema::create('attachment_types', function (Blueprint $table) {
            $table->engine = 'MyISAM';

            $table->id();
            $table->string('title', 120);

            $table->string('slug', 125);
            $table->string('properties_limit')->nullable()->default(null);
            # Minimum & Maximum sizes, durations
            # [dur|10,100]                 // video and sound
            # [width|10]                   // fix width
            # [height|10]                  // fix height
            # [width|10,100]               // width between 10px and 100px
            # [width|10,~]                 // no upper bound
            # [width|~,10]                 // no lower bound
            # [box|10,100|20,-|-,300]      // smart boxing
            # [size|100]                   // max filesize in KB

            $table->string('folder', 50)->default('oth');
            $table->enum('type', config_keys_all('enums.attachment_types'))->nullable()->default(null);//null = Unknown
            $table->timestamps();

            $table->unique(['type', 'slug']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('attachment_types');
    }
}
