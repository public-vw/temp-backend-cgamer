<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenuPlaceholdersTable extends Migration
{

    public function up()
    {
        Schema::create('menu_placeholders', function (Blueprint $table) {
            $table->engine = 'MyISAM';

            $table->id();
            $table->foreignId('panel_id')->index()->constrained('panels')->cascadeOnDelete();
            $table->string('title',100);# sidebar, topmenu, footer, ...
            $table->timestamps();

            $table->unique(['panel_id' , 'title']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('menu_placeholders');
    }
}
