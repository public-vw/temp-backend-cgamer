<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactEmailsTable extends Migration
{

    public function up()
    {
        Schema::create('contact_emails', function (Blueprint $table) {
            $table->engine = 'MyISAM';

            $table->id();
            $table->morphs('emailable');

            $table->string('title')->nullable()->default(null);
            $table->string('email');

            $table->unsignedSmallInteger('order')->default(0);

            $table->text('description')->nullable()->default(null)->comment('اطلاعات اضافی در مورد ایمیل');

            $table->boolean('default')->default(false);
            $table->boolean('active')->default(true);

            $table->timestamp('email_verified_at')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('contact_emails');
    }
}
