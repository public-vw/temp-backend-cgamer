<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServerConfigsTable extends Migration
{

    public function up()
    {
        Schema::create('server_configs', function (Blueprint $table) {
            $table->engine = "MyIsam";

            $table->id();
            $table->string('key');
            $table->text('value')->nullable()->default(NULL);
            $table->enum('status', config_keys_all('enums.server_configs_status'))->default(config('enums.server_configs_status_default'));
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('server_configs');
    }
}
