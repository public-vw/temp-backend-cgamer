<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDescriptionToArticleCategoriesTable extends Migration
{
    public function up()
    {
        Schema::table('article_categories', function (Blueprint $table) {
            $table->unsignedTinyInteger('order')->default(0)->after('title');
            $table->string('heading')->nullable()->after('title');
            $table->text('description')->nullable()->default(null)->after('heading');
        });
    }

    public function down()
    {
        Schema::table('article_categories', function (Blueprint $table) {
            $table->dropColumn('description');
            $table->dropColumn('heading');
            $table->dropColumn('order');
        });
    }
}
