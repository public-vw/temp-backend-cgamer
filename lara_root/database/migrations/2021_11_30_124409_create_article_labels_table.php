<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticleLabelsTable extends Migration
{
    public function up()
    {
        Schema::create('article_labels', function (Blueprint $table) {
            $table->engine = 'MyISAM';

            $table->id();
            $table->foreignId('group_id')->constrained('article_label_groups')->cascadeOnDelete();
            $table->string('title');
            $table->unsignedSmallInteger('order')->default(1);
            $table->enum('color_theme', config_keys_all('enums.random_colors'));
            $table->timestamps();

            $table->unique(['group_id','title']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article_labels');
    }
}
