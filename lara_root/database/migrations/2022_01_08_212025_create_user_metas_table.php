<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserMetasTable extends Migration
{
    public function up()
    {
        Schema::create('user_metas', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users')->cascadeOnDelete();

            $table->text('about_me')->nullable();
            $table->date('birthdate')->nullable();


            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('user_metas');
    }
}
