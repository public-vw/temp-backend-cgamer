<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProvincesTable extends Migration
{

    public function up()
    {
        Schema::create('provinces', function (Blueprint $table) {
            $table->engine = 'MyISAM';

            $table->id();

            $table->string('title');
            $table->foreignId('country_id')->nullable()->default(null)->constrained()->nullOnDelete();
            $table->string('phone_precode',10)->nullable()->default(NULL);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('provinces');
    }
}
