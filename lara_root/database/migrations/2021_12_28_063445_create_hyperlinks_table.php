<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHyperlinksTable extends Migration
{
    public function up()
    {
        Schema::create('hyperlinks', function (Blueprint $table) {
            $table->id();
            $table->nullableMorphs('hyperlinkable'); // Where we added this hyperlink, e.g. you can find this hyperlink in the article with id 3
            $table->foreignId('user_id')->nullable()->default(null)->constrained('users')->nullOnDelete();

            $table->text('title')->comment('Read-only title, uses for e.g. to make better translation on anchors. no effect on front-end');
            $table->enum('url_type', config_keys_all('enums.hyperlink_type'))->nullable()->default(NULL);
            $table->text('url')->nullable()->default(NULL);

            $table->text('properties')->nullable()->default(null);
            // Json String, associative array
            // new_tab: true [means] new tab (_blank)
            // follow: false [means] nofollow

            $table->enum('status',config_keys_all('enums.hyperlinks_status'))->default(config('enums.hyperlinks_status_default'));

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('hyperlinks');
    }
}
