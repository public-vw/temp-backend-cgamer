<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttachmentsTable extends Migration
{

    public function up()
    {
        Schema::create('attachments', function (Blueprint $table) {
            $table->engine = 'MyIsam';

            $table->id();
            $table->nullableMorphs('attachmentable');
            $table->foreignId('user_id')->nullable()->default(null)->constrained('users')->nullOnDelete();

            // if not NULL will added to base of attachment url, like this: https://img.datarivers.org/
            $table->string('storage_disk',80)->default('temporary');
            $table->string('file_name',255);
            $table->string('file_ext',5)->nullable()->default(null);
            $table->string('file_size',20);

            $table->text('properties')->nullable()->default(null);
            // Json String, associative array
            // Image: width, height
            // Animation, Clip: width, height, duration
            // Sound: Duration

            $table->string('md5',32);
            $table->double('quality_rank',4,2)->unsigned()->nullable()->default(null);
            $table->foreignId('type_id')->constrained('attachment_types')->nullOnDelete();
            $table->string('mimetype',30);
            $table->text('original_name')->nullable();
            $table->boolean('temporary')->default(true);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('attachments');
    }
}
