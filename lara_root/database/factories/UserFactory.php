<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    protected $model = User::class;

    public function definition()
    {
        return [
            'name'               => $this->faker->name,
            'username'           => $this->faker->unique()->userName,
            'ref_code'           => Str::random(10),
            'mobile'             => $this->faker->unique()->e164PhoneNumber,
            'mobile_verified_at' => now(),
            'random_color'       => (string)$this->faker->numberBetween(0, count(config_keys_all('enums.random_colors')) - 1),
            'telegram_uid'       => null,
            'referer_id'         => $this->faker->boolean ?
                                    User::all()->random()->id : null,
            'email'              => $this->faker->unique()->safeEmail,
            'email_verified_at'  => now(),
            'password'           => Hash::make('password'),
            'remember_token'     => Str::random(10),
            'status'             => (string)$this->faker->numberBetween(0, count(config_keys_all('enums.users_status')) - 1),
        ];
    }

    public function unverified()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }
}
