<?php

namespace Database\Factories;

use App\Models\City;
use App\Models\ContactAddress;
use App\Models\Region;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Collection;

class ContactAddressFactory extends Factory
{
    protected $model = ContactAddress::class;

    public function definition()
    {
        return [
            'title'       => $this->faker->unique()->company,
            'order'       => $this->faker->numberBetween(0, 50),
            'geocode'     => $this->faker->paragraphs(2, true),
            'description' => $this->faker->paragraphs(2, true),
            'city_id'     => City::all()->random()->id,
            'region_id'   => Region::all()->random()->id,
            'latitude'    => $this->faker->latitude,
            'longitude'   => $this->faker->longitude,
            'map_zoom'    => $this->faker->randomNumber(1, true) + 10,
            'default'     => $this->faker->boolean,
            'active'      => $this->faker->boolean,
        ];
    }
}
