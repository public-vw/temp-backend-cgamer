<?php

namespace Database\Factories;

use App\Models\City;
use App\Models\Region;
use Illuminate\Database\Eloquent\Factories\Factory;

class RegionFactory extends Factory
{
    protected $model = Region::class;

    public function definition()
    {
        return [
            'title' => $this->faker->catchPhrase,
            'city_id'   => City::all()->random()->id,
        ];
    }
}
