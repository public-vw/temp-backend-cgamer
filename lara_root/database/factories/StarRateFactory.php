<?php

namespace Database\Factories;

use App\Models\StarRate;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class StarRateFactory extends Factory
{
    protected $model = StarRate::class;

    public function definition()
    {
        return [
            'user_id' => User::all()->random()->id,
            'ip'      => $this->faker->ipv4,
            'rate'    => $this->faker->numberBetween(0, 5),
            'weight'  => $this->faker->numberBetween(1, 3),
        ];
    }
}
