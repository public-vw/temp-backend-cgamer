<?php

namespace Database\Factories;

use App\Models\SocialChannel;
use App\Models\SocialLink;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class SocialLinkFactory extends Factory
{
    protected $model = SocialLink::class;

    public function definition()
    {
        return [
            'title'              => $this->faker->company . ' Social',
            'channel_id'         => SocialChannel::all()->random()->id,
            'connection_string'  => $this->faker->unique()->userName,
            'order'              => $this->faker->numberBetween(0, 50),
            'description'        => $this->faker->paragraphs(2, true),
            'default'            => $this->faker->boolean,
            'active'             => $this->faker->boolean,
            'social_verified_at' => $this->faker->dateTimeThisDecade(),
        ];
    }
}
