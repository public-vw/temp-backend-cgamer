<?php

namespace Database\Factories;

use App\Models\City;
use App\Models\Province;
use Illuminate\Database\Eloquent\Factories\Factory;

class CityFactory extends Factory
{
    protected $model = City::class;

    public function definition()
    {
        return [
            'title' =>  $this->faker->city,
            'province_id'   =>  Province::all()->random()->id,
            'phone_precode' =>  $this->faker->boolean ? $this->faker->randomNumber(3) : null,
        ];
    }
}
