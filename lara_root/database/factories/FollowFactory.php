<?php

namespace Database\Factories;

use App\Models\Follow;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class FollowFactory extends Factory
{
    protected $model = Follow::class;

    public function definition()
    {
        return [
            'master_id' => User::all()->random()->id,
            'slave_id' => User::all()->random()->id,
            'unfollow' => $this->faker->boolean,
        ];
    }
}
