<?php

namespace Database\Factories;

use App\Models\City;
use App\Models\ContactPhone;
use App\Models\Region;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class ContactPhoneFactory extends Factory
{
    protected $model = ContactPhone::class;

    public function definition()
    {
        return [
            'title'             => $this->faker->unique()->company . ' Phone',
            'number'            => $this->faker->unique()->e164PhoneNumber,
            'order'             => $this->faker->numberBetween(0, 50),
            'description'       => $this->faker->paragraphs(2, true),
            'city_id'           => City::all()->random()->id,
            'region_id'         => Region::all()->random()->id,
            'default'           => $this->faker->boolean,
            'active'            => $this->faker->boolean,
            'phone_verified_at' => $this->faker->dateTimeThisDecade(),
        ];
    }
}
