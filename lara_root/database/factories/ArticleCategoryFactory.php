<?php

namespace Database\Factories;

use App\Models\ArticleCategory;
use Illuminate\Database\Eloquent\Factories\Factory;

class ArticleCategoryFactory extends Factory
{
    protected $model = ArticleCategory::class;

    public function definition()
    {
        return [
            'title'     => $this->faker->realText(150),
            'parent_id' => ArticleCategory::count() && $this->faker->boolean ? ArticleCategory::all()->random()->id : null,
            'status'    => (string)$this->faker->numberBetween(0, count(config_keys_all('enums.article_categories_status')) - 1),
        ];
    }
}
