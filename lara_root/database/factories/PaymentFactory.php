<?php

namespace Database\Factories;

use App\Models\Currency;
use App\Models\Payment;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class PaymentFactory extends Factory
{
    protected $model = Payment::class;

    public function definition()
    {
        return [
            'title'         => 'Fake Payment',
            'user_id'       => User::all()->random()->id,
            'date_on_paper' => $this->faker->dateTimeBetween('-15 days', 'now'),
            'currency_id'   => Currency::all()->random()->id,
            'amount'        => $this->faker->randomNumber(8),
            'status'        => (string)$this->faker->numberBetween(0, count(config_keys_all('enums.payments_status')) - 1),
        ];
    }
}
