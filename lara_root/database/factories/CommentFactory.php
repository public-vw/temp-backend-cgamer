<?php

namespace Database\Factories;

use App\Models\Comment;
use Illuminate\Database\Eloquent\Factories\Factory;

class CommentFactory extends Factory
{
    protected $model = Comment::class;

    public function definition()
    {
        $ret = [
            'content'   => $this->faker->realText(300),
            'status'    => (string)$this->faker->numberBetween(0, count(config_keys_all('enums.comments_status')) - 1),
        ];


        #TODO: enable parent_id for comments in faker mode

/*        $ret['parent_id'] = $this->faker->boolean ? null
            : (Comment::where('commentable_type',$ret['commentable_type'])
                ->where('commentable_id',$ret['commentable_id'])
                ->get()
                ->random()->id ?? null);*/

        return $ret;
    }
}
