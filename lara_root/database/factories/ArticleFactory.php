<?php

namespace Database\Factories;

use App\Models\Article;
use App\Models\ArticleCategory;
use App\Models\Author;
use Illuminate\Database\Eloquent\Factories\Factory;

class ArticleFactory extends Factory
{
    protected $model = Article::class;

    public function definition()
    {
        return [
            'author_id'    => Author::all()->random()->id,
            'category_id'    => ArticleCategory::all()->random()->id,
            'heading'      => $this->faker->realText(150),
            'reading_time' => null,
            'content'      => "<p class='post-open-paragraph'>".$this->faker->realText(1000)."</p>",
            'status'       => (string)$this->faker->numberBetween(0, count(config_keys_all('enums.articles_status')) - 1),
        ];
    }
}
