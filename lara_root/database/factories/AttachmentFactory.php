<?php

namespace Database\Factories;

use App\Models\Attachment;
use App\Models\ModelAttachmentType;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class AttachmentFactory extends Factory
{
    protected $model = Attachment::class;

    protected $size = ['width' => 640, 'height' => 480];

    public function definition()
    {
        //TODO:get dimensions from attachment type
        $file = $this->getImage($this->size['width'], $this->size['height']);

        return [
            'storage_disk'  => 'permanent',
            'user_id'       => null,
            'file_name'     => $file['name'],
            'file_ext'      => $file['ext'],
//            'file_ext'      => array_rand(array_flip(['jpg', 'png', 'svg', 'mp3', 'mp4'])),
            'file_size'     => $file['size'],
//            'file_size'     => $this->faker->numberBetween(1000, 500000),
            'properties'    => $this->size,
            'md5'           => $file['md5'],
            'quality_rank'  => $this->faker->numberBetween(1, 10),
            'mimetype'      => $file['mime'],
            'temporary'     => false,
            'original_name' => $this->faker->catchPhrase,
        ];
    }

    // $category : any, animals, arch, nature, people, tech
    // $filter : null, grayscale, sepia
    protected function getImage(int $width, int $height, string $category = 'any', string $filter = null): array
    {
        $url = "https://placeimg.com/$width/$height/$category" . ($filter ? "/$filter" : "");
        $arrContextOptions = array(
            "ssl" => array(
                "verify_peer"      => false,
                "verify_peer_name" => false,
            ),
        );
        $content = file_get_contents($url, false, stream_context_create($arrContextOptions));
        $filename = $this->makeFilename();
        $file_ext = 'jpg';
        $fullname = 'images/' . $filename . '.' . $file_ext;

        $file = Storage::disk('permanent')->put($fullname, $content);

        return [
            'name' => $filename,
            'ext'  => $file_ext,
            'size' => Storage::disk('permanent')->size($fullname),
            'mime' => Storage::disk('permanent')->mimeType($fullname),
            'md5'  => md5($content),
        ];
    }

    protected function makeFilename()
    {
        return Carbon::now()->format('Y-m/d-his-') . rand(101, 999);
    }
}
