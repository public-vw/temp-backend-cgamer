<?php

namespace Database\Factories;

use App\Models\Country;
use App\Models\Province;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProvinceFactory extends Factory
{
    protected $model = Province::class;

    public function definition()
    {
        return [
            'title' =>  $this->faker->catchPhrase,
            'country_id'    =>  Country::all()->random()->id,
            'phone_precode' =>  $this->faker->boolean ? $this->faker->countryCode : null,
        ];
    }
}
