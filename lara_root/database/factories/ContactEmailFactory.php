<?php

namespace Database\Factories;

use App\Models\ContactEmail;
use Illuminate\Database\Eloquent\Factories\Factory;

class ContactEmailFactory extends Factory
{
    protected $model = ContactEmail::class;

    public function definition()
    {
        return [
            'title'             => $this->faker->company . ' Email',
            'email'             => $this->faker->companyEmail,
            'order'             => $this->faker->numberBetween(0, 50),
            'description'       => $this->faker->paragraphs(2, true),
            'default'           => $this->faker->boolean,
            'active'            => $this->faker->boolean,
            'email_verified_at' => $this->faker->dateTimeThisDecade(),
        ];
    }
}
