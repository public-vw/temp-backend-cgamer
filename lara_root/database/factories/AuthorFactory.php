<?php

namespace Database\Factories;

use App\Models\Author;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class AuthorFactory extends Factory
{
    protected $model = Author::class;

    public function definition()
    {
        return [
            'user_id'       => User::all()->random()->id,
            'article_limit' => $this->faker->numberBetween(3,20),
            'status'        => (string)$this->faker->numberBetween(0, count(config_keys_all('enums.authors_status')) - 1),
        ];
    }
}
