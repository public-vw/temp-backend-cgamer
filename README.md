# Laravel 10 Pack
## Getting Started
use Script approach or manual approach
### Script Approach
simply run `./shell_scripts/starter.sh` and follow the instructions

### Manual Approach
copy general env file
decide if this is a development env or production env

mkdir data
chmod 777 data

### Use as base on a new project
1. create a new project repo in github|gitlab
2. initialize the project locally
`git clone <repo address>`
`git switch --create master`
`touch README.md`
`git add .`
`git commit -m "creates master branch"`
`git push -u origin master`
3. add 'Laravel 10 pack' to the project as a new branch
`git remote add larapack-origin git@datarivers.gitlab.com:own-projects-2023/lara-10-site-pack.git;`
`git fetch larapack-origin;`
`git checkout -b global-pack larapack-origin/master;`
`git checkout master;`

### Add remote repo of Gitlab to the server
`git clone https://username:password@gitlab.com/repo.git`

## Commands 
### using artisan
`./artisan.sh <command data>`

### using composer
`./composer.sh <command data>`

### using mail server
the port can be changed in the general env file

http://localhost:8025 
### using dump server
`./artisan.sh dump-server`

### using phpMyAdmin
#### starting the pma server
`bash ./pma/start.sh`
#### viewing the browser
the port can be changed in the general env file

http://localhost:8080
#### stopping the pma server
bash ./pma/stop.sh
