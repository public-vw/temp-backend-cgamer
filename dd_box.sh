#!/bin/bash

reset
docker compose exec -u $(id -u):$(id -g) phpfpm php artisan dump-server $@
